﻿using Grpc.Core;
using MBILibrary.Enums;
using MBILibrary.refDLL;
using MBILibrary.Util;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBIClient
{
    public sealed class GrpcClientChannel
    {
        private GrpcClientChannel() { }
        #region properties
        private const string gRPCStatusChangingFormat = "gRPC Status Changing ::: {0} -> {1} on {2}";
        private const string gRPCStatusFormat = "gRPC Server Status :: {0}.";

        private static Channel channel = null;
        /// <summary>
        /// 已連線成功的 GRPC Channel Object
        /// <para>當 GrpcServerIsRunning = true 時, 就可以透過此物件進行 GRPC 溝通</para>
        /// </summary>
        public static Channel GrpcChannel => channel;
        /// <summary>
        /// GRPC Channel 是否有連線 ?
        /// <para>true :: 連線中, channel != null</para>
        /// <para>false :: 已斷線, channel == null</para>
        /// </summary>
        public static bool IsGrpcChannelAlive => channel != null;
        /// <summary>
        /// 指定 gRPC Connecting 的最大等待時間為 0.1 秒, 沒連線就跳下一次再連線一次
        /// <para>1. 內部 ConnectAsync() 會等 0.1 秒</para>
        /// <para>2. 外部 GrpcListeningService 有沿用此參數, 也會等 0.1 秒</para>
        /// <para>結論 ::: 每次偵測 gRPC 的連線狀態過程歷時 0.1 + 0.1 秒, 共 0.2 秒</para>
        /// </summary>
        public const int ChannelConnectAsyncDeadLine = 100;

        /// <summary>
        /// gRPC Status Latest Update Time
        /// </summary>
        private static DateTime LatestUpdated = DateTime.MinValue;

        /// <summary>
        /// Initial Value ::: false(Close)
        /// </summary>
        private static bool _GrpcServerIsRunning = false;
        /// <summary>
        /// 監聽 "gRPC Server Status" 是否已經開啟 ?
        /// <para>已開啟, 才會傳送 message 過去</para>
        /// <para>未開啟, 將 message log 在本機, 等待連線成功後, 再傳送過去</para>
        /// </summary>
        public static bool GrpcServerIsRunning {
            get { return _GrpcServerIsRunning; }
            set {
                if (_GrpcServerIsRunning != value) {
                    UtilConstants.GrpcServerIsRunning = value;
                    UtilGlobalFunc.SetMBISimBridgeConsoleTitleMessage(); // 立即更新 gRPC Server 的連線狀態 ::: OPEN/CLOSE

                    #region gRPC OPEN... Connected
                    if (!_GrpcServerIsRunning && value) {
                        LatestUpdated = DateTime.Now;
                        _GrpcServerIsRunning = value;
                        ForceCheckStatus();
                        ShowGrpcConnStatusChanging(GrpcStatusEnum.CLOSE, GrpcStatusEnum.OPEN);
                    }
                    #endregion

                    #region gRPC Closed... Dis-Connected
                    if (_GrpcServerIsRunning && !value) {
                        LatestUpdated = DateTime.Now;
                        _GrpcServerIsRunning = value;
                        ForceCheckStatus();
                        ShowGrpcConnStatusChanging(GrpcStatusEnum.OPEN, GrpcStatusEnum.CLOSE);

                        #region 與 GrpcCommunicate.cs "Sending streaming Request" while loop 同步
                        // 因為 GrpcCommunicate.cs 那邊會等待 user 下達訊息指令 Console.ReadLine(), 所以當這邊偵測到 gRPC 斷線後, 馬上送出一個 "Enter" 指令給 GrpcCommunicate.cs
                        // 這樣 GrpcCommunicate.cs 就不會繼續 Hold 住不動作, 可以與這邊同步反應結果, GrpcCommunicate.cs 那邊會進入 RpcException 後, 再進行後續處理行為
                        ThreadPool.QueueUserWorkItem((o) => {
                            var hWnd = Process.GetCurrentProcess().MainWindowHandle;
                            User32.PostMessage(hWnd, User32.WM_KEYDOWN, User32.VK_RETURN, 0);
                            // 程式自動 send "Enter" 指令給 Console.ReadLine(),
                            // 讓 gRPC 的 "Sending streaming Request" while loop 可以馬上反應, 知道目前的 gRPC Connection 斷掉了
                            // 馬上進行後續的處置行為
                        });
                        #endregion
                    }
                    #endregion
                }
            }
        }
        #endregion

        /// <summary>
        /// 強制切換 GRPC 連線/斷線 狀態
        /// <para>true ::: 如果還是 [連線] 狀態, 就強制 [關閉連線]</para>
        /// <para>false ::: 如果還是 [斷線] 狀態, 就強制 [啟動連線]</para>
        /// </summary>
        private static bool ForceCheckStatus() {
            var res = ConfirmChannelConnectionStatus();
            if (res) { // 如果還是 [連線] 狀態, 就強制 [關閉連線]
                if (IsGrpcChannelAlive) { _ = channel.ShutdownAsync(); }
                channel = null;
            } else { // 如果還是 [斷線] 狀態, 就強制 [啟動連線]
                _ = IsChannelReadyAsync();
            }
            return res;
        }
        /// <summary>
        /// 顯示 GRPC 當下的連線改變狀態
        /// </summary>
        private static void ShowGrpcConnStatusChanging(GrpcStatusEnum From, GrpcStatusEnum To) {
            UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, gRPCStatusChangingFormat, From, To, LatestUpdated.ToString(UtilDateTime.Format_HHmmssfff)));
            UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, gRPCStatusFormat, To == GrpcStatusEnum.OPEN ? "Connected" : "Dis-Connect"));
        }

        /// <summary>
        /// 檢查 GRPC Channel 當下的連線狀態
        /// <para>修改 GrpcServerIsRunning 參數值 :: true/false</para>
        /// </summary>
        private static void CheckChannelStatus() => GrpcServerIsRunning = IsGrpcChannelAlive ? channel.State == ChannelState.Ready : false;

        /// <summary>
        /// 確認 GRPC Channel 連線狀態
        /// </summary>
        private static bool ConfirmChannelConnectionStatus() {
            if (!IsGrpcChannelAlive) { return false; }

            CheckChannelStatus();
            if (!GrpcServerIsRunning) {
                if (IsGrpcChannelAlive) { _ = channel.ShutdownAsync(); }
                channel = null;
            }
            return GrpcServerIsRunning;
        }

        /// <summary>
        /// 取得 Channel 當下的狀態資訊
        /// </summary>
        private static string GetChannelInfo => IsGrpcChannelAlive ? ShowChannelInfo : "channel is null";
        /// <summary>
        /// 顯示 channel 的基本屬性
        /// </summary>
        private static string ShowChannelInfo => string.Format(CultureInfo.CurrentCulture, "channelId({0}), Target({1}), State({2})", channel.GetHashCode(), channel.Target, channel.State);
        /// <summary>
        /// 顯示 GRPC 連線失敗
        /// </summary>
        private static string ConnFailureMsg => string.Format(CultureInfo.CurrentCulture, "{0}failure.. {1}, ", UtilGlobalFunc.GetMBIServerInfo, GetChannelInfo);
        /// <summary>
        /// 顯示 GRPC 連線成功
        /// </summary>
        private static string ConnSuccessMsg => string.Format(CultureInfo.CurrentCulture, "{0}alive ? {1}", UtilGlobalFunc.GetMBIServerInfo, GetChannelInfo);

        #region functions
        /// <summary>
        /// 監聽 "gRPC Server" 是否已經開啟 ?
        /// </summary>
        public static async Task<bool> IsChannelReadyAsync() {
            var ConnStatus = ConfirmChannelConnectionStatus();
            if (ConnStatus) { return GrpcServerIsRunning; } // GRPC 連線還活著.. 不需要重新連線

            #region 啟動 GRPC 重新連線作業
            string FailMsg = string.Empty;
            try {
                channel = GetChannel();
                UtilGlobalFunc.ShowMsgByDebug4BigMason(ConnSuccessMsg);
                if (IsGrpcChannelAlive) { await channel.ConnectAsync(deadline: DateTime.UtcNow.AddMilliseconds(ChannelConnectAsyncDeadLine)); }
            } catch (TaskCanceledException ex) {
                FailMsg = string.Format(CultureInfo.CurrentCulture, "{0}TaskCanceledException Message :: {1}", ConnFailureMsg, ex.Message);
            } catch (RpcException ex) {
                FailMsg = string.Format(CultureInfo.CurrentCulture, "{0}RpcException Status({1}), StatusCode({2}), Message :: {3}", ConnFailureMsg, ex.Status, ex.StatusCode, ex.Message);
                switch (ex.StatusCode) {
                    case StatusCode.Unknown: break;
                    case StatusCode.Aborted: break;
                    case StatusCode.AlreadyExists: break;
                    case StatusCode.Cancelled: break;
                    case StatusCode.DataLoss: break;
                    case StatusCode.DeadlineExceeded: break;
                    case StatusCode.FailedPrecondition: break;
                    case StatusCode.NotFound: break;
                    case StatusCode.Unavailable: break;
                    case StatusCode.Unimplemented: break;
                    default: break;
                }
            } catch (Exception ex) {
                FailMsg = string.Format(CultureInfo.CurrentCulture, "{0}Exception Message :: {1}", ConnFailureMsg, ex.Message);
            }
            UtilGlobalFunc.ShowMsgByDebug4BigMason(FailMsg);
            return ConfirmChannelConnectionStatus(); // 取得此次 GRPC 重新連線的結果
            #endregion
        }
        /// <summary>
        /// The "PORT Number" must match the port of the gRPC Server.
        /// </summary>
        private static Channel GetChannel(string networkAddress = null, string rootCertificate = null) {
            string targetGrpcServer = string.IsNullOrEmpty(networkAddress) ? UtilConstants.GrpcServer : networkAddress;
            ChannelCredentials rootCert = string.IsNullOrEmpty(rootCertificate) ? ChannelCredentials.Insecure : new SslCredentials(rootCertificate);
            return new Channel(targetGrpcServer, rootCert);
        }
        #endregion

    }
}
