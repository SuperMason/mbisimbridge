﻿using MBIServer.GrpcTesting;
using MBIServer.MbiGrpcService;
using MBILibrary.Enums;
using MBILibrary.Util;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using System.Globalization;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MBIClient
{
    /// <summary>
    /// "gRPC Client" Send Message to "MBIServer"
    /// <para>專門處理將 "Air Manager", "X-Plane" 的交易訊息, 即時傳送給 "MBIServer"</para>
    /// </summary>
    public class GrpcClientSender : IDisposable
    {
        #region Properties
        /// <summary>
        /// 紀錄 Send to MBIServer Failed, 等再度連線後, 重新寄送一次
        /// </summary>
        private readonly ConcurrentQueue<Dictionary<SendGrpcTypeEnum, MessageEventArgs>> Queue4Process = new ConcurrentQueue<Dictionary<SendGrpcTypeEnum, MessageEventArgs>>();
        /// <summary>
        /// 累計接收到 "MBIServer" Success 的回覆次數
        /// <para>與 current_id 數字應該一致, 表示每一個取得序號的 object 都有成功回覆</para>
        /// </summary>
        public static double GetGrpcServerSuccessResponseCNT { get; set; }
        /// <summary>
        /// 累計 "MBIServer" 回覆失敗的次數
        /// </summary>
        public static double GetGrpcServerFailureResponseCNT => current_id - GetGrpcServerSuccessResponseCNT;
        protected static readonly object lockElement = new object();
        protected static double current_id = 0;
        /// <summary>
        /// 序號
        /// </summary>
        public double Id { get; private set; }
        /// <summary>
        /// Result of sending Message to "MBIServer"
        /// </summary>
        public string SendResultMessage { get; private set; }
        /// <summary>
        /// 是否有正確送達 "MBIServer" ?
        /// </summary>
        public SendGrpcResultEnum SendStatus { get; private set; }
        /// <summary>
        /// 紀錄 EventArgs 參數中的 Data 屬性值
        /// </summary>
        public string EventDataMessage { get; private set; }
        /// <summary>
        /// 取得 MBIServer 的 Response 訊息格式
        /// </summary>
        private const string GetResponseFromServerMsg = "Get {0} Response From Server :: {1}";
        #endregion


        #region Constructor
        /// <summary>
        /// "gRPC Client" Send Message to "MBIServer"
        /// <para>專門處理將 "Air Manager", "X-Plane" 的交易訊息, 即時傳送給 "MBIServer"</para>
        /// </summary>
        public GrpcClientSender(SendGrpcTypeEnum sendType, MessageEventArgs e) {
            if (!e.Data.StartsWith(UtilConstants.GrpcReSendEventDataMessageHeader)) // 如果是之前傳送失敗的訊息, 就不用再取序號, 直接過號
            { lock (lockElement) { Id = ++current_id; } }
            SendMessageToGrpcServer(sendType, e);
        }
        #endregion


        #region 訊息分類器, 依據 sendType 類別, 呼叫特定的 gRPC service 進行訊息傳送
        /// <summary>
        /// 實際將 Message 傳送至 MBIServer
        /// </summary>
        private void SendMessageToGrpcServer(SendGrpcTypeEnum sendType, MessageEventArgs e) {
            SendStatus = SendGrpcResultEnum.Failure; // initialize
            EventDataMessage = e.Data;

//            先把所有的訊息 Queue 在 Queue4Process
//                之後再用 ConcurrentQueue 進行處理....

            try {
                if (GrpcClientChannel.IsChannelReadyAsync().Result) {
                    #region 實際使用 "自定義的 MbiSimBridge gRPC services" 進行通訊
                    if (sendType == SendGrpcTypeEnum.AirManagerExecuteDataRefWrite) { _ = Service4AirManagerExecuteDataRefWrite(e); }
                    else if (sendType == SendGrpcTypeEnum.AirManagerExecuteCommand) { Service4AirManagerExecuteCommand(e); }
                    else if (sendType == SendGrpcTypeEnum.XPlaneConnected) { Service4XPlaneConnected(e); }
                    else if (sendType == SendGrpcTypeEnum.XPlaneReadDataRef) { Service4XPlaneReadDataRef(e); }
                    else { }
                    #endregion

                    SendTestingMsg();

                    #region Success Result
                    lock (lockElement) { ++GetGrpcServerSuccessResponseCNT; }
                    SendResultMessage = SendGrpcResultEnum.Success.ToString();
                    SendStatus = SendGrpcResultEnum.Success;
                    #endregion
                } else { SendResultMessage = string.Format(CultureInfo.CurrentCulture, "'{0}' NOT Ready, IPAddress : {1}", UtilConstants.appConfig.gRPCServerName, UtilConstants.GrpcServer); }
            } catch (Exception ex) { SendResultMessage = string.Format(CultureInfo.CurrentCulture, "{0}.SendMessageToGrpcServer(),  Exception :: {1}", GetType().Name, ex.Message); }
        }

        /// <summary>
        /// 測試使用 "自定義的 HelloWorld gRPC services" 進行通訊
        /// </summary>
        private void SendTestingMsg() {
#if DEBUG
            if (UtilConstants.appConfig.Debug4BigMason) {
                Service4TestingSayHelloAsync();
                Service4TestingPlusAsync();
            }
#endif
        }

        #endregion

        /// <summary>
        /// Log Message
        /// </summary>
        protected void Log(string msg) => UtilGlobalFunc.Log(msg);

        #region 呼叫特定的 gRPC service
        #region 實際使用 "自定義的 MbiSimBridge gRPC services" 進行通訊
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        private async Task<string> Service4AirManagerExecuteDataRefWrite(MessageEventArgs e) {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return string.Empty; }
                var grpcClient = new HandleAirManager.HandleAirManagerClient(GrpcClientChannel.GrpcChannel);
                var arg = (DataRefEventArgs)e;
                DataRefRequest request = null;
                object ObjVal = arg.DataBus4Object.ObjVal;
                if (ObjVal != null) {
                    if (arg.DataBus4Object.IsObjValSingleValue || ObjVal.GetType() == typeof(char)) {
                        // 把 byte, char, float, int, bool, double 轉置成可以寫入 GrpcServer 的值
                        // arg.DataBus4Object.ObjVal  raw data type 包含 'char'
                        request = new DataRefRequest { DataRefName = arg.DataBus4Object.DataRef, FValue = arg.DataBus4Object.ObjVal2Float, Description = arg.DataBus4Object.Description };
                    } else if (ObjVal.GetType() == typeof(string)) {
                        // 把 string 轉置成可以寫入 GrpcServer 的值
                        request = new DataRefRequest { DataRefName = arg.DataBus4Object.DataRef, SValue = arg.DataBus4Object.ObjVal2String, Description = arg.DataBus4Object.Description };
                    } else { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefNoValueChangedMessage, ObjVal.GetType())); }
                } else { Log(UtilConstants.DataRefValueIsNullMessage); }

                var response = await grpcClient.ExecuteDataRefWriteAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "AirManager ExecuteDataRefWriteAsync()", response.Result));
                return response.Result;
            } catch (Exception ex) { LogAndDispose(ex.Message); return string.Empty; }
        }
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        private async void Service4AirManagerExecuteCommand(MessageEventArgs e) {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return; }
                var grpcClient = new HandleAirManager.HandleAirManagerClient(GrpcClientChannel.GrpcChannel);
                var arg = (CommandEvevntArgs)e;
                var request = new CmdRequest { Name = arg.Command.Name, Value = arg.Command.Value, Description = arg.Command.Description };

                var response = await grpcClient.ExecuteCommandAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "AirManager ExecuteCommandAsync()", response.Result));
            } catch (Exception ex) { LogAndDispose(ex.Message); }
        }
        /// <summary>
        /// X-Plane 已連線成功
        /// </summary>
        private async void Service4XPlaneConnected(MessageEventArgs e) {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return; }
                var grpcClient = new HandleXPlane.HandleXPlaneClient(GrpcClientChannel.GrpcChannel);
                var request = new ConnRequest { ConnMessage = string.Format(CultureInfo.CurrentCulture, UtilConstants.XPlaneConnectedMsg, nameof(GrpcClientSender), e.Data) };
                var response = await grpcClient.ConnectedAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "X-Plane ConnectedAsync()", response.Result));
            } catch (Exception ex) { LogAndDispose(ex.Message); }
        }
        /// <summary>
        /// 即時取得 X-Plane 的 DataRef 更新值
        /// </summary>
        private async void Service4XPlaneReadDataRef(MessageEventArgs e) {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return; }
                var grpcClient = new HandleXPlane.HandleXPlaneClient(GrpcClientChannel.GrpcChannel);
                var arg = (DataRefEventArgs)e;
                DataRefRequest request = null;
                if (arg.DataBusIsBytes) {
                    float val = arg.DataBus4Bytes.ObjVal2Float;
                    if (!string.IsNullOrEmpty(arg.DataBus4Bytes.Type) && arg.DataBus4Bytes.IsSingleIntType)
                    { request = new DataRefRequest { DataRefName = arg.DataBus4Bytes.DataRef, Description = arg.DataBus4Bytes.Description, FValue = (int)val }; }
                    else
                    { request = new DataRefRequest { DataRefName = arg.DataBus4Bytes.DataRef, Description = arg.DataBus4Bytes.Description, FValue = val }; }
                } else if (arg.DataBusIsString) {
                    request = new DataRefRequest { DataRefName = arg.DataBus4String.DataRef, Description = arg.DataBus4String.Description, SValue = arg.DataBus4String.RawStringValue };
                } else { }

                var response = await grpcClient.ReadDataRefAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "X-Plane ReadDataRefAsync()", response.Result));
            } catch (Exception ex) { LogAndDispose(ex.Message); }
        }
        #endregion

        #region 測試使用 "自定義的 HelloWorld gRPC services" 進行通訊
        /// <summary>
        /// 使用 "測試的 HelloWorld gRPC services" 進行通訊 : SayHelloAsync()
        /// </summary>
        private async void Service4TestingSayHelloAsync() {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return; }
                var grpcClient = new GreetService.GreetServiceClient(GrpcClientChannel.GrpcChannel);
                var request = new HelloRequest { Name = string.Format(CultureInfo.CurrentCulture, "{0}__{1}__{2}", nameof(MBIClient), UtilGlobalFunc.GetOnLineTimeTick, EventDataMessage) };
                var response = await grpcClient.SayHelloAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "SayHelloAsync()", response.Message));
            } catch (Exception ex) { LogAndDispose(ex.Message); }
        }
        /// <summary>
        /// 使用 "測試的 HelloWorld gRPC services" 進行通訊 : PlusAsync()
        /// </summary>
        private async void Service4TestingPlusAsync() {
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { return; }
                var grpcClient = new CalculatorService.CalculatorServiceClient(GrpcClientChannel.GrpcChannel);
                var request = new CalRequest { NumA = DateTime.Now.Millisecond, NumB = new Random(DateTime.Now.Millisecond).Next(1, 1000) };
                var response = await grpcClient.PlusAsync(request);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, GetResponseFromServerMsg, "PlusAsync()", string.Format(CultureInfo.CurrentCulture, "{0} + {1} = {2}", request.NumA, request.NumB, response.Result)));
            } catch (Exception ex) { LogAndDispose(ex.Message); }
        }
        #endregion
        #endregion


        private void LogAndDispose(string msg) {
            Log(msg);
            Dispose();
        }
        public void Dispose()
        { GC.SuppressFinalize(this); }

    }
}