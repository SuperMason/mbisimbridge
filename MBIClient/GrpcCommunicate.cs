﻿using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using MBILibrary.Util;
using MBILibrary.Models;
using MBIServer.MbiGrpcService;
using MBILibrary.Enums;
using Google.Protobuf.WellKnownTypes;
using XPlaneConnector.DataRef;
using XPlaneConnector.Instructions;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

namespace MBIClient
{
    /// <summary>
    /// Client MBISimBridge(gRPC Client) 剛啟動時所觸發的 BackgroundService, 進行 Online 通知 MBIServer(gRPC Server) 行為
    /// <para>Initial First Command ::: 向 MBIServer 登記 Local MBISimBridge 已上線</para>
    /// <para>Others Command ::: 開始進行訊息溝通交換</para>
    /// </summary>
    public class GrpcCommunicate : IHostedService, IDisposable
    {
        #region Properties
        /// <summary>
        /// 觸發更新 Air Manager DataRef value 的系統名稱
        /// </summary>
        private readonly TriggerByWhatSystem triggerBySystem = TriggerByWhatSystem.FromGRPC;
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private readonly ILogger<GrpcCommunicate> _logger;
        /// <summary>
        /// Client 端 Host Name
        /// </summary>
        private readonly string HostingName = string.Empty;
        /// <summary>
        /// 等待 gRPC 連線
        /// <para>值必須以毫秒為單位, 轉譯為 -1 (表示無限逾時)、0 或是小於或等於 Int32.MaxValue 的正整數。</para>
        /// <para>避免發生 System.ArgumentOutOfRangeException, 因為是包在 Microsoft.Extensions.Hosting 元件裡, 這應該是元件本身的限制, 因為會死在 IHostBuilder 元件裡</para>
        /// </summary>
        private int WaitingInterval = -1; // 微毫秒
        /// <summary>
        /// 是否已經註冊至 MBIServer ?
        /// </summary>
        private bool IsRegisteredOnGrpcServer = false;
        /// <summary>
        /// 因為 object or value is null, 所以不需要去通知 Air Manager
        /// </summary>
        private static string DoNotNeedtoNotifyAM = string.Format(CultureInfo.CurrentCulture, "{0}.{1}, Do NOT Notify to '{2}'", nameof(GrpcCommunicate), "{0} :: {1}({2}) is null", UtilConstants.AirManagerSoftware);
        private const string EventFunc4ByteArray = "NotifyChangedToAirManagerByte[]";
        private const string EventFunc4String = "NotifyChangedToAirManagerString";
        /// <summary>
        /// Update "X-Plane", "Air Manager" Data
        /// </summary>
        public WriteDataRefCtrller WriteDrCtrller = new WriteDataRefCtrller();
        private HandleClientServerCommunicate.HandleClientServerCommunicateClient grpcClient = null;
        #endregion

        /// <summary>
        /// Log Message
        /// </summary>
        private void Log(string msg) => UtilGlobalFunc.Log(msg);

        #region Constructor
        public GrpcCommunicate(ILogger<GrpcCommunicate> logger, IHostApplicationLifetime hostApplicationLifetime, IHostLifetime hostLifetime, IHostEnvironment hostEnvironment) {
            _hostApplicationLifetime = hostApplicationLifetime;
            _logger = logger;
            HostingName = string.Format(CultureInfo.CurrentCulture, "{0} ::: {1}", nameof(GrpcCommunicate), string.Format(CultureInfo.CurrentCulture, UtilConstants.GrpcHostNameFormat, UtilConstants.HostRegistryLicenseGuid, UtilConstants.HostRegistryMachineName));
            hostApplicationLifetime.ApplicationStarted.Register(this.OnStarted);
            hostApplicationLifetime.ApplicationStopping.Register(this.OnStopping);
            hostApplicationLifetime.ApplicationStopped.Register(this.OnStopped);
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, HostingName));
            //_logger.LogInformation($"主機環境：" +
            //                            $"ApplicationName = {hostEnvironment.ApplicationName}\r\n" +
            //                            $"EnvironmentName = {hostEnvironment.EnvironmentName}\r\n" +
            //                            $"RootPath = {hostEnvironment.ContentRootPath}\r\n" +
            //                            $"Root File Provider = {hostEnvironment.ContentRootFileProvider}\r\n");
        }
        #endregion


        #region function()
        private void OnStarted() => _logger.LogInformation("Run OnStarted()");
        private void OnStopping() => _logger.LogInformation("Run OnStopping()");
        private void OnStopped() => _logger.LogInformation("Run OnStopped()");
        public void Dispose() {
            _logger.LogCritical(string.Format(CultureInfo.CurrentCulture, "Exiting {0}...", nameof(GrpcCommunicate)));
            _hostApplicationLifetime.StopApplication();
            //GC.SuppressFinalize(this);
        }
        private void LogAndDispose(string msg) {
            Log(msg);
            //GC.SuppressFinalize(grpcClient);
            grpcClient = null;
            //Dispose();
        }
        #endregion


        #region StartAsync() & StopAsync()
        public async Task StopAsync(CancellationToken stoppingToken) {
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, "Run Host.StopAsync() and Restart to {0}", UtilGlobalFunc.GetMBIServerInfo));
            await StartAsync(new CancellationToken()); // Restart self, keep going to Listen MBIServer
        }
        public async Task StartAsync(CancellationToken stoppingToken) {
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, "Run Host.StartAsync() and {0}", UtilGlobalFunc.GetMBIServerInfo));

            // 當 MBIServer.exe 沒開啟時, 這邊會一直丟 "於 System.Threading.Tasks.TaskCanceledException 擲回例外狀況: 'mscorlib.dll'" 錯誤訊息
            // 開啟 MBIServer.exe 後, 就會停止丟錯誤訊息
            // 看如何偵測此錯誤訊息... 用來記錄 GRPC connection 狀態...
            //
            // 此 錯誤訊息是 .NET 官方定義的, 可以忽略他
            // 網友建議如下 : 
            // In the .Net framework itself when you pass a CancellationToken as a parameter you will get back a TaskCanceledException.
            // I would not go against that and create my own design pattern because people who are familiar with .Net will be familiar with your code.
            // My guideline is this:
            // The one that cancels the token is the one that should handle the TaskCanceledException, so
            // If you're using a CancellationToken inside your method for your own reasons, go ahead and use a try-catch block.
            // But if you get the token as a parameter, let the exception be thrown.

            while (!stoppingToken.IsCancellationRequested) {
                if (GrpcClientChannel.GrpcServerIsRunning) {
                    WaitingInterval = -1;// WaitingInterval = -1, 表示持續一直等.. 不會執行 while looping, 這樣才能在 ExeGrpcSendMessage() 裡, 用 gRPC 等待彼此的訊息交換
                    await ExeGrpcSendMessage(stoppingToken);
                } else {
                    WaitingInterval = 1; // WaitingInterval = 1, 表示 gRPC 尚未建立連線, 所以每 1 微毫秒, 持續監聽 gRPC Server 是否已經啟動? 聽到之後就會馬上切換至 -1
                }
                await Task.Delay(WaitingInterval, stoppingToken);
            }
        }
        #endregion


        #region gRPC 已連線, 所以開始以 gRPC 通訊。Microsoft.Extensions.Hosting.BackgroundService 必須持續活著, 讓 gRPC run 在 Hosting.BackgroundService 上面, 因此 WaitingInterval 設為 -1
        /// <summary>
        /// gRPC 已連線, 可執行訊息傳送
        /// <para>Initial First Command ::: 向 MBIServer 登記 Local MBISimBridge 已上線</para>
        /// <para>Others Command ::: 開始進行訊息溝通交換</para>
        /// </summary>
        private async Task ExeGrpcSendMessage(CancellationToken stoppingToken) {
            if (!GrpcClientChannel.IsChannelReadyAsync().Result) { await StopAsync(stoppingToken); } // Channel 尚未建立, Stop && Restart self, keep going to Listen MBIServer

            bool ErrOccur = true;
            try {
                if (!GrpcClientChannel.IsGrpcChannelAlive) { throw new Exception("Mason ::: channel is closed."); }
                grpcClient = new HandleClientServerCommunicate.HandleClientServerCommunicateClient(GrpcClientChannel.GrpcChannel);
                using (var sendData = grpcClient.SendingOrder(new CallOptions())) {
                    #region Receiving streaming Response, 啟動後, 獨立運作, 等待 MBIServer 的 ResponseStream
                    var responseTask = Task.Run(async () => {
                        try {
                            while (await sendData.ResponseStream.MoveNext(stoppingToken)) {
                                OrderResponse response = sendData.ResponseStream.Current;
                                if (response.SendMessageTarget == null) { return; } // 對象不明確.. 不處理

                                var Target = response.SendMessageTarget.Value.ToEnum<GrpcSendMessageTargetEnum>();
                                string ClientUUID = response.UniqueKeyOfMachine;
                                string ServerMsg = response.ServerResponse;
#if DEBUG
                                #region Testing Sample Code
                                if (UtilConstants.appConfig.Debug4BigMason) {
                                    #region IList, IDictionary sample code
                                    IList<Person> pList = response.Persons;
                                    foreach (Person p in pList)
                                    { Log(string.Format(CultureInfo.CurrentCulture, "Pid({0}), Name({1}), Active({2}), LocalDateTime({3})", p.Pid, p.Name, p.Active, p.CurrentDateTime.ToDateTime().ToLocalTime())); }

                                    IDictionary<string, CmdResponse> amCmdRes = response.AmCmdRes;
                                    foreach (var am in amCmdRes)
                                    { Log(string.Format(CultureInfo.CurrentCulture, "am Key({0}), Value({1})", am.Key, am.Value)); }
                                    #endregion

                                    #region proto Any Type Sample :: Add, Get, Del
                                    var addResult = UtilByteString.FromByteArray<UserModel>(response.AnyAddRes.ResultMsg.Value.ToByteArray());
                                    Log(string.Format(CultureInfo.CurrentCulture, "ADD user ::: Id({0}), Name({1}), Age({2})", addResult.Id, addResult.Name, addResult.Age));

                                    var users = response.AnyGetRes.ResultMsgs.Select(a => UtilByteString.FromByteArray<UserModel>(a.Value.ToByteArray()));
                                    foreach (var user in users)
                                    { Log(string.Format(CultureInfo.CurrentCulture, "Get users ::: Id({0}), Name({1}), Age({2})", user.Id, user.Name, user.Age)); }

                                    var delResult = UtilByteString.FromByteArray<string>(response.AnyDelRes.ResultMsg.Value.ToByteArray());
                                    Log(string.Format(CultureInfo.CurrentCulture, "Del Result Msg : {0}", delResult));
                                    #endregion
                                }
                                #endregion
#endif
                                if (Target == GrpcSendMessageTargetEnum.ServerBroadcastCheckingClientAlive) {
                                    //Log(string.Format(CultureInfo.CurrentCulture, "Server 在檢查 Client 是否還活著.. 不需要處理 ::: {0}, UtcDateTime({1}), LocalDateTime({2})", ClientUUID, response.CurrentDateTime, response.CurrentDateTime.ToDateTime().ToLocalTime()));
                                } else {
                                    // 分類與 MBIServer 互動時的結果 ::: 1.initialize, 2.others
                                    if (response.IsRegistered) {    // 1. initialize : 初始成功登記在 Server 上的訊息
                                        Log(string.Format(CultureInfo.CurrentCulture, "Initial Registering...UUID({0}), ServerResponse :: {1}", ClientUUID, ServerMsg));
                                        IsRegisteredOnGrpcServer = true;
                                    } else {                        // 2. others : 之後的所有互動指令訊息
                                        if (Target == GrpcSendMessageTargetEnum.ServerBroadcastToAirManager) {
                                            #region 更新 Air Manager 的 dataref Value
                                            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' GET Server DataRef for '{1}', Cmd :: {2}", ClientUUID, CmdTargetSystemEnum.AirManager, ServerMsg));
                                            string dataref = response.WriteDataRefValueToAirManager.Dataref;
                                            string units = response.WriteDataRefValueToAirManager.Units;
                                            string description = response.WriteDataRefValueToAirManager.Description;
                                            object val = UtilByteString.FromByteArray<object>(response.WriteDataRefValueToAirManager.Val.Value.ToByteArray()); // 從 MBIServer 接收到寫給 AM 的 Value
                                            int offset = response.WriteDataRefValueToAirManager.Offset;
                                            FilterVal(Target, val, dataref, units, description, offset);
                                            #endregion
                                        } else if (Target == GrpcSendMessageTargetEnum.ServerBroadcastToXPlane) {
                                            #region 更新 X-Plane 的 dataref Value
                                            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' GET Server DataRef for '{1}', Cmd :: {2}", ClientUUID, CmdTargetSystemEnum.XPlane, ServerMsg));
                                            string dataref = response.WriteDataRefValueToXPlane.Dataref;
                                            string units = response.WriteDataRefValueToXPlane.Units;
                                            string description = response.WriteDataRefValueToXPlane.Description;
                                            object val = UtilByteString.FromByteArray<object>(response.WriteDataRefValueToXPlane.Val.Value.ToByteArray()); // 從 MBIServer 接收到寫給 XP 的 Value
                                            int offset = response.WriteDataRefValueToXPlane.Offset;
                                            FilterVal(Target, val, dataref, units, description, offset);
                                            #endregion
                                        } else if (Target == GrpcSendMessageTargetEnum.ClientToServer) {
                                            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' GET 'Self Message' From 'Server Response', Msg :: {1}", ClientUUID, ServerMsg));
                                        } else {
                                            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' GET ERROR Target '{1}', Msg :: {2}", ClientUUID, Target, ServerMsg));
                                        }
                                    }
                                }
                            }
                        } catch (Exception ex) { throw new Exception(string.Format(CultureInfo.CurrentCulture, "Task.Run() Grpc ResponseStream Exception Message :: {0}", ex.Message)); }
                    });
                    #endregion

                    #region Sending streaming Request, 初始啟動時, 主動發出註冊訊息給 MBIServer, 並持續監聽是否註冊成功 ? 註冊成功後, 就開始等待 user 輸入指令, 空白指令會 skip 掉
                    OrderRequest request = GetInitialRegisterInfo();
                    while (!request.CommandLine.Equals(UtilConstants.ExitPattern)) {
                        if (!string.IsNullOrEmpty(request.CommandLine))
                        { await sendData.RequestStream.WriteAsync(request); }

                        while (!IsRegisteredOnGrpcServer) {
                            // 尚未確認註冊至 MBIServer, 所以持續檢查是否已經接到 MBIServer Response Message
                            Log(string.Format(CultureInfo.CurrentCulture, "等待 {0} Response ? {1}", UtilGlobalFunc.GetMBIServerInfo, IsRegisteredOnGrpcServer));
                            SpinWait.SpinUntil(() => IsRegisteredOnGrpcServer, TimeSpan.FromMilliseconds(1000));
                            if (IsRegisteredOnGrpcServer) { Log(string.Format(CultureInfo.CurrentCulture, "{0} 註冊成功 :: {1}", UtilGlobalFunc.GetMBIServerInfo, IsRegisteredOnGrpcServer)); break; }
                            request = GetOrderRequest("Is Registered On GrpcServer ?", false);
                        }

                        if (IsRegisteredOnGrpcServer) {
                            // 確定已經註冊至 MBIServer 上, 接下來等待 user 下達訊息指令。 開始通訊互動, 就不是登記行為了, 所以一律為 false
                            string UserCommandLine = Console.ReadLine();
                            if (!string.IsNullOrEmpty(UserCommandLine))
                            { request = GetOrderRequest(UserCommandLine, false); }
                        } else { Log(string.Format(CultureInfo.CurrentCulture, "尚未註冊至 {0} :: {1}", UtilGlobalFunc.GetMBIServerInfo, IsRegisteredOnGrpcServer)); }
                    }
                    await sendData.RequestStream.CompleteAsync();
                    #endregion
                }

                ErrOccur = false;
            }
            catch (TaskCanceledException ex) { LogAndDispose(string.Format(CultureInfo.CurrentCulture, UtilConstants.GrpcTaskCanceledExceptionMsg, nameof(GrpcCommunicate), ex.Message, ex.StackTrace)); }
            catch (RpcException ex) {
                LogAndDispose(string.Format(CultureInfo.CurrentCulture, "{0} Exception Message :: {1}\nStackTrace :: {2}", nameof(GrpcCommunicate), ex.Message, ex.StackTrace));
                /*RpcExceptionModel rpcException = JsonUtil.GetInstanceFromJSON<RpcExceptionModel>(ex.Status.DebugException.Message);
                LogAndDispose(string.Format(CultureInfo.CurrentCulture, "\nStatus ::: {0}\nGrpcMessage ::: {1}\nDescription ::: {2}"
                    , rpcException.GrpcStatus.ToEnumName<StatusCode>(), rpcException.GrpcMessage, rpcException.Description));
                switch (rpcException.GrpcStatus.ToEnum<StatusCode>()) {
                    case StatusCode.Unknown: break;
                    case StatusCode.Aborted: break;
                    case StatusCode.AlreadyExists: break;
                    case StatusCode.Cancelled: break;
                    case StatusCode.DataLoss: break;
                    case StatusCode.DeadlineExceeded: break;
                    case StatusCode.FailedPrecondition: break;
                    case StatusCode.NotFound: break;
                    case StatusCode.Unavailable: break;
                    case StatusCode.Unimplemented: break;
                    default: break;
                }*/
            } catch (Exception ex) { LogAndDispose(string.Format(CultureInfo.CurrentCulture, "{0} Exception Message :: {1}\nStackTrace :: {2}", nameof(GrpcCommunicate), ex.Message, ex.StackTrace)); }

            if (ErrOccur) { await StopAsync(stoppingToken); } // Channel 發生錯誤, Stop && Restart self, keep going to Listen MBIServer
        }

        /// <summary>
        /// 依據 MBIServer 傳過來的 object val
        /// <para>1. 從 MBIServer 接收到寫給 AM 的 Value</para>
        /// <para>2. 從 MBIServer 接收到寫給 XP 的 Value</para>
        /// </summary>
        private void FilterVal(GrpcSendMessageTargetEnum Target, object value, string dataref, string units, string description, int offset = UtilConstants.NullOffset) {
            if (!InitParamsCtrller.IsSubscribedDR(dataref)) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefIsNotSubscribed, dataref)); return; }

            if (Target == GrpcSendMessageTargetEnum.ServerBroadcastToAirManager) {
                if (value.ValueIsString()) {
                    var sdr = InitParamsCtrller.GetStringDataRefElementFromInit(dataref); // new StringDataRefElement(UtilConstants.DefaultStringLength) { DataRef = dataref, Units = units, Type = DataRefs.TypeStart_string, Description = description };
                    if (sdr != null) {
                        sdr.RawStringValue = value.ConvertObjectToString();
                        sdr.CheckObjValFrRawStringValue();
                        NotifyChangedToAirManager(sdr, sdr.ObjVal2String);
                    }
                } else {
                    var dr = InitParamsCtrller.GetDataRefElementFromInit(dataref);//  new DataRefElement() { DataRef = dataref, Units = units, Type = GetTheType(value), Description = description };
                    if (dr != null) {
                        dr.ObjVal = WriteDataRefCtrller.AlignDataRefValue(dr.TypeIsArray, dr.Type, value);
                        if (dr.IsObjValArrayValue) {
                            NotifyChangedToAirManager(dr);
                        } else {
                            dr.RawByteValue = GetSendByteMessage(WriteDataRefCtrller.AlignDataRefValue(dr.TypeIsArray, dr.Type, value));
                            dr.CheckObjValFrRawByteValue();
                            NotifyChangedToAirManager(dr, dr.RawByteValue);
                        }
                    }
                }
            } else if (Target == GrpcSendMessageTargetEnum.ServerBroadcastToXPlane) {
                WriteDrCtrller.NotifyXPlaneValueChangedFromGRPC(triggerBySystem, nameof(GrpcCommunicate), dataref, value, offset);
            }
        }
        private string GetTheType(object value) {
            if (value.GetType() == typeof(int)) { return DataRefs.TypeStart_int; }
            else if (value.GetType() == typeof(byte)) { return DataRefs.TypeStart_byte; }
            else if (value.GetType() == typeof(float)) { return DataRefs.TypeStart_float; }
            else if (value.GetType() == typeof(double)) { return DataRefs.TypeStart_double; }
            else if (value.GetType() == typeof(int[])) { return GetArrayType(DataRefs.TypeStart_int, value.ConvertObjectToIntArray().Length); }
            else if (value.GetType() == typeof(byte[])) { return GetArrayType(DataRefs.TypeStart_byte, value.ConvertObjectToByteArray().Length); }
            else if (value.GetType() == typeof(float[])) { return GetArrayType(DataRefs.TypeStart_float, value.ConvertObjectToFloatArray().Length); }
            else if (value.GetType() == typeof(double[])) { return GetArrayType(DataRefs.TypeStart_double, value.ConvertObjectToDoubleArray().Length); }
            else { return DataRefs.TypeStart_float; }
        }
        private string GetArrayType(string type, int len) => string.Format(CultureInfo.CurrentCulture, UtilConstants.ArrayDataRefNameFormat, type, len);
        /// <summary>
        /// 依據 object 的 data type 轉換成適當的 byte[]
        /// </summary>
        private byte[] GetSendByteMessage(object o) {
            byte[] res = o.ValueIsIntArray() ? ((int[])o).ConvertIntArrayToByteArray()
                : o.ValueIsByteArray() ? ((byte[])o)
                : o.ValueIsFloatArray() ? ((float[])o).ConvertFloatArrayToByteArray()
                : o.ValueIsDoubleArray() ? ((double[])o).ConvertDoubleArrayToByteArray()
                : o.ValueIsInt() ? ((int)o).ConvertToByteArray()
                : o.ValueIsByte() ? new byte[] { (byte)o }
                : o.ValueIsFloat() ? ((float)o).ConvertToByteArray()
                : o.ValueIsDouble() ? ((double)o).ConvertToByteArray()
                : null;
            if (res == null) { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElement), DataRefs.ManuallyDefinitionType, o.GetType())); }
            return res;
        }

        /// <summary>
        /// 取得初始登記註冊的資訊, 通訊初始化, 先通知 MBIServer 進行登記行為
        /// </summary>
        private OrderRequest GetInitialRegisterInfo()
        { return GetOrderRequest(UtilConstants.IsRegistered, true); }
        /// <summary>
        /// 取得與 MBIServer 溝通的 Message Package
        /// </summary>
        /// <param name="commandLine">gRPC Client 下達給 MBIServer 的指令字串</param>
        /// <param name="isRegistered">是否為初始化階段, 進行登記行為???</param>
        private OrderRequest GetOrderRequest(string commandLine, bool isRegistered) {
            return new OrderRequest {
                UniqueKeyOfMachine = UtilConstants.UniqueKeyOfMachine,
                TimeTick = UtilGlobalFunc.GetOnLineTimeTick,
                CommandLine = commandLine,
                IsRegistered = isRegistered,
                Description = HostingName,
                SendMessageTarget = (int)GrpcSendMessageTargetEnum.ClientToServer,
                CurrentDateTime = Timestamp.FromDateTimeOffset(DateTimeOffset.Now),
                CurrentTimeSpan = Duration.FromTimeSpan(TimeSpan.FromMinutes(1))
            };
        }
        #endregion

        #region 通知 "Air Manager" DataRef value 有異動, Update Value 至 "Air Manager" 的 C++ Wrapper
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: byte[]
        /// </summary>
        protected void NotifyChangedToAirManager(DataRefElement dr, byte[] newValue) {
            if (dr == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4ByteArray, nameof(DataRefElement), dr)); return; }
            if (UtilGlobalFunc.IsArrayEmpty(newValue))
            { Log(string.Format(CultureInfo.CurrentCulture, "{0}, DataRef({1})", string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4ByteArray, "byte[]", newValue), dr.DataRef)); return; }
            dr.RawByteValue = newValue.DeepClone();
            dr.CheckObjValFrRawByteValue();
            WriteToAirManager(dr, new DataRefEventArgs($"({dr.DataRef})", dr, triggerBySystem));
        }
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: string
        /// </summary>
        protected void NotifyChangedToAirManager(StringDataRefElement sdr, string newValue) {
            if (sdr == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4String, nameof(StringDataRefElement), sdr)); return; }
            if (string.IsNullOrEmpty(newValue))
            { Log(string.Format(CultureInfo.CurrentCulture, "{0}, DataRef({1})", string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4String, "string", newValue), sdr.DataRef)); return; }
            sdr.RawStringValue = newValue.DeepClone();
            sdr.CheckObjValFrRawStringValue();
            WriteToAirManager(sdr, new DataRefEventArgs($"({sdr.DataRef})", sdr, triggerBySystem));
        }
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: '單一值' int, float, double, string
        /// <para>注意 !!! Array DataRef 不是從這邊觸發更新</para>
        /// </summary>
        private void WriteToAirManager(DataRefElementBase drBase, DataRefEventArgs e) {
            if (drBase == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, nameof(WriteToAirManager), nameof(DataRefElementBase), drBase)); return; }
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueIntoAM, triggerBySystem, UtilConstants.AirManagerSoftware, drBase.Type, drBase.DataRef, drBase.ObjValShowString));
            Commands.StaticEvtInterface.OnUpdateAirManagerDataRefSingleVal(this, e);
        }
        /// <summary>
        /// 主動/手動更新 "Air Manager" DataRef value :: Array值 int[], float[], double[]
        /// <para>1. X-Plane 背景自動更新, 自行由 BackgroundWorker 組裝執行完成, 因 X-Plane 分拆 array element[i] 回覆時間有時間差, 必須等待到齊後, 才能統一寫入 AM</para>
        /// <para>2. AirForceSystem / GRPC 手動更新, 自行控制 array element[i] 完整到齊後, 統一寫入 AM</para>
        /// <para>Array DataRef value updated 同步至 Air Manager 的 C++ Wrapper</para>
        /// </summary>
        private void NotifyChangedToAirManager(DataRefElementBase drBase) {
            if (drBase == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, "NotifyChangedToAirManagerArray", nameof(DataRefElementBase), drBase)); return; }
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueIntoAM, triggerBySystem, UtilConstants.AirManagerSoftware, drBase.Type, drBase.DataRef, drBase.ObjValShowString));
            Commands.StaticEvtInterface.OnManualUpdateAirManagerDataRefArrayVal(drBase, new DataRefEventArgs($"({drBase.DataRef})", drBase, drBase.ObjVal, triggerBySystem));
        }
        #endregion

    }
}
