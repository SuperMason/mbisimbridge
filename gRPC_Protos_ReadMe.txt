1. 修改 /GrpcProto/protos/*.proto 檔案後，必須手動執行 generate_protos.bat 才會更新 gRPC service 相關程式碼
	例如：
			CMD:::	D:\MasonWorkSpace\_BitBucket\FlightSimulation\MBISimBridge>generate_protos.bat
			
2. 產生的檔案路徑如下:
		資料結構 ::: /GrpcProto/GenCode/messages/
		gRPC服務 ::: /GrpcProto/GenCode/services/
