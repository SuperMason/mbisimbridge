﻿using AirManagerAPICallbackDefine;
using MBILibrary.Util;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace XPlaneConnector
{
    /// <summary>
    /// Each X-Plane Command 的明細內容
    /// </summary>
    public sealed class XPlaneCommand {
        #region Properties
        /// <summary>
        /// X-Plane 指令名稱
        /// <para>ex: "sim/operation/quit"</para>
        /// </summary>
        public string Name { get; set; }
        /*
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>0. Command To 'Air Manager' as ::: (sbyte*)name</para>
        /// <para>1. MbiAmAPI.AddCommandProvider(name, _callback)</para>
        /// <para>2. 把 Name 轉成 byte[] 再弄成 byte* ptr</para>
        /// </summary>
        public byte[] NameAsByteArray { get { return Name.ConvertToByteArray(); } }
        */
        /// <summary>
        /// X-Plane 指令敘述
        /// <para>ex: "Quit X-Plane."</para>
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Air Manager 帶過來的 User 自定義 value
        /// </summary>
        public int Value { get; set; }
        #endregion

        #region event for Command Execute
        /// <summary>
        /// <para>1. Command Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public ManagedCallback _callback = null;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public XPlaneCommand() {
            _callback = CommandCallback;
        }
        #endregion


        #region CallBack Function() for Command Execute
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        /// <param name="value">Air Manager 帶入的值</param>
        /// <param name="CommandName">Air Manager 帶入的 Command 字串名稱. ex ::: 'sim/operation/quit'</param>
        private void CommandCallback(int value, string CommandName) {
            Value = value;
            Commands.StaticEvtInterface.OnExecuteAirManagerCommand(this, new CommandEvevntArgs(Description, this));
        }
        #endregion
    }
}