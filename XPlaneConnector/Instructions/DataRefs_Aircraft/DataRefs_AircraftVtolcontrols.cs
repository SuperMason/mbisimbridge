﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement AircraftVtolcontrolsAcfVecteq
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_vectEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfVectarmz
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_vectarmZ", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfCyclicElev
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_cyclic_elev", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfCyclicAiln
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_cyclic_ailn", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfDelta3
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_delta3", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfPuffl
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_puffL", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfPuffm
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_puffM", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfPuffn
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_puffN", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfTailWithColl
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_tail_with_coll", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfDiffCollWithRoll
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_diff_coll_with_roll", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfDiffCollWithHdng
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_diff_coll_with_hdng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfDiffCyclWithHdngLon
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_diff_cycl_with_hdng_lon", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftVtolcontrolsAcfAutoRpmWithTvec
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_auto_rpm_with_tvec", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// max rotor trim aft when stick fully forward
        /// </summary>
        public static DataRefElement AircraftVtolcontrolsAcfRotorTrimMaxFwd
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_rotor_trim_max_fwd", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "max rotor trim aft when stick fully forward"); } }
        /// <summary>
        /// max rotor trim aft when stick fully aft
        /// </summary>
        public static DataRefElement AircraftVtolcontrolsAcfRotorTrimMaxAft
        { get { return GetDataRefElement("sim/aircraft/vtolcontrols/acf_rotor_trim_max_aft", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "max rotor trim aft when stick fully aft"); } }
    }
}