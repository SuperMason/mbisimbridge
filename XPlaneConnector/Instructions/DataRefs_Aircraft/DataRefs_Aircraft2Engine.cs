﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// This is the idle ratio for the engines at low idle.  1.0 provides default behavior; this can be any number greater than zero.
        /// </summary>
        public static DataRefElement Aircraft2EngineLowIdleRatio
        { get { return GetDataRefElement("sim/aircraft2/engine/low_idle_ratio", TypeStart_float, UtilConstants.Flag_Yes, "multiplier", "This is the idle ratio for the engines at low idle.  1.0 provides default behavior; this can be any number greater than zero."); } }
        /// <summary>
        /// This is the idle ratio for the engines at high idle. 1.0 provides default behavior; this can be any number greater than zero.
        /// </summary>
        public static DataRefElement Aircraft2EngineHighIdleRatio
        { get { return GetDataRefElement("sim/aircraft2/engine/high_idle_ratio", TypeStart_float, UtilConstants.Flag_Yes, "multiplier", "This is the idle ratio for the engines at high idle. 1.0 provides default behavior; this can be any number greater than zero."); } }
        /// <summary>
        /// This is the maximum power output of the engine at sea level.  Note that if the engine has a waste gate this is the same as max power at critical altitude.
        /// </summary>
        public static DataRefElement Aircraft2EngineMaxPowerLimitedWatts
        { get { return GetDataRefElement("sim/aircraft2/engine/max_power_limited_watts", TypeStart_float, UtilConstants.Flag_Yes, "watts", "This is the maximum power output of the engine at sea level.  Note that if the engine has a waste gate this is the same as max power at critical altitude."); } }
        /// <summary>
        /// Time for flaps to go from full retraction to full extension
        /// </summary>
        public static DataRefElement Aircraft2EngineFlapExtensionTimeSec
        { get { return GetDataRefElement("sim/aircraft2/engine/flap_extension_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "second", "Time for flaps to go from full retraction to full extension"); } }
        /// <summary>
        /// Time for flaps to go from full extension to full retraction
        /// </summary>
        public static DataRefElement Aircraft2EngineFlapRetractionTimeSec
        { get { return GetDataRefElement("sim/aircraft2/engine/flap_retraction_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "second", "Time for flaps to go from full extension to full retraction"); } }
        /// <summary>
        /// dirtiness of built-in exhaust effects, 1.0 is default, positive numbers are more dirty
        /// </summary>
        public static DataRefElement Aircraft2EngineExhaustDirtinessRatio
        { get { return GetDataRefElement("sim/aircraft2/engine/exhaust_dirtiness_ratio", TypeStart_float, UtilConstants.Flag_No, "ratio", "dirtiness of built-in exhaust effects, 1.0 is default, positive numbers are more dirty"); } }
    }
}