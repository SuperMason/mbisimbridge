﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// The number of batteries on this plane
        /// </summary>
        public static DataRefElement AircraftElectricalNumBatteries
        { get { return GetDataRefElement("sim/aircraft/electrical/num_batteries", TypeStart_int, UtilConstants.Flag_No, "count", "The number of batteries on this plane"); } }
        /// <summary>
        /// The number of generators on this plane
        /// </summary>
        public static DataRefElement AircraftElectricalNumGenerators
        { get { return GetDataRefElement("sim/aircraft/electrical/num_generators", TypeStart_int, UtilConstants.Flag_No, "count", "The number of generators on this plane"); } }
        /// <summary>
        /// The number of inverters on this plane
        /// </summary>
        public static DataRefElement AircraftElectricalNumInverters
        { get { return GetDataRefElement("sim/aircraft/electrical/num_inverters", TypeStart_int, UtilConstants.Flag_No, "count", "The number of inverters on this plane"); } }
        /// <summary>
        /// The number of busses on this plane
        /// </summary>
        public static DataRefElement AircraftElectricalNumBuses
        { get { return GetDataRefElement("sim/aircraft/electrical/num_buses", TypeStart_int, UtilConstants.Flag_No, "count", "The number of busses on this plane"); } }
    }
}