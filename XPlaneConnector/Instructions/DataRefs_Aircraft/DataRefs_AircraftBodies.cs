﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// cd for fuselage
        /// </summary>
        public static DataRefElement AircraftBodiesAcfFuseCd
        { get { return GetDataRefElement("sim/aircraft/bodies/acf_fuse_cd", TypeStart_float, UtilConstants.Flag_Yes, "???", "cd for fuselage"); } }
        /// <summary>
        /// [PART] cd for all parts
        /// </summary>
        public static DataRefElement AircraftBodiesAcfFuseCdArray
        { get { return GetDataRefElement("sim/aircraft/bodies/acf_fuse_cd_array", TypeStart_float95, UtilConstants.Flag_Yes, "???", "[PART] cd for all parts"); } }
    }
}