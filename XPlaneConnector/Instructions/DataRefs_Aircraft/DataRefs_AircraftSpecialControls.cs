﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement AircraftSpecialcontrolsAcfJatoTheta
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_theta", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfJatoThrust
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_thrust", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfJatoDur
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_dur", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfJatoSfc
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_sfc", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfJatoY
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfJatoZ
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_jato_Z", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfChuteArea
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_chute_area", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfChuteY
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_chute_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfChuteZ
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_chute_Z", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAil1pitch
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_ail1pitch", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAil1flaps
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_ail1flaps", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAil2pitch
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_ail2pitch", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAil2flaps
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_ail2flaps", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfStabroll
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_stabroll", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfStabhdng
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_stabhdng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfTvecPtch
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_tvec_ptch", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfTvecRoll
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_tvec_roll", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfTvecHdng
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_tvec_hdng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfDiffThroWithHdng
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_diff_thro_with_hdng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfTksCapLiter
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_tks_cap_liter", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfWarn1eq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_warn1EQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfGearhorneq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_gearhornEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAutosbrkeq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_autosbrkEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAutofbrkeq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_autofbrkEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAutosweepeq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_autosweepEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftSpecialcontrolsAcfAutoslateq
        { get { return GetDataRefElement("sim/aircraft/specialcontrols/acf_autoslatEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
    }
}
