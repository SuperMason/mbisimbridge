﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// engine type - read only in v11, but you should NEVER EVER write this in v10 or earlier.  0=recip carb, 1=recip injected, 2=free turbine, 3=electric, 4=lo bypass jet, 5=hi bypass jet, 6=rocket, 7=tip rockets, 8=fixed turbine
        /// </summary>
        public static DataRefElement AircraftPropAcfEnType
        { get { return GetDataRefElement("sim/aircraft/prop/acf_en_type", TypeStart_int8, UtilConstants.Flag_No, "enum", "engine type - read only in v11, but you should NEVER EVER write this in v10 or earlier.  0=recip carb, 1=recip injected, 2=free turbine, 3=electric, 4=lo bypass jet, 5=hi bypass jet, 6=rocket, 7=tip rockets, 8=fixed turbine"); } }
        /// <summary>
        /// better organization to do it this way, NOTE : Used to be i8 in version 6
        /// </summary>
        public static DataRefElement AircraftPropAcfRevthrustEq
        { get { return GetDataRefElement("sim/aircraft/prop/acf_revthrust_eq", TypeStart_int, UtilConstants.Flag_Yes, "???", "better organization to do it this way, NOTE : Used to be i8 in version 6"); } }
        /// <summary>
        /// prop type
        /// </summary>
        public static DataRefElement AircraftPropAcfPropType
        { get { return GetDataRefElement("sim/aircraft/prop/acf_prop_type", TypeStart_int8, UtilConstants.Flag_Yes, "???", "prop type"); } }
        /// <summary>
        /// prop to engine or common power source
        /// </summary>
        public static DataRefElement AircraftPropAcfPropGearRat
        { get { return GetDataRefElement("sim/aircraft/prop/acf_prop_gear_rat", TypeStart_float8, UtilConstants.Flag_Yes, "???", "prop to engine or common power source"); } }
        /// <summary>
        /// 1.0=CW, -1.0=CCW, float so we can multiply effects by floats - made NOT writable in 11.10.  YOU SHOULD NEVER WRITE THIS IN ANY VERSION OF X-PLANE.
        /// </summary>
        public static DataRefElement AircraftPropAcfPropDir
        { get { return GetDataRefElement("sim/aircraft/prop/acf_prop_dir", TypeStart_float8, UtilConstants.Flag_No, "???", "1.0=CW, -1.0=CCW, float so we can multiply effects by floats - made NOT writable in 11.10.  YOU SHOULD NEVER WRITE THIS IN ANY VERSION OF X-PLANE."); } }
        /// <summary>
        /// float so we can multiply effects by floats
        /// </summary>
        public static DataRefElement AircraftPropAcfNumBlades
        { get { return GetDataRefElement("sim/aircraft/prop/acf_num_blades", TypeStart_float8, UtilConstants.Flag_Yes, "???", "float so we can multiply effects by floats"); } }
        /// <summary>
        /// by governor
        /// </summary>
        public static DataRefElement AircraftPropAcfMinPitch
        { get { return GetDataRefElement("sim/aircraft/prop/acf_min_pitch", TypeStart_float8, UtilConstants.Flag_Yes, "???", "by governor"); } }
        /// <summary>
        /// by governor
        /// </summary>
        public static DataRefElement AircraftPropAcfMaxPitch
        { get { return GetDataRefElement("sim/aircraft/prop/acf_max_pitch", TypeStart_float8, UtilConstants.Flag_Yes, "???", "by governor"); } }
        /// <summary>
        /// in reverse
        /// </summary>
        public static DataRefElement AircraftPropAcfReversedPitch
        { get { return GetDataRefElement("sim/aircraft/prop/acf_reversed_pitch", TypeStart_float, UtilConstants.Flag_Yes, "???", "in reverse"); } }
        /// <summary>
        /// this is physical geometry
        /// </summary>
        public static DataRefElement AircraftPropAcfSidecant
        { get { return GetDataRefElement("sim/aircraft/prop/acf_sidecant", TypeStart_float8, UtilConstants.Flag_Yes, "???", "this is physical geometry"); } }
        /// <summary>
        /// built into planes & helos and changed with thrust vector
        /// </summary>
        public static DataRefElement AircraftPropAcfVertcant
        { get { return GetDataRefElement("sim/aircraft/prop/acf_vertcant", TypeStart_float8, UtilConstants.Flag_Yes, "???", "built into planes & helos and changed with thrust vector"); } }
        /// <summary>
        /// mass of prop
        /// </summary>
        public static DataRefElement AircraftPropAcfPropMass
        { get { return GetDataRefElement("sim/aircraft/prop/acf_prop_mass", TypeStart_float8, UtilConstants.Flag_Yes, "???", "mass of prop"); } }
        /// <summary>
        /// MI for changing prop RPM
        /// </summary>
        public static DataRefElement AircraftPropAcfMipropRpm
        { get { return GetDataRefElement("sim/aircraft/prop/acf_miprop_rpm", TypeStart_float8, UtilConstants.Flag_Yes, "???", "MI for changing prop RPM"); } }
        /// <summary>
        /// for total propwash
        /// </summary>
        public static DataRefElement AircraftPropAcfDiscarea
        { get { return GetDataRefElement("sim/aircraft/prop/acf_discarea", TypeStart_float8, UtilConstants.Flag_Yes, "???", "for total propwash"); } }
        /// <summary>
        /// area each ring of prop
        /// </summary>
        public static DataRefElement AircraftPropAcfRingarea
        { get { return GetDataRefElement("sim/aircraft/prop/acf_ringarea", TypeStart_float8_10, UtilConstants.Flag_Yes, "???", "area each ring of prop"); } }
        /// <summary>
        /// design point
        /// </summary>
        public static DataRefElement AircraftPropAcfDesRpmPrp
        { get { return GetDataRefElement("sim/aircraft/prop/acf_des_rpm_prp", TypeStart_float8, UtilConstants.Flag_Yes, "???", "design point"); } }
        /// <summary>
        /// design point
        /// </summary>
        public static DataRefElement AircraftPropAcfDesKtsAcf
        { get { return GetDataRefElement("sim/aircraft/prop/acf_des_kts_acf", TypeStart_float8, UtilConstants.Flag_Yes, "???", "design point"); } }
        /// <summary>
        /// [PART]
        /// </summary>
        public static DataRefElement AircraftPropAcfPartEq
        { get { return GetDataRefElement("sim/aircraft/prop/acf_part_eq", TypeStart_int95, UtilConstants.Flag_No, "???", "[PART]"); } }
    }
}