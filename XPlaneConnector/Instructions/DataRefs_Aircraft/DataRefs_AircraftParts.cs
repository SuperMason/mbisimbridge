﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// array of airfoil names, per part
        /// </summary>
        public static DataRefElement AircraftPartsAcfRafl0
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Rafl0", TypeStart_byte2920, UtilConstants.Flag_Yes, "string[40]", "array of airfoil names, per part"); } }
        /// <summary>
        /// file, not path
        /// </summary>
        public static DataRefElement AircraftPartsAcfRafl1
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Rafl1", TypeStart_byte2920, UtilConstants.Flag_Yes, "string[40]", "file, not path"); } }
        /// <summary>
        /// file, not path
        /// </summary>
        public static DataRefElement AircraftPartsAcfTafl0
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Tafl0", TypeStart_byte2920, UtilConstants.Flag_Yes, "string[40]", "file, not path"); } }
        /// <summary>
        /// file, not path
        /// </summary>
        public static DataRefElement AircraftPartsAcfTafl1
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Tafl1", TypeStart_byte2920, UtilConstants.Flag_Yes, "string[40]", "file, not path"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfEls
        { get { return GetDataRefElement("sim/aircraft/parts/acf_els", TypeStart_int56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [GEAR]
        /// </summary>
        public static DataRefElement AircraftPartsAcfXarm
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Xarm", TypeStart_float10, UtilConstants.Flag_Yes, "???", "[GEAR]"); } }
        /// <summary>
        /// [GEAR]
        /// </summary>
        public static DataRefElement AircraftPartsAcfYarm
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Yarm", TypeStart_float10, UtilConstants.Flag_Yes, "???", "[GEAR]"); } }
        /// <summary>
        /// [GEAR]
        /// </summary>
        public static DataRefElement AircraftPartsAcfZarm
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Zarm", TypeStart_float10, UtilConstants.Flag_Yes, "???", "[GEAR]"); } }
        /// <summary>
        /// [PART] body aero center offset from it's reference
        /// </summary>
        public static DataRefElement AircraftPartsAcfXBodyAero
        { get { return GetDataRefElement("sim/aircraft/parts/acf_X_body_aero", TypeStart_float95, UtilConstants.Flag_Yes, "???", "[PART] body aero center offset from it's reference"); } }
        /// <summary>
        /// [PART] used for force build-up
        /// </summary>
        public static DataRefElement AircraftPartsAcfYBodyAero
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Y_body_aero", TypeStart_float95, UtilConstants.Flag_Yes, "???", "[PART] used for force build-up"); } }
        /// <summary>
        /// [PART]
        /// </summary>
        public static DataRefElement AircraftPartsAcfZBodyAero
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Z_body_aero", TypeStart_float95, UtilConstants.Flag_Yes, "???", "[PART]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfCroot
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Croot", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfCtip
        { get { return GetDataRefElement("sim/aircraft/parts/acf_Ctip", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfDihed1
        { get { return GetDataRefElement("sim/aircraft/parts/acf_dihed1", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfSweep1
        { get { return GetDataRefElement("sim/aircraft/parts/acf_sweep1", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfSweep2
        { get { return GetDataRefElement("sim/aircraft/parts/acf_sweep2", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING] semilen this segment only
        /// </summary>
        public static DataRefElement AircraftPartsAcfSemilenSEG
        { get { return GetDataRefElement("sim/aircraft/parts/acf_semilen_SEG", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING] semilen this segment only"); } }
        /// <summary>
        /// [WING] semilen of the JOINED wing segments, all JOINED SEGMENTS, for AR and CDi and ground effect, etc.
        /// </summary>
        public static DataRefElement AircraftPartsAcfSemilenJND
        { get { return GetDataRefElement("sim/aircraft/parts/acf_semilen_JND", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING] semilen of the JOINED wing segments, all JOINED SEGMENTS, for AR and CDi and ground effect, etc."); } }
        /// <summary>
        /// [WING] Oswald's E
        /// </summary>
        public static DataRefElement AircraftPartsAcfE
        { get { return GetDataRefElement("sim/aircraft/parts/acf_e", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING] Oswald's E"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfAR
        { get { return GetDataRefElement("sim/aircraft/parts/acf_AR", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement AircraftPartsAcfDeltaFac
        { get { return GetDataRefElement("sim/aircraft/parts/acf_delta_fac", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING]"); } }
        /// <summary>
        /// s for each element for foils, and FRONT, SIDE, TOP for BODIES.
        /// </summary>
        public static DataRefElement AircraftPartsAcfS
        { get { return GetDataRefElement("sim/aircraft/parts/acf_s", TypeStart_float730, UtilConstants.Flag_Yes, "???", "s for each element for foils, and FRONT, SIDE, TOP for BODIES."); } }
        public static DataRefElement AircraftPartsAcfMac
        { get { return GetDataRefElement("sim/aircraft/parts/acf_mac", TypeStart_float730, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfAnginc
        { get { return GetDataRefElement("sim/aircraft/parts/acf_anginc", TypeStart_float73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfFlapeq
        { get { return GetDataRefElement("sim/aircraft/parts/acf_flapEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfSlateq
        { get { return GetDataRefElement("sim/aircraft/parts/acf_slatEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfSbrkeq
        { get { return GetDataRefElement("sim/aircraft/parts/acf_sbrkEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfAil1
        { get { return GetDataRefElement("sim/aircraft/parts/acf_ail1", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfAil2
        { get { return GetDataRefElement("sim/aircraft/parts/acf_ail2", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfSplr
        { get { return GetDataRefElement("sim/aircraft/parts/acf_splr", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfFlap
        { get { return GetDataRefElement("sim/aircraft/parts/acf_flap", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfFlap2
        { get { return GetDataRefElement("sim/aircraft/parts/acf_flap2", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfSlat
        { get { return GetDataRefElement("sim/aircraft/parts/acf_slat", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfSbrk
        { get { return GetDataRefElement("sim/aircraft/parts/acf_sbrk", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfDrud
        { get { return GetDataRefElement("sim/aircraft/parts/acf_drud", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfElev
        { get { return GetDataRefElement("sim/aircraft/parts/acf_elev", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfRudd
        { get { return GetDataRefElement("sim/aircraft/parts/acf_rudd", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfRudd2
        { get { return GetDataRefElement("sim/aircraft/parts/acf_rudd2", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// [PART] Radius of part
        /// </summary>
        public static DataRefElement AircraftPartsAcfBodyR
        { get { return GetDataRefElement("sim/aircraft/parts/acf_body_r", TypeStart_float95, UtilConstants.Flag_Yes, "meters", "[PART] Radius of part"); } }
        public static DataRefElement AircraftPartsAcfGearType
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_type", TypeStart_int10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfGearLate
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_latE", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfGearLone
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_lonE", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// extended
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearAxie
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_axiE", TypeStart_float10, UtilConstants.Flag_Yes, "???", "extended"); } }
        public static DataRefElement AircraftPartsAcfGearLatr
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_latR", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfGearLonr
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_lonR", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// retracted
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearAxir
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_axiR", TypeStart_float10, UtilConstants.Flag_Yes, "???", "retracted"); } }
        public static DataRefElement AircraftPartsAcfGearLatn
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_latN", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftPartsAcfGearLonn
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_lonN", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// now
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearAxin
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_axiN", TypeStart_float10, UtilConstants.Flag_Yes, "???", "now"); } }
        /// <summary>
        /// gear param
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearLeglen
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_leglen", TypeStart_float10, UtilConstants.Flag_Yes, "???", "gear param"); } }
        /// <summary>
        /// gear param
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearTirrad
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_tirrad", TypeStart_float10, UtilConstants.Flag_Yes, "???", "gear param"); } }
        /// <summary>
        /// gear param
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearcon
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gearcon", TypeStart_float10, UtilConstants.Flag_Yes, "???", "gear param"); } }
        /// <summary>
        /// gear param
        /// </summary>
        public static DataRefElement AircraftPartsAcfGeardmp
        { get { return GetDataRefElement("sim/aircraft/parts/acf_geardmp", TypeStart_float10, UtilConstants.Flag_Yes, "???", "gear param"); } }
        /// <summary>
        /// static deflection... the gear TIRE LOCATION IS OFFSET DOWN BY THIS MUCH IN X-PLANE since people ALWAYS enter gear location UNDER STATIC DEFLECTION!
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearstatdef
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gearstatdef", TypeStart_float10, UtilConstants.Flag_Yes, "???", "static deflection... the gear TIRE LOCATION IS OFFSET DOWN BY THIS MUCH IN X-PLANE since people ALWAYS enter gear location UNDER STATIC DEFLECTION!"); } }
        /// <summary>
        /// landing gear deployment, 0.0->1.0
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearDeploy
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "???", "landing gear deployment, 0.0->1.0"); } }
        /// <summary>
        /// x location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised.
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearXnodef
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_xnodef", TypeStart_float10, UtilConstants.Flag_Yes, "meters", "x location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised."); } }
        /// <summary>
        /// y location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised.
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearYnodef
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_ynodef", TypeStart_float10, UtilConstants.Flag_Yes, "meters", "y location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised."); } }
        /// <summary>
        /// z location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised.
        /// </summary>
        public static DataRefElement AircraftPartsAcfGearZnodef
        { get { return GetDataRefElement("sim/aircraft/parts/acf_gear_znodef", TypeStart_float10, UtilConstants.Flag_Yes, "meters", "z location of the Nth gear's attach point relative to the CG, airplane coordinates.  This does not change as gear is raised."); } }
    }
}
