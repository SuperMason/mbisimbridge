﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement aircraftForcefeedbackAcfFfHydraulic = null;
        /// <summary>
        /// this test dataref is used internally for testing our models
        /// </summary>
        public static DataRefElement AircraftForcefeedbackAcfFfHydraulic {
            get {
                if (aircraftForcefeedbackAcfFfHydraulic == null)
                { aircraftForcefeedbackAcfFfHydraulic = GetDataRefElement("sim/aircraft/forcefeedback/acf_ff_hydraulic", TypeStart_int, UtilConstants.Flag_Yes, "", ""); }
                return aircraftForcefeedbackAcfFfHydraulic;
            }
        }

        private static DataRefElement aircraftForcefeedbackAcfFfStickshaker = null;
        /// <summary>
        /// this test dataref is used internally for testing our models
        /// </summary>
        public static DataRefElement AircraftForcefeedbackAcfFfStickshaker {
            get {
                if (aircraftForcefeedbackAcfFfStickshaker == null)
                { aircraftForcefeedbackAcfFfStickshaker = GetDataRefElement("sim/aircraft/forcefeedback/acf_ff_stickshaker", TypeStart_int, UtilConstants.Flag_Yes, "", ""); }
                return aircraftForcefeedbackAcfFfStickshaker;
            }
        }

    }
}