﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement AircraftControlsAcfAil1Crat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail1_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfAil1Up
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail1_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfAil1Dn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail1_dn", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Minimum prop speed with governor on, radians/second
        /// </summary>
        public static DataRefElement AircraftControlsAcfRSCMingovPrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_RSC_mingov_prp", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Minimum prop speed with governor on, radians/second"); } }
        /// <summary>
        /// Prop idle speed radians/second
        /// </summary>
        public static DataRefElement AircraftControlsAcfRSCIdlespeedPrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_RSC_idlespeed_prp", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Prop idle speed radians/second"); } }
        /// <summary>
        /// Max prop speed radians/second
        /// </summary>
        public static DataRefElement AircraftControlsAcfRSCRedlinePrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_RSC_redline_prp", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Max prop speed radians/second"); } }
        public static DataRefElement AircraftControlsAcfAil2Crat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail2_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfAil2Up
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail2_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfAil2Dn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ail2_dn", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRSCMingreenPrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_RSC_mingreen_prp", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRSCMaxgreenPrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_RSC_maxgreen_prp", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfElevCrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfElevUp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfElevDn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_dn", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfTrqMaxEng
        { get { return GetDataRefElement("sim/aircraft/controls/acf_trq_max_eng", TypeStart_float, UtilConstants.Flag_Yes, "newtonmeters", ""); } }
        /// <summary>
        /// NOTE : This is now the same as acf_trq_max_en in v7
        /// </summary>
        public static DataRefElement AircraftControlsAcfTrqMaxPrp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_trq_max_prp", TypeStart_float, UtilConstants.Flag_Yes, "newtonmeters", "NOTE : This is now the same as acf_trq_max_en in v7"); } }
        public static DataRefElement AircraftControlsAcfRuddCrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRuddLr
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_lr", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRuddRr
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_rr", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRud2Crat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rud2_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRud2Lr
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rud2_lr", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfRud2Rr
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rud2_rr", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSplrCrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_splr_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSplrUp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_splr_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSbrkCrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_sbrk_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSbrk2Crat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_sbrk2_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSbrkUp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_sbrk_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSbrk2Up
        { get { return GetDataRefElement("sim/aircraft/controls/acf_sbrk2_up", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapCrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Crat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_crat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapDn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_dn", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Dn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_dn", TypeStart_float10, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// This is the maximum degrees deflection up for a horizontal stabilizer that moves during trim
        /// </summary>
        public static DataRefElement AircraftControlsAcfHstbTrimUp
        { get { return GetDataRefElement("sim/aircraft/controls/acf_hstb_trim_up", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "This is the maximum degrees deflection up for a horizontal stabilizer that moves during trim"); } }
        /// <summary>
        /// This is the maximum degrees deflection down for a horizontal stabilizer that moves during trim
        /// </summary>
        public static DataRefElement AircraftControlsAcfHstbTrimDn
        { get { return GetDataRefElement("sim/aircraft/controls/acf_hstb_trim_dn", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "This is the maximum degrees deflection down for a horizontal stabilizer that moves during trim"); } }
        public static DataRefElement AircraftControlsAcfFlapType
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_type", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Type
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_type", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapCl
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_cl", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapCd
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_cd", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapCm
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_cm", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Cl
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_cl", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Cd
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_cd", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlap2Cm
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap2_cm", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapDetents
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_detents", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfFlapDeftime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_flap_deftime", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfSlatInc
        { get { return GetDataRefElement("sim/aircraft/controls/acf_slat_inc", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftControlsAcfBlownFlapMinEngag
        { get { return GetDataRefElement("sim/aircraft/controls/acf_blown_flap_min_engag", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// This is the trim position for takeoff expressed as a ratio, 1 = max up trim, -1 = max down trim.
        /// </summary>
        public static DataRefElement AircraftControlsAcfTakeoffTrim
        { get { return GetDataRefElement("sim/aircraft/controls/acf_takeoff_trim", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "This is the trim position for takeoff expressed as a ratio, 1 = max up trim, -1 = max down trim."); } }
        /// <summary>
        /// Maximum nose-down trim, expressed as a ratio of maximum nose-down elevator deflection
        /// </summary>
        public static DataRefElement AircraftControlsAcfMinTrimElev
        { get { return GetDataRefElement("sim/aircraft/controls/acf_min_trim_elev", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum nose-down trim, expressed as a ratio of maximum nose-down elevator deflection"); } }
        /// <summary>
        /// Maximum nose-up trim, expressed as a ratio of maximum nose-up elevator deflection
        /// </summary>
        public static DataRefElement AircraftControlsAcfMaxTrimElev
        { get { return GetDataRefElement("sim/aircraft/controls/acf_max_trim_elev", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum nose-up trim, expressed as a ratio of maximum nose-up elevator deflection"); } }
        /// <summary>
        /// This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast.
        /// </summary>
        public static DataRefElement AircraftControlsAcfElevTrimSpeedrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_trim_speedrat", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast."); } }
        /// <summary>
        /// This is the amount of elevator deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs.
        /// </summary>
        public static DataRefElement AircraftControlsAcfElevTab
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_tab", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "This is the amount of elevator deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs."); } }
        /// <summary>
        /// Maximum aileron downward trim, expressed as a ratio of maximum aileron downward travel
        /// </summary>
        public static DataRefElement AircraftControlsAcfMinTrimAiln
        { get { return GetDataRefElement("sim/aircraft/controls/acf_min_trim_ailn", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum aileron downward trim, expressed as a ratio of maximum aileron downward travel"); } }
        /// <summary>
        /// Maximum aileron upward trim, expressed as a ratio of maximum aileron upward travel
        /// </summary>
        public static DataRefElement AircraftControlsAcfMaxTrimAiln
        { get { return GetDataRefElement("sim/aircraft/controls/acf_max_trim_ailn", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum aileron upward trim, expressed as a ratio of maximum aileron upward travel"); } }
        /// <summary>
        /// This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast.
        /// </summary>
        public static DataRefElement AircraftControlsAcfAilnTrimSpeedrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ailn_trim_speedrat", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast."); } }
        /// <summary>
        /// This is the amount of aileron deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs.
        /// </summary>
        public static DataRefElement AircraftControlsAcfAilnTab
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ailn_tab", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "This is the amount of aileron deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs."); } }
        /// <summary>
        /// Maximum rudder left trim, expressed as a ratio of maximum rudder left travel
        /// </summary>
        public static DataRefElement AircraftControlsAcfMinTrimRudd
        { get { return GetDataRefElement("sim/aircraft/controls/acf_min_trim_rudd", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum rudder left trim, expressed as a ratio of maximum rudder left travel"); } }
        /// <summary>
        /// Maximum rudder right trim, expressed as a ratio of maximum rudder right travel
        /// </summary>
        public static DataRefElement AircraftControlsAcfMaxTrimRudd
        { get { return GetDataRefElement("sim/aircraft/controls/acf_max_trim_rudd", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Maximum rudder right trim, expressed as a ratio of maximum rudder right travel"); } }
        /// <summary>
        /// This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast.
        /// </summary>
        public static DataRefElement AircraftControlsAcfRuddTrimSpeedrat
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_trim_speedrat", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the speed of trim time, expressed as a ratio, where 1.0 means it takes 20 seconds to fully move trim from one extreme to the other.  2.0 means trim is twice as fast."); } }
        /// <summary>
        /// This is the amount of rudder deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs.
        /// </summary>
        public static DataRefElement AircraftControlsAcfRuddTab
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_tab", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "This is the amount of rudder deflection (as a ratio of max) induced by the aerodynamic effect of static trim tabs."); } }
        /// <summary>
        /// Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected.
        /// </summary>
        public static DataRefElement AircraftControlsAcfElevDefTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_def_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected."); } }
        /// <summary>
        /// Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected.
        /// </summary>
        public static DataRefElement AircraftControlsAcfAilnDefTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ailn_def_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected."); } }
        /// <summary>
        /// Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected.
        /// </summary>
        public static DataRefElement AircraftControlsAcfRuddDefTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_def_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "Enter 0.0 to be able to deflect the controls as fast as the pilot can move the stick or the art stab system can command a deflection. If the plane has a hydraulic system and a max rate of control deflection, though, enter how long it takes to go from center to fully-deflected."); } }
        /// <summary>
        /// This is the total time taken for the elevator trim to go from one extreme to the other.
        /// </summary>
        public static DataRefElement AircraftControlsAcfElevTrimTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_elev_trim_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "This is the total time taken for the elevator trim to go from one extreme to the other."); } }
        /// <summary>
        /// This is the total time taken for the aileron trim to go from one extreme to the other.
        /// </summary>
        public static DataRefElement AircraftControlsAcfAilnTrimTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_ailn_trim_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "This is the total time taken for the aileron trim to go from one extreme to the other."); } }
        /// <summary>
        /// This is the total time taken for the rudder trim to go from one extreme to the other.
        /// </summary>
        public static DataRefElement AircraftControlsAcfRuddTrimTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_rudd_trim_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "This is the total time taken for the rudder trim to go from one extreme to the other."); } }
        /// <summary>
        /// Speedbrake time to extend.
        /// </summary>
        public static DataRefElement AircraftControlsAcfSpeedbrakeExtTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_speedbrake_ext_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "Speedbrake time to extend."); } }
        /// <summary>
        /// Speedbrake time to retract.
        /// </summary>
        public static DataRefElement AircraftControlsAcfSpeedbrakeRetTime
        { get { return GetDataRefElement("sim/aircraft/controls/acf_speedbrake_ret_time", TypeStart_float, UtilConstants.Flag_Yes, "secs", "Speedbrake time to retract."); } }
    }
}
