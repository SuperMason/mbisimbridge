﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// This is the ORIGINAL reference point in PM in _feet_.
        /// </summary>
        public static DataRefElement AircraftWeightAcfCgYOriginal
        { get { return GetDataRefElement("sim/aircraft/weight/acf_cgY_original", TypeStart_float, UtilConstants.Flag_No, "feet", "This is the ORIGINAL reference point in PM in _feet_."); } }
        /// <summary>
        /// This is the ORIGINAL reference point in PM in _feet_.
        /// </summary>
        public static DataRefElement AircraftWeightAcfCgZOriginal
        { get { return GetDataRefElement("sim/aircraft/weight/acf_cgZ_original", TypeStart_float, UtilConstants.Flag_No, "feet", "This is the ORIGINAL reference point in PM in _feet_."); } }
        /// <summary>
        /// Moment of inertia per kg for the aircraft as a whole
        /// </summary>
        public static DataRefElement AircraftWeightAcfJxxUnitmass
        { get { return GetDataRefElement("sim/aircraft/weight/acf_Jxx_unitmass", TypeStart_float, UtilConstants.Flag_Yes, "meters^2", "Moment of inertia per kg for the aircraft as a whole"); } }
        /// <summary>
        /// Moment of inertia per kg for the aircraft as a whole
        /// </summary>
        public static DataRefElement AircraftWeightAcfJyyUnitmass
        { get { return GetDataRefElement("sim/aircraft/weight/acf_Jyy_unitmass", TypeStart_float, UtilConstants.Flag_Yes, "meters^2", "Moment of inertia per kg for the aircraft as a whole"); } }
        /// <summary>
        /// Moment of inertia per kg for the aircraft as a whole
        /// </summary>
        public static DataRefElement AircraftWeightAcfJzzUnitmass
        { get { return GetDataRefElement("sim/aircraft/weight/acf_Jzz_unitmass", TypeStart_float, UtilConstants.Flag_Yes, "meters^2", "Moment of inertia per kg for the aircraft as a whole"); } }
        public static DataRefElement AircraftWeightAcfMEmpty
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_empty", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftWeightAcfMDisplaced
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_displaced", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftWeightAcfMMax
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_max", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Weight of total fuel - appears to be in lbs.
        /// </summary>
        public static DataRefElement AircraftWeightAcfMFuelTot
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_fuel_tot", TypeStart_float, UtilConstants.Flag_Yes, "lbs", "Weight of total fuel - appears to be in lbs."); } }
        public static DataRefElement AircraftWeightAcfMJettison
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_jettison", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftWeightAcfMDisplacedY
        { get { return GetDataRefElement("sim/aircraft/weight/acf_m_displaced_y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
    }
}