﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// 1 if the vehicle is categorized as an ultralight, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsUltralight
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_ultralight", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as an ultralight, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as experimental, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsExperimental
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_experimental", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as experimental, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a general aviation aircraft, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsGeneralAviation
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_general_aviation", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a general aviation aircraft, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as an airliner, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsAirliner
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_airliner", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as an airliner, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a military aircraft, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsMilitary
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_military", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a military aircraft, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a cargo aircraft, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsCargo
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_cargo", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a cargo aircraft, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a glider, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsGlider
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_glider", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a glider, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a seaplane, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsSeaplane
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_seaplane", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a seaplane, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a helicopter, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsHelicopter
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_helicopter", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a helicopter, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a VTOL, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsVtol
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_vtol", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a VTOL, 0 if it is not."); } }
        /// <summary>
        /// 1 if the vehicle is categorized as a science fiction aircraft, 0 if it is not.
        /// </summary>
        public static DataRefElement Aircraft2MetadataIsSciFi
        { get { return GetDataRefElement("sim/aircraft2/metadata/is_sci_fi", TypeStart_int, UtilConstants.Flag_No, "Boolean", "1 if the vehicle is categorized as a science fiction aircraft, 0 if it is not."); } }
    }
}
