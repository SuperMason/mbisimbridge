﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Low value of the green arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoMP
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "Low value of the green arc for the manifold pressure instrument"); } }
        /// <summary>
        /// High value of the green arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiMP
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "High value of the green arc for the manifold pressure instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoMP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "Low value of the yellow arc for the manifold pressure instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiMP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "High value of the yellow arc for the manifold pressure instrument"); } }
        /// <summary>
        /// Low value of the red arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoMP
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "Low value of the red arc for the manifold pressure instrument"); } }
        /// <summary>
        /// High value of the red arc for the manifold pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiMP
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_MP", TypeStart_float, UtilConstants.Flag_No, "inhg", "High value of the red arc for the manifold pressure instrument"); } }
        /// <summary>
        /// Low value of the green arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoEPR
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "Low value of the green arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// High value of the green arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiEPR
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "High value of the green arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoEPR
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "Low value of the yellow arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiEPR
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "High value of the yellow arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// Low value of the red arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoEPR
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "Low value of the red arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// High value of the red arc for the engine pressure ratio instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiEPR
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_EPR", TypeStart_float, UtilConstants.Flag_No, "ratio", "High value of the red arc for the engine pressure ratio instrument"); } }
        /// <summary>
        /// Low value of the green arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "Low value of the green arc for the torque instrument"); } }
        /// <summary>
        /// High value of the green arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "High value of the green arc for the torque instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "Low value of the yellow arc for the torque instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "High value of the yellow arc for the torque instrument"); } }
        /// <summary>
        /// Low value of the red arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "Low value of the red arc for the torque instrument"); } }
        /// <summary>
        /// High value of the red arc for the torque instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiTRQ
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_TRQ", TypeStart_float, UtilConstants.Flag_No, "ft-lbs", "High value of the red arc for the torque instrument"); } }
        /// <summary>
        /// Low value of the green arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoFF
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "Low value of the green arc for the fuel flow instrument"); } }
        /// <summary>
        /// High value of the green arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiFF
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "High value of the green arc for the fuel flow instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoFF
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "Low value of the yellow arc for the fuel flow instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiFF
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "High value of the yellow arc for the fuel flow instrument"); } }
        /// <summary>
        /// Low value of the red arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoFF
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "Low value of the red arc for the fuel flow instrument"); } }
        /// <summary>
        /// High value of the red arc for the fuel flow instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiFF
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_FF", TypeStart_float, UtilConstants.Flag_No, "gal/hr_or_lb/hr", "High value of the red arc for the fuel flow instrument"); } }
        /// <summary>
        /// Low value of the green arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoITT
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the green arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// High value of the green arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiITT
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the green arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoITT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the yellow arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiITT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the yellow arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// Low value of the red arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoITT
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the red arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// High value of the red arc for the interturbine temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiITT
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_ITT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the red arc for the interturbine temperature instrument"); } }
        /// <summary>
        /// Low value of the green arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoEGT
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the green arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// High value of the green arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiEGT
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the green arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoEGT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the yellow arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiEGT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the yellow arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// Low value of the red arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoEGT
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the red arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// High value of the red arc for the exhaust gas temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiEGT
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_EGT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the red arc for the exhaust gas temperature instrument"); } }
        /// <summary>
        /// Low value of the green arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoCHT
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the green arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// High value of the green arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiCHT
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the green arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoCHT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the yellow arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiCHT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the yellow arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// Low value of the red arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoCHT
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the red arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// High value of the red arc for the cylinder-head temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiCHT
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_CHT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the red arc for the cylinder-head temperature instrument"); } }
        /// <summary>
        /// Low value of the green arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoOilT
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the green arc for the oil temperature instrument"); } }
        /// <summary>
        /// High value of the green arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiOilT
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the green arc for the oil temperature instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoOilT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the yellow arc for the oil temperature instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiOilT
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the yellow arc for the oil temperature instrument"); } }
        /// <summary>
        /// Low value of the red arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoOilT
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "Low value of the red arc for the oil temperature instrument"); } }
        /// <summary>
        /// High value of the red arc for the oil temperature instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiOilT
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_oilT", TypeStart_float, UtilConstants.Flag_No, "degC", "High value of the red arc for the oil temperature instrument"); } }
        /// <summary>
        /// Low value of the green arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoOilP
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the green arc for the oil pressure instrument"); } }
        /// <summary>
        /// High value of the green arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiOilP
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the green arc for the oil pressure instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoOilP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the yellow arc for the oil pressure instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiOilP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the yellow arc for the oil pressure instrument"); } }
        /// <summary>
        /// Low value of the red arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoOilP
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the red arc for the oil pressure instrument"); } }
        /// <summary>
        /// High value of the red arc for the oil pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiOilP
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_oilP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the red arc for the oil pressure instrument"); } }
        /// <summary>
        /// Low value of the green arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the green arc for the fuel pressure instrument"); } }
        /// <summary>
        /// High value of the green arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the green arc for the fuel pressure instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the yellow arc for the fuel pressure instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the yellow arc for the fuel pressure instrument"); } }
        /// <summary>
        /// Low value of the red arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "Low value of the red arc for the fuel pressure instrument"); } }
        /// <summary>
        /// High value of the red arc for the fuel pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiFuelP
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_fuelP", TypeStart_float, UtilConstants.Flag_No, "PSI", "High value of the red arc for the fuel pressure instrument"); } }
        /// <summary>
        /// Low value of the green arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the green arc for the generator amperage instrument"); } }
        /// <summary>
        /// High value of the green arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the green arc for the generator amperage instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the yellow arc for the generator amperage instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the yellow arc for the generator amperage instrument"); } }
        /// <summary>
        /// Low value of the red arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the red arc for the generator amperage instrument"); } }
        /// <summary>
        /// High value of the red arc for the generator amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiGenAmp
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_gen_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the red arc for the generator amperage instrument"); } }
        /// <summary>
        /// Low value of the green arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the green arc for the battery amperage instrument"); } }
        /// <summary>
        /// High value of the green arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the green arc for the battery amperage instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the yellow arc for the battery amperage instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the yellow arc for the battery amperage instrument"); } }
        /// <summary>
        /// Low value of the red arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Low value of the red arc for the battery amperage instrument"); } }
        /// <summary>
        /// High value of the red arc for the battery amperage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "High value of the red arc for the battery amperage instrument"); } }
        /// <summary>
        /// Battery amp when the non-standby batteries are fully charged.
        /// </summary>
        public static DataRefElement AircraftLimitsMaxBatAmp
        { get { return GetDataRefElement("sim/aircraft/limits/max_bat_amp", TypeStart_float, UtilConstants.Flag_No, "amps", "Battery amp when the non-standby batteries are fully charged."); } }
        /// <summary>
        /// Low value of the green arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "Low value of the green arc for the battery voltage instrument"); } }
        /// <summary>
        /// High value of the green arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "High value of the green arc for the battery voltage instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "Low value of the yellow arc for the battery voltage instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "High value of the yellow arc for the battery voltage instrument"); } }
        /// <summary>
        /// Low value of the red arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "Low value of the red arc for the battery voltage instrument"); } }
        /// <summary>
        /// High value of the red arc for the battery voltage instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiBatVolt
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_bat_volt", TypeStart_float, UtilConstants.Flag_No, "volts", "High value of the red arc for the battery voltage instrument"); } }
        /// <summary>
        /// This is the voltage when the standard (non-standby) batteries are fully charged.
        /// </summary>
        public static DataRefElement AircraftLimitsMaxBatVoltStandard
        { get { return GetDataRefElement("sim/aircraft/limits/max_bat_volt_standard", TypeStart_float, UtilConstants.Flag_No, "volts", "This is the voltage when the standard (non-standby) batteries are fully charged."); } }
        /// <summary>
        /// Low value of the green arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoVac
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "Low value of the green arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// High value of the green arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiVac
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "High value of the green arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoVac
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "Low value of the yellow arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiVac
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "High value of the yellow arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// Low value of the red arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoVac
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "Low value of the red arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// High value of the red arc for the vacuum pressure instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiVac
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "High value of the red arc for the vacuum pressure instrument"); } }
        /// <summary>
        /// Vacuum pressure put out when the engine is running at the bottom of red line (max vacuum).
        /// </summary>
        public static DataRefElement AircraftLimitsMaxVac
        { get { return GetDataRefElement("sim/aircraft/limits/max_vac", TypeStart_float, UtilConstants.Flag_No, "psi", "Vacuum pressure put out when the engine is running at the bottom of red line (max vacuum)."); } }
        /// <summary>
        /// Low value of the green arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoN1
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the green arc for the N1 instrument"); } }
        /// <summary>
        /// High value of the green arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiN1
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the green arc for the N1 instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoN1
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the yellow arc for the N1 instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiN1
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the yellow arc for the N1 instrument"); } }
        /// <summary>
        /// Low value of the red arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoN1
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the red arc for the N1 instrument"); } }
        /// <summary>
        /// High value of the red arc for the N1 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiN1
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_N1", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the red arc for the N1 instrument"); } }
        /// <summary>
        /// Low value of the green arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenLoN2
        { get { return GetDataRefElement("sim/aircraft/limits/green_lo_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the green arc for the N2 instrument"); } }
        /// <summary>
        /// High value of the green arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsGreenHiN2
        { get { return GetDataRefElement("sim/aircraft/limits/green_hi_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the green arc for the N2 instrument"); } }
        /// <summary>
        /// Low value of the yellow arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowLoN2
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_lo_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the yellow arc for the N2 instrument"); } }
        /// <summary>
        /// High value of the yellow arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsYellowHiN2
        { get { return GetDataRefElement("sim/aircraft/limits/yellow_hi_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the yellow arc for the N2 instrument"); } }
        /// <summary>
        /// Low value of the red arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedLoN2
        { get { return GetDataRefElement("sim/aircraft/limits/red_lo_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "Low value of the red arc for the N2 instrument"); } }
        /// <summary>
        /// High value of the red arc for the N2 instrument
        /// </summary>
        public static DataRefElement AircraftLimitsRedHiN2
        { get { return GetDataRefElement("sim/aircraft/limits/red_hi_N2", TypeStart_float, UtilConstants.Flag_No, "percent", "High value of the red arc for the N2 instrument"); } }

    }
}