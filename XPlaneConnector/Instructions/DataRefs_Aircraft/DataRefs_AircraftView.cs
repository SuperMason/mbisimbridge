﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement aircraftViewAcfTailnum = null;
        /// <summary>
        /// Tail number
        /// <para>這個參數值與 Cockpit2RadiosActuatorsFlightId 一樣</para>
        /// </summary>
        public static DataRefElement AircraftViewAcfTailnum {
            get {
                if (aircraftViewAcfTailnum == null)
                { aircraftViewAcfTailnum = GetDataRefElement("sim/aircraft/view/acf_tailnum", TypeStart_byte40, UtilConstants.Flag_Yes, "string", "Tail number"); }
                return aircraftViewAcfTailnum;
            }
        }

        private static DataRefElement aircraftViewAcfModesId = null;
        /// <summary>
        /// 24bit (0-16777215 or 0-0xFFFFFF) unique ID of the airframe. This is also known as the ADS-B "hexcode".
        /// </summary>
        public static DataRefElement AircraftViewAcfModesId {
            get {
                if (aircraftViewAcfModesId == null)
                { aircraftViewAcfModesId = GetDataRefElement("sim/aircraft/view/acf_modeS_id", TypeStart_int, UtilConstants.Flag_Yes, "integer", "24bit (0-16777215 or 0-0xFFFFFF) unique ID of the airframe. This is also known as the ADS-B \"hexcode\"."); }
                return aircraftViewAcfModesId;
            }
        }

        private static DataRefElement aircraftViewAcfAuthor = null;
        /// <summary>
        /// Author's Name
        /// </summary>
        public static DataRefElement AircraftViewAcfAuthor {
            get {
                if (aircraftViewAcfAuthor == null)
                { aircraftViewAcfAuthor = GetDataRefElement("sim/aircraft/view/acf_author", TypeStart_byte500, UtilConstants.Flag_Yes, "string", "Author's Name"); }
                return aircraftViewAcfAuthor;
            }
        }

        private static DataRefElement aircraftViewAcfDescrip = null;
        /// <summary>
        /// Brief description of the plane. Was 500 chars in older planes.
        /// </summary>
        public static DataRefElement AircraftViewAcfDescrip {
            get {
                if (aircraftViewAcfDescrip == null)
                { aircraftViewAcfDescrip = GetDataRefElement("sim/aircraft/view/acf_descrip", TypeStart_byte260, UtilConstants.Flag_Yes, "string", "Brief description of the plane. Was 500 chars in older planes."); }
                return aircraftViewAcfDescrip;
            }
        }

        private static DataRefElement aircraftViewAcfNotes = null;
        /// <summary>
        /// Notes on the plane
        /// </summary>
        public static DataRefElement AircraftViewAcfNotes {
            get {
                if (aircraftViewAcfNotes == null)
                { aircraftViewAcfNotes = GetDataRefElement("sim/aircraft/view/acf_notes", TypeStart_byte240, UtilConstants.Flag_Yes, "string", "Notes on the plane"); }
                return aircraftViewAcfNotes;
            }
        }

        private static DataRefElement aircraftViewAcfSizeX = null;
        public static DataRefElement AircraftViewAcfSizeX {
            get {
                if (aircraftViewAcfSizeX == null)
                { aircraftViewAcfSizeX = GetDataRefElement("sim/aircraft/view/acf_size_x", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfSizeX;
            }
        }

        private static DataRefElement aircraftViewAcfSizeZ = null;
        /// <summary>
        /// shadow size, and viewing distance size
        /// </summary>
        public static DataRefElement AircraftViewAcfSizeZ {
            get {
                if (aircraftViewAcfSizeZ == null)
                { aircraftViewAcfSizeZ = GetDataRefElement("sim/aircraft/view/acf_size_z", TypeStart_float, UtilConstants.Flag_Yes, "???", "shadow size, and viewing distance size"); }
                return aircraftViewAcfSizeZ;
            }
        }

        private static DataRefElement aircraftViewAcfAsiKts = null;
        /// <summary>
        /// air speed indicator knots calibration
        /// </summary>
        public static DataRefElement AircraftViewAcfAsiKts {
            get {
                if (aircraftViewAcfAsiKts == null)
                { aircraftViewAcfAsiKts = GetDataRefElement("sim/aircraft/view/acf_asi_kts", TypeStart_int, UtilConstants.Flag_Yes, "enum", "air speed indicator knots calibration"); }
                return aircraftViewAcfAsiKts;
            }
        }

        private static DataRefElement aircraftViewAcfCockpitType = null;
        /// <summary>
        /// cockpit panel type
        /// </summary>
        public static DataRefElement AircraftViewAcfCockpitType {
            get {
                if (aircraftViewAcfCockpitType == null)
                { aircraftViewAcfCockpitType = GetDataRefElement("sim/aircraft/view/acf_cockpit_type", TypeStart_int, UtilConstants.Flag_Yes, "enum", "cockpit panel type"); }
                return aircraftViewAcfCockpitType;
            }
        }

        private static DataRefElement aircraftViewAcfHasSCFd = null;
        /// <summary>
        /// has single cue flight director ?
        /// </summary>
        public static DataRefElement AircraftViewAcfHasSCFd {
            get {
                if (aircraftViewAcfHasSCFd == null)
                { aircraftViewAcfHasSCFd = GetDataRefElement("sim/aircraft/view/acf_has_SC_fd", TypeStart_int, UtilConstants.Flag_Yes, "bool", "has single cue flight director?"); }
                return aircraftViewAcfHasSCFd;
            }
        }

        private static DataRefElement aircraftViewAcfHasStallwarn = null;
        /// <summary>
        /// has audio stall warning ?
        /// </summary>
        public static DataRefElement AircraftViewAcfHasStallwarn {
            get {
                if (aircraftViewAcfHasStallwarn == null)
                { aircraftViewAcfHasStallwarn = GetDataRefElement("sim/aircraft/view/acf_has_stallwarn", TypeStart_int, UtilConstants.Flag_Yes, "bool", "has audio stall warning?"); }
                return aircraftViewAcfHasStallwarn;
            }
        }

        private static DataRefElement aircraftViewAcfHasLitemapTex = null;
        /// <summary>
        /// Do we have a lite map texture for this ?
        /// </summary>
        public static DataRefElement AircraftViewAcfHasLitemapTex {
            get {
                if (aircraftViewAcfHasLitemapTex == null)
                { aircraftViewAcfHasLitemapTex = GetDataRefElement("sim/aircraft/view/acf_has_litemap_tex", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Do we have a lite map texture for this?"); }
                return aircraftViewAcfHasLitemapTex;
            }
        }

        private static DataRefElement aircraftViewAcfPeX = null;
        /// <summary>
        /// Position of pilot's head relative to CG, X
        /// </summary>
        public static DataRefElement AircraftViewAcfPeX {
            get {
                if (aircraftViewAcfPeX == null)
                { aircraftViewAcfPeX = GetDataRefElement("sim/aircraft/view/acf_peX", TypeStart_float, UtilConstants.Flag_Yes, "pos", "Position of pilot's head relative to CG, X"); }
                return aircraftViewAcfPeX;
            }
        }

        private static DataRefElement aircraftViewAcfPeY = null;
        /// <summary>
        /// Position of pilot's head relative to CG, Y
        /// </summary>
        public static DataRefElement AircraftViewAcfPeY {
            get {
                if (aircraftViewAcfPeY == null)
                { aircraftViewAcfPeY = GetDataRefElement("sim/aircraft/view/acf_peY", TypeStart_float, UtilConstants.Flag_Yes, "pos", "Position of pilot's head relative to CG, Y"); }
                return aircraftViewAcfPeY;
            }
        }

        private static DataRefElement aircraftViewAcfPeZ = null;
        /// <summary>
        /// Position of pilot's head relative to CG, Z
        /// </summary>
        public static DataRefElement AircraftViewAcfPeZ {
            get {
                if (aircraftViewAcfPeZ == null)
                { aircraftViewAcfPeZ = GetDataRefElement("sim/aircraft/view/acf_peZ", TypeStart_float, UtilConstants.Flag_Yes, "pos", "Position of pilot's head relative to CG, Z"); }
                return aircraftViewAcfPeZ;
            }
        }

        private static DataRefElement aircraftViewAcfVso = null;
        /// <summary>
        /// Various speed maxes for the aircraft.
        /// </summary>
        public static DataRefElement AircraftViewAcfVso {
            get {
                if (aircraftViewAcfVso == null)
                { aircraftViewAcfVso = GetDataRefElement("sim/aircraft/view/acf_Vso", TypeStart_float, UtilConstants.Flag_Yes, "kias", ""); }
                return aircraftViewAcfVso;
            }
        }

        private static DataRefElement aircraftViewAcfVs = null;
        public static DataRefElement AircraftViewAcfVs {
            get {
                if (aircraftViewAcfVs == null)
                { aircraftViewAcfVs = GetDataRefElement("sim/aircraft/view/acf_Vs", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfVs;
            }
        }

        private static DataRefElement aircraftViewAcfVfe = null;
        public static DataRefElement AircraftViewAcfVfe {
            get {
                if (aircraftViewAcfVfe == null)
                { aircraftViewAcfVfe = GetDataRefElement("sim/aircraft/view/acf_Vfe", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfVfe;
            }
        }

        private static DataRefElement aircraftViewAcfVno = null;
        public static DataRefElement AircraftViewAcfVno {
            get {
                if (aircraftViewAcfVno == null)
                { aircraftViewAcfVno = GetDataRefElement("sim/aircraft/view/acf_Vno", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfVno;
            }
        }

        private static DataRefElement aircraftViewAcfVne = null;
        public static DataRefElement AircraftViewAcfVne {
            get {
                if (aircraftViewAcfVne == null)
                { aircraftViewAcfVne = GetDataRefElement("sim/aircraft/view/acf_Vne", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfVne;
            }
        }

        private static DataRefElement aircraftViewAcfMmo = null;
        public static DataRefElement AircraftViewAcfMmo {
            get {
                if (aircraftViewAcfMmo == null)
                { aircraftViewAcfMmo = GetDataRefElement("sim/aircraft/view/acf_Mmo", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfMmo;
            }
        }

        private static DataRefElement aircraftViewAcfGneg = null;
        public static DataRefElement AircraftViewAcfGneg {
            get {
                if (aircraftViewAcfGneg == null)
                { aircraftViewAcfGneg = GetDataRefElement("sim/aircraft/view/acf_Gneg", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfGneg;
            }
        }

        private static DataRefElement aircraftViewAcfGpos = null;
        public static DataRefElement AircraftViewAcfGpos {
            get {
                if (aircraftViewAcfGpos == null)
                { aircraftViewAcfGpos = GetDataRefElement("sim/aircraft/view/acf_Gpos", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfGpos;
            }
        }

        private static DataRefElement aircraftViewAcfYawstringx = null;
        /// <summary>
        /// The yaw string, that thing that no one knows how to get rid of.
        /// </summary>
        public static DataRefElement AircraftViewAcfYawstringx {
            get {
                if (aircraftViewAcfYawstringx == null)
                { aircraftViewAcfYawstringx = GetDataRefElement("sim/aircraft/view/acf_yawstringx", TypeStart_float, UtilConstants.Flag_Yes, "???", "The yaw string, that thing that no one knows how to get rid of."); }
                return aircraftViewAcfYawstringx;
            }
        }

        private static DataRefElement aircraftViewAcfYawstringy = null;
        public static DataRefElement AircraftViewAcfYawstringy {
            get {
                if (aircraftViewAcfYawstringy == null)
                { aircraftViewAcfYawstringy = GetDataRefElement("sim/aircraft/view/acf_yawstringy", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfYawstringy;
            }
        }

        private static DataRefElement aircraftViewAcfHUDCntry = null;
        public static DataRefElement AircraftViewAcfHUDCntry {
            get {
                if (aircraftViewAcfHUDCntry == null)
                { aircraftViewAcfHUDCntry = GetDataRefElement("sim/aircraft/view/acf_HUD_cntry", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfHUDCntry;
            }
        }

        private static DataRefElement aircraftViewAcfHUDDelx = null;
        public static DataRefElement AircraftViewAcfHUDDelx {
            get {
                if (aircraftViewAcfHUDDelx == null)
                { aircraftViewAcfHUDDelx = GetDataRefElement("sim/aircraft/view/acf_HUD_delx", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfHUDDelx;
            }
        }

        private static DataRefElement aircraftViewAcfHUDDely = null;
        public static DataRefElement AircraftViewAcfHUDDely {
            get {
                if (aircraftViewAcfHUDDely == null)
                { aircraftViewAcfHUDDely = GetDataRefElement("sim/aircraft/view/acf_HUD_dely", TypeStart_float, UtilConstants.Flag_Yes, "", ""); }
                return aircraftViewAcfHUDDely;
            }
        }

        private static DataRefElement aircraftViewAcfICAO = null;
        /// <summary>
        /// ICAO code for aircraft (a string) entered by author
        /// </summary>
        public static DataRefElement AircraftViewAcfICAO {
            get {
                if (aircraftViewAcfICAO == null)
                { aircraftViewAcfICAO = GetDataRefElement("sim/aircraft/view/acf_ICAO", TypeStart_byte40, UtilConstants.Flag_Yes, "string", "ICAO code for aircraft (a string) entered by author"); }
                return aircraftViewAcfICAO;
            }
        }

        private static DataRefElement aircraftViewAcfDoorX = null;
        /// <summary>
        /// position of door relative to CG, latitude offset in meters
        /// </summary>
        public static DataRefElement AircraftViewAcfDoorX {
            get {
                if (aircraftViewAcfDoorX == null)
                { aircraftViewAcfDoorX = GetDataRefElement("sim/aircraft/view/acf_door_x", TypeStart_float, UtilConstants.Flag_No, "meters", "position of door relative to CG, latitude offset in meters"); }
                return aircraftViewAcfDoorX;
            }
        }

        private static DataRefElement aircraftViewAcfDoorY = null;
        /// <summary>
        /// position of door relative to CG, vertical offset in meters
        /// </summary>
        public static DataRefElement AircraftViewAcfDoorY {
            get {
                if (aircraftViewAcfDoorY == null)
                { aircraftViewAcfDoorY = GetDataRefElement("sim/aircraft/view/acf_door_y", TypeStart_float, UtilConstants.Flag_No, "meters", "position of door relative to CG, vertical offset in meters"); }
                return aircraftViewAcfDoorY;
            }
        }

        private static DataRefElement aircraftViewAcfDoorZ = null;
        /// <summary>
        /// position of door relative to CG, longitude offset in meters
        /// </summary>
        public static DataRefElement AircraftViewAcfDoorZ {
            get {
                if (aircraftViewAcfDoorZ == null)
                { aircraftViewAcfDoorZ = GetDataRefElement("sim/aircraft/view/acf_door_z", TypeStart_float, UtilConstants.Flag_No, "meters", "position of door relative to CG, longitude offset in meters"); }
                return aircraftViewAcfDoorZ;
            }
        }

        private static DataRefElement aircraftViewAcfHasHOOPSHUD = null;
        /// <summary>
        /// has Hoops HUD
        /// </summary>
        public static DataRefElement AircraftViewAcfHasHOOPSHUD {
            get {
                if (aircraftViewAcfHasHOOPSHUD == null)
                { aircraftViewAcfHasHOOPSHUD = GetDataRefElement("sim/aircraft/view/acf_has_HOOPS_HUD", TypeStart_int, UtilConstants.Flag_Yes, "???", "has Hoops HUD"); }
                return aircraftViewAcfHasHOOPSHUD;
            }
        }

        private static DataRefElement aircraftViewAcfLiveryIndex = null;
        /// <summary>
        /// index number of livery
        /// </summary>
        public static DataRefElement AircraftViewAcfLiveryIndex {
            get {
                if (aircraftViewAcfLiveryIndex == null)
                { aircraftViewAcfLiveryIndex = GetDataRefElement("sim/aircraft/view/acf_livery_index", TypeStart_int, UtilConstants.Flag_No, "index", "index number of livery"); }
                return aircraftViewAcfLiveryIndex;
            }
        }

        private static DataRefElement aircraftViewAcfLiveryPath = null;
        /// <summary>
        /// path of current livery. dir separator i, ends in dir separator. WARNING: slow dataref, don't read a lot!
        /// </summary>
        public static DataRefElement AircraftViewAcfLiveryPath {
            get {
                if (aircraftViewAcfLiveryPath == null)
                { aircraftViewAcfLiveryPath = GetDataRefElement("sim/aircraft/view/acf_livery_path", TypeStart_byte1024, UtilConstants.Flag_No, "string", "path of current livery. dir separator i, ends in dir separator. WARNING: slow dataref, don't read a lot!"); }
                return aircraftViewAcfLiveryPath;
            }
        }
    }
}