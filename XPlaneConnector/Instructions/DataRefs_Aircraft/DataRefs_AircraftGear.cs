﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Are any of the gear on this plane retractable.  We strongly recommend you DO NOT write to this dataref.
        /// </summary>
        public static DataRefElement AircraftGearAcfGearRetract
        { get { return GetDataRefElement("sim/aircraft/gear/acf_gear_retract", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Are any of the gear on this plane retractable.  We strongly recommend you DO NOT write to this dataref."); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfGearIsSkid
        { get { return GetDataRefElement("sim/aircraft/gear/acf_gear_is_skid", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfNwSteerdeg1
        { get { return GetDataRefElement("sim/aircraft/gear/acf_nw_steerdeg1", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfNwSteerdeg2
        { get { return GetDataRefElement("sim/aircraft/gear/acf_nw_steerdeg2", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfWaterRudLongarm
        { get { return GetDataRefElement("sim/aircraft/gear/acf_water_rud_longarm", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfWaterRudArea
        { get { return GetDataRefElement("sim/aircraft/gear/acf_water_rud_area", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfWaterRudMaxdef
        { get { return GetDataRefElement("sim/aircraft/gear/acf_water_rud_maxdef", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfHEqlbm
        { get { return GetDataRefElement("sim/aircraft/gear/acf_h_eqlbm", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// 
        /// </summary>
        public static DataRefElement AircraftGearAcfTheEqlbm
        { get { return GetDataRefElement("sim/aircraft/gear/acf_the_eqlbm", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// door current angle, not in flite since it is geo and this is a nice place to keep all the door geo.
        /// </summary>
        public static DataRefElement AircraftGearAcfGearDoorAngNow
        { get { return GetDataRefElement("sim/aircraft/gear/acf_gear_door_ang_now", TypeStart_float20, UtilConstants.Flag_Yes, "???", "door current angle, not in flite since it is geo and this is a nice place to keep all the door geo."); } }
    }
}