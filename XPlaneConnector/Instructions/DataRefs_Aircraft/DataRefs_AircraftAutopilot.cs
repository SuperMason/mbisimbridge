﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement aircraftAutopilotVviStepFt = null;
        /// <summary>
        /// Step increment for autopilot VVI
        /// </summary>
        public static DataRefElement AircraftAutopilotVviStepFt {
            get {
                if (aircraftAutopilotVviStepFt == null)
                { aircraftAutopilotVviStepFt = GetDataRefElement("sim/aircraft/autopilot/vvi_step_ft", TypeStart_float, UtilConstants.Flag_Yes, "Feet", "Step increment for autopilot VVI"); }
                return aircraftAutopilotVviStepFt;
            }
        }

        private static DataRefElement aircraftAutopilotAltStepFt = null;
        /// <summary>
        /// Step increment for autopilot altitude
        /// </summary>
        public static DataRefElement AircraftAutopilotAltStepFt {
            get {
                if (aircraftAutopilotAltStepFt == null)
                { aircraftAutopilotAltStepFt = GetDataRefElement("sim/aircraft/autopilot/alt_step_ft", TypeStart_float, UtilConstants.Flag_Yes, "Feet", "Step increment for autopilot altitude"); }
                return aircraftAutopilotAltStepFt;
            }
        }

        private static DataRefElement aircraftAutopilotRadioAltimeterStepFt = null;
        /// <summary>
        /// Step increment for radio altimeter decision height
        /// </summary>
        public static DataRefElement AircraftAutopilotRadioAltimeterStepFt {
            get {
                if (aircraftAutopilotRadioAltimeterStepFt == null)
                { aircraftAutopilotRadioAltimeterStepFt = GetDataRefElement("sim/aircraft/autopilot/radio_altimeter_step_ft", TypeStart_float, UtilConstants.Flag_Yes, "Feet", "Step increment for radio altimeter decision height"); }
                return aircraftAutopilotRadioAltimeterStepFt;
            }
        }

        private static DataRefElement aircraftAutopilotPreconfiguredApType = null;
        /// <summary>
        /// 0=X-Plane custom, 1=Airliner, 2=GFC-700, 3=S-Tec 55, 4=S-Tec 55 with altitude preselect, 5=KAP-140 single axis, 6=KAP-140 dual axis, 7=KAP-140 dual axis with altitude preselect, 8=Piper Autocontrol, 9=Rockwell/Collins FCS-65
        /// </summary>
        public static DataRefElement AircraftAutopilotPreconfiguredApType {
            get {
                if (aircraftAutopilotPreconfiguredApType == null)
                { aircraftAutopilotPreconfiguredApType = GetDataRefElement("sim/aircraft/autopilot/preconfigured_ap_type", TypeStart_int, UtilConstants.Flag_No, "enum", "0=X-Plane custom, 1=Airliner, 2=GFC-700, 3=S-Tec 55, 4=S-Tec 55 with altitude preselect, 5=KAP-140 single axis, 6=KAP-140 dual axis, 7=KAP-140 dual axis with altitude preselect, 8=Piper Autocontrol, 9=Rockwell/Collins FCS-65"); }
                return aircraftAutopilotPreconfiguredApType;
            }
        }

        private static DataRefElement aircraftAutopilotSingleAxisAutopilot = null;
        /// <summary>
        /// Whether the autopilot is strictly single axis (bank) only.
        /// </summary>
        public static DataRefElement AircraftAutopilotSingleAxisAutopilot {
            get {
                if (aircraftAutopilotSingleAxisAutopilot == null)
                { aircraftAutopilotSingleAxisAutopilot = GetDataRefElement("sim/aircraft/autopilot/single_axis_autopilot", TypeStart_int, UtilConstants.Flag_No, "boolean", "Whether the autopilot is strictly single axis (bank) only."); }
                return aircraftAutopilotSingleAxisAutopilot;
            }
        }

        private static DataRefElement aircraftAutopilotAhSource = null;
        /// <summary>
        /// Attitude source for the autopilot: 10 = AHRS, 11 = elec gyro, 12 = vacuum gyro, 13 = turn coordinator and absolute pressure transducer
        /// </summary>
        public static DataRefElement AircraftAutopilotAhSource {
            get {
                if (aircraftAutopilotAhSource == null)
                { aircraftAutopilotAhSource = GetDataRefElement("sim/aircraft/autopilot/ah_source", TypeStart_int, UtilConstants.Flag_No, "enum", "Attitude source for the autopilot: 10 = AHRS, 11 = elec gyro, 12 = vacuum gyro, 13 = turn coordinator and absolute pressure transducer"); }
                return aircraftAutopilotAhSource;
            }
        }

        private static DataRefElement aircraftAutopilotDgSource = null;
        /// <summary>
        /// Directional gyro source for the autopillot: 10 = AHRS, 11 = elec gyro (HSI or DG), 12 = vacuum gyro (DG)
        /// </summary>
        public static DataRefElement AircraftAutopilotDgSource {
            get {
                if (aircraftAutopilotDgSource == null)
                { aircraftAutopilotDgSource = GetDataRefElement("sim/aircraft/autopilot/dg_source", TypeStart_int, UtilConstants.Flag_No, "enum", "Directional gyro source for the autopillot: 10 = AHRS, 11 = elec gyro (HSI or DG), 12 = vacuum gyro (DG)"); }
                return aircraftAutopilotDgSource;
            }
        }
    }
}