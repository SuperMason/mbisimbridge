﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// amount the stab moves in trim automatically as you go to redline (zero at zero airspeed)
        /// </summary>
        public static DataRefElement AircraftOverflowAcfStabDelincToVne
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_stab_delinc_to_Vne", TypeStart_float, UtilConstants.Flag_Yes, "degree", "amount the stab moves in trim automatically as you go to redline (zero at zero airspeed)"); } }
        public static DataRefElement AircraftOverflowAcfVmca
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_Vmca", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// v-speeds
        /// </summary>
        public static DataRefElement AircraftOverflowAcfVyse
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_Vyse", TypeStart_float, UtilConstants.Flag_Yes, "???", "v-speeds"); } }
        /// <summary>
        /// flapping hinge arm
        /// </summary>
        public static DataRefElement AircraftOverflowAcfFlapArm
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_flap_arm", TypeStart_float, UtilConstants.Flag_Yes, "???", "flapping hinge arm"); } }
        public static DataRefElement AircraftOverflowAcfCgZFwd
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cgZ_fwd", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// cg limits
        /// </summary>
        public static DataRefElement AircraftOverflowAcfCgZAft
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cgZ_aft", TypeStart_float, UtilConstants.Flag_Yes, "???", "cg limits"); } }
        /// <summary>
        /// gear cycle time... different for different gear in some cases, NOTE : This used to be f5 in v6
        /// </summary>
        public static DataRefElement AircraftOverflowAcfGearCycTime
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_gear_cyc_time", TypeStart_float10, UtilConstants.Flag_Yes, "???", "gear cycle time... different for different gear in some cases, NOTE : This used to be f5 in v6"); } }
        public static DataRefElement AircraftOverflowAcfRefuelX
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_refuel_X", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfRefuelY
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_refuel_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// refueling port location
        /// </summary>
        public static DataRefElement AircraftOverflowAcfRefuelZ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_refuel_Z", TypeStart_float, UtilConstants.Flag_Yes, "???", "refueling port location"); } }
        /// <summary>
        /// this gear turns with rudder input - writable until v10
        /// </summary>
        public static DataRefElement AircraftOverflowAcfGearSteers
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_gear_steers", TypeStart_int10, UtilConstants.Flag_No, "???", "this gear turns with rudder input - writable until v10"); } }
        /// <summary>
        /// [WING] variable dihedral
        /// </summary>
        public static DataRefElement AircraftOverflowAcfDihed2
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_dihed2", TypeStart_float56, UtilConstants.Flag_Yes, "???", "[WING] variable dihedral"); } }
        public static DataRefElement AircraftOverflowJettX
        { get { return GetDataRefElement("sim/aircraft/overflow/jett_X", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowJettY
        { get { return GetDataRefElement("sim/aircraft/overflow/jett_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowJettZ
        { get { return GetDataRefElement("sim/aircraft/overflow/jett_Z", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfPuffx
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_puffX", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfPuffy
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_puffY", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// manuevering rocket forces
        /// </summary>
        public static DataRefElement AircraftOverflowAcfPuffZ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_puffZ", TypeStart_float, UtilConstants.Flag_Yes, "???", "manuevering rocket forces"); } }
        /// <summary>
        /// Vle
        /// </summary>
        public static DataRefElement AircraftOverflowAcfVle
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_Vle", TypeStart_float, UtilConstants.Flag_Yes, "???", "Vle"); } }
        public static DataRefElement AircraftOverflowAcfAspHiRate
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_ASp_hi_rate", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// astab stuff i should have had in there the first time!
        /// </summary>
        public static DataRefElement AircraftOverflowAcfAShHiRate
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_ASh_hi_rate", TypeStart_float, UtilConstants.Flag_Yes, "???", "astab stuff i should have had in there the first time!"); } }
        /// <summary>
        /// elevator align with flaps
        /// </summary>
        public static DataRefElement AircraftOverflowAcfElevflaps
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_elevflaps", TypeStart_float, UtilConstants.Flag_Yes, "???", "elevator align with flaps"); } }
        /// <summary>
        /// fuel tank locations when empty - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankX
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_X", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when empty - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank locations when empty - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankY
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_Y", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when empty - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank locations when empty - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankZ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_Z", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when empty - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank locations when full - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankXFull
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_X_full", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when full - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank locations when full - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankYFull
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_Y_full", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when full - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank locations when full - was dim 3 in XP8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankZFull
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_Z_full", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank locations when full - was dim 3 in XP8 and earlier"); } }
        /// <summary>
        /// fuel tank ratio per tank -- was dim 3 in xp 8 and earlier
        /// </summary>
        public static DataRefElement AircraftOverflowAcfTankRat
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tank_rat", TypeStart_float9, UtilConstants.Flag_Yes, "???", "fuel tank ratio per tank -- was dim 3 in xp 8 and earlier"); } }
        /// <summary>
        /// alpha of stall warning... user must specify since warning different for different planes.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfStallWarnAlpha
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_stall_warn_alpha", TypeStart_float, UtilConstants.Flag_Yes, "???", "alpha of stall warning... user must specify since warning different for different planes."); } }
        /// <summary>
        /// hang-gliders and wright gliders
        /// </summary>
        public static DataRefElement AircraftOverflowAcfMassShift
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_mass_shift", TypeStart_float, UtilConstants.Flag_Yes, "???", "hang-gliders and wright gliders"); } }
        /// <summary>
        /// hang-gliders and wright gliders
        /// </summary>
        public static DataRefElement AircraftOverflowAcfMassShiftDx
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_mass_shift_dx", TypeStart_float, UtilConstants.Flag_Yes, "???", "hang-gliders and wright gliders"); } }
        /// <summary>
        /// hang-gliders and wright gliders
        /// </summary>
        public static DataRefElement AircraftOverflowAcfMassShiftDz
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_mass_shift_dz", TypeStart_float, UtilConstants.Flag_Yes, "???", "hang-gliders and wright gliders"); } }
        /// <summary>
        /// let people decide feathered pitch to get right for their plane.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfFeatheredPitch
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_feathered_pitch", TypeStart_float, UtilConstants.Flag_Yes, "???", "let people decide feathered pitch to get right for their plane."); } }
        /// <summary>
        /// astab stuff I should have had in there the first time!
        /// </summary>
        public static DataRefElement AircraftOverflowAcfASmaxgHi
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_ASmaxg_hi", TypeStart_float, UtilConstants.Flag_Yes, "???", "astab stuff I should have had in there the first time!"); } }
        public static DataRefElement AircraftOverflowAcfAsgHiPos
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_ASg_hi_pos", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// astab stuff I should have had in there the first time!
        /// </summary>
        public static DataRefElement AircraftOverflowAcfASgHiRate
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_ASg_hi_rate", TypeStart_float, UtilConstants.Flag_Yes, "???", "astab stuff I should have had in there the first time!"); } }
        /// <summary>
        /// wing-tilt steering
        /// </summary>
        public static DataRefElement AircraftOverflowAcfWingTiltPtch
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_wing_tilt_ptch", TypeStart_float, UtilConstants.Flag_Yes, "???", "wing-tilt steering"); } }
        /// <summary>
        /// wing-tilt steering
        /// </summary>
        public static DataRefElement AircraftOverflowAcfWingTiltRoll
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_wing_tilt_roll", TypeStart_float, UtilConstants.Flag_Yes, "???", "wing-tilt steering"); } }
        /// <summary>
        /// max pressurization of the fuselage
        /// </summary>
        public static DataRefElement AircraftOverflowAcfMaxPressDiff
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_max_press_diff", TypeStart_float, UtilConstants.Flag_Yes, "pascals", "max pressurization of the fuselage"); } }
        /// <summary>
        /// capacity of the crew oxygen reserve
        /// </summary>
        public static DataRefElement AircraftOverflowAcfO2BottleCapLiters
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_o2_bottle_cap_liters", TypeStart_int, UtilConstants.Flag_Yes, "liters", "capacity of the crew oxygen reserve"); } }
        /// <summary>
        /// used by x-19
        /// </summary>
        public static DataRefElement AircraftOverflowAcfDiffCollWithPtch
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_diff_coll_with_ptch", TypeStart_float, UtilConstants.Flag_Yes, "???", "used by x-19"); } }
        public static DataRefElement AircraftOverflowAcfFlapRoll
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_flap_roll", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// flap actuation... like for really high-lift guys still manuevering at low speeds.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfFlapPtch
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_flap_ptch", TypeStart_float, UtilConstants.Flag_Yes, "???", "flap actuation... like for really high-lift guys still manuevering at low speeds."); } }
        /// <summary>
        /// ch-47 chinook performance
        /// </summary>
        public static DataRefElement AircraftOverflowAcfDiffCyclWithHdngLat
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_diff_cycl_with_hdng_lat", TypeStart_float, UtilConstants.Flag_Yes, "???", "ch-47 chinook performance"); } }
        /// <summary>
        /// phase thrust-vectoring maneuvering in as we go from 90 to 0, going from hover with puffers to f-22 dogfight
        /// </summary>
        public static DataRefElement AircraftOverflowAcfPhaseTvectOutAt90
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_phase_tvect_out_at_90", TypeStart_int, UtilConstants.Flag_Yes, "???", "phase thrust-vectoring maneuvering in as we go from 90 to 0, going from hover with puffers to f-22 dogfight"); } }
        public static DataRefElement AircraftOverflowAcfRollCo
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_roll_co", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// rolling and braking
        /// </summary>
        public static DataRefElement AircraftOverflowAcfBrakeCo
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_brake_co", TypeStart_float, UtilConstants.Flag_Yes, "???", "rolling and braking"); } }
        /// <summary>
        /// engine always runs at optimum mixture... like FADEC or auto conversions.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfDriveByWire
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_drive_by_wire", TypeStart_int, UtilConstants.Flag_Yes, "???", "engine always runs at optimum mixture... like FADEC or auto conversions."); } }
        /// <summary>
        /// plane has specularity lighting.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfIsGlossy
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_is_glossy", TypeStart_int, UtilConstants.Flag_Yes, "???", "plane has specularity lighting."); } }

        private static DataRefElement aircraftOverflowAcfNumTanks = null;
        /// <summary>
        /// number fuel tanks - as of 860, all planes have 9 tanks and ratios for each - ratio of 0.0 means tank is not used
        /// </summary>
        public static DataRefElement AircraftOverflowAcfNumTanks {
            get {
                if (aircraftOverflowAcfNumTanks == null)
                { aircraftOverflowAcfNumTanks = GetDataRefElement("sim/aircraft/overflow/acf_num_tanks", TypeStart_int, UtilConstants.Flag_Yes, "count", "number fuel tanks - as of 860, all planes have 9 tanks and ratios for each - ratio of 0.0 means tank is not used"); }
                return aircraftOverflowAcfNumTanks;
            }
        }

        /// <summary>
        /// refueling port
        /// </summary>
        public static DataRefElement AircraftOverflowAcfHasRefuel
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_has_refuel", TypeStart_int, UtilConstants.Flag_Yes, "???", "refueling port"); } }
        /// <summary>
        /// jettisonable load is slung now
        /// </summary>
        public static DataRefElement AircraftOverflowAcfJettIsSlung
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_jett_is_slung", TypeStart_int, UtilConstants.Flag_Yes, "???", "jettisonable load is slung now"); } }
        /// <summary>
        /// mass of each engine for distribution and loss on engine separation. NOTE : This used to be f57 in v6
        /// </summary>
        public static DataRefElement AircraftOverflowAcfEngMass
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_eng_mass", TypeStart_float8, UtilConstants.Flag_Yes, "???", "mass of each engine for distribution and loss on engine separation. NOTE : This used to be f57 in v6"); } }
        /// <summary>
        /// phase thrust-vectoring maneuvering out as we go from 90 to 0 deg tvec, going from maneuver-to-hover to regular thrust
        /// </summary>
        public static DataRefElement AircraftOverflowAcfPhaseTvectOutAt00
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_phase_tvect_out_at_00", TypeStart_int, UtilConstants.Flag_Yes, "???", "phase thrust-vectoring maneuvering out as we go from 90 to 0 deg tvec, going from maneuver-to-hover to regular thrust"); } }
        /// <summary>
        /// auto-trim out any flight loads... numerous planes have this.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfAutoTrimEQ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_auto_trimEQ", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "auto-trim out any flight loads... numerous planes have this."); } }
        /// <summary>
        /// has dual-cue flight-dir
        /// </summary>
        public static DataRefElement AircraftOverflowAcfHasDCFd
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_has_DC_fd", TypeStart_int, UtilConstants.Flag_Yes, "???", "has dual-cue flight-dir"); } }
        /// <summary>
        /// the Jatviggen does flaps with gear automatically
        /// </summary>
        public static DataRefElement AircraftOverflowAcfFlapsWithGearEQ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_flaps_with_gearEQ", TypeStart_int, UtilConstants.Flag_Yes, "???", "the Jatviggen does flaps with gear automatically"); } }
        /// <summary>
        /// viggen does this!
        /// </summary>
        public static DataRefElement AircraftOverflowAcfRevOnTouchdown
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_rev_on_touchdown", TypeStart_int, UtilConstants.Flag_Yes, "???", "viggen does this!"); } }
        /// <summary>
        /// bring in the flaps with thrust vector
        /// </summary>
        public static DataRefElement AircraftOverflowAcfFlapsWithVecEQ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_flaps_with_vecEQ", TypeStart_int, UtilConstants.Flag_Yes, "???", "bring in the flaps with thrust vector"); } }
        /// <summary>
        /// bitchin betty 2... for fighters
        /// </summary>
        public static DataRefElement AircraftOverflowAcfWarn2EQ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_warn2EQ", TypeStart_int, UtilConstants.Flag_Yes, "???", "bitchin betty 2... for fighters"); } }
        /// <summary>
        /// number props can be different than number of engines - * This can crash Xplane, use at your own risk.  Not writeable in v10.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfNumThrustpoints
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_num_thrustpoints", TypeStart_int, UtilConstants.Flag_No, "???", "number props can be different than number of engines - * This can crash Xplane, use at your own risk.  Not writeable in v10."); } }
        public static DataRefElement AircraftOverflowAcfCusRndUse
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_use", TypeStart_int50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusRndLoVal
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_lo_val", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusRndHiVal
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_hi_val", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusRndLoAng
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_lo_ang", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusRndHiAng
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_hi_ang", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfHasBeta
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_has_beta", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// mirror the angles on even-number engines on the twins
        /// </summary>
        public static DataRefElement AircraftOverflowAcfCusRndMirror
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_mirror", TypeStart_int50, UtilConstants.Flag_Yes, "???", "mirror the angles on even-number engines on the twins"); } }
        /// <summary>
        /// draw labels on the instruments... many do not label the scales, only draw digital numbers.
        /// </summary>
        public static DataRefElement AircraftOverflowAcfCusRndLabel
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_rnd_label", TypeStart_int50, UtilConstants.Flag_Yes, "???", "draw labels on the instruments... many do not label the scales, only draw digital numbers."); } }
        public static DataRefElement AircraftOverflowAcfCusDigUse
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_dig_use", TypeStart_int50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusDigOffset
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_dig_offset", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusDigScale
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_dig_scale", TypeStart_float50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusDigDig
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_dig_dig", TypeStart_int50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfCusDigDec
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_cus_dig_dec", TypeStart_int50, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfIncAil
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_inc_ail", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfIncAil2
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_inc_ail2", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfIncVec
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_inc_vec", TypeStart_int73_10, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfTowHookY
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tow_hook_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfTowHookZ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_tow_hook_Z", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfWinHookY
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_win_hook_Y", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowAcfWinHookZ
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_win_hook_Z", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// tail wheel spring constant (per degree offset from centered)
        /// </summary>
        public static DataRefElement AircraftOverflowAcfNosewheelK
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_nosewheel_k", TypeStart_float, UtilConstants.Flag_Yes, "???", "tail wheel spring constant (per degree offset from centered)"); } }
        /// <summary>
        /// prop needs offset in z and y from the pivot point... z for V-22, y for motorgliders
        /// </summary>
        public static DataRefElement AircraftOverflowAcfVectarmY
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_vectarmY", TypeStart_float, UtilConstants.Flag_Yes, "???", "prop needs offset in z and y from the pivot point... z for V-22, y for motorgliders"); } }
        /// <summary>
        /// for those motorgliders or prop/jet combos or what have you that hide their props when not in use
        /// </summary>
        public static DataRefElement AircraftOverflowAcfHidePropAt90Vect
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_hide_prop_at_90_vect", TypeStart_int, UtilConstants.Flag_Yes, "???", "for those motorgliders or prop/jet combos or what have you that hide their props when not in use"); } }
        /// <summary>
        /// Aircraft has option to draw from any tank
        /// </summary>
        public static DataRefElement AircraftOverflowAcfHasFuelAll
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_has_fuel_all", TypeStart_int, UtilConstants.Flag_Yes, "???", "Aircraft has option to draw from any tank"); } }
        /// <summary>
        /// Aircraft has Fuel selector
        /// </summary>
        public static DataRefElement AircraftOverflowAcfHasFuelAny
        { get { return GetDataRefElement("sim/aircraft/overflow/acf_has_fuel_any", TypeStart_int, UtilConstants.Flag_Yes, "boolean", ""); } }
        public static DataRefElement AircraftOverflowHasHsi
        { get { return GetDataRefElement("sim/aircraft/overflow/has_hsi", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowHasYawdampBut
        { get { return GetDataRefElement("sim/aircraft/overflow/has_yawdamp_but", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowHasLitemapTex2
        { get { return GetDataRefElement("sim/aircraft/overflow/has_litemap_tex_2", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowHasTransonicAudio
        { get { return GetDataRefElement("sim/aircraft/overflow/has_transonic_audio", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowHasPreRotate
        { get { return GetDataRefElement("sim/aircraft/overflow/has_pre_rotate", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCAltLoPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_alt_lo_PRP", TypeStart_float, UtilConstants.Flag_Yes, "meters", ""); } }
        public static DataRefElement AircraftOverflowSFCHalfLoPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_half_lo_PRP", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCFullLoPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_full_lo_PRP", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCAltHiPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_alt_hi_PRP", TypeStart_float, UtilConstants.Flag_Yes, "meters", ""); } }
        public static DataRefElement AircraftOverflowSFCHalfHiPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_half_hi_PRP", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCFullHiPRP
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_full_hi_PRP", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowFfRatIdlePRP
        { get { return GetDataRefElement("sim/aircraft/overflow/ff_rat_idle_PRP", TypeStart_float, UtilConstants.Flag_Yes, "ratio", ""); } }
        public static DataRefElement AircraftOverflowSFCAltLoJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_alt_lo_JET", TypeStart_float, UtilConstants.Flag_Yes, "meters", ""); } }
        public static DataRefElement AircraftOverflowSFCHalfLoJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_half_lo_JET", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCFullLoJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_full_lo_JET", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCAltHiJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_alt_hi_JET", TypeStart_float, UtilConstants.Flag_Yes, "meters", ""); } }
        public static DataRefElement AircraftOverflowSFCHalfHiJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_half_hi_JET", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowSFCFullHiJET
        { get { return GetDataRefElement("sim/aircraft/overflow/SFC_full_hi_JET", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftOverflowFfRatIdleJET
        { get { return GetDataRefElement("sim/aircraft/overflow/ff_rat_idle_JET", TypeStart_float, UtilConstants.Flag_Yes, "ratio", ""); } }
    }
}
