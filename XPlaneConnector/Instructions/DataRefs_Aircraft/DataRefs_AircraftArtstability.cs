﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement AircraftArtstabilityAcfAshiv
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_AShiV", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAslov
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASloV", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxpLo
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxp_lo", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAspLoRate
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASp_lo_rate", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxpHi
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxp_hi", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAspHiPos
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASp_hi_pos", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxhLo
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxh_lo", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAshLoRate
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASh_lo_rate", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxhHi
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxh_hi", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAshHiPos
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASh_hi_pos", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxrLo
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxr_lo", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsrLoRate
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASr_lo_rate", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsmaxrHi
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASmaxr_hi", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfAsrHiRate
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_ASr_hi_rate", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftArtstabilityAcfHasClutch
        { get { return GetDataRefElement("sim/aircraft/artstability/acf_has_clutch", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
    }
}