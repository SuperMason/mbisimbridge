﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement AircraftEngineAcfNumEngines
        { get { return GetDataRefElement("sim/aircraft/engine/acf_num_engines", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfAutoFeathereq
        { get { return GetDataRefElement("sim/aircraft/engine/acf_auto_featherEQ", TypeStart_int, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Where the prop pitch angle goes when the governor loses oil pressure: 0=low pitch, 1=high pitch, 2=feather, 3=start lock
        /// </summary>
        public static DataRefElement AircraftEngineAcfPropFailMode
        { get { return GetDataRefElement("sim/aircraft/engine/acf_prop_fail_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Where the prop pitch angle goes when the governor loses oil pressure: 0=low pitch, 1=high pitch, 2=feather, 3=start lock"); } }
        public static DataRefElement AircraftEngineAcfThrotmaxFWD
        { get { return GetDataRefElement("sim/aircraft/engine/acf_throtmax_FWD", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfThrotmaxREV
        { get { return GetDataRefElement("sim/aircraft/engine/acf_throtmax_REV", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Minimum engine speed with governor on radians/second
        /// </summary>
        public static DataRefElement AircraftEngineAcfRSCMingovEng
        { get { return GetDataRefElement("sim/aircraft/engine/acf_RSC_mingov_eng", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Minimum engine speed with governor on radians/second"); } }
        /// <summary>
        /// Engine idle speed radians/second.
        /// </summary>
        public static DataRefElement AircraftEngineAcfRSCIdlespeedEng
        { get { return GetDataRefElement("sim/aircraft/engine/acf_RSC_idlespeed_eng", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Engine idle speed radians/second."); } }
        /// <summary>
        /// Max engine speed radians/second.
        /// </summary>
        public static DataRefElement AircraftEngineAcfRSCRedlineEng
        { get { return GetDataRefElement("sim/aircraft/engine/acf_RSC_redline_eng", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "Max engine speed radians/second."); } }
        public static DataRefElement AircraftEngineAcfRSCMingreenEng
        { get { return GetDataRefElement("sim/aircraft/engine/acf_RSC_mingreen_eng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfRSCMaxgreenEng
        { get { return GetDataRefElement("sim/aircraft/engine/acf_RSC_maxgreen_eng", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfPmax
        { get { return GetDataRefElement("sim/aircraft/engine/acf_pmax", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfTmax
        { get { return GetDataRefElement("sim/aircraft/engine/acf_tmax", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfBurnerinc
        { get { return GetDataRefElement("sim/aircraft/engine/acf_burnerinc", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Critical altitude for props
        /// </summary>
        public static DataRefElement AircraftEngineAcfCritalt
        { get { return GetDataRefElement("sim/aircraft/engine/acf_critalt", TypeStart_float, UtilConstants.Flag_Yes, "meters", "Critical altitude for props"); } }
        public static DataRefElement AircraftEngineAcfMpmax
        { get { return GetDataRefElement("sim/aircraft/engine/acf_mpmax", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// This is for backward compatibility, you can use acf_prop_gear_rat from v700 onwards
        /// </summary>
        public static DataRefElement AircraftEngineAcfGearRat
        { get { return GetDataRefElement("sim/aircraft/engine/acf_gear_rat", TypeStart_float, UtilConstants.Flag_Yes, "???", "This is for backward compatibility, you can use acf_prop_gear_rat from v700 onwards"); } }
        public static DataRefElement AircraftEngineAcfFaceJet
        { get { return GetDataRefElement("sim/aircraft/engine/acf_face_jet", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfFaceRocket
        { get { return GetDataRefElement("sim/aircraft/engine/acf_face_rocket", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// This is the delay in increasing the throttle for jet engines - it is the number of seconds to actuate a full advance.
        /// </summary>
        public static DataRefElement AircraftEngineAcfSpooltimeJet
        { get { return GetDataRefElement("sim/aircraft/engine/acf_spooltime_jet", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "This is the delay in increasing the throttle for jet engines - it is the number of seconds to actuate a full advance."); } }
        /// <summary>
        /// This is the delay in increasing the throttle for prop/turboprop engines - it is the number of seconds to actuate a full advance.
        /// </summary>
        public static DataRefElement AircraftEngineAcfSpooltimeProp
        { get { return GetDataRefElement("sim/aircraft/engine/acf_spooltime_prop", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "This is the delay in increasing the throttle for prop/turboprop engines - it is the number of seconds to actuate a full advance."); } }
        /// <summary>
        /// This is the number of seconds it takes for a free turbine to spin up from idle to full RPM.
        /// </summary>
        public static DataRefElement AircraftEngineAcfSpooltimeTurbine
        { get { return GetDataRefElement("sim/aircraft/engine/acf_spooltime_turbine", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "This is the number of seconds it takes for a free turbine to spin up from idle to full RPM."); } }
        /// <summary>
        /// This is the number of seconds it takes for full idle fuel flow to be reached on engine start, for propeller engines
        /// </summary>
        public static DataRefElement AircraftEngineAcfFuelIntroTimeProp
        { get { return GetDataRefElement("sim/aircraft/engine/acf_fuel_intro_time_prop", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "This is the number of seconds it takes for full idle fuel flow to be reached on engine start, for propeller engines"); } }
        /// <summary>
        /// This is the number of seconds it takes for full idle fuel flow to be reached on engine start, for jet engines
        /// </summary>
        public static DataRefElement AircraftEngineFuelIntroTimeJet
        { get { return GetDataRefElement("sim/aircraft/engine/fuel_intro_time_jet", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "This is the number of seconds it takes for full idle fuel flow to be reached on engine start, for jet engines"); } }
        public static DataRefElement AircraftEngineAcfMaxMachEff
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_mach_eff", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfFmaxSl
        { get { return GetDataRefElement("sim/aircraft/engine/acf_fmax_sl", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfFmaxOpt
        { get { return GetDataRefElement("sim/aircraft/engine/acf_fmax_opt", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfFmaxVac
        { get { return GetDataRefElement("sim/aircraft/engine/acf_fmax_vac", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAcfHOpt
        { get { return GetDataRefElement("sim/aircraft/engine/acf_h_opt", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAacfTipMachDes50
        { get { return GetDataRefElement("sim/aircraft/engine/aacf_tip_mach_des_50", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAacfTipMachDes100
        { get { return GetDataRefElement("sim/aircraft/engine/aacf_tip_mach_des_100", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAacfRotorMiRat
        { get { return GetDataRefElement("sim/aircraft/engine/aacf_rotor_mi_rat", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        public static DataRefElement AircraftEngineAacfTipWeight
        { get { return GetDataRefElement("sim/aircraft/engine/aacf_tip_weight", TypeStart_float, UtilConstants.Flag_Yes, "", ""); } }
        /// <summary>
        /// Max internal turbine temperature the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxITT
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_ITT", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max internal turbine temperature the plane can have before engine failure."); } }
        /// <summary>
        /// Max exhaust gas temperature the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxEGT
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_EGT", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max exhaust gas temperature the plane can have before engine failure."); } }
        /// <summary>
        /// Max cylinder head temperature the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxCHT
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_CHT", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max cylinder head temperature the plane can have before engine failure."); } }
        /// <summary>
        /// Max Oil Pressure the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxOILP
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_OILP", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max Oil Pressure the plane can have before engine failure."); } }
        /// <summary>
        /// Max Oil Temperature the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxOILT
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_OILT", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max Oil Temperature the plane can have before engine failure."); } }
        /// <summary>
        /// Oil temperature dataref reads in Celsius
        /// </summary>
        public static DataRefElement AircraftEngineAcfOiltIsC
        { get { return GetDataRefElement("sim/aircraft/engine/acf_oilT_is_C", TypeStart_int, UtilConstants.Flag_No, "boolean", "Oil temperature dataref reads in Celsius"); } }
        /// <summary>
        /// Inter-turbine temperature dataref reads in Celsius
        /// </summary>
        public static DataRefElement AircraftEngineAcfITTIsC
        { get { return GetDataRefElement("sim/aircraft/engine/acf_ITT_is_C", TypeStart_int, UtilConstants.Flag_No, "boolean", "Inter-turbine temperature dataref reads in Celsius"); } }
        /// <summary>
        /// Exhaust gas temperature dataref reads in Celsius
        /// </summary>
        public static DataRefElement AircraftEngineAcfEGTIsC
        { get { return GetDataRefElement("sim/aircraft/engine/acf_EGT_is_C", TypeStart_int, UtilConstants.Flag_No, "boolean", "Exhaust gas temperature dataref reads in Celsius"); } }
        /// <summary>
        /// Cylinder head temperature dataref reads in Celsius
        /// </summary>
        public static DataRefElement AircraftEngineAcfCHTIsC
        { get { return GetDataRefElement("sim/aircraft/engine/acf_CHT_is_C", TypeStart_int, UtilConstants.Flag_No, "boolean", "Cylinder head temperature dataref reads in Celsius"); } }
        /// <summary>
        /// Max Fuel Pressure the plane can have before engine failure.
        /// </summary>
        public static DataRefElement AircraftEngineAcfMaxFUELP
        { get { return GetDataRefElement("sim/aircraft/engine/acf_max_FUELP", TypeStart_float, UtilConstants.Flag_Yes, "???", "Max Fuel Pressure the plane can have before engine failure."); } }
        /// <summary>
        /// This is the ratio of the engine's maximum torque that the starter applies at its design RPM.
        /// </summary>
        public static DataRefElement AircraftEngineAcfStarterTorqueRatio
        { get { return GetDataRefElement("sim/aircraft/engine/acf_starter_torque_ratio", TypeStart_float, UtilConstants.Flag_Yes, "Ratio", "This is the ratio of the engine's maximum torque that the starter applies at its design RPM."); } }
        /// <summary>
        /// This is the ratio of the engine's max RPM that the starter can spin the engine up to before it loses torque.
        /// </summary>
        public static DataRefElement AircraftEngineAcfStarterMaxRpmRatio
        { get { return GetDataRefElement("sim/aircraft/engine/acf_starter_max_rpm_ratio", TypeStart_float, UtilConstants.Flag_Yes, "Ratio", "This is the ratio of the engine's max RPM that the starter can spin the engine up to before it loses torque."); } }
        /// <summary>
        /// Boost Amount
        /// </summary>
        public static DataRefElement AircraftEngineBoostRatio
        { get { return GetDataRefElement("sim/aircraft/engine/boost_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Boost Amount"); } }
        /// <summary>
        /// Boost Capacity
        /// </summary>
        public static DataRefElement AircraftEngineBoostMaxSeconds
        { get { return GetDataRefElement("sim/aircraft/engine/boost_max_seconds", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Boost Capacity"); } }
        /// <summary>
        /// How long the APU door takes to open or close
        /// </summary>
        public static DataRefElement AircraftEngineAcfAPUDoorTime
        { get { return GetDataRefElement("sim/aircraft/engine/acf_APU_door_time", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "How long the APU door takes to open or close"); } }
        /// <summary>
        /// How fast the APU EGT cools down after use - influences how long the APU runs in cool down mode before it is shut down
        /// </summary>
        public static DataRefElement AircraftEngineAcfAPUCooldownTime
        { get { return GetDataRefElement("sim/aircraft/engine/acf_APU_cooldown_time", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "How fast the APU EGT cools down after use - influences how long the APU runs in cool down mode before it is shut down"); } }
        /// <summary>
        /// How long the APU takes to spool up to 100 percent
        /// </summary>
        public static DataRefElement AircraftEngineAcfAPUSpoolupTime
        { get { return GetDataRefElement("sim/aircraft/engine/acf_APU_spoolup_time", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "How long the APU takes to spool up to 100 percent"); } }
        /// <summary>
        /// How long the APU takes to spool down to 10 percent
        /// </summary>
        public static DataRefElement AircraftEngineAcfAPUSpooldnTime
        { get { return GetDataRefElement("sim/aircraft/engine/acf_APU_spooldn_time", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "How long the APU takes to spool down to 10 percent"); } }
    }
}