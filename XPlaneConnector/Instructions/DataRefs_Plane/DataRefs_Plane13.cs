﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane13BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 13 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 13 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 13 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 13 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 13 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_x",
                    Units = "meters",
                    Description = "Plane 13 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_y",
                    Units = "meters",
                    Description = "Plane 13 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_z",
                    Units = "meters",
                    Description = "Plane 13 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_the",
                    Units = "degrees",
                    Description = "Plane 13 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_phi",
                    Units = "degrees",
                    Description = "Plane 13 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_psi",
                    Units = "degrees",
                    Description = "Plane 13 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 13 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 13 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 13 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 13 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 13 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 13 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 13 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_throttle",
                    Units = "ratio",
                    Description = "Plane 13 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 13 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 13 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 13 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_lat",
                    Units = "degs",
                    Description = "Plane 13 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_v_x",
                    Units = "m/s",
                    Description = "Plane 13 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane13VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane13_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
