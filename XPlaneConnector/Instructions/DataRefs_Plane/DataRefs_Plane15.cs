﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane15BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 15 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 15 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 15 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 15 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 15 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_x",
                    Units = "meters",
                    Description = "Plane 15 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_y",
                    Units = "meters",
                    Description = "Plane 15 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_z",
                    Units = "meters",
                    Description = "Plane 15 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_the",
                    Units = "degrees",
                    Description = "Plane 15 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_phi",
                    Units = "degrees",
                    Description = "Plane 15 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_psi",
                    Units = "degrees",
                    Description = "Plane 15 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 15 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 15 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 15 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 15 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 15 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 15 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 15 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_throttle",
                    Units = "ratio",
                    Description = "Plane 15 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 15 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 15 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 15 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_lat",
                    Units = "degs",
                    Description = "Plane 15 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_v_x",
                    Units = "m/s",
                    Description = "Plane 15 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane15VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane15_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
