﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane16BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 16 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 16 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 16 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 16 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 16 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_x",
                    Units = "meters",
                    Description = "Plane 16 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_y",
                    Units = "meters",
                    Description = "Plane 16 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_z",
                    Units = "meters",
                    Description = "Plane 16 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_the",
                    Units = "degrees",
                    Description = "Plane 16 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_phi",
                    Units = "degrees",
                    Description = "Plane 16 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_psi",
                    Units = "degrees",
                    Description = "Plane 16 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 16 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 16 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 16 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 16 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 16 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 16 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 16 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_throttle",
                    Units = "ratio",
                    Description = "Plane 16 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 16 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 16 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 16 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_lat",
                    Units = "degs",
                    Description = "Plane 16 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_v_x",
                    Units = "m/s",
                    Description = "Plane 16 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane16VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane16_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
