﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane18BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 18 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 18 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 18 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 18 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 18 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_x",
                    Units = "meters",
                    Description = "Plane 18 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_y",
                    Units = "meters",
                    Description = "Plane 18 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_z",
                    Units = "meters",
                    Description = "Plane 18 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_the",
                    Units = "degrees",
                    Description = "Plane 18 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_phi",
                    Units = "degrees",
                    Description = "Plane 18 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_psi",
                    Units = "degrees",
                    Description = "Plane 18 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 18 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 18 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 18 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 18 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 18 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 18 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 18 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_throttle",
                    Units = "ratio",
                    Description = "Plane 18 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 18 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 18 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 18 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_lat",
                    Units = "degs",
                    Description = "Plane 18 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_v_x",
                    Units = "m/s",
                    Description = "Plane 18 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane18VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane18_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
