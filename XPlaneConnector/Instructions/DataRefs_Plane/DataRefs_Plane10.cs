﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane10BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 10 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 10 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 10 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 10 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 10 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_x",
                    Units = "meters",
                    Description = "Plane 10 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_y",
                    Units = "meters",
                    Description = "Plane 10 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_z",
                    Units = "meters",
                    Description = "Plane 10 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_the",
                    Units = "degrees",
                    Description = "Plane 10 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_phi",
                    Units = "degrees",
                    Description = "Plane 10 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_psi",
                    Units = "degrees",
                    Description = "Plane 10 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 10 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 10 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 10 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 10 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 10 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 10 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 10 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_throttle",
                    Units = "ratio",
                    Description = "Plane 10 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 10 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 10 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 10 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_lat",
                    Units = "degs",
                    Description = "Plane 10 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_v_x",
                    Units = "m/s",
                    Description = "Plane 10 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane10VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane10_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
