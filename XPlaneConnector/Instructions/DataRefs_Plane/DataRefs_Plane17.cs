﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane17BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 17 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 17 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 17 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 17 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 17 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_x",
                    Units = "meters",
                    Description = "Plane 17 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_y",
                    Units = "meters",
                    Description = "Plane 17 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_z",
                    Units = "meters",
                    Description = "Plane 17 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_the",
                    Units = "degrees",
                    Description = "Plane 17 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_phi",
                    Units = "degrees",
                    Description = "Plane 17 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_psi",
                    Units = "degrees",
                    Description = "Plane 17 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 17 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 17 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 17 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 17 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 17 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 17 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 17 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_throttle",
                    Units = "ratio",
                    Description = "Plane 17 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 17 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 17 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 17 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_lat",
                    Units = "degs",
                    Description = "Plane 17 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_v_x",
                    Units = "m/s",
                    Description = "Plane 17 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane17VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane17_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
