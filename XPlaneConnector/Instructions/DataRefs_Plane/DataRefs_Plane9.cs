﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 9 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9X
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 9 x location"); } }
        /// <summary>
        /// Plane 9 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 9 y location"); } }
        /// <summary>
        /// Plane 9 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 9 z location"); } }
        /// <summary>
        /// Plane 9 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9The
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 9 theta (pitch)"); } }
        /// <summary>
        /// Plane 9 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 9 phi (roll)"); } }
        /// <summary>
        /// Plane 9 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 9 psi (heading)"); } }
        /// <summary>
        /// Plane 9 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 9 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 9 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 9 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 9 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 9 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 9 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 9 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 9 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 9 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 9 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 9 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 9 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 9 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 9 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 9 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9El
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 9 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 9 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 9 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 9 Beacon Light"); } }
        /// <summary>
        /// Plane 9 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 9 Landing Light"); } }
        /// <summary>
        /// Plane 9 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 9 Navigation Light"); } }
        /// <summary>
        /// Plane 9 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 9 Strobe Light"); } }
        /// <summary>
        /// Plane 9 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane9TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane9_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 9 Taxi Lights"); } }

    }
}
