﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane11BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 11 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 11 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 11 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 11 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 11 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_x",
                    Units = "meters",
                    Description = "Plane 11 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_y",
                    Units = "meters",
                    Description = "Plane 11 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_z",
                    Units = "meters",
                    Description = "Plane 11 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_the",
                    Units = "degrees",
                    Description = "Plane 11 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_phi",
                    Units = "degrees",
                    Description = "Plane 11 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_psi",
                    Units = "degrees",
                    Description = "Plane 11 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 11 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 11 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 11 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 11 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 11 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 11 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 11 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_throttle",
                    Units = "ratio",
                    Description = "Plane 11 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 11 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 11 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 11 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_lat",
                    Units = "degs",
                    Description = "Plane 11 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_v_x",
                    Units = "m/s",
                    Description = "Plane 11 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane11VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane11_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
