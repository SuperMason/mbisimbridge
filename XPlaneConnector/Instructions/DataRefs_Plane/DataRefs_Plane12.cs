﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane12BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 12 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 12 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 12 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 12 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 12 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_x",
                    Units = "meters",
                    Description = "Plane 12 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_y",
                    Units = "meters",
                    Description = "Plane 12 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_z",
                    Units = "meters",
                    Description = "Plane 12 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_the",
                    Units = "degrees",
                    Description = "Plane 12 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_phi",
                    Units = "degrees",
                    Description = "Plane 12 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_psi",
                    Units = "degrees",
                    Description = "Plane 12 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 12 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 12 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 12 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 12 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 12 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 12 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 12 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_throttle",
                    Units = "ratio",
                    Description = "Plane 12 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 12 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 12 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 12 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_lat",
                    Units = "degs",
                    Description = "Plane 12 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_v_x",
                    Units = "m/s",
                    Description = "Plane 12 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane12VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane12_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
