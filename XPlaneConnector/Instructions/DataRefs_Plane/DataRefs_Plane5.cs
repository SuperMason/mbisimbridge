﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 5 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5X
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 5 x location"); } }
        /// <summary>
        /// Plane 5 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 5 y location"); } }
        /// <summary>
        /// Plane 5 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 5 z location"); } }
        /// <summary>
        /// Plane 5 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5The
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 5 theta (pitch)"); } }
        /// <summary>
        /// Plane 5 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 5 phi (roll)"); } }
        /// <summary>
        /// Plane 5 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 5 psi (heading)"); } }
        /// <summary>
        /// Plane 5 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 5 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 5 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 5 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 5 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 5 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 5 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 5 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 5 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 5 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 5 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 5 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 5 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 5 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 5 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 5 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5El
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 5 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 5 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 5 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 5 Beacon Light"); } }
        /// <summary>
        /// Plane 5 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 5 Landing Light"); } }
        /// <summary>
        /// Plane 5 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 5 Navigation Light"); } }
        /// <summary>
        /// Plane 5 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 5 Strobe Light"); } }
        /// <summary>
        /// Plane 5 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane5TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane5_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 5 Taxi Lights"); } }

    }
}
