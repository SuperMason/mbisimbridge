﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 6 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6X
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 6 x location"); } }
        /// <summary>
        /// Plane 6 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 6 y location"); } }
        /// <summary>
        /// Plane 6 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 6 z location"); } }
        /// <summary>
        /// Plane 6 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6The
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 6 theta (pitch)"); } }
        /// <summary>
        /// Plane 6 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 6 phi (roll)"); } }
        /// <summary>
        /// Plane 6 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 6 psi (heading)"); } }
        /// <summary>
        /// Plane 6 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 6 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 6 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 6 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 6 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 6 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 6 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 6 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 6 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 6 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 6 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 6 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 6 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 6 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 6 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 6 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6El
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 6 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 6 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 6 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 6 Beacon Light"); } }
        /// <summary>
        /// Plane 6 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 6 Landing Light"); } }
        /// <summary>
        /// Plane 6 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 6 Navigation Light"); } }
        /// <summary>
        /// Plane 6 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 6 Strobe Light"); } }
        /// <summary>
        /// Plane 6 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane6TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane6_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 6 Taxi Lights"); } }

    }
}
