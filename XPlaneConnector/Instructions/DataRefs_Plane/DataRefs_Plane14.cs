﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane14BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 14 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 14 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 14 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 14 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 14 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_x",
                    Units = "meters",
                    Description = "Plane 14 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_y",
                    Units = "meters",
                    Description = "Plane 14 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_z",
                    Units = "meters",
                    Description = "Plane 14 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_the",
                    Units = "degrees",
                    Description = "Plane 14 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_phi",
                    Units = "degrees",
                    Description = "Plane 14 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_psi",
                    Units = "degrees",
                    Description = "Plane 14 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 14 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 14 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 14 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 14 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 14 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 14 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 14 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_throttle",
                    Units = "ratio",
                    Description = "Plane 14 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 14 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 14 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 14 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_lat",
                    Units = "degs",
                    Description = "Plane 14 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_v_x",
                    Units = "m/s",
                    Description = "Plane 14 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane14VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane14_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
