﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 4 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4X
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 4 x location"); } }
        /// <summary>
        /// Plane 4 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 4 y location"); } }
        /// <summary>
        /// Plane 4 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 4 z location"); } }
        /// <summary>
        /// Plane 4 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4The
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 4 theta (pitch)"); } }
        /// <summary>
        /// Plane 4 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 4 phi (roll)"); } }
        /// <summary>
        /// Plane 4 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 4 psi (heading)"); } }
        /// <summary>
        /// Plane 4 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 4 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 4 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 4 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 4 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 4 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 4 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 4 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 4 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 4 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 4 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 4 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 4 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 4 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 4 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 4 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4El
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 4 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 4 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 4 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 4 Beacon Light"); } }
        /// <summary>
        /// Plane 4 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 4 Landing Light"); } }
        /// <summary>
        /// Plane 4 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 4 Navigation Light"); } }
        /// <summary>
        /// Plane 4 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 4 Strobe Light"); } }
        /// <summary>
        /// Plane 4 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane4TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane4_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 4 Taxi Lights"); } }

    }
}
