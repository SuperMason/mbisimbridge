﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement MultiplayerPositionPlane19BeaconLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_beacon_lights_on",
                    Units = "bool",
                    Description = "Plane 19 Beacon Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19LandingLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_landing_lights_on",
                    Units = "bool",
                    Description = "Plane 19 Landing Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19NavLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_nav_lights_on",
                    Units = "bool",
                    Description = "Plane 19 Navigation Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19StrobeLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_strobe_lights_on",
                    Units = "bool",
                    Description = "Plane 19 Strobe Light",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19TaxiLightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_taxi_light_on",
                    Units = "bool",
                    Description = "Plane 19 Taxi Lights",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19X
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_x",
                    Units = "meters",
                    Description = "Plane 19 x location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Y
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_y",
                    Units = "meters",
                    Description = "Plane 19 y location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Z
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_z",
                    Units = "meters",
                    Description = "Plane 19 z location",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19The
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_the",
                    Units = "degrees",
                    Description = "Plane 19 theta (pitch)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Phi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_phi",
                    Units = "degrees",
                    Description = "Plane 19 phi (roll)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Psi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_psi",
                    Units = "degrees",
                    Description = "Plane 19 psi (heading)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19GearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_gear_deploy",
                    Units = "ratio",
                    Description = "Plane 19 gear deployment for 6 gear. 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19FlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_flap_ratio",
                    Units = "ratio",
                    Description = "Plane 19 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19FlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_flap_ratio2",
                    Units = "ratio",
                    Description = "Plane 19 flap lowering 0 = clean, 1 = max flaps",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19SpoilerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_spoiler_ratio",
                    Units = "ratio",
                    Description = "Plane 19 spoiler ratio (0 = clean, 1 = max spoilers)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19SpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_speedbrake_ratio",
                    Units = "ratio",
                    Description = "Plane 19 speed brake ratio (0 = clean, 1 = max speed brakes)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Sla1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_sla1_ratio",
                    Units = "ratio",
                    Description = "Plane 19 slat deployment ratio 0 = clean, 1 = max slats",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19WingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_wing_sweep",
                    Units = "ratio",
                    Description = "Plane 19 wing sweep, 0 = normal, 1 = most sweep",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Throttle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_throttle",
                    Units = "ratio",
                    Description = "Plane 19 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19YolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_yolk_pitch",
                    Units = "ratio",
                    Description = "Plane 19 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19YolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_yolk_roll",
                    Units = "ratio",
                    Description = "Plane 19 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19YolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_yolk_yaw",
                    Units = "ratio",
                    Description = "Plane 19 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_lat",
                    Units = "degs",
                    Description = "Plane 19 Lat lon and elevation.  NOTE: your plugin must set the plane's",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_lon",
                    Units = "degs",
                    Description = "position by writing x, y and z.  Also if another plugin is updating plane",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19El
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_el",
                    Units = "meters",
                    Description = "position then these will not be accurate unless the plane updates them.",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19VX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_v_x",
                    Units = "m/s",
                    Description = "Plane 19 cartesian velocities.  These may not be accurate if another plugin",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19VY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_v_y",
                    Units = "m/s",
                    Description = "is controlling the plane andn ot updating this data.  You cannot use these to",
                    
                };
            }
        }
        public static DataRefElement MultiplayerPositionPlane19VZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/multiplayer/position/plane19_v_z",
                    Units = "m/s",
                    Description = "manipulate the plane.",
                    
                };
            }
        }
    }
}
