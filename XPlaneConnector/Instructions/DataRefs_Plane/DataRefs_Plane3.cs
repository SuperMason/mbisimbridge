﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 3 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3X
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 3 x location"); } }
        /// <summary>
        /// Plane 3 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 3 y location"); } }
        /// <summary>
        /// Plane 3 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 3 z location"); } }
        /// <summary>
        /// Plane 3 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3The
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 3 theta (pitch)"); } }
        /// <summary>
        /// Plane 3 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 3 phi (roll)"); } }
        /// <summary>
        /// Plane 3 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 3 psi (heading)"); } }
        /// <summary>
        /// Plane 3 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 3 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 3 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 3 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 3 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 3 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 3 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 3 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 3 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 3 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 3 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 3 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 3 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 3 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 3 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 3 Lat lon and elevation.  NOTE: your plugin must set the plane's");  } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3El
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 3 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 3 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 3 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 3 Beacon Light"); } }
        /// <summary>
        /// Plane 3 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 3 Landing Light"); } }
        /// <summary>
        /// Plane 3 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 3 Navigation Light"); } }
        /// <summary>
        /// Plane 3 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 3 Strobe Light"); } }
        /// <summary>
        /// Plane 3 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane3TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane3_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 3 Taxi Lights"); } }

    }
}
