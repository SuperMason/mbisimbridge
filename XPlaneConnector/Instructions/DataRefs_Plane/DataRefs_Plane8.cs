﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 8 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8X
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 8 x location"); } }
        /// <summary>
        /// Plane 8 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 8 y location"); } }
        /// <summary>
        /// Plane 8 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 8 z location"); } }
        /// <summary>
        /// Plane 8 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8The
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 8 theta (pitch)"); } }
        /// <summary>
        /// Plane 8 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 8 phi (roll)"); } }
        /// <summary>
        /// Plane 8 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 8 psi (heading)"); } }
        /// <summary>
        /// Plane 8 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 8 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 8 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 8 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 8 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 8 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 8 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 8 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 8 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 8 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 8 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 8 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 8 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 8 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 8 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 8 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8El
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 8 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 8 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 8 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 8 Beacon Light"); } }
        /// <summary>
        /// Plane 8 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 8 Landing Light"); } }
        /// <summary>
        /// Plane 8 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 8 Navigation Light"); } }
        /// <summary>
        /// Plane 8 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 8 Strobe Light"); } }
        /// <summary>
        /// Plane 8 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane8TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane8_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 8 Taxi Lights"); } }

    }
}
