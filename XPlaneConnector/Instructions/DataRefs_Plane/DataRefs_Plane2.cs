﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 2 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2X
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 2 x location"); } }
        /// <summary>
        /// Plane 2 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 2 y location"); } }
        /// <summary>
        /// Plane 2 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 2 z location"); } }
        /// <summary>
        /// Plane 2 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2The
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 2 theta (pitch)"); } }
        /// <summary>
        /// Plane 2 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 2 phi (roll)"); } }
        /// <summary>
        /// Plane 2 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 2 psi (heading)"); } }
        /// <summary>
        /// Plane 2 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 2 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 2 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 2 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 2 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 2 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 2 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 2 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 2 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 2 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 2 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 2 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 2 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 2 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 2 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 2 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2El
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 2 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 2 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 2 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 2 Beacon Light"); } }
        /// <summary>
        /// Plane 2 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 2 Landing Light"); } }
        /// <summary>
        /// Plane 2 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 2 Navigation Light"); } }
        /// <summary>
        /// Plane 2 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 2 Strobe Light"); } }
        /// <summary>
        /// Plane 2 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane2TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane2_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 2 Taxi Lights"); } }

    }
}
