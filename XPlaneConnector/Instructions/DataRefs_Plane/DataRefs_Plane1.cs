﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 1 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1X
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 1 x location"); } }
        /// <summary>
        /// Plane 1 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 1 y location"); } }
        /// <summary>
        /// Plane 1 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 1 z location"); } }
        /// <summary>
        /// Plane 1 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1The
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 1 theta (pitch)"); } }
        /// <summary>
        /// Plane 1 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 1 phi (roll)"); } }
        /// <summary>
        /// Plane 1 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 1 psi (heading)"); } }
        /// <summary>
        /// Plane 1 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 1 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 1 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 1 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 1 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 1 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 1 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 1 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 1 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 1 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 1 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 1 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 1 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 1 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 1 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 1 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1El
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 1 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 1 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 1 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 1 Beacon Light"); } }
        /// <summary>
        /// Plane 1 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 1 Landing Light"); } }
        /// <summary>
        /// Plane 1 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 1 Navigation Light"); } }
        /// <summary>
        /// Plane 1 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 1 Strobe Light"); } }
        /// <summary>
        /// Plane 1 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane1TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane1_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 1 Taxi Lights"); } }
    }
}