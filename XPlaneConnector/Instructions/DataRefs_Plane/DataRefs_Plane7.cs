﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Plane 7 x location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7X
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 7 x location"); } }
        /// <summary>
        /// Plane 7 y location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Y
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 7 y location"); } }
        /// <summary>
        /// Plane 7 z location
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Z
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "Plane 7 z location"); } }
        /// <summary>
        /// Plane 7 theta (pitch)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7The
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_the", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 7 theta (pitch)"); } }
        /// <summary>
        /// Plane 7 phi (roll)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Phi
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 7 phi (roll)"); } }
        /// <summary>
        /// Plane 7 psi (heading)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Psi
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Plane 7 psi (heading)"); } }
        /// <summary>
        /// Plane 7 gear deployment for 6 gear. 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7GearDeploy
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_gear_deploy", TypeStart_float10, UtilConstants.Flag_Yes, "ratio", "Plane 7 gear deployment for 6 gear. 0 = up, 1 = down"); } }
        /// <summary>
        /// Plane 7 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7FlapRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 7 flap lowering 0 = clean, 1 = max flaps
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7FlapRatio2
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_flap_ratio2", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 flap lowering 0 = clean, 1 = max flaps"); } }
        /// <summary>
        /// Plane 7 spoiler ratio (0 = clean, 1 = max spoilers)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7SpoilerRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_spoiler_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 spoiler ratio (0 = clean, 1 = max spoilers)"); } }
        /// <summary>
        /// Plane 7 speed brake ratio (0 = clean, 1 = max speed brakes)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7SpeedbrakeRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 speed brake ratio (0 = clean, 1 = max speed brakes)"); } }
        /// <summary>
        /// Plane 7 slat deployment ratio 0 = clean, 1 = max slats
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7SlatRatio
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_slat_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 slat deployment ratio 0 = clean, 1 = max slats"); } }
        /// <summary>
        /// Plane 7 wing sweep, 0 = normal, 1 = most sweep
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7WingSweep
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_wing_sweep", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 wing sweep, 0 = normal, 1 = most sweep"); } }
        /// <summary>
        /// Plane 7 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Throttle
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_throttle", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Plane 7 Percent of max throttle per 8 engines (0 = none, 1 = full fwd, -1 = full reverse)"); } }
        /// <summary>
        /// Plane 7 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7YolkPitch
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_yolk_pitch", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 Commanded pitch (Legacy for compatibility - use sim/multiplayer/controls/yoke_pitch_ratio)"); } }
        /// <summary>
        /// Plane 7 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7YolkRoll
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_yolk_roll", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 Commanded roll (Legacy for compatibility - use sim/multiplayer/controls/yoke_roll_ratio)"); } }
        /// <summary>
        /// Plane 7 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7YolkYaw
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_yolk_yaw", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Plane 7 Commanded yaw (Legacy for compatibility - use sim/multiplayer/controls/yoke_heading_ratio)"); } }
        /// <summary>
        /// Plane 7 Lat lon and elevation.  NOTE: your plugin must set the plane's
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Lat
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_lat", TypeStart_double, UtilConstants.Flag_No, "degs", "Plane 7 Lat lon and elevation.  NOTE: your plugin must set the plane's"); } }
        /// <summary>
        /// position by writing x, y and z.  Also if another plugin is updating plane
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7Lon
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_lon", TypeStart_double, UtilConstants.Flag_No, "degs", "position by writing x, y and z.  Also if another plugin is updating plane"); } }
        /// <summary>
        /// position then these will not be accurate unless the plane updates them.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7El
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_el", TypeStart_double, UtilConstants.Flag_No, "meters", "position then these will not be accurate unless the plane updates them."); } }
        /// <summary>
        /// Plane 7 cartesian velocities.  These may not be accurate if another plugin
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7VX
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_v_x", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "Plane 7 cartesian velocities.  These may not be accurate if another plugin"); } }
        /// <summary>
        /// is controlling the plane andn ot updating this data.  You cannot use these to
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7VY
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_v_y", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "is controlling the plane andn ot updating this data.  You cannot use these to"); } }
        /// <summary>
        /// manipulate the plane.
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7VZ
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_v_z", TypeStart_float, UtilConstants.Flag_Yes, "m/s", "manipulate the plane."); } }
        /// <summary>
        /// Plane 7 Beacon Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7BeaconLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 7 Beacon Light"); } }
        /// <summary>
        /// Plane 7 Landing Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7LandingLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 7 Landing Light"); } }
        /// <summary>
        /// Plane 7 Navigation Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7NavLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 7 Navigation Light"); } }
        /// <summary>
        /// Plane 7 Strobe Light
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7StrobeLightsOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 7 Strobe Light"); } }
        /// <summary>
        /// Plane 7 Taxi Lights
        /// </summary>
        public static DataRefElement MultiplayerPositionPlane7TaxiLightOn
        { get { return GetDataRefElement("sim/multiplayer/position/plane7_taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Plane 7 Taxi Lights"); } }

    }
}
