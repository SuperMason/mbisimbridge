﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand FlightControlsCowlFlapsOpen { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open", "Move cowl flaps open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed", "Move cowl flaps to closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed1 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_1", "Move cowl flaps #1 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed2 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_2", "Move cowl flaps #2 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed3 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_3", "Move cowl flaps #3 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed4 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_4", "Move cowl flaps #4 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed5 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_5", "Move cowl flaps #5 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed6 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_6", "Move cowl flaps #6 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed7 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_7", "Move cowl flaps #7 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsClosed8 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_closed_8", "Move cowl flaps #8 closed a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen1 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_1", "Move cowl flaps #1 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen2 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_2", "Move cowl flaps #2 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen3 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_3", "Move cowl flaps #3 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen4 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_4", "Move cowl flaps #4 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen5 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_5", "Move cowl flaps #5 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen6 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_6", "Move cowl flaps #6 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen7 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_7", "Move cowl flaps #7 open a bit."); } }
        public static XPlaneCommand FlightControlsCowlFlapsOpen8 { get { return GetXPlaneCommand("sim/flight_controls/cowl_flaps_open_8", "Move cowl flaps #8 open a bit."); } }
        public static XPlaneCommand FlightControlsFlapsUp { get { return GetXPlaneCommand("sim/flight_controls/flaps_up", "Flaps up a notch."); } }
        public static XPlaneCommand FlightControlsFlapsDown { get { return GetXPlaneCommand("sim/flight_controls/flaps_down", "Flaps down a notch."); } }
        public static XPlaneCommand FlightControlsVectorSweepAft { get { return GetXPlaneCommand("sim/flight_controls/vector_sweep_aft", "Vector or sweep aft."); } }
        public static XPlaneCommand FlightControlsVectorSweepForward { get { return GetXPlaneCommand("sim/flight_controls/vector_sweep_forward", "Vector or sweep forward."); } }
        public static XPlaneCommand FlightControlsBlimpLiftDown { get { return GetXPlaneCommand("sim/flight_controls/blimp_lift_down", "Blimp lift down a bit."); } }
        public static XPlaneCommand FlightControlsBlimpLiftUp { get { return GetXPlaneCommand("sim/flight_controls/blimp_lift_up", "Blimp lift up a bit."); } }
        public static XPlaneCommand FlightControlsSpeedBrakesDownOne { get { return GetXPlaneCommand("sim/flight_controls/speed_brakes_down_one", "Speedbrakes extend one."); } }
        public static XPlaneCommand FlightControlsSpeedBrakesUpOne { get { return GetXPlaneCommand("sim/flight_controls/speed_brakes_up_one", "Speedbrakes retract one."); } }
        public static XPlaneCommand FlightControlsSpeedBrakesDownAll { get { return GetXPlaneCommand("sim/flight_controls/speed_brakes_down_all", "Speedbrakes extend full."); } }
        public static XPlaneCommand FlightControlsSpeedBrakesUpAll { get { return GetXPlaneCommand("sim/flight_controls/speed_brakes_up_all", "Speedbrakes retract full."); } }
        public static XPlaneCommand FlightControlsSpeedBrakesToggle { get { return GetXPlaneCommand("sim/flight_controls/speed_brakes_toggle", "Speedbrakes toggle."); } }
        public static XPlaneCommand FlightControlsLandingGearDown { get { return GetXPlaneCommand("sim/flight_controls/landing_gear_down", "Landing gear down."); } }
        public static XPlaneCommand FlightControlsLandingGearUp { get { return GetXPlaneCommand("sim/flight_controls/landing_gear_up", "Landing gear up."); } }
        public static XPlaneCommand FlightControlsLandingGearToggle { get { return GetXPlaneCommand("sim/flight_controls/landing_gear_toggle", "Landing gear toggle."); } }
        public static XPlaneCommand FlightControlsLandingGearEmerOn { get { return GetXPlaneCommand("sim/flight_controls/landing_gear_emer_on", "Landing gear emergency override down."); } }
        public static XPlaneCommand FlightControlsLandingGearEmerOff { get { return GetXPlaneCommand("sim/flight_controls/landing_gear_emer_off", "Landing gear emergency override off."); } }
        public static XPlaneCommand FlightControlsNwheelSteerToggle { get { return GetXPlaneCommand("sim/flight_controls/nwheel_steer_toggle", "Nosewheel steer toggle."); } }
        public static XPlaneCommand FlightControlsTailWheelLockToggle { get { return GetXPlaneCommand("sim/flight_controls/tail_wheel_lock_toggle", "Toggle tailwheel lock."); } }
        public static XPlaneCommand FlightControlsTailWheelLockEngage { get { return GetXPlaneCommand("sim/flight_controls/tail_wheel_lock_engage", "Engage tailwheel lock."); } }
        public static XPlaneCommand FlightControlsWaterRudderDown { get { return GetXPlaneCommand("sim/flight_controls/water_rudder_down", "Water rudder down (engage)."); } }
        public static XPlaneCommand FlightControlsWaterRudderUp { get { return GetXPlaneCommand("sim/flight_controls/water_rudder_up", "Water rudder up (disengage)."); } }
        public static XPlaneCommand FlightControlsWaterRudderToggle { get { return GetXPlaneCommand("sim/flight_controls/water_rudder_toggle", "Toggle water rudder."); } }
        public static XPlaneCommand FlightControlsLeftBrake { get { return GetXPlaneCommand("sim/flight_controls/left_brake", "Hold brake left."); } }
        public static XPlaneCommand FlightControlsRightBrake { get { return GetXPlaneCommand("sim/flight_controls/right_brake", "Hold brake right."); } }
        public static XPlaneCommand FlightControlsBrakesToggleRegular { get { return GetXPlaneCommand("sim/flight_controls/brakes_toggle_regular", "Toggle brakes regular effort."); } }
        public static XPlaneCommand FlightControlsBrakesToggleMax { get { return GetXPlaneCommand("sim/flight_controls/brakes_toggle_max", "Toggle brakes max effort."); } }
        public static XPlaneCommand FlightControlsBrakesRegular { get { return GetXPlaneCommand("sim/flight_controls/brakes_regular", "Hold brakes regular."); } }
        public static XPlaneCommand FlightControlsBrakesMax { get { return GetXPlaneCommand("sim/flight_controls/brakes_max", "Hold brakes maximum."); } }
        public static XPlaneCommand FlightControlsBrakesToggleAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_toggle_auto", "Toggle auto-brakes."); } }
        public static XPlaneCommand FlightControlsBrakesDnAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_dn_auto", "Auto-brakes down."); } }
        public static XPlaneCommand FlightControlsBrakesUpAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_up_auto", "Auto-brakes up."); } }
        public static XPlaneCommand FlightControlsBrakesOffAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_off_auto", "Auto-brakes off/disarm."); } }
        public static XPlaneCommand FlightControlsBrakesRtoAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_rto_auto", "Auto-brakes RTO."); } }
        public static XPlaneCommand FlightControlsBrakes1Auto { get { return GetXPlaneCommand("sim/flight_controls/brakes_1_auto", "Auto-brakes 1."); } }
        public static XPlaneCommand FlightControlsBrakes2Auto { get { return GetXPlaneCommand("sim/flight_controls/brakes_2_auto", "Auto-brakes 2."); } }
        public static XPlaneCommand FlightControlsBrakes3Auto { get { return GetXPlaneCommand("sim/flight_controls/brakes_3_auto", "Auto-brakes 3."); } }
        public static XPlaneCommand FlightControlsBrakesMaxAuto { get { return GetXPlaneCommand("sim/flight_controls/brakes_max_auto", "Auto-brakes max."); } }
        public static XPlaneCommand FlightControlsHydraulicOn { get { return GetXPlaneCommand("sim/flight_controls/hydraulic_on", "Engine-driven hydraulic pumps on"); } }
        public static XPlaneCommand FlightControlsHydraulicOff { get { return GetXPlaneCommand("sim/flight_controls/hydraulic_off", "Engine-driven hydraulic pumps off"); } }
        public static XPlaneCommand FlightControlsHydraulicTog { get { return GetXPlaneCommand("sim/flight_controls/hydraulic_tog", "Engine-driven hydraulic pumps tog"); } }
        public static XPlaneCommand FlightControlsTailhookDown { get { return GetXPlaneCommand("sim/flight_controls/tailhook_down", "Tailhook down."); } }
        public static XPlaneCommand FlightControlsTailhookUp { get { return GetXPlaneCommand("sim/flight_controls/tailhook_up", "Tailhook up."); } }
        public static XPlaneCommand FlightControlsTailhookToggle { get { return GetXPlaneCommand("sim/flight_controls/tailhook_toggle", "Toggle the tailhook."); } }
        public static XPlaneCommand FlightControlsCanopyOpen { get { return GetXPlaneCommand("sim/flight_controls/canopy_open", "Canopy open."); } }
        public static XPlaneCommand FlightControlsCanopyClose { get { return GetXPlaneCommand("sim/flight_controls/canopy_close", "Canopy close."); } }
        public static XPlaneCommand FlightControlsCanopyToggle { get { return GetXPlaneCommand("sim/flight_controls/canopy_toggle", "Canopy toggle."); } }
        public static XPlaneCommand FlightControlsRotorBrakeToggle { get { return GetXPlaneCommand("sim/flight_controls/rotor_brake_toggle", "Toggle rotor brake."); } }
        public static XPlaneCommand FlightControlsHotelModeToggle { get { return GetXPlaneCommand("sim/flight_controls/hotel_mode_toggle", "Toggle hotel mode."); } }
        public static XPlaneCommand FlightControlsPuffersToggle { get { return GetXPlaneCommand("sim/flight_controls/puffers_toggle", "Toggle puffers."); } }
        public static XPlaneCommand FlightControlsParachuteFlares { get { return GetXPlaneCommand("sim/flight_controls/parachute_flares", "Deploy parachute flares."); } }
        public static XPlaneCommand FlightControlsSmokeToggle { get { return GetXPlaneCommand("sim/flight_controls/smoke_toggle", "Toggle smoke puffing."); } }
        public static XPlaneCommand FlightControlsWaterScoopToggle { get { return GetXPlaneCommand("sim/flight_controls/water_scoop_toggle", "Toggle water scoop."); } }
        public static XPlaneCommand FlightControlsBoost { get { return GetXPlaneCommand("sim/flight_controls/boost", "Water or Nitrous engine BOOST."); } }
        public static XPlaneCommand FlightControlsIgniteJato { get { return GetXPlaneCommand("sim/flight_controls/ignite_jato", "Ignite JATO."); } }
        public static XPlaneCommand FlightControlsJettisonPayload { get { return GetXPlaneCommand("sim/flight_controls/jettison_payload", "Jettison the payload."); } }
        public static XPlaneCommand FlightControlsDumpFuelOn { get { return GetXPlaneCommand("sim/flight_controls/dump_fuel_on", "Dump fuel on."); } }
        public static XPlaneCommand FlightControlsDumpFuelOff { get { return GetXPlaneCommand("sim/flight_controls/dump_fuel_off", "Dump fuel off."); } }
        public static XPlaneCommand FlightControlsDumpFuelToggle { get { return GetXPlaneCommand("sim/flight_controls/dump_fuel_toggle", "Dump fuel toggle."); } }
        public static XPlaneCommand FlightControlsDeployParachute { get { return GetXPlaneCommand("sim/flight_controls/deploy_parachute", "Deploy/jettison chute."); } }
        public static XPlaneCommand FlightControlsEject { get { return GetXPlaneCommand("sim/flight_controls/eject", "Eject."); } }
        public static XPlaneCommand FlightControlsDropTank { get { return GetXPlaneCommand("sim/flight_controls/drop_tank", "Drop all drop tanks."); } }
        public static XPlaneCommand FlightControlsDoorOpen1 { get { return GetXPlaneCommand("sim/flight_controls/door_open_1", "Door #1 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen2 { get { return GetXPlaneCommand("sim/flight_controls/door_open_2", "Door #2 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen3 { get { return GetXPlaneCommand("sim/flight_controls/door_open_3", "Door #3 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen4 { get { return GetXPlaneCommand("sim/flight_controls/door_open_4", "Door #4 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen5 { get { return GetXPlaneCommand("sim/flight_controls/door_open_5", "Door #5 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen6 { get { return GetXPlaneCommand("sim/flight_controls/door_open_6", "Door #6 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen7 { get { return GetXPlaneCommand("sim/flight_controls/door_open_7", "Door #7 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen8 { get { return GetXPlaneCommand("sim/flight_controls/door_open_8", "Door #8 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen9 { get { return GetXPlaneCommand("sim/flight_controls/door_open_9", "Door #9 open."); } }
        public static XPlaneCommand FlightControlsDoorOpen10 { get { return GetXPlaneCommand("sim/flight_controls/door_open_10", "Door #10 open."); } }
        public static XPlaneCommand FlightControlsDoorClose1 { get { return GetXPlaneCommand("sim/flight_controls/door_close_1", "Door #1 close."); } }
        public static XPlaneCommand FlightControlsDoorClose2 { get { return GetXPlaneCommand("sim/flight_controls/door_close_2", "Door #2 close."); } }
        public static XPlaneCommand FlightControlsDoorClose3 { get { return GetXPlaneCommand("sim/flight_controls/door_close_3", "Door #3 close."); } }
        public static XPlaneCommand FlightControlsDoorClose4 { get { return GetXPlaneCommand("sim/flight_controls/door_close_4", "Door #4 close."); } }
        public static XPlaneCommand FlightControlsDoorClose5 { get { return GetXPlaneCommand("sim/flight_controls/door_close_5", "Door #5 close."); } }
        public static XPlaneCommand FlightControlsDoorClose6 { get { return GetXPlaneCommand("sim/flight_controls/door_close_6", "Door #6 close."); } }
        public static XPlaneCommand FlightControlsDoorClose7 { get { return GetXPlaneCommand("sim/flight_controls/door_close_7", "Door #7 close."); } }
        public static XPlaneCommand FlightControlsDoorClose8 { get { return GetXPlaneCommand("sim/flight_controls/door_close_8", "Door #8 close."); } }
        public static XPlaneCommand FlightControlsDoorClose9 { get { return GetXPlaneCommand("sim/flight_controls/door_close_9", "Door #9 close."); } }
        public static XPlaneCommand FlightControlsDoorClose10 { get { return GetXPlaneCommand("sim/flight_controls/door_close_10", "Door #10 close."); } }
        public static XPlaneCommand FlightControlsGliderTowRelease { get { return GetXPlaneCommand("sim/flight_controls/glider_tow_release", "Release tow-plane cable for gliders."); } }
        public static XPlaneCommand FlightControlsWinchRelease { get { return GetXPlaneCommand("sim/flight_controls/winch_release", "Release winch (for gliders)."); } }
        public static XPlaneCommand FlightControlsGliderAllRelease { get { return GetXPlaneCommand("sim/flight_controls/glider_all_release", "Release tow-plane and winch for gliders."); } }
        public static XPlaneCommand FlightControlsEngageCatShot { get { return GetXPlaneCommand("sim/flight_controls/engage_cat_shot", "Engage catapault."); } }
        public static XPlaneCommand FlightControlsGliderTowLeft { get { return GetXPlaneCommand("sim/flight_controls/glider_tow_left", "Tow-plane for gliders: Take me left."); } }
        public static XPlaneCommand FlightControlsGliderTowStraight { get { return GetXPlaneCommand("sim/flight_controls/glider_tow_straight", "Tow-plane for gliders: Take me straight."); } }
        public static XPlaneCommand FlightControlsGliderTowRight { get { return GetXPlaneCommand("sim/flight_controls/glider_tow_right", "Tow-plane for gliders: Take me right."); } }
        public static XPlaneCommand FlightControlsWinchFaster { get { return GetXPlaneCommand("sim/flight_controls/winch_faster", "Winch faster. (for gliders)."); } }
        public static XPlaneCommand FlightControlsWinchSlower { get { return GetXPlaneCommand("sim/flight_controls/winch_slower", "Winch slower. (for gliders)."); } }
        public static XPlaneCommand FlightControlsCarrierILS { get { return GetXPlaneCommand("sim/flight_controls/carrier_ILS", "Set the radios for the carrier ILS."); } }
        public static XPlaneCommand FlightControlsPumpFlaps { get { return GetXPlaneCommand("sim/flight_controls/pump_flaps", "Pump flaps up/down."); } }
        public static XPlaneCommand FlightControlsPumpGear { get { return GetXPlaneCommand("sim/flight_controls/pump_gear", "Pump gear up/down."); } }
        public static XPlaneCommand FlightControlsPitchTrimaUp { get { return GetXPlaneCommand("sim/flight_controls/pitch_trimA_up", "Pitch trim A up."); } }
        public static XPlaneCommand FlightControlsPitchTrimaDown { get { return GetXPlaneCommand("sim/flight_controls/pitch_trimA_down", "Pitch trim A down."); } }
        public static XPlaneCommand FlightControlsPitchTrimbUp { get { return GetXPlaneCommand("sim/flight_controls/pitch_trimB_up", "Pitch trim B up."); } }
        public static XPlaneCommand FlightControlsPitchTrimbDown { get { return GetXPlaneCommand("sim/flight_controls/pitch_trimB_down", "Pitch trim B down."); } }
        public static XPlaneCommand FlightControlsPitchTrimUp { get { return GetXPlaneCommand("sim/flight_controls/pitch_trim_up", "Pitch trim up."); } }
        public static XPlaneCommand FlightControlsPitchTrimDown { get { return GetXPlaneCommand("sim/flight_controls/pitch_trim_down", "Pitch trim down."); } }
        public static XPlaneCommand FlightControlsPitchTrimUpMech { get { return GetXPlaneCommand("sim/flight_controls/pitch_trim_up_mech", "Pitch trim up - Mechanical, not servo."); } }
        public static XPlaneCommand FlightControlsPitchTrimDownMech { get { return GetXPlaneCommand("sim/flight_controls/pitch_trim_down_mech", "Pitch trim down - Mechanical, not servo."); } }
        public static XPlaneCommand FlightControlsAileronTrimLeft { get { return GetXPlaneCommand("sim/flight_controls/aileron_trim_left", "Roll trim left."); } }
        public static XPlaneCommand FlightControlsAileronTrimRight { get { return GetXPlaneCommand("sim/flight_controls/aileron_trim_right", "Roll trim right."); } }
        public static XPlaneCommand FlightControlsRudderTrimLeft { get { return GetXPlaneCommand("sim/flight_controls/rudder_trim_left", "Yaw trim left."); } }
        public static XPlaneCommand FlightControlsRudderTrimRight { get { return GetXPlaneCommand("sim/flight_controls/rudder_trim_right", "Yaw trim right."); } }
        public static XPlaneCommand FlightControlsGyroRotorTrimUp { get { return GetXPlaneCommand("sim/flight_controls/gyro_rotor_trim_up", "Gyro rotor trim up."); } }
        public static XPlaneCommand FlightControlsGyroRotorTrimDown { get { return GetXPlaneCommand("sim/flight_controls/gyro_rotor_trim_down", "Gyro rotor trim down."); } }
        public static XPlaneCommand FlightControlsRotorRpmTrimUp { get { return GetXPlaneCommand("sim/flight_controls/rotor_rpm_trim_up", "Rotor RPM trim up."); } }
        public static XPlaneCommand FlightControlsRotorRpmTrimDown { get { return GetXPlaneCommand("sim/flight_controls/rotor_rpm_trim_down", "Rotor RPM trim down."); } }
        public static XPlaneCommand FlightControlsMagneticLock { get { return GetXPlaneCommand("sim/flight_controls/magnetic_lock", "Controls magnetic lock."); } }
        public static XPlaneCommand FlightControlsPitchTrimTakeoff { get { return GetXPlaneCommand("sim/flight_controls/pitch_trim_takeoff", "Pitch trim takeoff."); } }
        public static XPlaneCommand FlightControlsAileronTrimCenter { get { return GetXPlaneCommand("sim/flight_controls/aileron_trim_center", "Aileron trim center."); } }
        public static XPlaneCommand FlightControlsRudderTrimCenter { get { return GetXPlaneCommand("sim/flight_controls/rudder_trim_center", "Rudder trim center."); } }
        public static XPlaneCommand FlightControlsRudderLft { get { return GetXPlaneCommand("sim/flight_controls/rudder_lft", "Rudder left."); } }
        public static XPlaneCommand FlightControlsRudderCtr { get { return GetXPlaneCommand("sim/flight_controls/rudder_ctr", "Rudder center."); } }
        public static XPlaneCommand FlightControlsRudderRgt { get { return GetXPlaneCommand("sim/flight_controls/rudder_rgt", "Rudder right."); } }
    }
}