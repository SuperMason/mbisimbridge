﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand IgnitionIgnitionDown1 { get { return GetXPlaneCommand("sim/ignition/ignition_down_1", "Ignition key down one notch for engine #1."); } }
        public static XPlaneCommand IgnitionIgnitionDown2 { get { return GetXPlaneCommand("sim/ignition/ignition_down_2", "Ignition key down one notch for engine #2."); } }
        public static XPlaneCommand IgnitionIgnitionDown3 { get { return GetXPlaneCommand("sim/ignition/ignition_down_3", "Ignition key down one notch for engine #3."); } }
        public static XPlaneCommand IgnitionIgnitionDown4 { get { return GetXPlaneCommand("sim/ignition/ignition_down_4", "Ignition key down one notch for engine #4."); } }
        public static XPlaneCommand IgnitionIgnitionDown5 { get { return GetXPlaneCommand("sim/ignition/ignition_down_5", "Ignition key down one notch for engine #5."); } }
        public static XPlaneCommand IgnitionIgnitionDown6 { get { return GetXPlaneCommand("sim/ignition/ignition_down_6", "Ignition key down one notch for engine #6."); } }
        public static XPlaneCommand IgnitionIgnitionDown7 { get { return GetXPlaneCommand("sim/ignition/ignition_down_7", "Ignition key down one notch for engine #7."); } }
        public static XPlaneCommand IgnitionIgnitionDown8 { get { return GetXPlaneCommand("sim/ignition/ignition_down_8", "Ignition key down one notch for engine #8."); } }
        public static XPlaneCommand IgnitionIgnitionUp1 { get { return GetXPlaneCommand("sim/ignition/ignition_up_1", "Ignition key up one notch for engine #1."); } }
        public static XPlaneCommand IgnitionIgnitionUp2 { get { return GetXPlaneCommand("sim/ignition/ignition_up_2", "Ignition key up one notch for engine #2."); } }
        public static XPlaneCommand IgnitionIgnitionUp3 { get { return GetXPlaneCommand("sim/ignition/ignition_up_3", "Ignition key up one notch for engine #3."); } }
        public static XPlaneCommand IgnitionIgnitionUp4 { get { return GetXPlaneCommand("sim/ignition/ignition_up_4", "Ignition key up one notch for engine #4."); } }
        public static XPlaneCommand IgnitionIgnitionUp5 { get { return GetXPlaneCommand("sim/ignition/ignition_up_5", "Ignition key up one notch for engine #5."); } }
        public static XPlaneCommand IgnitionIgnitionUp6 { get { return GetXPlaneCommand("sim/ignition/ignition_up_6", "Ignition key up one notch for engine #6."); } }
        public static XPlaneCommand IgnitionIgnitionUp7 { get { return GetXPlaneCommand("sim/ignition/ignition_up_7", "Ignition key up one notch for engine #7."); } }
        public static XPlaneCommand IgnitionIgnitionUp8 { get { return GetXPlaneCommand("sim/ignition/ignition_up_8", "Ignition key up one notch for engine #8."); } }
    }
}