﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand IceAntiIceToggle { get { return GetXPlaneCommand("sim/ice/anti_ice_toggle", "Anti-ice: toggle all."); } }
        public static XPlaneCommand IceAlternateStaticPort { get { return GetXPlaneCommand("sim/ice/alternate_static_port", "Toggle alternate static port."); } }
        public static XPlaneCommand IcePitotHeat0On { get { return GetXPlaneCommand("sim/ice/pitot_heat0_on", "Anti-ice: left pitot heat on."); } }
        public static XPlaneCommand IcePitotHeat1On { get { return GetXPlaneCommand("sim/ice/pitot_heat1_on", "Anti-ice: right pitot heat on."); } }
        public static XPlaneCommand IcePitotHeat0Off { get { return GetXPlaneCommand("sim/ice/pitot_heat0_off", "Anti-ice: left pitot heat off."); } }
        public static XPlaneCommand IcePitotHeat1Off { get { return GetXPlaneCommand("sim/ice/pitot_heat1_off", "Anti-ice: right pitot heat off."); } }
        public static XPlaneCommand IcePitotHeat0Tog { get { return GetXPlaneCommand("sim/ice/pitot_heat0_tog", "Anti-ice: left pitot heat toggle."); } }
        public static XPlaneCommand IcePitotHeat1Tog { get { return GetXPlaneCommand("sim/ice/pitot_heat1_tog", "Anti-ice: right pitot heat toggle."); } }
        public static XPlaneCommand IceStaticHeat0On { get { return GetXPlaneCommand("sim/ice/static_heat0_on", "Anti-ice: left static heat on."); } }
        public static XPlaneCommand IceStaticHeat1On { get { return GetXPlaneCommand("sim/ice/static_heat1_on", "Anti-ice: right static heat on."); } }
        public static XPlaneCommand IceStaticHeat0Off { get { return GetXPlaneCommand("sim/ice/static_heat0_off", "Anti-ice: left static heat off."); } }
        public static XPlaneCommand IceStaticHeat1Off { get { return GetXPlaneCommand("sim/ice/static_heat1_off", "Anti-ice: right static heat off."); } }
        public static XPlaneCommand IceStaticHeat0Tog { get { return GetXPlaneCommand("sim/ice/static_heat0_tog", "Anti-ice: left static heat toggle."); } }
        public static XPlaneCommand IceStaticHeat1Tog { get { return GetXPlaneCommand("sim/ice/static_heat1_tog", "Anti-ice: right static heat toggle."); } }
        public static XPlaneCommand IceAOAHeat0On { get { return GetXPlaneCommand("sim/ice/AOA_heat0_on", "Anti-ice: AOA on."); } }
        public static XPlaneCommand IceAOAHeat1On { get { return GetXPlaneCommand("sim/ice/AOA_heat1_on", "Anti-ice: AOA on."); } }
        public static XPlaneCommand IceAOAHeat0Off { get { return GetXPlaneCommand("sim/ice/AOA_heat0_off", "Anti-ice: AOA off."); } }
        public static XPlaneCommand IceAOAHeat1Off { get { return GetXPlaneCommand("sim/ice/AOA_heat1_off", "Anti-ice: AOA off."); } }
        public static XPlaneCommand IceAOAHeat0Tog { get { return GetXPlaneCommand("sim/ice/AOA_heat0_tog", "Anti-ice: AOA toggle."); } }
        public static XPlaneCommand IceAOAHeat1Tog { get { return GetXPlaneCommand("sim/ice/AOA_heat1_tog", "Anti-ice: AOA toggle."); } }
        public static XPlaneCommand IceWindowHeatOn { get { return GetXPlaneCommand("sim/ice/window_heat_on", "Anti-ice: window heat on."); } }
        public static XPlaneCommand IceWindowHeatOff { get { return GetXPlaneCommand("sim/ice/window_heat_off", "Anti-ice: window heat off."); } }
        public static XPlaneCommand IceWindowHeatTog { get { return GetXPlaneCommand("sim/ice/window_heat_tog", "Anti-ice: window heat toggle."); } }
        public static XPlaneCommand IceWingHeatOn { get { return GetXPlaneCommand("sim/ice/wing_heat_on", "Anti-ice: all wing, anti-ice heat on."); } }
        public static XPlaneCommand IceWingHeat0On { get { return GetXPlaneCommand("sim/ice/wing_heat0_on", "Anti-ice: left wing, anti-ice heat on."); } }
        public static XPlaneCommand IceWingHeat1On { get { return GetXPlaneCommand("sim/ice/wing_heat1_on", "Anti-ice: right wing, anti-ice heat on."); } }
        public static XPlaneCommand IceWingHeatOff { get { return GetXPlaneCommand("sim/ice/wing_heat_off", "Anti-ice: all wing, anti-ice heat off."); } }
        public static XPlaneCommand IceWingHeat0Off { get { return GetXPlaneCommand("sim/ice/wing_heat0_off", "Anti-ice: left wing, anti-ice heat off."); } }
        public static XPlaneCommand IceWingHeat1Off { get { return GetXPlaneCommand("sim/ice/wing_heat1_off", "Anti-ice: right wing, anti-ice heat off."); } }
        public static XPlaneCommand IceWingHeatTog { get { return GetXPlaneCommand("sim/ice/wing_heat_tog", "Anti-ice: all wing, anti-ice heat toggle."); } }
        public static XPlaneCommand IceWingHeat0Tog { get { return GetXPlaneCommand("sim/ice/wing_heat0_tog", "Anti-ice: left wing, anti-ice heat toggle."); } }
        public static XPlaneCommand IceWingHeat1Tog { get { return GetXPlaneCommand("sim/ice/wing_heat1_tog", "Anti-ice: right wing, anti-ice heat toggle."); } }
        public static XPlaneCommand IceWingBootOn { get { return GetXPlaneCommand("sim/ice/wing_boot_on", "Anti-ice: all wing, de-icing boots on schedule."); } }
        public static XPlaneCommand IceWingBoot0On { get { return GetXPlaneCommand("sim/ice/wing_boot0_on", "Anti-ice: left wing, de-icing boots on schedule."); } }
        public static XPlaneCommand IceWingBoot1On { get { return GetXPlaneCommand("sim/ice/wing_boot1_on", "Anti-ice: right wing, de-icing boots on schedule."); } }
        public static XPlaneCommand IceWingBootOff { get { return GetXPlaneCommand("sim/ice/wing_boot_off", "Anti-ice: all wing, de-icing boots off."); } }
        public static XPlaneCommand IceWingBoot0Off { get { return GetXPlaneCommand("sim/ice/wing_boot0_off", "Anti-ice: left wing, de-icing boots off."); } }
        public static XPlaneCommand IceWingBoot1Off { get { return GetXPlaneCommand("sim/ice/wing_boot1_off", "Anti-ice: right wing, de-icing boots off."); } }
        public static XPlaneCommand IceWingBootTog { get { return GetXPlaneCommand("sim/ice/wing_boot_tog", "Anti-ice: all wing, de-icing boots toggle."); } }
        public static XPlaneCommand IceWingBoot0Tog { get { return GetXPlaneCommand("sim/ice/wing_boot0_tog", "Anti-ice: left wing, de-icing boots toggle."); } }
        public static XPlaneCommand IceWingBoot1Tog { get { return GetXPlaneCommand("sim/ice/wing_boot1_tog", "Anti-ice: right wing, de-icing boots toggle."); } }
        public static XPlaneCommand IceWingTaiOn { get { return GetXPlaneCommand("sim/ice/wing_tai_on", "Anti-ice: all wing, bleed anti-ice on."); } }
        public static XPlaneCommand IceWingTai0On { get { return GetXPlaneCommand("sim/ice/wing_tai0_on", "Anti-ice: left wing, bleed anti-ice on."); } }
        public static XPlaneCommand IceWingTai1On { get { return GetXPlaneCommand("sim/ice/wing_tai1_on", "Anti-ice: right wing, bleed anti-ice on."); } }
        public static XPlaneCommand IceWingTaiOff { get { return GetXPlaneCommand("sim/ice/wing_tai_off", "Anti-ice: all wing, bleed anti-ice off."); } }
        public static XPlaneCommand IceWingTai0Off { get { return GetXPlaneCommand("sim/ice/wing_tai0_off", "Anti-ice: left wing, bleed anti-ice off."); } }
        public static XPlaneCommand IceWingTai1Off { get { return GetXPlaneCommand("sim/ice/wing_tai1_off", "Anti-ice: right wing, bleed anti-ice off."); } }
        public static XPlaneCommand IceWingTaiTog { get { return GetXPlaneCommand("sim/ice/wing_tai_tog", "Anti-ice: all wing, bleed anti-ice toggle."); } }
        public static XPlaneCommand IceWingTai0Tog { get { return GetXPlaneCommand("sim/ice/wing_tai0_tog", "Anti-ice: left wing, bleed anti-ice toggle."); } }
        public static XPlaneCommand IceWingTai1Tog { get { return GetXPlaneCommand("sim/ice/wing_tai1_tog", "Anti-ice: right wing, bleed anti-ice toggle."); } }
        public static XPlaneCommand IceWingTksOn { get { return GetXPlaneCommand("sim/ice/wing_tks_on", "Anti-ice: all wing, TKS de-ice normal."); } }
        public static XPlaneCommand IceWingTks0On { get { return GetXPlaneCommand("sim/ice/wing_tks0_on", "Anti-ice: left wing, TKS de-ice normal."); } }
        public static XPlaneCommand IceWingTks1On { get { return GetXPlaneCommand("sim/ice/wing_tks1_on", "Anti-ice: right wing, TKS de-ice normal."); } }
        public static XPlaneCommand IceWingTksHigh { get { return GetXPlaneCommand("sim/ice/wing_tks_high", "Anti-ice: all wing, TKS de-ice high."); } }
        public static XPlaneCommand IceWingTks0High { get { return GetXPlaneCommand("sim/ice/wing_tks0_high", "Anti-ice: left wing, TKS de-ice high."); } }
        public static XPlaneCommand IceWingTks1High { get { return GetXPlaneCommand("sim/ice/wing_tks1_high", "Anti-ice: right wing, TKS de-ice high."); } }
        public static XPlaneCommand IceWingTksOff { get { return GetXPlaneCommand("sim/ice/wing_tks_off", "Anti-ice: all wing, TKS de-ice off."); } }
        public static XPlaneCommand IceWingTks0Off { get { return GetXPlaneCommand("sim/ice/wing_tks0_off", "Anti-ice: left wing, TKS de-ice off."); } }
        public static XPlaneCommand IceWingTks1Off { get { return GetXPlaneCommand("sim/ice/wing_tks1_off", "Anti-ice: right wing, TKS de-ice off."); } }
        public static XPlaneCommand IceWingTksTog { get { return GetXPlaneCommand("sim/ice/wing_tks_tog", "Anti-ice: all wing, TKS de-ice toggle."); } }
        public static XPlaneCommand IceWingTks0Tog { get { return GetXPlaneCommand("sim/ice/wing_tks0_tog", "Anti-ice: left wing, TKS de-ice toggle."); } }
        public static XPlaneCommand IceWingTks1Tog { get { return GetXPlaneCommand("sim/ice/wing_tks1_tog", "Anti-ice: right wing, TKS de-ice toggle."); } }
        public static XPlaneCommand IceInletHeatOn { get { return GetXPlaneCommand("sim/ice/inlet_heat_on", "Anti-ice: all engines inlet heat on."); } }
        public static XPlaneCommand IceInletHeatOff { get { return GetXPlaneCommand("sim/ice/inlet_heat_off", "Anti-ice: all engines inlet heat off."); } }
        public static XPlaneCommand IceInletHeatTog { get { return GetXPlaneCommand("sim/ice/inlet_heat_tog", "Anti-ice: all engines inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat0On { get { return GetXPlaneCommand("sim/ice/inlet_heat0_on", "Anti-ice: engine #1 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat1On { get { return GetXPlaneCommand("sim/ice/inlet_heat1_on", "Anti-ice: engine #2 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat2On { get { return GetXPlaneCommand("sim/ice/inlet_heat2_on", "Anti-ice: engine #3 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat3On { get { return GetXPlaneCommand("sim/ice/inlet_heat3_on", "Anti-ice: engine #4 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat4On { get { return GetXPlaneCommand("sim/ice/inlet_heat4_on", "Anti-ice: engine #5 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat5On { get { return GetXPlaneCommand("sim/ice/inlet_heat5_on", "Anti-ice: engine #6 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat6On { get { return GetXPlaneCommand("sim/ice/inlet_heat6_on", "Anti-ice: engine #7 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat7On { get { return GetXPlaneCommand("sim/ice/inlet_heat7_on", "Anti-ice: engine #8 inlet heat on."); } }
        public static XPlaneCommand IceInletHeat0Off { get { return GetXPlaneCommand("sim/ice/inlet_heat0_off", "Anti-ice: engine #1 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat1Off { get { return GetXPlaneCommand("sim/ice/inlet_heat1_off", "Anti-ice: engine #2 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat2Off { get { return GetXPlaneCommand("sim/ice/inlet_heat2_off", "Anti-ice: engine #3 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat3Off { get { return GetXPlaneCommand("sim/ice/inlet_heat3_off", "Anti-ice: engine #4 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat4Off { get { return GetXPlaneCommand("sim/ice/inlet_heat4_off", "Anti-ice: engine #5 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat5Off { get { return GetXPlaneCommand("sim/ice/inlet_heat5_off", "Anti-ice: engine #6 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat6Off { get { return GetXPlaneCommand("sim/ice/inlet_heat6_off", "Anti-ice: engine #7 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat7Off { get { return GetXPlaneCommand("sim/ice/inlet_heat7_off", "Anti-ice: engine #8 inlet heat off."); } }
        public static XPlaneCommand IceInletHeat0Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat0_tog", "Anti-ice: engine #1 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat1Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat1_tog", "Anti-ice: engine #2 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat2Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat2_tog", "Anti-ice: engine #3 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat3Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat3_tog", "Anti-ice: engine #4 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat4Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat4_tog", "Anti-ice: engine #5 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat5Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat5_tog", "Anti-ice: engine #6 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat6Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat6_tog", "Anti-ice: engine #7 inlet heat toggle."); } }
        public static XPlaneCommand IceInletHeat7Tog { get { return GetXPlaneCommand("sim/ice/inlet_heat7_tog", "Anti-ice: engine #8 inlet heat toggle."); } }
        public static XPlaneCommand IceInletEai0On { get { return GetXPlaneCommand("sim/ice/inlet_eai0_on", "Anti-ice: engine #1 anti-ice on."); } }
        public static XPlaneCommand IceInletEai1On { get { return GetXPlaneCommand("sim/ice/inlet_eai1_on", "Anti-ice: engine #2 anti-ice on."); } }
        public static XPlaneCommand IceInletEai2On { get { return GetXPlaneCommand("sim/ice/inlet_eai2_on", "Anti-ice: engine #3 anti-ice on."); } }
        public static XPlaneCommand IceInletEai3On { get { return GetXPlaneCommand("sim/ice/inlet_eai3_on", "Anti-ice: engine #4 anti-ice on."); } }
        public static XPlaneCommand IceInletEai4On { get { return GetXPlaneCommand("sim/ice/inlet_eai4_on", "Anti-ice: engine #5 anti-ice on."); } }
        public static XPlaneCommand IceInletEai5On { get { return GetXPlaneCommand("sim/ice/inlet_eai5_on", "Anti-ice: engine #6 anti-ice on."); } }
        public static XPlaneCommand IceInletEai6On { get { return GetXPlaneCommand("sim/ice/inlet_eai6_on", "Anti-ice: engine #7 anti-ice on."); } }
        public static XPlaneCommand IceInletEai7On { get { return GetXPlaneCommand("sim/ice/inlet_eai7_on", "Anti-ice: engine #8 anti-ice on."); } }
        public static XPlaneCommand IceInletEai0Off { get { return GetXPlaneCommand("sim/ice/inlet_eai0_off", "Anti-ice: engine #1 anti-ice off."); } }
        public static XPlaneCommand IceInletEai1Off { get { return GetXPlaneCommand("sim/ice/inlet_eai1_off", "Anti-ice: engine #2 anti-ice off."); } }
        public static XPlaneCommand IceInletEai2Off { get { return GetXPlaneCommand("sim/ice/inlet_eai2_off", "Anti-ice: engine #3 anti-ice off."); } }
        public static XPlaneCommand IceInletEai3Off { get { return GetXPlaneCommand("sim/ice/inlet_eai3_off", "Anti-ice: engine #4 anti-ice off."); } }
        public static XPlaneCommand IceInletEai4Off { get { return GetXPlaneCommand("sim/ice/inlet_eai4_off", "Anti-ice: engine #5 anti-ice off."); } }
        public static XPlaneCommand IceInletEai5Off { get { return GetXPlaneCommand("sim/ice/inlet_eai5_off", "Anti-ice: engine #6 anti-ice off."); } }
        public static XPlaneCommand IceInletEai6Off { get { return GetXPlaneCommand("sim/ice/inlet_eai6_off", "Anti-ice: engine #7 anti-ice off."); } }
        public static XPlaneCommand IceInletEai7Off { get { return GetXPlaneCommand("sim/ice/inlet_eai7_off", "Anti-ice: engine #8 anti-ice off."); } }
        public static XPlaneCommand IceInletEai0Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai0_tog", "Anti-ice: engine #1 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai1Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai1_tog", "Anti-ice: engine #2 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai2Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai2_tog", "Anti-ice: engine #3 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai3Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai3_tog", "Anti-ice: engine #4 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai4Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai4_tog", "Anti-ice: engine #5 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai5Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai5_tog", "Anti-ice: engine #6 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai6Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai6_tog", "Anti-ice: engine #7 anti-ice toggle."); } }
        public static XPlaneCommand IceInletEai7Tog { get { return GetXPlaneCommand("sim/ice/inlet_eai7_tog", "Anti-ice: engine #8 anti-ice toggle."); } }
        public static XPlaneCommand IcePropHeatOn { get { return GetXPlaneCommand("sim/ice/prop_heat_on", "Anti-ice: all prop heat on."); } }
        public static XPlaneCommand IcePropHeatOff { get { return GetXPlaneCommand("sim/ice/prop_heat_off", "Anti-ice: all prop heat off."); } }
        public static XPlaneCommand IcePropHeatTog { get { return GetXPlaneCommand("sim/ice/prop_heat_tog", "Anti-ice: all prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat0On { get { return GetXPlaneCommand("sim/ice/prop_heat0_on", "Anti-ice: engine #1 prop heat on."); } }
        public static XPlaneCommand IcePropHeat1On { get { return GetXPlaneCommand("sim/ice/prop_heat1_on", "Anti-ice: engine #2 prop heat on."); } }
        public static XPlaneCommand IcePropHeat2On { get { return GetXPlaneCommand("sim/ice/prop_heat2_on", "Anti-ice: engine #3 prop heat on."); } }
        public static XPlaneCommand IcePropHeat3On { get { return GetXPlaneCommand("sim/ice/prop_heat3_on", "Anti-ice: engine #4 prop heat on."); } }
        public static XPlaneCommand IcePropHeat4On { get { return GetXPlaneCommand("sim/ice/prop_heat4_on", "Anti-ice: engine #5 prop heat on."); } }
        public static XPlaneCommand IcePropHeat5On { get { return GetXPlaneCommand("sim/ice/prop_heat5_on", "Anti-ice: engine #6 prop heat on."); } }
        public static XPlaneCommand IcePropHeat6On { get { return GetXPlaneCommand("sim/ice/prop_heat6_on", "Anti-ice: engine #7 prop heat on."); } }
        public static XPlaneCommand IcePropHeat7On { get { return GetXPlaneCommand("sim/ice/prop_heat7_on", "Anti-ice: engine #8 prop heat on."); } }
        public static XPlaneCommand IcePropHeat0Off { get { return GetXPlaneCommand("sim/ice/prop_heat0_off", "Anti-ice: engine #1 prop heat off."); } }
        public static XPlaneCommand IcePropHeat1Off { get { return GetXPlaneCommand("sim/ice/prop_heat1_off", "Anti-ice: engine #2 prop heat off."); } }
        public static XPlaneCommand IcePropHeat2Off { get { return GetXPlaneCommand("sim/ice/prop_heat2_off", "Anti-ice: engine #3 prop heat off."); } }
        public static XPlaneCommand IcePropHeat3Off { get { return GetXPlaneCommand("sim/ice/prop_heat3_off", "Anti-ice: engine #4 prop heat off."); } }
        public static XPlaneCommand IcePropHeat4Off { get { return GetXPlaneCommand("sim/ice/prop_heat4_off", "Anti-ice: engine #5 prop heat off."); } }
        public static XPlaneCommand IcePropHeat5Off { get { return GetXPlaneCommand("sim/ice/prop_heat5_off", "Anti-ice: engine #6 prop heat off."); } }
        public static XPlaneCommand IcePropHeat6Off { get { return GetXPlaneCommand("sim/ice/prop_heat6_off", "Anti-ice: engine #7 prop heat off."); } }
        public static XPlaneCommand IcePropHeat7Off { get { return GetXPlaneCommand("sim/ice/prop_heat7_off", "Anti-ice: engine #8 prop heat off."); } }
        public static XPlaneCommand IcePropHeat0Tog { get { return GetXPlaneCommand("sim/ice/prop_heat0_tog", "Anti-ice: engine #1 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat1Tog { get { return GetXPlaneCommand("sim/ice/prop_heat1_tog", "Anti-ice: engine #2 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat2Tog { get { return GetXPlaneCommand("sim/ice/prop_heat2_tog", "Anti-ice: engine #3 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat3Tog { get { return GetXPlaneCommand("sim/ice/prop_heat3_tog", "Anti-ice: engine #4 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat4Tog { get { return GetXPlaneCommand("sim/ice/prop_heat4_tog", "Anti-ice: engine #5 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat5Tog { get { return GetXPlaneCommand("sim/ice/prop_heat5_tog", "Anti-ice: engine #6 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat6Tog { get { return GetXPlaneCommand("sim/ice/prop_heat6_tog", "Anti-ice: engine #7 prop heat toggle."); } }
        public static XPlaneCommand IcePropHeat7Tog { get { return GetXPlaneCommand("sim/ice/prop_heat7_tog", "Anti-ice: engine #8 prop heat toggle."); } }
        public static XPlaneCommand IcePropTksOn { get { return GetXPlaneCommand("sim/ice/prop_tks_on", "Anti-ice: all prop TKS norm."); } }
        public static XPlaneCommand IcePropTksHigh { get { return GetXPlaneCommand("sim/ice/prop_tks_high", "Anti-ice: all prop TKS high."); } }
        public static XPlaneCommand IcePropTksOff { get { return GetXPlaneCommand("sim/ice/prop_tks_off", "Anti-ice: all prop TKS off."); } }
        public static XPlaneCommand IcePropTksTog { get { return GetXPlaneCommand("sim/ice/prop_tks_tog", "Anti-ice: all prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks0On { get { return GetXPlaneCommand("sim/ice/prop_tks0_on", "Anti-ice: engine #1 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks1On { get { return GetXPlaneCommand("sim/ice/prop_tks1_on", "Anti-ice: engine #2 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks2On { get { return GetXPlaneCommand("sim/ice/prop_tks2_on", "Anti-ice: engine #3 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks3On { get { return GetXPlaneCommand("sim/ice/prop_tks3_on", "Anti-ice: engine #4 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks4On { get { return GetXPlaneCommand("sim/ice/prop_tks4_on", "Anti-ice: engine #5 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks5On { get { return GetXPlaneCommand("sim/ice/prop_tks5_on", "Anti-ice: engine #6 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks6On { get { return GetXPlaneCommand("sim/ice/prop_tks6_on", "Anti-ice: engine #7 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks7On { get { return GetXPlaneCommand("sim/ice/prop_tks7_on", "Anti-ice: engine #8 prop TKS norm."); } }
        public static XPlaneCommand IcePropTks0High { get { return GetXPlaneCommand("sim/ice/prop_tks0_high", "Anti-ice: engine #1 prop TKS high."); } }
        public static XPlaneCommand IcePropTks1High { get { return GetXPlaneCommand("sim/ice/prop_tks1_high", "Anti-ice: engine #2 prop TKS high."); } }
        public static XPlaneCommand IcePropTks2High { get { return GetXPlaneCommand("sim/ice/prop_tks2_high", "Anti-ice: engine #3 prop TKS high."); } }
        public static XPlaneCommand IcePropTks3High { get { return GetXPlaneCommand("sim/ice/prop_tks3_high", "Anti-ice: engine #4 prop TKS high."); } }
        public static XPlaneCommand IcePropTks4High { get { return GetXPlaneCommand("sim/ice/prop_tks4_high", "Anti-ice: engine #5 prop TKS high."); } }
        public static XPlaneCommand IcePropTks5High { get { return GetXPlaneCommand("sim/ice/prop_tks5_high", "Anti-ice: engine #6 prop TKS high."); } }
        public static XPlaneCommand IcePropTks6High { get { return GetXPlaneCommand("sim/ice/prop_tks6_high", "Anti-ice: engine #7 prop TKS high."); } }
        public static XPlaneCommand IcePropTks7High { get { return GetXPlaneCommand("sim/ice/prop_tks7_high", "Anti-ice: engine #8 prop TKS high."); } }
        public static XPlaneCommand IcePropTks0Off { get { return GetXPlaneCommand("sim/ice/prop_tks0_off", "Anti-ice: engine #1 prop TKS off."); } }
        public static XPlaneCommand IcePropTks1Off { get { return GetXPlaneCommand("sim/ice/prop_tks1_off", "Anti-ice: engine #2 prop TKS off."); } }
        public static XPlaneCommand IcePropTks2Off { get { return GetXPlaneCommand("sim/ice/prop_tks2_off", "Anti-ice: engine #3 prop TKS off."); } }
        public static XPlaneCommand IcePropTks3Off { get { return GetXPlaneCommand("sim/ice/prop_tks3_off", "Anti-ice: engine #4 prop TKS off."); } }
        public static XPlaneCommand IcePropTks4Off { get { return GetXPlaneCommand("sim/ice/prop_tks4_off", "Anti-ice: engine #5 prop TKS off."); } }
        public static XPlaneCommand IcePropTks5Off { get { return GetXPlaneCommand("sim/ice/prop_tks5_off", "Anti-ice: engine #6 prop TKS off."); } }
        public static XPlaneCommand IcePropTks6Off { get { return GetXPlaneCommand("sim/ice/prop_tks6_off", "Anti-ice: engine #7 prop TKS off."); } }
        public static XPlaneCommand IcePropTks7Off { get { return GetXPlaneCommand("sim/ice/prop_tks7_off", "Anti-ice: engine #8 prop TKS off."); } }
        public static XPlaneCommand IcePropTks0Tog { get { return GetXPlaneCommand("sim/ice/prop_tks0_tog", "Anti-ice: engine #1 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks1Tog { get { return GetXPlaneCommand("sim/ice/prop_tks1_tog", "Anti-ice: engine #2 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks2Tog { get { return GetXPlaneCommand("sim/ice/prop_tks2_tog", "Anti-ice: engine #3 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks3Tog { get { return GetXPlaneCommand("sim/ice/prop_tks3_tog", "Anti-ice: engine #4 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks4Tog { get { return GetXPlaneCommand("sim/ice/prop_tks4_tog", "Anti-ice: engine #5 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks5Tog { get { return GetXPlaneCommand("sim/ice/prop_tks5_tog", "Anti-ice: engine #6 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks6Tog { get { return GetXPlaneCommand("sim/ice/prop_tks6_tog", "Anti-ice: engine #7 prop TKS toggle."); } }
        public static XPlaneCommand IcePropTks7Tog { get { return GetXPlaneCommand("sim/ice/prop_tks7_tog", "Anti-ice: engine #8 prop TKS toggle."); } }
        public static XPlaneCommand IceDetectOn { get { return GetXPlaneCommand("sim/ice/detect_on", "Anti-ice: ice detection on."); } }
        public static XPlaneCommand IceDetectOff { get { return GetXPlaneCommand("sim/ice/detect_off", "Anti-ice: ice detection off."); } }
    }
}