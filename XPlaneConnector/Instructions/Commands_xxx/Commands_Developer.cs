﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand DeveloperToggleTextureBrowser { get { return GetXPlaneCommand("sim/developer/toggle_texture_browser", "Toggle the texture browser window."); } }
        public static XPlaneCommand DeveloperToggleParticleBrowser { get { return GetXPlaneCommand("sim/developer/toggle_particle_browser", "Toggle the particle system browser window."); } }
        public static XPlaneCommand DeveloperToggleMicroprofiler { get { return GetXPlaneCommand("sim/developer/toggle_microprofiler", "Toggle the frame timing profiler window."); } }
        public static XPlaneCommand DeveloperToggleVramProfiler { get { return GetXPlaneCommand("sim/developer/toggle_vram_profiler", "Toggle the VRAM profiler window."); } }
        public static XPlaneCommand DeveloperTogglePluginAdmin { get { return GetXPlaneCommand("sim/developer/toggle_plugin_admin", "Toggle the Plugin Admin window."); } }
    }
}