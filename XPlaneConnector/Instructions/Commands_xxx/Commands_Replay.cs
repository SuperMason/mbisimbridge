﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand ReplayReplayToggle { get { return GetXPlaneCommand("sim/replay/replay_toggle", "Toggle replay mode on/off."); } }
        public static XPlaneCommand ReplayReplayOff { get { return GetXPlaneCommand("sim/replay/replay_off", "Replay mode off."); } }
        public static XPlaneCommand ReplayReplayControlsToggle { get { return GetXPlaneCommand("sim/replay/replay_controls_toggle", "Replay mode: Toggle controls visibility."); } }
        public static XPlaneCommand ReplayRepBegin { get { return GetXPlaneCommand("sim/replay/rep_begin", "Replay mode: go to beginning."); } }
        public static XPlaneCommand ReplayRepPlayFr { get { return GetXPlaneCommand("sim/replay/rep_play_fr", "Replay mode: play fast reverse."); } }
        public static XPlaneCommand ReplayRepPlayRr { get { return GetXPlaneCommand("sim/replay/rep_play_rr", "Replay mode: play reverse."); } }
        public static XPlaneCommand ReplayRepPlaySr { get { return GetXPlaneCommand("sim/replay/rep_play_sr", "Replay mode: play slow reverse."); } }
        public static XPlaneCommand ReplayRepPause { get { return GetXPlaneCommand("sim/replay/rep_pause", "Replay mode: pause."); } }
        public static XPlaneCommand ReplayRepPlaySf { get { return GetXPlaneCommand("sim/replay/rep_play_sf", "Replay mode: play slow forward."); } }
        public static XPlaneCommand ReplayRepPlayRf { get { return GetXPlaneCommand("sim/replay/rep_play_rf", "Replay mode: play forward."); } }
        public static XPlaneCommand ReplayRepPlayFf { get { return GetXPlaneCommand("sim/replay/rep_play_ff", "Replay mode: play fast forward."); } }
        public static XPlaneCommand ReplayRepEnd { get { return GetXPlaneCommand("sim/replay/rep_end", "Replay mode: go to end."); } }
    }
}