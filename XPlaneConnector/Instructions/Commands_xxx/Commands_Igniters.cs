﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand IgnitersIgniterArmOff1 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_1", "Igniter #1 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff2 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_2", "Igniter #2 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff3 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_3", "Igniter #3 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff4 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_4", "Igniter #4 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff5 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_5", "Igniter #5 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff6 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_6", "Igniter #6 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff7 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_7", "Igniter #7 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOff8 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_off_8", "Igniter #8 arm off."); } }
        public static XPlaneCommand IgnitersIgniterArmOn1 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_1", "Igniter #1 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn2 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_2", "Igniter #2 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn3 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_3", "Igniter #3 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn4 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_4", "Igniter #4 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn5 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_5", "Igniter #5 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn6 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_6", "Igniter #6 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn7 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_7", "Igniter #7 arm on."); } }
        public static XPlaneCommand IgnitersIgniterArmOn8 { get { return GetXPlaneCommand("sim/igniters/igniter_arm_on_8", "Igniter #8 arm on."); } }
        public static XPlaneCommand IgnitersIgniterContinOff1 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_1", "Igniter #1 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff2 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_2", "Igniter #2 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff3 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_3", "Igniter #3 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff4 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_4", "Igniter #4 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff5 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_5", "Igniter #5 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff6 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_6", "Igniter #6 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff7 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_7", "Igniter #7 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOff8 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_off_8", "Igniter #8 contin-ignition off."); } }
        public static XPlaneCommand IgnitersIgniterContinOn1 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_1", "Igniter #1 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn2 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_2", "Igniter #2 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn3 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_3", "Igniter #3 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn4 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_4", "Igniter #4 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn5 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_5", "Igniter #5 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn6 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_6", "Igniter #6 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn7 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_7", "Igniter #7 contin-ignition on."); } }
        public static XPlaneCommand IgnitersIgniterContinOn8 { get { return GetXPlaneCommand("sim/igniters/igniter_contin_on_8", "Igniter #8 contin-ignition on."); } }
    }
}