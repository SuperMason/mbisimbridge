﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand TransponderTransponderDigit0 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_0", "Transponder digit to 0."); } }
        public static XPlaneCommand TransponderTransponderDigit1 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_1", "Transponder digit to 1."); } }
        public static XPlaneCommand TransponderTransponderDigit2 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_2", "Transponder digit to 2."); } }
        public static XPlaneCommand TransponderTransponderDigit3 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_3", "Transponder digit to 3."); } }
        public static XPlaneCommand TransponderTransponderDigit4 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_4", "Transponder digit to 4."); } }
        public static XPlaneCommand TransponderTransponderDigit5 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_5", "Transponder digit to 5."); } }
        public static XPlaneCommand TransponderTransponderDigit6 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_6", "Transponder digit to 6."); } }
        public static XPlaneCommand TransponderTransponderDigit7 { get { return GetXPlaneCommand("sim/transponder/transponder_digit_7", "Transponder digit to 7."); } }
        public static XPlaneCommand TransponderTransponderCLR { get { return GetXPlaneCommand("sim/transponder/transponder_CLR", "Transponder reset to first digit."); } }
        public static XPlaneCommand TransponderTransponderThousandsDown { get { return GetXPlaneCommand("sim/transponder/transponder_thousands_down", "Transponder thousands down."); } }
        public static XPlaneCommand TransponderTransponderThousandsUp { get { return GetXPlaneCommand("sim/transponder/transponder_thousands_up", "Transponder thousands up."); } }
        public static XPlaneCommand TransponderTransponderHundredsDown { get { return GetXPlaneCommand("sim/transponder/transponder_hundreds_down", "Transponder hundreds down."); } }
        public static XPlaneCommand TransponderTransponderHundredsUp { get { return GetXPlaneCommand("sim/transponder/transponder_hundreds_up", "Transponder hundreds up."); } }
        public static XPlaneCommand TransponderTransponderTensDown { get { return GetXPlaneCommand("sim/transponder/transponder_tens_down", "Transponder tens down."); } }
        public static XPlaneCommand TransponderTransponderTensUp { get { return GetXPlaneCommand("sim/transponder/transponder_tens_up", "Transponder tens up."); } }
        public static XPlaneCommand TransponderTransponderOnesDown { get { return GetXPlaneCommand("sim/transponder/transponder_ones_down", "Transponder ones down."); } }
        public static XPlaneCommand TransponderTransponderOnesUp { get { return GetXPlaneCommand("sim/transponder/transponder_ones_up", "Transponder ones up."); } }
        public static XPlaneCommand TransponderTransponder12Down { get { return GetXPlaneCommand("sim/transponder/transponder_12_down", "Transponder digits 1 and 2 down."); } }
        public static XPlaneCommand TransponderTransponder12Up { get { return GetXPlaneCommand("sim/transponder/transponder_12_up", "Transponder digits 1 and 2 up."); } }
        public static XPlaneCommand TransponderTransponder34Down { get { return GetXPlaneCommand("sim/transponder/transponder_34_down", "Transponder digits 3 and 4 down."); } }
        public static XPlaneCommand TransponderTransponder34Up { get { return GetXPlaneCommand("sim/transponder/transponder_34_up", "Transponder digits 3 and 4 up."); } }
        public static XPlaneCommand TransponderTransponderIdent { get { return GetXPlaneCommand("sim/transponder/transponder_ident", "Transponder ID."); } }
        public static XPlaneCommand TransponderTransponderOff { get { return GetXPlaneCommand("sim/transponder/transponder_off", "Transponder off."); } }
        public static XPlaneCommand TransponderTransponderStandby { get { return GetXPlaneCommand("sim/transponder/transponder_standby", "Transponder standby."); } }
        public static XPlaneCommand TransponderTransponderOn { get { return GetXPlaneCommand("sim/transponder/transponder_on", "Transponder on."); } }
        public static XPlaneCommand TransponderTransponderAlt { get { return GetXPlaneCommand("sim/transponder/transponder_alt", "Transponder alt."); } }
        public static XPlaneCommand TransponderTransponderTest { get { return GetXPlaneCommand("sim/transponder/transponder_test", "Transponder test."); } }
        public static XPlaneCommand TransponderTransponderGround { get { return GetXPlaneCommand("sim/transponder/transponder_ground", "Transponder ground."); } }
        public static XPlaneCommand TransponderTransponderDn { get { return GetXPlaneCommand("sim/transponder/transponder_dn", "Transponder mode down."); } }
        public static XPlaneCommand TransponderTransponderUp { get { return GetXPlaneCommand("sim/transponder/transponder_up", "Transponder mode up."); } }
    }
}