﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand RadiosPowerNav1Off { get { return GetXPlaneCommand("sim/radios/power_nav1_off", "Power NAV1 off."); } }
        public static XPlaneCommand RadiosPowerNav1On { get { return GetXPlaneCommand("sim/radios/power_nav1_on", "Power NAV1 on."); } }
        public static XPlaneCommand RadiosPowerNav2Off { get { return GetXPlaneCommand("sim/radios/power_nav2_off", "Power NAV2 off."); } }
        public static XPlaneCommand RadiosPowerNav2On { get { return GetXPlaneCommand("sim/radios/power_nav2_on", "Power NAV2 on."); } }
        public static XPlaneCommand RadiosPowerCom1Off { get { return GetXPlaneCommand("sim/radios/power_com1_off", "Power COM1 off."); } }
        public static XPlaneCommand RadiosPowerCom1On { get { return GetXPlaneCommand("sim/radios/power_com1_on", "Power COM1 on."); } }
        public static XPlaneCommand RadiosPowerCom2Off { get { return GetXPlaneCommand("sim/radios/power_com2_off", "Power COM2 off."); } }
        public static XPlaneCommand RadiosPowerCom2On { get { return GetXPlaneCommand("sim/radios/power_com2_on", "Power COM2 on."); } }
        public static XPlaneCommand RadiosPowerAdf1Dn { get { return GetXPlaneCommand("sim/radios/power_adf1_dn", "Power ADF1 dn."); } }
        public static XPlaneCommand RadiosPowerAdf1Up { get { return GetXPlaneCommand("sim/radios/power_adf1_up", "Power ADF1 up."); } }
        public static XPlaneCommand RadiosPowerAdf2Dn { get { return GetXPlaneCommand("sim/radios/power_adf2_dn", "Power ADF2 dn."); } }
        public static XPlaneCommand RadiosPowerAdf2Up { get { return GetXPlaneCommand("sim/radios/power_adf2_up", "Power ADF2 up."); } }
        public static XPlaneCommand RadiosAdf1PowerMode0 { get { return GetXPlaneCommand("sim/radios/adf1_power_mode_0", "ADF 1 off."); } }
        public static XPlaneCommand RadiosAdf1PowerMode1 { get { return GetXPlaneCommand("sim/radios/adf1_power_mode_1", "ADF 1 antenna."); } }
        public static XPlaneCommand RadiosAdf1PowerMode2 { get { return GetXPlaneCommand("sim/radios/adf1_power_mode_2", "ADF 1 on."); } }
        public static XPlaneCommand RadiosAdf1PowerMode3 { get { return GetXPlaneCommand("sim/radios/adf1_power_mode_3", "ADF 1 tone."); } }
        public static XPlaneCommand RadiosAdf1PowerMode4 { get { return GetXPlaneCommand("sim/radios/adf1_power_mode_4", "ADF 1 test."); } }
        public static XPlaneCommand RadiosAdf2PowerMode0 { get { return GetXPlaneCommand("sim/radios/adf2_power_mode_0", "ADF 2 off."); } }
        public static XPlaneCommand RadiosAdf2PowerMode1 { get { return GetXPlaneCommand("sim/radios/adf2_power_mode_1", "ADF 2 antenna."); } }
        public static XPlaneCommand RadiosAdf2PowerMode2 { get { return GetXPlaneCommand("sim/radios/adf2_power_mode_2", "ADF 2 on."); } }
        public static XPlaneCommand RadiosAdf2PowerMode3 { get { return GetXPlaneCommand("sim/radios/adf2_power_mode_3", "ADF 2 tone."); } }
        public static XPlaneCommand RadiosAdf2PowerMode4 { get { return GetXPlaneCommand("sim/radios/adf2_power_mode_4", "ADF 2 test."); } }
        public static XPlaneCommand RadiosActvCom1CoarseDown { get { return GetXPlaneCommand("sim/radios/actv_com1_coarse_down", "COM 1 coarse down."); } }
        public static XPlaneCommand RadiosActvCom1CoarseUp { get { return GetXPlaneCommand("sim/radios/actv_com1_coarse_up", "COM 1 coarse up."); } }
        public static XPlaneCommand RadiosActvCom1FineDown { get { return GetXPlaneCommand("sim/radios/actv_com1_fine_down", "COM 1 fine down (25kHz)."); } }
        public static XPlaneCommand RadiosActvCom1FineUp { get { return GetXPlaneCommand("sim/radios/actv_com1_fine_up", "COM 1 fine up (25kHz)."); } }
        public static XPlaneCommand RadiosActvCom1CoarseDown833 { get { return GetXPlaneCommand("sim/radios/actv_com1_coarse_down_833", "COM 1 coarse down (8.33 kHz)."); } }
        public static XPlaneCommand RadiosActvCom1CoarseUp833 { get { return GetXPlaneCommand("sim/radios/actv_com1_coarse_up_833", "COM 1 coarse up (8.33 kHz)."); } }
        public static XPlaneCommand RadiosActvCom1FineDown833 { get { return GetXPlaneCommand("sim/radios/actv_com1_fine_down_833", "COM 1 fine down (8.33 kHz)."); } }
        public static XPlaneCommand RadiosActvCom1FineUp833 { get { return GetXPlaneCommand("sim/radios/actv_com1_fine_up_833", "COM 1 fine up (8.33 kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1CoarseDown { get { return GetXPlaneCommand("sim/radios/stby_com1_coarse_down", "COM 1 standby coarse down."); } }
        public static XPlaneCommand RadiosStbyCom1CoarseUp { get { return GetXPlaneCommand("sim/radios/stby_com1_coarse_up", "COM 1 standby coarse up."); } }
        public static XPlaneCommand RadiosStbyCom1FineDown { get { return GetXPlaneCommand("sim/radios/stby_com1_fine_down", "COM 1 standby fine down (25kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1FineUp { get { return GetXPlaneCommand("sim/radios/stby_com1_fine_up", "COM 1 standby fine up (25kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1CoarseDown833 { get { return GetXPlaneCommand("sim/radios/stby_com1_coarse_down_833", "COM 1 standby coarse down (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1CoarseUp833 { get { return GetXPlaneCommand("sim/radios/stby_com1_coarse_up_833", "COM 1 standby coarse up (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1FineDown833 { get { return GetXPlaneCommand("sim/radios/stby_com1_fine_down_833", "COM 1 standby fine down (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom1FineUp833 { get { return GetXPlaneCommand("sim/radios/stby_com1_fine_up_833", "COM 1 standby fine up (8.33kHz)."); } }
        public static XPlaneCommand RadiosActvCom2CoarseDown { get { return GetXPlaneCommand("sim/radios/actv_com2_coarse_down", "COM 2 coarse down."); } }
        public static XPlaneCommand RadiosActvCom2CoarseUp { get { return GetXPlaneCommand("sim/radios/actv_com2_coarse_up", "COM 2 coarse up."); } }
        public static XPlaneCommand RadiosActvCom2FineDown { get { return GetXPlaneCommand("sim/radios/actv_com2_fine_down", "COM 2 fine down (25kHz)."); } }
        public static XPlaneCommand RadiosActvCom2FineUp { get { return GetXPlaneCommand("sim/radios/actv_com2_fine_up", "COM 2 fine up (25kHz)."); } }
        public static XPlaneCommand RadiosActvCom2CoarseDown833 { get { return GetXPlaneCommand("sim/radios/actv_com2_coarse_down_833", "COM 2 coarse down (8.33kHz)."); } }
        public static XPlaneCommand RadiosActvCom2CoarseUp833 { get { return GetXPlaneCommand("sim/radios/actv_com2_coarse_up_833", "COM 2 coarse up (8.33kHz)."); } }
        public static XPlaneCommand RadiosActvCom2FineDown833 { get { return GetXPlaneCommand("sim/radios/actv_com2_fine_down_833", "COM 2 fine down (8.33kHz)."); } }
        public static XPlaneCommand RadiosActvCom2FineUp833 { get { return GetXPlaneCommand("sim/radios/actv_com2_fine_up_833", "COM 2 fine up (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2CoarseDown { get { return GetXPlaneCommand("sim/radios/stby_com2_coarse_down", "COM 2 standby coarse down."); } }
        public static XPlaneCommand RadiosStbyCom2CoarseUp { get { return GetXPlaneCommand("sim/radios/stby_com2_coarse_up", "COM 2 standby coarse up."); } }
        public static XPlaneCommand RadiosStbyCom2FineDown { get { return GetXPlaneCommand("sim/radios/stby_com2_fine_down", "COM 2 standby fine down (25kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2FineUp { get { return GetXPlaneCommand("sim/radios/stby_com2_fine_up", "COM 2 standby fine up (25kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2CoarseDown833 { get { return GetXPlaneCommand("sim/radios/stby_com2_coarse_down_833", "COM 2 standby coarse down (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2CoarseUp833 { get { return GetXPlaneCommand("sim/radios/stby_com2_coarse_up_833", "COM 2 standby coarse up (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2FineDown833 { get { return GetXPlaneCommand("sim/radios/stby_com2_fine_down_833", "COM 2 standby fine down (8.33kHz)."); } }
        public static XPlaneCommand RadiosStbyCom2FineUp833 { get { return GetXPlaneCommand("sim/radios/stby_com2_fine_up_833", "COM 2 standby fine up (8.33kHz)."); } }
        public static XPlaneCommand RadiosActvNav1CoarseDown { get { return GetXPlaneCommand("sim/radios/actv_nav1_coarse_down", "NAV 1 coarse down."); } }
        public static XPlaneCommand RadiosActvNav1CoarseUp { get { return GetXPlaneCommand("sim/radios/actv_nav1_coarse_up", "NAV 1 coarse up."); } }
        public static XPlaneCommand RadiosActvNav1FineDown { get { return GetXPlaneCommand("sim/radios/actv_nav1_fine_down", "NAV 1 fine down."); } }
        public static XPlaneCommand RadiosActvNav1FineUp { get { return GetXPlaneCommand("sim/radios/actv_nav1_fine_up", "NAV 1 fine up."); } }
        public static XPlaneCommand RadiosStbyNav1CoarseDown { get { return GetXPlaneCommand("sim/radios/stby_nav1_coarse_down", "NAV 1 standby coarse down."); } }
        public static XPlaneCommand RadiosStbyNav1CoarseUp { get { return GetXPlaneCommand("sim/radios/stby_nav1_coarse_up", "NAV 1 standby coarse up."); } }
        /// <summary>
        /// NAV 1 standby fine down.
        /// </summary>
        public static XPlaneCommand RadiosStbyNav1FineDown { get { return GetXPlaneCommand("sim/radios/stby_nav1_fine_down", "NAV 1 standby fine down."); } }
        /// <summary>
        /// NAV 1 standby fine up.
        /// </summary>
        public static XPlaneCommand RadiosStbyNav1FineUp { get { return GetXPlaneCommand("sim/radios/stby_nav1_fine_up", "NAV 1 standby fine up."); } }
        public static XPlaneCommand RadiosActvNav2CoarseDown { get { return GetXPlaneCommand("sim/radios/actv_nav2_coarse_down", "NAV 2 coarse down."); } }
        public static XPlaneCommand RadiosActvNav2CoarseUp { get { return GetXPlaneCommand("sim/radios/actv_nav2_coarse_up", "NAV 2 coarse up."); } }
        public static XPlaneCommand RadiosActvNav2FineDown { get { return GetXPlaneCommand("sim/radios/actv_nav2_fine_down", "NAV 2 fine down."); } }
        public static XPlaneCommand RadiosActvNav2FineUp { get { return GetXPlaneCommand("sim/radios/actv_nav2_fine_up", "NAV 2 fine up."); } }
        public static XPlaneCommand RadiosStbyNav2CoarseDown { get { return GetXPlaneCommand("sim/radios/stby_nav2_coarse_down", "NAV 2 standby coarse down."); } }
        public static XPlaneCommand RadiosStbyNav2CoarseUp { get { return GetXPlaneCommand("sim/radios/stby_nav2_coarse_up", "NAV 2 standby coarse up."); } }
        public static XPlaneCommand RadiosStbyNav2FineDown { get { return GetXPlaneCommand("sim/radios/stby_nav2_fine_down", "NAV 2 standby fine down."); } }
        public static XPlaneCommand RadiosStbyNav2FineUp { get { return GetXPlaneCommand("sim/radios/stby_nav2_fine_up", "NAV 2 standby fine up."); } }
        public static XPlaneCommand RadiosActvDmeCoarseDown { get { return GetXPlaneCommand("sim/radios/actv_dme_coarse_down", "DME coarse down."); } }
        public static XPlaneCommand RadiosActvDmeCoarseUp { get { return GetXPlaneCommand("sim/radios/actv_dme_coarse_up", "DME coarse up."); } }
        public static XPlaneCommand RadiosActvDmeFineDown { get { return GetXPlaneCommand("sim/radios/actv_dme_fine_down", "DME fine down."); } }
        public static XPlaneCommand RadiosActvDmeFineUp { get { return GetXPlaneCommand("sim/radios/actv_dme_fine_up", "DME fine up."); } }
        public static XPlaneCommand RadiosStbyDmeCoarseDown { get { return GetXPlaneCommand("sim/radios/stby_dme_coarse_down", "DME standby coarse down."); } }
        public static XPlaneCommand RadiosStbyDmeCoarseUp { get { return GetXPlaneCommand("sim/radios/stby_dme_coarse_up", "DME standby coarse up."); } }
        public static XPlaneCommand RadiosStbyDmeFineDown { get { return GetXPlaneCommand("sim/radios/stby_dme_fine_down", "DME standby fine down."); } }
        public static XPlaneCommand RadiosStbyDmeFineUp { get { return GetXPlaneCommand("sim/radios/stby_dme_fine_up", "DME standby fine up."); } }
        public static XPlaneCommand RadiosActvAdf1HundredsDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_hundreds_down", "ADF active 1 hundreds down."); } }
        public static XPlaneCommand RadiosActvAdf1HundredsUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_hundreds_up", "ADF active 1 hundreds up."); } }
        public static XPlaneCommand RadiosActvAdf1TensDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_tens_down", "ADF active 1 tens down."); } }
        public static XPlaneCommand RadiosActvAdf1TensUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_tens_up", "ADF active 1 tens up."); } }
        public static XPlaneCommand RadiosActvAdf1OnesDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_ones_down", "ADF active 1 ones down."); } }
        public static XPlaneCommand RadiosActvAdf1OnesUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_ones_up", "ADF active 1 ones up."); } }
        public static XPlaneCommand RadiosActvAdf1OnesTensDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_ones_tens_down", "ADF active 1 ones and tens down."); } }
        public static XPlaneCommand RadiosActvAdf1OnesTensUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_ones_tens_up", "ADF active 1 ones and tens up."); } }
        public static XPlaneCommand RadiosActvAdf1HundredsThousDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_hundreds_thous_down", "ADF active 1 hundreds and thousands down."); } }
        public static XPlaneCommand RadiosActvAdf1HundredsThousUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_hundreds_thous_up", "ADF active 1 hundreds and thousands up."); } }
        public static XPlaneCommand RadiosActvAdf14DigHundredsDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_hundreds_down", "4-digit ADF active 1 hundreds down."); } }
        public static XPlaneCommand RadiosActvAdf14DigHundredsUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_hundreds_up", "4-digit ADF active 1 hundreds up."); } }
        public static XPlaneCommand RadiosActvAdf14DigTensDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_tens_down", "4-digit ADF active 1 tens down."); } }
        public static XPlaneCommand RadiosActvAdf14DigTensUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_tens_up", "4-digit ADF active 1 tens up."); } }
        public static XPlaneCommand RadiosActvAdf14DigOnesDown { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_ones_down", "4-digit ADF active 1 ones down."); } }
        public static XPlaneCommand RadiosActvAdf14DigOnesUp { get { return GetXPlaneCommand("sim/radios/actv_adf1_4dig_ones_up", "4-digit ADF active 1 ones up."); } }
        public static XPlaneCommand RadiosStbyAdf1HundredsDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_hundreds_down", "ADF standby 1 hundreds down."); } }
        public static XPlaneCommand RadiosStbyAdf1HundredsUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_hundreds_up", "ADF standby 1 hundreds up."); } }
        public static XPlaneCommand RadiosStbyAdf1TensDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_tens_down", "ADF standby 1 tens down."); } }
        public static XPlaneCommand RadiosStbyAdf1TensUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_tens_up", "ADF standby 1 tens up."); } }
        public static XPlaneCommand RadiosStbyAdf1OnesDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_ones_down", "ADF standby 1 ones down."); } }
        public static XPlaneCommand RadiosStbyAdf1OnesUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_ones_up", "ADF standby 1 ones up."); } }
        public static XPlaneCommand RadiosStbyAdf1OnesTensDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_ones_tens_down", "ADF standby 1 ones and tens down."); } }
        public static XPlaneCommand RadiosStbyAdf1OnesTensUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_ones_tens_up", "ADF standby 1 ones and tens up."); } }
        public static XPlaneCommand RadiosStbyAdf1HundredsThousDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_hundreds_thous_down", "ADF standby 1 hundreds and thousands down."); } }
        public static XPlaneCommand RadiosStbyAdf1HundredsThousUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_hundreds_thous_up", "ADF standby 1 hundreds and thousands up."); } }
        public static XPlaneCommand RadiosStbyAdf14DigHundredsDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_hundreds_down", "4-digit ADF standby 1 hundreds down."); } }
        public static XPlaneCommand RadiosStbyAdf14DigHundredsUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_hundreds_up", "4-digit ADF standby 1 hundreds up."); } }
        public static XPlaneCommand RadiosStbyAdf14DigTensDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_tens_down", "4-digit ADF standby 1 tens down."); } }
        public static XPlaneCommand RadiosStbyAdf14DigTensUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_tens_up", "4-digit ADF standby 1 tens up."); } }
        public static XPlaneCommand RadiosStbyAdf14DigOnesDown { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_ones_down", "4-digit ADF standby 1 ones down."); } }
        public static XPlaneCommand RadiosStbyAdf14DigOnesUp { get { return GetXPlaneCommand("sim/radios/stby_adf1_4dig_ones_up", "4-digit ADF standby 1 ones up."); } }
        public static XPlaneCommand RadiosActvAdf2HundredsDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_hundreds_down", "ADF active 2 hundreds down."); } }
        public static XPlaneCommand RadiosActvAdf2HundredsUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_hundreds_up", "ADF active 2 hundreds up."); } }
        public static XPlaneCommand RadiosActvAdf2TensDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_tens_down", "ADF active 2 tens down."); } }
        public static XPlaneCommand RadiosActvAdf2TensUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_tens_up", "ADF active 2 tens up."); } }
        public static XPlaneCommand RadiosActvAdf2OnesDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_ones_down", "ADF active 2 ones down."); } }
        public static XPlaneCommand RadiosActvAdf2OnesUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_ones_up", "ADF active 2 ones up."); } }
        public static XPlaneCommand RadiosActvAdf2OnesTensDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_ones_tens_down", "ADF active 2 ones and tens down."); } }
        public static XPlaneCommand RadiosActvAdf2OnesTensUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_ones_tens_up", "ADF active 2 ones and tens up."); } }
        public static XPlaneCommand RadiosActvAdf2HundredsThousDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_hundreds_thous_down", "ADF active 2 hundreds and thousands down."); } }
        public static XPlaneCommand RadiosActvAdf2HundredsThousUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_hundreds_thous_up", "ADF active 2 hundreds and thousands up."); } }
        public static XPlaneCommand RadiosActvAdf24DigHundredsDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_hundreds_down", "4-digit ADF active 2 hundreds down."); } }
        public static XPlaneCommand RadiosActvAdf24DigHundredsUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_hundreds_up", "4-digit ADF active 2 hundreds up."); } }
        public static XPlaneCommand RadiosActvAdf24DigTensDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_tens_down", "4-digit ADF active 2 tens down."); } }
        public static XPlaneCommand RadiosActvAdf24DigTensUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_tens_up", "4-digit ADF active 2 tens up."); } }
        public static XPlaneCommand RadiosActvAdf24DigOnesDown { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_ones_down", "4-digit ADF active 2 ones down."); } }
        public static XPlaneCommand RadiosActvAdf24DigOnesUp { get { return GetXPlaneCommand("sim/radios/actv_adf2_4dig_ones_up", "4-digit ADF active 2 ones up."); } }
        public static XPlaneCommand RadiosStbyAdf2HundredsDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_hundreds_down", "ADF standby 2 hundreds down."); } }
        public static XPlaneCommand RadiosStbyAdf2HundredsUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_hundreds_up", "ADF standby 2 hundreds up."); } }
        public static XPlaneCommand RadiosStbyAdf2TensDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_tens_down", "ADF standby 2 tens down."); } }
        public static XPlaneCommand RadiosStbyAdf2TensUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_tens_up", "ADF standby 2 tens up."); } }
        public static XPlaneCommand RadiosStbyAdf2OnesDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_ones_down", "ADF standby 2 ones down."); } }
        public static XPlaneCommand RadiosStbyAdf2OnesUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_ones_up", "ADF standby 2 ones up."); } }
        public static XPlaneCommand RadiosStbyAdf2OnesTensDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_ones_tens_down", "ADF standby 2 ones and tens down."); } }
        public static XPlaneCommand RadiosStbyAdf2OnesTensUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_ones_tens_up", "ADF standby 2 ones and tens up."); } }
        public static XPlaneCommand RadiosStbyAdf2HundredsThousDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_hundreds_thous_down", "ADF standby 2 hundreds and thousands down."); } }
        public static XPlaneCommand RadiosStbyAdf2HundredsThousUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_hundreds_thous_up", "ADF standby 2 hundreds and thousands up."); } }
        public static XPlaneCommand RadiosStbyAdf24DigHundredsDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_hundreds_down", "4-digit ADF standby 2 hundreds down."); } }
        public static XPlaneCommand RadiosStbyAdf24DigHundredsUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_hundreds_up", "4-digit ADF standby 2 hundreds up."); } }
        public static XPlaneCommand RadiosStbyAdf24DigTensDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_tens_down", "4-digit ADF standby 2 tens down."); } }
        public static XPlaneCommand RadiosStbyAdf24DigTensUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_tens_up", "4-digit ADF standby 2 tens up."); } }
        public static XPlaneCommand RadiosStbyAdf24DigOnesDown { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_ones_down", "4-digit ADF standby 2 ones down."); } }
        public static XPlaneCommand RadiosStbyAdf24DigOnesUp { get { return GetXPlaneCommand("sim/radios/stby_adf2_4dig_ones_up", "4-digit ADF standby 2 ones up."); } }
        public static XPlaneCommand RadiosNav1StandyFlip { get { return GetXPlaneCommand("sim/radios/nav1_standy_flip", "NAV 1 flip standby."); } }
        public static XPlaneCommand RadiosNav2StandyFlip { get { return GetXPlaneCommand("sim/radios/nav2_standy_flip", "NAV 2 flip standby."); } }
        public static XPlaneCommand RadiosCom1StandyFlip { get { return GetXPlaneCommand("sim/radios/com1_standy_flip", "COM 1 flip standby."); } }
        public static XPlaneCommand RadiosCom2StandyFlip { get { return GetXPlaneCommand("sim/radios/com2_standy_flip", "COM 2 flip standby."); } }
        public static XPlaneCommand RadiosAdf1StandyFlip { get { return GetXPlaneCommand("sim/radios/adf1_standy_flip", "ADF 1 flip standby."); } }
        public static XPlaneCommand RadiosAdf2StandyFlip { get { return GetXPlaneCommand("sim/radios/adf2_standy_flip", "ADF 2 flip standby."); } }
        public static XPlaneCommand RadiosDmeStandbyFlip { get { return GetXPlaneCommand("sim/radios/dme_standby_flip", "DME flip standby."); } }
        public static XPlaneCommand RadiosRMILTog { get { return GetXPlaneCommand("sim/radios/RMI_L_tog", "RMI left NAV toggle VOR/NDB."); } }
        public static XPlaneCommand RadiosRMIRTog { get { return GetXPlaneCommand("sim/radios/RMI_R_tog", "RMI right NAV toggle VOR/NDB."); } }
        public static XPlaneCommand RadiosCopilotRMILTogCop { get { return GetXPlaneCommand("sim/radios/copilot_RMI_L_tog_cop", "Copilot RMI left NAV toggle VOR/NDB."); } }
        public static XPlaneCommand RadiosCopilotRMIRTogCop { get { return GetXPlaneCommand("sim/radios/copilot_RMI_R_tog_cop", "Copilot RMI right NAV toggle VOR/NDB."); } }
        public static XPlaneCommand RadiosObs1Down { get { return GetXPlaneCommand("sim/radios/obs1_down", "OBS 1 down."); } }
        public static XPlaneCommand RadiosObs1Up { get { return GetXPlaneCommand("sim/radios/obs1_up", "OBS 1 up."); } }
        public static XPlaneCommand RadiosObs2Down { get { return GetXPlaneCommand("sim/radios/obs2_down", "OBS 2 down."); } }
        public static XPlaneCommand RadiosObs2Up { get { return GetXPlaneCommand("sim/radios/obs2_up", "OBS 2 up."); } }
        public static XPlaneCommand RadiosObsHSIDown { get { return GetXPlaneCommand("sim/radios/obs_HSI_down", "OBS HSI down."); } }
        public static XPlaneCommand RadiosObsHSIUp { get { return GetXPlaneCommand("sim/radios/obs_HSI_up", "OBS HSI up."); } }
        public static XPlaneCommand RadiosObsHSIDirect { get { return GetXPlaneCommand("sim/radios/obs_HSI_direct", "OBS HSI direct course."); } }
        public static XPlaneCommand RadiosAdf1CardDown { get { return GetXPlaneCommand("sim/radios/adf1_card_down", "ADF card 1 down."); } }
        public static XPlaneCommand RadiosAdf1CardUp { get { return GetXPlaneCommand("sim/radios/adf1_card_up", "ADF card 1 up."); } }
        public static XPlaneCommand RadiosAdf2CardDown { get { return GetXPlaneCommand("sim/radios/adf2_card_down", "ADF card 2 down."); } }
        public static XPlaneCommand RadiosAdf2CardUp { get { return GetXPlaneCommand("sim/radios/adf2_card_up", "ADF card 2 up."); } }
        public static XPlaneCommand RadiosCopilotObs1Down { get { return GetXPlaneCommand("sim/radios/copilot_obs1_down", "Copilot OBS 1 down."); } }
        public static XPlaneCommand RadiosCopilotObs1Up { get { return GetXPlaneCommand("sim/radios/copilot_obs1_up", "Copilot OBS 1 up."); } }
        public static XPlaneCommand RadiosCopilotObs2Down { get { return GetXPlaneCommand("sim/radios/copilot_obs2_down", "Copilot OBS 2 down."); } }
        public static XPlaneCommand RadiosCopilotObs2Up { get { return GetXPlaneCommand("sim/radios/copilot_obs2_up", "Copilot OBS 2 up."); } }
        public static XPlaneCommand RadiosCopilotObsHSIDown { get { return GetXPlaneCommand("sim/radios/copilot_obs_HSI_down", "Copilot OBS HSI down."); } }
        public static XPlaneCommand RadiosCopilotObsHSIUp { get { return GetXPlaneCommand("sim/radios/copilot_obs_HSI_up", "Copilot OBS HSI up."); } }
        public static XPlaneCommand RadiosCopilotObsHSIDirect { get { return GetXPlaneCommand("sim/radios/copilot_obs_HSI_direct", "Copilot OBS HSI direct course."); } }
        public static XPlaneCommand RadiosCopilotAdf1CardDown { get { return GetXPlaneCommand("sim/radios/copilot_adf1_card_down", "Copilot ADF card 1 down."); } }
        public static XPlaneCommand RadiosCopilotAdf1CardUp { get { return GetXPlaneCommand("sim/radios/copilot_adf1_card_up", "Copilot ADF card 1 up."); } }
        public static XPlaneCommand RadiosCopilotAdf2CardDown { get { return GetXPlaneCommand("sim/radios/copilot_adf2_card_down", "Copilot ADF card 2 down."); } }
        public static XPlaneCommand RadiosCopilotAdf2CardUp { get { return GetXPlaneCommand("sim/radios/copilot_adf2_card_up", "Copilot ADF card 2 up."); } }

    }
}