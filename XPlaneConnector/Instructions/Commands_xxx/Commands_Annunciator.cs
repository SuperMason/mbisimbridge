﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand AnnunciatorTestAllAnnunciators { get { return GetXPlaneCommand("sim/annunciator/test_all_annunciators", "Test all annunciators."); } }
        public static XPlaneCommand AnnunciatorTestStall { get { return GetXPlaneCommand("sim/annunciator/test_stall", "Test stall warn."); } }
        public static XPlaneCommand AnnunciatorTestFire1Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_1_annun", "Test fire 1 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire2Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_2_annun", "Test fire 2 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire3Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_3_annun", "Test fire 3 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire4Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_4_annun", "Test fire 4 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire5Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_5_annun", "Test fire 5 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire6Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_6_annun", "Test fire 6 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire7Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_7_annun", "Test fire 7 annunciator."); } }
        public static XPlaneCommand AnnunciatorTestFire8Annun { get { return GetXPlaneCommand("sim/annunciator/test_fire_8_annun", "Test fire 8 annunciator."); } }
        public static XPlaneCommand AnnunciatorClearMasterCaution { get { return GetXPlaneCommand("sim/annunciator/clear_master_caution", "Clear master caution."); } }
        public static XPlaneCommand AnnunciatorClearMasterWarning { get { return GetXPlaneCommand("sim/annunciator/clear_master_warning", "Clear master warning."); } }
        public static XPlaneCommand AnnunciatorClearMasterAccept { get { return GetXPlaneCommand("sim/annunciator/clear_master_accept", "Clear master accept."); } }
        public static XPlaneCommand AnnunciatorGearWarningMute { get { return GetXPlaneCommand("sim/annunciator/gear_warning_mute", "Mute gear warning horn."); } }
        public static XPlaneCommand AnnunciatorMarkerBeaconMute { get { return GetXPlaneCommand("sim/annunciator/marker_beacon_mute", "Mute marker beacons until next marker is received."); } }
        public static XPlaneCommand AnnunciatorMarkerBeaconMuteOrOff { get { return GetXPlaneCommand("sim/annunciator/marker_beacon_mute_or_off", "Mute marker beacons until next marker is received or indefinitely if none is received right now."); } }
        public static XPlaneCommand AnnunciatorMarkerBeaconSensHi { get { return GetXPlaneCommand("sim/annunciator/marker_beacon_sens_hi", "Marker beacon receiver hi sens."); } }
        public static XPlaneCommand AnnunciatorMarkerBeaconSensLo { get { return GetXPlaneCommand("sim/annunciator/marker_beacon_sens_lo", "Marker beacon receiver lo sens."); } }
        public static XPlaneCommand AnnunciatorMarkerBeaconSensToggle { get { return GetXPlaneCommand("sim/annunciator/marker_beacon_sens_toggle", "Marker beacon receiver sens toggle."); } }
    }
}