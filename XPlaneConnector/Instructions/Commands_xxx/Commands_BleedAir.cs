﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand BleedAirBleedAirDown { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_down", "Bleed air mode down."); } }
        public static XPlaneCommand BleedAirBleedAirUp { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_up", "Bleed air mode up."); } }
        public static XPlaneCommand BleedAirBleedAirOff { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_off", "Bleed air off."); } }
        public static XPlaneCommand BleedAirBleedAirLeft { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_left", "Bleed air from left engine."); } }
        public static XPlaneCommand BleedAirBleedAirBoth { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_both", "Bleed air from either engine."); } }
        public static XPlaneCommand BleedAirBleedAirRight { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_right", "Bleed air from right engine."); } }
        public static XPlaneCommand BleedAirBleedAirApu { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_apu", "Bleed air from APU."); } }
        public static XPlaneCommand BleedAirBleedAirAuto { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_auto", "Bleed air from any engine or APU."); } }
        public static XPlaneCommand BleedAirBleedAirLeftOn { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_left_on", "Bleed air left engine on."); } }
        public static XPlaneCommand BleedAirBleedAirLeftInsOnly { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_left_ins_only", "Bleed air left engine instruments only."); } }
        public static XPlaneCommand BleedAirBleedAirLeftOff { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_left_off", "Bleed air left engine off."); } }
        public static XPlaneCommand BleedAirBleedAirRightOn { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_right_on", "Bleed air right engine on."); } }
        public static XPlaneCommand BleedAirBleedAirRightInsOnly { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_right_ins_only", "Bleed air right engine instruments only."); } }
        public static XPlaneCommand BleedAirBleedAirRightOff { get { return GetXPlaneCommand("sim/bleed_air/bleed_air_right_off", "Bleed air right engine off."); } }
        public static XPlaneCommand BleedAirEngine1Off { get { return GetXPlaneCommand("sim/bleed_air/engine_1_off", "Bleed air shut off engine 1."); } }
        public static XPlaneCommand BleedAirEngine2Off { get { return GetXPlaneCommand("sim/bleed_air/engine_2_off", "Bleed air shut off engine 2."); } }
        public static XPlaneCommand BleedAirEngine3Off { get { return GetXPlaneCommand("sim/bleed_air/engine_3_off", "Bleed air shut off engine 3."); } }
        public static XPlaneCommand BleedAirEngine4Off { get { return GetXPlaneCommand("sim/bleed_air/engine_4_off", "Bleed air shut off engine 4."); } }
        public static XPlaneCommand BleedAirEngine5Off { get { return GetXPlaneCommand("sim/bleed_air/engine_5_off", "Bleed air shut off engine 5."); } }
        public static XPlaneCommand BleedAirEngine6Off { get { return GetXPlaneCommand("sim/bleed_air/engine_6_off", "Bleed air shut off engine 6."); } }
        public static XPlaneCommand BleedAirEngine7Off { get { return GetXPlaneCommand("sim/bleed_air/engine_7_off", "Bleed air shut off engine 7."); } }
        public static XPlaneCommand BleedAirEngine8Off { get { return GetXPlaneCommand("sim/bleed_air/engine_8_off", "Bleed air shut off engine 8."); } }
        public static XPlaneCommand BleedAirEngine1On { get { return GetXPlaneCommand("sim/bleed_air/engine_1_on", "Bleed air on engine 1."); } }
        public static XPlaneCommand BleedAirEngine2On { get { return GetXPlaneCommand("sim/bleed_air/engine_2_on", "Bleed air on engine 2."); } }
        public static XPlaneCommand BleedAirEngine3On { get { return GetXPlaneCommand("sim/bleed_air/engine_3_on", "Bleed air on engine 3."); } }
        public static XPlaneCommand BleedAirEngine4On { get { return GetXPlaneCommand("sim/bleed_air/engine_4_on", "Bleed air on engine 4."); } }
        public static XPlaneCommand BleedAirEngine5On { get { return GetXPlaneCommand("sim/bleed_air/engine_5_on", "Bleed air on engine 5."); } }
        public static XPlaneCommand BleedAirEngine6On { get { return GetXPlaneCommand("sim/bleed_air/engine_6_on", "Bleed air on engine 6."); } }
        public static XPlaneCommand BleedAirEngine7On { get { return GetXPlaneCommand("sim/bleed_air/engine_7_on", "Bleed air on engine 7."); } }
        public static XPlaneCommand BleedAirEngine8On { get { return GetXPlaneCommand("sim/bleed_air/engine_8_on", "Bleed air on engine 8."); } }
        public static XPlaneCommand BleedAirEngine1Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_1_toggle", "Bleed air toggle engine 1."); } }
        public static XPlaneCommand BleedAirEngine2Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_2_toggle", "Bleed air toggle engine 2."); } }
        public static XPlaneCommand BleedAirEngine3Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_3_toggle", "Bleed air toggle engine 3."); } }
        public static XPlaneCommand BleedAirEngine4Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_4_toggle", "Bleed air toggle engine 4."); } }
        public static XPlaneCommand BleedAirEngine5Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_5_toggle", "Bleed air toggle engine 5."); } }
        public static XPlaneCommand BleedAirEngine6Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_6_toggle", "Bleed air toggle engine 6."); } }
        public static XPlaneCommand BleedAirEngine7Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_7_toggle", "Bleed air toggle engine 7."); } }
        public static XPlaneCommand BleedAirEngine8Toggle { get { return GetXPlaneCommand("sim/bleed_air/engine_8_toggle", "Bleed air toggle engine 8."); } }
        public static XPlaneCommand BleedAirGpuOff { get { return GetXPlaneCommand("sim/bleed_air/gpu_off", "Bleed air shut off GPU."); } }
        public static XPlaneCommand BleedAirGpuOn { get { return GetXPlaneCommand("sim/bleed_air/gpu_on", "Bleed air on GPU."); } }
        public static XPlaneCommand BleedAirGpuToggle { get { return GetXPlaneCommand("sim/bleed_air/gpu_toggle", "Bleed air toggle GPU."); } }
        public static XPlaneCommand BleedAirApuOff { get { return GetXPlaneCommand("sim/bleed_air/apu_off", "Bleed air shut off APU."); } }
        public static XPlaneCommand BleedAirApuOn { get { return GetXPlaneCommand("sim/bleed_air/apu_on", "Bleed air on APU."); } }
        public static XPlaneCommand BleedAirApuToggle { get { return GetXPlaneCommand("sim/bleed_air/apu_toggle", "Bleed air toggle APU."); } }
        public static XPlaneCommand BleedAirIsolationLeftShut { get { return GetXPlaneCommand("sim/bleed_air/isolation_left_shut", "Bleed air shut left isolation."); } }
        public static XPlaneCommand BleedAirIsolationLeftOpen { get { return GetXPlaneCommand("sim/bleed_air/isolation_left_open", "Bleed air open left isolation."); } }
        public static XPlaneCommand BleedAirIsolationLeftToggle { get { return GetXPlaneCommand("sim/bleed_air/isolation_left_toggle", "Bleed air toggle left isolation."); } }
        public static XPlaneCommand BleedAirIsolationRightShut { get { return GetXPlaneCommand("sim/bleed_air/isolation_right_shut", "Bleed air shut right isolation."); } }
        public static XPlaneCommand BleedAirIsolationRightOpen { get { return GetXPlaneCommand("sim/bleed_air/isolation_right_open", "Bleed air open right isolation."); } }
        public static XPlaneCommand BleedAirIsolationRightToggle { get { return GetXPlaneCommand("sim/bleed_air/isolation_right_toggle", "Bleed air toggle right isolation."); } }
        public static XPlaneCommand BleedAirPackLeftOff { get { return GetXPlaneCommand("sim/bleed_air/pack_left_off", "Bleed air left pack off."); } }
        public static XPlaneCommand BleedAirPackLeftOn { get { return GetXPlaneCommand("sim/bleed_air/pack_left_on", "Bleed air left pack on."); } }
        public static XPlaneCommand BleedAirPackLeftToggle { get { return GetXPlaneCommand("sim/bleed_air/pack_left_toggle", "Bleed air left pack toggle."); } }
        public static XPlaneCommand BleedAirPackCenterOff { get { return GetXPlaneCommand("sim/bleed_air/pack_center_off", "Bleed air center pack off."); } }
        public static XPlaneCommand BleedAirPackCenterOn { get { return GetXPlaneCommand("sim/bleed_air/pack_center_on", "Bleed air center pack on."); } }
        public static XPlaneCommand BleedAirPackCenterToggle { get { return GetXPlaneCommand("sim/bleed_air/pack_center_toggle", "Bleed air center pack toggle."); } }
        public static XPlaneCommand BleedAirPackRightOff { get { return GetXPlaneCommand("sim/bleed_air/pack_right_off", "Bleed air right pack off."); } }
        public static XPlaneCommand BleedAirPackRightOn { get { return GetXPlaneCommand("sim/bleed_air/pack_right_on", "Bleed air right pack on."); } }
        public static XPlaneCommand BleedAirPackRightToggle { get { return GetXPlaneCommand("sim/bleed_air/pack_right_toggle", "Bleed air right pack toggle."); } }
    }
}