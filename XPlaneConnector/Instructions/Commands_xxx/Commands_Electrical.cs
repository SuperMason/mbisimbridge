﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        /// <summary>
        /// Cross-tie on.
        /// </summary>
        public static XPlaneCommand ElectricalCrossTieOn { get { return GetXPlaneCommand("sim/electrical/cross_tie_on", "Cross-tie on."); } }
        /// <summary>
        /// Cross-tie off.
        /// </summary>
        public static XPlaneCommand ElectricalCrossTieOff { get { return GetXPlaneCommand("sim/electrical/cross_tie_off", "Cross-tie off."); } }
        /// <summary>
        /// Cross-tie toggle.
        /// </summary>
        public static XPlaneCommand ElectricalCrossTieToggle { get { return GetXPlaneCommand("sim/electrical/cross_tie_toggle", "Cross-tie toggle."); } }
        /// <summary>
        /// Inverters on.
        /// </summary>
        public static XPlaneCommand ElectricalInvertersOn { get { return GetXPlaneCommand("sim/electrical/inverters_on", "Inverters on."); } }
        /// <summary>
        /// Inverters off.
        /// </summary>
        public static XPlaneCommand ElectricalInvertersOff { get { return GetXPlaneCommand("sim/electrical/inverters_off", "Inverters off."); } }
        /// <summary>
        /// Inverters toggle.
        /// </summary>
        public static XPlaneCommand ElectricalInvertersToggle { get { return GetXPlaneCommand("sim/electrical/inverters_toggle", "Inverters toggle."); } }
        /// <summary>
        /// Inverter 1 on.
        /// </summary>
        public static XPlaneCommand ElectricalInverter1On { get { return GetXPlaneCommand("sim/electrical/inverter_1_on", "Inverter 1 on."); } }
        /// <summary>
        /// Inverter 1 off.
        /// </summary>
        public static XPlaneCommand ElectricalInverter1Off { get { return GetXPlaneCommand("sim/electrical/inverter_1_off", "Inverter 1 off."); } }
        /// <summary>
        /// Inverter 1 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalInverter1Toggle { get { return GetXPlaneCommand("sim/electrical/inverter_1_toggle", "Inverter 1 toggle."); } }
        /// <summary>
        /// Inverter 2 on.
        /// </summary>
        public static XPlaneCommand ElectricalInverter2On { get { return GetXPlaneCommand("sim/electrical/inverter_2_on", "Inverter 2 on."); } }
        /// <summary>
        /// Inverter 2 off.
        /// </summary>
        public static XPlaneCommand ElectricalInverter2Off { get { return GetXPlaneCommand("sim/electrical/inverter_2_off", "Inverter 2 off."); } }
        /// <summary>
        /// Inverter 2 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalInverter2Toggle { get { return GetXPlaneCommand("sim/electrical/inverter_2_toggle", "Inverter 2 toggle."); } }
        /// <summary>
        /// Batteries all toggle.
        /// </summary>
        public static XPlaneCommand ElectricalBatteriesToggle { get { return GetXPlaneCommand("sim/electrical/batteries_toggle", "Batteries all toggle."); } }
        /// <summary>
        /// Battery 1 on.
        /// </summary>
        public static XPlaneCommand ElectricalBattery1On { get { return GetXPlaneCommand("sim/electrical/battery_1_on", "Battery 1 on."); } }
        /// <summary>
        /// Battery 2 on.
        /// </summary>
        public static XPlaneCommand ElectricalBattery2On { get { return GetXPlaneCommand("sim/electrical/battery_2_on", "Battery 2 on."); } }
        /// <summary>
        /// Battery 1 off.
        /// </summary>
        public static XPlaneCommand ElectricalBattery1Off { get { return GetXPlaneCommand("sim/electrical/battery_1_off", "Battery 1 off."); } }
        /// <summary>
        /// Battery 2 off.
        /// </summary>
        public static XPlaneCommand ElectricalBattery2Off { get { return GetXPlaneCommand("sim/electrical/battery_2_off", "Battery 2 off."); } }
        /// <summary>
        /// Battery 1 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalBattery1Toggle { get { return GetXPlaneCommand("sim/electrical/battery_1_toggle", "Battery 1 toggle."); } }
        /// <summary>
        /// Battery 2 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalBattery2Toggle { get { return GetXPlaneCommand("sim/electrical/battery_2_toggle", "Battery 2 toggle."); } }
        /// <summary>
        /// Generators all toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGeneratorsToggle { get { return GetXPlaneCommand("sim/electrical/generators_toggle", "Generators all toggle."); } }
        /// <summary>
        /// Generator #1 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator1Off { get { return GetXPlaneCommand("sim/electrical/generator_1_off", "Generator #1 off."); } }
        /// <summary>
        /// Generator #2 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator2Off { get { return GetXPlaneCommand("sim/electrical/generator_2_off", "Generator #2 off."); } }
        /// <summary>
        /// Generator #3 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator3Off { get { return GetXPlaneCommand("sim/electrical/generator_3_off", "Generator #3 off."); } }
        /// <summary>
        /// Generator #4 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator4Off { get { return GetXPlaneCommand("sim/electrical/generator_4_off", "Generator #4 off."); } }
        /// <summary>
        /// Generator #5 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator5Off { get { return GetXPlaneCommand("sim/electrical/generator_5_off", "Generator #5 off."); } }
        /// <summary>
        /// Generator #6 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator6Off { get { return GetXPlaneCommand("sim/electrical/generator_6_off", "Generator #6 off."); } }
        /// <summary>
        /// Generator #7 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator7Off { get { return GetXPlaneCommand("sim/electrical/generator_7_off", "Generator #7 off."); } }
        /// <summary>
        /// Generator #8 off.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator8Off { get { return GetXPlaneCommand("sim/electrical/generator_8_off", "Generator #8 off."); } }
        /// <summary>
        /// Generator #1 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator1On { get { return GetXPlaneCommand("sim/electrical/generator_1_on", "Generator #1 on."); } }
        /// <summary>
        /// Generator #2 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator2On { get { return GetXPlaneCommand("sim/electrical/generator_2_on", "Generator #2 on."); } }
        /// <summary>
        /// Generator #3 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator3On { get { return GetXPlaneCommand("sim/electrical/generator_3_on", "Generator #3 on."); } }
        /// <summary>
        /// Generator #4 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator4On { get { return GetXPlaneCommand("sim/electrical/generator_4_on", "Generator #4 on."); } }
        /// <summary>
        /// Generator #5 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator5On { get { return GetXPlaneCommand("sim/electrical/generator_5_on", "Generator #5 on."); } }
        /// <summary>
        /// Generator #6 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator6On { get { return GetXPlaneCommand("sim/electrical/generator_6_on", "Generator #6 on."); } }
        /// <summary>
        /// Generator #7 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator7On { get { return GetXPlaneCommand("sim/electrical/generator_7_on", "Generator #7 on."); } }
        /// <summary>
        /// Generator #8 on.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator8On { get { return GetXPlaneCommand("sim/electrical/generator_8_on", "Generator #8 on."); } }
        /// <summary>
        /// Generator #1 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator1Toggle { get { return GetXPlaneCommand("sim/electrical/generator_1_toggle", "Generator #1 toggle."); } }
        /// <summary>
        /// Generator #2 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator2Toggle { get { return GetXPlaneCommand("sim/electrical/generator_2_toggle", "Generator #2 toggle."); } }
        /// <summary>
        /// Generator #3 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator3Toggle { get { return GetXPlaneCommand("sim/electrical/generator_3_toggle", "Generator #3 toggle."); } }
        /// <summary>
        /// Generator #4 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator4Toggle { get { return GetXPlaneCommand("sim/electrical/generator_4_toggle", "Generator #4 toggle."); } }
        /// <summary>
        /// Generator #5 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator5Toggle { get { return GetXPlaneCommand("sim/electrical/generator_5_toggle", "Generator #5 toggle."); } }
        /// <summary>
        /// Generator #6 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator6Toggle { get { return GetXPlaneCommand("sim/electrical/generator_6_toggle", "Generator #6 toggle."); } }
        /// <summary>
        /// Generator #7 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator7Toggle { get { return GetXPlaneCommand("sim/electrical/generator_7_toggle", "Generator #7 toggle."); } }
        /// <summary>
        /// Generator #8 toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator8Toggle { get { return GetXPlaneCommand("sim/electrical/generator_8_toggle", "Generator #8 toggle."); } }
        /// <summary>
        /// Generator #1 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator1Reset { get { return GetXPlaneCommand("sim/electrical/generator_1_reset", "Generator #1 reset."); } }
        /// <summary>
        /// Generator #2 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator2Reset { get { return GetXPlaneCommand("sim/electrical/generator_2_reset", "Generator #2 reset."); } }
        /// <summary>
        /// Generator #3 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator3Reset { get { return GetXPlaneCommand("sim/electrical/generator_3_reset", "Generator #3 reset."); } }
        /// <summary>
        /// Generator #4 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator4Reset { get { return GetXPlaneCommand("sim/electrical/generator_4_reset", "Generator #4 reset."); } }
        /// <summary>
        /// Generator #5 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator5Reset { get { return GetXPlaneCommand("sim/electrical/generator_5_reset", "Generator #5 reset."); } }
        /// <summary>
        /// Generator #6 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator6Reset { get { return GetXPlaneCommand("sim/electrical/generator_6_reset", "Generator #6 reset."); } }
        /// <summary>
        /// Generator #7 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator7Reset { get { return GetXPlaneCommand("sim/electrical/generator_7_reset", "Generator #7 reset."); } }
        /// <summary>
        /// Generator #8 reset.
        /// </summary>
        public static XPlaneCommand ElectricalGenerator8Reset { get { return GetXPlaneCommand("sim/electrical/generator_8_reset", "Generator #8 reset."); } }
        /// <summary>
        /// APU start.
        /// </summary>
        public static XPlaneCommand ElectricalAPUStart { get { return GetXPlaneCommand("sim/electrical/APU_start", "APU start."); } }
        /// <summary>
        /// APU on.
        /// </summary>
        public static XPlaneCommand ElectricalAPUOn { get { return GetXPlaneCommand("sim/electrical/APU_on", "APU on."); } }
        /// <summary>
        /// APU off.
        /// </summary>
        public static XPlaneCommand ElectricalAPUOff { get { return GetXPlaneCommand("sim/electrical/APU_off", "APU off."); } }
        /// <summary>
        /// APU emergency shutoff with fire handle.
        /// </summary>
        public static XPlaneCommand ElectricalAPUFireShutoff { get { return GetXPlaneCommand("sim/electrical/APU_fire_shutoff", "APU emergency shutoff with fire handle."); } }
        /// <summary>
        /// APU generator on.
        /// </summary>
        public static XPlaneCommand ElectricalAPUGeneratorOn { get { return GetXPlaneCommand("sim/electrical/APU_generator_on", "APU generator on."); } }
        /// <summary>
        /// APU generator off.
        /// </summary>
        public static XPlaneCommand ElectricalAPUGeneratorOff { get { return GetXPlaneCommand("sim/electrical/APU_generator_off", "APU generator off."); } }
        /// <summary>
        /// GPU on.
        /// </summary>
        public static XPlaneCommand ElectricalGPUOn { get { return GetXPlaneCommand("sim/electrical/GPU_on", "GPU on."); } }
        /// <summary>
        /// GPU off.
        /// </summary>
        public static XPlaneCommand ElectricalGPUOff { get { return GetXPlaneCommand("sim/electrical/GPU_off", "GPU off."); } }
        /// <summary>
        /// GPU toggle.
        /// </summary>
        public static XPlaneCommand ElectricalGPUToggle { get { return GetXPlaneCommand("sim/electrical/GPU_toggle", "GPU toggle."); } }
        /// <summary>
        /// Re-charge batteries.
        /// </summary>
        public static XPlaneCommand ElectricalRecharge { get { return GetXPlaneCommand("sim/electrical/recharge", "Re-charge batteries."); } }
        /// <summary>
        /// DC Voltmeter down.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltDn { get { return GetXPlaneCommand("sim/electrical/dc_volt_dn", "DC Voltmeter down."); } }
        /// <summary>
        /// DC Voltmeter up.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltUp { get { return GetXPlaneCommand("sim/electrical/dc_volt_up", "DC Voltmeter up."); } }
        /// <summary>
        /// DC Voltmeter external power.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltExt { get { return GetXPlaneCommand("sim/electrical/dc_volt_ext", "DC Voltmeter external power."); } }
        /// <summary>
        /// DC Voltmeter center.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltCtr { get { return GetXPlaneCommand("sim/electrical/dc_volt_ctr", "DC Voltmeter center."); } }
        /// <summary>
        /// DC Voltmeter left.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltLft { get { return GetXPlaneCommand("sim/electrical/dc_volt_lft", "DC Voltmeter left."); } }
        /// <summary>
        /// DC Voltmeter right.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltRgt { get { return GetXPlaneCommand("sim/electrical/dc_volt_rgt", "DC Voltmeter right."); } }
        /// <summary>
        /// DC Voltmeter TPL-fed.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltTpl { get { return GetXPlaneCommand("sim/electrical/dc_volt_tpl", "DC Voltmeter TPL-fed."); } }
        /// <summary>
        /// DC Voltmeter battery.
        /// </summary>
        public static XPlaneCommand ElectricalDcVoltBat { get { return GetXPlaneCommand("sim/electrical/dc_volt_bat", "DC Voltmeter battery."); } }
    }
}