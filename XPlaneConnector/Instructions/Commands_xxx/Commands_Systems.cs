﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand SystemsYawDamperOn { get { return GetXPlaneCommand("sim/systems/yaw_damper_on", "Yaw damper on."); } }
        public static XPlaneCommand SystemsYawDamperOff { get { return GetXPlaneCommand("sim/systems/yaw_damper_off", "Yaw damper off."); } }
        public static XPlaneCommand SystemsYawDamperToggle { get { return GetXPlaneCommand("sim/systems/yaw_damper_toggle", "Toggle yaw damper."); } }
        public static XPlaneCommand SystemsPropSyncOn { get { return GetXPlaneCommand("sim/systems/prop_sync_on", "Prop sync on."); } }
        public static XPlaneCommand SystemsPropSyncOff { get { return GetXPlaneCommand("sim/systems/prop_sync_off", "Prop sync off."); } }
        public static XPlaneCommand SystemsPropSyncToggle { get { return GetXPlaneCommand("sim/systems/prop_sync_toggle", "Prop sync toggle."); } }
        public static XPlaneCommand SystemsFeatherModeDown { get { return GetXPlaneCommand("sim/systems/feather_mode_down", "Auto-feather mode down."); } }
        public static XPlaneCommand SystemsFeatherModeUp { get { return GetXPlaneCommand("sim/systems/feather_mode_up", "Auto-feather mode up."); } }
        public static XPlaneCommand SystemsFeatherModeOff { get { return GetXPlaneCommand("sim/systems/feather_mode_off", "Auto-feather off."); } }
        public static XPlaneCommand SystemsFeatherModeArm { get { return GetXPlaneCommand("sim/systems/feather_mode_arm", "Auto-feather on."); } }
        public static XPlaneCommand SystemsFeatherModeTest { get { return GetXPlaneCommand("sim/systems/feather_mode_test", "Auto-feather test."); } }
        public static XPlaneCommand SystemsArtificialStabilityToggle { get { return GetXPlaneCommand("sim/systems/artificial_stability_toggle", "Toggle artificial stability power."); } }
        public static XPlaneCommand SystemsAvionicsOn { get { return GetXPlaneCommand("sim/systems/avionics_on", "Avionics on."); } }
        public static XPlaneCommand SystemsAvionicsOff { get { return GetXPlaneCommand("sim/systems/avionics_off", "Avionics off."); } }
        public static XPlaneCommand SystemsAvionicsToggle { get { return GetXPlaneCommand("sim/systems/avionics_toggle", "Avionics toggle."); } }
        public static XPlaneCommand SystemsSeatbeltSignToggle { get { return GetXPlaneCommand("sim/systems/seatbelt_sign_toggle", "Toggle seatbelt sign."); } }
        public static XPlaneCommand SystemsNoSmokingToggle { get { return GetXPlaneCommand("sim/systems/no_smoking_toggle", "Toggle smoking sign."); } }
        public static XPlaneCommand SystemsWipersDn { get { return GetXPlaneCommand("sim/systems/wipers_dn", "Windshield wipers down."); } }
        public static XPlaneCommand SystemsWipersUp { get { return GetXPlaneCommand("sim/systems/wipers_up", "Windshield wipers up."); } }
        public static XPlaneCommand SystemsTotalEnergyAudioToggle { get { return GetXPlaneCommand("sim/systems/total_energy_audio_toggle", "Toggle total-energy audio."); } }
        public static XPlaneCommand SystemsPreRotateToggle { get { return GetXPlaneCommand("sim/systems/pre_rotate_toggle", "Toggle pre-rotate."); } }
        public static XPlaneCommand SystemsOverspeedTest { get { return GetXPlaneCommand("sim/systems/overspeed_test", "Prop overspeed test."); } }
    }
}