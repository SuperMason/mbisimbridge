﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand PressurizationTest { get { return GetXPlaneCommand("sim/pressurization/test", "Pressurization test."); } }
        public static XPlaneCommand PressurizationDumpOn { get { return GetXPlaneCommand("sim/pressurization/dump_on", "Dump pressurization on."); } }
        public static XPlaneCommand PressurizationDumpOff { get { return GetXPlaneCommand("sim/pressurization/dump_off", "Dump pressurization off."); } }
        public static XPlaneCommand PressurizationVviDown { get { return GetXPlaneCommand("sim/pressurization/vvi_down", "Cabin vertical speed down."); } }
        public static XPlaneCommand PressurizationVviUp { get { return GetXPlaneCommand("sim/pressurization/vvi_up", "Cabin vertical speed up."); } }
        public static XPlaneCommand PressurizationCabinAltDown { get { return GetXPlaneCommand("sim/pressurization/cabin_alt_down", "Cabin altitude down."); } }
        public static XPlaneCommand PressurizationCabinAltUp { get { return GetXPlaneCommand("sim/pressurization/cabin_alt_up", "Cabin altitude up."); } }
        public static XPlaneCommand PressurizationAircondOn { get { return GetXPlaneCommand("sim/pressurization/aircond_on", "Air conditioning on."); } }
        public static XPlaneCommand PressurizationAircondOff { get { return GetXPlaneCommand("sim/pressurization/aircond_off", "Air conditioning off."); } }
        public static XPlaneCommand PressurizationHeaterOn { get { return GetXPlaneCommand("sim/pressurization/heater_on", "Electric heater on."); } }
        public static XPlaneCommand PressurizationHeaterGrdMax { get { return GetXPlaneCommand("sim/pressurization/heater_grd_max", "Electric heater on ground max."); } }
        public static XPlaneCommand PressurizationHeaterOff { get { return GetXPlaneCommand("sim/pressurization/heater_off", "Electric heater off."); } }
        public static XPlaneCommand PressurizationHeaterUp { get { return GetXPlaneCommand("sim/pressurization/heater_up", "Electric heater off->on->grd max."); } }
        public static XPlaneCommand PressurizationHeaterDn { get { return GetXPlaneCommand("sim/pressurization/heater_dn", "Electric heater grd max->on->off."); } }
        public static XPlaneCommand PressurizationFanAuto { get { return GetXPlaneCommand("sim/pressurization/fan_auto", "Vent fan auto (on only with AC or heat)."); } }
        public static XPlaneCommand PressurizationFanLow { get { return GetXPlaneCommand("sim/pressurization/fan_low", "Vent fan on low."); } }
        public static XPlaneCommand PressurizationFanHigh { get { return GetXPlaneCommand("sim/pressurization/fan_high", "Vent fan high."); } }
        public static XPlaneCommand PressurizationFanUp { get { return GetXPlaneCommand("sim/pressurization/fan_up", "Vent fan setting up."); } }
        public static XPlaneCommand PressurizationFanDown { get { return GetXPlaneCommand("sim/pressurization/fan_down", "Vent fan setting down."); } }
    }
}