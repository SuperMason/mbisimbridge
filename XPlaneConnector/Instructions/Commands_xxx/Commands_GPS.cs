﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand GPSModeAirport { get { return GetXPlaneCommand("sim/GPS/mode_airport", "GPS mode: airports."); } }
        public static XPlaneCommand GPSModeVOR { get { return GetXPlaneCommand("sim/GPS/mode_VOR", "GPS mode: VORs."); } }
        public static XPlaneCommand GPSModeNDB { get { return GetXPlaneCommand("sim/GPS/mode_NDB", "GPS mode: NDBs."); } }
        public static XPlaneCommand GPSModeWaypoint { get { return GetXPlaneCommand("sim/GPS/mode_waypoint", "GPS mode: waypoints."); } }
        public static XPlaneCommand GPSFineSelectDown { get { return GetXPlaneCommand("sim/GPS/fine_select_down", "GPS fine select down."); } }
        public static XPlaneCommand GPSFineSelectUp { get { return GetXPlaneCommand("sim/GPS/fine_select_up", "GPS fine select up."); } }
        public static XPlaneCommand GPSCoarseSelectDown { get { return GetXPlaneCommand("sim/GPS/coarse_select_down", "GPS coarse select down."); } }
        public static XPlaneCommand GPSCoarseSelectUp { get { return GetXPlaneCommand("sim/GPS/coarse_select_up", "GPS coarse select up."); } }
        public static XPlaneCommand GPSG430n1CoarseDown { get { return GetXPlaneCommand("sim/GPS/g430n1_coarse_down", "GNS COM/NAV 1 coarse down."); } }
        public static XPlaneCommand GPSG430n1CoarseUp { get { return GetXPlaneCommand("sim/GPS/g430n1_coarse_up", "GNS COM/NAV 1 coarse up."); } }
        public static XPlaneCommand GPSG430n1FineDown { get { return GetXPlaneCommand("sim/GPS/g430n1_fine_down", "GNS COM/NAV 1 fine down."); } }
        public static XPlaneCommand GPSG430n1FineUp { get { return GetXPlaneCommand("sim/GPS/g430n1_fine_up", "GNS COM/NAV 1 fine up."); } }
        public static XPlaneCommand GPSG430n1ChapterUp { get { return GetXPlaneCommand("sim/GPS/g430n1_chapter_up", "GNS NAV 1 chapter up."); } }
        public static XPlaneCommand GPSG430n1ChapterDn { get { return GetXPlaneCommand("sim/GPS/g430n1_chapter_dn", "GNS NAV 1 chapter dn."); } }
        public static XPlaneCommand GPSG430n1PageUp { get { return GetXPlaneCommand("sim/GPS/g430n1_page_up", "GNS NAV 1 page up."); } }
        public static XPlaneCommand GPSG430n1PageDn { get { return GetXPlaneCommand("sim/GPS/g430n1_page_dn", "GNS NAV 1 page dn."); } }
        public static XPlaneCommand GPSG430n1ZoomIn { get { return GetXPlaneCommand("sim/GPS/g430n1_zoom_in", "GNS NAV 1 zoom in."); } }
        public static XPlaneCommand GPSG430n1ZoomOut { get { return GetXPlaneCommand("sim/GPS/g430n1_zoom_out", "GNS NAV 1 zoom out."); } }
        public static XPlaneCommand GPSG430n1NavComTog { get { return GetXPlaneCommand("sim/GPS/g430n1_nav_com_tog", "GNS NAV 1 NAV COM toggle."); } }
        public static XPlaneCommand GPSG430n1Cdi { get { return GetXPlaneCommand("sim/GPS/g430n1_cdi", "GNS NAV 1 CDI."); } }
        public static XPlaneCommand GPSG430n1Obs { get { return GetXPlaneCommand("sim/GPS/g430n1_obs", "GNS NAV 1 OBS."); } }
        public static XPlaneCommand GPSG430n1Msg { get { return GetXPlaneCommand("sim/GPS/g430n1_msg", "GNS NAV 1 MSG."); } }
        public static XPlaneCommand GPSG430n1Fpl { get { return GetXPlaneCommand("sim/GPS/g430n1_fpl", "GNS NAV 1 FPL."); } }
        public static XPlaneCommand GPSG430n1Proc { get { return GetXPlaneCommand("sim/GPS/g430n1_proc", "GNS NAV 1 PROC."); } }
        public static XPlaneCommand GPSG430n1Vnav { get { return GetXPlaneCommand("sim/GPS/g430n1_vnav", "GNS NAV 1 VNAV."); } }
        public static XPlaneCommand GPSG430n1Direct { get { return GetXPlaneCommand("sim/GPS/g430n1_direct", "GNS NAV 1 Direct."); } }
        public static XPlaneCommand GPSG430n1Menu { get { return GetXPlaneCommand("sim/GPS/g430n1_menu", "GNS NAV 1 Menu."); } }
        public static XPlaneCommand GPSG430n1Clr { get { return GetXPlaneCommand("sim/GPS/g430n1_clr", "GNS NAV 1 CLR."); } }
        public static XPlaneCommand GPSG430n1Ent { get { return GetXPlaneCommand("sim/GPS/g430n1_ent", "GNS NAV 1 ENT."); } }
        public static XPlaneCommand GPSG430n1ComFf { get { return GetXPlaneCommand("sim/GPS/g430n1_com_ff", "GNS NAV 1 COM flip flop."); } }
        public static XPlaneCommand GPSG430n1NavFf { get { return GetXPlaneCommand("sim/GPS/g430n1_nav_ff", "GNS NAV 1 NAV flip flop."); } }
        public static XPlaneCommand GPSG430n1Cursor { get { return GetXPlaneCommand("sim/GPS/g430n1_cursor", "GNS NAV 1 push cursor."); } }
        public static XPlaneCommand GPSG430n1Popout { get { return GetXPlaneCommand("sim/GPS/g430n1_popout", "GNS NAV 1 pop out window."); } }
        public static XPlaneCommand GPSG430n1Popup { get { return GetXPlaneCommand("sim/GPS/g430n1_popup", "GNS NAV 1 toggle popup."); } }
        public static XPlaneCommand GPSG430n1Cvol { get { return GetXPlaneCommand("sim/GPS/g430n1_cvol", "GNS NAV 1 COM audio."); } }
        public static XPlaneCommand GPSG430n1Vvol { get { return GetXPlaneCommand("sim/GPS/g430n1_vvol", "GNS NAV 1 NAV ID"); } }
        public static XPlaneCommand GPSG430n1CvolUp { get { return GetXPlaneCommand("sim/GPS/g430n1_cvol_up", "GNS NAV 1 COM audio volume up."); } }
        public static XPlaneCommand GPSG430n1CvolDn { get { return GetXPlaneCommand("sim/GPS/g430n1_cvol_dn", "GNS NAV 1 COM audio volume down."); } }
        public static XPlaneCommand GPSG430n1VvolUp { get { return GetXPlaneCommand("sim/GPS/g430n1_vvol_up", "GNS NAV 1 NAV audio volume up."); } }
        public static XPlaneCommand GPSG430n1VvolDn { get { return GetXPlaneCommand("sim/GPS/g430n1_vvol_dn", "GNS NAV 1 NAV audio volume down."); } }
        public static XPlaneCommand GPSG430n2CoarseDown { get { return GetXPlaneCommand("sim/GPS/g430n2_coarse_down", "GNS COM/NAV 2 coarse down."); } }
        public static XPlaneCommand GPSG430n2CoarseUp { get { return GetXPlaneCommand("sim/GPS/g430n2_coarse_up", "GNS COM/NAV 2 coarse up."); } }
        public static XPlaneCommand GPSG430n2FineDown { get { return GetXPlaneCommand("sim/GPS/g430n2_fine_down", "GNS COM/NAV 2 fine down."); } }
        public static XPlaneCommand GPSG430n2FineUp { get { return GetXPlaneCommand("sim/GPS/g430n2_fine_up", "GNS COM/NAV 2 fine up."); } }
        public static XPlaneCommand GPSG430n2ChapterUp { get { return GetXPlaneCommand("sim/GPS/g430n2_chapter_up", "GNS NAV 2 chapter up."); } }
        public static XPlaneCommand GPSG430n2ChapterDn { get { return GetXPlaneCommand("sim/GPS/g430n2_chapter_dn", "GNS NAV 2 chapter dn."); } }
        public static XPlaneCommand GPSG430n2PageUp { get { return GetXPlaneCommand("sim/GPS/g430n2_page_up", "GNS NAV 2 page up."); } }
        public static XPlaneCommand GPSG430n2PageDn { get { return GetXPlaneCommand("sim/GPS/g430n2_page_dn", "GNS NAV 2 page dn."); } }
        public static XPlaneCommand GPSG430n2ZoomIn { get { return GetXPlaneCommand("sim/GPS/g430n2_zoom_in", "GNS NAV 2 zoom in."); } }
        public static XPlaneCommand GPSG430n2ZoomOut { get { return GetXPlaneCommand("sim/GPS/g430n2_zoom_out", "GNS NAV 2 zoom out."); } }
        public static XPlaneCommand GPSG430n2NavComTog { get { return GetXPlaneCommand("sim/GPS/g430n2_nav_com_tog", "GNS NAV 2 NAV COM toggle."); } }
        public static XPlaneCommand GPSG430n2Cdi { get { return GetXPlaneCommand("sim/GPS/g430n2_cdi", "GNS NAV 2 CDI."); } }
        public static XPlaneCommand GPSG430n2Obs { get { return GetXPlaneCommand("sim/GPS/g430n2_obs", "GNS NAV 2 OBS."); } }
        public static XPlaneCommand GPSG430n2Msg { get { return GetXPlaneCommand("sim/GPS/g430n2_msg", "GNS NAV 2 MSG."); } }
        public static XPlaneCommand GPSG430n2Fpl { get { return GetXPlaneCommand("sim/GPS/g430n2_fpl", "GNS NAV 2 FPL."); } }
        public static XPlaneCommand GPSG430n2Proc { get { return GetXPlaneCommand("sim/GPS/g430n2_proc", "GNS NAV 2 PROC."); } }
        public static XPlaneCommand GPSG430n2Vnav { get { return GetXPlaneCommand("sim/GPS/g430n2_vnav", "GNS NAV 2 VNAV."); } }
        public static XPlaneCommand GPSG430n2Direct { get { return GetXPlaneCommand("sim/GPS/g430n2_direct", "GNS NAV 2 Direct."); } }
        public static XPlaneCommand GPSG430n2Menu { get { return GetXPlaneCommand("sim/GPS/g430n2_menu", "GNS NAV 2 Menu."); } }
        public static XPlaneCommand GPSG430n2Clr { get { return GetXPlaneCommand("sim/GPS/g430n2_clr", "GNS NAV 2 CLR."); } }
        public static XPlaneCommand GPSG430n2Ent { get { return GetXPlaneCommand("sim/GPS/g430n2_ent", "GNS NAV 2 ENT."); } }
        public static XPlaneCommand GPSG430n2ComFf { get { return GetXPlaneCommand("sim/GPS/g430n2_com_ff", "GNS NAV 2 COM FF."); } }
        public static XPlaneCommand GPSG430n2NavFf { get { return GetXPlaneCommand("sim/GPS/g430n2_nav_ff", "GNS NAV 2 NAV FF."); } }
        public static XPlaneCommand GPSG430n2Cursor { get { return GetXPlaneCommand("sim/GPS/g430n2_cursor", "GNS NAV 2 push cursor."); } }
        public static XPlaneCommand GPSG430n2Popout { get { return GetXPlaneCommand("sim/GPS/g430n2_popout", "GNS NAV 2 pop out window."); } }
        public static XPlaneCommand GPSG430n2Popup { get { return GetXPlaneCommand("sim/GPS/g430n2_popup", "GNS NAV 2 toggle popup."); } }
        public static XPlaneCommand GPSG430n2Cvol { get { return GetXPlaneCommand("sim/GPS/g430n2_cvol", "GNS NAV 2 COM audio."); } }
        public static XPlaneCommand GPSG430n2Vvol { get { return GetXPlaneCommand("sim/GPS/g430n2_vvol", "GNS NAV 2 NAV ID"); } }
        public static XPlaneCommand GPSG430n2CvolUp { get { return GetXPlaneCommand("sim/GPS/g430n2_cvol_up", "GNS NAV 2 COM audio volume up."); } }
        public static XPlaneCommand GPSG430n2CvolDn { get { return GetXPlaneCommand("sim/GPS/g430n2_cvol_dn", "GNS NAV 2 COM audio volume down."); } }
        public static XPlaneCommand GPSG430n2VvolUp { get { return GetXPlaneCommand("sim/GPS/g430n2_vvol_up", "GNS NAV 2 NAV audio volume up."); } }
        public static XPlaneCommand GPSG430n2VvolDn { get { return GetXPlaneCommand("sim/GPS/g430n2_vvol_dn", "GNS NAV 2 NAV audio volume down."); } }
        public static XPlaneCommand GPSG1000n1Nvol { get { return GetXPlaneCommand("sim/GPS/g1000n1_nvol", "G1000 pilot NAV audio."); } }
        public static XPlaneCommand GPSG1000n1NvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_nvol_up", "G1000 pilot NAV audio volume up."); } }
        public static XPlaneCommand GPSG1000n1NvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n1_nvol_dn", "G1000 pilot NAV audio volume down."); } }
        public static XPlaneCommand GPSG1000n1NavFf { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav_ff", "G1000 pilot NAV flip flop."); } }
        public static XPlaneCommand GPSG1000n1NavOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav_outer_up", "G1000 pilot NAV outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n1NavOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav_outer_down", "G1000 pilot NAV outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n1NavInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav_inner_up", "G1000 pilot NAV inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n1NavInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav_inner_down", "G1000 pilot NAV inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n1Nav12 { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav12", "G1000 pilot PFD NAV 1/2."); } }
        public static XPlaneCommand GPSG1000n1HdgUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_hdg_up", "G1000 pilot HDG up."); } }
        public static XPlaneCommand GPSG1000n1HdgDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_hdg_down", "G1000 pilot HDG down."); } }
        public static XPlaneCommand GPSG1000n1HdgSync { get { return GetXPlaneCommand("sim/GPS/g1000n1_hdg_sync", "G1000 pilot HDG sync."); } }
        public static XPlaneCommand GPSG1000n1Ap { get { return GetXPlaneCommand("sim/GPS/g1000n1_ap", "G1000 pilot autopilot."); } }
        public static XPlaneCommand GPSG1000n1Fd { get { return GetXPlaneCommand("sim/GPS/g1000n1_fd", "G1000 pilot flight director."); } }
        public static XPlaneCommand GPSG1000n1Yd { get { return GetXPlaneCommand("sim/GPS/g1000n1_yd", "G1000 pilot yaw damper."); } }
        public static XPlaneCommand GPSG1000n1Hdg { get { return GetXPlaneCommand("sim/GPS/g1000n1_hdg", "G1000 pilot AP HDG."); } }
        public static XPlaneCommand GPSG1000n1Alt { get { return GetXPlaneCommand("sim/GPS/g1000n1_alt", "G1000 pilot AP ALT."); } }
        public static XPlaneCommand GPSG1000n1Nav { get { return GetXPlaneCommand("sim/GPS/g1000n1_nav", "G1000 pilot AP NAV."); } }
        public static XPlaneCommand GPSG1000n1Vnv { get { return GetXPlaneCommand("sim/GPS/g1000n1_vnv", "G1000 pilot AP VNAV."); } }
        public static XPlaneCommand GPSG1000n1Apr { get { return GetXPlaneCommand("sim/GPS/g1000n1_apr", "G1000 pilot AP approach."); } }
        public static XPlaneCommand GPSG1000n1Bc { get { return GetXPlaneCommand("sim/GPS/g1000n1_bc", "G1000 pilot AP backcourse."); } }
        public static XPlaneCommand GPSG1000n1Vs { get { return GetXPlaneCommand("sim/GPS/g1000n1_vs", "G1000 pilot AP vertical speed."); } }
        public static XPlaneCommand GPSG1000n1Flc { get { return GetXPlaneCommand("sim/GPS/g1000n1_flc", "G1000 pilot AP flight level change."); } }
        public static XPlaneCommand GPSG1000n1NoseUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_nose_up", "G1000 pilot AP nose up."); } }
        public static XPlaneCommand GPSG1000n1NoseDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_nose_down", "G1000 pilot AP nose down."); } }
        public static XPlaneCommand GPSG1000n1AltOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_alt_outer_up", "G1000 pilot altitude outer ring up."); } }
        public static XPlaneCommand GPSG1000n1AltOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_alt_outer_down", "G1000 pilot altitude outer ring down."); } }
        public static XPlaneCommand GPSG1000n1AltInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_alt_inner_up", "G1000 pilot altitude inner ring up."); } }
        public static XPlaneCommand GPSG1000n1AltInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_alt_inner_down", "G1000 pilot altitude inner ring down."); } }
        public static XPlaneCommand GPSG1000n1Softkey1 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey1", "G1000 pilot PFD Softkey 1."); } }
        public static XPlaneCommand GPSG1000n1Softkey2 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey2", "G1000 pilot PFD Softkey 2."); } }
        public static XPlaneCommand GPSG1000n1Softkey3 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey3", "G1000 pilot PFD Softkey 3."); } }
        public static XPlaneCommand GPSG1000n1Softkey4 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey4", "G1000 pilot PFD Softkey 4."); } }
        public static XPlaneCommand GPSG1000n1Softkey5 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey5", "G1000 pilot PFD Softkey 5."); } }
        public static XPlaneCommand GPSG1000n1Softkey6 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey6", "G1000 pilot PFD Softkey 6."); } }
        public static XPlaneCommand GPSG1000n1Softkey7 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey7", "G1000 pilot PFD Softkey 7."); } }
        public static XPlaneCommand GPSG1000n1Softkey8 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey8", "G1000 pilot PFD Softkey 8."); } }
        public static XPlaneCommand GPSG1000n1Softkey9 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey9", "G1000 pilot PFD Softkey 9."); } }
        public static XPlaneCommand GPSG1000n1Softkey10 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey10", "G1000 pilot PFD Softkey 10."); } }
        public static XPlaneCommand GPSG1000n1Softkey11 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey11", "G1000 pilot PFD Softkey 11."); } }
        public static XPlaneCommand GPSG1000n1Softkey12 { get { return GetXPlaneCommand("sim/GPS/g1000n1_softkey12", "G1000 pilot PFD Softkey 12."); } }
        public static XPlaneCommand GPSG1000n1Cvol { get { return GetXPlaneCommand("sim/GPS/g1000n1_cvol", "G1000 pilot COM audio."); } }
        public static XPlaneCommand GPSG1000n1CvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_cvol_up", "G1000 pilot COM audio volume up."); } }
        public static XPlaneCommand GPSG1000n1CvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n1_cvol_dn", "G1000 pilot COM audio volume down."); } }
        public static XPlaneCommand GPSG1000n1ComFf { get { return GetXPlaneCommand("sim/GPS/g1000n1_com_ff", "G1000 pilot COM flip flop."); } }
        public static XPlaneCommand GPSG1000n1ComOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_com_outer_up", "G1000 pilot COM outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n1ComOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_com_outer_down", "G1000 pilot COM outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n1ComInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_com_inner_up", "G1000 pilot COM inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n1ComInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_com_inner_down", "G1000 pilot COM inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n1Com12 { get { return GetXPlaneCommand("sim/GPS/g1000n1_com12", "G1000 pilot PFD Com 1/2."); } }
        public static XPlaneCommand GPSG1000n1CrsUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_crs_up", "G1000 pilot CRS up."); } }
        public static XPlaneCommand GPSG1000n1CrsDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_crs_down", "G1000 pilot CRS down."); } }
        public static XPlaneCommand GPSG1000n1CrsSync { get { return GetXPlaneCommand("sim/GPS/g1000n1_crs_sync", "G1000 pilot CRS sync."); } }
        public static XPlaneCommand GPSG1000n1BaroUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_baro_up", "G1000 pilot baro up."); } }
        public static XPlaneCommand GPSG1000n1BaroDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_baro_down", "G1000 pilot baro down."); } }
        public static XPlaneCommand GPSG1000n1RangeUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_range_up", "G1000 pilot range up."); } }
        public static XPlaneCommand GPSG1000n1RangeDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_range_down", "G1000 pilot range down."); } }
        public static XPlaneCommand GPSG1000n1PanUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_up", "G1000 pilot pan up."); } }
        public static XPlaneCommand GPSG1000n1PanDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_down", "G1000 pilot pan down."); } }
        public static XPlaneCommand GPSG1000n1PanLeft { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_left", "G1000 pilot pan left."); } }
        public static XPlaneCommand GPSG1000n1PanRight { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_right", "G1000 pilot pan right."); } }
        public static XPlaneCommand GPSG1000n1PanUpLeft { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_up_left", "G1000 pilot pan up left."); } }
        public static XPlaneCommand GPSG1000n1PanDownLeft { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_down_left", "G1000 pilot pan down left."); } }
        public static XPlaneCommand GPSG1000n1PanUpRight { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_up_right", "G1000 pilot pan up right."); } }
        public static XPlaneCommand GPSG1000n1PanDownRight { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_down_right", "G1000 pilot pan down right."); } }
        public static XPlaneCommand GPSG1000n1PanPush { get { return GetXPlaneCommand("sim/GPS/g1000n1_pan_push", "G1000 pilot push pan."); } }
        public static XPlaneCommand GPSG1000n1Direct { get { return GetXPlaneCommand("sim/GPS/g1000n1_direct", "G1000 pilot -D->."); } }
        public static XPlaneCommand GPSG1000n1Menu { get { return GetXPlaneCommand("sim/GPS/g1000n1_menu", "G1000 pilot menu."); } }
        public static XPlaneCommand GPSG1000n1Fpl { get { return GetXPlaneCommand("sim/GPS/g1000n1_fpl", "G1000 pilot FPL."); } }
        public static XPlaneCommand GPSG1000n1Proc { get { return GetXPlaneCommand("sim/GPS/g1000n1_proc", "G1000 pilot proc."); } }
        public static XPlaneCommand GPSG1000n1Clr { get { return GetXPlaneCommand("sim/GPS/g1000n1_clr", "G1000 pilot CLR."); } }
        public static XPlaneCommand GPSG1000n1Ent { get { return GetXPlaneCommand("sim/GPS/g1000n1_ent", "G1000 pilot ENT."); } }
        public static XPlaneCommand GPSG1000n1FmsOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_fms_outer_up", "G1000 pilot FMS outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n1FmsOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_fms_outer_down", "G1000 pilot FMS outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n1FmsInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n1_fms_inner_up", "G1000 pilot FMS inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n1FmsInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n1_fms_inner_down", "G1000 pilot FMS inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n1Cursor { get { return GetXPlaneCommand("sim/GPS/g1000n1_cursor", "G1000 pilot FMS cursor."); } }
        public static XPlaneCommand GPSG1000n1Popout { get { return GetXPlaneCommand("sim/GPS/g1000n1_popout", "G1000 pilot pop out window."); } }
        public static XPlaneCommand GPSG1000n1Popup { get { return GetXPlaneCommand("sim/GPS/g1000n1_popup", "G1000 pilot popup."); } }
        public static XPlaneCommand GPSG1000n2Nvol { get { return GetXPlaneCommand("sim/GPS/g1000n2_nvol", "G1000 copilot NAV audio."); } }
        public static XPlaneCommand GPSG1000n2NvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_nvol_up", "G1000 copilot NAV audio volume up."); } }
        public static XPlaneCommand GPSG1000n2NvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n2_nvol_dn", "G1000 copilot NAV audio volume down."); } }
        public static XPlaneCommand GPSG1000n2NavFf { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav_ff", "G1000 copilot NAV flip flop."); } }
        public static XPlaneCommand GPSG1000n2NavOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav_outer_up", "G1000 copilot NAV outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n2NavOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav_outer_down", "G1000 copilot NAV outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n2NavInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav_inner_up", "G1000 copilot NAV inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n2NavInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav_inner_down", "G1000 copilot NAV inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n2Nav12 { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav12", "G1000 copilot PFD NAV 1/2."); } }
        public static XPlaneCommand GPSG1000n2HdgUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_hdg_up", "G1000 copilot HDG up."); } }
        public static XPlaneCommand GPSG1000n2HdgDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_hdg_down", "G1000 copilot HDG down."); } }
        public static XPlaneCommand GPSG1000n2HdgSync { get { return GetXPlaneCommand("sim/GPS/g1000n2_hdg_sync", "G1000 copilot HDG sync."); } }
        public static XPlaneCommand GPSG1000n2Ap { get { return GetXPlaneCommand("sim/GPS/g1000n2_ap", "G1000 copilot autopilot."); } }
        public static XPlaneCommand GPSG1000n2Fd { get { return GetXPlaneCommand("sim/GPS/g1000n2_fd", "G1000 copilot flight director."); } }
        public static XPlaneCommand GPSG1000n2Yd { get { return GetXPlaneCommand("sim/GPS/g1000n2_yd", "G1000 copilot yaw damper."); } }
        public static XPlaneCommand GPSG1000n2Hdg { get { return GetXPlaneCommand("sim/GPS/g1000n2_hdg", "G1000 copilot AP HDG."); } }
        public static XPlaneCommand GPSG1000n2Alt { get { return GetXPlaneCommand("sim/GPS/g1000n2_alt", "G1000 copilot AP ALT."); } }
        public static XPlaneCommand GPSG1000n2Nav { get { return GetXPlaneCommand("sim/GPS/g1000n2_nav", "G1000 copilot AP NAV."); } }
        public static XPlaneCommand GPSG1000n2Vnv { get { return GetXPlaneCommand("sim/GPS/g1000n2_vnv", "G1000 copilot AP VNAV."); } }
        public static XPlaneCommand GPSG1000n2Apr { get { return GetXPlaneCommand("sim/GPS/g1000n2_apr", "G1000 copilot AP approach."); } }
        public static XPlaneCommand GPSG1000n2Bc { get { return GetXPlaneCommand("sim/GPS/g1000n2_bc", "G1000 copilot AP backcourse."); } }
        public static XPlaneCommand GPSG1000n2Vs { get { return GetXPlaneCommand("sim/GPS/g1000n2_vs", "G1000 copilot AP vertical speed."); } }
        public static XPlaneCommand GPSG1000n2Flc { get { return GetXPlaneCommand("sim/GPS/g1000n2_flc", "G1000 copilot AP flight level change."); } }
        public static XPlaneCommand GPSG1000n2NoseUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_nose_up", "G1000 copilot AP nose up."); } }
        public static XPlaneCommand GPSG1000n2NoseDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_nose_down", "G1000 copilot AP nose down."); } }
        public static XPlaneCommand GPSG1000n2AltOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_alt_outer_up", "G1000 copilot altitude outer ring up."); } }
        public static XPlaneCommand GPSG1000n2AltOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_alt_outer_down", "G1000 copilot altitude outer ring down."); } }
        public static XPlaneCommand GPSG1000n2AltInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_alt_inner_up", "G1000 copilot altitude inner ring up."); } }
        public static XPlaneCommand GPSG1000n2AltInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_alt_inner_down", "G1000 copilot altitude inner ring down."); } }
        public static XPlaneCommand GPSG1000n2Softkey1 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey1", "G1000 copilot PFD Softkey 1."); } }
        public static XPlaneCommand GPSG1000n2Softkey2 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey2", "G1000 copilot PFD Softkey 2."); } }
        public static XPlaneCommand GPSG1000n2Softkey3 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey3", "G1000 copilot PFD Softkey 3."); } }
        public static XPlaneCommand GPSG1000n2Softkey4 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey4", "G1000 copilot PFD Softkey 4."); } }
        public static XPlaneCommand GPSG1000n2Softkey5 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey5", "G1000 copilot PFD Softkey 5."); } }
        public static XPlaneCommand GPSG1000n2Softkey6 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey6", "G1000 copilot PFD Softkey 6."); } }
        public static XPlaneCommand GPSG1000n2Softkey7 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey7", "G1000 copilot PFD Softkey 7."); } }
        public static XPlaneCommand GPSG1000n2Softkey8 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey8", "G1000 copilot PFD Softkey 8."); } }
        public static XPlaneCommand GPSG1000n2Softkey9 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey9", "G1000 copilot PFD Softkey 9."); } }
        public static XPlaneCommand GPSG1000n2Softkey10 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey10", "G1000 copilot PFD Softkey 10."); } }
        public static XPlaneCommand GPSG1000n2Softkey11 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey11", "G1000 copilot PFD Softkey 11."); } }
        public static XPlaneCommand GPSG1000n2Softkey12 { get { return GetXPlaneCommand("sim/GPS/g1000n2_softkey12", "G1000 copilot PFD Softkey 12."); } }
        public static XPlaneCommand GPSG1000n2Cvol { get { return GetXPlaneCommand("sim/GPS/g1000n2_cvol", "G1000 copilot COM audio."); } }
        public static XPlaneCommand GPSG1000n2CvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_cvol_up", "G1000 copilot COM audio volume up."); } }
        public static XPlaneCommand GPSG1000n2CvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n2_cvol_dn", "G1000 copilot COM audio volume down."); } }
        public static XPlaneCommand GPSG1000n2ComFf { get { return GetXPlaneCommand("sim/GPS/g1000n2_com_ff", "G1000 copilot COM flip flop."); } }
        public static XPlaneCommand GPSG1000n2ComOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_com_outer_up", "G1000 copilot COM outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n2ComOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_com_outer_down", "G1000 copilot COM outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n2ComInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_com_inner_up", "G1000 copilot COM inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n2ComInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_com_inner_down", "G1000 copilot COM inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n2Com12 { get { return GetXPlaneCommand("sim/GPS/g1000n2_com12", "G1000 copilot PFD Com 1/2."); } }
        public static XPlaneCommand GPSG1000n2CrsUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_crs_up", "G1000 copilot CRS up."); } }
        public static XPlaneCommand GPSG1000n2CrsDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_crs_down", "G1000 copilot CRS down."); } }
        public static XPlaneCommand GPSG1000n2CrsSync { get { return GetXPlaneCommand("sim/GPS/g1000n2_crs_sync", "G1000 copilot CRS sync."); } }
        public static XPlaneCommand GPSG1000n2BaroUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_baro_up", "G1000 copilot baro up."); } }
        public static XPlaneCommand GPSG1000n2BaroDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_baro_down", "G1000 copilot baro down."); } }
        public static XPlaneCommand GPSG1000n2RangeUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_range_up", "G1000 copilot range up."); } }
        public static XPlaneCommand GPSG1000n2RangeDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_range_down", "G1000 copilot range down."); } }
        public static XPlaneCommand GPSG1000n2PanUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_up", "G1000 copilot pan up."); } }
        public static XPlaneCommand GPSG1000n2PanDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_down", "G1000 copilot pan down."); } }
        public static XPlaneCommand GPSG1000n2PanLeft { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_left", "G1000 copilot pan left."); } }
        public static XPlaneCommand GPSG1000n2PanRight { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_right", "G1000 copilot pan right."); } }
        public static XPlaneCommand GPSG1000n2PanUpLeft { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_up_left", "G1000 copilot pan up left."); } }
        public static XPlaneCommand GPSG1000n2PanDownLeft { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_down_left", "G1000 copilot pan down left."); } }
        public static XPlaneCommand GPSG1000n2PanUpRight { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_up_right", "G1000 copilot pan up right."); } }
        public static XPlaneCommand GPSG1000n2PanDownRight { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_down_right", "G1000 copilot pan down right."); } }
        public static XPlaneCommand GPSG1000n2PanPush { get { return GetXPlaneCommand("sim/GPS/g1000n2_pan_push", "G1000 copilot push pan."); } }
        public static XPlaneCommand GPSG1000n2Direct { get { return GetXPlaneCommand("sim/GPS/g1000n2_direct", "G1000 copilot -D->."); } }
        public static XPlaneCommand GPSG1000n2Menu { get { return GetXPlaneCommand("sim/GPS/g1000n2_menu", "G1000 copilot menu."); } }
        public static XPlaneCommand GPSG1000n2Fpl { get { return GetXPlaneCommand("sim/GPS/g1000n2_fpl", "G1000 copilot FPL."); } }
        public static XPlaneCommand GPSG1000n2Proc { get { return GetXPlaneCommand("sim/GPS/g1000n2_proc", "G1000 copilot proc."); } }
        public static XPlaneCommand GPSG1000n2Clr { get { return GetXPlaneCommand("sim/GPS/g1000n2_clr", "G1000 copilot CLR."); } }
        public static XPlaneCommand GPSG1000n2Ent { get { return GetXPlaneCommand("sim/GPS/g1000n2_ent", "G1000 copilot ENT."); } }
        public static XPlaneCommand GPSG1000n2FmsOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_fms_outer_up", "G1000 copilot FMS outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n2FmsOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_fms_outer_down", "G1000 copilot FMS outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n2FmsInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n2_fms_inner_up", "G1000 copilot FMS inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n2FmsInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n2_fms_inner_down", "G1000 copilot FMS inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n2Cursor { get { return GetXPlaneCommand("sim/GPS/g1000n2_cursor", "G1000 copilot FMS cursor."); } }
        public static XPlaneCommand GPSG1000n2Popout { get { return GetXPlaneCommand("sim/GPS/g1000n2_popout", "G1000 copilot pop out window."); } }
        public static XPlaneCommand GPSG1000n2Popup { get { return GetXPlaneCommand("sim/GPS/g1000n2_popup", "G1000 copilot popup."); } }
        public static XPlaneCommand GPSG1000n3Nvol { get { return GetXPlaneCommand("sim/GPS/g1000n3_nvol", "G1000 MFD NAV audio."); } }
        public static XPlaneCommand GPSG1000n3NvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_nvol_up", "G1000 MFD NAV audio volume up."); } }
        public static XPlaneCommand GPSG1000n3NvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n3_nvol_dn", "G1000 MFD NAV audio volume down."); } }
        public static XPlaneCommand GPSG1000n3NavFf { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav_ff", "G1000 MFD NAV flip flop."); } }
        public static XPlaneCommand GPSG1000n3NavOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav_outer_up", "G1000 MFD NAV outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n3NavOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav_outer_down", "G1000 MFD NAV outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n3NavInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav_inner_up", "G1000 MFD NAV inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n3NavInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav_inner_down", "G1000 MFD NAV inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n3Nav12 { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav12", "G1000 MFD NAV 1/2."); } }
        public static XPlaneCommand GPSG1000n3HdgUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_hdg_up", "G1000 MFD HDG up."); } }
        public static XPlaneCommand GPSG1000n3HdgDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_hdg_down", "G1000 MFD HDG down."); } }
        public static XPlaneCommand GPSG1000n3HdgSync { get { return GetXPlaneCommand("sim/GPS/g1000n3_hdg_sync", "G1000 MFD HDG sync."); } }
        public static XPlaneCommand GPSG1000n3Ap { get { return GetXPlaneCommand("sim/GPS/g1000n3_ap", "G1000 MFD autopilot."); } }
        public static XPlaneCommand GPSG1000n3Fd { get { return GetXPlaneCommand("sim/GPS/g1000n3_fd", "G1000 MFD flight director."); } }
        public static XPlaneCommand GPSG1000n3Yd { get { return GetXPlaneCommand("sim/GPS/g1000n3_yd", "G1000 MFD yaw damper."); } }
        public static XPlaneCommand GPSG1000n3Hdg { get { return GetXPlaneCommand("sim/GPS/g1000n3_hdg", "G1000 MFD AP HDG."); } }
        public static XPlaneCommand GPSG1000n3Alt { get { return GetXPlaneCommand("sim/GPS/g1000n3_alt", "G1000 MFD AP ALT."); } }
        public static XPlaneCommand GPSG1000n3Nav { get { return GetXPlaneCommand("sim/GPS/g1000n3_nav", "G1000 MFD AP NAV."); } }
        public static XPlaneCommand GPSG1000n3Vnv { get { return GetXPlaneCommand("sim/GPS/g1000n3_vnv", "G1000 MFD AP VNAV."); } }
        public static XPlaneCommand GPSG1000n3Apr { get { return GetXPlaneCommand("sim/GPS/g1000n3_apr", "G1000 MFD AP approach."); } }
        public static XPlaneCommand GPSG1000n3Bc { get { return GetXPlaneCommand("sim/GPS/g1000n3_bc", "G1000 MFD AP backcourse."); } }
        public static XPlaneCommand GPSG1000n3Vs { get { return GetXPlaneCommand("sim/GPS/g1000n3_vs", "G1000 MFD AP vertical speed."); } }
        public static XPlaneCommand GPSG1000n3Flc { get { return GetXPlaneCommand("sim/GPS/g1000n3_flc", "G1000 MFD AP flight level change."); } }
        public static XPlaneCommand GPSG1000n3NoseUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_nose_up", "G1000 MFD AP nose up."); } }
        public static XPlaneCommand GPSG1000n3NoseDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_nose_down", "G1000 MFD AP nose down."); } }
        public static XPlaneCommand GPSG1000n3AltOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_alt_outer_up", "G1000 MFD altitude outer ring up."); } }
        public static XPlaneCommand GPSG1000n3AltOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_alt_outer_down", "G1000 MFD altitude outer ring down."); } }
        public static XPlaneCommand GPSG1000n3AltInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_alt_inner_up", "G1000 MFD altitude inner ring up."); } }
        public static XPlaneCommand GPSG1000n3AltInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_alt_inner_down", "G1000 MFD altitude inner ring down."); } }
        public static XPlaneCommand GPSG1000n3Softkey1 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey1", "G1000 MFD Softkey 1."); } }
        public static XPlaneCommand GPSG1000n3Softkey2 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey2", "G1000 MFD Softkey 2."); } }
        public static XPlaneCommand GPSG1000n3Softkey3 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey3", "G1000 MFD Softkey 3."); } }
        public static XPlaneCommand GPSG1000n3Softkey4 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey4", "G1000 MFD Softkey 4."); } }
        public static XPlaneCommand GPSG1000n3Softkey5 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey5", "G1000 MFD Softkey 5."); } }
        public static XPlaneCommand GPSG1000n3Softkey6 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey6", "G1000 MFD Softkey 6."); } }
        public static XPlaneCommand GPSG1000n3Softkey7 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey7", "G1000 MFD Softkey 7."); } }
        public static XPlaneCommand GPSG1000n3Softkey8 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey8", "G1000 MFD Softkey 8."); } }
        public static XPlaneCommand GPSG1000n3Softkey9 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey9", "G1000 MFD Softkey 9."); } }
        public static XPlaneCommand GPSG1000n3Softkey10 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey10", "G1000 MFD Softkey 10."); } }
        public static XPlaneCommand GPSG1000n3Softkey11 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey11", "G1000 MFD Softkey 11."); } }
        public static XPlaneCommand GPSG1000n3Softkey12 { get { return GetXPlaneCommand("sim/GPS/g1000n3_softkey12", "G1000 MFD Softkey 12."); } }
        public static XPlaneCommand GPSG1000n3Cvol { get { return GetXPlaneCommand("sim/GPS/g1000n3_cvol", "G1000 MFD COM audio."); } }
        public static XPlaneCommand GPSG1000n3CvolUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_cvol_up", "G1000 MFD COM audio volume up."); } }
        public static XPlaneCommand GPSG1000n3CvolDn { get { return GetXPlaneCommand("sim/GPS/g1000n3_cvol_dn", "G1000 MFD COM audio volume down."); } }
        public static XPlaneCommand GPSG1000n3ComFf { get { return GetXPlaneCommand("sim/GPS/g1000n3_com_ff", "G1000 MFD COM flip flop."); } }
        public static XPlaneCommand GPSG1000n3ComOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_com_outer_up", "G1000 MFD COM outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n3ComOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_com_outer_down", "G1000 MFD COM outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n3ComInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_com_inner_up", "G1000 MFD COM inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n3ComInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_com_inner_down", "G1000 MFD COM inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n3Com12 { get { return GetXPlaneCommand("sim/GPS/g1000n3_com12", "G1000 MFD COM 1/2."); } }
        public static XPlaneCommand GPSG1000n3CrsUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_crs_up", "G1000 MFD CRS up."); } }
        public static XPlaneCommand GPSG1000n3CrsDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_crs_down", "G1000 MFD CRS down."); } }
        public static XPlaneCommand GPSG1000n3CrsSync { get { return GetXPlaneCommand("sim/GPS/g1000n3_crs_sync", "G1000 MFD CRS sync."); } }
        public static XPlaneCommand GPSG1000n3BaroUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_baro_up", "G1000 MFD baro up."); } }
        public static XPlaneCommand GPSG1000n3BaroDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_baro_down", "G1000 MFD baro down."); } }
        public static XPlaneCommand GPSG1000n3RangeUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_range_up", "G1000 MFD range up."); } }
        public static XPlaneCommand GPSG1000n3RangeDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_range_down", "G1000 MFD range down."); } }
        public static XPlaneCommand GPSG1000n3PanUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_up", "G1000 MFD pan up."); } }
        public static XPlaneCommand GPSG1000n3PanDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_down", "G1000 MFD pan down."); } }
        public static XPlaneCommand GPSG1000n3PanLeft { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_left", "G1000 MFD pan left."); } }
        public static XPlaneCommand GPSG1000n3PanRight { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_right", "G1000 MFD pan right."); } }
        public static XPlaneCommand GPSG1000n3PanUpLeft { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_up_left", "G1000 MFD pan up left."); } }
        public static XPlaneCommand GPSG1000n3PanDownLeft { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_down_left", "G1000 MFD pan down left."); } }
        public static XPlaneCommand GPSG1000n3PanUpRight { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_up_right", "G1000 MFD pan up right."); } }
        public static XPlaneCommand GPSG1000n3PanDownRight { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_down_right", "G1000 MFD pan down right."); } }
        public static XPlaneCommand GPSG1000n3PanPush { get { return GetXPlaneCommand("sim/GPS/g1000n3_pan_push", "G1000 MFD push pan."); } }
        public static XPlaneCommand GPSG1000n3Direct { get { return GetXPlaneCommand("sim/GPS/g1000n3_direct", "G1000 MFD -D->."); } }
        public static XPlaneCommand GPSG1000n3Menu { get { return GetXPlaneCommand("sim/GPS/g1000n3_menu", "G1000 MFD menu."); } }
        public static XPlaneCommand GPSG1000n3Fpl { get { return GetXPlaneCommand("sim/GPS/g1000n3_fpl", "G1000 MFD FPL."); } }
        public static XPlaneCommand GPSG1000n3Proc { get { return GetXPlaneCommand("sim/GPS/g1000n3_proc", "G1000 MFD proc."); } }
        public static XPlaneCommand GPSG1000n3Clr { get { return GetXPlaneCommand("sim/GPS/g1000n3_clr", "G1000 MFD CLR."); } }
        public static XPlaneCommand GPSG1000n3Ent { get { return GetXPlaneCommand("sim/GPS/g1000n3_ent", "G1000 MFD ENT."); } }
        public static XPlaneCommand GPSG1000n3FmsOuterUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_fms_outer_up", "G1000 MFD FMS outer ring tune up."); } }
        public static XPlaneCommand GPSG1000n3FmsOuterDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_fms_outer_down", "G1000 MFD FMS outer ring tune down."); } }
        public static XPlaneCommand GPSG1000n3FmsInnerUp { get { return GetXPlaneCommand("sim/GPS/g1000n3_fms_inner_up", "G1000 MFD FMS inner ring tune up."); } }
        public static XPlaneCommand GPSG1000n3FmsInnerDown { get { return GetXPlaneCommand("sim/GPS/g1000n3_fms_inner_down", "G1000 MFD FMS inner ring tune down."); } }
        public static XPlaneCommand GPSG1000n3Cursor { get { return GetXPlaneCommand("sim/GPS/g1000n3_cursor", "G1000 MFD FMS cursor."); } }
        public static XPlaneCommand GPSG1000n3Popout { get { return GetXPlaneCommand("sim/GPS/g1000n3_popout", "G1000 MFD pop out window."); } }
        public static XPlaneCommand GPSG1000n3Popup { get { return GetXPlaneCommand("sim/GPS/g1000n3_popup", "G1000 MFD popup."); } }
        public static XPlaneCommand GPSGcu478A { get { return GetXPlaneCommand("sim/GPS/gcu478/A", "GCU: A"); } }
        public static XPlaneCommand GPSGcu478B { get { return GetXPlaneCommand("sim/GPS/gcu478/B", "GCU: B"); } }
        public static XPlaneCommand GPSGcu478C { get { return GetXPlaneCommand("sim/GPS/gcu478/C", "GCU: C"); } }
        public static XPlaneCommand GPSGcu478D { get { return GetXPlaneCommand("sim/GPS/gcu478/D", "GCU: D"); } }
        public static XPlaneCommand GPSGcu478E { get { return GetXPlaneCommand("sim/GPS/gcu478/E", "GCU: E"); } }
        public static XPlaneCommand GPSGcu478F { get { return GetXPlaneCommand("sim/GPS/gcu478/F", "GCU: F"); } }
        public static XPlaneCommand GPSGcu478G { get { return GetXPlaneCommand("sim/GPS/gcu478/G", "GCU: G"); } }
        public static XPlaneCommand GPSGcu478H { get { return GetXPlaneCommand("sim/GPS/gcu478/H", "GCU: H"); } }
        public static XPlaneCommand GPSGcu478I { get { return GetXPlaneCommand("sim/GPS/gcu478/I", "GCU: I"); } }
        public static XPlaneCommand GPSGcu478J { get { return GetXPlaneCommand("sim/GPS/gcu478/J", "GCU: J"); } }
        public static XPlaneCommand GPSGcu478K { get { return GetXPlaneCommand("sim/GPS/gcu478/K", "GCU: K"); } }
        public static XPlaneCommand GPSGcu478L { get { return GetXPlaneCommand("sim/GPS/gcu478/L", "GCU: L"); } }
        public static XPlaneCommand GPSGcu478M { get { return GetXPlaneCommand("sim/GPS/gcu478/M", "GCU: M"); } }
        public static XPlaneCommand GPSGcu478N { get { return GetXPlaneCommand("sim/GPS/gcu478/N", "GCU: N"); } }
        public static XPlaneCommand GPSGcu478O { get { return GetXPlaneCommand("sim/GPS/gcu478/O", "GCU: O"); } }
        public static XPlaneCommand GPSGcu478P { get { return GetXPlaneCommand("sim/GPS/gcu478/P", "GCU: P"); } }
        public static XPlaneCommand GPSGcu478Q { get { return GetXPlaneCommand("sim/GPS/gcu478/Q", "GCU: Q"); } }
        public static XPlaneCommand GPSGcu478R { get { return GetXPlaneCommand("sim/GPS/gcu478/R", "GCU: R"); } }
        public static XPlaneCommand GPSGcu478S { get { return GetXPlaneCommand("sim/GPS/gcu478/S", "GCU: S"); } }
        public static XPlaneCommand GPSGcu478T { get { return GetXPlaneCommand("sim/GPS/gcu478/T", "GCU: T"); } }
        public static XPlaneCommand GPSGcu478U { get { return GetXPlaneCommand("sim/GPS/gcu478/U", "GCU: U"); } }
        public static XPlaneCommand GPSGcu478V { get { return GetXPlaneCommand("sim/GPS/gcu478/V", "GCU: V"); } }
        public static XPlaneCommand GPSGcu478W { get { return GetXPlaneCommand("sim/GPS/gcu478/W", "GCU: W"); } }
        public static XPlaneCommand GPSGcu478X { get { return GetXPlaneCommand("sim/GPS/gcu478/X", "GCU: X"); } }
        public static XPlaneCommand GPSGcu478Y { get { return GetXPlaneCommand("sim/GPS/gcu478/Y", "GCU: Y"); } }
        public static XPlaneCommand GPSGcu478Z { get { return GetXPlaneCommand("sim/GPS/gcu478/Z", "GCU: Z"); } }
        public static XPlaneCommand GPSGcu4780 { get { return GetXPlaneCommand("sim/GPS/gcu478/0", "GCU: 0"); } }
        public static XPlaneCommand GPSGcu4781 { get { return GetXPlaneCommand("sim/GPS/gcu478/1", "GCU: 1"); } }
        public static XPlaneCommand GPSGcu4782 { get { return GetXPlaneCommand("sim/GPS/gcu478/2", "GCU: 2"); } }
        public static XPlaneCommand GPSGcu4783 { get { return GetXPlaneCommand("sim/GPS/gcu478/3", "GCU: 3"); } }
        public static XPlaneCommand GPSGcu4784 { get { return GetXPlaneCommand("sim/GPS/gcu478/4", "GCU: 4"); } }
        public static XPlaneCommand GPSGcu4785 { get { return GetXPlaneCommand("sim/GPS/gcu478/5", "GCU: 5"); } }
        public static XPlaneCommand GPSGcu4786 { get { return GetXPlaneCommand("sim/GPS/gcu478/6", "GCU: 6"); } }
        public static XPlaneCommand GPSGcu4787 { get { return GetXPlaneCommand("sim/GPS/gcu478/7", "GCU: 7"); } }
        public static XPlaneCommand GPSGcu4788 { get { return GetXPlaneCommand("sim/GPS/gcu478/8", "GCU: 8"); } }
        public static XPlaneCommand GPSGcu4789 { get { return GetXPlaneCommand("sim/GPS/gcu478/9", "GCU: 9"); } }
        public static XPlaneCommand GPSGcu478Dot { get { return GetXPlaneCommand("sim/GPS/gcu478/dot", "GCU: dot"); } }
        public static XPlaneCommand GPSGcu478Minus { get { return GetXPlaneCommand("sim/GPS/gcu478/minus", "GCU: minus"); } }
        public static XPlaneCommand GPSGcu478Spc { get { return GetXPlaneCommand("sim/GPS/gcu478/spc", "GCU: space"); } }
        public static XPlaneCommand GPSGcu478Bksp { get { return GetXPlaneCommand("sim/GPS/gcu478/bksp", "GCU: backspace"); } }
        public static XPlaneCommand GPSGcu478HdgUp { get { return GetXPlaneCommand("sim/GPS/gcu478/hdg_up", "GCU: HDG up."); } }
        public static XPlaneCommand GPSGcu478HdgDown { get { return GetXPlaneCommand("sim/GPS/gcu478/hdg_down", "GCU: HDG down."); } }
        public static XPlaneCommand GPSGcu478HdgSync { get { return GetXPlaneCommand("sim/GPS/gcu478/hdg_sync", "GCU: HDG sync."); } }
        public static XPlaneCommand GPSGcu478CrsUp { get { return GetXPlaneCommand("sim/GPS/gcu478/crs_up", "GCU: CRS up."); } }
        public static XPlaneCommand GPSGcu478CrsDown { get { return GetXPlaneCommand("sim/GPS/gcu478/crs_down", "GCU: CRS down."); } }
        public static XPlaneCommand GPSGcu478CrsSync { get { return GetXPlaneCommand("sim/GPS/gcu478/crs_sync", "GCU: CRS sync."); } }
        public static XPlaneCommand GPSGcu478AltUp { get { return GetXPlaneCommand("sim/GPS/gcu478/alt_up", "GCU: altitude up."); } }
        public static XPlaneCommand GPSGcu478AltDown { get { return GetXPlaneCommand("sim/GPS/gcu478/alt_down", "GCU: altitude down."); } }
        public static XPlaneCommand GPSGcu478AltSync { get { return GetXPlaneCommand("sim/GPS/gcu478/alt_sync", "GCU: altitude sync."); } }
        public static XPlaneCommand GPSGcu478RangeUp { get { return GetXPlaneCommand("sim/GPS/gcu478/range_up", "GCU: range up."); } }
        public static XPlaneCommand GPSGcu478RangeDown { get { return GetXPlaneCommand("sim/GPS/gcu478/range_down", "GCU: range down."); } }
        public static XPlaneCommand GPSGcu478PanUp { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_up", "GCU: pan up."); } }
        public static XPlaneCommand GPSGcu478PanDown { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_down", "GCU: pan down."); } }
        public static XPlaneCommand GPSGcu478PanLeft { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_left", "GCU: pan left."); } }
        public static XPlaneCommand GPSGcu478PanRight { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_right", "GCU: pan right."); } }
        public static XPlaneCommand GPSGcu478PanUpLeft { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_up_left", "GCU: pan up left."); } }
        public static XPlaneCommand GPSGcu478PanDownLeft { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_down_left", "GCU: pan down left."); } }
        public static XPlaneCommand GPSGcu478PanUpRight { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_up_right", "GCU: pan up right."); } }
        public static XPlaneCommand GPSGcu478PanDownRight { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_down_right", "GCU: pan down right."); } }
        public static XPlaneCommand GPSGcu478PanPush { get { return GetXPlaneCommand("sim/GPS/gcu478/pan_push", "GCU: push pan."); } }
        public static XPlaneCommand GPSGcu478Fms { get { return GetXPlaneCommand("sim/GPS/gcu478/fms", "GCU: FMS"); } }
        public static XPlaneCommand GPSGcu478Xpdr { get { return GetXPlaneCommand("sim/GPS/gcu478/xpdr", "GCU: XPDR"); } }
        public static XPlaneCommand GPSGcu478Com { get { return GetXPlaneCommand("sim/GPS/gcu478/com", "GCU: COM"); } }
        public static XPlaneCommand GPSGcu478Nav { get { return GetXPlaneCommand("sim/GPS/gcu478/nav", "GCU: NAV"); } }
        public static XPlaneCommand GPSGcu478Ff { get { return GetXPlaneCommand("sim/GPS/gcu478/ff", "GCU: COM/NAV flip flop."); } }
        public static XPlaneCommand GPSGcu478Direct { get { return GetXPlaneCommand("sim/GPS/gcu478/direct", "GCU: -D->."); } }
        public static XPlaneCommand GPSGcu478Menu { get { return GetXPlaneCommand("sim/GPS/gcu478/menu", "GCU: menu."); } }
        public static XPlaneCommand GPSGcu478Fpl { get { return GetXPlaneCommand("sim/GPS/gcu478/fpl", "GCU: FPL."); } }
        public static XPlaneCommand GPSGcu478Proc { get { return GetXPlaneCommand("sim/GPS/gcu478/proc", "GCU: proc."); } }
        public static XPlaneCommand GPSGcu478Clr { get { return GetXPlaneCommand("sim/GPS/gcu478/clr", "GCU: CLR."); } }
        public static XPlaneCommand GPSGcu478Ent { get { return GetXPlaneCommand("sim/GPS/gcu478/ent", "GCU: ENT."); } }
        public static XPlaneCommand GPSGcu478OuterUp { get { return GetXPlaneCommand("sim/GPS/gcu478/outer_up", "GCU: outer ring tune up."); } }
        public static XPlaneCommand GPSGcu478OuterDown { get { return GetXPlaneCommand("sim/GPS/gcu478/outer_down", "GCU: outer ring tune down."); } }
        public static XPlaneCommand GPSGcu478InnerUp { get { return GetXPlaneCommand("sim/GPS/gcu478/inner_up", "GCU: inner ring tune up."); } }
        public static XPlaneCommand GPSGcu478InnerDown { get { return GetXPlaneCommand("sim/GPS/gcu478/inner_down", "GCU: inner ring tune down."); } }
        public static XPlaneCommand GPSGcu478Cursor { get { return GetXPlaneCommand("sim/GPS/gcu478/cursor", "GCU: cursor/1-2."); } }
        public static XPlaneCommand GPSG1000DisplayReversion { get { return GetXPlaneCommand("sim/GPS/G1000_display_reversion", "G1000 red button for display reversion (backup display)."); } }
    }
}