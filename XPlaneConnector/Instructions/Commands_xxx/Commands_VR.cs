﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand VRXpadHomeButton { get { return GetXPlaneCommand("sim/VR/xpad/home_button", "Press the Home Button of the VR xPad."); } }
        public static XPlaneCommand VRToggle3DMouseCursor { get { return GetXPlaneCommand("sim/VR/toggle_3d_mouse_cursor", "Toggle the 3-d mouse cursor in VR."); } }
        public static XPlaneCommand VRToggleVr { get { return GetXPlaneCommand("sim/VR/toggle_vr", "Toggle enabling of VR hardware."); } }
        public static XPlaneCommand VRGeneralResetView { get { return GetXPlaneCommand("sim/VR/general/reset_view", "Reset VR View."); } }
        public static XPlaneCommand VRQuickZoomView { get { return GetXPlaneCommand("sim/VR/quick_zoom_view", "Temporarily and Quickly Zoom in while in VR."); } }
        public static XPlaneCommand VRReservedSelect { get { return GetXPlaneCommand("sim/VR/reserved/select", "Reserved: VR Select/Trigger Button."); } }
        public static XPlaneCommand VRReservedMenu { get { return GetXPlaneCommand("sim/VR/reserved/menu", "Reserved: VR Menu Button."); } }
        public static XPlaneCommand VRReservedTouchpad { get { return GetXPlaneCommand("sim/VR/reserved/touchpad", "Reserved: VR Thumbstick/Touchpad Button."); } }
    }
}