﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand MagnetosMagnetosOff { get { return GetXPlaneCommand("sim/magnetos/magnetos_off", "Magnetos off."); } }
        public static XPlaneCommand MagnetosMagnetosBoth { get { return GetXPlaneCommand("sim/magnetos/magnetos_both", "Magnetos both."); } }
        public static XPlaneCommand MagnetosMagnetosDown1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_1", "Magnetos down one notch for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosDown2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_2", "Magnetos down one notch for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosDown3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_3", "Magnetos down one notch for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosDown4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_4", "Magnetos down one notch for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosDown5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_5", "Magnetos down one notch for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosDown6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_6", "Magnetos down one notch for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosDown7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_7", "Magnetos down one notch for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosDown8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_down_8", "Magnetos down one notch for engine #8."); } }
        public static XPlaneCommand MagnetosMagnetosUp1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_1", "Magnetos up one notch for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosUp2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_2", "Magnetos up one notch for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosUp3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_3", "Magnetos up one notch for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosUp4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_4", "Magnetos up one notch for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosUp5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_5", "Magnetos up one notch for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosUp6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_6", "Magnetos up one notch for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosUp7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_7", "Magnetos up one notch for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosUp8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_up_8", "Magnetos up one notch for engine #8."); } }
        public static XPlaneCommand MagnetosMagnetosOff1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_1", "Magnetos off for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosOff2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_2", "Magnetos off for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosOff3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_3", "Magnetos off for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosOff4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_4", "Magnetos off for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosOff5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_5", "Magnetos off for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosOff6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_6", "Magnetos off for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosOff7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_7", "Magnetos off for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosOff8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_off_8", "Magnetos off for engine #8."); } }
        public static XPlaneCommand MagnetosMagnetosLeft1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_1", "Magnetos left for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosLeft2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_2", "Magnetos left for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosLeft3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_3", "Magnetos left for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosLeft4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_4", "Magnetos left for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosLeft5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_5", "Magnetos left for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosLeft6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_6", "Magnetos left for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosLeft7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_7", "Magnetos left for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosLeft8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_left_8", "Magnetos left for engine #8."); } }
        public static XPlaneCommand MagnetosMagnetosRight1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_1", "Magnetos right for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosRight2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_2", "Magnetos right for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosRight3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_3", "Magnetos right for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosRight4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_4", "Magnetos right for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosRight5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_5", "Magnetos right for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosRight6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_6", "Magnetos right for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosRight7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_7", "Magnetos right for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosRight8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_right_8", "Magnetos right for engine #8."); } }
        public static XPlaneCommand MagnetosMagnetosBoth1 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_1", "Magnetos both for engine #1."); } }
        public static XPlaneCommand MagnetosMagnetosBoth2 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_2", "Magnetos both for engine #2."); } }
        public static XPlaneCommand MagnetosMagnetosBoth3 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_3", "Magnetos both for engine #3."); } }
        public static XPlaneCommand MagnetosMagnetosBoth4 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_4", "Magnetos both for engine #4."); } }
        public static XPlaneCommand MagnetosMagnetosBoth5 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_5", "Magnetos both for engine #5."); } }
        public static XPlaneCommand MagnetosMagnetosBoth6 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_6", "Magnetos both for engine #6."); } }
        public static XPlaneCommand MagnetosMagnetosBoth7 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_7", "Magnetos both for engine #7."); } }
        public static XPlaneCommand MagnetosMagnetosBoth8 { get { return GetXPlaneCommand("sim/magnetos/magnetos_both_8", "Magnetos both for engine #8."); } }
    }
}