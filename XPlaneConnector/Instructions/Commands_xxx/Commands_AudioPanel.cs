﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand AudioPanelTransmitAudioCom1 { get { return GetXPlaneCommand("sim/audio_panel/transmit_audio_com1", "Transmit audio: COM1."); } }
        public static XPlaneCommand AudioPanelTransmitAudioCom2 { get { return GetXPlaneCommand("sim/audio_panel/transmit_audio_com2", "Transmit audio: COM2."); } }
        public static XPlaneCommand AudioPanelMonitorAudioComAuto { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com_auto", "Monitor audio: COM auto (same as transmit) toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom1 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com1", "Monitor audio: COM1 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom2 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com2", "Monitor audio: COM2 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav1 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav1", "Monitor audio: NAV1 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav2 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav2", "Monitor audio: NAV2 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf1 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf1", "Monitor audio: ADF1 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf2 { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf2", "Monitor audio: ADF2 toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioDme { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_dme", "Monitor audio: DME toggle."); } }
        public static XPlaneCommand AudioPanelMonitorAudioMkr { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_mkr", "Monitor audio: MKR toggle."); } }
        public static XPlaneCommand AudioPanelTransmitAudioCom1Man { get { return GetXPlaneCommand("sim/audio_panel/transmit_audio_com1_man", "Transmit audio: COM1 - old panel, doesn't change listener."); } }
        public static XPlaneCommand AudioPanelTransmitAudioCom2Man { get { return GetXPlaneCommand("sim/audio_panel/transmit_audio_com2_man", "Transmit audio: COM2 - old panel, doesn't change listener."); } }
        public static XPlaneCommand AudioPanelMonitorAudioComAutoOff { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com_auto_off", "Monitor audio: COM auto (same as transmit) off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom1Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com1_off", "Monitor audio: COM1 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom2Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com2_off", "Monitor audio: COM2 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav1Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav1_off", "Monitor audio: NAV1 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav2Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav2_off", "Monitor audio: NAV2 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf1Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf1_off", "Monitor audio: ADF1 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf2Off { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf2_off", "Monitor audio: ADF2 off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioDmeOff { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_dme_off", "Monitor audio: DME off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioMkrOff { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_mkr_off", "Monitor audio: MKR off."); } }
        public static XPlaneCommand AudioPanelMonitorAudioComAutoOn { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com_auto_on", "Monitor audio: COM auto (same as transmit) on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom1On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com1_on", "Monitor audio: COM1 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioCom2On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_com2_on", "Monitor audio: COM2 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav1On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav1_on", "Monitor audio: NAV1 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioNav2On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_nav2_on", "Monitor audio: NAV2 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf1On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf1_on", "Monitor audio: ADF1 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioAdf2On { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_adf2_on", "Monitor audio: ADF2 on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioDmeOn { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_dme_on", "Monitor audio: DME on."); } }
        public static XPlaneCommand AudioPanelMonitorAudioMkrOn { get { return GetXPlaneCommand("sim/audio_panel/monitor_audio_mkr_on", "Monitor audio: MKR on."); } }
    }
}