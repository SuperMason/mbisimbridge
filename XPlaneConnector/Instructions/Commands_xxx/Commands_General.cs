﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand GeneralAction { get { return GetXPlaneCommand("sim/general/action", "General action command."); } }
        public static XPlaneCommand GeneralLeft { get { return GetXPlaneCommand("sim/general/left", "Move view left."); } }
        public static XPlaneCommand GeneralRight { get { return GetXPlaneCommand("sim/general/right", "Move view right."); } }
        public static XPlaneCommand GeneralUp { get { return GetXPlaneCommand("sim/general/up", "Move view up."); } }
        public static XPlaneCommand GeneralDown { get { return GetXPlaneCommand("sim/general/down", "Move view down."); } }
        public static XPlaneCommand GeneralForward { get { return GetXPlaneCommand("sim/general/forward", "Move view forward."); } }
        public static XPlaneCommand GeneralBackward { get { return GetXPlaneCommand("sim/general/backward", "Move view backward."); } }
        public static XPlaneCommand GeneralZoomIn { get { return GetXPlaneCommand("sim/general/zoom_in", "Zoom in."); } }
        public static XPlaneCommand GeneralZoomOut { get { return GetXPlaneCommand("sim/general/zoom_out", "Zoom out."); } }
        public static XPlaneCommand GeneralHatSwitchLeft { get { return GetXPlaneCommand("sim/general/hat_switch_left", "Hat switch left."); } }
        public static XPlaneCommand GeneralHatSwitchRight { get { return GetXPlaneCommand("sim/general/hat_switch_right", "Hat switch right."); } }
        public static XPlaneCommand GeneralHatSwitchUp { get { return GetXPlaneCommand("sim/general/hat_switch_up", "Hat switch up."); } }
        public static XPlaneCommand GeneralHatSwitchDown { get { return GetXPlaneCommand("sim/general/hat_switch_down", "Hat switch down."); } }
        public static XPlaneCommand GeneralHatSwitchUpLeft { get { return GetXPlaneCommand("sim/general/hat_switch_up_left", "Hat switch up + left."); } }
        public static XPlaneCommand GeneralHatSwitchUpRight { get { return GetXPlaneCommand("sim/general/hat_switch_up_right", "Hat switch up + right."); } }
        public static XPlaneCommand GeneralHatSwitchDownLeft { get { return GetXPlaneCommand("sim/general/hat_switch_down_left", "Hat switch down + left."); } }
        public static XPlaneCommand GeneralHatSwitchDownRight { get { return GetXPlaneCommand("sim/general/hat_switch_down_right", "Hat switch down + right."); } }
        public static XPlaneCommand GeneralLeftFast { get { return GetXPlaneCommand("sim/general/left_fast", "Move view left fast."); } }
        public static XPlaneCommand GeneralRightFast { get { return GetXPlaneCommand("sim/general/right_fast", "Move view right fast."); } }
        public static XPlaneCommand GeneralUpFast { get { return GetXPlaneCommand("sim/general/up_fast", "Move view up fast."); } }
        public static XPlaneCommand GeneralDownFast { get { return GetXPlaneCommand("sim/general/down_fast", "Move view down fast."); } }
        public static XPlaneCommand GeneralForwardFast { get { return GetXPlaneCommand("sim/general/forward_fast", "Move view forward fast."); } }
        public static XPlaneCommand GeneralBackwardFast { get { return GetXPlaneCommand("sim/general/backward_fast", "Move view backward fast."); } }
        public static XPlaneCommand GeneralZoomInFast { get { return GetXPlaneCommand("sim/general/zoom_in_fast", "Zoom in fast."); } }
        public static XPlaneCommand GeneralZoomOutFast { get { return GetXPlaneCommand("sim/general/zoom_out_fast", "Zoom out fast."); } }
        public static XPlaneCommand GeneralLeftSlow { get { return GetXPlaneCommand("sim/general/left_slow", "Move view left slow."); } }
        public static XPlaneCommand GeneralRightSlow { get { return GetXPlaneCommand("sim/general/right_slow", "Move view right slow."); } }
        public static XPlaneCommand GeneralUpSlow { get { return GetXPlaneCommand("sim/general/up_slow", "Move view up slow."); } }
        public static XPlaneCommand GeneralDownSlow { get { return GetXPlaneCommand("sim/general/down_slow", "Move view down slow."); } }
        public static XPlaneCommand GeneralForwardSlow { get { return GetXPlaneCommand("sim/general/forward_slow", "Move view forward slow."); } }
        public static XPlaneCommand GeneralBackwardSlow { get { return GetXPlaneCommand("sim/general/backward_slow", "Move view backward slow."); } }
        public static XPlaneCommand GeneralZoomInSlow { get { return GetXPlaneCommand("sim/general/zoom_in_slow", "Zoom in slow."); } }
        public static XPlaneCommand GeneralZoomOutSlow { get { return GetXPlaneCommand("sim/general/zoom_out_slow", "Zoom out slow."); } }
        public static XPlaneCommand GeneralRotUp { get { return GetXPlaneCommand("sim/general/rot_up", "Rotate view: tilt up."); } }
        public static XPlaneCommand GeneralRotDown { get { return GetXPlaneCommand("sim/general/rot_down", "Rotate view: tilt down."); } }
        public static XPlaneCommand GeneralRotLeft { get { return GetXPlaneCommand("sim/general/rot_left", "Rotate view: pan left."); } }
        public static XPlaneCommand GeneralRotRight { get { return GetXPlaneCommand("sim/general/rot_right", "Rotate view: pan right."); } }
        public static XPlaneCommand GeneralRotUpFast { get { return GetXPlaneCommand("sim/general/rot_up_fast", "Rotate view: tilt up fast."); } }
        public static XPlaneCommand GeneralRotDownFast { get { return GetXPlaneCommand("sim/general/rot_down_fast", "Rotate view: tilt down fast."); } }
        public static XPlaneCommand GeneralRotLeftFast { get { return GetXPlaneCommand("sim/general/rot_left_fast", "Rotate view: pan left fast."); } }
        public static XPlaneCommand GeneralRotRightFast { get { return GetXPlaneCommand("sim/general/rot_right_fast", "Rotate view: pan right fast."); } }
        public static XPlaneCommand GeneralRotUpSlow { get { return GetXPlaneCommand("sim/general/rot_up_slow", "Rotate view: tilt up slow."); } }
        public static XPlaneCommand GeneralRotDownSlow { get { return GetXPlaneCommand("sim/general/rot_down_slow", "Rotate view: tilt down slow."); } }
        public static XPlaneCommand GeneralRotLeftSlow { get { return GetXPlaneCommand("sim/general/rot_left_slow", "Rotate view: pan left slow."); } }
        public static XPlaneCommand GeneralRotRightSlow { get { return GetXPlaneCommand("sim/general/rot_right_slow", "Rotate view: pan right slow."); } }
        public static XPlaneCommand GeneralTrackP0 { get { return GetXPlaneCommand("sim/general/track_p0", "Views track aircraft: Yours."); } }
        public static XPlaneCommand GeneralTrackPNext { get { return GetXPlaneCommand("sim/general/track_p_next", "Views track aircraft: Next"); } }
        public static XPlaneCommand GeneralTrackPPrev { get { return GetXPlaneCommand("sim/general/track_p_prev", "Views track aircraft: Previous."); } }
        public static XPlaneCommand GeneralToggleArtificialStabWin { get { return GetXPlaneCommand("sim/general/toggle_artificial_stab_win", "Toggle the artificial stability constants window."); } }
        public static XPlaneCommand GeneralToggleTrafficPaths { get { return GetXPlaneCommand("sim/general/toggle_traffic_paths", "Toggle display of traffic paths."); } }
        public static XPlaneCommand GeneralToggleAutopilotConstantsWin { get { return GetXPlaneCommand("sim/general/toggle_autopilot_constants_win", "Toggle the autopilot constants window."); } }
        public static XPlaneCommand GeneralToggleFadecWin { get { return GetXPlaneCommand("sim/general/toggle_fadec_win", "Toggle the FADEC constants window."); } }
        public static XPlaneCommand GeneralToggleControlDeflectionsWin { get { return GetXPlaneCommand("sim/general/toggle_control_deflections_win", "Toggle the control deflections window."); } }
        public static XPlaneCommand GeneralToggleWeaponGuidanceWin { get { return GetXPlaneCommand("sim/general/toggle_weapon_guidance_win", "Toggle the weapon guidance window."); } }
        public static XPlaneCommand GeneralToggleProjectionWin { get { return GetXPlaneCommand("sim/general/toggle_projection_win", "Toggle the projection configuration window."); } }
    }
}