﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand EnginesEngageStarters { get { return GetXPlaneCommand("sim/engines/engage_starters", "Engage starters."); } }
        public static XPlaneCommand EnginesThrottleDown { get { return GetXPlaneCommand("sim/engines/throttle_down", "Throttle down a bit."); } }
        public static XPlaneCommand EnginesThrottleUp { get { return GetXPlaneCommand("sim/engines/throttle_up", "Throttle up a bit."); } }
        public static XPlaneCommand EnginesTOGAPower { get { return GetXPlaneCommand("sim/engines/TOGA_power", "Engage TOGA power."); } }
        public static XPlaneCommand EnginesPropDown { get { return GetXPlaneCommand("sim/engines/prop_down", "Prop coarse a bit."); } }
        public static XPlaneCommand EnginesPropUp { get { return GetXPlaneCommand("sim/engines/prop_up", "Prop fine a bit."); } }
        public static XPlaneCommand EnginesMixtureMin { get { return GetXPlaneCommand("sim/engines/mixture_min", "Mixture to cut off."); } }
        public static XPlaneCommand EnginesMixtureDown { get { return GetXPlaneCommand("sim/engines/mixture_down", "Mixture lean a bit."); } }
        public static XPlaneCommand EnginesMixtureUp { get { return GetXPlaneCommand("sim/engines/mixture_up", "Mixture rich a bit."); } }
        public static XPlaneCommand EnginesMixtureMax { get { return GetXPlaneCommand("sim/engines/mixture_max", "Mixture to full rich."); } }
        public static XPlaneCommand EnginesCarbHeatOff { get { return GetXPlaneCommand("sim/engines/carb_heat_off", "Carb heat off."); } }
        public static XPlaneCommand EnginesCarbHeatOn { get { return GetXPlaneCommand("sim/engines/carb_heat_on", "Carb heat on."); } }
        public static XPlaneCommand EnginesCarbHeatToggle { get { return GetXPlaneCommand("sim/engines/carb_heat_toggle", "Carb heat toggle."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle", "Idle high/low toggle."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle1 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_1", "Idle high/low toggle Engine 1."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle2 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_2", "Idle high/low toggle Engine 2."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle3 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_3", "Idle high/low toggle Engine 3."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle4 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_4", "Idle high/low toggle Engine 4."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle5 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_5", "Idle high/low toggle Engine 5."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle6 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_6", "Idle high/low toggle Engine 6."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle7 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_7", "Idle high/low toggle Engine 7."); } }
        public static XPlaneCommand EnginesIdleHiLoToggle8 { get { return GetXPlaneCommand("sim/engines/idle_hi_lo_toggle_8", "Idle high/low toggle Engine 8."); } }
        public static XPlaneCommand EnginesGovernorOn { get { return GetXPlaneCommand("sim/engines/governor_on", "Throttle governor on."); } }
        public static XPlaneCommand EnginesGovernorOff { get { return GetXPlaneCommand("sim/engines/governor_off", "Throttle governor off."); } }
        public static XPlaneCommand EnginesGovernorToggle { get { return GetXPlaneCommand("sim/engines/governor_toggle", "Throttle governor toggle."); } }
        public static XPlaneCommand EnginesClutchOn { get { return GetXPlaneCommand("sim/engines/clutch_on", "Clutch on."); } }
        public static XPlaneCommand EnginesClutchOff { get { return GetXPlaneCommand("sim/engines/clutch_off", "Clutch off."); } }
        public static XPlaneCommand EnginesClutchToggle { get { return GetXPlaneCommand("sim/engines/clutch_toggle", "Clutch toggle."); } }
        public static XPlaneCommand EnginesBetaToggle { get { return GetXPlaneCommand("sim/engines/beta_toggle", "Toggle Beta prop."); } }
        public static XPlaneCommand EnginesThrustReverseToggle { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle", "Toggle thrust reversers."); } }
        public static XPlaneCommand EnginesThrustReverseHold { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold", "Hold thrust reverse at max."); } }
        public static XPlaneCommand EnginesThrottleDown1 { get { return GetXPlaneCommand("sim/engines/throttle_down_1", "Throttle down a bit for engine #1."); } }
        public static XPlaneCommand EnginesThrottleDown2 { get { return GetXPlaneCommand("sim/engines/throttle_down_2", "Throttle down a bit for engine #2."); } }
        public static XPlaneCommand EnginesThrottleDown3 { get { return GetXPlaneCommand("sim/engines/throttle_down_3", "Throttle down a bit for engine #3."); } }
        public static XPlaneCommand EnginesThrottleDown4 { get { return GetXPlaneCommand("sim/engines/throttle_down_4", "Throttle down a bit for engine #4."); } }
        public static XPlaneCommand EnginesThrottleDown5 { get { return GetXPlaneCommand("sim/engines/throttle_down_5", "Throttle down a bit for engine #5."); } }
        public static XPlaneCommand EnginesThrottleDown6 { get { return GetXPlaneCommand("sim/engines/throttle_down_6", "Throttle down a bit for engine #6."); } }
        public static XPlaneCommand EnginesThrottleDown7 { get { return GetXPlaneCommand("sim/engines/throttle_down_7", "Throttle down a bit for engine #7."); } }
        public static XPlaneCommand EnginesThrottleDown8 { get { return GetXPlaneCommand("sim/engines/throttle_down_8", "Throttle down a bit for engine #8."); } }
        public static XPlaneCommand EnginesThrottleUp1 { get { return GetXPlaneCommand("sim/engines/throttle_up_1", "Throttle up a bit for engine #1."); } }
        public static XPlaneCommand EnginesThrottleUp2 { get { return GetXPlaneCommand("sim/engines/throttle_up_2", "Throttle up a bit for engine #2."); } }
        public static XPlaneCommand EnginesThrottleUp3 { get { return GetXPlaneCommand("sim/engines/throttle_up_3", "Throttle up a bit for engine #3."); } }
        public static XPlaneCommand EnginesThrottleUp4 { get { return GetXPlaneCommand("sim/engines/throttle_up_4", "Throttle up a bit for engine #4."); } }
        public static XPlaneCommand EnginesThrottleUp5 { get { return GetXPlaneCommand("sim/engines/throttle_up_5", "Throttle up a bit for engine #5."); } }
        public static XPlaneCommand EnginesThrottleUp6 { get { return GetXPlaneCommand("sim/engines/throttle_up_6", "Throttle up a bit for engine #6."); } }
        public static XPlaneCommand EnginesThrottleUp7 { get { return GetXPlaneCommand("sim/engines/throttle_up_7", "Throttle up a bit for engine #7."); } }
        public static XPlaneCommand EnginesThrottleUp8 { get { return GetXPlaneCommand("sim/engines/throttle_up_8", "Throttle up a bit for engine #8."); } }
        public static XPlaneCommand EnginesPropDown1 { get { return GetXPlaneCommand("sim/engines/prop_down_1", "Prop down a bit for engine #1."); } }
        public static XPlaneCommand EnginesPropDown2 { get { return GetXPlaneCommand("sim/engines/prop_down_2", "Prop down a bit for engine #2."); } }
        public static XPlaneCommand EnginesPropDown3 { get { return GetXPlaneCommand("sim/engines/prop_down_3", "Prop down a bit for engine #3."); } }
        public static XPlaneCommand EnginesPropDown4 { get { return GetXPlaneCommand("sim/engines/prop_down_4", "Prop down a bit for engine #4."); } }
        public static XPlaneCommand EnginesPropDown5 { get { return GetXPlaneCommand("sim/engines/prop_down_5", "Prop down a bit for engine #5."); } }
        public static XPlaneCommand EnginesPropDown6 { get { return GetXPlaneCommand("sim/engines/prop_down_6", "Prop down a bit for engine #6."); } }
        public static XPlaneCommand EnginesPropDown7 { get { return GetXPlaneCommand("sim/engines/prop_down_7", "Prop down a bit for engine #7."); } }
        public static XPlaneCommand EnginesPropDown8 { get { return GetXPlaneCommand("sim/engines/prop_down_8", "Prop down a bit for engine #8."); } }
        public static XPlaneCommand EnginesPropUp1 { get { return GetXPlaneCommand("sim/engines/prop_up_1", "Prop up a bit for engine #1."); } }
        public static XPlaneCommand EnginesPropUp2 { get { return GetXPlaneCommand("sim/engines/prop_up_2", "Prop up a bit for engine #2."); } }
        public static XPlaneCommand EnginesPropUp3 { get { return GetXPlaneCommand("sim/engines/prop_up_3", "Prop up a bit for engine #3."); } }
        public static XPlaneCommand EnginesPropUp4 { get { return GetXPlaneCommand("sim/engines/prop_up_4", "Prop up a bit for engine #4."); } }
        public static XPlaneCommand EnginesPropUp5 { get { return GetXPlaneCommand("sim/engines/prop_up_5", "Prop up a bit for engine #5."); } }
        public static XPlaneCommand EnginesPropUp6 { get { return GetXPlaneCommand("sim/engines/prop_up_6", "Prop up a bit for engine #6."); } }
        public static XPlaneCommand EnginesPropUp7 { get { return GetXPlaneCommand("sim/engines/prop_up_7", "Prop up a bit for engine #7."); } }
        public static XPlaneCommand EnginesPropUp8 { get { return GetXPlaneCommand("sim/engines/prop_up_8", "Prop up a bit for engine #8."); } }
        public static XPlaneCommand EnginesMixtureDown1 { get { return GetXPlaneCommand("sim/engines/mixture_down_1", "Mixture down a bit for engine #1."); } }
        public static XPlaneCommand EnginesMixtureDown2 { get { return GetXPlaneCommand("sim/engines/mixture_down_2", "Mixture down a bit for engine #2."); } }
        public static XPlaneCommand EnginesMixtureDown3 { get { return GetXPlaneCommand("sim/engines/mixture_down_3", "Mixture down a bit for engine #3."); } }
        public static XPlaneCommand EnginesMixtureDown4 { get { return GetXPlaneCommand("sim/engines/mixture_down_4", "Mixture down a bit for engine #4."); } }
        public static XPlaneCommand EnginesMixtureDown5 { get { return GetXPlaneCommand("sim/engines/mixture_down_5", "Mixture down a bit for engine #5."); } }
        public static XPlaneCommand EnginesMixtureDown6 { get { return GetXPlaneCommand("sim/engines/mixture_down_6", "Mixture down a bit for engine #6."); } }
        public static XPlaneCommand EnginesMixtureDown7 { get { return GetXPlaneCommand("sim/engines/mixture_down_7", "Mixture down a bit for engine #7."); } }
        public static XPlaneCommand EnginesMixtureDown8 { get { return GetXPlaneCommand("sim/engines/mixture_down_8", "Mixture down a bit for engine #8."); } }
        public static XPlaneCommand EnginesMixtureUp1 { get { return GetXPlaneCommand("sim/engines/mixture_up_1", "Mixture up a bit for engine #1."); } }
        public static XPlaneCommand EnginesMixtureUp2 { get { return GetXPlaneCommand("sim/engines/mixture_up_2", "Mixture up a bit for engine #2."); } }
        public static XPlaneCommand EnginesMixtureUp3 { get { return GetXPlaneCommand("sim/engines/mixture_up_3", "Mixture up a bit for engine #3."); } }
        public static XPlaneCommand EnginesMixtureUp4 { get { return GetXPlaneCommand("sim/engines/mixture_up_4", "Mixture up a bit for engine #4."); } }
        public static XPlaneCommand EnginesMixtureUp5 { get { return GetXPlaneCommand("sim/engines/mixture_up_5", "Mixture up a bit for engine #5."); } }
        public static XPlaneCommand EnginesMixtureUp6 { get { return GetXPlaneCommand("sim/engines/mixture_up_6", "Mixture up a bit for engine #6."); } }
        public static XPlaneCommand EnginesMixtureUp7 { get { return GetXPlaneCommand("sim/engines/mixture_up_7", "Mixture up a bit for engine #7."); } }
        public static XPlaneCommand EnginesMixtureUp8 { get { return GetXPlaneCommand("sim/engines/mixture_up_8", "Mixture up a bit for engine #8."); } }
        public static XPlaneCommand EnginesBetaToggle1 { get { return GetXPlaneCommand("sim/engines/beta_toggle_1", "Toggle beta prop #1."); } }
        public static XPlaneCommand EnginesBetaToggle2 { get { return GetXPlaneCommand("sim/engines/beta_toggle_2", "Toggle beta prop #2."); } }
        public static XPlaneCommand EnginesBetaToggle3 { get { return GetXPlaneCommand("sim/engines/beta_toggle_3", "Toggle beta prop #3."); } }
        public static XPlaneCommand EnginesBetaToggle4 { get { return GetXPlaneCommand("sim/engines/beta_toggle_4", "Toggle beta prop #4."); } }
        public static XPlaneCommand EnginesBetaToggle5 { get { return GetXPlaneCommand("sim/engines/beta_toggle_5", "Toggle beta prop #5."); } }
        public static XPlaneCommand EnginesBetaToggle6 { get { return GetXPlaneCommand("sim/engines/beta_toggle_6", "Toggle beta prop #6."); } }
        public static XPlaneCommand EnginesBetaToggle7 { get { return GetXPlaneCommand("sim/engines/beta_toggle_7", "Toggle beta prop #7."); } }
        public static XPlaneCommand EnginesBetaToggle8 { get { return GetXPlaneCommand("sim/engines/beta_toggle_8", "Toggle beta prop #8."); } }
        public static XPlaneCommand EnginesThrustReverseToggle1 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_1", "Toggle thrust reversers #1."); } }
        public static XPlaneCommand EnginesThrustReverseToggle2 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_2", "Toggle thrust reversers #2."); } }
        public static XPlaneCommand EnginesThrustReverseToggle3 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_3", "Toggle thrust reversers #3."); } }
        public static XPlaneCommand EnginesThrustReverseToggle4 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_4", "Toggle thrust reversers #4."); } }
        public static XPlaneCommand EnginesThrustReverseToggle5 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_5", "Toggle thrust reversers #5."); } }
        public static XPlaneCommand EnginesThrustReverseToggle6 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_6", "Toggle thrust reversers #6."); } }
        public static XPlaneCommand EnginesThrustReverseToggle7 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_7", "Toggle thrust reversers #7."); } }
        public static XPlaneCommand EnginesThrustReverseToggle8 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_toggle_8", "Toggle thrust reversers #8."); } }
        public static XPlaneCommand EnginesThrustReverseHold1 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_1", "Hold thrust reverse at max #1."); } }
        public static XPlaneCommand EnginesThrustReverseHold2 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_2", "Hold thrust reverse at max #2."); } }
        public static XPlaneCommand EnginesThrustReverseHold3 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_3", "Hold thrust reverse at max #3."); } }
        public static XPlaneCommand EnginesThrustReverseHold4 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_4", "Hold thrust reverse at max #4."); } }
        public static XPlaneCommand EnginesThrustReverseHold5 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_5", "Hold thrust reverse at max #5."); } }
        public static XPlaneCommand EnginesThrustReverseHold6 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_6", "Hold thrust reverse at max #6."); } }
        public static XPlaneCommand EnginesThrustReverseHold7 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_7", "Hold thrust reverse at max #7."); } }
        public static XPlaneCommand EnginesThrustReverseHold8 { get { return GetXPlaneCommand("sim/engines/thrust_reverse_hold_8", "Hold thrust reverse at max #8."); } }
        public static XPlaneCommand EnginesFireExt1Off { get { return GetXPlaneCommand("sim/engines/fire_ext_1_off", "Fire extinguisher #1 off."); } }
        public static XPlaneCommand EnginesFireExt2Off { get { return GetXPlaneCommand("sim/engines/fire_ext_2_off", "Fire extinguisher #2 off."); } }
        public static XPlaneCommand EnginesFireExt3Off { get { return GetXPlaneCommand("sim/engines/fire_ext_3_off", "Fire extinguisher #3 off."); } }
        public static XPlaneCommand EnginesFireExt4Off { get { return GetXPlaneCommand("sim/engines/fire_ext_4_off", "Fire extinguisher #4 off."); } }
        public static XPlaneCommand EnginesFireExt5Off { get { return GetXPlaneCommand("sim/engines/fire_ext_5_off", "Fire extinguisher #5 off."); } }
        public static XPlaneCommand EnginesFireExt6Off { get { return GetXPlaneCommand("sim/engines/fire_ext_6_off", "Fire extinguisher #6 off."); } }
        public static XPlaneCommand EnginesFireExt7Off { get { return GetXPlaneCommand("sim/engines/fire_ext_7_off", "Fire extinguisher #7 off."); } }
        public static XPlaneCommand EnginesFireExt8Off { get { return GetXPlaneCommand("sim/engines/fire_ext_8_off", "Fire extinguisher #8 off."); } }
        public static XPlaneCommand EnginesFireExt1On { get { return GetXPlaneCommand("sim/engines/fire_ext_1_on", "Fire extinguisher #1 on."); } }
        public static XPlaneCommand EnginesFireExt2On { get { return GetXPlaneCommand("sim/engines/fire_ext_2_on", "Fire extinguisher #2 on."); } }
        public static XPlaneCommand EnginesFireExt3On { get { return GetXPlaneCommand("sim/engines/fire_ext_3_on", "Fire extinguisher #3 on."); } }
        public static XPlaneCommand EnginesFireExt4On { get { return GetXPlaneCommand("sim/engines/fire_ext_4_on", "Fire extinguisher #4 on."); } }
        public static XPlaneCommand EnginesFireExt5On { get { return GetXPlaneCommand("sim/engines/fire_ext_5_on", "Fire extinguisher #5 on."); } }
        public static XPlaneCommand EnginesFireExt6On { get { return GetXPlaneCommand("sim/engines/fire_ext_6_on", "Fire extinguisher #6 on."); } }
        public static XPlaneCommand EnginesFireExt7On { get { return GetXPlaneCommand("sim/engines/fire_ext_7_on", "Fire extinguisher #7 on."); } }
        public static XPlaneCommand EnginesFireExt8On { get { return GetXPlaneCommand("sim/engines/fire_ext_8_on", "Fire extinguisher #8 on."); } }
        public static XPlaneCommand EnginesRocketsUp { get { return GetXPlaneCommand("sim/engines/rockets_up", "Orbital maneuver rockets up."); } }
        public static XPlaneCommand EnginesRocketsDown { get { return GetXPlaneCommand("sim/engines/rockets_down", "Orbital maneuver rockets down."); } }
        public static XPlaneCommand EnginesRocketsLeft { get { return GetXPlaneCommand("sim/engines/rockets_left", "Orbital maneuver rockets left."); } }
        public static XPlaneCommand EnginesRocketsRight { get { return GetXPlaneCommand("sim/engines/rockets_right", "Orbital maneuver rockets right."); } }
        public static XPlaneCommand EnginesRocketsForward { get { return GetXPlaneCommand("sim/engines/rockets_forward", "Orbital maneuver rockets fore."); } }
        public static XPlaneCommand EnginesRocketsAft { get { return GetXPlaneCommand("sim/engines/rockets_aft", "Orbital maneuver rockets aft."); } }
    }
}