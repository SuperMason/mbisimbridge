﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand LightsNavLightsOn { get { return GetXPlaneCommand("sim/lights/nav_lights_on", "Nav lights on."); } }
        public static XPlaneCommand LightsNavLightsOff { get { return GetXPlaneCommand("sim/lights/nav_lights_off", "Nav lights off."); } }
        public static XPlaneCommand LightsNavLightsToggle { get { return GetXPlaneCommand("sim/lights/nav_lights_toggle", "Nav lights toggle."); } }
        public static XPlaneCommand LightsBeaconLightsOn { get { return GetXPlaneCommand("sim/lights/beacon_lights_on", "Beacon lights on."); } }
        public static XPlaneCommand LightsBeaconLightsOff { get { return GetXPlaneCommand("sim/lights/beacon_lights_off", "Beacon lights off."); } }
        public static XPlaneCommand LightsBeaconLightsToggle { get { return GetXPlaneCommand("sim/lights/beacon_lights_toggle", "Beacon lights toggle."); } }
        public static XPlaneCommand LightsStrobeLightsOn { get { return GetXPlaneCommand("sim/lights/strobe_lights_on", "Strobe lights on."); } }
        public static XPlaneCommand LightsStrobeLightsOff { get { return GetXPlaneCommand("sim/lights/strobe_lights_off", "Strobe lights off."); } }
        public static XPlaneCommand LightsStrobeLightsToggle { get { return GetXPlaneCommand("sim/lights/strobe_lights_toggle", "Strobe lights toggle."); } }
        public static XPlaneCommand LightsTaxiLightsOn { get { return GetXPlaneCommand("sim/lights/taxi_lights_on", "Taxi lights on."); } }
        public static XPlaneCommand LightsTaxiLightsOff { get { return GetXPlaneCommand("sim/lights/taxi_lights_off", "Taxi lights off."); } }
        public static XPlaneCommand LightsTaxiLightsToggle { get { return GetXPlaneCommand("sim/lights/taxi_lights_toggle", "Taxi lights toggle."); } }
        public static XPlaneCommand LightsLandingLightsOn { get { return GetXPlaneCommand("sim/lights/landing_lights_on", "Landing lights on."); } }
        public static XPlaneCommand LightsLandingLightsOff { get { return GetXPlaneCommand("sim/lights/landing_lights_off", "Landing lights off."); } }
        public static XPlaneCommand LightsLandingLightsToggle { get { return GetXPlaneCommand("sim/lights/landing_lights_toggle", "Landing lights toggle."); } }
        public static XPlaneCommand LightsLanding01LightOn { get { return GetXPlaneCommand("sim/lights/landing_01_light_on", "Landing light 01 on."); } }
        public static XPlaneCommand LightsLanding02LightOn { get { return GetXPlaneCommand("sim/lights/landing_02_light_on", "Landing light 02 on."); } }
        public static XPlaneCommand LightsLanding03LightOn { get { return GetXPlaneCommand("sim/lights/landing_03_light_on", "Landing light 03 on."); } }
        public static XPlaneCommand LightsLanding04LightOn { get { return GetXPlaneCommand("sim/lights/landing_04_light_on", "Landing light 04 on."); } }
        public static XPlaneCommand LightsLanding05LightOn { get { return GetXPlaneCommand("sim/lights/landing_05_light_on", "Landing light 05 on."); } }
        public static XPlaneCommand LightsLanding06LightOn { get { return GetXPlaneCommand("sim/lights/landing_06_light_on", "Landing light 06 on."); } }
        public static XPlaneCommand LightsLanding07LightOn { get { return GetXPlaneCommand("sim/lights/landing_07_light_on", "Landing light 07 on."); } }
        public static XPlaneCommand LightsLanding08LightOn { get { return GetXPlaneCommand("sim/lights/landing_08_light_on", "Landing light 08 on."); } }
        public static XPlaneCommand LightsLanding09LightOn { get { return GetXPlaneCommand("sim/lights/landing_09_light_on", "Landing light 09 on."); } }
        public static XPlaneCommand LightsLanding10LightOn { get { return GetXPlaneCommand("sim/lights/landing_10_light_on", "Landing light 10 on."); } }
        public static XPlaneCommand LightsLanding11LightOn { get { return GetXPlaneCommand("sim/lights/landing_11_light_on", "Landing light 11 on."); } }
        public static XPlaneCommand LightsLanding12LightOn { get { return GetXPlaneCommand("sim/lights/landing_12_light_on", "Landing light 12 on."); } }
        public static XPlaneCommand LightsLanding13LightOn { get { return GetXPlaneCommand("sim/lights/landing_13_light_on", "Landing light 13 on."); } }
        public static XPlaneCommand LightsLanding14LightOn { get { return GetXPlaneCommand("sim/lights/landing_14_light_on", "Landing light 14 on."); } }
        public static XPlaneCommand LightsLanding15LightOn { get { return GetXPlaneCommand("sim/lights/landing_15_light_on", "Landing light 15 on."); } }
        public static XPlaneCommand LightsLanding16LightOn { get { return GetXPlaneCommand("sim/lights/landing_16_light_on", "Landing light 16 on."); } }
        public static XPlaneCommand LightsLanding01LightOff { get { return GetXPlaneCommand("sim/lights/landing_01_light_off", "Landing light 01 off."); } }
        public static XPlaneCommand LightsLanding02LightOff { get { return GetXPlaneCommand("sim/lights/landing_02_light_off", "Landing light 02 off."); } }
        public static XPlaneCommand LightsLanding03LightOff { get { return GetXPlaneCommand("sim/lights/landing_03_light_off", "Landing light 03 off."); } }
        public static XPlaneCommand LightsLanding04LightOff { get { return GetXPlaneCommand("sim/lights/landing_04_light_off", "Landing light 04 off."); } }
        public static XPlaneCommand LightsLanding05LightOff { get { return GetXPlaneCommand("sim/lights/landing_05_light_off", "Landing light 05 off."); } }
        public static XPlaneCommand LightsLanding06LightOff { get { return GetXPlaneCommand("sim/lights/landing_06_light_off", "Landing light 06 off."); } }
        public static XPlaneCommand LightsLanding07LightOff { get { return GetXPlaneCommand("sim/lights/landing_07_light_off", "Landing light 07 off."); } }
        public static XPlaneCommand LightsLanding08LightOff { get { return GetXPlaneCommand("sim/lights/landing_08_light_off", "Landing light 08 off."); } }
        public static XPlaneCommand LightsLanding09LightOff { get { return GetXPlaneCommand("sim/lights/landing_09_light_off", "Landing light 09 off."); } }
        public static XPlaneCommand LightsLanding10LightOff { get { return GetXPlaneCommand("sim/lights/landing_10_light_off", "Landing light 10 off."); } }
        public static XPlaneCommand LightsLanding11LightOff { get { return GetXPlaneCommand("sim/lights/landing_11_light_off", "Landing light 11 off."); } }
        public static XPlaneCommand LightsLanding12LightOff { get { return GetXPlaneCommand("sim/lights/landing_12_light_off", "Landing light 12 off."); } }
        public static XPlaneCommand LightsLanding13LightOff { get { return GetXPlaneCommand("sim/lights/landing_13_light_off", "Landing light 13 off."); } }
        public static XPlaneCommand LightsLanding14LightOff { get { return GetXPlaneCommand("sim/lights/landing_14_light_off", "Landing light 14 off."); } }
        public static XPlaneCommand LightsLanding15LightOff { get { return GetXPlaneCommand("sim/lights/landing_15_light_off", "Landing light 15 off."); } }
        public static XPlaneCommand LightsLanding16LightOff { get { return GetXPlaneCommand("sim/lights/landing_16_light_off", "Landing light 16 off."); } }
        public static XPlaneCommand LightsLanding01LightTog { get { return GetXPlaneCommand("sim/lights/landing_01_light_tog", "Landing light 01 toggle."); } }
        public static XPlaneCommand LightsLanding02LightTog { get { return GetXPlaneCommand("sim/lights/landing_02_light_tog", "Landing light 02 toggle."); } }
        public static XPlaneCommand LightsLanding03LightTog { get { return GetXPlaneCommand("sim/lights/landing_03_light_tog", "Landing light 03 toggle."); } }
        public static XPlaneCommand LightsLanding04LightTog { get { return GetXPlaneCommand("sim/lights/landing_04_light_tog", "Landing light 04 toggle."); } }
        public static XPlaneCommand LightsLanding05LightTog { get { return GetXPlaneCommand("sim/lights/landing_05_light_tog", "Landing light 05 toggle."); } }
        public static XPlaneCommand LightsLanding06LightTog { get { return GetXPlaneCommand("sim/lights/landing_06_light_tog", "Landing light 06 toggle."); } }
        public static XPlaneCommand LightsLanding07LightTog { get { return GetXPlaneCommand("sim/lights/landing_07_light_tog", "Landing light 07 toggle."); } }
        public static XPlaneCommand LightsLanding08LightTog { get { return GetXPlaneCommand("sim/lights/landing_08_light_tog", "Landing light 08 toggle."); } }
        public static XPlaneCommand LightsLanding09LightTog { get { return GetXPlaneCommand("sim/lights/landing_09_light_tog", "Landing light 09 toggle."); } }
        public static XPlaneCommand LightsLanding10LightTog { get { return GetXPlaneCommand("sim/lights/landing_10_light_tog", "Landing light 10 toggle."); } }
        public static XPlaneCommand LightsLanding11LightTog { get { return GetXPlaneCommand("sim/lights/landing_11_light_tog", "Landing light 11 toggle."); } }
        public static XPlaneCommand LightsLanding12LightTog { get { return GetXPlaneCommand("sim/lights/landing_12_light_tog", "Landing light 12 toggle."); } }
        public static XPlaneCommand LightsLanding13LightTog { get { return GetXPlaneCommand("sim/lights/landing_13_light_tog", "Landing light 13 toggle."); } }
        public static XPlaneCommand LightsLanding14LightTog { get { return GetXPlaneCommand("sim/lights/landing_14_light_tog", "Landing light 14 toggle."); } }
        public static XPlaneCommand LightsLanding15LightTog { get { return GetXPlaneCommand("sim/lights/landing_15_light_tog", "Landing light 15 toggle."); } }
        public static XPlaneCommand LightsLanding16LightTog { get { return GetXPlaneCommand("sim/lights/landing_16_light_tog", "Landing light 16 toggle."); } }
        public static XPlaneCommand LightsGeneric01LightTog { get { return GetXPlaneCommand("sim/lights/generic_01_light_tog", "Generic light 01 toggle."); } }
        public static XPlaneCommand LightsGeneric02LightTog { get { return GetXPlaneCommand("sim/lights/generic_02_light_tog", "Generic light 02 toggle."); } }
        public static XPlaneCommand LightsGeneric03LightTog { get { return GetXPlaneCommand("sim/lights/generic_03_light_tog", "Generic light 03 toggle."); } }
        public static XPlaneCommand LightsGeneric04LightTog { get { return GetXPlaneCommand("sim/lights/generic_04_light_tog", "Generic light 04 toggle."); } }
        public static XPlaneCommand LightsGeneric05LightTog { get { return GetXPlaneCommand("sim/lights/generic_05_light_tog", "Generic light 05 toggle."); } }
        public static XPlaneCommand LightsGeneric06LightTog { get { return GetXPlaneCommand("sim/lights/generic_06_light_tog", "Generic light 06 toggle."); } }
        public static XPlaneCommand LightsGeneric07LightTog { get { return GetXPlaneCommand("sim/lights/generic_07_light_tog", "Generic light 07 toggle."); } }
        public static XPlaneCommand LightsGeneric08LightTog { get { return GetXPlaneCommand("sim/lights/generic_08_light_tog", "Generic light 08 toggle."); } }
        public static XPlaneCommand LightsGeneric09LightTog { get { return GetXPlaneCommand("sim/lights/generic_09_light_tog", "Generic light 09 toggle."); } }
        public static XPlaneCommand LightsGeneric10LightTog { get { return GetXPlaneCommand("sim/lights/generic_10_light_tog", "Generic light 10 toggle."); } }
        public static XPlaneCommand LightsGeneric11LightTog { get { return GetXPlaneCommand("sim/lights/generic_11_light_tog", "Generic light 11 toggle."); } }
        public static XPlaneCommand LightsGeneric12LightTog { get { return GetXPlaneCommand("sim/lights/generic_12_light_tog", "Generic light 12 toggle."); } }
        public static XPlaneCommand LightsGeneric13LightTog { get { return GetXPlaneCommand("sim/lights/generic_13_light_tog", "Generic light 13 toggle."); } }
        public static XPlaneCommand LightsGeneric14LightTog { get { return GetXPlaneCommand("sim/lights/generic_14_light_tog", "Generic light 14 toggle."); } }
        public static XPlaneCommand LightsGeneric15LightTog { get { return GetXPlaneCommand("sim/lights/generic_15_light_tog", "Generic light 15 toggle."); } }
        public static XPlaneCommand LightsGeneric16LightTog { get { return GetXPlaneCommand("sim/lights/generic_16_light_tog", "Generic light 16 toggle."); } }
        public static XPlaneCommand LightsGeneric17LightTog { get { return GetXPlaneCommand("sim/lights/generic_17_light_tog", "Generic light 17 toggle."); } }
        public static XPlaneCommand LightsGeneric18LightTog { get { return GetXPlaneCommand("sim/lights/generic_18_light_tog", "Generic light 18 toggle."); } }
        public static XPlaneCommand LightsGeneric19LightTog { get { return GetXPlaneCommand("sim/lights/generic_19_light_tog", "Generic light 19 toggle."); } }
        public static XPlaneCommand LightsGeneric20LightTog { get { return GetXPlaneCommand("sim/lights/generic_20_light_tog", "Generic light 20 toggle."); } }
        public static XPlaneCommand LightsGeneric21LightTog { get { return GetXPlaneCommand("sim/lights/generic_21_light_tog", "Generic light 21 toggle."); } }
        public static XPlaneCommand LightsGeneric22LightTog { get { return GetXPlaneCommand("sim/lights/generic_22_light_tog", "Generic light 22 toggle."); } }
        public static XPlaneCommand LightsGeneric23LightTog { get { return GetXPlaneCommand("sim/lights/generic_23_light_tog", "Generic light 23 toggle."); } }
        public static XPlaneCommand LightsGeneric24LightTog { get { return GetXPlaneCommand("sim/lights/generic_24_light_tog", "Generic light 24 toggle."); } }
        public static XPlaneCommand LightsGeneric25LightTog { get { return GetXPlaneCommand("sim/lights/generic_25_light_tog", "Generic light 25 toggle."); } }
        public static XPlaneCommand LightsGeneric26LightTog { get { return GetXPlaneCommand("sim/lights/generic_26_light_tog", "Generic light 26 toggle."); } }
        public static XPlaneCommand LightsGeneric27LightTog { get { return GetXPlaneCommand("sim/lights/generic_27_light_tog", "Generic light 27 toggle."); } }
        public static XPlaneCommand LightsGeneric28LightTog { get { return GetXPlaneCommand("sim/lights/generic_28_light_tog", "Generic light 28 toggle."); } }
        public static XPlaneCommand LightsGeneric29LightTog { get { return GetXPlaneCommand("sim/lights/generic_29_light_tog", "Generic light 29 toggle."); } }
        public static XPlaneCommand LightsGeneric30LightTog { get { return GetXPlaneCommand("sim/lights/generic_30_light_tog", "Generic light 30 toggle."); } }
        public static XPlaneCommand LightsGeneric31LightTog { get { return GetXPlaneCommand("sim/lights/generic_31_light_tog", "Generic light 31 toggle."); } }
        public static XPlaneCommand LightsGeneric32LightTog { get { return GetXPlaneCommand("sim/lights/generic_32_light_tog", "Generic light 32 toggle."); } }
        public static XPlaneCommand LightsGeneric33LightTog { get { return GetXPlaneCommand("sim/lights/generic_33_light_tog", "Generic light 33 toggle."); } }
        public static XPlaneCommand LightsGeneric34LightTog { get { return GetXPlaneCommand("sim/lights/generic_34_light_tog", "Generic light 34 toggle."); } }
        public static XPlaneCommand LightsGeneric35LightTog { get { return GetXPlaneCommand("sim/lights/generic_35_light_tog", "Generic light 35 toggle."); } }
        public static XPlaneCommand LightsGeneric36LightTog { get { return GetXPlaneCommand("sim/lights/generic_36_light_tog", "Generic light 36 toggle."); } }
        public static XPlaneCommand LightsGeneric37LightTog { get { return GetXPlaneCommand("sim/lights/generic_37_light_tog", "Generic light 37 toggle."); } }
        public static XPlaneCommand LightsGeneric38LightTog { get { return GetXPlaneCommand("sim/lights/generic_38_light_tog", "Generic light 38 toggle."); } }
        public static XPlaneCommand LightsGeneric39LightTog { get { return GetXPlaneCommand("sim/lights/generic_39_light_tog", "Generic light 39 toggle."); } }
        public static XPlaneCommand LightsGeneric40LightTog { get { return GetXPlaneCommand("sim/lights/generic_40_light_tog", "Generic light 40 toggle."); } }
        public static XPlaneCommand LightsGeneric41LightTog { get { return GetXPlaneCommand("sim/lights/generic_41_light_tog", "Generic light 41 toggle."); } }
        public static XPlaneCommand LightsGeneric42LightTog { get { return GetXPlaneCommand("sim/lights/generic_42_light_tog", "Generic light 42 toggle."); } }
        public static XPlaneCommand LightsGeneric43LightTog { get { return GetXPlaneCommand("sim/lights/generic_43_light_tog", "Generic light 43 toggle."); } }
        public static XPlaneCommand LightsGeneric44LightTog { get { return GetXPlaneCommand("sim/lights/generic_44_light_tog", "Generic light 44 toggle."); } }
        public static XPlaneCommand LightsGeneric45LightTog { get { return GetXPlaneCommand("sim/lights/generic_45_light_tog", "Generic light 45 toggle."); } }
        public static XPlaneCommand LightsGeneric46LightTog { get { return GetXPlaneCommand("sim/lights/generic_46_light_tog", "Generic light 46 toggle."); } }
        public static XPlaneCommand LightsGeneric47LightTog { get { return GetXPlaneCommand("sim/lights/generic_47_light_tog", "Generic light 47 toggle."); } }
        public static XPlaneCommand LightsGeneric48LightTog { get { return GetXPlaneCommand("sim/lights/generic_48_light_tog", "Generic light 48 toggle."); } }
        public static XPlaneCommand LightsGeneric49LightTog { get { return GetXPlaneCommand("sim/lights/generic_49_light_tog", "Generic light 49 toggle."); } }
        public static XPlaneCommand LightsGeneric50LightTog { get { return GetXPlaneCommand("sim/lights/generic_50_light_tog", "Generic light 50 toggle."); } }
        public static XPlaneCommand LightsGeneric51LightTog { get { return GetXPlaneCommand("sim/lights/generic_51_light_tog", "Generic light 51 toggle."); } }
        public static XPlaneCommand LightsGeneric52LightTog { get { return GetXPlaneCommand("sim/lights/generic_52_light_tog", "Generic light 52 toggle."); } }
        public static XPlaneCommand LightsGeneric53LightTog { get { return GetXPlaneCommand("sim/lights/generic_53_light_tog", "Generic light 53 toggle."); } }
        public static XPlaneCommand LightsGeneric54LightTog { get { return GetXPlaneCommand("sim/lights/generic_54_light_tog", "Generic light 54 toggle."); } }
        public static XPlaneCommand LightsGeneric55LightTog { get { return GetXPlaneCommand("sim/lights/generic_55_light_tog", "Generic light 55 toggle."); } }
        public static XPlaneCommand LightsGeneric56LightTog { get { return GetXPlaneCommand("sim/lights/generic_56_light_tog", "Generic light 56 toggle."); } }
        public static XPlaneCommand LightsGeneric57LightTog { get { return GetXPlaneCommand("sim/lights/generic_57_light_tog", "Generic light 57 toggle."); } }
        public static XPlaneCommand LightsGeneric58LightTog { get { return GetXPlaneCommand("sim/lights/generic_58_light_tog", "Generic light 58 toggle."); } }
        public static XPlaneCommand LightsGeneric59LightTog { get { return GetXPlaneCommand("sim/lights/generic_59_light_tog", "Generic light 59 toggle."); } }
        public static XPlaneCommand LightsGeneric60LightTog { get { return GetXPlaneCommand("sim/lights/generic_60_light_tog", "Generic light 60 toggle."); } }
        public static XPlaneCommand LightsGeneric61LightTog { get { return GetXPlaneCommand("sim/lights/generic_61_light_tog", "Generic light 61 toggle."); } }
        public static XPlaneCommand LightsGeneric62LightTog { get { return GetXPlaneCommand("sim/lights/generic_62_light_tog", "Generic light 62 toggle."); } }
        public static XPlaneCommand LightsGeneric63LightTog { get { return GetXPlaneCommand("sim/lights/generic_63_light_tog", "Generic light 63 toggle."); } }
        public static XPlaneCommand LightsGeneric64LightTog { get { return GetXPlaneCommand("sim/lights/generic_64_light_tog", "Generic light 64 toggle."); } }
        public static XPlaneCommand LightsSpotLightsOn { get { return GetXPlaneCommand("sim/lights/spot_lights_on", "Spot lights on."); } }
        public static XPlaneCommand LightsSpotLightsOff { get { return GetXPlaneCommand("sim/lights/spot_lights_off", "Spot lights off."); } }
        public static XPlaneCommand LightsSpotLightsToggle { get { return GetXPlaneCommand("sim/lights/spot_lights_toggle", "Spot lights toggle."); } }
        public static XPlaneCommand LightsSpotLightLeft { get { return GetXPlaneCommand("sim/lights/spot_light_left", "Aim spotlight left."); } }
        public static XPlaneCommand LightsSpotLightRight { get { return GetXPlaneCommand("sim/lights/spot_light_right", "Aim spotlight right."); } }
        public static XPlaneCommand LightsSpotLightUp { get { return GetXPlaneCommand("sim/lights/spot_light_up", "Aim spotlight up."); } }
        public static XPlaneCommand LightsSpotLightDown { get { return GetXPlaneCommand("sim/lights/spot_light_down", "Aim spotlight down."); } }
        public static XPlaneCommand LightsSpotLightCenter { get { return GetXPlaneCommand("sim/lights/spot_light_center", "Aim spotlight to center."); } }
    }
}