﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand FadecFadecToggle { get { return GetXPlaneCommand("sim/fadec/fadec_toggle", "FADEC toggle."); } }
        public static XPlaneCommand FadecFadec1Off { get { return GetXPlaneCommand("sim/fadec/fadec_1_off", "FADEC #1 off."); } }
        public static XPlaneCommand FadecFadec2Off { get { return GetXPlaneCommand("sim/fadec/fadec_2_off", "FADEC #2 off."); } }
        public static XPlaneCommand FadecFadec3Off { get { return GetXPlaneCommand("sim/fadec/fadec_3_off", "FADEC #3 off."); } }
        public static XPlaneCommand FadecFadec4Off { get { return GetXPlaneCommand("sim/fadec/fadec_4_off", "FADEC #4 off."); } }
        public static XPlaneCommand FadecFadec5Off { get { return GetXPlaneCommand("sim/fadec/fadec_5_off", "FADEC #5 off."); } }
        public static XPlaneCommand FadecFadec6Off { get { return GetXPlaneCommand("sim/fadec/fadec_6_off", "FADEC #6 off."); } }
        public static XPlaneCommand FadecFadec7Off { get { return GetXPlaneCommand("sim/fadec/fadec_7_off", "FADEC #7 off."); } }
        public static XPlaneCommand FadecFadec8Off { get { return GetXPlaneCommand("sim/fadec/fadec_8_off", "FADEC #8 off."); } }
        public static XPlaneCommand FadecFadec1On { get { return GetXPlaneCommand("sim/fadec/fadec_1_on", "FADEC #1 on."); } }
        public static XPlaneCommand FadecFadec2On { get { return GetXPlaneCommand("sim/fadec/fadec_2_on", "FADEC #2 on."); } }
        public static XPlaneCommand FadecFadec3On { get { return GetXPlaneCommand("sim/fadec/fadec_3_on", "FADEC #3 on."); } }
        public static XPlaneCommand FadecFadec4On { get { return GetXPlaneCommand("sim/fadec/fadec_4_on", "FADEC #4 on."); } }
        public static XPlaneCommand FadecFadec5On { get { return GetXPlaneCommand("sim/fadec/fadec_5_on", "FADEC #5 on."); } }
        public static XPlaneCommand FadecFadec6On { get { return GetXPlaneCommand("sim/fadec/fadec_6_on", "FADEC #6 on."); } }
        public static XPlaneCommand FadecFadec7On { get { return GetXPlaneCommand("sim/fadec/fadec_7_on", "FADEC #7 on."); } }
        public static XPlaneCommand FadecFadec8On { get { return GetXPlaneCommand("sim/fadec/fadec_8_on", "FADEC #8 on."); } }
    }
}