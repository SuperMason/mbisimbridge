﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand AutopilotHsiSelectDown { get { return GetXPlaneCommand("sim/autopilot/hsi_select_down", "HSI select down one."); } }
        public static XPlaneCommand AutopilotHsiSelectUp { get { return GetXPlaneCommand("sim/autopilot/hsi_select_up", "HSI select up one."); } }
        public static XPlaneCommand AutopilotHsiSelectNav1 { get { return GetXPlaneCommand("sim/autopilot/hsi_select_nav_1", "HSI shows NAV 1."); } }
        public static XPlaneCommand AutopilotHsiSelectNav2 { get { return GetXPlaneCommand("sim/autopilot/hsi_select_nav_2", "HSI shows NAV 2."); } }
        public static XPlaneCommand AutopilotHsiSelectGps { get { return GetXPlaneCommand("sim/autopilot/hsi_select_gps", "HSI shows GPS."); } }
        public static XPlaneCommand AutopilotHsiSelectCopilotDown { get { return GetXPlaneCommand("sim/autopilot/hsi_select_copilot_down", "HSI select down one, copilot."); } }
        public static XPlaneCommand AutopilotHsiSelectCopilotUp { get { return GetXPlaneCommand("sim/autopilot/hsi_select_copilot_up", "HSI select up one, copilot."); } }
        public static XPlaneCommand AutopilotHsiSelectCopilotNav1 { get { return GetXPlaneCommand("sim/autopilot/hsi_select_copilot_nav_1", "HSI shows NAV 1, copilot."); } }
        public static XPlaneCommand AutopilotHsiSelectCopilotNav2 { get { return GetXPlaneCommand("sim/autopilot/hsi_select_copilot_nav_2", "HSI shows NAV 2, copilot."); } }
        public static XPlaneCommand AutopilotHsiSelectCopilotGps { get { return GetXPlaneCommand("sim/autopilot/hsi_select_copilot_gps", "HSI shows GPS, copilot."); } }
        public static XPlaneCommand AutopilotSource01 { get { return GetXPlaneCommand("sim/autopilot/source_01", "Change autopilot instrument source, 0 or 1"); } }
        public static XPlaneCommand AutopilotFdirOn { get { return GetXPlaneCommand("sim/autopilot/fdir_on", "Flight director on."); } }
        public static XPlaneCommand AutopilotFdirToggle { get { return GetXPlaneCommand("sim/autopilot/fdir_toggle", "Flight director toggle."); } }
        public static XPlaneCommand AutopilotServosOn { get { return GetXPlaneCommand("sim/autopilot/servos_on", "Servos on."); } }
        public static XPlaneCommand AutopilotServosToggle { get { return GetXPlaneCommand("sim/autopilot/servos_toggle", "Servos toggle."); } }
        public static XPlaneCommand AutopilotFdirServosDownOne { get { return GetXPlaneCommand("sim/autopilot/fdir_servos_down_one", "Flight director down (on/FDIR/off)."); } }
        public static XPlaneCommand AutopilotFdirServosUpOne { get { return GetXPlaneCommand("sim/autopilot/fdir_servos_up_one", "Flight director up (off/FDIR/on)."); } }
        public static XPlaneCommand AutopilotServosFdirOff { get { return GetXPlaneCommand("sim/autopilot/servos_fdir_off", "Disco servos, flt dir."); } }
        public static XPlaneCommand AutopilotServosFdirYawdOff { get { return GetXPlaneCommand("sim/autopilot/servos_fdir_yawd_off", "Disco servos, flt dir, yaw damp."); } }
        public static XPlaneCommand AutopilotServosFdirYawdTrimOff { get { return GetXPlaneCommand("sim/autopilot/servos_fdir_yawd_trim_off", "Disco servos, flt dir, yaw damp, trim."); } }
        public static XPlaneCommand AutopilotControlWheelSteer { get { return GetXPlaneCommand("sim/autopilot/control_wheel_steer", "Control wheel steering mode."); } }
        public static XPlaneCommand AutopilotFdir2On { get { return GetXPlaneCommand("sim/autopilot/fdir2_on", "Flight director 2 on."); } }
        public static XPlaneCommand AutopilotFdir2Toggle { get { return GetXPlaneCommand("sim/autopilot/fdir2_toggle", "Flight director 2 toggle."); } }
        public static XPlaneCommand AutopilotServos2On { get { return GetXPlaneCommand("sim/autopilot/servos2_on", "Servos 2 on."); } }
        public static XPlaneCommand AutopilotServos2Toggle { get { return GetXPlaneCommand("sim/autopilot/servos2_toggle", "Servos 2 toggle."); } }
        public static XPlaneCommand AutopilotFdir2ServosDownOne { get { return GetXPlaneCommand("sim/autopilot/fdir2_servos_down_one", "Flight director 2 down (on/FDIR/off)."); } }
        public static XPlaneCommand AutopilotFdir2ServosUpOne { get { return GetXPlaneCommand("sim/autopilot/fdir2_servos_up_one", "Flight director 2 up (off/FDIR/on)."); } }
        public static XPlaneCommand AutopilotServosFdir2Off { get { return GetXPlaneCommand("sim/autopilot/servos_fdir2_off", "Disco servos 2, flt dir 2."); } }
        public static XPlaneCommand AutopilotCWSA { get { return GetXPlaneCommand("sim/autopilot/CWSA", "CWS A - Control wheel steering mode AP A."); } }
        public static XPlaneCommand AutopilotCWSB { get { return GetXPlaneCommand("sim/autopilot/CWSB", "CWS B - Control wheel steering mode AP B."); } }
        public static XPlaneCommand AutopilotServos3On { get { return GetXPlaneCommand("sim/autopilot/servos3_on", "Servos 3 on."); } }
        public static XPlaneCommand AutopilotServos3Toggle { get { return GetXPlaneCommand("sim/autopilot/servos3_toggle", "Servos 3 toggle."); } }
        public static XPlaneCommand AutopilotServosFdir3Off { get { return GetXPlaneCommand("sim/autopilot/servos_fdir3_off", "Disco servos 3."); } }
        public static XPlaneCommand AutopilotServosOffAny { get { return GetXPlaneCommand("sim/autopilot/servos_off_any", "Disco servos, which ever side is active."); } }
        public static XPlaneCommand AutopilotServosYawdOffAny { get { return GetXPlaneCommand("sim/autopilot/servos_yawd_off_any", "Disco servos and yaw damp, any side."); } }
        public static XPlaneCommand AutopilotServosYawdTrimOffAny { get { return GetXPlaneCommand("sim/autopilot/servos_yawd_trim_off_any", "Disco servos, yaw damp and trim any side."); } }
        public static XPlaneCommand AutopilotAutothrottleOn { get { return GetXPlaneCommand("sim/autopilot/autothrottle_on", "Autopilot auto-throttle speed-hold on."); } }
        public static XPlaneCommand AutopilotAutothrottleOff { get { return GetXPlaneCommand("sim/autopilot/autothrottle_off", "Autopilot auto-throttle all modes off."); } }
        public static XPlaneCommand AutopilotAutothrottleToggle { get { return GetXPlaneCommand("sim/autopilot/autothrottle_toggle", "Autopilot auto-throttle speed toggle."); } }
        public static XPlaneCommand AutopilotAutothrottleN1epr { get { return GetXPlaneCommand("sim/autopilot/autothrottle_n1epr", "Autopilot auto-throttle EPR/N1 hold on."); } }
        public static XPlaneCommand AutopilotAutothrottleN1eprToggle { get { return GetXPlaneCommand("sim/autopilot/autothrottle_n1epr_toggle", "Autopilot auto-throttle EPR/N1 hold toggle."); } }
        public static XPlaneCommand AutopilotHeading { get { return GetXPlaneCommand("sim/autopilot/heading", "Autopilot heading-select."); } }
        public static XPlaneCommand AutopilotTrack { get { return GetXPlaneCommand("sim/autopilot/track", "Autopilot track."); } }
        public static XPlaneCommand AutopilotHeadingHold { get { return GetXPlaneCommand("sim/autopilot/heading_hold", "Autopilot heading-hold."); } }
        public static XPlaneCommand AutopilotWingLeveler { get { return GetXPlaneCommand("sim/autopilot/wing_leveler", "Autopilot wing-level / roll hold."); } }
        public static XPlaneCommand AutopilotRateHold { get { return GetXPlaneCommand("sim/autopilot/rate_hold", "Autopilot wing-level / rate hold."); } }
        public static XPlaneCommand AutopilotHdgNav { get { return GetXPlaneCommand("sim/autopilot/hdg_nav", "Autopilot heading select and NAV arm."); } }
        public static XPlaneCommand AutopilotNAV { get { return GetXPlaneCommand("sim/autopilot/NAV", "Autopilot VOR/LOC arm."); } }
        public static XPlaneCommand AutopilotVerticalSpeed { get { return GetXPlaneCommand("sim/autopilot/vertical_speed", "Autopilot vertical speed, at current VSI."); } }
        public static XPlaneCommand AutopilotFpa { get { return GetXPlaneCommand("sim/autopilot/fpa", "Autopilot flight path angle, current FPA."); } }
        public static XPlaneCommand AutopilotAltVs { get { return GetXPlaneCommand("sim/autopilot/alt_vs", "Autopilot vertical speed, at current VSI, arm ALT."); } }
        public static XPlaneCommand AutopilotVerticalSpeedPreSel { get { return GetXPlaneCommand("sim/autopilot/vertical_speed_pre_sel", "Autopilot vertical speed, at pre-sel VSI."); } }
        public static XPlaneCommand AutopilotPitchSync { get { return GetXPlaneCommand("sim/autopilot/pitch_sync", "Autopilot pitch-sync."); } }
        public static XPlaneCommand AutopilotLevelChange { get { return GetXPlaneCommand("sim/autopilot/level_change", "Autopilot level change."); } }
        public static XPlaneCommand AutopilotAltitudeHold { get { return GetXPlaneCommand("sim/autopilot/altitude_hold", "Autopilot altitude select or hold."); } }
        public static XPlaneCommand AutopilotTerrainFollowing { get { return GetXPlaneCommand("sim/autopilot/terrain_following", "Autopilot terrain-mode following."); } }
        public static XPlaneCommand AutopilotTakeOffGoAround { get { return GetXPlaneCommand("sim/autopilot/take_off_go_around", "Autopilot take-off go-around."); } }
        public static XPlaneCommand AutopilotReentry { get { return GetXPlaneCommand("sim/autopilot/reentry", "Autopilot re-entry."); } }
        public static XPlaneCommand AutopilotGlideSlope { get { return GetXPlaneCommand("sim/autopilot/glide_slope", "Autopilot glideslope."); } }
        public static XPlaneCommand AutopilotVnav { get { return GetXPlaneCommand("sim/autopilot/vnav", "Autopilot VNAV for G1000."); } }
        public static XPlaneCommand AutopilotGpss { get { return GetXPlaneCommand("sim/autopilot/gpss", "Autopilot GPS Steering."); } }
        public static XPlaneCommand AutopilotClimb { get { return GetXPlaneCommand("sim/autopilot/climb", "Autopilot GPS Climb."); } }
        public static XPlaneCommand AutopilotDescend { get { return GetXPlaneCommand("sim/autopilot/descend", "Autopilot GPS Descend."); } }
        public static XPlaneCommand AutopilotTrkfpa { get { return GetXPlaneCommand("sim/autopilot/trkfpa", "Autopilot TRK/FPA vs HDG/VS toggle."); } }
        public static XPlaneCommand AutopilotAirspeedSync { get { return GetXPlaneCommand("sim/autopilot/airspeed_sync", "Autopilot airspeed sync."); } }
        public static XPlaneCommand AutopilotHeadingSync { get { return GetXPlaneCommand("sim/autopilot/heading_sync", "Autopilot heading sync."); } }
        public static XPlaneCommand AutopilotVerticalSpeedSync { get { return GetXPlaneCommand("sim/autopilot/vertical_speed_sync", "Autopilot VVI sync."); } }
        public static XPlaneCommand AutopilotAltitudeSync { get { return GetXPlaneCommand("sim/autopilot/altitude_sync", "Autopilot altitude sync."); } }
        public static XPlaneCommand AutopilotHeadingDown { get { return GetXPlaneCommand("sim/autopilot/heading_down", "Autopilot heading down."); } }
        public static XPlaneCommand AutopilotHeadingUp { get { return GetXPlaneCommand("sim/autopilot/heading_up", "Autopilot heading up."); } }
        public static XPlaneCommand AutopilotHeadingCopilotDown { get { return GetXPlaneCommand("sim/autopilot/heading_copilot_down", "Autopilot heading copilot down."); } }
        public static XPlaneCommand AutopilotHeadingCopilotUp { get { return GetXPlaneCommand("sim/autopilot/heading_copilot_up", "Autopilot heading copilot up."); } }
        public static XPlaneCommand AutopilotAirspeedDown { get { return GetXPlaneCommand("sim/autopilot/airspeed_down", "Autopilot airspeed down."); } }
        public static XPlaneCommand AutopilotAirspeedUp { get { return GetXPlaneCommand("sim/autopilot/airspeed_up", "Autopilot airspeed up."); } }
        public static XPlaneCommand AutopilotVerticalSpeedDown { get { return GetXPlaneCommand("sim/autopilot/vertical_speed_down", "Autopilot VVI down."); } }
        public static XPlaneCommand AutopilotVerticalSpeedUp { get { return GetXPlaneCommand("sim/autopilot/vertical_speed_up", "Autopilot VVI up."); } }
        public static XPlaneCommand AutopilotAltitudeDown { get { return GetXPlaneCommand("sim/autopilot/altitude_down", "Autopilot altitude down."); } }
        public static XPlaneCommand AutopilotAltitudeUp { get { return GetXPlaneCommand("sim/autopilot/altitude_up", "Autopilot altitude up."); } }
        public static XPlaneCommand AutopilotNoseDown { get { return GetXPlaneCommand("sim/autopilot/nose_down", "Autopilot nose down."); } }
        public static XPlaneCommand AutopilotNoseUp { get { return GetXPlaneCommand("sim/autopilot/nose_up", "Autopilot nose up."); } }
        public static XPlaneCommand AutopilotNoseDownPitchMode { get { return GetXPlaneCommand("sim/autopilot/nose_down_pitch_mode", "Autopilot nose down, go into pitch mode."); } }
        public static XPlaneCommand AutopilotNoseUpPitchMode { get { return GetXPlaneCommand("sim/autopilot/nose_up_pitch_mode", "Autopilot nose up, go into pitch mode"); } }
        public static XPlaneCommand AutopilotOverrideLeft { get { return GetXPlaneCommand("sim/autopilot/override_left", "Autopilot override left: Go to ROL mode."); } }
        public static XPlaneCommand AutopilotOverrideRight { get { return GetXPlaneCommand("sim/autopilot/override_right", "Autopilot override right: Go to ROL mode."); } }
        public static XPlaneCommand AutopilotOverrideCenter { get { return GetXPlaneCommand("sim/autopilot/override_center", "Autopilot override center: Go to ROL mode, 0 turn rate roll angle."); } }
        public static XPlaneCommand AutopilotOverrideUp { get { return GetXPlaneCommand("sim/autopilot/override_up", "Autopilot override up: Go to SYN mode."); } }
        public static XPlaneCommand AutopilotOverrideDown { get { return GetXPlaneCommand("sim/autopilot/override_down", "Autopilot override down: Go to SYN mode."); } }
        public static XPlaneCommand AutopilotAltitudeArm { get { return GetXPlaneCommand("sim/autopilot/altitude_arm", "Autopilot altitude hold ARM."); } }
        public static XPlaneCommand AutopilotApproach { get { return GetXPlaneCommand("sim/autopilot/approach", "Autopilot approach."); } }
        public static XPlaneCommand AutopilotBackCourse { get { return GetXPlaneCommand("sim/autopilot/back_course", "Autopilot back-course."); } }
        public static XPlaneCommand AutopilotKnotsMachToggle { get { return GetXPlaneCommand("sim/autopilot/knots_mach_toggle", "Toggle knots-Mach airspeeed hold."); } }
        public static XPlaneCommand AutopilotFMS { get { return GetXPlaneCommand("sim/autopilot/FMS", "Autopilot FMS altitude."); } }
        public static XPlaneCommand AutopilotBankLimitDown { get { return GetXPlaneCommand("sim/autopilot/bank_limit_down", "Bank angle limit down."); } }
        public static XPlaneCommand AutopilotBankLimitUp { get { return GetXPlaneCommand("sim/autopilot/bank_limit_up", "Bank angle limit up."); } }
        public static XPlaneCommand AutopilotBankLimitToggle { get { return GetXPlaneCommand("sim/autopilot/bank_limit_toggle", "Bank angle limit toggle."); } }
        public static XPlaneCommand AutopilotSoftRideToggle { get { return GetXPlaneCommand("sim/autopilot/soft_ride_toggle", "Soft ride toggle."); } }
        public static XPlaneCommand AutopilotTestAutoAnnunciators { get { return GetXPlaneCommand("sim/autopilot/test_auto_annunciators", "Autopilot test annunciators."); } }
        public static XPlaneCommand AutopilotSetOttSeldispALTVVIVvi { get { return GetXPlaneCommand("sim/autopilot/set_ott_seldisp_ALT_VVI_vvi", "Let ott_seldisp_ALT_VVI show VVI."); } }
        public static XPlaneCommand AutopilotSetOttSeldispALTVVIAlt { get { return GetXPlaneCommand("sim/autopilot/set_ott_seldisp_ALT_VVI_alt", "Let ott_seldisp_ALT_VVI show ALT."); } }
    }
}