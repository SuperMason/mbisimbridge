﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand AltairAlternateAirOff1 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_1", "Alternate air #1 off."); } }
        public static XPlaneCommand AltairAlternateAirOff2 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_2", "Alternate air #2 off."); } }
        public static XPlaneCommand AltairAlternateAirOff3 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_3", "Alternate air #3 off."); } }
        public static XPlaneCommand AltairAlternateAirOff4 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_4", "Alternate air #4 off."); } }
        public static XPlaneCommand AltairAlternateAirOff5 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_5", "Alternate air #5 off."); } }
        public static XPlaneCommand AltairAlternateAirOff6 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_6", "Alternate air #6 off."); } }
        public static XPlaneCommand AltairAlternateAirOff7 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_7", "Alternate air #7 off."); } }
        public static XPlaneCommand AltairAlternateAirOff8 { get { return GetXPlaneCommand("sim/altair/alternate_air_off_8", "Alternate air #8 off."); } }
        public static XPlaneCommand AltairAlternateAirOn1 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_1", "Alternate air #1 on."); } }
        public static XPlaneCommand AltairAlternateAirOn2 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_2", "Alternate air #2 on."); } }
        public static XPlaneCommand AltairAlternateAirOn3 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_3", "Alternate air #3 on."); } }
        public static XPlaneCommand AltairAlternateAirOn4 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_4", "Alternate air #4 on."); } }
        public static XPlaneCommand AltairAlternateAirOn5 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_5", "Alternate air #5 on."); } }
        public static XPlaneCommand AltairAlternateAirOn6 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_6", "Alternate air #6 on."); } }
        public static XPlaneCommand AltairAlternateAirOn7 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_7", "Alternate air #7 on."); } }
        public static XPlaneCommand AltairAlternateAirOn8 { get { return GetXPlaneCommand("sim/altair/alternate_air_on_8", "Alternate air #8 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff1 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_1", "Alternate air backup #1 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff2 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_2", "Alternate air backup #2 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff3 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_3", "Alternate air backup #3 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff4 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_4", "Alternate air backup #4 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff5 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_5", "Alternate air backup #5 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff6 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_6", "Alternate air backup #6 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff7 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_7", "Alternate air backup #7 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOff8 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_off_8", "Alternate air backup #8 off."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn1 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_1", "Alternate air backup #1 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn2 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_2", "Alternate air backup #2 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn3 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_3", "Alternate air backup #3 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn4 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_4", "Alternate air backup #4 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn5 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_5", "Alternate air backup #5 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn6 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_6", "Alternate air backup #6 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn7 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_7", "Alternate air backup #7 on."); } }
        public static XPlaneCommand AltairAlternateAirBackupOn8 { get { return GetXPlaneCommand("sim/altair/alternate_air_backup_on_8", "Alternate air backup #8 on."); } }
    }
}