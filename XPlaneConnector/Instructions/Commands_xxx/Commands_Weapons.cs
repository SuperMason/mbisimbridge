﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand WeaponsReArmAircraft { get { return GetXPlaneCommand("sim/weapons/re_arm_aircraft", "Re-arm aircraft to default specs."); } }
        public static XPlaneCommand WeaponsMasterArmOn { get { return GetXPlaneCommand("sim/weapons/master_arm_on", "Master arm on."); } }
        public static XPlaneCommand WeaponsMasterArmOff { get { return GetXPlaneCommand("sim/weapons/master_arm_off", "Master arm off."); } }
        public static XPlaneCommand WeaponsFireGun { get { return GetXPlaneCommand("sim/weapons/fire_guns", "Fire guns."); } }
        public static XPlaneCommand WeaponsFireModeDown { get { return GetXPlaneCommand("sim/weapons/fire_mode_down", "Weapon single/pair/ripple/salvo down."); } }
        public static XPlaneCommand WeaponsFireModeUp { get { return GetXPlaneCommand("sim/weapons/fire_mode_up", "Weapon single/pair/ripple/salvo up."); } }
        public static XPlaneCommand WeaponsFireRateDown { get { return GetXPlaneCommand("sim/weapons/fire_rate_down", "Weapon fire rate down."); } }
        public static XPlaneCommand WeaponsFireRateUp { get { return GetXPlaneCommand("sim/weapons/fire_rate_up", "Weapon fire rate up."); } }
        public static XPlaneCommand WeaponsWeaponSelectDown { get { return GetXPlaneCommand("sim/weapons/weapon_select_down", "Weapon select down."); } }
        public static XPlaneCommand WeaponsWeaponSelectUp { get { return GetXPlaneCommand("sim/weapons/weapon_select_up", "Weapon select up."); } }
        public static XPlaneCommand WeaponsFireGuns { get { return GetXPlaneCommand("sim/weapons/fire_guns", "Fire guns."); } }
        public static XPlaneCommand WeaponsFireAirToAir { get { return GetXPlaneCommand("sim/weapons/fire_air_to_air", "Fire air-to-air selection."); } }
        public static XPlaneCommand WeaponsFireAirToGround { get { return GetXPlaneCommand("sim/weapons/fire_air_to_ground", "Fire air-to-ground selection."); } }
        public static XPlaneCommand WeaponsFireAnyArmed { get { return GetXPlaneCommand("sim/weapons/fire_any_armed", "Fire all armed selections."); } }
        public static XPlaneCommand WeaponsFireAnyShell { get { return GetXPlaneCommand("sim/weapons/fire_any_shell", "Fire selected weapon."); } }
        public static XPlaneCommand WeaponsGPSLockHere { get { return GetXPlaneCommand("sim/weapons/GPS_lock_here", "Target camera pointer in GPS."); } }
        public static XPlaneCommand WeaponsWeaponTargetDown { get { return GetXPlaneCommand("sim/weapons/weapon_target_down", "Target select down."); } }
        public static XPlaneCommand WeaponsWeaponTargetUp { get { return GetXPlaneCommand("sim/weapons/weapon_target_up", "Target select up."); } }
        public static XPlaneCommand WeaponsDeployChaff { get { return GetXPlaneCommand("sim/weapons/deploy_chaff", "Deploy chaff."); } }
        public static XPlaneCommand WeaponsDeployFlares { get { return GetXPlaneCommand("sim/weapons/deploy_flares", "Deploy flares."); } }
    }
}