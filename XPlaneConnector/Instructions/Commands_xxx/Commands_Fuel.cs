﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand FuelFuelTankSelectorLftOne { get { return GetXPlaneCommand("sim/fuel/fuel_tank_selector_lft_one", "Select fuel tank left one."); } }
        public static XPlaneCommand FuelFuelTankSelectorRgtOne { get { return GetXPlaneCommand("sim/fuel/fuel_tank_selector_rgt_one", "Select fuel tank right one."); } }
        public static XPlaneCommand FuelFuelTankPump1On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_1_on", "Fuel pump for tank #1 on."); } }
        public static XPlaneCommand FuelFuelTankPump2On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_2_on", "Fuel pump for tank #2 on."); } }
        public static XPlaneCommand FuelFuelTankPump3On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_3_on", "Fuel pump for tank #3 on."); } }
        public static XPlaneCommand FuelFuelTankPump4On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_4_on", "Fuel pump for tank #4 on."); } }
        public static XPlaneCommand FuelFuelTankPump5On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_5_on", "Fuel pump for tank #5 on."); } }
        public static XPlaneCommand FuelFuelTankPump6On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_6_on", "Fuel pump for tank #6 on."); } }
        public static XPlaneCommand FuelFuelTankPump7On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_7_on", "Fuel pump for tank #7 on."); } }
        public static XPlaneCommand FuelFuelTankPump8On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_8_on", "Fuel pump for tank #8 on."); } }
        public static XPlaneCommand FuelFuelTankPump9On { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_9_on", "Fuel pump for tank #9 on."); } }
        public static XPlaneCommand FuelFuelTankPump1Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_1_off", "Fuel pump for tank #1 off."); } }
        public static XPlaneCommand FuelFuelTankPump2Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_2_off", "Fuel pump for tank #2 off."); } }
        public static XPlaneCommand FuelFuelTankPump3Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_3_off", "Fuel pump for tank #3 off."); } }
        public static XPlaneCommand FuelFuelTankPump4Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_4_off", "Fuel pump for tank #4 off."); } }
        public static XPlaneCommand FuelFuelTankPump5Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_5_off", "Fuel pump for tank #5 off."); } }
        public static XPlaneCommand FuelFuelTankPump6Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_6_off", "Fuel pump for tank #6 off."); } }
        public static XPlaneCommand FuelFuelTankPump7Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_7_off", "Fuel pump for tank #7 off."); } }
        public static XPlaneCommand FuelFuelTankPump8Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_8_off", "Fuel pump for tank #8 off."); } }
        public static XPlaneCommand FuelFuelTankPump9Off { get { return GetXPlaneCommand("sim/fuel/fuel_tank_pump_9_off", "Fuel pump for tank #9 off."); } }
        public static XPlaneCommand FuelFuelSelectorNone { get { return GetXPlaneCommand("sim/fuel/fuel_selector_none", "Set fuel selector to none (shut off)."); } }
        public static XPlaneCommand FuelFuelSelectorLft { get { return GetXPlaneCommand("sim/fuel/fuel_selector_lft", "Set fuel selector to left tanks."); } }
        public static XPlaneCommand FuelFuelSelectorCtr { get { return GetXPlaneCommand("sim/fuel/fuel_selector_ctr", "Set fuel selector to center tanks."); } }
        public static XPlaneCommand FuelFuelSelectorRgt { get { return GetXPlaneCommand("sim/fuel/fuel_selector_rgt", "Set fuel selector to right tanks."); } }
        public static XPlaneCommand FuelFuelSelectorAll { get { return GetXPlaneCommand("sim/fuel/fuel_selector_all", "Set fuel selector to all tanks."); } }
        public static XPlaneCommand FuelLeftFuelSelectorNone { get { return GetXPlaneCommand("sim/fuel/left_fuel_selector_none", "Set fuel selector for left engine to none (shut off)."); } }
        public static XPlaneCommand FuelLeftFuelSelectorLft { get { return GetXPlaneCommand("sim/fuel/left_fuel_selector_lft", "Set fuel selector for left engine to left tanks."); } }
        public static XPlaneCommand FuelLeftFuelSelectorCtr { get { return GetXPlaneCommand("sim/fuel/left_fuel_selector_ctr", "Set fuel selector for left engine to center tanks."); } }
        public static XPlaneCommand FuelLeftFuelSelectorRgt { get { return GetXPlaneCommand("sim/fuel/left_fuel_selector_rgt", "Set fuel selector for left engine to right tanks."); } }
        public static XPlaneCommand FuelLeftFuelSelectorAll { get { return GetXPlaneCommand("sim/fuel/left_fuel_selector_all", "Set fuel selector for left engine to all tanks."); } }
        public static XPlaneCommand FuelRightFuelSelectorNone { get { return GetXPlaneCommand("sim/fuel/right_fuel_selector_none", "Set fuel selector for right engine to none (shut off)."); } }
        public static XPlaneCommand FuelRightFuelSelectorLft { get { return GetXPlaneCommand("sim/fuel/right_fuel_selector_lft", "Set fuel selector for right engine to left tanks."); } }
        public static XPlaneCommand FuelRightFuelSelectorCtr { get { return GetXPlaneCommand("sim/fuel/right_fuel_selector_ctr", "Set fuel selector for right engine to center tanks."); } }
        public static XPlaneCommand FuelRightFuelSelectorRgt { get { return GetXPlaneCommand("sim/fuel/right_fuel_selector_rgt", "Set fuel selector for right engine to right tanks."); } }
        public static XPlaneCommand FuelRightFuelSelectorAll { get { return GetXPlaneCommand("sim/fuel/right_fuel_selector_all", "Set fuel selector for right engine to all tanks."); } }
        public static XPlaneCommand FuelFuelTransferToLft { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_to_lft", "Transfer fuel to left."); } }
        public static XPlaneCommand FuelFuelTransferToCtr { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_to_ctr", "Transfer fuel to center."); } }
        public static XPlaneCommand FuelFuelTransferToRgt { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_to_rgt", "Transfer fuel to right."); } }
        public static XPlaneCommand FuelFuelTransferToAft { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_to_aft", "Transfer fuel to aft."); } }
        public static XPlaneCommand FuelFuelTransferToOff { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_to_off", "Transfer fuel to none."); } }
        public static XPlaneCommand FuelFuelTransferFromLft { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_from_lft", "Transfer fuel from left."); } }
        public static XPlaneCommand FuelFuelTransferFromCtr { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_from_ctr", "Transfer fuel from center."); } }
        public static XPlaneCommand FuelFuelTransferFromRgt { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_from_rgt", "Transfer fuel from right."); } }
        public static XPlaneCommand FuelFuelTransferFromAft { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_from_aft", "Transfer fuel from aft."); } }
        public static XPlaneCommand FuelFuelTransferFromOff { get { return GetXPlaneCommand("sim/fuel/fuel_transfer_from_off", "Transfer fuel from none."); } }
        public static XPlaneCommand FuelFuelCrossfeedFromLftTank { get { return GetXPlaneCommand("sim/fuel/fuel_crossfeed_from_lft_tank", "Cross-feed fuel from left tank."); } }
        public static XPlaneCommand FuelFuelCrossfeedOff { get { return GetXPlaneCommand("sim/fuel/fuel_crossfeed_off", "Cross-feed fuel off."); } }
        public static XPlaneCommand FuelFuelCrossfeedFromRgtTank { get { return GetXPlaneCommand("sim/fuel/fuel_crossfeed_from_rgt_tank", "Cross-feed fuel from right tank."); } }
        public static XPlaneCommand FuelFuelFirewallValveLftOpen { get { return GetXPlaneCommand("sim/fuel/fuel_firewall_valve_lft_open", "Firewall fuel valve left open."); } }
        public static XPlaneCommand FuelFuelFirewallValveLftClosed { get { return GetXPlaneCommand("sim/fuel/fuel_firewall_valve_lft_closed", "Firewall fuel valve left closed."); } }
        public static XPlaneCommand FuelFuelFirewallValveRgtOpen { get { return GetXPlaneCommand("sim/fuel/fuel_firewall_valve_rgt_open", "Firewall fuel valve right open."); } }
        public static XPlaneCommand FuelFuelFirewallValveRgtClosed { get { return GetXPlaneCommand("sim/fuel/fuel_firewall_valve_rgt_closed", "Firewall fuel valve right closed."); } }
        public static XPlaneCommand FuelLeftXferOverride { get { return GetXPlaneCommand("sim/fuel/left_xfer_override", "Aux to feeder transfer left override."); } }
        public static XPlaneCommand FuelLeftXferOn { get { return GetXPlaneCommand("sim/fuel/left_xfer_on", "Aux to feeder transfer left on."); } }
        public static XPlaneCommand FuelLeftXferOff { get { return GetXPlaneCommand("sim/fuel/left_xfer_off", "Aux to feeder transfer left off."); } }
        public static XPlaneCommand FuelLeftXferUp { get { return GetXPlaneCommand("sim/fuel/left_xfer_up", "Aux to feeder transfer left off->on->overide."); } }
        public static XPlaneCommand FuelLeftXferDn { get { return GetXPlaneCommand("sim/fuel/left_xfer_dn", "Aux to feeder transfer left override->on->off."); } }
        public static XPlaneCommand FuelRightXferOverride { get { return GetXPlaneCommand("sim/fuel/right_xfer_override", "Aux to feeder transfer right override."); } }
        public static XPlaneCommand FuelRightXferOn { get { return GetXPlaneCommand("sim/fuel/right_xfer_on", "Aux to feeder transfer right on."); } }
        public static XPlaneCommand FuelRightXferOff { get { return GetXPlaneCommand("sim/fuel/right_xfer_off", "Aux to feeder transfer right off."); } }
        public static XPlaneCommand FuelRightXferUp { get { return GetXPlaneCommand("sim/fuel/right_xfer_up", "Aux to feeder transfer right off->on->overide."); } }
        public static XPlaneCommand FuelRightXferDn { get { return GetXPlaneCommand("sim/fuel/right_xfer_dn", "Aux to feeder transfer right override->on->off."); } }
        public static XPlaneCommand FuelLeftXferTest { get { return GetXPlaneCommand("sim/fuel/left_xfer_test", "Aux to feeder transfer test left."); } }
        public static XPlaneCommand FuelRightXferTest { get { return GetXPlaneCommand("sim/fuel/right_xfer_test", "Aux to feeder transfer test right."); } }
        public static XPlaneCommand FuelAutoCrossfeedOnOpen { get { return GetXPlaneCommand("sim/fuel/auto_crossfeed_on_open", "Crossfeed valve open."); } }
        public static XPlaneCommand FuelAutoCrossfeedAuto { get { return GetXPlaneCommand("sim/fuel/auto_crossfeed_auto", "Open crossfeed valve when pressure difference detected."); } }
        public static XPlaneCommand FuelAutoCrossfeedOff { get { return GetXPlaneCommand("sim/fuel/auto_crossfeed_off", "Close crossfeed valve and turn off auto-crossfeed."); } }
        public static XPlaneCommand FuelAutoCrossfeedUp { get { return GetXPlaneCommand("sim/fuel/auto_crossfeed_up", "Auto-crossfeed off->auto->on."); } }
        public static XPlaneCommand FuelAutoCrossfeedDown { get { return GetXPlaneCommand("sim/fuel/auto_crossfeed_down", "Auto-crossfeed on->auto->off."); } }
        public static XPlaneCommand FuelFuelPumpsOn { get { return GetXPlaneCommand("sim/fuel/fuel_pumps_on", "Fuel pumps on."); } }
        public static XPlaneCommand FuelFuelPumpsOff { get { return GetXPlaneCommand("sim/fuel/fuel_pumps_off", "Fuel pumps off."); } }
        public static XPlaneCommand FuelFuelPumpsTog { get { return GetXPlaneCommand("sim/fuel/fuel_pumps_tog", "Fuel pumps toggle."); } }
        public static XPlaneCommand FuelFuelPump1On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_1_on", "Fuel pump for engine #1 on."); } }
        public static XPlaneCommand FuelFuelPump2On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_2_on", "Fuel pump for engine #2 on."); } }
        public static XPlaneCommand FuelFuelPump3On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_3_on", "Fuel pump for engine #3 on."); } }
        public static XPlaneCommand FuelFuelPump4On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_4_on", "Fuel pump for engine #4 on."); } }
        public static XPlaneCommand FuelFuelPump5On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_5_on", "Fuel pump for engine #5 on."); } }
        public static XPlaneCommand FuelFuelPump6On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_6_on", "Fuel pump for engine #6 on."); } }
        public static XPlaneCommand FuelFuelPump7On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_7_on", "Fuel pump for engine #7 on."); } }
        public static XPlaneCommand FuelFuelPump8On { get { return GetXPlaneCommand("sim/fuel/fuel_pump_8_on", "Fuel pump for engine #8 on."); } }
        public static XPlaneCommand FuelFuelPump1Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_1_off", "Fuel pump for engine #1 off."); } }
        public static XPlaneCommand FuelFuelPump2Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_2_off", "Fuel pump for engine #2 off."); } }
        public static XPlaneCommand FuelFuelPump3Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_3_off", "Fuel pump for engine #3 off."); } }
        public static XPlaneCommand FuelFuelPump4Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_4_off", "Fuel pump for engine #4 off."); } }
        public static XPlaneCommand FuelFuelPump5Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_5_off", "Fuel pump for engine #5 off."); } }
        public static XPlaneCommand FuelFuelPump6Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_6_off", "Fuel pump for engine #6 off."); } }
        public static XPlaneCommand FuelFuelPump7Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_7_off", "Fuel pump for engine #7 off."); } }
        public static XPlaneCommand FuelFuelPump8Off { get { return GetXPlaneCommand("sim/fuel/fuel_pump_8_off", "Fuel pump for engine #8 off."); } }
        public static XPlaneCommand FuelFuelPump1Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_1_tog", "Fuel pump for engine #1 toggle."); } }
        public static XPlaneCommand FuelFuelPump2Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_2_tog", "Fuel pump for engine #2 toggle."); } }
        public static XPlaneCommand FuelFuelPump3Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_3_tog", "Fuel pump for engine #3 toggle."); } }
        public static XPlaneCommand FuelFuelPump4Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_4_tog", "Fuel pump for engine #4 toggle."); } }
        public static XPlaneCommand FuelFuelPump5Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_5_tog", "Fuel pump for engine #5 toggle."); } }
        public static XPlaneCommand FuelFuelPump6Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_6_tog", "Fuel pump for engine #6 toggle."); } }
        public static XPlaneCommand FuelFuelPump7Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_7_tog", "Fuel pump for engine #7 toggle."); } }
        public static XPlaneCommand FuelFuelPump8Tog { get { return GetXPlaneCommand("sim/fuel/fuel_pump_8_tog", "Fuel pump for engine #8 toggle."); } }
        public static XPlaneCommand FuelFuelPump1Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_1_prime", "Fuel pump prime for engine #1 on."); } }
        public static XPlaneCommand FuelFuelPump2Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_2_prime", "Fuel pump prime for engine #2 on."); } }
        public static XPlaneCommand FuelFuelPump3Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_3_prime", "Fuel pump prime for engine #3 on."); } }
        public static XPlaneCommand FuelFuelPump4Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_4_prime", "Fuel pump prime for engine #4 on."); } }
        public static XPlaneCommand FuelFuelPump5Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_5_prime", "Fuel pump prime for engine #5 on."); } }
        public static XPlaneCommand FuelFuelPump6Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_6_prime", "Fuel pump prime for engine #6 on."); } }
        public static XPlaneCommand FuelFuelPump7Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_7_prime", "Fuel pump prime for engine #7 on."); } }
        public static XPlaneCommand FuelFuelPump8Prime { get { return GetXPlaneCommand("sim/fuel/fuel_pump_8_prime", "Fuel pump prime for engine #8 on."); } }
        public static XPlaneCommand FuelIndicateAux { get { return GetXPlaneCommand("sim/fuel/indicate_aux", "Fuel tanks show auxiliary tanks."); } }
        public static XPlaneCommand FuelIndicateAll { get { return GetXPlaneCommand("sim/fuel/indicate_all", "Fuel tanks show all tanks."); } }
        public static XPlaneCommand FuelIndicateNacelle { get { return GetXPlaneCommand("sim/fuel/indicate_nacelle", "Fuel tanks show nacelle tanks."); } }
    }
}