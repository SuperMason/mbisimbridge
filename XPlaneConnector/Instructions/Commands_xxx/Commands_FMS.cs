﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand FMSLs1L { get { return GetXPlaneCommand("sim/FMS/ls_1l", "FMS: line select 1l"); } }
        public static XPlaneCommand FMSLs2L { get { return GetXPlaneCommand("sim/FMS/ls_2l", "FMS: line select 2l"); } }
        public static XPlaneCommand FMSLs3L { get { return GetXPlaneCommand("sim/FMS/ls_3l", "FMS: line select 3l"); } }
        public static XPlaneCommand FMSLs4L { get { return GetXPlaneCommand("sim/FMS/ls_4l", "FMS: line select 4l"); } }
        public static XPlaneCommand FMSLs5L { get { return GetXPlaneCommand("sim/FMS/ls_5l", "FMS: line select 5l"); } }
        public static XPlaneCommand FMSLs6L { get { return GetXPlaneCommand("sim/FMS/ls_6l", "FMS: line select 6l"); } }
        public static XPlaneCommand FMSLs1R { get { return GetXPlaneCommand("sim/FMS/ls_1r", "FMS: line select 1r"); } }
        public static XPlaneCommand FMSLs2R { get { return GetXPlaneCommand("sim/FMS/ls_2r", "FMS: line select 2r"); } }
        public static XPlaneCommand FMSLs3R { get { return GetXPlaneCommand("sim/FMS/ls_3r", "FMS: line select 3r"); } }
        public static XPlaneCommand FMSLs4R { get { return GetXPlaneCommand("sim/FMS/ls_4r", "FMS: line select 4r"); } }
        public static XPlaneCommand FMSLs5R { get { return GetXPlaneCommand("sim/FMS/ls_5r", "FMS: line select 5r"); } }
        public static XPlaneCommand FMSLs6R { get { return GetXPlaneCommand("sim/FMS/ls_6r", "FMS: line select 6r"); } }
        public static XPlaneCommand FMSIndex { get { return GetXPlaneCommand("sim/FMS/index", "FMS: INDEX"); } }
        public static XPlaneCommand FMSFpln { get { return GetXPlaneCommand("sim/FMS/fpln", "FMS: FPLN"); } }
        public static XPlaneCommand FMSClb { get { return GetXPlaneCommand("sim/FMS/clb", "FMS: CLB"); } }
        public static XPlaneCommand FMSCrz { get { return GetXPlaneCommand("sim/FMS/crz", "FMS: CRZ"); } }
        public static XPlaneCommand FMSDes { get { return GetXPlaneCommand("sim/FMS/des", "FMS: DES"); } }
        public static XPlaneCommand FMSDirIntc { get { return GetXPlaneCommand("sim/FMS/dir_intc", "FMS: DIR/INTC"); } }
        public static XPlaneCommand FMSLegs { get { return GetXPlaneCommand("sim/FMS/legs", "FMS: FPLN"); } }
        public static XPlaneCommand FMSDepArr { get { return GetXPlaneCommand("sim/FMS/dep_arr", "FMS: DEP/ARR"); } }
        public static XPlaneCommand FMSHold { get { return GetXPlaneCommand("sim/FMS/hold", "FMS: HOLD"); } }
        public static XPlaneCommand FMSProg { get { return GetXPlaneCommand("sim/FMS/prog", "FMS: PROG"); } }
        public static XPlaneCommand FMSExec { get { return GetXPlaneCommand("sim/FMS/exec", "FMS: EXEC"); } }
        public static XPlaneCommand FMSFix { get { return GetXPlaneCommand("sim/FMS/fix", "FMS: FIX"); } }
        public static XPlaneCommand FMSNavrad { get { return GetXPlaneCommand("sim/FMS/navrad", "FMS: NAVRAD"); } }
        public static XPlaneCommand FMSInit { get { return GetXPlaneCommand("sim/FMS/init", "FMS: init"); } }
        public static XPlaneCommand FMSPrev { get { return GetXPlaneCommand("sim/FMS/prev", "FMS: prev"); } }
        public static XPlaneCommand FMSNext { get { return GetXPlaneCommand("sim/FMS/next", "FMS: next"); } }
        public static XPlaneCommand FMSClear { get { return GetXPlaneCommand("sim/FMS/clear", "FMS: clear"); } }
        public static XPlaneCommand FMSDirect { get { return GetXPlaneCommand("sim/FMS/direct", "FMS: direct"); } }
        public static XPlaneCommand FMSSign { get { return GetXPlaneCommand("sim/FMS/sign", "FMS: sign"); } }
        public static XPlaneCommand FMSTypeApt { get { return GetXPlaneCommand("sim/FMS/type_apt", "FMS: apt"); } }
        public static XPlaneCommand FMSTypeVor { get { return GetXPlaneCommand("sim/FMS/type_vor", "FMS: vor"); } }
        public static XPlaneCommand FMSTypeNdb { get { return GetXPlaneCommand("sim/FMS/type_ndb", "FMS: ndb"); } }
        public static XPlaneCommand FMSTypeFix { get { return GetXPlaneCommand("sim/FMS/type_fix", "FMS: fix"); } }
        public static XPlaneCommand FMSTypeLatlon { get { return GetXPlaneCommand("sim/FMS/type_latlon", "FMS: lat/lon"); } }
        public static XPlaneCommand FMSKey0 { get { return GetXPlaneCommand("sim/FMS/key_0", "FMS: key_0"); } }
        public static XPlaneCommand FMSKey1 { get { return GetXPlaneCommand("sim/FMS/key_1", "FMS: key_1"); } }
        public static XPlaneCommand FMSKey2 { get { return GetXPlaneCommand("sim/FMS/key_2", "FMS: key_2"); } }
        public static XPlaneCommand FMSKey3 { get { return GetXPlaneCommand("sim/FMS/key_3", "FMS: key_3"); } }
        public static XPlaneCommand FMSKey4 { get { return GetXPlaneCommand("sim/FMS/key_4", "FMS: key_4"); } }
        public static XPlaneCommand FMSKey5 { get { return GetXPlaneCommand("sim/FMS/key_5", "FMS: key_5"); } }
        public static XPlaneCommand FMSKey6 { get { return GetXPlaneCommand("sim/FMS/key_6", "FMS: key_6"); } }
        public static XPlaneCommand FMSKey7 { get { return GetXPlaneCommand("sim/FMS/key_7", "FMS: key_7"); } }
        public static XPlaneCommand FMSKey8 { get { return GetXPlaneCommand("sim/FMS/key_8", "FMS: key_8"); } }
        public static XPlaneCommand FMSKey9 { get { return GetXPlaneCommand("sim/FMS/key_9", "FMS: key_9"); } }
        public static XPlaneCommand FMSKeyA { get { return GetXPlaneCommand("sim/FMS/key_A", "FMS: key_A"); } }
        public static XPlaneCommand FMSKeyB { get { return GetXPlaneCommand("sim/FMS/key_B", "FMS: key_B"); } }
        public static XPlaneCommand FMSKeyC { get { return GetXPlaneCommand("sim/FMS/key_C", "FMS: key_C"); } }
        public static XPlaneCommand FMSKeyD { get { return GetXPlaneCommand("sim/FMS/key_D", "FMS: key_D"); } }
        public static XPlaneCommand FMSKeyE { get { return GetXPlaneCommand("sim/FMS/key_E", "FMS: key_E"); } }
        public static XPlaneCommand FMSKeyF { get { return GetXPlaneCommand("sim/FMS/key_F", "FMS: key_F"); } }
        public static XPlaneCommand FMSKeyG { get { return GetXPlaneCommand("sim/FMS/key_G", "FMS: key_G"); } }
        public static XPlaneCommand FMSKeyH { get { return GetXPlaneCommand("sim/FMS/key_H", "FMS: key_H"); } }
        public static XPlaneCommand FMSKeyI { get { return GetXPlaneCommand("sim/FMS/key_I", "FMS: key_I"); } }
        public static XPlaneCommand FMSKeyJ { get { return GetXPlaneCommand("sim/FMS/key_J", "FMS: key_J"); } }
        public static XPlaneCommand FMSKeyK { get { return GetXPlaneCommand("sim/FMS/key_K", "FMS: key_K"); } }
        public static XPlaneCommand FMSKeyL { get { return GetXPlaneCommand("sim/FMS/key_L", "FMS: key_L"); } }
        public static XPlaneCommand FMSKeyM { get { return GetXPlaneCommand("sim/FMS/key_M", "FMS: key_M"); } }
        public static XPlaneCommand FMSKeyN { get { return GetXPlaneCommand("sim/FMS/key_N", "FMS: key_N"); } }
        public static XPlaneCommand FMSKeyO { get { return GetXPlaneCommand("sim/FMS/key_O", "FMS: key_O"); } }
        public static XPlaneCommand FMSKeyP { get { return GetXPlaneCommand("sim/FMS/key_P", "FMS: key_P"); } }
        public static XPlaneCommand FMSKeyQ { get { return GetXPlaneCommand("sim/FMS/key_Q", "FMS: key_Q"); } }
        public static XPlaneCommand FMSKeyR { get { return GetXPlaneCommand("sim/FMS/key_R", "FMS: key_R"); } }
        public static XPlaneCommand FMSKeyS { get { return GetXPlaneCommand("sim/FMS/key_S", "FMS: key_S"); } }
        public static XPlaneCommand FMSKeyT { get { return GetXPlaneCommand("sim/FMS/key_T", "FMS: key_T"); } }
        public static XPlaneCommand FMSKeyU { get { return GetXPlaneCommand("sim/FMS/key_U", "FMS: key_U"); } }
        public static XPlaneCommand FMSKeyV { get { return GetXPlaneCommand("sim/FMS/key_V", "FMS: key_V"); } }
        public static XPlaneCommand FMSKeyW { get { return GetXPlaneCommand("sim/FMS/key_W", "FMS: key_W"); } }
        public static XPlaneCommand FMSKeyX { get { return GetXPlaneCommand("sim/FMS/key_X", "FMS: key_X"); } }
        public static XPlaneCommand FMSKeyY { get { return GetXPlaneCommand("sim/FMS/key_Y", "FMS: key_Y"); } }
        public static XPlaneCommand FMSKeyZ { get { return GetXPlaneCommand("sim/FMS/key_Z", "FMS: key_Z"); } }
        public static XPlaneCommand FMSKeyPeriod { get { return GetXPlaneCommand("sim/FMS/key_period", "FMS: key_period"); } }
        public static XPlaneCommand FMSKeyMinus { get { return GetXPlaneCommand("sim/FMS/key_minus", "FMS: key_minus"); } }
        public static XPlaneCommand FMSKeySlash { get { return GetXPlaneCommand("sim/FMS/key_slash", "FMS: key_slash"); } }
        public static XPlaneCommand FMSKeyBack { get { return GetXPlaneCommand("sim/FMS/key_back", "FMS: key_back"); } }
        public static XPlaneCommand FMSKeySpace { get { return GetXPlaneCommand("sim/FMS/key_space", "FMS: key_space"); } }
        public static XPlaneCommand FMSKeyLoad { get { return GetXPlaneCommand("sim/FMS/key_load", "FMS: key_load"); } }
        public static XPlaneCommand FMSKeySave { get { return GetXPlaneCommand("sim/FMS/key_save", "FMS: key_save"); } }
        public static XPlaneCommand FMSKeyDelete { get { return GetXPlaneCommand("sim/FMS/key_delete", "FMS: key_delete"); } }
        public static XPlaneCommand FMSKeyClear { get { return GetXPlaneCommand("sim/FMS/key_clear", "FMS: key_clear"); } }
        public static XPlaneCommand FMSCDUPopup { get { return GetXPlaneCommand("sim/FMS/CDU_popup", "FMS: CDU popup"); } }
        public static XPlaneCommand FMSCDUPopout { get { return GetXPlaneCommand("sim/FMS/CDU_popout", "FMS: pop out CDU window"); } }
        public static XPlaneCommand FMSFixNext { get { return GetXPlaneCommand("sim/FMS/fix_next", "FMS: next fix"); } }
        public static XPlaneCommand FMSFixPrev { get { return GetXPlaneCommand("sim/FMS/fix_prev", "FMS: prev fix"); } }
    }
}