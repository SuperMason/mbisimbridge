﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        /// <summary>
        /// Quit X-Plane.
        /// </summary>
        public static XPlaneCommand OperationQuit { get { return GetXPlaneCommand("sim/operation/quit", "Quit X-Plane."); } }
        /// <summary>
        /// Take a screenshot.
        /// </summary>
        public static XPlaneCommand OperationScreenshot { get { return GetXPlaneCommand("sim/operation/screenshot", "Take a screenshot."); } }
        /// <summary>
        /// Show the in-sim menu.
        /// </summary>
        public static XPlaneCommand OperationShowMenu { get { return GetXPlaneCommand("sim/operation/show_menu", "Show the in-sim menu."); } }
        /// <summary>
        /// (Re)generate all icons for the current aircraft.
        /// </summary>
        public static XPlaneCommand OperationMakeCurrentAircraftIcons { get { return GetXPlaneCommand("sim/operation/make_current_aircraft_icons", "(Re)generate all icons for the current aircraft."); } }
        /// <summary>
        /// (Re)generate the icon for the current aircraft & livery.
        /// </summary>
        public static XPlaneCommand OperationMakeSingleIcon { get { return GetXPlaneCommand("sim/operation/make_single_icon", "(Re)generate the icon for the current aircraft & livery."); } }
        /// <summary>
        /// Generate all missing aircraft & livery icons.
        /// </summary>
        public static XPlaneCommand OperationMakeMissingIcons { get { return GetXPlaneCommand("sim/operation/make_missing_icons", "Generate all missing aircraft & livery icons."); } }
        /// <summary>
        /// Regenerate weather.
        /// </summary>
        public static XPlaneCommand OperationRegenWeather { get { return GetXPlaneCommand("sim/operation/regen_weather", "Regenerate weather."); } }
        /// <summary>
        /// Print out a cycle dump for this frame.
        /// </summary>
        public static XPlaneCommand OperationCycleDump { get { return GetXPlaneCommand("sim/operation/cycle_dump", "Print out a cycle dump for this frame."); } }
        /// <summary>
        /// Print longitudinal stability derivative.
        /// </summary>
        public static XPlaneCommand OperationStabDerivPitch { get { return GetXPlaneCommand("sim/operation/stab_deriv_pitch", "Print longitudinal stability derivative."); } }
        /// <summary>
        /// Print lateral stability derivative.
        /// </summary>
        public static XPlaneCommand OperationStabDerivHeading { get { return GetXPlaneCommand("sim/operation/stab_deriv_heading", "Print lateral stability derivative."); } }
        /// <summary>
        /// Toggle user interaction recording pane.
        /// </summary>
        public static XPlaneCommand OperationRecording { get { return GetXPlaneCommand("sim/operation/recording", "Toggle user interaction recording pane."); } }
        /// <summary>
        /// Drop a snapshot marker for replay mode.
        /// </summary>
        public static XPlaneCommand OperationCreateSnapMarker { get { return GetXPlaneCommand("sim/operation/create_snap_marker", "Drop a snapshot marker for replay mode."); } }
        /// <summary>
        /// Test dataref: Run from 0 to 1 and back.
        /// </summary>
        public static XPlaneCommand OperationTestDataRef { get { return GetXPlaneCommand("sim/operation/test_data_ref", "Test dataref: Run from 0 to 1 and back."); } }
        /// <summary>
        /// Toggle on-screen frame-rate display.
        /// </summary>
        public static XPlaneCommand OperationShowFps { get { return GetXPlaneCommand("sim/operation/show_fps", "Toggle on-screen frame-rate display."); } }
        /// <summary>
        /// Toggle dev console.
        /// </summary>
        public static XPlaneCommand OperationDevConsole { get { return GetXPlaneCommand("sim/operation/dev_console", "Toggle dev console."); } }
        /// <summary>
        /// Toggle full-screen mode.
        /// </summary>
        public static XPlaneCommand OperationToggleFullScreen { get { return GetXPlaneCommand("sim/operation/toggle_full_screen", "Toggle full-screen mode."); } }
        /// <summary>
        /// Force reloading the current aircraft (including art).
        /// </summary>
        public static XPlaneCommand OperationReloadAircraft { get { return GetXPlaneCommand("sim/operation/reload_aircraft", "Force reloading the current aircraft (including art)."); } }
        /// <summary>
        /// Force reloading the current aircraft (skip art reload).
        /// </summary>
        public static XPlaneCommand OperationReloadAircraftNoArt { get { return GetXPlaneCommand("sim/operation/reload_aircraft_no_art", "Force reloading the current aircraft (skip art reload)."); } }
        /// <summary>
        /// Force reloading the current scenery.
        /// </summary>
        public static XPlaneCommand OperationReloadScenery { get { return GetXPlaneCommand("sim/operation/reload_scenery", "Force reloading the current scenery."); } }
        /// <summary>
        /// Scan real weather files.
        /// </summary>
        public static XPlaneCommand OperationLoadRealWeather { get { return GetXPlaneCommand("sim/operation/load_real_weather", "Scan real weather files."); } }
        /// <summary>
        /// Fail selected in failures screen.
        /// </summary>
        public static XPlaneCommand OperationFailSystem { get { return GetXPlaneCommand("sim/operation/fail_system", "Fail selected in failures screen."); } }
        /// <summary>
        /// Make screenshots of your panels.
        /// </summary>
        public static XPlaneCommand OperationMakePanelPreviews { get { return GetXPlaneCommand("sim/operation/make_panel_previews", "Make screenshots of your panels."); } }
        /// <summary>
        /// Close any windows to get back to cockpit.
        /// </summary>
        public static XPlaneCommand OperationCloseWindows { get { return GetXPlaneCommand("sim/operation/close_windows", "Close any windows to get back to cockpit."); } }
        /// <summary>
        /// Load situation 1.
        /// </summary>
        public static XPlaneCommand OperationLoadSituation1 { get { return GetXPlaneCommand("sim/operation/load_situation_1", "Load situation 1."); } }
        /// <summary>
        /// Load situation 2.
        /// </summary>
        public static XPlaneCommand OperationLoadSituation2 { get { return GetXPlaneCommand("sim/operation/load_situation_2", "Load situation 2."); } }
        /// <summary>
        /// Load situation 3.
        /// </summary>
        public static XPlaneCommand OperationLoadSituation3 { get { return GetXPlaneCommand("sim/operation/load_situation_3", "Load situation 3."); } }
        /// <summary>
        /// Toggle the Flight Configuration window.
        /// </summary>
        public static XPlaneCommand OperationToggleFlightConfig { get { return GetXPlaneCommand("sim/operation/toggle_flight_config", "Toggle the Flight Configuration window."); } }
        /// <summary>
        /// Toggle the Main Menu screen.
        /// </summary>
        public static XPlaneCommand OperationToggleMainMenu { get { return GetXPlaneCommand("sim/operation/toggle_main_menu", "Toggle the Main Menu screen."); } }
        /// <summary>
        /// Toggle the Settings window.
        /// </summary>
        public static XPlaneCommand OperationToggleSettingsWindow { get { return GetXPlaneCommand("sim/operation/toggle_settings_window", "Toggle the Settings window."); } }
        /// <summary>
        /// Toggle the Flight School window.
        /// </summary>
        public static XPlaneCommand OperationToggleFlightSchoolWindow { get { return GetXPlaneCommand("sim/operation/toggle_flight_school_window", "Toggle the Flight School window."); } }
        /// <summary>
        /// Toggle the Keyboard Shortcuts window.
        /// </summary>
        public static XPlaneCommand OperationToggleKeyShortcutsWindow { get { return GetXPlaneCommand("sim/operation/toggle_key_shortcuts_window", "Toggle the Keyboard Shortcuts window."); } }
        /// <summary>
        /// Open the Weight & Balance window.
        /// </summary>
        public static XPlaneCommand OperationOpenWeightAndBalanceWindow { get { return GetXPlaneCommand("sim/operation/open_weight_and_balance_window", "Open the Weight & Balance window."); } }
        /// <summary>
        /// Open the Failures window.
        /// </summary>
        public static XPlaneCommand OperationOpenFailuresWindow { get { return GetXPlaneCommand("sim/operation/open_failures_window", "Open the Failures window."); } }
        /// <summary>
        /// Toggle display of the data output graph.
        /// </summary>
        public static XPlaneCommand OperationToggleDataOutputGraph { get { return GetXPlaneCommand("sim/operation/toggle_data_output_graph", "Toggle display of the data output graph."); } }
        /// <summary>
        /// Toggle display of the cockpit data output.
        /// </summary>
        public static XPlaneCommand OperationToggleDataOutputCockpit { get { return GetXPlaneCommand("sim/operation/toggle_data_output_cockpit", "Toggle display of the cockpit data output."); } }
        /// <summary>
        /// Toggle the keyboard & joystick profiles window.
        /// </summary>
        public static XPlaneCommand OperationToggleJoyProfilesWindow { get { return GetXPlaneCommand("sim/operation/toggle_joy_profiles_window", "Toggle the keyboard & joystick profiles window."); } }
        /// <summary>
        /// Toggle the Location details window.
        /// </summary>
        public static XPlaneCommand OperationToggleCustomLocationWindow { get { return GetXPlaneCommand("sim/operation/toggle_custom_location_window", "Toggle the Location details window."); } }
        /// <summary>
        /// Toggle display of the V11 UI style guide.
        /// </summary>
        public static XPlaneCommand OperationToggleStyleGuide { get { return GetXPlaneCommand("sim/operation/toggle_style_guide", "Toggle display of the V11 UI style guide."); } }
        /// <summary>
        /// Slider #1 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider01 { get { return GetXPlaneCommand("sim/operation/slider_01", "Slider #1 On/Off control."); } }
        /// <summary>
        /// Slider #2 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider02 { get { return GetXPlaneCommand("sim/operation/slider_02", "Slider #2 On/Off control."); } }
        /// <summary>
        /// Slider #3 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider03 { get { return GetXPlaneCommand("sim/operation/slider_03", "Slider #3 On/Off control."); } }
        /// <summary>
        /// Slider #4 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider04 { get { return GetXPlaneCommand("sim/operation/slider_04", "Slider #4 On/Off control."); } }
        /// <summary>
        /// Slider #5 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider05 { get { return GetXPlaneCommand("sim/operation/slider_05", "Slider #5 On/Off control."); } }
        /// <summary>
        /// Slider #6 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider06 { get { return GetXPlaneCommand("sim/operation/slider_06", "Slider #6 On/Off control."); } }
        /// <summary>
        /// Slider #7 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider07 { get { return GetXPlaneCommand("sim/operation/slider_07", "Slider #7 On/Off control."); } }
        /// <summary>
        /// Slider #8 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider08 { get { return GetXPlaneCommand("sim/operation/slider_08", "Slider #8 On/Off control."); } }
        /// <summary>
        /// Slider #9 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider09 { get { return GetXPlaneCommand("sim/operation/slider_09", "Slider #9 On/Off control."); } }
        /// <summary>
        /// Slider #10 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider10 { get { return GetXPlaneCommand("sim/operation/slider_10", "Slider #10 On/Off control."); } }
        /// <summary>
        /// Slider #11 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider11 { get { return GetXPlaneCommand("sim/operation/slider_11", "Slider #11 On/Off control."); } }
        /// <summary>
        /// Slider #12 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider12 { get { return GetXPlaneCommand("sim/operation/slider_12", "Slider #12 On/Off control."); } }
        /// <summary>
        /// Slider #13 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider13 { get { return GetXPlaneCommand("sim/operation/slider_13", "Slider #13 On/Off control."); } }
        /// <summary>
        /// Slider #14 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider14 { get { return GetXPlaneCommand("sim/operation/slider_14", "Slider #14 On/Off control."); } }
        /// <summary>
        /// Slider #15 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider15 { get { return GetXPlaneCommand("sim/operation/slider_15", "Slider #15 On/Off control."); } }
        /// <summary>
        /// Slider #16 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider16 { get { return GetXPlaneCommand("sim/operation/slider_16", "Slider #16 On/Off control."); } }
        /// <summary>
        /// Slider #17 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider17 { get { return GetXPlaneCommand("sim/operation/slider_17", "Slider #17 On/Off control."); } }
        /// <summary>
        /// Slider #18 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider18 { get { return GetXPlaneCommand("sim/operation/slider_18", "Slider #18 On/Off control."); } }
        /// <summary>
        /// Slider #19 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider19 { get { return GetXPlaneCommand("sim/operation/slider_19", "Slider #19 On/Off control."); } }
        /// <summary>
        /// Slider #20 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider20 { get { return GetXPlaneCommand("sim/operation/slider_20", "Slider #20 On/Off control."); } }
        /// <summary>
        /// Slider #21 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider21 { get { return GetXPlaneCommand("sim/operation/slider_21", "Slider #21 On/Off control."); } }
        /// <summary>
        /// Slider #22 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider22 { get { return GetXPlaneCommand("sim/operation/slider_22", "Slider #22 On/Off control."); } }
        /// <summary>
        /// Slider #23 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider23 { get { return GetXPlaneCommand("sim/operation/slider_23", "Slider #23 On/Off control."); } }
        /// <summary>
        /// Slider #24 On/Off control.
        /// </summary>
        public static XPlaneCommand OperationSlider24 { get { return GetXPlaneCommand("sim/operation/slider_24", "Slider #24 On/Off control."); } }
        /// <summary>
        /// Fix all failed systems.
        /// </summary>
        public static XPlaneCommand OperationFixAllSystems { get { return GetXPlaneCommand("sim/operation/fix_all_systems", "Fix all failed systems."); } }
        /// <summary>
        /// Auto-set electrical system for boarding.
        /// </summary>
        public static XPlaneCommand OperationAutoBoard { get { return GetXPlaneCommand("sim/operation/auto_board", "Auto-set electrical system for boarding."); } }
        /// <summary>
        /// Auto-start engines to running, real-time.
        /// </summary>
        public static XPlaneCommand OperationAutoStart { get { return GetXPlaneCommand("sim/operation/auto_start", "Auto-start engines to running, real-time."); } }
        /// <summary>
        /// Quick-start engines to running.
        /// </summary>
        public static XPlaneCommand OperationQuickStart { get { return GetXPlaneCommand("sim/operation/quick_start", "Quick-start engines to running."); } }
        /// <summary>
        /// Load previous livery.
        /// </summary>
        public static XPlaneCommand OperationPrevLivery { get { return GetXPlaneCommand("sim/operation/prev_livery", "Load previous livery."); } }
        /// <summary>
        /// Load next livery.
        /// </summary>
        public static XPlaneCommand OperationNextLivery { get { return GetXPlaneCommand("sim/operation/next_livery", "Load next livery."); } }
        /// <summary>
        /// Sim 1x 2x 4x ground speed.
        /// </summary>
        public static XPlaneCommand OperationGroundSpeedChange { get { return GetXPlaneCommand("sim/operation/ground_speed_change", "Sim 1x 2x 4x ground speed."); } }
        /// <summary>
        /// Freeze the groundspeed of the simulation.
        /// </summary>
        public static XPlaneCommand OperationFreezeToggle { get { return GetXPlaneCommand("sim/operation/freeze_toggle", "Freeze the groundspeed of the simulation."); } }
        /// <summary>
        /// Time: a little earlier.
        /// </summary>
        public static XPlaneCommand OperationTimeDown { get { return GetXPlaneCommand("sim/operation/time_down", "Time: a little earlier."); } }
        /// <summary>
        /// Time: a little later.
        /// </summary>
        public static XPlaneCommand OperationTimeUp { get { return GetXPlaneCommand("sim/operation/time_up", "Time: a little later."); } }
        /// <summary>
        /// Time: a lot earlier.
        /// </summary>
        public static XPlaneCommand OperationTimeDownLots { get { return GetXPlaneCommand("sim/operation/time_down_lots", "Time: a lot earlier."); } }
        /// <summary>
        /// Time: a lot later.
        /// </summary>
        public static XPlaneCommand OperationTimeUpLots { get { return GetXPlaneCommand("sim/operation/time_up_lots", "Time: a lot later."); } }
        /// <summary>
        /// Date: a little earlier.
        /// </summary>
        public static XPlaneCommand OperationDateDown { get { return GetXPlaneCommand("sim/operation/date_down", "Date: a little earlier."); } }
        /// <summary>
        /// Date: a little later.
        /// </summary>
        public static XPlaneCommand OperationDateUp { get { return GetXPlaneCommand("sim/operation/date_up", "Date: a little later."); } }
        /// <summary>
        /// Sim 1x 2x 4x sim speed.
        /// </summary>
        public static XPlaneCommand OperationFlightmodelSpeedChange { get { return GetXPlaneCommand("sim/operation/flightmodel_speed_change", "Sim 1x 2x 4x sim speed."); } }
        /// <summary>
        /// Pause the simulation.
        /// </summary>
        public static XPlaneCommand OperationPauseToggle { get { return GetXPlaneCommand("sim/operation/pause_toggle", "Pause the simulation."); } }
        /// <summary>
        /// Toggle AVI movie recording.
        /// </summary>
        public static XPlaneCommand OperationVideoRecordToggle { get { return GetXPlaneCommand("sim/operation/video_record_toggle", "Toggle AVI movie recording."); } }
        /// <summary>
        /// Configure AVI movie recording.
        /// </summary>
        public static XPlaneCommand OperationConfigureVideoRecording { get { return GetXPlaneCommand("sim/operation/configure_video_recording", "Configure AVI movie recording."); } }
        /// <summary>
        /// Toggle logbook window.
        /// </summary>
        public static XPlaneCommand OperationToggleLogbook { get { return GetXPlaneCommand("sim/operation/toggle_logbook", "Toggle logbook window."); } }
        /// <summary>
        /// Toggle Save Flight window.
        /// </summary>
        public static XPlaneCommand OperationSaveFlight { get { return GetXPlaneCommand("sim/operation/save_flight", "Toggle Save Flight window."); } }
        /// <summary>
        /// Toggle Load Flight window.
        /// </summary>
        public static XPlaneCommand OperationLoadFlight { get { return GetXPlaneCommand("sim/operation/load_flight", "Toggle Load Flight window."); } }
        /// <summary>
        /// Toggle text file.
        /// </summary>
        public static XPlaneCommand OperationTextFileToggle { get { return GetXPlaneCommand("sim/operation/text_file_toggle", "Toggle text file."); } }
        /// <summary>
        /// Toggle checklist.
        /// </summary>
        public static XPlaneCommand OperationChecklistToggle { get { return GetXPlaneCommand("sim/operation/checklist_toggle", "Toggle checklist."); } }
        /// <summary>
        /// Next item in checklist.
        /// </summary>
        public static XPlaneCommand OperationChecklistNext { get { return GetXPlaneCommand("sim/operation/checklist_next", "Next item in checklist."); } }
        /// <summary>
        /// Previous item in checklist.
        /// </summary>
        public static XPlaneCommand OperationChecklistPrevious { get { return GetXPlaneCommand("sim/operation/checklist_previous", "Previous item in checklist."); } }
        /// <summary>
        /// Contact ATC.
        /// </summary>
        public static XPlaneCommand OperationContactAtc { get { return GetXPlaneCommand("sim/operation/contact_atc", "Contact ATC."); } }
        /// <summary>
        /// Toggle AI flying your aircraft.
        /// </summary>
        public static XPlaneCommand OperationToggleAiFlies { get { return GetXPlaneCommand("sim/operation/toggle_ai_flies", "Toggle AI flying your aircraft."); } }
        /// <summary>
        /// Toggle yoke visibility.
        /// </summary>
        public static XPlaneCommand OperationToggleYoke { get { return GetXPlaneCommand("sim/operation/toggle_yoke", "Toggle yoke visibility."); } }
        /// <summary>
        /// Reset flight to most recent start.
        /// </summary>
        public static XPlaneCommand OperationResetFlight { get { return GetXPlaneCommand("sim/operation/reset_flight", "Reset flight to most recent start."); } }
        /// <summary>
        /// Reset flight to nearest airport.
        /// </summary>
        public static XPlaneCommand OperationGoToDefault { get { return GetXPlaneCommand("sim/operation/go_to_default", "Reset flight to nearest airport."); } }
        /// <summary>
        /// Reset flight to nearest runway.
        /// </summary>
        public static XPlaneCommand OperationResetToRunway { get { return GetXPlaneCommand("sim/operation/reset_to_runway", "Reset flight to nearest runway."); } }
        /// <summary>
        /// Reset flight to next runway on current airport
        /// </summary>
        public static XPlaneCommand OperationGoNextRunway { get { return GetXPlaneCommand("sim/operation/go_next_runway", "Reset flight to next runway on current airport"); } }
        /// <summary>
        /// Grass field takeoff.
        /// </summary>
        public static XPlaneCommand OperationGrassFieldTakeoff { get { return GetXPlaneCommand("sim/operation/Grass_Field_Takeoff", "Grass field takeoff."); } }
        /// <summary>
        /// Dirt field takeoff.
        /// </summary>
        public static XPlaneCommand OperationDirtFieldTakeoff { get { return GetXPlaneCommand("sim/operation/Dirt_Field_Takeoff", "Dirt field takeoff."); } }
        /// <summary>
        /// Gravel field takeoff.
        /// </summary>
        public static XPlaneCommand OperationGravelFieldTakeoff { get { return GetXPlaneCommand("sim/operation/Gravel_Field_Takeoff", "Gravel field takeoff."); } }
        /// <summary>
        /// Waterway takeoff.
        /// </summary>
        public static XPlaneCommand OperationWaterWayTakeoff { get { return GetXPlaneCommand("sim/operation/Water_Way_Takeoff", "Waterway takeoff."); } }
        /// <summary>
        /// Helipad takeoff.
        /// </summary>
        public static XPlaneCommand OperationHelipadTakeoff { get { return GetXPlaneCommand("sim/operation/Helipad_Takeoff", "Helipad takeoff."); } }
        /// <summary>
        /// Carrier catshot.
        /// </summary>
        public static XPlaneCommand OperationCarrierCatshot { get { return GetXPlaneCommand("sim/operation/Carrier_Catshot", "Carrier catshot."); } }
        /// <summary>
        /// Glider winch start.
        /// </summary>
        public static XPlaneCommand OperationGliderWinch { get { return GetXPlaneCommand("sim/operation/Glider_Winch", "Glider winch start."); } }
        /// <summary>
        /// Glider tow start.
        /// </summary>
        public static XPlaneCommand OperationGliderTow { get { return GetXPlaneCommand("sim/operation/Glider_Tow", "Glider tow start."); } }
        /// <summary>
        /// Airdrop from B-52.
        /// </summary>
        public static XPlaneCommand OperationAirDropFromB52 { get { return GetXPlaneCommand("sim/operation/Air_Drop_from_B_52", "Airdrop from B-52."); } }
        /// <summary>
        /// Other aircraft carries your own.
        /// </summary>
        public static XPlaneCommand OperationStartCarried { get { return GetXPlaneCommand("sim/operation/start_carried", "Other aircraft carries your own."); } }
        /// <summary>
        /// Piggyback Shuttle on 747.
        /// </summary>
        public static XPlaneCommand OperationPiggybackShuttleOn747 { get { return GetXPlaneCommand("sim/operation/Piggyback_Shuttle_on_747", "Piggyback Shuttle on 747."); } }
        /// <summary>
        /// Carry another aircraft.
        /// </summary>
        public static XPlaneCommand OperationCarryOtherAircraft { get { return GetXPlaneCommand("sim/operation/carry_other_aircraft", "Carry another aircraft."); } }
        /// <summary>
        /// Formation flying.
        /// </summary>
        public static XPlaneCommand OperationFormationFlying { get { return GetXPlaneCommand("sim/operation/Formation_Flying", "Formation flying."); } }
        /// <summary>
        /// Air refueling (boom).
        /// </summary>
        public static XPlaneCommand OperationAirRefuelBoom { get { return GetXPlaneCommand("sim/operation/Air_Refuel_Boom", "Air refueling (boom)."); } }
        /// <summary>
        /// Air refueling (basket).
        /// </summary>
        public static XPlaneCommand OperationAirRefuelBasket { get { return GetXPlaneCommand("sim/operation/Air_Refuel_Basket", "Air refueling (basket)."); } }
        /// <summary>
        /// Aircraft carrier approach.
        /// </summary>
        public static XPlaneCommand OperationAircraftCarrierApproach { get { return GetXPlaneCommand("sim/operation/Aircraft_Carrier_Approach", "Aircraft carrier approach."); } }
        /// <summary>
        /// Frigate approach.
        /// </summary>
        public static XPlaneCommand OperationFrigateApproach { get { return GetXPlaneCommand("sim/operation/Frigate_Approach", "Frigate approach."); } }
        /// <summary>
        /// Medium oil rig approach.
        /// </summary>
        public static XPlaneCommand OperationMediumOilRigApproach { get { return GetXPlaneCommand("sim/operation/Medium_Oil_Rig_Approach", "Medium oil rig approach."); } }
        /// <summary>
        /// Large oil platform approach.
        /// </summary>
        public static XPlaneCommand OperationLargeOilPlatformApproach { get { return GetXPlaneCommand("sim/operation/Large_Oil_Platform_Approach", "Large oil platform approach."); } }
        /// <summary>
        /// Forest fire approach.
        /// </summary>
        public static XPlaneCommand OperationForestFireApproach { get { return GetXPlaneCommand("sim/operation/Forest_Fire_Approach", "Forest fire approach."); } }
        /// <summary>
        /// Space Shuttle: Full re-entry.
        /// </summary>
        public static XPlaneCommand OperationSpaceShuttleFullReEntry { get { return GetXPlaneCommand("sim/operation/Space_Shuttle_Full_Re_entry", "Space Shuttle: Full re-entry."); } }
        /// <summary>
        /// Space Shuttle: Final re-entry.
        /// </summary>
        public static XPlaneCommand OperationSpaceShuttleFinalReEntry { get { return GetXPlaneCommand("sim/operation/Space_Shuttle_Final_Re_entry", "Space Shuttle: Final re-entry."); } }
        /// <summary>
        /// Space Shuttle: Full approach.
        /// </summary>
        public static XPlaneCommand OperationSpaceShuttleFullApproach { get { return GetXPlaneCommand("sim/operation/Space_Shuttle_Full_Approach", "Space Shuttle: Full approach."); } }
        /// <summary>
        /// Space Shuttle: Final approach.
        /// </summary>
        public static XPlaneCommand OperationSpaceShuttleFinalApproach { get { return GetXPlaneCommand("sim/operation/Space_Shuttle_Final_Approach", "Space Shuttle: Final approach."); } }
        /// <summary>
        /// Toggle the sky colors window.
        /// </summary>
        public static XPlaneCommand OperationToggleSkyColorsWin { get { return GetXPlaneCommand("sim/operation/toggle_sky_colors_win", "Toggle the sky colors window."); } }
    }
}