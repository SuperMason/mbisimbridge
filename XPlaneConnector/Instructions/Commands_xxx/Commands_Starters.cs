﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand StartersShutDown { get { return GetXPlaneCommand("sim/starters/shut_down", "Pull fuel and mags for shutdown."); } }
        public static XPlaneCommand StartersEngageStarter1 { get { return GetXPlaneCommand("sim/starters/engage_starter_1", "Engage starter #1."); } }
        public static XPlaneCommand StartersEngageStarter2 { get { return GetXPlaneCommand("sim/starters/engage_starter_2", "Engage starter #2."); } }
        public static XPlaneCommand StartersEngageStarter3 { get { return GetXPlaneCommand("sim/starters/engage_starter_3", "Engage starter #3."); } }
        public static XPlaneCommand StartersEngageStarter4 { get { return GetXPlaneCommand("sim/starters/engage_starter_4", "Engage starter #4."); } }
        public static XPlaneCommand StartersEngageStarter5 { get { return GetXPlaneCommand("sim/starters/engage_starter_5", "Engage starter #5."); } }
        public static XPlaneCommand StartersEngageStarter6 { get { return GetXPlaneCommand("sim/starters/engage_starter_6", "Engage starter #6."); } }
        public static XPlaneCommand StartersEngageStarter7 { get { return GetXPlaneCommand("sim/starters/engage_starter_7", "Engage starter #7."); } }
        public static XPlaneCommand StartersEngageStarter8 { get { return GetXPlaneCommand("sim/starters/engage_starter_8", "Engage starter #8."); } }
        public static XPlaneCommand StartersShutDown1 { get { return GetXPlaneCommand("sim/starters/shut_down_1", "Pull fuel and mags for shut-down #1."); } }
        public static XPlaneCommand StartersShutDown2 { get { return GetXPlaneCommand("sim/starters/shut_down_2", "Pull fuel and mags for shut-down #2."); } }
        public static XPlaneCommand StartersShutDown3 { get { return GetXPlaneCommand("sim/starters/shut_down_3", "Pull fuel and mags for shut-down #3."); } }
        public static XPlaneCommand StartersShutDown4 { get { return GetXPlaneCommand("sim/starters/shut_down_4", "Pull fuel and mags for shut-down #4."); } }
        public static XPlaneCommand StartersShutDown5 { get { return GetXPlaneCommand("sim/starters/shut_down_5", "Pull fuel and mags for shut-down #5."); } }
        public static XPlaneCommand StartersShutDown6 { get { return GetXPlaneCommand("sim/starters/shut_down_6", "Pull fuel and mags for shut-down #6."); } }
        public static XPlaneCommand StartersShutDown7 { get { return GetXPlaneCommand("sim/starters/shut_down_7", "Pull fuel and mags for shut-down #7."); } }
        public static XPlaneCommand StartersShutDown8 { get { return GetXPlaneCommand("sim/starters/shut_down_8", "Pull fuel and mags for shut-down #8."); } }
    }
}