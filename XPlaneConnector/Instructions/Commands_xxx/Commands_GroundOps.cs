﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand GroundOpsServicePlane { get { return GetXPlaneCommand("sim/ground_ops/service_plane", "Service the airplane with ground trucks."); } }
        public static XPlaneCommand GroundOpsPushbackLeft { get { return GetXPlaneCommand("sim/ground_ops/pushback_left", "Push-back for airliners: Nose 90 left."); } }
        public static XPlaneCommand GroundOpsPushbackStraight { get { return GetXPlaneCommand("sim/ground_ops/pushback_straight", "Push-back for airliners: Straight back."); } }
        public static XPlaneCommand GroundOpsPushbackRight { get { return GetXPlaneCommand("sim/ground_ops/pushback_right", "Push-back for airliners: Nose 90 right."); } }
        public static XPlaneCommand GroundOpsPushbackStop { get { return GetXPlaneCommand("sim/ground_ops/pushback_stop", "Push-back for airliners: Stop and let go."); } }
        public static XPlaneCommand GroundOpsToggleWindow { get { return GetXPlaneCommand("sim/ground_ops/toggle_window", "Toggle the ground handling window."); } }
    }
}