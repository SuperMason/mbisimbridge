﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand InstrumentsECAMModeUp { get { return GetXPlaneCommand("sim/instruments/ECAM_mode_up", "ECAM mode up."); } }
        public static XPlaneCommand InstrumentsECAMModeDown { get { return GetXPlaneCommand("sim/instruments/ECAM_mode_down", "ECAM mode down."); } }
        public static XPlaneCommand InstrumentsMapZoomIn { get { return GetXPlaneCommand("sim/instruments/map_zoom_in", "Zoom in EFIS map."); } }
        public static XPlaneCommand InstrumentsMapZoomOut { get { return GetXPlaneCommand("sim/instruments/map_zoom_out", "Zoom out EFIS map."); } }
        public static XPlaneCommand InstrumentsEFISWxr { get { return GetXPlaneCommand("sim/instruments/EFIS_wxr", "EFIS map weather."); } }
        public static XPlaneCommand InstrumentsEFISTcas { get { return GetXPlaneCommand("sim/instruments/EFIS_tcas", "EFIS map TCAS."); } }
        public static XPlaneCommand InstrumentsEFISApt { get { return GetXPlaneCommand("sim/instruments/EFIS_apt", "EFIS map airport."); } }
        public static XPlaneCommand InstrumentsEFISFix { get { return GetXPlaneCommand("sim/instruments/EFIS_fix", "EFIS map fix."); } }
        public static XPlaneCommand InstrumentsEFISVor { get { return GetXPlaneCommand("sim/instruments/EFIS_vor", "EFIS map VOR."); } }
        public static XPlaneCommand InstrumentsEFISNdb { get { return GetXPlaneCommand("sim/instruments/EFIS_ndb", "EFIS map NDB."); } }
        public static XPlaneCommand InstrumentsEFISPageUp { get { return GetXPlaneCommand("sim/instruments/EFIS_page_up", "EFIS page up."); } }
        public static XPlaneCommand InstrumentsEFISPageDn { get { return GetXPlaneCommand("sim/instruments/EFIS_page_dn", "EFIS page down."); } }
        public static XPlaneCommand InstrumentsEFISModeUp { get { return GetXPlaneCommand("sim/instruments/EFIS_mode_up", "EFIS mode up (APP/VOR/MAP/PLN)"); } }
        public static XPlaneCommand InstrumentsEFISModeDn { get { return GetXPlaneCommand("sim/instruments/EFIS_mode_dn", "EFIS mode down (PLN/MAP/VOR/APP)"); } }
        public static XPlaneCommand InstrumentsEFIS1PilotSelDn { get { return GetXPlaneCommand("sim/instruments/EFIS_1_pilot_sel_dn", "EFIS 1 selection pilot down (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS1PilotSelUp { get { return GetXPlaneCommand("sim/instruments/EFIS_1_pilot_sel_up", "EFIS 1 selection pilot up (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS1CopilotSelDn { get { return GetXPlaneCommand("sim/instruments/EFIS_1_copilot_sel_dn", "EFIS 1 selection copilot down (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS1CopilotSelUp { get { return GetXPlaneCommand("sim/instruments/EFIS_1_copilot_sel_up", "EFIS 1 selection copilot up (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS2PilotSelDn { get { return GetXPlaneCommand("sim/instruments/EFIS_2_pilot_sel_dn", "EFIS 2 selection pilot down (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS2PilotSelUp { get { return GetXPlaneCommand("sim/instruments/EFIS_2_pilot_sel_up", "EFIS 2 selection pilot up (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS2CopilotSelDn { get { return GetXPlaneCommand("sim/instruments/EFIS_2_copilot_sel_dn", "EFIS 2 selection copilot down (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsEFIS2CopilotSelUp { get { return GetXPlaneCommand("sim/instruments/EFIS_2_copilot_sel_up", "EFIS 2 selection copilot up (ADF/off/VOR)"); } }
        public static XPlaneCommand InstrumentsThermoUnitsToggle { get { return GetXPlaneCommand("sim/instruments/thermo_units_toggle", "Toggle english/metric thermometer."); } }
        public static XPlaneCommand InstrumentsBarometer2992 { get { return GetXPlaneCommand("sim/instruments/barometer_2992", "Baro pressure selection 2992."); } }
        public static XPlaneCommand InstrumentsDGSyncDown { get { return GetXPlaneCommand("sim/instruments/DG_sync_down", "vacuum DG sync down."); } }
        public static XPlaneCommand InstrumentsDGSyncUp { get { return GetXPlaneCommand("sim/instruments/DG_sync_up", "vacuum DG sync up."); } }
        public static XPlaneCommand InstrumentsDGSyncMag { get { return GetXPlaneCommand("sim/instruments/DG_sync_mag", "vacuum DG sync to magnetic north."); } }
        public static XPlaneCommand InstrumentsCopilotDGSyncDown { get { return GetXPlaneCommand("sim/instruments/copilot_DG_sync_down", "Copilot vacuum DG sync down."); } }
        public static XPlaneCommand InstrumentsCopilotDGSyncUp { get { return GetXPlaneCommand("sim/instruments/copilot_DG_sync_up", "Copilot vacuum DG sync up."); } }
        public static XPlaneCommand InstrumentsCopilotDGSyncMag { get { return GetXPlaneCommand("sim/instruments/copilot_DG_sync_mag", "Copilot vacuum DG sync to magnetic north."); } }
        public static XPlaneCommand InstrumentsFreeGyro { get { return GetXPlaneCommand("sim/instruments/free_gyro", "electric DG free."); } }
        public static XPlaneCommand InstrumentsSlaveGyro { get { return GetXPlaneCommand("sim/instruments/slave_gyro", "electric DG slave."); } }
        public static XPlaneCommand InstrumentsCopilotFreeGyro { get { return GetXPlaneCommand("sim/instruments/copilot_free_gyro", "Copilot electric DG free."); } }
        public static XPlaneCommand InstrumentsCopilotSlaveGyro { get { return GetXPlaneCommand("sim/instruments/copilot_slave_gyro", "Copilot electric DG slave."); } }
        public static XPlaneCommand InstrumentsFreeGyroDown { get { return GetXPlaneCommand("sim/instruments/free_gyro_down", "electric DG free sync down."); } }
        public static XPlaneCommand InstrumentsFreeGyroUp { get { return GetXPlaneCommand("sim/instruments/free_gyro_up", "electric DG free sync up."); } }
        public static XPlaneCommand InstrumentsCopilotFreeGyroDown { get { return GetXPlaneCommand("sim/instruments/copilot_free_gyro_down", "Copilot electric DG free sync down."); } }
        public static XPlaneCommand InstrumentsCopilotFreeGyroUp { get { return GetXPlaneCommand("sim/instruments/copilot_free_gyro_up", "Copilot electric DG free sync up."); } }
        public static XPlaneCommand InstrumentsAhRefDown { get { return GetXPlaneCommand("sim/instruments/ah_ref_down", "Horizon reference down a bit."); } }
        public static XPlaneCommand InstrumentsAhRefUp { get { return GetXPlaneCommand("sim/instruments/ah_ref_up", "Horizon reference up a bit."); } }
        public static XPlaneCommand InstrumentsAhRefCopilotDown { get { return GetXPlaneCommand("sim/instruments/ah_ref_copilot_down", "Horizon reference down a bit, copilot."); } }
        public static XPlaneCommand InstrumentsAhRefCopilotUp { get { return GetXPlaneCommand("sim/instruments/ah_ref_copilot_up", "Horizon reference down a bit, copilot."); } }
        public static XPlaneCommand InstrumentsAhFastErect { get { return GetXPlaneCommand("sim/instruments/ah_fast_erect", "Attitude vacuum gyro fast erect."); } }
        public static XPlaneCommand InstrumentsAhCage { get { return GetXPlaneCommand("sim/instruments/ah_cage", "Attitude vacuum gyro cage toggle."); } }
        public static XPlaneCommand InstrumentsAhFastErectCopilot { get { return GetXPlaneCommand("sim/instruments/ah_fast_erect_copilot", "Attitude vacuum gyro fast erect, copilot."); } }
        public static XPlaneCommand InstrumentsAhCageCopilot { get { return GetXPlaneCommand("sim/instruments/ah_cage_copilot", "Attitude vacuum gyro cage toggle, copilot"); } }
        public static XPlaneCommand InstrumentsBarometerDown { get { return GetXPlaneCommand("sim/instruments/barometer_down", "Baro selection down a bit."); } }
        public static XPlaneCommand InstrumentsBarometerUp { get { return GetXPlaneCommand("sim/instruments/barometer_up", "Baro selection up a bit."); } }
        public static XPlaneCommand InstrumentsBarometerCopilotDown { get { return GetXPlaneCommand("sim/instruments/barometer_copilot_down", "Baro selection down a bit, copilot."); } }
        public static XPlaneCommand InstrumentsBarometerCopilotUp { get { return GetXPlaneCommand("sim/instruments/barometer_copilot_up", "Baro selection up a bit, copilot."); } }
        public static XPlaneCommand InstrumentsBarometerStbyDown { get { return GetXPlaneCommand("sim/instruments/barometer_stby_down", "Baro selection down a bit, standby alt."); } }
        public static XPlaneCommand InstrumentsBarometerStbyUp { get { return GetXPlaneCommand("sim/instruments/barometer_stby_up", "Baro selection up a bit, standby alt."); } }
        public static XPlaneCommand InstrumentsBarometerApDown { get { return GetXPlaneCommand("sim/instruments/barometer_ap_down", "Baro selection down a bit, AP presel."); } }
        public static XPlaneCommand InstrumentsBarometerApUp { get { return GetXPlaneCommand("sim/instruments/barometer_ap_up", "Baro selection up a bit, AP presel."); } }
        public static XPlaneCommand InstrumentsDhRefDown { get { return GetXPlaneCommand("sim/instruments/dh_ref_down", "Decision-height reference down."); } }
        public static XPlaneCommand InstrumentsDhRefUp { get { return GetXPlaneCommand("sim/instruments/dh_ref_up", "Decision-height reference up."); } }
        public static XPlaneCommand InstrumentsDhRefCopilotDown { get { return GetXPlaneCommand("sim/instruments/dh_ref_copilot_down", "Decision-height reference copilot down."); } }
        public static XPlaneCommand InstrumentsDhRefCopilotUp { get { return GetXPlaneCommand("sim/instruments/dh_ref_copilot_up", "Decision-height reference copilot up."); } }
        public static XPlaneCommand InstrumentsMdaRefDown { get { return GetXPlaneCommand("sim/instruments/mda_ref_down", "Minimum descend alt reference down."); } }
        public static XPlaneCommand InstrumentsMdaRefUp { get { return GetXPlaneCommand("sim/instruments/mda_ref_up", "Minimum descend alt reference up."); } }
        public static XPlaneCommand InstrumentsMdaRefCopilotDown { get { return GetXPlaneCommand("sim/instruments/mda_ref_copilot_down", "Minimum descend alt reference copilot down."); } }
        public static XPlaneCommand InstrumentsMdaRefCopilotUp { get { return GetXPlaneCommand("sim/instruments/mda_ref_copilot_up", "Minimum descend alt reference copilot up."); } }
        public static XPlaneCommand InstrumentsBaroAltAlertCancel { get { return GetXPlaneCommand("sim/instruments/baro_alt_alert_cancel", "Cancel altitude alert (preselector)."); } }
        public static XPlaneCommand InstrumentsMdaAlertCancel { get { return GetXPlaneCommand("sim/instruments/mda_alert_cancel", "Cancel MDA alert."); } }
        public static XPlaneCommand InstrumentsPanelBrightDown { get { return GetXPlaneCommand("sim/instruments/panel_bright_down", "Panel brightness down a bit."); } }
        public static XPlaneCommand InstrumentsPanelBrightUp { get { return GetXPlaneCommand("sim/instruments/panel_bright_up", "Panel brightness up a bit."); } }
        public static XPlaneCommand InstrumentsInstrumentBrightDown { get { return GetXPlaneCommand("sim/instruments/instrument_bright_down", "Instrument brightness down a bit."); } }
        public static XPlaneCommand InstrumentsInstrumentBrightUp { get { return GetXPlaneCommand("sim/instruments/instrument_bright_up", "Instrument brightness up a bit."); } }
        public static XPlaneCommand InstrumentsTimerStartStop { get { return GetXPlaneCommand("sim/instruments/timer_start_stop", "Start or stop the timer."); } }
        public static XPlaneCommand InstrumentsTimerReset { get { return GetXPlaneCommand("sim/instruments/timer_reset", "Reset the timer."); } }
        public static XPlaneCommand InstrumentsTimerShowDate { get { return GetXPlaneCommand("sim/instruments/timer_show_date", "Show date on the chrono."); } }
        public static XPlaneCommand InstrumentsTimerMode { get { return GetXPlaneCommand("sim/instruments/timer_mode", "Timer/clock mode for chronos."); } }
        public static XPlaneCommand InstrumentsTimerCycle { get { return GetXPlaneCommand("sim/instruments/timer_cycle", "Timer start/stop/reset."); } }
        public static XPlaneCommand InstrumentsTimerIsGMT { get { return GetXPlaneCommand("sim/instruments/timer_is_GMT", "Timer is GMT."); } }
    }
}