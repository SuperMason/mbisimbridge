﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        /// <summary>
        /// Toggle the map window.
        /// </summary>
        public static XPlaneCommand MapShowCurrent { get { return GetXPlaneCommand("sim/map/show_current", "Toggle the map window."); } }
        /// <summary>
        /// Toggle the instructor operator station (IOS) window.
        /// </summary>
        public static XPlaneCommand MapShowInstructorOperatorStation { get { return GetXPlaneCommand("sim/map/show_instructor_operator_station", "Toggle the instructor operator station (IOS) window."); } }
        /// <summary>
        /// Toggle the low enroute map.
        /// </summary>
        public static XPlaneCommand MapShowLowEnroute { get { return GetXPlaneCommand("sim/map/show_low_enroute", "Toggle the low enroute map."); } }
        /// <summary>
        /// Toggle the high enroute map.
        /// </summary>
        public static XPlaneCommand MapShowHighEnroute { get { return GetXPlaneCommand("sim/map/show_high_enroute", "Toggle the high enroute map."); } }
        /// <summary>
        /// Toggle the sectional map.
        /// </summary>
        public static XPlaneCommand MapShowSectional { get { return GetXPlaneCommand("sim/map/show_sectional", "Toggle the sectional map."); } }
    }
}