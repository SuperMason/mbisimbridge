﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand OxyCrewValveOn { get { return GetXPlaneCommand("sim/oxy/crew_valve_on", "Crew oxygen: master valve open/on."); } }
        public static XPlaneCommand OxyCrewValveOff { get { return GetXPlaneCommand("sim/oxy/crew_valve_off", "Crew oxygen: master valve closed/off."); } }
        public static XPlaneCommand OxyCrewValveToggle { get { return GetXPlaneCommand("sim/oxy/crew_valve_toggle", "Crew oxygen: master valve toggle"); } }
        public static XPlaneCommand OxyCrewRegulatorUp { get { return GetXPlaneCommand("sim/oxy/crew_regulator_up", "Crew oxygen: demand regulator up."); } }
        public static XPlaneCommand OxyCrewRegulatorDown { get { return GetXPlaneCommand("sim/oxy/crew_regulator_down", "Crew oxygen: demand regulator down."); } }
        public static XPlaneCommand OxyPassengerO2On { get { return GetXPlaneCommand("sim/oxy/passenger_o2_on", "Passenger oxygen: drop masks."); } }
    }
}