﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand FMS2Ls1L { get { return GetXPlaneCommand("sim/FMS2/ls_1l", "FMS2: line select 1l"); } }
        public static XPlaneCommand FMS2Ls2L { get { return GetXPlaneCommand("sim/FMS2/ls_2l", "FMS2: line select 2l"); } }
        public static XPlaneCommand FMS2Ls3L { get { return GetXPlaneCommand("sim/FMS2/ls_3l", "FMS2: line select 3l"); } }
        public static XPlaneCommand FMS2Ls4L { get { return GetXPlaneCommand("sim/FMS2/ls_4l", "FMS2: line select 4l"); } }
        public static XPlaneCommand FMS2Ls5L { get { return GetXPlaneCommand("sim/FMS2/ls_5l", "FMS2: line select 5l"); } }
        public static XPlaneCommand FMS2Ls6L { get { return GetXPlaneCommand("sim/FMS2/ls_6l", "FMS2: line select 6l"); } }
        public static XPlaneCommand FMS2Ls1R { get { return GetXPlaneCommand("sim/FMS2/ls_1r", "FMS2: line select 1r"); } }
        public static XPlaneCommand FMS2Ls2R { get { return GetXPlaneCommand("sim/FMS2/ls_2r", "FMS2: line select 2r"); } }
        public static XPlaneCommand FMS2Ls3R { get { return GetXPlaneCommand("sim/FMS2/ls_3r", "FMS2: line select 3r"); } }
        public static XPlaneCommand FMS2Ls4R { get { return GetXPlaneCommand("sim/FMS2/ls_4r", "FMS2: line select 4r"); } }
        public static XPlaneCommand FMS2Ls5R { get { return GetXPlaneCommand("sim/FMS2/ls_5r", "FMS2: line select 5r"); } }
        public static XPlaneCommand FMS2Ls6R { get { return GetXPlaneCommand("sim/FMS2/ls_6r", "FMS2: line select 6r"); } }
        public static XPlaneCommand FMS2Index { get { return GetXPlaneCommand("sim/FMS2/index", "FMS: INDEX"); } }
        public static XPlaneCommand FMS2Fpln { get { return GetXPlaneCommand("sim/FMS2/fpln", "FMS2: FPLN"); } }
        public static XPlaneCommand FMS2Clb { get { return GetXPlaneCommand("sim/FMS2/clb", "FMS2: CLB"); } }
        public static XPlaneCommand FMS2Crz { get { return GetXPlaneCommand("sim/FMS2/crz", "FMS2: CRZ"); } }
        public static XPlaneCommand FMS2Des { get { return GetXPlaneCommand("sim/FMS2/des", "FMS2: DES"); } }
        public static XPlaneCommand FMS2DirIntc { get { return GetXPlaneCommand("sim/FMS2/dir_intc", "FMS2: DIR/INTC"); } }
        public static XPlaneCommand FMS2Legs { get { return GetXPlaneCommand("sim/FMS2/legs", "FMS2: FPLN"); } }
        public static XPlaneCommand FMS2DepArr { get { return GetXPlaneCommand("sim/FMS2/dep_arr", "FMS2: DEP/ARR"); } }
        public static XPlaneCommand FMS2Hold { get { return GetXPlaneCommand("sim/FMS2/hold", "FMS2: HOLD"); } }
        public static XPlaneCommand FMS2Prog { get { return GetXPlaneCommand("sim/FMS2/prog", "FMS2: PROG"); } }
        public static XPlaneCommand FMS2Exec { get { return GetXPlaneCommand("sim/FMS2/exec", "FMS2: EXEC"); } }
        public static XPlaneCommand FMS2Fix { get { return GetXPlaneCommand("sim/FMS2/fix", "FMS2: FIX"); } }
        public static XPlaneCommand FMS2Navrad { get { return GetXPlaneCommand("sim/FMS2/navrad", "FMS2: NAVRAD"); } }
        public static XPlaneCommand FMS2Prev { get { return GetXPlaneCommand("sim/FMS2/prev", "FMS2: prev"); } }
        public static XPlaneCommand FMS2Next { get { return GetXPlaneCommand("sim/FMS2/next", "FMS2: next"); } }
        public static XPlaneCommand FMS2Key0 { get { return GetXPlaneCommand("sim/FMS2/key_0", "FMS2: key_0"); } }
        public static XPlaneCommand FMS2Key1 { get { return GetXPlaneCommand("sim/FMS2/key_1", "FMS2: key_1"); } }
        public static XPlaneCommand FMS2Key2 { get { return GetXPlaneCommand("sim/FMS2/key_2", "FMS2: key_2"); } }
        public static XPlaneCommand FMS2Key3 { get { return GetXPlaneCommand("sim/FMS2/key_3", "FMS2: key_3"); } }
        public static XPlaneCommand FMS2Key4 { get { return GetXPlaneCommand("sim/FMS2/key_4", "FMS2: key_4"); } }
        public static XPlaneCommand FMS2Key5 { get { return GetXPlaneCommand("sim/FMS2/key_5", "FMS2: key_5"); } }
        public static XPlaneCommand FMS2Key6 { get { return GetXPlaneCommand("sim/FMS2/key_6", "FMS2: key_6"); } }
        public static XPlaneCommand FMS2Key7 { get { return GetXPlaneCommand("sim/FMS2/key_7", "FMS2: key_7"); } }
        public static XPlaneCommand FMS2Key8 { get { return GetXPlaneCommand("sim/FMS2/key_8", "FMS2: key_8"); } }
        public static XPlaneCommand FMS2Key9 { get { return GetXPlaneCommand("sim/FMS2/key_9", "FMS2: key_9"); } }
        public static XPlaneCommand FMS2KeyA { get { return GetXPlaneCommand("sim/FMS2/key_A", "FMS2: key_A"); } }
        public static XPlaneCommand FMS2KeyB { get { return GetXPlaneCommand("sim/FMS2/key_B", "FMS2: key_B"); } }
        public static XPlaneCommand FMS2KeyC { get { return GetXPlaneCommand("sim/FMS2/key_C", "FMS2: key_C"); } }
        public static XPlaneCommand FMS2KeyD { get { return GetXPlaneCommand("sim/FMS2/key_D", "FMS2: key_D"); } }
        public static XPlaneCommand FMS2KeyE { get { return GetXPlaneCommand("sim/FMS2/key_E", "FMS2: key_E"); } }
        public static XPlaneCommand FMS2KeyF { get { return GetXPlaneCommand("sim/FMS2/key_F", "FMS2: key_F"); } }
        public static XPlaneCommand FMS2KeyG { get { return GetXPlaneCommand("sim/FMS2/key_G", "FMS2: key_G"); } }
        public static XPlaneCommand FMS2KeyH { get { return GetXPlaneCommand("sim/FMS2/key_H", "FMS2: key_H"); } }
        public static XPlaneCommand FMS2KeyI { get { return GetXPlaneCommand("sim/FMS2/key_I", "FMS2: key_I"); } }
        public static XPlaneCommand FMS2KeyJ { get { return GetXPlaneCommand("sim/FMS2/key_J", "FMS2: key_J"); } }
        public static XPlaneCommand FMS2KeyK { get { return GetXPlaneCommand("sim/FMS2/key_K", "FMS2: key_K"); } }
        public static XPlaneCommand FMS2KeyL { get { return GetXPlaneCommand("sim/FMS2/key_L", "FMS2: key_L"); } }
        public static XPlaneCommand FMS2KeyM { get { return GetXPlaneCommand("sim/FMS2/key_M", "FMS2: key_M"); } }
        public static XPlaneCommand FMS2KeyN { get { return GetXPlaneCommand("sim/FMS2/key_N", "FMS2: key_N"); } }
        public static XPlaneCommand FMS2KeyO { get { return GetXPlaneCommand("sim/FMS2/key_O", "FMS2: key_O"); } }
        public static XPlaneCommand FMS2KeyP { get { return GetXPlaneCommand("sim/FMS2/key_P", "FMS2: key_P"); } }
        public static XPlaneCommand FMS2KeyQ { get { return GetXPlaneCommand("sim/FMS2/key_Q", "FMS2: key_Q"); } }
        public static XPlaneCommand FMS2KeyR { get { return GetXPlaneCommand("sim/FMS2/key_R", "FMS2: key_R"); } }
        public static XPlaneCommand FMS2KeyS { get { return GetXPlaneCommand("sim/FMS2/key_S", "FMS2: key_S"); } }
        public static XPlaneCommand FMS2KeyT { get { return GetXPlaneCommand("sim/FMS2/key_T", "FMS2: key_T"); } }
        public static XPlaneCommand FMS2KeyU { get { return GetXPlaneCommand("sim/FMS2/key_U", "FMS2: key_U"); } }
        public static XPlaneCommand FMS2KeyV { get { return GetXPlaneCommand("sim/FMS2/key_V", "FMS2: key_V"); } }
        public static XPlaneCommand FMS2KeyW { get { return GetXPlaneCommand("sim/FMS2/key_W", "FMS2: key_W"); } }
        public static XPlaneCommand FMS2KeyX { get { return GetXPlaneCommand("sim/FMS2/key_X", "FMS2: key_X"); } }
        public static XPlaneCommand FMS2KeyY { get { return GetXPlaneCommand("sim/FMS2/key_Y", "FMS2: key_Y"); } }
        public static XPlaneCommand FMS2KeyZ { get { return GetXPlaneCommand("sim/FMS2/key_Z", "FMS2: key_Z"); } }
        public static XPlaneCommand FMS2KeyPeriod { get { return GetXPlaneCommand("sim/FMS2/key_period", "FMS2: key_period"); } }
        public static XPlaneCommand FMS2KeyMinus { get { return GetXPlaneCommand("sim/FMS2/key_minus", "FMS2: key_minus"); } }
        public static XPlaneCommand FMS2KeySlash { get { return GetXPlaneCommand("sim/FMS2/key_slash", "FMS2: key_slash"); } }
        public static XPlaneCommand FMS2KeyBack { get { return GetXPlaneCommand("sim/FMS2/key_back", "FMS2: key_back"); } }
        public static XPlaneCommand FMS2KeySpace { get { return GetXPlaneCommand("sim/FMS2/key_space", "FMS2: key_space"); } }
        public static XPlaneCommand FMS2KeyDelete { get { return GetXPlaneCommand("sim/FMS2/key_delete", "FMS2: key_delete"); } }
        public static XPlaneCommand FMS2KeyClear { get { return GetXPlaneCommand("sim/FMS2/key_clear", "FMS2: key_clear"); } }
        public static XPlaneCommand FMS2CDUPopout { get { return GetXPlaneCommand("sim/FMS2/CDU_popout", "FMS2: pop out CDU window"); } }
        public static XPlaneCommand FMS2CDUPopup { get { return GetXPlaneCommand("sim/FMS2/CDU_popup", "FMS2: CDU popup"); } }
    }
}