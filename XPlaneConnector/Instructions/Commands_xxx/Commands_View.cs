﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        public static XPlaneCommand ViewTrackIrToggle { get { return GetXPlaneCommand("sim/view/track-ir_toggle", "Toggle TrackIR control."); } }
        public static XPlaneCommand ViewAiControlsViews { get { return GetXPlaneCommand("sim/view/ai_controls_views", "Toggle AI controls your views."); } }
        public static XPlaneCommand ViewFreeCamera { get { return GetXPlaneCommand("sim/view/free_camera", "Free camera."); } }
        public static XPlaneCommand ViewDefaultView { get { return GetXPlaneCommand("sim/view/default_view", "Default view."); } }
        public static XPlaneCommand ViewForwardWith2dPanel { get { return GetXPlaneCommand("sim/view/forward_with_2d_panel", "Forward with 2-D panel."); } }
        public static XPlaneCommand ViewForwardWithHud { get { return GetXPlaneCommand("sim/view/forward_with_hud", "Forward with HUD."); } }
        public static XPlaneCommand ViewForwardWithNothing { get { return GetXPlaneCommand("sim/view/forward_with_nothing", "Forward with nothing."); } }
        public static XPlaneCommand ViewLinearSpot { get { return GetXPlaneCommand("sim/view/linear_spot", "Linear spot."); } }
        public static XPlaneCommand ViewStillSpot { get { return GetXPlaneCommand("sim/view/still_spot", "Still spot."); } }
        public static XPlaneCommand ViewRunway { get { return GetXPlaneCommand("sim/view/runway", "Runway."); } }
        public static XPlaneCommand ViewCircle { get { return GetXPlaneCommand("sim/view/circle", "Circle."); } }
        public static XPlaneCommand ViewTower { get { return GetXPlaneCommand("sim/view/tower", "Tower."); } }
        public static XPlaneCommand ViewRidealong { get { return GetXPlaneCommand("sim/view/ridealong", "Ride-along."); } }
        public static XPlaneCommand ViewTrackWeapon { get { return GetXPlaneCommand("sim/view/track_weapon", "Track weapon."); } }
        public static XPlaneCommand ViewChase { get { return GetXPlaneCommand("sim/view/chase", "Chase."); } }
        public static XPlaneCommand View3dCockpitCmndLook { get { return GetXPlaneCommand("sim/view/3d_cockpit_cmnd_look", "3-D cockpit."); } }
        public static XPlaneCommand View3dCockpitToggle { get { return GetXPlaneCommand("sim/view/3d_cockpit_toggle", "Toggle between 2-D and 3-D cockpit."); } }
        public static XPlaneCommand ViewLockGeo { get { return GetXPlaneCommand("sim/view/lock_geo", "Lock onto current location."); } }
        public static XPlaneCommand ViewCinemaVerite { get { return GetXPlaneCommand("sim/view/cinema_verite", "Cinema verite."); } }
        public static XPlaneCommand ViewSunglasses { get { return GetXPlaneCommand("sim/view/sunglasses", "Sunglasses."); } }
        public static XPlaneCommand ViewNightVision { get { return GetXPlaneCommand("sim/view/night_vision", "Night vision."); } }
        public static XPlaneCommand ViewFlashlightRed { get { return GetXPlaneCommand("sim/view/flashlight_red", "Toggle the red flashlight."); } }
        public static XPlaneCommand ViewFlashlightWht { get { return GetXPlaneCommand("sim/view/flashlight_wht", "Toggle the white flashlight."); } }
        public static XPlaneCommand ViewGlanceLeft { get { return GetXPlaneCommand("sim/view/glance_left", "Glance left."); } }
        public static XPlaneCommand ViewGlanceRight { get { return GetXPlaneCommand("sim/view/glance_right", "Glance right."); } }
        public static XPlaneCommand ViewUpLeft { get { return GetXPlaneCommand("sim/view/up_left", "Glance up and left."); } }
        public static XPlaneCommand ViewUpRight { get { return GetXPlaneCommand("sim/view/up_right", "Glance up and right."); } }
        public static XPlaneCommand ViewStraightUp { get { return GetXPlaneCommand("sim/view/straight_up", "Glance straight up."); } }
        public static XPlaneCommand ViewStraightDown { get { return GetXPlaneCommand("sim/view/straight_down", "Glance straight down."); } }
        public static XPlaneCommand ViewLeft45 { get { return GetXPlaneCommand("sim/view/left_45", "Glance 45 degrees left."); } }
        public static XPlaneCommand ViewRight45 { get { return GetXPlaneCommand("sim/view/right_45", "Glance 45 degrees right."); } }
        public static XPlaneCommand ViewLeft90 { get { return GetXPlaneCommand("sim/view/left_90", "Glance 90 degrees left."); } }
        public static XPlaneCommand ViewRight90 { get { return GetXPlaneCommand("sim/view/right_90", "Glance 90 degrees right."); } }
        public static XPlaneCommand ViewLeft135 { get { return GetXPlaneCommand("sim/view/left_135", "Glance 135 degrees left."); } }
        public static XPlaneCommand ViewRight135 { get { return GetXPlaneCommand("sim/view/right_135", "Glance 135 degrees right."); } }
        public static XPlaneCommand ViewBack { get { return GetXPlaneCommand("sim/view/back", "Glance backward."); } }
        public static XPlaneCommand View3dPathToggle { get { return GetXPlaneCommand("sim/view/3d_path_toggle", "3-D path toggle."); } }
        public static XPlaneCommand View3dPathReset { get { return GetXPlaneCommand("sim/view/3d_path_reset", "3-D path reset."); } }
        public static XPlaneCommand ViewShowPhysicsModel { get { return GetXPlaneCommand("sim/view/show_physics_model", "Toggle physics model visualization."); } }
        public static XPlaneCommand ViewMouseClickRegionsToggle { get { return GetXPlaneCommand("sim/view/mouse_click_regions_toggle", "Toggle visualization of clickable cockpit areas."); } }
        public static XPlaneCommand ViewInstrumentDescriptionsToggle { get { return GetXPlaneCommand("sim/view/instrument_descriptions_toggle", "Toggle instrument descriptions on hover."); } }
        public static XPlaneCommand ViewQuickLook0 { get { return GetXPlaneCommand("sim/view/quick_look_0", "Go to save 3-D cockpit location #1."); } }
        public static XPlaneCommand ViewQuickLook1 { get { return GetXPlaneCommand("sim/view/quick_look_1", "Go to save 3-D cockpit location #2."); } }
        public static XPlaneCommand ViewQuickLook2 { get { return GetXPlaneCommand("sim/view/quick_look_2", "Go to save 3-D cockpit location #3."); } }
        public static XPlaneCommand ViewQuickLook3 { get { return GetXPlaneCommand("sim/view/quick_look_3", "Go to save 3-D cockpit location #4."); } }
        public static XPlaneCommand ViewQuickLook4 { get { return GetXPlaneCommand("sim/view/quick_look_4", "Go to save 3-D cockpit location #5."); } }
        public static XPlaneCommand ViewQuickLook5 { get { return GetXPlaneCommand("sim/view/quick_look_5", "Go to save 3-D cockpit location #6."); } }
        public static XPlaneCommand ViewQuickLook6 { get { return GetXPlaneCommand("sim/view/quick_look_6", "Go to save 3-D cockpit location #7."); } }
        public static XPlaneCommand ViewQuickLook7 { get { return GetXPlaneCommand("sim/view/quick_look_7", "Go to save 3-D cockpit location #8."); } }
        public static XPlaneCommand ViewQuickLook8 { get { return GetXPlaneCommand("sim/view/quick_look_8", "Go to save 3-D cockpit location #9."); } }
        public static XPlaneCommand ViewQuickLook9 { get { return GetXPlaneCommand("sim/view/quick_look_9", "Go to save 3-D cockpit location #10."); } }
        public static XPlaneCommand ViewQuickLook10 { get { return GetXPlaneCommand("sim/view/quick_look_10", "Go to save 3-D cockpit location #11."); } }
        public static XPlaneCommand ViewQuickLook11 { get { return GetXPlaneCommand("sim/view/quick_look_11", "Go to save 3-D cockpit location #12."); } }
        public static XPlaneCommand ViewQuickLook12 { get { return GetXPlaneCommand("sim/view/quick_look_12", "Go to save 3-D cockpit location #13."); } }
        public static XPlaneCommand ViewQuickLook13 { get { return GetXPlaneCommand("sim/view/quick_look_13", "Go to save 3-D cockpit location #14."); } }
        public static XPlaneCommand ViewQuickLook14 { get { return GetXPlaneCommand("sim/view/quick_look_14", "Go to save 3-D cockpit location #15."); } }
        public static XPlaneCommand ViewQuickLook15 { get { return GetXPlaneCommand("sim/view/quick_look_15", "Go to save 3-D cockpit location #16."); } }
        public static XPlaneCommand ViewQuickLook16 { get { return GetXPlaneCommand("sim/view/quick_look_16", "Go to save 3-D cockpit location #17."); } }
        public static XPlaneCommand ViewQuickLook17 { get { return GetXPlaneCommand("sim/view/quick_look_17", "Go to save 3-D cockpit location #18."); } }
        public static XPlaneCommand ViewQuickLook18 { get { return GetXPlaneCommand("sim/view/quick_look_18", "Go to save 3-D cockpit location #19."); } }
        public static XPlaneCommand ViewQuickLook19 { get { return GetXPlaneCommand("sim/view/quick_look_19", "Go to save 3-D cockpit location #20."); } }
        public static XPlaneCommand ViewQuickLook0Mem { get { return GetXPlaneCommand("sim/view/quick_look_0_mem", "Memorize 3-D cockpit location #1."); } }
        public static XPlaneCommand ViewQuickLook1Mem { get { return GetXPlaneCommand("sim/view/quick_look_1_mem", "Memorize 3-D cockpit location #2."); } }
        public static XPlaneCommand ViewQuickLook2Mem { get { return GetXPlaneCommand("sim/view/quick_look_2_mem", "Memorize 3-D cockpit location #3."); } }
        public static XPlaneCommand ViewQuickLook3Mem { get { return GetXPlaneCommand("sim/view/quick_look_3_mem", "Memorize 3-D cockpit location #4."); } }
        public static XPlaneCommand ViewQuickLook4Mem { get { return GetXPlaneCommand("sim/view/quick_look_4_mem", "Memorize 3-D cockpit location #5."); } }
        public static XPlaneCommand ViewQuickLook5Mem { get { return GetXPlaneCommand("sim/view/quick_look_5_mem", "Memorize 3-D cockpit location #6."); } }
        public static XPlaneCommand ViewQuickLook6Mem { get { return GetXPlaneCommand("sim/view/quick_look_6_mem", "Memorize 3-D cockpit location #7."); } }
        public static XPlaneCommand ViewQuickLook7Mem { get { return GetXPlaneCommand("sim/view/quick_look_7_mem", "Memorize 3-D cockpit location #8."); } }
        public static XPlaneCommand ViewQuickLook8Mem { get { return GetXPlaneCommand("sim/view/quick_look_8_mem", "Memorize 3-D cockpit location #9."); } }
        public static XPlaneCommand ViewQuickLook9Mem { get { return GetXPlaneCommand("sim/view/quick_look_9_mem", "Memorize 3-D cockpit location #10."); } }
        public static XPlaneCommand ViewQuickLook10Mem { get { return GetXPlaneCommand("sim/view/quick_look_10_mem", "Memorize 3-D cockpit location #11."); } }
        public static XPlaneCommand ViewQuickLook11Mem { get { return GetXPlaneCommand("sim/view/quick_look_11_mem", "Memorize 3-D cockpit location #12."); } }
        public static XPlaneCommand ViewQuickLook12Mem { get { return GetXPlaneCommand("sim/view/quick_look_12_mem", "Memorize 3-D cockpit location #13."); } }
        public static XPlaneCommand ViewQuickLook13Mem { get { return GetXPlaneCommand("sim/view/quick_look_13_mem", "Memorize 3-D cockpit location #14."); } }
        public static XPlaneCommand ViewQuickLook14Mem { get { return GetXPlaneCommand("sim/view/quick_look_14_mem", "Memorize 3-D cockpit location #15."); } }
        public static XPlaneCommand ViewQuickLook15Mem { get { return GetXPlaneCommand("sim/view/quick_look_15_mem", "Memorize 3-D cockpit location #16."); } }
        public static XPlaneCommand ViewQuickLook16Mem { get { return GetXPlaneCommand("sim/view/quick_look_16_mem", "Memorize 3-D cockpit location #17."); } }
        public static XPlaneCommand ViewQuickLook17Mem { get { return GetXPlaneCommand("sim/view/quick_look_17_mem", "Memorize 3-D cockpit location #18."); } }
        public static XPlaneCommand ViewQuickLook18Mem { get { return GetXPlaneCommand("sim/view/quick_look_18_mem", "Memorize 3-D cockpit location #19."); } }
        public static XPlaneCommand ViewQuickLook19Mem { get { return GetXPlaneCommand("sim/view/quick_look_19_mem", "Memorize 3-D cockpit location #20."); } }
    }
}