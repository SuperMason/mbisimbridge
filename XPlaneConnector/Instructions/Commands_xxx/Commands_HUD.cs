﻿namespace XPlaneConnector.Instructions
{
    public sealed partial class Commands
    {
        /// <summary>
        /// HUD toggle power.
        /// </summary>
        public static XPlaneCommand HUDPowerToggle { get { return GetXPlaneCommand("sim/HUD/power_toggle", "HUD toggle power."); } }
        /// <summary>
        /// HUD toggle brightness.
        /// </summary>
        public static XPlaneCommand HUDBrightnessToggle { get { return GetXPlaneCommand("sim/HUD/brightness_toggle", "HUD toggle brightness."); } }
    }
}