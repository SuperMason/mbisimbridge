﻿using Newtonsoft.Json;
using System.Collections.Generic;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public class SelfParamsDefinition
    {
        [JsonProperty("SelfDataRefs")]
        public List<FilterSelfDR> SelfDataRefs { get; set; }

        [JsonProperty("SelfCommands")]
        public List<XPlaneCommand> SelfCommands { get; set; }

    }
}