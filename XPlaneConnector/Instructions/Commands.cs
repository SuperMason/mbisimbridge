﻿using MBILibrary.Util;
using System.Collections.Generic;
using System.Reflection;
using XPlaneConnector.DataSwitch.MbiEvtInterfaces;

namespace XPlaneConnector.Instructions
{
    /// <summary>
    /// X-Plane Command 指令集
    /// </summary>
    public sealed partial class Commands
    {
        /// <summary>
        /// Global Event Interface for subscribing Event Handler
        /// </summary>
        public static EvtInterfaceService StaticEvtInterface { get; set; }

        /// <summary>
        /// 取得必要的 Commands
        /// </summary>
        /// <returns></returns>
        public static List<XPlaneCommand> GetMustNeedCommands() {
            return new List<XPlaneCommand>() {
                NoneNone,
                OperationQuit,
                OperationScreenshot,
                OperationShowMenu,
                ElectricalBattery1On,
                ElectricalBattery2On,
                ElectricalBattery1Off,
                ElectricalBattery2Off,
                ElectricalBattery1Toggle,
                ElectricalBattery2Toggle,
                HUDPowerToggle,
                HUDBrightnessToggle,
                FlightControlsLandingGearDown,
                FlightControlsLandingGearUp
            };
        }

        /// <summary>
        /// 訂閱全部的 Commands, 共 2303 個, 壓力測試先用 2000 個測試
        /// </summary>
        public static List<XPlaneCommand> GetAllCommands() {
            var outList = new List<XPlaneCommand>();
            var allStaticProperties = GetAllStaticProperties();
            int cnt = 0;
            foreach (var propInfo in allStaticProperties) {
                if (cnt == UtilConstants.MaxSubscribeCNT) // 超過 2000 個 C++ 會出問題, 所以先弄 2000 個測試
                { UtilGlobalFunc.Log($"'Command' Subscribing Counter ::: {cnt}"); break; }
                ++cnt;

                object propObj = propInfo.GetValue(null, null);
                if (propObj.GetType() == typeof(XPlaneCommand))
                { outList.Add((XPlaneCommand)propObj); }
                else { --cnt; }
            }

            return outList;
        }

        #region Function()
        /// <summary>
        /// 取得所有 X-Plane 的 Command 定義
        /// </summary>
        /// <returns></returns>
        private static PropertyInfo[] GetAllStaticProperties()
        { return typeof(Commands).GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy); }
        /// <summary>
        /// 取得 XPlaneCommand
        /// </summary>
        public static XPlaneCommand GetXPlaneCommand(string name, string description) {
            return new XPlaneCommand {
                Name = name,
                Description = description,
            };
        }
        #endregion


        #region Commands
        /// <summary>
        /// Do nothing.
        /// </summary>
        public static XPlaneCommand NoneNone { get { return GetXPlaneCommand("sim/none/none", "Do nothing."); } }
        #endregion
    }
}