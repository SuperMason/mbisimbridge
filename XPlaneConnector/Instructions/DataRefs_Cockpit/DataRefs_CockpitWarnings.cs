﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Time master caution will go out.  (Use command-system instead of this dataref.)
        /// </summary>
        public static DataRefElement CockpitWarningsMasterCautionTimeout
        { get { return GetDataRefElement("sim/cockpit/warnings/master_caution_timeout", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Time master caution will go out.  (Use command-system instead of this dataref.)"); } }
        /// <summary>
        /// Master caution is enabled for being lit (because it is not pressed)
        /// </summary>
        public static DataRefElement CockpitWarningsMasterCautionOn
        { get { return GetDataRefElement("sim/cockpit/warnings/master_caution_on", TypeStart_float, UtilConstants.Flag_Yes, "boolean", "Master caution is enabled for being lit (because it is not pressed)"); } }
        /// <summary>
        /// Master warning is enabled for being lit (because it is not pressed)
        /// </summary>
        public static DataRefElement CockpitWarningsMasterWarningOn
        { get { return GetDataRefElement("sim/cockpit/warnings/master_warning_on", TypeStart_float, UtilConstants.Flag_Yes, "boolean", "Master warning is enabled for being lit (because it is not pressed)"); } }
        /// <summary>
        /// Master accept is enabled for being lit (because it is not pressed)
        /// </summary>
        public static DataRefElement CockpitWarningsMasterAcceptOn
        { get { return GetDataRefElement("sim/cockpit/warnings/master_accept_on", TypeStart_float, UtilConstants.Flag_Yes, "boolean", "Master accept is enabled for being lit (because it is not pressed)"); } }
        /// <summary>
        /// Time annunciator test will end (use annunciator_test_pressed instead)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorTestTimeout
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciator_test_timeout", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Time annunciator test will end (use annunciator_test_pressed instead)"); } }
        /// <summary>
        /// True if the annunciator test button is pressed now.
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorTestPressed
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciator_test_pressed", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if the annunciator test button is pressed now."); } }
        /// <summary>
        /// The autopilot is beeping as part of its self-test
        /// </summary>
        public static DataRefElement CockpitWarningsAutopilotTestBeeping
        { get { return GetDataRefElement("sim/cockpit/warnings/autopilot_test_beeping", TypeStart_int, UtilConstants.Flag_No, "boolean", "The autopilot is beeping as part of its self-test"); } }
        /// <summary>
        /// The autopilot mode lights are on as part of its self test
        /// </summary>
        public static DataRefElement CockpitWarningsAutopilotTestModesLit
        { get { return GetDataRefElement("sim/cockpit/warnings/autopilot_test_modes_lit", TypeStart_int, UtilConstants.Flag_No, "boolean", "The autopilot mode lights are on as part of its self test"); } }
        /// <summary>
        /// The autopilot trim lights are on as part of its self test
        /// </summary>
        public static DataRefElement CockpitWarningsAutopilotTestTrimLit
        { get { return GetDataRefElement("sim/cockpit/warnings/autopilot_test_trim_lit", TypeStart_int, UtilConstants.Flag_No, "boolean", "The autopilot trim lights are on as part of its self test"); } }
        /// <summary>
        /// The autopilot engaged lights are on as part of their self test
        /// </summary>
        public static DataRefElement CockpitWarningsAutopilotTestApLit
        { get { return GetDataRefElement("sim/cockpit/warnings/autopilot_test_ap_lit", TypeStart_int, UtilConstants.Flag_No, "boolean", "The autopilot engaged lights are on as part of their self test"); } }
        /// <summary>
        /// Master caution light on/off
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsMasterCaution
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/master_caution", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Master caution light on/off"); } }
        /// <summary>
        /// Master warning
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsMasterWarning
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/master_warning", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Master warning"); } }
        /// <summary>
        /// Master accept light on/off
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsMasterAccept
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/master_accept", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Master accept light on/off"); } }
        /// <summary>
        /// autopilot has been disconnected
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotDisconnect
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_disconnect", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot has been disconnected"); } }
        /// <summary>
        /// low vacuum pressure
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsLowVacuum
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/low_vacuum", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "low vacuum pressure"); } }
        /// <summary>
        /// low battery voltage
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsLowVoltage
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/low_voltage", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "low battery voltage"); } }
        /// <summary>
        /// running out of fuel
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsFuelQuantity
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/fuel_quantity", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "running out of fuel"); } }
        /// <summary>
        /// hydraulic pressure low
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsHydraulicPressure
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/hydraulic_pressure", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "hydraulic pressure low"); } }
        /// <summary>
        /// speedbrakes deployed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsSpeedbrake
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/speedbrake", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "speedbrakes deployed"); } }
        /// <summary>
        /// GPWS failed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsGPWS
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/GPWS", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "GPWS failed"); } }
        /// <summary>
        /// ice detected
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsIce
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/ice", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "ice detected"); } }
        /// <summary>
        /// low rotor speed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsLoRotor
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/lo_rotor", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "low rotor speed"); } }
        /// <summary>
        /// high rotor speed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsHiRotor
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/hi_rotor", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "high rotor speed"); } }
        /// <summary>
        /// pitot heat off
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsPitotHeatOff
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/pitot_heat_off", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "pitot heat off"); } }
        /// <summary>
        /// transonic
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsTransonic
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/transonic", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "transonic"); } }
        /// <summary>
        /// slats deployed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsSlats
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/slats", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "slats deployed"); } }
        /// <summary>
        /// flight director failure
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsFlightDirector
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/flight_director", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "flight director failure"); } }
        /// <summary>
        /// autopilot failure
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilot
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot failure"); } }
        /// <summary>
        /// yaw damper failure
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsYawDamper
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/yaw_damper", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "yaw damper failure"); } }
        /// <summary>
        /// fuel pressure low - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsFuelPressureLow
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/fuel_pressure_low", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "fuel pressure low - per engine"); } }
        /// <summary>
        /// fuel pressure low - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsOilPressureLow
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/oil_pressure_low", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "fuel pressure low - per engine"); } }
        /// <summary>
        /// oil temperature high - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsOilTemperatureHigh
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/oil_temperature_high", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "oil temperature high - per engine"); } }
        /// <summary>
        /// generator off - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsGeneratorOff
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/generator_off", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "generator off - per engine"); } }
        /// <summary>
        /// chip detected - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsChipDetected
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/chip_detected", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "chip detected - per engine"); } }
        /// <summary>
        /// engine fire - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsEngineFires
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/engine_fires", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "engine fire - per engine"); } }
        /// <summary>
        /// igniter on - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsIgniterOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/igniter_on", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "igniter on - per engine"); } }
        /// <summary>
        /// reverser on - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsReverserOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/reverser_on", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "reverser on - per engine"); } }
        /// <summary>
        /// burner on - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsBurnerOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/burner_on", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "burner on - per engine"); } }
        /// <summary>
        /// inverter off - per 2 inverters
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsInverterOff
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/inverter_off", TypeStart_int2, UtilConstants.Flag_Yes, "boolean", "inverter off - per 2 inverters"); } }
        /// <summary>
        /// N1 of engine is too low for AC - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsN1Low
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/N1_low", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "N1 of engine is too low for AC - per engine"); } }
        /// <summary>
        /// N1 too high - per engine
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsN1High
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/N1_high", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "N1 too high - per engine"); } }
        /// <summary>
        /// reversers not ready
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsReverserNotReady
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/reverser_not_ready", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "reversers not ready"); } }
        /// <summary>
        /// ice vain extended (per engine)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsIceVaneExtend
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/ice_vane_extend", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "ice vain extended (per engine)"); } }
        /// <summary>
        /// ice vain failed (per engine)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsIceVaneFail
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/ice_vane_fail", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "ice vain failed (per engine)"); } }
        /// <summary>
        /// bleed air off (per engine)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsBleedAirOff
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/bleed_air_off", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "bleed air off (per engine)"); } }
        /// <summary>
        /// bleed air failed (per engine)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsBleedAirFail
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/bleed_air_fail", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "bleed air failed (per engine)"); } }
        /// <summary>
        /// auto feather armed (per engine)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutoFeatherArm
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/auto_feather_arm", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "auto feather armed (per engine)"); } }
        /// <summary>
        /// fuel transfer on (per tank)
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsFuelTransfer
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/fuel_transfer", TypeStart_int9, UtilConstants.Flag_Yes, "boolean", "fuel transfer on (per tank)"); } }
        /// <summary>
        /// duct overheated
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsHvac
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/hvac", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "duct overheated"); } }
        /// <summary>
        /// battery is charging too rapidly - may overheat
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsBatteryChargeHi
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/battery_charge_hi", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "battery is charging too rapidly - may overheat"); } }
        /// <summary>
        /// cabin altitude at or above 12500
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsCabinAltitude12500
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/cabin_altitude_12500", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "cabin altitude at or above 12500"); } }
        /// <summary>
        /// autopilot trim failure
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotTrimFail
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_trim_fail", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot trim failure"); } }
        /// <summary>
        /// electric trim is off
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsElectricTrimOff
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/electric_trim_off", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "electric trim is off"); } }
        /// <summary>
        /// crossfeed on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsCrossfeedOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/crossfeed_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "crossfeed on"); } }
        /// <summary>
        /// landing or taxiway light on but gear up
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsLandingTaxiLite
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/landing_taxi_lite", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "landing or taxiway light on but gear up"); } }
        /// <summary>
        /// cabin door is open
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsCabinDoorOpen
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/cabin_door_open", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "cabin door is open"); } }
        /// <summary>
        /// external power is on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsExternalPowerOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/external_power_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "external power is on"); } }
        /// <summary>
        /// passenger oxygen on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsPassengerOxyOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/passenger_oxy_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "passenger oxygen on"); } }
        /// <summary>
        /// gear is unsafe
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsGearUnsafe
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/gear_unsafe", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "gear is unsafe"); } }
        /// <summary>
        /// autopilot trimming down
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotTrimDown
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_trim_down", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot trimming down"); } }
        /// <summary>
        /// autopilot trimming up
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotTrimUp
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_trim_up", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot trimming up"); } }
        /// <summary>
        /// autopilot bank limit is turned ON, autopilot will keep bank below 12.5 degrees of bank
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotBankLimit
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_bank_limit", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot bank limit is turned ON, autopilot will keep bank below 12.5 degrees of bank"); } }
        /// <summary>
        /// autopilot soft ride is on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutopilotSoftRide
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/autopilot_soft_ride", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "autopilot soft ride is on"); } }
        /// <summary>
        /// no inverters are on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsNoInverters
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/no_inverters", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "no inverters are on"); } }
        /// <summary>
        /// glideslope deviation
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsGlideslope
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/glideslope", TypeStart_int, UtilConstants.Flag_No, "boolean", "glideslope deviation"); } }
        /// <summary>
        /// fuel pressure low
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsFuelPressure
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/fuel_pressure", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "fuel pressure low"); } }
        /// <summary>
        /// oil pressure low
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsOilPressure
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/oil_pressure", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "oil pressure low"); } }
        /// <summary>
        /// oil temperature too high
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsOilTemperature
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/oil_temperature", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "oil temperature too high"); } }
        /// <summary>
        /// generators are off or broken
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsGenerator
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/generator", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "generators are off or broken"); } }
        /// <summary>
        /// a chip has been detected in, um, a prop or turbine?
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsChipDetect
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/chip_detect", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "a chip has been detected in, um, a prop or turbine?"); } }
        /// <summary>
        /// yer engines are on fire, fer cryin out loud
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsEngineFire
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/engine_fire", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "yer engines are on fire, fer cryin out loud"); } }
        /// <summary>
        /// auto ignition ???
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAutoIgnition
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/auto_ignition", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "auto ignition ???"); } }
        /// <summary>
        /// reversers deployed
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsReverse
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/reverse", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "reversers deployed"); } }
        /// <summary>
        /// afterburners on
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsAfterburnersOn
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/afterburners_on", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "afterburners on"); } }
        /// <summary>
        /// inverters are off or broken
        /// </summary>
        public static DataRefElement CockpitWarningsAnnunciatorsInverter
        { get { return GetDataRefElement("sim/cockpit/warnings/annunciators/inverter", TypeStart_int, UtilConstants.Flag_Yes, "bitfield", "inverters are off or broken"); } }
    }
}
