﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpitWeaponsGunsArmed = null;
        /// <summary>
        /// Are guns armed
        /// </summary>
        public static DataRefElement CockpitWeaponsGunsArmed {
            get {
                if (cockpitWeaponsGunsArmed == null)
                { cockpitWeaponsGunsArmed = GetDataRefElement("sim/cockpit/weapons/guns_armed", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are guns armed"); }
                return cockpitWeaponsGunsArmed;
            }
        }

        private static DataRefElement cockpitWeaponsRocketsArmed = null;
        /// <summary>
        /// Are rockets armed
        /// </summary>
        public static DataRefElement CockpitWeaponsRocketsArmed {
            get {
                if (cockpitWeaponsRocketsArmed == null)
                { cockpitWeaponsRocketsArmed = GetDataRefElement("sim/cockpit/weapons/rockets_armed", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are rockets armed"); }
                return cockpitWeaponsRocketsArmed;
            }
        }

        private static DataRefElement cockpitWeaponsMissilesArmed = null;
        /// <summary>
        /// Are missiles armed
        /// </summary>
        public static DataRefElement CockpitWeaponsMissilesArmed {
            get {
                if (cockpitWeaponsMissilesArmed == null)
                { cockpitWeaponsMissilesArmed = GetDataRefElement("sim/cockpit/weapons/missiles_armed", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are missiles armed"); }
                return cockpitWeaponsMissilesArmed;
            }
        }

        private static DataRefElement cockpitWeaponsBombsArmed = null;
        /// <summary>
        /// Are bombs armed
        /// </summary>
        public static DataRefElement CockpitWeaponsBombsArmed {
            get {
                if (cockpitWeaponsBombsArmed == null)
                { cockpitWeaponsBombsArmed = GetDataRefElement("sim/cockpit/weapons/bombs_armed", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are bombs armed"); }
                return cockpitWeaponsBombsArmed;
            }
        }

        private static DataRefElement cockpitWeaponsFiringMode = null;
        /// <summary>
        /// Firing mode (single, ripple, etc. for bombs)
        /// </summary>
        public static DataRefElement CockpitWeaponsFiringMode {
            get {
                if (cockpitWeaponsFiringMode == null)
                { cockpitWeaponsFiringMode = GetDataRefElement("sim/cockpit/weapons/firing_mode", TypeStart_int, UtilConstants.Flag_Yes, "???", "Firing mode (single, ripple, etc. for bombs)"); }
                return cockpitWeaponsFiringMode;
            }
        }

        private static DataRefElement cockpitWeaponsFiringRate = null;
        /// <summary>
        /// Firing rate (for bombs)
        /// </summary>
        public static DataRefElement CockpitWeaponsFiringRate {
            get {
                if (cockpitWeaponsFiringRate == null)
                { cockpitWeaponsFiringRate = GetDataRefElement("sim/cockpit/weapons/firing_rate", TypeStart_int, UtilConstants.Flag_Yes, "???", "Firing rate (for bombs)"); }
                return cockpitWeaponsFiringRate;
            }
        }

        private static DataRefElement cockpitWeaponsPlaneTargetIndex = null;
        /// <summary>
        /// Index of plane that is being targeted
        /// </summary>
        public static DataRefElement CockpitWeaponsPlaneTargetIndex {
            get {
                if (cockpitWeaponsPlaneTargetIndex == null)
                { cockpitWeaponsPlaneTargetIndex = GetDataRefElement("sim/cockpit/weapons/plane_target_index", TypeStart_int, UtilConstants.Flag_Yes, "???", "Index of plane that is being targeted"); }
                return cockpitWeaponsPlaneTargetIndex;
            }
        }

        private static DataRefElement cockpitWeaponsChaffNow = null;
        /// <summary>
        /// Number of rounds left in the sum of all chaff weapons on aircraft. Missing in 11.00-11.05.  Writable in v10 and earlier.
        /// </summary>
        public static DataRefElement CockpitWeaponsChaffNow {
            get {
                if (cockpitWeaponsChaffNow == null)
                { cockpitWeaponsChaffNow = GetDataRefElement("sim/cockpit/weapons/chaff_now", TypeStart_int, UtilConstants.Flag_No, "count", "Number of rounds left in the sum of all chaff weapons on aircraft. Missing in 11.00-11.05.  Writable in v10 and earlier."); }
                return cockpitWeaponsChaffNow;
            }
        }

        private static DataRefElement cockpitWeaponsFlareNow = null;
        /// <summary>
        /// Number of rounds left in all flare weapons on the aircraft.  Missing in 11.00-11.05.  Writable in v10 and earlier.
        /// </summary>
        public static DataRefElement CockpitWeaponsFlareNow {
            get {
                if (cockpitWeaponsFlareNow == null)
                { cockpitWeaponsFlareNow = GetDataRefElement("sim/cockpit/weapons/flare_now", TypeStart_int, UtilConstants.Flag_No, "count", "Number of rounds left in all flare weapons on the aircraft.  Missing in 11.00-11.05.  Writable in v10 and earlier."); }
                return cockpitWeaponsFlareNow;
            }
        }

        private static DataRefElement cockpitWeaponsWpnSelConsole = null;
        /// <summary>
        /// INDIVIDUAL weapon selected
        /// </summary>
        public static DataRefElement CockpitWeaponsWpnSelConsole {
            get {
                if (cockpitWeaponsWpnSelConsole == null)
                { cockpitWeaponsWpnSelConsole = GetDataRefElement("sim/cockpit/weapons/wpn_sel_console", TypeStart_int, UtilConstants.Flag_Yes, "???", "INDIVIDUAL weapon selected"); }
                return cockpitWeaponsWpnSelConsole;
            }
        }
    }
}
