﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitGpsCourse
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gps/course",
                    Units = "???",
                    Description = "The currently programmed GPS course (true degrees)",
                    
                };
            }
        }
        public static DataRefElement CockpitGpsDestinationType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gps/destination_type",
                    Units = "???",
                    Description = "The current type of navaid we're flying to",
                    
                };
            }
        }
        public static DataRefElement CockpitGpsDestinationIndex
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gps/destination_index",
                    Units = "???",
                    Description = "The index of the navaid we're flying to",
                    
                };
            }
        }
    }
}
