﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitEngineInverterOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/inverter_on",
                    Units = "bool",
                    Description = "Is the inverter providing power (was one per engine, now 2 max.)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineInverterEq
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/inverter_eq",
                    Units = "bool",
                    Description = "Does this cockpit have inverter switches?",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineFuelPumpOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/fuel_pump_on",
                    Units = "bool",
                    Description = "Is the fuel pump on (one per engine)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineFadecOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/fadec_on",
                    Units = "bool",
                    Description = "Is the fadec on (one per engine)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineIdleSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/idle_speed",
                    Units = "bool",
                    Description = "Idle speed (per engine)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineFuelTankSelector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/fuel_tank_selector",
                    Units = "???",
                    Description = "Which fuel tank is open for flight",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineFuelTankTransfer
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/fuel_tank_transfer",
                    Units = "???",
                    Description = "Which fuel tank is open for transfers (destination)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineFuelTankTransferFrom
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/fuel_tank_transfer_from",
                    Units = "???",
                    Description = "Which fuel tank is open for transfers (source)",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineIgnitionOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/ignition_on",
                    Units = "enum",
                    Description = "ignition key position 0 = off, 1 = left, 2 = right, 3 = both",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineIgnitersOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/igniters_on",
                    Units = "boolean",
                    Description = "starter ignition",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineStarterDuration
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/starter_duration",
                    Units = "seconds",
                    Description = "time since ignition was pressed or something",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineClutchEngage
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/clutch_engage",
                    Units = "???",
                    Description = "Clutch engaged",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineAPUSwitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/APU_switch",
                    Units = "enum",
                    Description = "APU starter switch 0 = off, 1 = on, 2 = start",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineAPURunning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/APU_running",
                    Units = "boolean",
                    Description = "APU running - 1 = on, 0 = off.",
                    
                };
            }
        }
        public static DataRefElement CockpitEngineAPUN1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/engine/APU_N1",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
    }
}
