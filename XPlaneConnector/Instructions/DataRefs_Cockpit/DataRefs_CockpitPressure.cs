﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitPressureBleedAirOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/bleed_air_on",
                    Units = "bool",
                    Description = "Bleed air is on (legacy)",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureBleedAirMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/bleed_air_mode",
                    Units = "enum",
                    Description = "0=off,1=L,2=B,3=R,4=APU",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureCabinAltitudeSetMMsl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/cabin_altitude_set_m_msl",
                    Units = "???",
                    Description = "The desired cabin altitude (BEFORE 730 this was incorrectly int type)",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureCabinVviSetMMsec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/cabin_vvi_set_m_msec",
                    Units = "???",
                    Description = "The desired cabin altitude rate change (BEFORE 730 this was incorrectly int type)",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureCabinPressureDifferentialPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/cabin_pressure_differential_psi",
                    Units = "psi",
                    Description = "????",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureCabinAltitudeActualMMsl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/cabin_altitude_actual_m_msl",
                    Units = "???",
                    Description = "The real cabin altitude (BEFORE 730 this was incorrectly int type). Writeable with override_pressurization",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureCabinVviActualMMsec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/cabin_vvi_actual_m_msec",
                    Units = "???",
                    Description = "The real cabin altitude rate change (BEFORE 730 this was incorrectly int type). Writeable with override_pressurization",
                    
                };
            }
        }
        public static DataRefElement CockpitPressurePressureTestTimeout
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/pressure_test_timeout",
                    Units = "???",
                    Description = "Timeout for some kind of pressure test/",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureMaxAllowableAltitude
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/max_allowable_altitude",
                    Units = "???",
                    Description = "Max altitude the plane can keep pressure at?",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureDumpAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/dump_all",
                    Units = "bool",
                    Description = "Pressure dump switch",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureDumpToAlt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/dump_to_alt",
                    Units = "bool",
                    Description = "Pressure dump to altitude switch",
                    
                };
            }
        }
        public static DataRefElement CockpitPressureOutflowValve
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/pressure/outflow_valve",
                    Units = "ratio",
                    Description = "Pressurization outflow valve ratio. 0 for fully closed, 1 for fully open. Writeable with override_pressurization",
                    
                };
            }
        }
    }
}
