﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitAvidyneLftHil
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/lft_hil",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement CockpitAvidyneRgtHil
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/rgt_hil",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement CockpitAvidyneAltHil
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/alt_hil",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement CockpitAvidyneSrc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/src",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement CockpitAvidyneHsiMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/hsi_mode",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement CockpitAvidyneMapRangeSel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/avidyne/map_range_sel",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
    }
}
