﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitGyrosTheVacIndDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_vac_ind_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the first vacuum instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheEleIndDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ele_ind_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the second vacuum instrument - dataref name is worng!",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndDeg3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_deg3",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the first elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndDeg4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_deg4",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the second elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndVacPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_vac_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the pilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndVacCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_vac_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the copilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndElecPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_elec_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the pilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndElecCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_elec_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the copilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndAharsPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_ahars_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the pilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosTheIndAharsCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/the_ind_ahars_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated pitch on the panel for the copilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiVacIndDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_vac_ind_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated heading on the panel for the first vacuum instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiEleIndDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ele_ind_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated heading on the panel for the second vacuum instrument - dataref name is worng!",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndDegm3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_degm3",
                    Units = "degrees_magnetic",
                    Description = "The indicated heading on the panel for the first elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndDegm4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_degm4",
                    Units = "degrees_magnetic",
                    Description = "The indicated heading on the panel for the second elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndVacPilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_vac_pilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the pilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndVacCopilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_vac_copilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the copilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndElecPilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_elec_pilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the pilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndElecCopilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_elec_copilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the copilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndAharsPilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_ahars_pilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the pilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPsiIndAharsCopilotDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/psi_ind_ahars_copilot_degm",
                    Units = "degrees_magnetic",
                    Description = "The indicated magnetic heading on the panel for the copilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiVacIndDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_vac_ind_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the first vacuum instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiEleIndDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ele_ind_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the second vacuum instrument - dataref name is worng!",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndDeg3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_deg3",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the first elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndDeg4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_deg4",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the second elect instrument",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndVacPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_vac_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the pilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndVacCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_vac_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the copilot's side, vacuum driven",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndElecPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_elec_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the pilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndElecCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_elec_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the copilot's side, electric",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndAharsPilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_ahars_pilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the pilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosPhiIndAharsCopilotDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/phi_ind_ahars_copilot_deg",
                    Units = "degrees",
                    Description = "The indicated roll on the panel for the copilot's side, AHARS",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftVacDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_vac_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for vacuum 1 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftVac2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_vac2_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for vacuum 2 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftEleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_ele_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for electric 1 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftEle2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_ele2_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for electric 2 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftAharsDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_ahars_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for AHARS 1 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosDgDriftAhars2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/dg_drift_ahars2_deg",
                    Units = "degrees",
                    Description = "A delta between the plane's heading and the DG for AHARS 2 powered DGs",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosGyrForce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/gyr_force",
                    Units = "???",
                    Description = "was dim 4 until 920 enums are ahrs1,ahrs2,elec1,elec2,vac1,vac2",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosGyrSpin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/gyr_spin",
                    Units = "???",
                    Description = "was dim 4 until 920 enums are ahrs1,ahrs2,elec1,elec2,vac1,vac2",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosGyrFreeSlaved
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/gyr_free_slaved",
                    Units = "boolean",
                    Description = "free or slaved to magnetometer, the enums are ahrs1,ahrs2,elec1,elec2,vac1,vac2",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosGyrFlag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/gyr_flag",
                    Units = "boolean",
                    Description = "DG/HSI instrument for this gyro must show flag because gyro is not spinning, or magnetometer failure, or adjustment in progress",
                    
                };
            }
        }
        public static DataRefElement CockpitGyrosGyrCageRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/gyros/gyr_cage_ratio",
                    Units = "ratio",
                    Description = "attitude indicator for this gyro is caged. 0 for fully free, 1 for fully caged.",
                    
                };
            }
        }
    }
}
