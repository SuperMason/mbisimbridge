﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpitRadiosNav1FreqHz = null;
        /// <summary>
        /// The current frequency of the nav1 radio.
        /// </summary>
        public static DataRefElement CockpitRadiosNav1FreqHz {
            get {
                if (cockpitRadiosNav1FreqHz == null)
                { cockpitRadiosNav1FreqHz = GetDataRefElement("sim/cockpit/radios/nav1_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the nav1 radio."); }
                return cockpitRadiosNav1FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosNav2FreqHz = null;
        /// <summary>
        /// The current frequency of the nav2 radio.
        /// </summary>
        public static DataRefElement CockpitRadiosNav2FreqHz {
            get {
                if (cockpitRadiosNav2FreqHz == null)
                { cockpitRadiosNav2FreqHz = GetDataRefElement("sim/cockpit/radios/nav2_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the nav2 radio."); }
                return cockpitRadiosNav2FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosCom1FreqHz = null;
        /// <summary>
        /// The current frequency of the copm1 radio. (官網 is wrong. Instead of 'int' its a 'float' (IEE754), and the unit is not 10 Hz, but 10 kHz.)
        /// </summary>
        public static DataRefElement CockpitRadiosCom1FreqHz {
            get {
                if (cockpitRadiosCom1FreqHz == null)
                { cockpitRadiosCom1FreqHz = GetDataRefElement("sim/cockpit/radios/com1_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10kHz", "The current frequency of the copm1 radio. (官網 is wrong. Instead of 'int' its a 'float' (IEE754), and the unit is not 10 Hz, but 10 kHz.)"); }
                return cockpitRadiosCom1FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosCom2FreqHz = null;
        /// <summary>
        /// The current frequency of the com2 radio.
        /// </summary>
        public static DataRefElement CockpitRadiosCom2FreqHz {
            get {
                if (cockpitRadiosCom2FreqHz == null)
                { cockpitRadiosCom2FreqHz = GetDataRefElement("sim/cockpit/radios/com2_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the com2 radio."); }
                return cockpitRadiosCom2FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosAdf1FreqHz = null;
        /// <summary>
        /// The current frequency of the first automatic direction finder.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf1FreqHz {
            get {
                if (cockpitRadiosAdf1FreqHz == null)
                { cockpitRadiosAdf1FreqHz = GetDataRefElement("sim/cockpit/radios/adf1_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the first automatic direction finder."); }
                return cockpitRadiosAdf1FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosAdf2FreqHz = null;
        /// <summary>
        /// The current frequency of the second automatic direction finder.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf2FreqHz {
            get {
                if (cockpitRadiosAdf2FreqHz == null)
                { cockpitRadiosAdf2FreqHz = GetDataRefElement("sim/cockpit/radios/adf2_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the second automatic direction finder."); }
                return cockpitRadiosAdf2FreqHz;
            }
        }

        private static DataRefElement cockpitRadiosDmeFreqHz = null;
        /// <summary>
        /// The current frequency of the standalone DME receiver.
        /// </summary>
        public static DataRefElement CockpitRadiosDmeFreqHz {
            get {
                if (cockpitRadiosDmeFreqHz == null)
                { cockpitRadiosDmeFreqHz = GetDataRefElement("sim/cockpit/radios/dme_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The current frequency of the standalone DME receiver."); }
                return cockpitRadiosDmeFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosNav1StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosNav1StdbyFreqHz {
            get {
                if (cockpitRadiosNav1StdbyFreqHz == null)
                { cockpitRadiosNav1StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/nav1_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosNav1StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosNav2StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosNav2StdbyFreqHz {
            get {
                if (cockpitRadiosNav2StdbyFreqHz == null)
                { cockpitRadiosNav2StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/nav2_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosNav2StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosCom1StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosCom1StdbyFreqHz {
            get {
                if (cockpitRadiosCom1StdbyFreqHz == null)
                { cockpitRadiosCom1StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/com1_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosCom1StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosCom2StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosCom2StdbyFreqHz {
            get {
                if (cockpitRadiosCom2StdbyFreqHz == null)
                { cockpitRadiosCom2StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/com2_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosCom2StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosAdf1StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf1StdbyFreqHz {
            get {
                if (cockpitRadiosAdf1StdbyFreqHz == null)
                { cockpitRadiosAdf1StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/adf1_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosAdf1StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosAdf2StdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf2StdbyFreqHz {
            get {
                if (cockpitRadiosAdf2StdbyFreqHz == null)
                { cockpitRadiosAdf2StdbyFreqHz = GetDataRefElement("sim/cockpit/radios/adf2_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios."); }
                return cockpitRadiosAdf2StdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosDmeStdbyFreqHz = null;
        /// <summary>
        /// The standby frequency for the radio mentioned above for flip/flop radios.<p>NOTE: X-Plane does not currently feature a flip-flop standalone DME instrument, but the data exists.
        /// </summary>
        public static DataRefElement CockpitRadiosDmeStdbyFreqHz {
            get {
                if (cockpitRadiosDmeStdbyFreqHz == null)
                { cockpitRadiosDmeStdbyFreqHz = GetDataRefElement("sim/cockpit/radios/dme_stdby_freq_hz", TypeStart_int, UtilConstants.Flag_Yes, "10Hz", "The standby frequency for the radio mentioned above for flip/flop radios.<p>NOTE: X-Plane does not currently feature a flip-flop standalone DME instrument, but the data exists."); }
                return cockpitRadiosDmeStdbyFreqHz;
            }
        }

        private static DataRefElement cockpitRadiosNav1ObsDegt = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (true: legacy)
        /// </summary>
        public static DataRefElement CockpitRadiosNav1ObsDegt {
            get {
                if (cockpitRadiosNav1ObsDegt == null)
                { cockpitRadiosNav1ObsDegt = GetDataRefElement("sim/cockpit/radios/nav1_obs_degt", TypeStart_float, UtilConstants.Flag_Yes, "degt", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (true: legacy)"); }
                return cockpitRadiosNav1ObsDegt;
            }
        }

        private static DataRefElement cockpitRadiosNav2ObsDegt = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (true: legacy)
        /// </summary>
        public static DataRefElement CockpitRadiosNav2ObsDegt {
            get {
                if (cockpitRadiosNav2ObsDegt == null)
                { cockpitRadiosNav2ObsDegt = GetDataRefElement("sim/cockpit/radios/nav2_obs_degt", TypeStart_float, UtilConstants.Flag_Yes, "degt", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (true: legacy)"); }
                return cockpitRadiosNav2ObsDegt;
            }
        }

        private static DataRefElement cockpitRadiosNav1ObsDegm = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (mag: modern) pilot
        /// </summary>
        public static DataRefElement CockpitRadiosNav1ObsDegm {
            get {
                if (cockpitRadiosNav1ObsDegm == null)
                { cockpitRadiosNav1ObsDegm = GetDataRefElement("sim/cockpit/radios/nav1_obs_degm", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (mag: modern) pilot"); }
                return cockpitRadiosNav1ObsDegm;
            }
        }

        private static DataRefElement cockpitRadiosNav1ObsDegm2 = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (mag: modern) copilot
        /// </summary>
        public static DataRefElement CockpitRadiosNav1ObsDegm2 {
            get {
                if (cockpitRadiosNav1ObsDegm2 == null)
                { cockpitRadiosNav1ObsDegm2 = GetDataRefElement("sim/cockpit/radios/nav1_obs_degm2", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 1. (mag: modern) copilot"); }
                return cockpitRadiosNav1ObsDegm2;
            }
        }

        private static DataRefElement cockpitRadiosNav2ObsDegm = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (mag: modern) pilot
        /// </summary>
        public static DataRefElement CockpitRadiosNav2ObsDegm {
            get {
                if (cockpitRadiosNav2ObsDegm == null)
                { cockpitRadiosNav2ObsDegm = GetDataRefElement("sim/cockpit/radios/nav2_obs_degm", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (mag: modern) pilot"); }
                return cockpitRadiosNav2ObsDegm;
            }
        }

        private static DataRefElement cockpitRadiosNav2ObsDegm2 = null;
        /// <summary>
        /// The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (mag: modern) copilot
        /// </summary>
        public static DataRefElement CockpitRadiosNav2ObsDegm2 {
            get {
                if (cockpitRadiosNav2ObsDegm2 == null)
                { cockpitRadiosNav2ObsDegm2 = GetDataRefElement("sim/cockpit/radios/nav2_obs_degm2", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The 'obs' heading programmed into VOR and HSI gauges that follow nav radio 2. (mag: modern) copilot"); }
                return cockpitRadiosNav2ObsDegm2;
            }
        }

        private static DataRefElement cockpitRadiosNav1DirDegt = null;
        /// <summary>
        /// The relative bearing to the beacon indicated by nav1.  Set override with override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav1DirDegt {
            get {
                if (cockpitRadiosNav1DirDegt == null)
                { cockpitRadiosNav1DirDegt = GetDataRefElement("sim/cockpit/radios/nav1_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the beacon indicated by nav1.  Set override with override_navneedles"); }
                return cockpitRadiosNav1DirDegt;
            }
        }

        private static DataRefElement cockpitRadiosNav2DirDegt = null;
        /// <summary>
        /// The relative bearing to the beacon indicated by nav2.  Set override with override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav2DirDegt {
            get {
                if (cockpitRadiosNav2DirDegt == null)
                { cockpitRadiosNav2DirDegt = GetDataRefElement("sim/cockpit/radios/nav2_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the beacon indicated by nav2.  Set override with override_navneedles"); }
                return cockpitRadiosNav2DirDegt;
            }
        }

        private static DataRefElement cockpitRadiosAdf1DirDegt = null;
        /// <summary>
        /// The relative bearing to the beacon indicated by adf1.  Use override_adf to stg.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf1DirDegt {
            get {
                if (cockpitRadiosAdf1DirDegt == null)
                { cockpitRadiosAdf1DirDegt = GetDataRefElement("sim/cockpit/radios/adf1_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the beacon indicated by adf1.  Use override_adf to stg."); }
                return cockpitRadiosAdf1DirDegt;
            }
        }

        private static DataRefElement cockpitRadiosAdf2DirDegt = null;
        /// <summary>
        /// The relative bearing to the beacon indicated by adf2.  Use override_adf to stg.
        /// </summary>
        public static DataRefElement CockpitRadiosAdf2DirDegt {
            get {
                if (cockpitRadiosAdf2DirDegt == null)
                { cockpitRadiosAdf2DirDegt = GetDataRefElement("sim/cockpit/radios/adf2_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the beacon indicated by adf2.  Use override_adf to stg."); }
                return cockpitRadiosAdf2DirDegt;
            }
        }

        private static DataRefElement cockpitRadiosGpsDirDegt = null;
        /// <summary>
        /// The relative bearing to the GPS 1 destination.
        /// </summary>
        public static DataRefElement CockpitRadiosGpsDirDegt {
            get {
                if (cockpitRadiosGpsDirDegt == null)
                { cockpitRadiosGpsDirDegt = GetDataRefElement("sim/cockpit/radios/gps_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the GPS 1 destination."); }
                return cockpitRadiosGpsDirDegt;
            }
        }

        private static DataRefElement cockpitRadiosGps2DirDegt = null;
        /// <summary>
        /// The relative bearing to the GPS 2 destination.
        /// </summary>
        public static DataRefElement CockpitRadiosGps2DirDegt {
            get {
                if (cockpitRadiosGps2DirDegt == null)
                { cockpitRadiosGps2DirDegt = GetDataRefElement("sim/cockpit/radios/gps2_dir_degt", TypeStart_float, UtilConstants.Flag_Yes, "deg", "The relative bearing to the GPS 2 destination."); }
                return cockpitRadiosGps2DirDegt;
            }
        }

        private static DataRefElement cockpitRadiosDmeDirDegt = null;
        /// <summary>
        /// The relative bearing to whatever becaon the standalone DME is programmed for.
        /// </summary>
        public static DataRefElement CockpitRadiosDmeDirDegt {
            get {
                if (cockpitRadiosDmeDirDegt == null)
                { cockpitRadiosDmeDirDegt = GetDataRefElement("sim/cockpit/radios/dme_dir_degt", TypeStart_float, UtilConstants.Flag_No, "deg", "The relative bearing to whatever becaon the standalone DME is programmed for."); }
                return cockpitRadiosDmeDirDegt;
            }
        }

        private static DataRefElement cockpitRadiosNav1HdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav1HdefDot {
            get {
                if (cockpitRadiosNav1HdefDot == null)
                { cockpitRadiosNav1HdefDot = GetDataRefElement("sim/cockpit/radios/nav1_hdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_navneedles"); }
                return cockpitRadiosNav1HdefDot;
            }
        }

        private static DataRefElement cockpitRadiosNav1HdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav1HdefDot2 {
            get {
                if (cockpitRadiosNav1HdefDot2 == null)
                { cockpitRadiosNav1HdefDot2 = GetDataRefElement("sim/cockpit/radios/nav1_hdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_navneedles"); }
                return cockpitRadiosNav1HdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosNav2HdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav2HdefDot {
            get {
                if (cockpitRadiosNav2HdefDot == null)
                { cockpitRadiosNav2HdefDot = GetDataRefElement("sim/cockpit/radios/nav2_hdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_navneedles"); }
                return cockpitRadiosNav2HdefDot;
            }
        }

        private static DataRefElement cockpitRadiosNav2HdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav2HdefDot2 {
            get {
                if (cockpitRadiosNav2HdefDot2 == null)
                { cockpitRadiosNav2HdefDot2 = GetDataRefElement("sim/cockpit/radios/nav2_hdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_navneedles"); }
                return cockpitRadiosNav2HdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosGpsHdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGpsHdefDot {
            get {
                if (cockpitRadiosGpsHdefDot == null)
                { cockpitRadiosGpsHdefDot = GetDataRefElement("sim/cockpit/radios/gps_hdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_gps"); }
                return cockpitRadiosGpsHdefDot;
            }
        }

        private static DataRefElement cockpitRadiosGpsHdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGpsHdefDot2 {
            get {
                if (cockpitRadiosGpsHdefDot2 == null)
                { cockpitRadiosGpsHdefDot2 = GetDataRefElement("sim/cockpit/radios/gps_hdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_gps"); }
                return cockpitRadiosGpsHdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosGps2HdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGps2HdefDot {
            get {
                if (cockpitRadiosGps2HdefDot == null)
                { cockpitRadiosGps2HdefDot = GetDataRefElement("sim/cockpit/radios/gps2_hdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - pilot.  override_gps"); }
                return cockpitRadiosGps2HdefDot;
            }
        }

        private static DataRefElement cockpitRadiosGps2HdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGps2HdefDot2 {
            get {
                if (cockpitRadiosGps2HdefDot2 == null)
                { cockpitRadiosGps2HdefDot2 = GetDataRefElement("sim/cockpit/radios/gps2_hdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in course in 'dots' on a VOR compass - copilot.  override_gps"); }
                return cockpitRadiosGps2HdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosNav1VdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav1VdefDot {
            get {
                if (cockpitRadiosNav1VdefDot == null)
                { cockpitRadiosNav1VdefDot = GetDataRefElement("sim/cockpit/radios/nav1_vdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot.  override_navneedles"); }
                return cockpitRadiosNav1VdefDot;
            }
        }

        private static DataRefElement cockpitRadiosNav1VdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav1VdefDot2 {
            get {
                if (cockpitRadiosNav1VdefDot2 == null)
                { cockpitRadiosNav1VdefDot2 = GetDataRefElement("sim/cockpit/radios/nav1_vdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot.  override_navneedles"); }
                return cockpitRadiosNav1VdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosNav2VdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav2VdefDot {
            get {
                if (cockpitRadiosNav2VdefDot == null)
                { cockpitRadiosNav2VdefDot = GetDataRefElement("sim/cockpit/radios/nav2_vdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot.  override_navneedles"); }
                return cockpitRadiosNav2VdefDot;
            }
        }

        private static DataRefElement cockpitRadiosNav2VdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot.  override_navneedles
        /// </summary>
        public static DataRefElement CockpitRadiosNav2VdefDot2 {
            get {
                if (cockpitRadiosNav2VdefDot2 == null)
                { cockpitRadiosNav2VdefDot2 = GetDataRefElement("sim/cockpit/radios/nav2_vdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot.  override_navneedles"); }
                return cockpitRadiosNav2VdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosGpsVdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot. override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGpsVdefDot {
            get {
                if (cockpitRadiosGpsVdefDot == null)
                { cockpitRadiosGpsVdefDot = GetDataRefElement("sim/cockpit/radios/gps_vdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot. override_gps"); }
                return cockpitRadiosGpsVdefDot;
            }
        }

        private static DataRefElement cockpitRadiosGpsVdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot. override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGpsVdefDot2 {
            get {
                if (cockpitRadiosGpsVdefDot2 == null)
                { cockpitRadiosGpsVdefDot2 = GetDataRefElement("sim/cockpit/radios/gps_vdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot. override_gps"); }
                return cockpitRadiosGpsVdefDot2;
            }
        }

        private static DataRefElement cockpitRadiosGps2VdefDot = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot. override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGps2VdefDot {
            get {
                if (cockpitRadiosGps2VdefDot == null)
                { cockpitRadiosGps2VdefDot = GetDataRefElement("sim/cockpit/radios/gps2_vdef_dot", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - pilot. override_gps"); }
                return cockpitRadiosGps2VdefDot;
            }
        }

        private static DataRefElement cockpitRadiosGps2VdefDot2 = null;
        /// <summary>
        /// The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot. override_gps
        /// </summary>
        public static DataRefElement CockpitRadiosGps2VdefDot2 {
            get {
                if (cockpitRadiosGps2VdefDot2 == null)
                { cockpitRadiosGps2VdefDot2 = GetDataRefElement("sim/cockpit/radios/gps2_vdef_dot2", TypeStart_float, UtilConstants.Flag_Yes, "prcnt", "The deflection from the aircraft to the tuned in glide slope in dots on an ILS gauge - copilot. override_gps"); }
                return cockpitRadiosGps2VdefDot2;
            }
        }











        public static DataRefElement CockpitRadiosNav1Fromto
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_fromto",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav1 beacon - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1Fromto2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_fromto2",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav1 beacon - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2Fromto
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_fromto",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2Fromto2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_fromto2",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsFromto
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_fromto",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsFromto2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_fromto2",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2Fromto
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_fromto",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2Fromto2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_fromto2",
                    Units = "enum",
                    Description = "Whether we are heading to or from (or over) our nav2 beacon - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1CDI
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_CDI",
                    Units = "bool",
                    Description = "Are we receiving an expected glide slope for nav1",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2CDI
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_CDI",
                    Units = "bool",
                    Description = "Are we receiving an expected glide slope for nav2",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1DmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on nav1.  override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2DmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on nav2.  override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1DmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on adf1.  override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2DmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on adf2.  override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsDmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on the GPS.  override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2DmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on the GPS.  override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosStandaloneDmeDistM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/standalone_dme_dist_m",
                    Units = "nautical_miles",
                    Description = "Our distance in nautical miles from the beacon tuned in on the standalone DME receiver. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on nav1. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on nav2. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on adf1. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on adf2. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsDmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on the GPS. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on the GPS. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosStandaloneDmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/standalone_dme_speed_kts",
                    Units = "knots",
                    Description = "Our closing speed to the beacon tuned in on the standalone DME receiver. override_dme",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1DmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on nav1. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2DmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on nav2. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1DmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on adf1. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2DmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on adf2. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsDmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on the GPS 1. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2DmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on the GPS 2. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosStandaloneDmeTimeSecs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/standalone_dme_time_secs",
                    Units = "mins",
                    Description = "Our time to reach the beacon tuned in on the standalone DME. override_dme  (Dataref is labeled - this has always been minutes.)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1CourseDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_course_degm",
                    Units = "degm",
                    Description = "The localizer course for Nav1 or tuned in radial for a VOR. (Magnetic, new) - pilot use override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1CourseDegm2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_course_degm2",
                    Units = "degm",
                    Description = "The localizer course for Nav1 or tuned in radial for a VOR. (Magnetic, new) - copilot use override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2CourseDegm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_course_degm",
                    Units = "degm",
                    Description = "The localizer course for Nav2 or tuned in radial for a VOR. (Magnetic, new) - pilot use override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2CourseDegm2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_course_degm2",
                    Units = "degm",
                    Description = "The localizer course for Nav2 or tuned in radial for a VOR. (Magnetic, new) - copilot use override_navneedles",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsCourseDegtm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_course_degtm",
                    Units = "degm",
                    Description = "The localizer course for GPS 1 or tuned in radial for a VOR (Magnetic, new) - pilot - use override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsCourseDegtm2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_course_degtm2",
                    Units = "degm",
                    Description = "The localizer course for GPS 1 or tuned in radial for a VOR (Magnetic, new) - copilot - use override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2CourseDegtm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_course_degtm",
                    Units = "degm",
                    Description = "The localizer course for GPS 2 or tuned in radial for a VOR (Magnetic, new) - pilot - use override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2CourseDegtm2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_course_degtm2",
                    Units = "degm",
                    Description = "The localizer course for GPS 2 or tuned in radial for a VOR (Magnetic, new) - copilot - use override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1SlopeDegt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_slope_degt",
                    Units = "deg",
                    Description = "The glide slope slope for nav1.  Writable with override_navneedles starting in 940.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2SlopeDegt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_slope_degt",
                    Units = "deg",
                    Description = "The glide slope slope for nav2.  Writable with override_navneedles starting in 940.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsSlopeDegt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_slope_degt",
                    Units = "deg",
                    Description = "The glide slope slope for the GPS 1.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2SlopeDegt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_slope_degt",
                    Units = "deg",
                    Description = "The glide slope slope for the GPS 2.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsGpMtrPerDot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_gp_mtr_per_dot",
                    Units = "meter",
                    Description = "Baro-VNAV vertical path sensitivity in meters per dot of deflection.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2GpMtrPerDot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_gp_mtr_per_dot",
                    Units = "meter",
                    Description = "Baro-VNAV vertical path sensitivity in meters per dot of deflection.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsHdefNmPerDot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_hdef_nm_per_dot",
                    Units = "nautical_miles",
                    Description = "GPS CDI sensitivity in nautical miles per dot of deflection.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2HdefNmPerDot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_hdef_nm_per_dot",
                    Units = "nautical_miles",
                    Description = "GPS CDI sensitivity in nautical miles per dot of deflection.  Writable with override_gps.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosTransponderCode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/transponder_code",
                    Units = "code",
                    Description = "Our transponder code.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosTransponderId
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/transponder_id",
                    Units = "bool",
                    Description = "Whether we are squawking ident right now.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosTransponderLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/transponder_light",
                    Units = "???",
                    Description = "Whether the transponder is lit",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosTransponderBrightness
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/transponder_brightness",
                    Units = "ratio",
                    Description = "Transponder light brightness ratio from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosTransponderMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/transponder_mode",
                    Units = "enum",
                    Description = "Transponder mode (off=0,stdby=1,on=2,test=3)",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1CardinalDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_cardinal_dir",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for VOR 1 - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1CardinalDir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_cardinal_dir2",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for VOR 1 - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2CardinalDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_cardinal_dir",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for VOR 2 - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2CardinalDir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_cardinal_dir2",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for VOR 2 - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1CardinalDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_cardinal_dir",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for ADF 1 - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1CardinalDir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_cardinal_dir2",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for ADF 1 - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2CardinalDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_cardinal_dir",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for ADF 2 - pilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2CardinalDir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_cardinal_dir2",
                    Units = "degm",
                    Description = "Magnetic heading of the compass card for ADF 2 - copilot.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav1HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav1_has_dme",
                    Units = "bool",
                    Description = "Does this nav aid have DME?  Use override_dme to set",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNav2HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav2_has_dme",
                    Units = "bool",
                    Description = "Does this nav aid have DME?  Use override_dme to set",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf1HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf1_has_dme",
                    Units = "bool",
                    Description = "Does this adf aid have DME?  Use override_dme to set",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosAdf2HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/adf2_has_dme",
                    Units = "bool",
                    Description = "Does this adf aid have DME?  Use override_dme to set",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosDme5HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/dme5_has_dme",
                    Units = "bool",
                    Description = "Does this adf aid have DME?  Use override_dme to set",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosObsMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/obs_mag",
                    Units = "???",
                    Description = "OBS",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGearAudioWorking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gear_audio_working",
                    Units = "???",
                    Description = "Suppresses Gear Audio",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosMarkerAudioWorking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/marker_audio_working",
                    Units = "???",
                    Description = "Suppresses Marker Audio",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNavType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav_type",
                    Units = "enum",
                    Description = "Type of NAVAID that is tuned in.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosApSrc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/ap_src",
                    Units = "int",
                    Description = "autopilot source 0 is pilot, 1 is copilot",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosNavComAdfMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/nav_com_adf_mode",
                    Units = "int",
                    Description = "for a multifunction receiver...0-5 for nav1,nav2,com1,com2,adf1,adf2.",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGpsHasGlideslope
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps_has_glideslope",
                    Units = "int",
                    Description = "does the GPS 1 provide vertical guidance?  Write with override_gps",
                    
                };
            }
        }
        public static DataRefElement CockpitRadiosGps2HasGlideslope
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/radios/gps2_has_glideslope",
                    Units = "int",
                    Description = "does the GPS 2 provide vertical guidance?  Write with override_gps",
                    
                };
            }
        }
    }
}
