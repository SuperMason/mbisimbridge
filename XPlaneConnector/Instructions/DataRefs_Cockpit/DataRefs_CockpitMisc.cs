﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement CockpitMiscOuterMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/outer_marker_lit",
                    Units = "???",
                    Description = "Is the outer marker beacon lit right now",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscMiddleMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/middle_marker_lit",
                    Units = "???",
                    Description = "Is the middle marker beacon lit right now",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscInnerMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/inner_marker_lit",
                    Units = "???",
                    Description = "Is the inner marker beacon lit right now",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscOverOuterMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/over_outer_marker",
                    Units = "???",
                    Description = "Are we over the outer marker beacon",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscOverMiddleMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/over_middle_marker",
                    Units = "???",
                    Description = "Are we over the middle marker beacon",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscOverInnerMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/over_inner_marker",
                    Units = "???",
                    Description = "Are we over the inner marker beacon",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscBarometerSetting
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/barometer_setting",
                    Units = "???",
                    Description = "The pilots altimeter setting",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscBarometerSetting2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/barometer_setting2",
                    Units = "???",
                    Description = "The copilots altimeter setting",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscRadioAltimeterMinimum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/radio_altimeter_minimum",
                    Units = "Feet",
                    Description = "The decision height for the radio altimeter",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscShowPath
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/show_path",
                    Units = "???",
                    Description = "Show our path as we fly?",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscVacuum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/vacuum",
                    Units = "???",
                    Description = "Vacuum Ratio",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscVacuum2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/vacuum2",
                    Units = "???",
                    Description = "Vacuum 2 Ratio",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscAhAdjust
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/ah_adjust",
                    Units = "pixels",
                    Description = "Adjustment to the artificial horizon bars (pilot)",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscAhAdjust2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/ah_adjust2",
                    Units = "pixels",
                    Description = "Adjustment to the artificial horizon bars (copilot)",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscCompassIndicated
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/compass_indicated",
                    Units = "degm",
                    Description = "Indicated cockpit heading in magnetic degrees",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscHydraulicQuantity
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/hydraulic_quantity",
                    Units = "???",
                    Description = "Hydraulic Quantity 1",
                    
                };
            }
        }
        public static DataRefElement CockpitMiscHydraulicQuantity2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit/misc/hydraulic_quantity2",
                    Units = "???",
                    Description = "Hydraulic Quantity 2",
                    
                };
            }
        }
    }
}