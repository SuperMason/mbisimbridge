﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpitSwitchesDMERadioSelector = null;
        /// <summary>
        /// Which nav radio is the slaved DME connected to
        /// </summary>
        public static DataRefElement CockpitSwitchesDMERadioSelector {
            get {
                if (cockpitSwitchesDMERadioSelector == null)
                { cockpitSwitchesDMERadioSelector = GetDataRefElement("sim/cockpit/switches/DME_radio_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Which nav radio is the slaved DME connected to"); }
                return cockpitSwitchesDMERadioSelector;
            }
        }

        private static DataRefElement cockpitSwitchesDMEDistanceOrTime = null;
        /// <summary>
        /// Is the standalone DME showing distance or groundspeed/time
        /// </summary>
        public static DataRefElement CockpitSwitchesDMEDistanceOrTime {
            get {
                if (cockpitSwitchesDMEDistanceOrTime == null)
                { cockpitSwitchesDMEDistanceOrTime = GetDataRefElement("sim/cockpit/switches/DME_distance_or_time", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the standalone DME showing distance or groundspeed/time"); }
                return cockpitSwitchesDMEDistanceOrTime;
            }
        }

        private static DataRefElement cockpitSwitchesHSISelector = null;
        /// <summary>
        /// Is the HSI showing nav1, nav2, or GPS
        /// </summary>
        public static DataRefElement CockpitSwitchesHSISelector {
            get {
                if (cockpitSwitchesHSISelector == null)
                { cockpitSwitchesHSISelector = GetDataRefElement("sim/cockpit/switches/HSI_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the HSI showing nav1, nav2, or GPS"); }
                return cockpitSwitchesHSISelector;
            }
        }

        private static DataRefElement cockpitSwitchesHSISelector2 = null;
        /// <summary>
        /// Is the HSI showing nav1, nav2, or GPS
        /// </summary>
        public static DataRefElement CockpitSwitchesHSISelector2 {
            get {
                if (cockpitSwitchesHSISelector2 == null)
                { cockpitSwitchesHSISelector2 = GetDataRefElement("sim/cockpit/switches/HSI_selector2", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the HSI showing nav1, nav2, or GPS"); }
                return cockpitSwitchesHSISelector2;
            }
        }

        private static DataRefElement cockpitSwitchesRMISelector = null;
        /// <summary>
        /// Is the RMI showing nav1, nav2, or GPS
        /// </summary>
        public static DataRefElement CockpitSwitchesRMISelector {
            get {
                if (cockpitSwitchesRMISelector == null)
                { cockpitSwitchesRMISelector = GetDataRefElement("sim/cockpit/switches/RMI_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the RMI showing nav1, nav2, or GPS"); }
                return cockpitSwitchesRMISelector;
            }
        }

        private static DataRefElement cockpitSwitchesRMISelector2 = null;
        /// <summary>
        /// Is the RMI showing nav1, nav2, or GPS
        /// </summary>
        public static DataRefElement CockpitSwitchesRMISelector2 {
            get {
                if (cockpitSwitchesRMISelector2 == null)
                { cockpitSwitchesRMISelector2 = GetDataRefElement("sim/cockpit/switches/RMI_selector2", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the RMI showing nav1, nav2, or GPS"); }
                return cockpitSwitchesRMISelector2;
            }
        }

        private static DataRefElement cockpitSwitchesRMILVorAdfSelector = null;
        /// <summary>
        /// Is the left side of a VOR/ADF RMI showing the VOR or ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesRMILVorAdfSelector {
            get {
                if (cockpitSwitchesRMILVorAdfSelector == null)
                { cockpitSwitchesRMILVorAdfSelector = GetDataRefElement("sim/cockpit/switches/RMI_l_vor_adf_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the left side of a VOR/ADF RMI showing the VOR or ADF"); }
                return cockpitSwitchesRMILVorAdfSelector;
            }
        }

        private static DataRefElement cockpitSwitchesRMILVorAdfSelector2 = null;
        /// <summary>
        /// Is the left side of a VOR/ADF RMI showing the VOR or ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesRMILVorAdfSelector2 {
            get {
                if (cockpitSwitchesRMILVorAdfSelector2 == null)
                { cockpitSwitchesRMILVorAdfSelector2 = GetDataRefElement("sim/cockpit/switches/RMI_l_vor_adf_selector2", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the left side of a VOR/ADF RMI showing the VOR or ADF"); }
                return cockpitSwitchesRMILVorAdfSelector2;
            }
        }

        private static DataRefElement cockpitSwitchesRMIRVorAdfSelector = null;
        /// <summary>
        /// Is the right side of a VOR/ADF RMI showing the VOR or ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesRMIRVorAdfSelector {
            get {
                if (cockpitSwitchesRMIRVorAdfSelector == null)
                { cockpitSwitchesRMIRVorAdfSelector = GetDataRefElement("sim/cockpit/switches/RMI_r_vor_adf_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the right side of a VOR/ADF RMI showing the VOR or ADF"); }
                return cockpitSwitchesRMIRVorAdfSelector;
            }
        }

        private static DataRefElement cockpitSwitchesRMIRVorAdfSelector2 = null;
        /// <summary>
        /// Is the right side of a VOR/ADF RMI showing the VOR or ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesRMIRVorAdfSelector2 {
            get {
                if (cockpitSwitchesRMIRVorAdfSelector2 == null)
                { cockpitSwitchesRMIRVorAdfSelector2 = GetDataRefElement("sim/cockpit/switches/RMI_r_vor_adf_selector2", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the right side of a VOR/ADF RMI showing the VOR or ADF"); }
                return cockpitSwitchesRMIRVorAdfSelector2;
            }
        }

        private static DataRefElement cockpitSwitchesEFISDme1Selector = null;
        /// <summary>
        /// Is the first EFIS DME showing nothing, dist to VOR, or dist to ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISDme1Selector {
            get {
                if (cockpitSwitchesEFISDme1Selector == null)
                { cockpitSwitchesEFISDme1Selector = GetDataRefElement("sim/cockpit/switches/EFIS_dme_1_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the first EFIS DME showing nothing, dist to VOR, or dist to ADF"); }
                return cockpitSwitchesEFISDme1Selector;
            }
        }

        private static DataRefElement cockpitSwitchesEFISDme2Selector = null;
        /// <summary>
        /// Is the second EFIS DME showing nothing, dist to VOR, or dist to ADF
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISDme2Selector {
            get {
                if (cockpitSwitchesEFISDme2Selector == null)
                { cockpitSwitchesEFISDme2Selector = GetDataRefElement("sim/cockpit/switches/EFIS_dme_2_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the second EFIS DME showing nothing, dist to VOR, or dist to ADF"); }
                return cockpitSwitchesEFISDme2Selector;
            }
        }

        private static DataRefElement cockpitSwitchesMarkerPanelOut = null;
        /// <summary>
        /// Settings for the marker beacon audio panel
        /// </summary>
        public static DataRefElement CockpitSwitchesMarkerPanelOut {
            get {
                if (cockpitSwitchesMarkerPanelOut == null)
                { cockpitSwitchesMarkerPanelOut = GetDataRefElement("sim/cockpit/switches/marker_panel_out", TypeStart_int, UtilConstants.Flag_Yes, "???", "Settings for the marker beacon audio panel"); }
                return cockpitSwitchesMarkerPanelOut;
            }
        }

        private static DataRefElement cockpitSwitchesAudioPanelOut = null;
        /// <summary>
        /// Settings for the com radio audio panel. 6=com1,7=com2
        /// </summary>
        public static DataRefElement CockpitSwitchesAudioPanelOut {
            get {
                if (cockpitSwitchesAudioPanelOut == null)
                { cockpitSwitchesAudioPanelOut = GetDataRefElement("sim/cockpit/switches/audio_panel_out", TypeStart_int, UtilConstants.Flag_Yes, "???", "Settings for the com radio audio panel. 6=com1,7=com2"); }
                return cockpitSwitchesAudioPanelOut;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceOn = null;
        /// <summary>
        /// Is the anti-icing system on.  This turns on EVERY anti-ice system.
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceOn {
            get {
                if (cockpitSwitchesAntiIceOn == null)
                { cockpitSwitchesAntiIceOn = GetDataRefElement("sim/cockpit/switches/anti_ice_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the anti-icing system on.  This turns on EVERY anti-ice system."); }
                return cockpitSwitchesAntiIceOn;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceInletHeat = null;
        /// <summary>
        /// Turns on inlet heating for engine 1
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceInletHeat {
            get {
                if (cockpitSwitchesAntiIceInletHeat == null)
                { cockpitSwitchesAntiIceInletHeat = GetDataRefElement("sim/cockpit/switches/anti_ice_inlet_heat", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on inlet heating for engine 1"); }
                return cockpitSwitchesAntiIceInletHeat;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceInletHeatPerEnigne = null;
        /// <summary>
        /// Turns on inlet heating - array access to all engines, up to 8. (Deprecated - use correct spelled dataref)
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceInletHeatPerEnigne {
            get {
                if (cockpitSwitchesAntiIceInletHeatPerEnigne == null)
                { cockpitSwitchesAntiIceInletHeatPerEnigne = GetDataRefElement("sim/cockpit/switches/anti_ice_inlet_heat_per_enigne", TypeStart_int8, UtilConstants.Flag_Yes, "bool", "Turns on inlet heating - array access to all engines, up to 8. (Deprecated - use correct spelled dataref)"); }
                return cockpitSwitchesAntiIceInletHeatPerEnigne;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceInletHeatPerEngine = null;
        /// <summary>
        /// Turns on inlet heating - array access to all engines, up to 8.
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceInletHeatPerEngine {
            get {
                if (cockpitSwitchesAntiIceInletHeatPerEngine == null)
                { cockpitSwitchesAntiIceInletHeatPerEngine = GetDataRefElement("sim/cockpit/switches/anti_ice_inlet_heat_per_engine", TypeStart_int8, UtilConstants.Flag_Yes, "bool", "Turns on inlet heating - array access to all engines, up to 8."); }
                return cockpitSwitchesAntiIceInletHeatPerEngine;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIcePropHeat = null;
        /// <summary>
        /// Turns on prop heat for prop 1
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIcePropHeat {
            get {
                if (cockpitSwitchesAntiIcePropHeat == null)
                { cockpitSwitchesAntiIcePropHeat = GetDataRefElement("sim/cockpit/switches/anti_ice_prop_heat", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on prop heat for prop 1"); }
                return cockpitSwitchesAntiIcePropHeat;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIcePropHeatPerEngine = null;
        /// <summary>
        /// Turns on prop heat - array access to all props, up to 8.
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIcePropHeatPerEngine {
            get {
                if (cockpitSwitchesAntiIcePropHeatPerEngine == null)
                { cockpitSwitchesAntiIcePropHeatPerEngine = GetDataRefElement("sim/cockpit/switches/anti_ice_prop_heat_per_engine", TypeStart_int8, UtilConstants.Flag_Yes, "bool", "Turns on prop heat - array access to all props, up to 8."); }
                return cockpitSwitchesAntiIcePropHeatPerEngine;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceWindowHeat = null;
        /// <summary>
        /// Turns on anti-icing fr the windshield
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceWindowHeat {
            get {
                if (cockpitSwitchesAntiIceWindowHeat == null)
                { cockpitSwitchesAntiIceWindowHeat = GetDataRefElement("sim/cockpit/switches/anti_ice_window_heat", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on anti-icing fr the windshield"); }
                return cockpitSwitchesAntiIceWindowHeat;
            }
        }

        private static DataRefElement cockpitSwitchesPitotHeatOn = null;
        /// <summary>
        /// Is the pitot heat on
        /// </summary>
        public static DataRefElement CockpitSwitchesPitotHeatOn {
            get {
                if (cockpitSwitchesPitotHeatOn == null)
                { cockpitSwitchesPitotHeatOn = GetDataRefElement("sim/cockpit/switches/pitot_heat_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the pitot heat on"); }
                return cockpitSwitchesPitotHeatOn;
            }
        }

        private static DataRefElement cockpitSwitchesPitotHeatOn2 = null;
        /// <summary>
        /// Is the backup pitot heat on
        /// </summary>
        public static DataRefElement CockpitSwitchesPitotHeatOn2 {
            get {
                if (cockpitSwitchesPitotHeatOn2 == null)
                { cockpitSwitchesPitotHeatOn2 = GetDataRefElement("sim/cockpit/switches/pitot_heat_on2", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the backup pitot heat on"); }
                return cockpitSwitchesPitotHeatOn2;
            }
        }

        private static DataRefElement cockpitSwitchesStaticHeatOn = null;
        /// <summary>
        /// Is the static port heat on
        /// </summary>
        public static DataRefElement CockpitSwitchesStaticHeatOn {
            get {
                if (cockpitSwitchesStaticHeatOn == null)
                { cockpitSwitchesStaticHeatOn = GetDataRefElement("sim/cockpit/switches/static_heat_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the static port heat on"); }
                return cockpitSwitchesStaticHeatOn;
            }
        }

        private static DataRefElement cockpitSwitchesStaticHeatOn2 = null;
        /// <summary>
        /// Is the backup static port heat on
        /// </summary>
        public static DataRefElement CockpitSwitchesStaticHeatOn2 {
            get {
                if (cockpitSwitchesStaticHeatOn2 == null)
                { cockpitSwitchesStaticHeatOn2 = GetDataRefElement("sim/cockpit/switches/static_heat_on2", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the backup static port heat on"); }
                return cockpitSwitchesStaticHeatOn2;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceAOAHeat = null;
        /// <summary>
        /// Turns on anti-icing for alpha vane heater, pilot side
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceAOAHeat {
            get {
                if (cockpitSwitchesAntiIceAOAHeat == null)
                { cockpitSwitchesAntiIceAOAHeat = GetDataRefElement("sim/cockpit/switches/anti_ice_AOA_heat", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on anti-icing for alpha vane heater, pilot side"); }
                return cockpitSwitchesAntiIceAOAHeat;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceAOAHeat2 = null;
        /// <summary>
        /// Turns on anti-icing for alpha vane heater, copilot side
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceAOAHeat2 {
            get {
                if (cockpitSwitchesAntiIceAOAHeat2 == null)
                { cockpitSwitchesAntiIceAOAHeat2 = GetDataRefElement("sim/cockpit/switches/anti_ice_AOA_heat2", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on anti-icing for alpha vane heater, copilot side"); }
                return cockpitSwitchesAntiIceAOAHeat2;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceSurfHeat = null;
        /// <summary>
        /// Turns on surface heat on the wings
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceSurfHeat {
            get {
                if (cockpitSwitchesAntiIceSurfHeat == null)
                { cockpitSwitchesAntiIceSurfHeat = GetDataRefElement("sim/cockpit/switches/anti_ice_surf_heat", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on surface heat on the wings"); }
                return cockpitSwitchesAntiIceSurfHeat;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceSurfHeatLeft = null;
        /// <summary>
        /// Turns on surface heat on the left wing
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceSurfHeatLeft {
            get {
                if (cockpitSwitchesAntiIceSurfHeatLeft == null)
                { cockpitSwitchesAntiIceSurfHeatLeft = GetDataRefElement("sim/cockpit/switches/anti_ice_surf_heat_left", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on surface heat on the left wing"); }
                return cockpitSwitchesAntiIceSurfHeatLeft;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceSurfHeatRight = null;
        /// <summary>
        /// Turns on surface heat on the right wing
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceSurfHeatRight {
            get {
                if (cockpitSwitchesAntiIceSurfHeatRight == null)
                { cockpitSwitchesAntiIceSurfHeatRight = GetDataRefElement("sim/cockpit/switches/anti_ice_surf_heat_right", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on surface heat on the right wing"); }
                return cockpitSwitchesAntiIceSurfHeatRight;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceSurfBoot = null;
        /// <summary>
        /// Turns on the wing-surface anti-ice boot, which knocks ice off the leading edge, once a certain thickness has accumulated.
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceSurfBoot {
            get {
                if (cockpitSwitchesAntiIceSurfBoot == null)
                { cockpitSwitchesAntiIceSurfBoot = GetDataRefElement("sim/cockpit/switches/anti_ice_surf_boot", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on the wing-surface anti-ice boot, which knocks ice off the leading edge, once a certain thickness has accumulated."); }
                return cockpitSwitchesAntiIceSurfBoot;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceEngineAir = null;
        /// <summary>
        /// ????
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceEngineAir {
            get {
                if (cockpitSwitchesAntiIceEngineAir == null)
                { cockpitSwitchesAntiIceEngineAir = GetDataRefElement("sim/cockpit/switches/anti_ice_engine_air", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "????"); }
                return cockpitSwitchesAntiIceEngineAir;
            }
        }

        private static DataRefElement cockpitSwitchesAntiIceAutoIgnite = null;
        /// <summary>
        /// ignition source is turned on automatically on low N1 to prevent flameout
        /// </summary>
        public static DataRefElement CockpitSwitchesAntiIceAutoIgnite {
            get {
                if (cockpitSwitchesAntiIceAutoIgnite == null)
                { cockpitSwitchesAntiIceAutoIgnite = GetDataRefElement("sim/cockpit/switches/anti_ice_auto_ignite", TypeStart_int, UtilConstants.Flag_Yes, "bool", "ignition source is turned on automatically on low N1 to prevent flameout"); }
                return cockpitSwitchesAntiIceAutoIgnite;
            }
        }

        private static DataRefElement cockpitSwitchesIceDetect = null;
        /// <summary>
        /// Turns on ice detect
        /// </summary>
        public static DataRefElement CockpitSwitchesIceDetect {
            get {
                if (cockpitSwitchesIceDetect == null)
                { cockpitSwitchesIceDetect = GetDataRefElement("sim/cockpit/switches/ice_detect", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Turns on ice detect"); }
                return cockpitSwitchesIceDetect;
            }
        }

        private static DataRefElement cockpitSwitchesAutoBrakeSettings = null;
        /// <summary>
        /// Settings for the autobrake control
        /// </summary>
        public static DataRefElement CockpitSwitchesAutoBrakeSettings {
            get {
                if (cockpitSwitchesAutoBrakeSettings == null)
                { cockpitSwitchesAutoBrakeSettings = GetDataRefElement("sim/cockpit/switches/auto_brake_settings", TypeStart_int, UtilConstants.Flag_Yes, "???", "Settings for the autobrake control"); }
                return cockpitSwitchesAutoBrakeSettings;
            }
        }

        private static DataRefElement cockpitSwitchesAutoFeatherMode = null;
        /// <summary>
        /// Settings for auto-feathering mode
        /// </summary>
        public static DataRefElement CockpitSwitchesAutoFeatherMode {
            get {
                if (cockpitSwitchesAutoFeatherMode == null)
                { cockpitSwitchesAutoFeatherMode = GetDataRefElement("sim/cockpit/switches/auto_feather_mode", TypeStart_int, UtilConstants.Flag_Yes, "???", "Settings for auto-feathering mode"); }
                return cockpitSwitchesAutoFeatherMode;
            }
        }

        private static DataRefElement cockpitSwitchesYawDamperOn = null;
        /// <summary>
        /// Is the yaw damper on
        /// </summary>
        public static DataRefElement CockpitSwitchesYawDamperOn {
            get {
                if (cockpitSwitchesYawDamperOn == null)
                { cockpitSwitchesYawDamperOn = GetDataRefElement("sim/cockpit/switches/yaw_damper_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the yaw damper on"); }
                return cockpitSwitchesYawDamperOn;
            }
        }

        private static DataRefElement cockpitSwitchesArtStabOn = null;
        /// <summary>
        /// Is the artificial stability system on?
        /// </summary>
        public static DataRefElement CockpitSwitchesArtStabOn {
            get {
                if (cockpitSwitchesArtStabOn == null)
                { cockpitSwitchesArtStabOn = GetDataRefElement("sim/cockpit/switches/art_stab_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the artificial stability system on?"); }
                return cockpitSwitchesArtStabOn;
            }
        }

        private static DataRefElement cockpitSwitchesPreRotateLevel = null;
        public static DataRefElement CockpitSwitchesPreRotateLevel {
            get {
                if (cockpitSwitchesPreRotateLevel == null)
                { cockpitSwitchesPreRotateLevel = GetDataRefElement("sim/cockpit/switches/pre_rotate_level", TypeStart_int, UtilConstants.Flag_Yes, "???", "???"); }
                return cockpitSwitchesPreRotateLevel;
            }
        }

        private static DataRefElement cockpitSwitchesParachuteOn = null;
        /// <summary>
        /// Is the parachute deployed
        /// </summary>
        public static DataRefElement CockpitSwitchesParachuteOn {
            get {
                if (cockpitSwitchesParachuteOn == null)
                { cockpitSwitchesParachuteOn = GetDataRefElement("sim/cockpit/switches/parachute_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the parachute deployed"); }
                return cockpitSwitchesParachuteOn;
            }
        }

        private static DataRefElement cockpitSwitchesJatoOn = null;
        /// <summary>
        /// Are jatos on
        /// </summary>
        public static DataRefElement CockpitSwitchesJatoOn {
            get {
                if (cockpitSwitchesJatoOn == null)
                { cockpitSwitchesJatoOn = GetDataRefElement("sim/cockpit/switches/jato_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are jatos on"); }
                return cockpitSwitchesJatoOn;
            }
        }

        private static DataRefElement cockpitSwitchesPropSyncOn = null;
        /// <summary>
        /// Is prop sync on
        /// </summary>
        public static DataRefElement CockpitSwitchesPropSyncOn {
            get {
                if (cockpitSwitchesPropSyncOn == null)
                { cockpitSwitchesPropSyncOn = GetDataRefElement("sim/cockpit/switches/prop_sync_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is prop sync on"); }
                return cockpitSwitchesPropSyncOn;
            }
        }

        private static DataRefElement cockpitSwitchesPuffersOn = null;
        /// <summary>
        /// Are puffers on
        /// </summary>
        public static DataRefElement CockpitSwitchesPuffersOn {
            get {
                if (cockpitSwitchesPuffersOn == null)
                { cockpitSwitchesPuffersOn = GetDataRefElement("sim/cockpit/switches/puffers_on", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are puffers on"); }
                return cockpitSwitchesPuffersOn;
            }
        }

        private static DataRefElement cockpitSwitchesWaterScoop = null;
        /// <summary>
        /// Is water scoop active
        /// </summary>
        public static DataRefElement CockpitSwitchesWaterScoop {
            get {
                if (cockpitSwitchesWaterScoop == null)
                { cockpitSwitchesWaterScoop = GetDataRefElement("sim/cockpit/switches/water_scoop", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is water scoop active"); }
                return cockpitSwitchesWaterScoop;
            }
        }

        private static DataRefElement cockpitSwitchesArrestingGear = null;
        /// <summary>
        /// Is the arresting gear deployed
        /// </summary>
        public static DataRefElement CockpitSwitchesArrestingGear {
            get {
                if (cockpitSwitchesArrestingGear == null)
                { cockpitSwitchesArrestingGear = GetDataRefElement("sim/cockpit/switches/arresting_gear", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the arresting gear deployed"); }
                return cockpitSwitchesArrestingGear;
            }
        }

        private static DataRefElement cockpitSwitchesCanopyReq = null;
        /// <summary>
        /// Is the canopy handle open
        /// </summary>
        public static DataRefElement CockpitSwitchesCanopyReq {
            get {
                if (cockpitSwitchesCanopyReq == null)
                { cockpitSwitchesCanopyReq = GetDataRefElement("sim/cockpit/switches/canopy_req", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the canopy handle open"); }
                return cockpitSwitchesCanopyReq;
            }
        }

        private static DataRefElement cockpitSwitchesDumpingFuel = null;
        /// <summary>
        /// Are we dumping fuel
        /// </summary>
        public static DataRefElement CockpitSwitchesDumpingFuel {
            get {
                if (cockpitSwitchesDumpingFuel == null)
                { cockpitSwitchesDumpingFuel = GetDataRefElement("sim/cockpit/switches/dumping_fuel", TypeStart_int, UtilConstants.Flag_Yes, "???", "Are we dumping fuel"); }
                return cockpitSwitchesDumpingFuel;
            }
        }

        private static DataRefElement cockpitSwitchesTotEnerAudio = null;
        public static DataRefElement CockpitSwitchesTotEnerAudio {
            get {
                if (cockpitSwitchesTotEnerAudio == null)
                { cockpitSwitchesTotEnerAudio = GetDataRefElement("sim/cockpit/switches/tot_ener_audio", TypeStart_int, UtilConstants.Flag_Yes, "???", "???"); }
                return cockpitSwitchesTotEnerAudio;
            }
        }

        private static DataRefElement cockpitSwitchesEFISMapMode = null;
        /// <summary>
        /// Is the moving map showing the map or another HSI
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISMapMode {
            get {
                if (cockpitSwitchesEFISMapMode == null)
                { cockpitSwitchesEFISMapMode = GetDataRefElement("sim/cockpit/switches/EFIS_map_mode", TypeStart_int, UtilConstants.Flag_Yes, "???", "Is the moving map showing the map or another HSI"); }
                return cockpitSwitchesEFISMapMode;
            }
        }

        private static DataRefElement cockpitSwitchesEFISMapSubmode = null;
        /// <summary>
        /// 0=app,1=vor,2=map,3=nav,4=pln specific mode of the map (or HSI)
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISMapSubmode {
            get {
                if (cockpitSwitchesEFISMapSubmode == null)
                { cockpitSwitchesEFISMapSubmode = GetDataRefElement("sim/cockpit/switches/EFIS_map_submode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "0=app,1=vor,2=map,3=nav,4=pln specific mode of the map (or HSI)"); }
                return cockpitSwitchesEFISMapSubmode;
            }
        }

        private static DataRefElement cockpitSwitchesEFISMapRangeSelector = null;
        /// <summary>
        /// The display range for the moving map
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISMapRangeSelector {
            get {
                if (cockpitSwitchesEFISMapRangeSelector == null)
                { cockpitSwitchesEFISMapRangeSelector = GetDataRefElement("sim/cockpit/switches/EFIS_map_range_selector", TypeStart_int, UtilConstants.Flag_Yes, "???", "The display range for the moving map"); }
                return cockpitSwitchesEFISMapRangeSelector;
            }
        }

        private static DataRefElement cockpitSwitchesECAMMode = null;
        /// <summary>
        /// The display mode for the ECAM
        /// </summary>
        public static DataRefElement CockpitSwitchesECAMMode {
            get {
                if (cockpitSwitchesECAMMode == null)
                { cockpitSwitchesECAMMode = GetDataRefElement("sim/cockpit/switches/ECAM_mode", TypeStart_int, UtilConstants.Flag_Yes, "???", "The display mode for the ECAM"); }
                return cockpitSwitchesECAMMode;
            }
        }

        private static DataRefElement cockpitSwitchesGearHandleStatus = null;
        /// <summary>
        /// Gear handle is up or down?
        /// </summary>
        public static DataRefElement CockpitSwitchesGearHandleStatus {
            get {
                if (cockpitSwitchesGearHandleStatus == null)
                { cockpitSwitchesGearHandleStatus = GetDataRefElement("sim/cockpit/switches/gear_handle_status", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Gear handle is up or down?"); }
                return cockpitSwitchesGearHandleStatus;
            }
        }

        private static DataRefElement cockpitSwitchesEFIFSShowsWeather = null;
        /// <summary>
        /// Does the EFIS display show storms/weather? - legacy - origin dataref contained a typo
        /// </summary>
        public static DataRefElement CockpitSwitchesEFIFSShowsWeather {
            get {
                if (cockpitSwitchesEFIFSShowsWeather == null)
                { cockpitSwitchesEFIFSShowsWeather = GetDataRefElement("sim/cockpit/switches/EFIFS_shows_weather", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS display show storms/weather? - legacy - origin dataref contained a typo"); }
                return cockpitSwitchesEFIFSShowsWeather;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsWeather = null;
        /// <summary>
        /// Does the EFIS display show storms/weather?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsWeather {
            get {
                if (cockpitSwitchesEFISShowsWeather == null)
                { cockpitSwitchesEFISShowsWeather = GetDataRefElement("sim/cockpit/switches/EFIS_shows_weather", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS display show storms/weather?"); }
                return cockpitSwitchesEFISShowsWeather;
            }
        }

        private static DataRefElement cockpitSwitchesEFISWeatherAlpha = null;
        /// <summary>
        /// Alpha level of EFIS weather from 0 to 1
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISWeatherAlpha {
            get {
                if (cockpitSwitchesEFISWeatherAlpha == null)
                { cockpitSwitchesEFISWeatherAlpha = GetDataRefElement("sim/cockpit/switches/EFIS_weather_alpha", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Alpha level of EFIS weather from 0 to 1"); }
                return cockpitSwitchesEFISWeatherAlpha;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsTcas = null;
        /// <summary>
        /// Does the EFIS show other aircraft?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsTcas {
            get {
                if (cockpitSwitchesEFISShowsTcas == null)
                { cockpitSwitchesEFISShowsTcas = GetDataRefElement("sim/cockpit/switches/EFIS_shows_tcas", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS show other aircraft?"); }
                return cockpitSwitchesEFISShowsTcas;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsAirports = null;
        /// <summary>
        /// Does the EFIS show other airports?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsAirports {
            get {
                if (cockpitSwitchesEFISShowsAirports == null)
                { cockpitSwitchesEFISShowsAirports = GetDataRefElement("sim/cockpit/switches/EFIS_shows_airports", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS show other airports?"); }
                return cockpitSwitchesEFISShowsAirports;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsWaypoints = null;
        /// <summary>
        /// Does the EFIS show waypoints?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsWaypoints {
            get {
                if (cockpitSwitchesEFISShowsWaypoints == null)
                { cockpitSwitchesEFISShowsWaypoints = GetDataRefElement("sim/cockpit/switches/EFIS_shows_waypoints", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS show waypoints?"); }
                return cockpitSwitchesEFISShowsWaypoints;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsVORs = null;
        /// <summary>
        /// Does the EFIS show VORs?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsVORs {
            get {
                if (cockpitSwitchesEFISShowsVORs == null)
                { cockpitSwitchesEFISShowsVORs = GetDataRefElement("sim/cockpit/switches/EFIS_shows_VORs", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS show VORs?"); }
                return cockpitSwitchesEFISShowsVORs;
            }
        }

        private static DataRefElement cockpitSwitchesEFISShowsNDBs = null;
        /// <summary>
        /// Does the EFIS show NDBs?
        /// </summary>
        public static DataRefElement CockpitSwitchesEFISShowsNDBs {
            get {
                if (cockpitSwitchesEFISShowsNDBs == null)
                { cockpitSwitchesEFISShowsNDBs = GetDataRefElement("sim/cockpit/switches/EFIS_shows_NDBs", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Does the EFIS show NDBs?"); }
                return cockpitSwitchesEFISShowsNDBs;
            }
        }

        private static DataRefElement cockpitSwitchesArgusMode = null;
        /// <summary>
        /// What mode is the Argus 2000 in
        /// </summary>
        public static DataRefElement CockpitSwitchesArgusMode {
            get {
                if (cockpitSwitchesArgusMode == null)
                { cockpitSwitchesArgusMode = GetDataRefElement("sim/cockpit/switches/argus_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "What mode is the Argus 2000 in"); }
                return cockpitSwitchesArgusMode;
            }
        }

        private static DataRefElement cockpitSwitchesNoSmoking = null;
        /// <summary>
        /// No Smoking
        /// </summary>
        public static DataRefElement CockpitSwitchesNoSmoking {
            get {
                if (cockpitSwitchesNoSmoking == null)
                { cockpitSwitchesNoSmoking = GetDataRefElement("sim/cockpit/switches/no_smoking", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "No Smoking"); }
                return cockpitSwitchesNoSmoking;
            }
        }

        private static DataRefElement cockpitSwitchesFastenSeatBelts = null;
        /// <summary>
        /// Fasten Seat Belts
        /// </summary>
        public static DataRefElement CockpitSwitchesFastenSeatBelts {
            get {
                if (cockpitSwitchesFastenSeatBelts == null)
                { cockpitSwitchesFastenSeatBelts = GetDataRefElement("sim/cockpit/switches/fasten_seat_belts", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Fasten Seat Belts"); }
                return cockpitSwitchesFastenSeatBelts;
            }
        }
    }
}
