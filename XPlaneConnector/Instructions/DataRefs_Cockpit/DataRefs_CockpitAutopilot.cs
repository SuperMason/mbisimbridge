﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// The autopilot master mode (off=0, flight director=1, on=2)
        /// </summary>
        public static DataRefElement CockpitAutopilotAutopilotMode
        { get { return GetDataRefElement("sim/cockpit/autopilot/autopilot_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "The autopilot master mode (off=0, flight director=1, on=2)"); } }
        /// <summary>
        /// Airspeed mode for the autopilot. DEPRECATED
        /// </summary>
        public static DataRefElement CockpitAutopilotAirspeedMode
        { get { return GetDataRefElement("sim/cockpit/autopilot/airspeed_mode", TypeStart_int, UtilConstants.Flag_No, "enum", "Airspeed mode for the autopilot. DEPRECATED"); } }
        /// <summary>
        /// Lateral navigation mode (GPS, heading, L-Nav approach). DEPRECATED
        /// </summary>
        public static DataRefElement CockpitAutopilotHeadingMode
        { get { return GetDataRefElement("sim/cockpit/autopilot/heading_mode", TypeStart_int, UtilConstants.Flag_No, "enum", "Lateral navigation mode (GPS, heading, L-Nav approach). DEPRECATED"); } }
        /// <summary>
        /// Vertical navigation (alt hold, VVI hold, V-Nav approach). DEPRECATED
        /// </summary>
        public static DataRefElement CockpitAutopilotAltitudeMode
        { get { return GetDataRefElement("sim/cockpit/autopilot/altitude_mode", TypeStart_int, UtilConstants.Flag_No, "enum", "Vertical navigation (alt hold, VVI hold, V-Nav approach). DEPRECATED"); } }
        /// <summary>
        /// Back course selection
        /// </summary>
        public static DataRefElement CockpitAutopilotBackcourseOn
        { get { return GetDataRefElement("sim/cockpit/autopilot/backcourse_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Back course selection"); } }
        /// <summary>
        /// Altitude dialed into the AP
        /// </summary>
        public static DataRefElement CockpitAutopilotAltitude
        { get { return GetDataRefElement("sim/cockpit/autopilot/altitude", TypeStart_float, UtilConstants.Flag_Yes, "ftmsl", "Altitude dialed into the AP"); } }
        /// <summary>
        /// Currently held altitude (remembered until you hit flchg)
        /// </summary>
        public static DataRefElement CockpitAutopilotCurrentAltitude
        { get { return GetDataRefElement("sim/cockpit/autopilot/current_altitude", TypeStart_float, UtilConstants.Flag_Yes, "ftmsl", "Currently held altitude (remembered until you hit flchg)"); } }
        /// <summary>
        /// Vertical speed to hold
        /// </summary>
        public static DataRefElement CockpitAutopilotVerticalVelocity
        { get { return GetDataRefElement("sim/cockpit/autopilot/vertical_velocity", TypeStart_float, UtilConstants.Flag_Yes, "fpm", "Vertical speed to hold"); } }
        /// <summary>
        /// Airspeed to hold, this changes from knots to a mach number
        /// </summary>
        public static DataRefElement CockpitAutopilotAirspeed
        { get { return GetDataRefElement("sim/cockpit/autopilot/airspeed", TypeStart_float, UtilConstants.Flag_Yes, "knots_mach", "Airspeed to hold, this changes from knots to a mach number"); } }
        /// <summary>
        /// The heading to fly (true, legacy)
        /// </summary>
        public static DataRefElement CockpitAutopilotHeading
        { get { return GetDataRefElement("sim/cockpit/autopilot/heading", TypeStart_float, UtilConstants.Flag_Yes, "degt", "The heading to fly (true, legacy)"); } }
        /// <summary>
        /// The heading to fly (magnetic, preferred) pilot
        /// </summary>
        public static DataRefElement CockpitAutopilotHeadingMag
        { get { return GetDataRefElement("sim/cockpit/autopilot/heading_mag", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The heading to fly (magnetic, preferred) pilot"); } }
        /// <summary>
        /// The heading to fly (magnetic, preferred) copilot
        /// </summary>
        public static DataRefElement CockpitAutopilotHeadingMag2
        { get { return GetDataRefElement("sim/cockpit/autopilot/heading_mag2", TypeStart_float, UtilConstants.Flag_Yes, "degm", "The heading to fly (magnetic, preferred) copilot"); } }
        /// <summary>
        /// Is our airspeed a mach number (this is writable if the panel has the button, otherwise sim controls)
        /// </summary>
        public static DataRefElement CockpitAutopilotAirspeedIsMach
        { get { return GetDataRefElement("sim/cockpit/autopilot/airspeed_is_mach", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is our airspeed a mach number (this is writable if the panel has the button, otherwise sim controls)"); } }
        /// <summary>
        /// The recommended pitch from the Flight Director.  Use override_flightdir
        /// </summary>
        public static DataRefElement CockpitAutopilotFlightDirectorPitch
        { get { return GetDataRefElement("sim/cockpit/autopilot/flight_director_pitch", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The recommended pitch from the Flight Director.  Use override_flightdir"); } }
        /// <summary>
        /// The recommended roll from the Flight Director.  Use override_flightdir
        /// </summary>
        public static DataRefElement CockpitAutopilotFlightDirectorRoll
        { get { return GetDataRefElement("sim/cockpit/autopilot/flight_director_roll", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The recommended roll from the Flight Director.  Use override_flightdir"); } }
        /// <summary>
        /// Various autopilot engage modes, etc.  See docs for flags
        /// </summary>
        public static DataRefElement CockpitAutopilotAutopilotState
        { get { return GetDataRefElement("sim/cockpit/autopilot/autopilot_state", TypeStart_int, UtilConstants.Flag_Yes, "flags", "Various autopilot engage modes, etc.  See docs for flags"); } }
        /// <summary>
        /// Bank limit - 0 = auto, 1-6 = 5-30 degrees of bank
        /// </summary>
        public static DataRefElement CockpitAutopilotHeadingRollMode
        { get { return GetDataRefElement("sim/cockpit/autopilot/heading_roll_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Bank limit - 0 = auto, 1-6 = 5-30 degrees of bank"); } }
        /// <summary>
        /// Localizer mode (off, armed, engaged). DEPRECATED
        /// </summary>
        public static DataRefElement CockpitAutopilotModeHnav
        { get { return GetDataRefElement("sim/cockpit/autopilot/mode_hnav", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Localizer mode (off, armed, engaged). DEPRECATED"); } }
        /// <summary>
        /// Glide-slope mode (off, armed, engaged). DEPRECATED
        /// </summary>
        public static DataRefElement CockpitAutopilotModeGls
        { get { return GetDataRefElement("sim/cockpit/autopilot/mode_gls", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Glide-slope mode (off, armed, engaged). DEPRECATED"); } }
        /// <summary>
        /// The pitch held when in pitch-hold mode.
        /// </summary>
        public static DataRefElement CockpitAutopilotSynHoldDeg
        { get { return GetDataRefElement("sim/cockpit/autopilot/syn_hold_deg", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The pitch held when in pitch-hold mode."); } }
        /// <summary>
        /// Heading to fly in nav mode - write this when override_nav_heading is set.  Useful for making a custom GPS that flies arcs.
        /// </summary>
        public static DataRefElement CockpitAutopilotNavSteerDegMag
        { get { return GetDataRefElement("sim/cockpit/autopilot/nav_steer_deg_mag", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Heading to fly in nav mode - write this when override_nav_heading is set.  Useful for making a custom GPS that flies arcs."); } }
    }
}