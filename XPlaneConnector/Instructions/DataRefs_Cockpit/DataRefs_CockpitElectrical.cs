﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpitElectricalBatteryOn = null;
        /// <summary>
        /// Is the main battery on ?
        /// </summary>
        public static DataRefElement CockpitElectricalBatteryOn {
            get {
                if (cockpitElectricalBatteryOn == null)
                { cockpitElectricalBatteryOn = GetDataRefElement("sim/cockpit/electrical/battery_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the main battery on"); }
                return cockpitElectricalBatteryOn;
            }
        }

        private static DataRefElement cockpitElectricalBatteryArrayOn = null;
        /// <summary>
        /// Is the battery selected on ?
        /// </summary>
        public static DataRefElement CockpitElectricalBatteryArrayOn {
            get {
                if (cockpitElectricalBatteryArrayOn == null)
                { cockpitElectricalBatteryArrayOn = GetDataRefElement("sim/cockpit/electrical/battery_array_on", TypeStart_int8, UtilConstants.Flag_Yes, "bool", "Is the battery selected on"); }
                return cockpitElectricalBatteryArrayOn;
            }
        }

        private static DataRefElement cockpitElectricalBatteryEQ = null;
        /// <summary>
        /// Does this cockpit have a battery switch ?
        /// </summary>
        public static DataRefElement CockpitElectricalBatteryEQ {
            get {
                if (cockpitElectricalBatteryEQ == null)
                { cockpitElectricalBatteryEQ = GetDataRefElement("sim/cockpit/electrical/battery_EQ", TypeStart_int, UtilConstants.Flag_No, "bool", "Does this cockpit have a battery switch"); }
                return cockpitElectricalBatteryEQ;
            }
        }

        private static DataRefElement cockpitElectricalAvionicsOn = null;
        /// <summary>
        /// Is there power to the avionics ?
        /// </summary>
        public static DataRefElement CockpitElectricalAvionicsOn {
            get {
                if (cockpitElectricalAvionicsOn == null)
                { cockpitElectricalAvionicsOn = GetDataRefElement("sim/cockpit/electrical/avionics_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is there power to the avionics"); }
                return cockpitElectricalAvionicsOn;
            }
        }

        private static DataRefElement cockpitElectricalAvionicsEQ = null;
        /// <summary>
        /// Does this cockpit have an avionics switch ?
        /// </summary>
        public static DataRefElement CockpitElectricalAvionicsEQ {
            get {
                if (cockpitElectricalAvionicsEQ == null)
                { cockpitElectricalAvionicsEQ = GetDataRefElement("sim/cockpit/electrical/avionics_EQ", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Does this cockpit have an avionics switch"); }
                return cockpitElectricalAvionicsEQ;
            }
        }

        private static DataRefElement cockpitElectricalGeneratorOn = null;
        /// <summary>
        /// Is the generator on (to charge batteries) - one for each engine ?
        /// </summary>
        public static DataRefElement CockpitElectricalGeneratorOn {
            get {
                if (cockpitElectricalGeneratorOn == null)
                { cockpitElectricalGeneratorOn = GetDataRefElement("sim/cockpit/electrical/generator_on", TypeStart_int8, UtilConstants.Flag_Yes, "bool", "Is the generator on (to charge batteries) - one for each engine"); }
                return cockpitElectricalGeneratorOn;
            }
        }

        private static DataRefElement cockpitElectricalGeneratorEQ = null;
        /// <summary>
        /// Does this cockpit have generator switches ?
        /// </summary>
        public static DataRefElement CockpitElectricalGeneratorEQ {
            get {
                if (cockpitElectricalGeneratorEQ == null)
                { cockpitElectricalGeneratorEQ = GetDataRefElement("sim/cockpit/electrical/generator_EQ", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Does this cockpit have generator switches"); }
                return cockpitElectricalGeneratorEQ;
            }
        }

        private static DataRefElement cockpitElectricalGeneratorApuOn = null;
        /// <summary>
        /// Is the APU Generator on ?
        /// </summary>
        public static DataRefElement CockpitElectricalGeneratorApuOn {
            get {
                if (cockpitElectricalGeneratorApuOn == null)
                { cockpitElectricalGeneratorApuOn = GetDataRefElement("sim/cockpit/electrical/generator_apu_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the APU Generator on"); }
                return cockpitElectricalGeneratorApuOn;
            }
        }

        private static DataRefElement cockpitElectricalGpuOn = null;
        /// <summary>
        /// Is the GPU on ?
        /// </summary>
        public static DataRefElement CockpitElectricalGpuOn {
            get {
                if (cockpitElectricalGpuOn == null)
                { cockpitElectricalGpuOn = GetDataRefElement("sim/cockpit/electrical/gpu_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the GPU on"); }
                return cockpitElectricalGpuOn;
            }
        }

        private static DataRefElement cockpitElectricalGeneratorApuAmps = null;
        /// <summary>
        /// APU Generator amps
        /// </summary>
        public static DataRefElement CockpitElectricalGeneratorApuAmps {
            get {
                if (cockpitElectricalGeneratorApuAmps == null)
                { cockpitElectricalGeneratorApuAmps = GetDataRefElement("sim/cockpit/electrical/generator_apu_amps", TypeStart_float, UtilConstants.Flag_Yes, "amps", "APU Generator amps"); }
                return cockpitElectricalGeneratorApuAmps;
            }
        }

        private static DataRefElement cockpitElectricalGpuAmps = null;
        /// <summary>
        /// GPU Amps
        /// </summary>
        public static DataRefElement CockpitElectricalGpuAmps {
            get {
                if (cockpitElectricalGpuAmps == null)
                { cockpitElectricalGpuAmps = GetDataRefElement("sim/cockpit/electrical/gpu_amps", TypeStart_float, UtilConstants.Flag_Yes, "amps", "GPU Amps"); }
                return cockpitElectricalGpuAmps;
            }
        }

        private static DataRefElement cockpitElectricalHUDOn = null;
        /// <summary>
        /// Is the HUD on ?
        /// </summary>
        public static DataRefElement CockpitElectricalHUDOn {
            get {
                if (cockpitElectricalHUDOn == null)
                { cockpitElectricalHUDOn = GetDataRefElement("sim/cockpit/electrical/HUD_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Is the HUD on"); }
                return cockpitElectricalHUDOn;
            }
        }

        private static DataRefElement cockpitElectricalHUDBrightness = null;
        /// <summary>
        /// HUD brightness level (0-1)
        /// </summary>
        public static DataRefElement CockpitElectricalHUDBrightness {
            get {
                if (cockpitElectricalHUDBrightness == null)
                { cockpitElectricalHUDBrightness = GetDataRefElement("sim/cockpit/electrical/HUD_brightness", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "HUD brightness level (0-1)"); }
                return cockpitElectricalHUDBrightness;
            }
        }

        private static DataRefElement cockpitElectricalBeaconLightsOn = null;
        /// <summary>
        /// Beacon Light
        /// </summary>
        public static DataRefElement CockpitElectricalBeaconLightsOn {
            get {
                if (cockpitElectricalBeaconLightsOn == null)
                { cockpitElectricalBeaconLightsOn = GetDataRefElement("sim/cockpit/electrical/beacon_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Beacon Light"); }
                return cockpitElectricalBeaconLightsOn;
            }
        }

        private static DataRefElement cockpitElectricalLandingLightsOn = null;
        /// <summary>
        /// Landing Light
        /// </summary>
        public static DataRefElement CockpitElectricalLandingLightsOn {
            get {
                if (cockpitElectricalLandingLightsOn == null)
                { cockpitElectricalLandingLightsOn = GetDataRefElement("sim/cockpit/electrical/landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Landing Light"); }
                return cockpitElectricalLandingLightsOn;
            }
        }

        private static DataRefElement cockpitElectricalNavLightsOn = null;
        /// <summary>
        /// Navigation Light
        /// </summary>
        public static DataRefElement CockpitElectricalNavLightsOn {
            get {
                if (cockpitElectricalNavLightsOn == null)
                { cockpitElectricalNavLightsOn = GetDataRefElement("sim/cockpit/electrical/nav_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Navigation Light"); }
                return cockpitElectricalNavLightsOn;
            }
        }

        private static DataRefElement cockpitElectricalStrobeLightsOn = null;
        /// <summary>
        /// Strobe Light
        /// </summary>
        public static DataRefElement CockpitElectricalStrobeLightsOn {
            get {
                if (cockpitElectricalStrobeLightsOn == null)
                { cockpitElectricalStrobeLightsOn = GetDataRefElement("sim/cockpit/electrical/strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Strobe Light"); }
                return cockpitElectricalStrobeLightsOn;
            }
        }

        private static DataRefElement cockpitElectricalTaxiLightOn = null;
        /// <summary>
        /// Taxi Lights
        /// </summary>
        public static DataRefElement CockpitElectricalTaxiLightOn {
            get {
                if (cockpitElectricalTaxiLightOn == null)
                { cockpitElectricalTaxiLightOn = GetDataRefElement("sim/cockpit/electrical/taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Taxi Lights"); }
                return cockpitElectricalTaxiLightOn;
            }
        }

        private static DataRefElement cockpitElectricalCockpitLightsOn = null;
        /// <summary>
        /// Are cockpit lights on.  (NOTE - previous docs were wrong, this is always read-only)
        /// </summary>
        public static DataRefElement CockpitElectricalCockpitLightsOn {
            get {
                if (cockpitElectricalCockpitLightsOn == null)
                { cockpitElectricalCockpitLightsOn = GetDataRefElement("sim/cockpit/electrical/cockpit_lights_on", TypeStart_int, UtilConstants.Flag_No, "bool", "Are cockpit lights on.  (NOTE - previous docs were wrong, this is always read-only)"); }
                return cockpitElectricalCockpitLightsOn;
            }
        }

        private static DataRefElement cockpitElectricalCockpitLights = null;
        /// <summary>
        /// Cockpit light level
        /// </summary>
        public static DataRefElement CockpitElectricalCockpitLights {
            get {
                if (cockpitElectricalCockpitLights == null)
                { cockpitElectricalCockpitLights = GetDataRefElement("sim/cockpit/electrical/cockpit_lights", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Cockpit light level"); }
                return cockpitElectricalCockpitLights;
            }
        }

        private static DataRefElement cockpitElectricalInstrumentBrightness = null;
        /// <summary>
        /// Instrument LED lighting level
        /// </summary>
        public static DataRefElement CockpitElectricalInstrumentBrightness {
            get {
                if (cockpitElectricalInstrumentBrightness == null)
                { cockpitElectricalInstrumentBrightness = GetDataRefElement("sim/cockpit/electrical/instrument_brightness", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Instrument LED lighting level"); }
                return cockpitElectricalInstrumentBrightness;
            }
        }

        private static DataRefElement cockpitElectricalSunglassesOn = null;
        /// <summary>
        /// Sunglasses on?
        /// </summary>
        public static DataRefElement CockpitElectricalSunglassesOn {
            get {
                if (cockpitElectricalSunglassesOn == null)
                { cockpitElectricalSunglassesOn = GetDataRefElement("sim/cockpit/electrical/sunglasses_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Sunglasses on?"); }
                return cockpitElectricalSunglassesOn;
            }
        }

        private static DataRefElement cockpitElectricalNightVisionOn = null;
        /// <summary>
        /// Night vision goggles on?
        /// </summary>
        public static DataRefElement CockpitElectricalNightVisionOn {
            get {
                if (cockpitElectricalNightVisionOn == null)
                { cockpitElectricalNightVisionOn = GetDataRefElement("sim/cockpit/electrical/night_vision_on", TypeStart_int, UtilConstants.Flag_Yes, "bool", "Night vision goggles on?"); }
                return cockpitElectricalNightVisionOn;
            }
        }

        private static DataRefElement cockpitElectricalAhBar = null;
        /// <summary>
        /// Cockpit light rheostat - this appears to be legacy and no longer used.
        /// </summary>
        public static DataRefElement CockpitElectricalAhBar {
            get {
                if (cockpitElectricalAhBar == null)
                { cockpitElectricalAhBar = GetDataRefElement("sim/cockpit/electrical/ah_bar", TypeStart_float, UtilConstants.Flag_Yes, "???", "Cockpit light rheostat - this appears to be legacy and no longer used."); }
                return cockpitElectricalAhBar;
            }
        }

        private static DataRefElement cockpitElectricalBatteryChargeWattHr = null;
        /// <summary>
        /// Current charge of each of the 8 batteries in watt-hours.
        /// </summary>
        public static DataRefElement CockpitElectricalBatteryChargeWattHr {
            get {
                if (cockpitElectricalBatteryChargeWattHr == null)
                { cockpitElectricalBatteryChargeWattHr = GetDataRefElement("sim/cockpit/electrical/battery_charge_watt_hr", TypeStart_float8, UtilConstants.Flag_Yes, "watt/hours", "Current charge of each of the 8 batteries in watt-hours."); }
                return cockpitElectricalBatteryChargeWattHr;
            }
        }
    }
}