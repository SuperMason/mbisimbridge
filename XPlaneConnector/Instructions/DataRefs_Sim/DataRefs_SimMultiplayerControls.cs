﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement multiplayerControlsYokePitchRatio = null;
        /// <summary>
        /// The deflection of the axis controlling pitch.
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsYokePitchRatio {
            get {
                if (multiplayerControlsYokePitchRatio == null)
                { multiplayerControlsYokePitchRatio = GetDataRefElement("sim/multiplayer/controls/yoke_pitch_ratio", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "The deflection of the axis controlling pitch."); }
                return multiplayerControlsYokePitchRatio;
            }
        }

        private static DataRefElement multiplayerControlsYokeRollRatio = null;
        /// <summary>
        /// The deflection of the axis controlling roll.
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsYokeRollRatio {
            get {
                if (multiplayerControlsYokeRollRatio == null)
                { multiplayerControlsYokeRollRatio = GetDataRefElement("sim/multiplayer/controls/yoke_roll_ratio", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "The deflection of the axis controlling roll."); }
                return multiplayerControlsYokeRollRatio;
            }
        }

        private static DataRefElement multiplayerControlsYokeHeadingRatio = null;
        /// <summary>
        /// The deflection of the axis controlling yaw.
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsYokeHeadingRatio {
            get {
                if (multiplayerControlsYokeHeadingRatio == null)
                { multiplayerControlsYokeHeadingRatio = GetDataRefElement("sim/multiplayer/controls/yoke_heading_ratio", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "The deflection of the axis controlling yaw."); }
                return multiplayerControlsYokeHeadingRatio;
            }
        }

        private static DataRefElement multiplayerControlsGearRequest = null;
        /// <summary>
        /// 0 = up, 1 = down
        /// </summary>
        public static DataRefElement MultiplayerControlsGearRequest {
            get {
                if (multiplayerControlsGearRequest == null)
                { multiplayerControlsGearRequest = GetDataRefElement("sim/multiplayer/controls/gear_request", TypeStart_int20, UtilConstants.Flag_Yes, "enum", "0 = up, 1 = down"); }
                return multiplayerControlsGearRequest;
            }
        }

        private static DataRefElement multiplayerControlsFlapRequest = null;
        /// <summary>
        /// Requested flap deployment
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsFlapRequest {
            get {
                if (multiplayerControlsFlapRequest == null)
                { multiplayerControlsFlapRequest = GetDataRefElement("sim/multiplayer/controls/flap_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Requested flap deployment"); }
                return multiplayerControlsFlapRequest;
            }
        }

        private static DataRefElement multiplayerControlsSpeedBrakeRequest = null;
        /// <summary>
        /// Speed Brake
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsSpeedBrakeRequest {
            get {
                if (multiplayerControlsSpeedBrakeRequest == null)
                { multiplayerControlsSpeedBrakeRequest = GetDataRefElement("sim/multiplayer/controls/speed_brake_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Speed Brake"); }
                return multiplayerControlsSpeedBrakeRequest;
            }
        }

        private static DataRefElement multiplayerControlsVectorRequest = null;
        /// <summary>
        /// Vectored Thrust
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsVectorRequest {
            get {
                if (multiplayerControlsVectorRequest == null)
                { multiplayerControlsVectorRequest = GetDataRefElement("sim/multiplayer/controls/vector_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Vectored Thrust"); }
                return multiplayerControlsVectorRequest;
            }
        }

        private static DataRefElement multiplayerControlsSweepRequest = null;
        /// <summary>
        /// Wing Sweep
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsSweepRequest {
            get {
                if (multiplayerControlsSweepRequest == null)
                { multiplayerControlsSweepRequest = GetDataRefElement("sim/multiplayer/controls/sweep__request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Wing Sweep"); }
                return multiplayerControlsSweepRequest;
            }
        }

        private static DataRefElement multiplayerControlsIncidenceRequest = null;
        /// <summary>
        /// Variable Wing Incidence
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsIncidenceRequest {
            get {
                if (multiplayerControlsIncidenceRequest == null)
                { multiplayerControlsIncidenceRequest = GetDataRefElement("sim/multiplayer/controls/incidence_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Variable Wing Incidence"); }
                return multiplayerControlsIncidenceRequest;
            }
        }

        private static DataRefElement multiplayerControlsDihedralRequest = null;
        /// <summary>
        /// Variable Wing Dihedral
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsDihedralRequest {
            get {
                if (multiplayerControlsDihedralRequest == null)
                { multiplayerControlsDihedralRequest = GetDataRefElement("sim/multiplayer/controls/dihedral_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Variable Wing Dihedral"); }
                return multiplayerControlsDihedralRequest;
            }
        }

        private static DataRefElement multiplayerControlsTailLockRatio = null;
        /// <summary>
        /// This is how locked the tail-wheel is ... 0 is free castoring, 1 is locked.
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsTailLockRatio {
            get {
                if (multiplayerControlsTailLockRatio == null)
                { multiplayerControlsTailLockRatio = GetDataRefElement("sim/multiplayer/controls/tail_lock_ratio", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "This is how locked the tail-wheel is ... 0 is free castoring, 1 is locked."); }
                return multiplayerControlsTailLockRatio;
            }
        }

        private static DataRefElement multiplayerControlsLBrakeAdd = null;
        /// <summary>
        /// Left Brake (off to max braking)
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsLBrakeAdd {
            get {
                if (multiplayerControlsLBrakeAdd == null)
                { multiplayerControlsLBrakeAdd = GetDataRefElement("sim/multiplayer/controls/l_brake_add", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Left Brake (off to max braking)"); }
                return multiplayerControlsLBrakeAdd;
            }
        }

        private static DataRefElement multiplayerControlsRBrakeAdd = null;
        /// <summary>
        /// Right Brake (off to max braking)
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsRBrakeAdd {
            get {
                if (multiplayerControlsRBrakeAdd == null)
                { multiplayerControlsRBrakeAdd = GetDataRefElement("sim/multiplayer/controls/r_brake_add", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Right Brake (off to max braking)"); }
                return multiplayerControlsRBrakeAdd;
            }
        }

        private static DataRefElement multiplayerControlsParkingBrake = null;
        /// <summary>
        /// Parking Brake (off to max braking)
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsParkingBrake {
            get {
                if (multiplayerControlsParkingBrake == null)
                { multiplayerControlsParkingBrake = GetDataRefElement("sim/multiplayer/controls/parking_brake", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Parking Brake (off to max braking)"); }
                return multiplayerControlsParkingBrake;
            }
        }

        private static DataRefElement multiplayerControlsAileronTrim = null;
        /// <summary>
        /// Aileron Trim (as ratio of full control surface deflection) -1=left,1=right
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsAileronTrim {
            get {
                if (multiplayerControlsAileronTrim == null)
                { multiplayerControlsAileronTrim = GetDataRefElement("sim/multiplayer/controls/aileron_trim", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "Aileron Trim (as ratio of full control surface deflection) -1=left,1=right"); }
                return multiplayerControlsAileronTrim;
            }
        }

        private static DataRefElement multiplayerControlsElevatorTrim = null;
        /// <summary>
        /// Elevation Trim (as ratio of full control surface deflection) -1=down,1=up
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsElevatorTrim {
            get {
                if (multiplayerControlsElevatorTrim == null)
                { multiplayerControlsElevatorTrim = GetDataRefElement("sim/multiplayer/controls/elevator_trim", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "Elevation Trim (as ratio of full control surface deflection) -1=down,1=up"); }
                return multiplayerControlsElevatorTrim;
            }
        }

        private static DataRefElement multiplayerControlsRotorTrim = null;
        /// <summary>
        /// Rotor Trim (as ratio of full control surface deflection) -1=nose down,1=nose up
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsRotorTrim {
            get {
                if (multiplayerControlsRotorTrim == null)
                { multiplayerControlsRotorTrim = GetDataRefElement("sim/multiplayer/controls/rotor_trim", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "Rotor Trim (as ratio of full control surface deflection) -1=nose down,1=nose up"); }
                return multiplayerControlsRotorTrim;
            }
        }

        private static DataRefElement multiplayerControlsRudderTrim = null;
        /// <summary>
        /// Rudder Trim (as ratio of full control surface deflection) -1=left,1=right
        /// <para>value range :: [-1..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsRudderTrim {
            get {
                if (multiplayerControlsRudderTrim == null)
                { multiplayerControlsRudderTrim = GetDataRefElement("sim/multiplayer/controls/rudder_trim", TypeStart_float20, UtilConstants.Flag_Yes, "[-1..1]", "Rudder Trim (as ratio of full control surface deflection) -1=left,1=right"); }
                return multiplayerControlsRudderTrim;
            }
        }

        private static DataRefElement multiplayerControlsEngineThrottleRequest = null;
        /// <summary>
        /// Requested Throttle Position
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsEngineThrottleRequest {
            get {
                if (multiplayerControlsEngineThrottleRequest == null)
                { multiplayerControlsEngineThrottleRequest = GetDataRefElement("sim/multiplayer/controls/engine_throttle_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Requested Throttle Position"); }
                return multiplayerControlsEngineThrottleRequest;
            }
        }

        private static DataRefElement multiplayerControlsEnginePropRequest = null;
        /// <summary>
        /// Requested Prop Position
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsEnginePropRequest {
            get {
                if (multiplayerControlsEnginePropRequest == null)
                { multiplayerControlsEnginePropRequest = GetDataRefElement("sim/multiplayer/controls/engine_prop_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Requested Prop Position"); }
                return multiplayerControlsEnginePropRequest;
            }
        }

        private static DataRefElement multiplayerControlsEnginePitchRequest = null;
        /// <summary>
        /// Requested Pitch Position
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsEnginePitchRequest {
            get {
                if (multiplayerControlsEnginePitchRequest == null)
                { multiplayerControlsEnginePitchRequest = GetDataRefElement("sim/multiplayer/controls/engine_pitch_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Requested Pitch Position"); }
                return multiplayerControlsEnginePitchRequest;
            }
        }

        private static DataRefElement multiplayerControlsEngineMixtureRequest = null;
        /// <summary>
        /// Requested Mixture Position
        /// <para>value range :: [0..1]</para>
        /// </summary>
        public static DataRefElement MultiplayerControlsEngineMixtureRequest {
            get {
                if (multiplayerControlsEngineMixtureRequest == null)
                { multiplayerControlsEngineMixtureRequest = GetDataRefElement("sim/multiplayer/controls/engine_mixture_request", TypeStart_float20, UtilConstants.Flag_Yes, "[0..1]", "Requested Mixture Position"); }
                return multiplayerControlsEngineMixtureRequest;
            }
        }
    }
}