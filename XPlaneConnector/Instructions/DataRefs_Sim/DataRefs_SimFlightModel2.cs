﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement flightmodel2ControlsPitchRatio = null;
        /// <summary>
        /// This is how much the flight controls are deflected in pitch after any stability augmentation, in ratio, where -1.0 is full down, and 1.0 is full up.
        /// </summary>
        public static DataRefElement Flightmodel2ControlsPitchRatio {
            get {
                if (flightmodel2ControlsPitchRatio == null)
                { flightmodel2ControlsPitchRatio = GetDataRefElement("sim/flightmodel2/controls/pitch_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is how much the flight controls are deflected in pitch after any stability augmentation, in ratio, where -1.0 is full down, and 1.0 is full up."); }
                return flightmodel2ControlsPitchRatio;
            }
        }

        public static DataRefElement Flightmodel2ControlsRollRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/roll_ratio",
                    Units = "ratio",
                    Description = "This is how much the flight controls are deflected in roll after any stability augmentation, in ratio, where -1.0 is full left, and 1.0 is full right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/heading_ratio",
                    Units = "ratio",
                    Description = "This is how much the flight controls are deflected in heading after any stability augmentation, where -1.0 is full left, and 1.0 is full right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsSpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/speedbrake_ratio",
                    Units = "ratio",
                    Description = "This is how much the speedbrakes surfaces are extended, in ratio, where 0.0 is fully retracted, and 1.0 is fully extended.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsWingsweepRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/wingsweep_ratio",
                    Units = "ratio",
                    Description = "Actual sweep, in ratio. 0.0 is no sweep deployment, 1 is max sweep deployment.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsThrustVectorRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/thrust_vector_ratio",
                    Units = "ratio",
                    Description = "Actual thrust vector, in ratio. 0.0 is no thrust vector deployment, 1 is max thrust vector deployment.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsDihedralRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/dihedral_ratio",
                    Units = "ratio",
                    Description = "Actual dihedral, in ratio. 0.0 is no dihedral deployment, 1 is max dihedral deployment.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsIncidenceRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/incidence_ratio",
                    Units = "ratio",
                    Description = "Actual incidence, in ratio. 0.0 is no incidence deployment, 1 is max incidence deployment.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsWingRetractionRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/wing_retraction_ratio",
                    Units = "ratio",
                    Description = "Actual wing-retraction, in ratio. 0.0 is no wing-retraction deployment, 1 is max wing-retraction deployment.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsFlapHandleDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/flap_handle_deploy_ratio",
                    Units = "ratio",
                    Description = "This is the ACTUAL FLAP deployment for overall flap system, in ratio, where 0.0 is flaps fully retracted, and 1.0 is flaps fully extended. You should probably use the deployment for flap set 1 or flap set 2 to deflect the surfaces though.  This takes into account that flaps deploy slowly, not instantaneously as the handle is dragged.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsSlat1DeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/slat1_deploy_ratio",
                    Units = "ratio",
                    Description = "Slat deployment, where 0.0 is slats fully retracted, 1.0 is slats fully extended. This variable applies to lading-edge flaps as well.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsSlat2DeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/slat2_deploy_ratio",
                    Units = "ratio",
                    Description = "Slat deployment, where 0.0 is slats fully retracted, 1.0 is slats fully extended. This variable applies to lading-edge flaps as well.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsFlap1DeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/flap1_deploy_ratio",
                    Units = "ratio",
                    Description = "This is the ACTUAL FLAP deployment for flap-set #1, in ratio, where 0.0 is flaps fully retracted, and 1.0 is flaps fully extended.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsFlap2DeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/flap2_deploy_ratio",
                    Units = "ratio",
                    Description = "This is the ACTUAL FLAP deployment for flap-set #2, in ratio, where 0.0 is flaps fully retracted, and 1.0 is flaps fully extended.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsStabilizerDeflectionDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/stabilizer_deflection_degrees",
                    Units = "degrees",
                    Description = "This is the actual stabilizer deflection with trim for all-moving horizontal stabilizers. This is the deflection you can see visually on airliners. This is in degrees, positive for leading-edge nose up.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsAileronTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/aileron_trim",
                    Units = "ratio",
                    Description = "Aileron trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the aileron trim is deflected enough to move the ailerons through 30% of their travel, then that is an aileron trim of 0.3.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsElevatorTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/elevator_trim",
                    Units = "ratio",
                    Description = "Elevator trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the elevator trim is deflected enough to move the elevators through 30% of their travel, then that is an elevator trim of 0.3.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsRudderTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/rudder_trim",
                    Units = "ratio",
                    Description = "Rudder trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rudder trim is deflected enough to move the rudders through 30% of their travel, then that is an rudder trim of 0.3.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsRotorTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/rotor_trim",
                    Units = "ratio",
                    Description = "Rotor trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rotor trim is deflected enough to move the rotor through 30% of its travel, then that is a rotor trim of 0.3.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2ControlsWaterRudderDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/controls/water_rudder_deploy_ratio",
                    Units = "ratio",
                    Description = "Deployment of the water rudder, 0 is none, 1 is max",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2DoorsType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/doors/type",
                    Units = "enumeration",
                    Description = "0=unused (plane does not use this door) 1=standard (opens before gear goes down) 2=attached to strut (cycles with door), 3=closed (opens before gear goes down, then closes again)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2DoorsAngleNowDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/doors/angle_now_deg",
                    Units = "degrees",
                    Description = "Current angle about the axis of rotation of this gear door, degrees.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesLocationXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/location_x_mtr",
                    Units = "meters",
                    Description = "Engine location, meters x, y, z, with respect to the default center of gravity.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesLocationYMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/location_y_mtr",
                    Units = "meters",
                    Description = "Engine location, meters x, y, z, with respect to the default center of gravity.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesLocationZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/location_z_mtr",
                    Units = "meters",
                    Description = "Engine location, meters x, y, z, with respect to the default center of gravity.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesThrottleUsedRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/throttle_used_ratio",
                    Units = "ratio",
                    Description = "Throttle that is actually going to the engine, which could be different than the commanded throttle due to FADEC throttle adjustments.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesHasFuelFlowBeforeMixture
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/has_fuel_flow_before_mixture",
                    Units = "boolean",
                    Description = "Engine has fuel making to the mixture control, yes or no.  Writable if override_fuel_system is true.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesHasFuelFlowAfterMixture
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/has_fuel_flow_after_mixture",
                    Units = "boolean",
                    Description = "Engine has fuel making to the combustion process, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesEngineIsBurningFuel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/engine_is_burning_fuel",
                    Units = "boolean",
                    Description = "Engine is currently burning fuel, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesEngineFuelInIntake
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/engine_fuel_in_intake",
                    Units = "ratio",
                    Description = "How much fuel has been primed into the intake port (fuel-injected engine) 0..1",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesAfterburnerOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/afterburner_on",
                    Units = "boolean",
                    Description = "Afterburner on, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesAfterburnerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/afterburner_ratio",
                    Units = "ratio",
                    Description = "Afterburner engaged ratio, 0.0 to 1.0.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesEngineRotationSpeedRadSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/engine_rotation_speed_rad_sec",
                    Units = "radians/second",
                    Description = "Rotational speed of the engine, in radians per second.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesEngineRotationAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/engine_rotation_angle_deg",
                    Units = "degrees",
                    Description = "This is the angle of the engine as it turns over, running 0 to 360 over and over again in normal operation. Engine speed can be different than prop rotation speed in clutched designs. This is radians per second.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropRotationSpeedRadSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_rotation_speed_rad_sec",
                    Units = "radians/second",
                    Description = "Radians per second rotation speed of the prop.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropRotationAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_rotation_angle_deg",
                    Units = "degrees",
                    Description = "This is the angle of the prop or engine-fan as it rotates. You will see this value circulate 0 to 360 degrees over and over as the engine runs and the prop or fan turns.  Override witih /prop_disc/override per engine!",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropNoDiscRotationAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_no_disc_rotation_angle_deg",
                    Units = "degrees",
                    Description = "This is the angle of the prop or engine-fan as it rotates. You will see this value circulate 0 to 360 degrees over and over as the engine runs and the prop or fan turns. This dataref NEVER shows the speed of the prop disc if it happens to be showing. This always tracks the actual prop.  Override witih /prop_disc/override per engine!",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_pitch_deg",
                    Units = "degrees",
                    Description = "This is the ACTUAL pitch of the prop in degrees from its flat-pitch setting.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropConeAngleRad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_cone_angle_rad",
                    Units = "radians",
                    Description = "This is the coning angle of the disc, in radians. Typically close to 0.0.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesRotorVerticalVectorDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/rotor_vertical_vector_deg",
                    Units = "degrees",
                    Description = "This engine rotor-disc vertical vector, in degrees, where 0 is straight forwards, 90 is straight up.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesRotorVerticalCyclicDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/rotor_vertical_cyclic_deg",
                    Units = "degrees",
                    Description = "This engine rotor-disc longitudinal cyclic, in degrees, where -10 is 10 degrees forwards, 10 is 10 degrees aft.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesRotorSideCyclicDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/rotor_side_cyclic_deg",
                    Units = "degrees",
                    Description = "This engine rotor-disc lateral cyclic, in degrees, where -10 is 10 degrees left, 10 is 10 degrees right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesRotorCyclicElevatorTiltDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/rotor_cyclic_elevator_tilt_deg",
                    Units = "degrees",
                    Description = "Longitudinal disc tilt from cyclic deflection.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesRotorCyclicAileronTiltDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/rotor_cyclic_aileron_tilt_deg",
                    Units = "degrees",
                    Description = "Lateral disc tilt from cyclic deflection.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesNacelleVerticalAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/nacelle_vertical_angle_deg",
                    Units = "degrees",
                    Description = "This engine nacelle vertical vector, in degrees, where 0 is straight forwards, 90 is straight up.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesThrustReverserDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/thrust_reverser_deploy_ratio",
                    Units = "ratio",
                    Description = "How far the reverser doors/mechanism have traveled.  0 = fully stowed, 1.0 = fully deployed.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropIsDisc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_is_disc",
                    Units = "boolean",
                    Description = "Is the prop a disc now?  Override with /prop_disc/override per engine!",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropTipDeflectionDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_tip_deflection_degrees",
                    Units = "degrees",
                    Description = "Degrees of deflection of a line from the crank shaft to the prop tip.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesFuelPumpSpinning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/fuel_pump_spinning",
                    Units = "0..1",
                    Description = "Is the electric fuel pump for this engine spinning?  1 = yes, 0 = no.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesStarterIsRunning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/starter_is_running",
                    Units = "boolean",
                    Description = "Is this starter running at all",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesStarterMakingTorque
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/starter_making_torque",
                    Units = "boolean",
                    Description = "Is this starter applying torque to the engine",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesIsOnFire
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/is_on_fire",
                    Units = "0..1",
                    Description = "Engine fire, 0 means none, 1 means max.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesSideSign
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/side_sign",
                    Units = "-1,0,1",
                    Description = "Sign of the side of the ACF for this engine: -1 for left side, 0 for inline, 1 for right side",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesN1Percent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/N1_percent",
                    Units = "percent",
                    Description = "N1 speed as percent of max (per engine)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesN2Percent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/N2_percent",
                    Units = "percent",
                    Description = "N2 speed as percent of max (per engine)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesITTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/ITT_deg_C",
                    Units = "degrees",
                    Description = "ITT (per engine) in degrees C",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesEGTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/EGT_deg_C",
                    Units = "degrees",
                    Description = "EGT (per engine) in degrees C",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesCHTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/CHT_deg_C",
                    Units = "degrees",
                    Description = "CHT (per engine) in degrees C",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesJetwashMtrSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/jetwash_mtr_sec",
                    Units = "meters/second",
                    Description = "Speed of propwash/jetwash behind the prop disc/engine exhaust in meters/second",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscOverride
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/override",
                    Units = "boolean",
                    Description = "Set to 1 to control the prop disc from a plugin.  Overrides all other variables in this section.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscWidth
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_width",
                    Units = "meters",
                    Description = "If larger than zero, the prop disc is actually two discs, with this separation at the root and no separation at the tip.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscLengthRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_length_ratio",
                    Units = "ratio",
                    Description = "Ratio to scale the length of the side image.  1.0 = the real length of the prop.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscS
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_s",
                    Units = "offset",
                    Description = "Offset from left (in \"slots\") for the prop disc texture.  Fractions blend horizontal images.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_t",
                    Units = "offset",
                    Description = "Offset from bottom (in \"slots\") for the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscSDim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_s_dim",
                    Units = "count",
                    Description = "Number of horizontal slots for the prop disc in the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscTDim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_t_dim",
                    Units = "count",
                    Description = "Number of vertical slots for the prop disc in the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscAlphaFront
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_alpha_front",
                    Units = "ratio",
                    Description = "Alpha of prop disc when viewed from front.  X-Plane interpolates as the view angle moves.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscAlphaSide
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_alpha_side",
                    Units = "ratio",
                    Description = "Alpha of prop disc when viewed of side.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscDiscAlphaInside
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/disc_alpha_inside",
                    Units = "ratio",
                    Description = "Ratio to multiply disc alpha when view is inside the airplane.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideWidth
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_width",
                    Units = "meters",
                    Description = "Width of prop side image in meters.  Height comes from prop radius.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideLengthRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_length_ratio",
                    Units = "ratio",
                    Description = "Ratio to scale the length of the side image.  1.0 = the real length of the prop.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideAngle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_angle",
                    Units = "degrees",
                    Description = "Rotation angle of the side images now - allows side to rotate independently of disc, perhaps faster.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideNumberOfBlades
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_number_of_blades",
                    Units = "count",
                    Description = "Number of side blades to draw.  Should be at least 2!",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideIsBillboard
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_is_billboard",
                    Units = "boolean",
                    Description = "If true, prop side angle comes from billboarding logic - if false, it comes from side_angle dataref.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideS
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_s",
                    Units = "offset",
                    Description = "Offset from left (in \"slots\") for the prop disc texture.  Fractions blend horizontal images.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_t",
                    Units = "offset",
                    Description = "Offset from bottom (in \"slots\") for the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideSDim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_s_dim",
                    Units = "count",
                    Description = "Number of horizontal slots for the prop side in the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideTDim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_t_dim",
                    Units = "count",
                    Description = "Number of vertical slots for the prop side in the prop disc texture",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideAlphaFront
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_alpha_front",
                    Units = "ratio",
                    Description = "Alpha of prop side when viewed from front.  X-Plane interpolates as the view angle moves.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideAlphaSide
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_alpha_side",
                    Units = "ratio",
                    Description = "Alpha of prop side when viewed of side.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideAlphaInside
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_alpha_inside",
                    Units = "ratio",
                    Description = "Ratio to multiply side alpha when view is inside the airplane.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2EnginesPropDiscSideAlphaToCamera
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/engines/prop_disc/side_alpha_to_camera",
                    Units = "ratio",
                    Description = "A ratio to multiply side alpha when the rotor is extending toward the camera.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearGearHeadingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/gear_heading_deg",
                    Units = "degrees",
                    Description = "Current gear heading angle, degrees.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearGearPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/gear_pitch_deg",
                    Units = "degrees",
                    Description = "Current gear pitch angle, degrees.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearGearRollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/gear_roll_deg",
                    Units = "degrees",
                    Description = "Current gear roll angle, degrees.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireSteerCommandDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_steer_command_deg",
                    Units = "degrees",
                    Description = "Steering command being sent to this gear, degrees positive right.  This takes into account steering algorithms for big planes like 747, but does not free castoring and springiness.  Writable in 1030.  Override via override_wheel_steer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireSteerActualDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_steer_actual_deg",
                    Units = "degrees",
                    Description = "Steering command actually enacted by the gear, degrees positive right.  Writable in 1030.  Override via override_wheel_steer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireVerticalDeflectionMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_vertical_deflection_mtr",
                    Units = "meters",
                    Description = "Vertical deflection of this gear, meters.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireVerticalForceNMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_vertical_force_n_mtr",
                    Units = "newton_meters",
                    Description = "Vertical force on this gear, newtons.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireRotationSpeedRadSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_rotation_speed_rad_sec",
                    Units = "radians/second",
                    Description = "Rotational speed of this tire, radians per second.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireRotationAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_rotation_angle_deg",
                    Units = "degrees",
                    Description = "Tire rotation in degrees, running 0 to 360 over and over as it rolls.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/deploy_ratio",
                    Units = "ratio",
                    Description = "This is how far down the landing gear is.  0=up, 1= down",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearEagleClawAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/eagle_claw_angle_deg",
                    Units = "degrees",
                    Description = "Angle of eagle-claw landing gear now, degrees, per gear.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireSkidRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_skid_ratio",
                    Units = "0..1",
                    Description = "Ratio of how much this tire is in a skid - 0 = full traction, 1 = completely skidding",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTotalDeflectionRate
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/total_deflection_rate",
                    Units = "meters/second",
                    Description = "Rate at which the tires are deflecting, positive means shocks are contracting",
                    
                };
            }
        }

        private static DataRefElement flightmodel2GearOnGround = null;
        /// <summary>
        /// Is this wheel on the ground
        /// </summary>
        public static DataRefElement Flightmodel2GearOnGround {
            get {
                if (flightmodel2GearOnGround == null)
                { flightmodel2GearOnGround = GetDataRefElement("sim/flightmodel2/gear/on_ground", TypeStart_int10, UtilConstants.Flag_No, "boolean", "Is this wheel on the ground"); }
                return flightmodel2GearOnGround;
            }
        }

        private static DataRefElement flightmodel2GearOnGrass = null;
        /// <summary>
        /// Is this wheel on grass
        /// </summary>
        public static DataRefElement Flightmodel2GearOnGrass {
            get {
                if (flightmodel2GearOnGrass == null)
                { flightmodel2GearOnGrass = GetDataRefElement("sim/flightmodel2/gear/on_grass", TypeStart_int10, UtilConstants.Flag_No, "boolean", "Is this wheel on grass"); }
                return flightmodel2GearOnGrass;
            }
        }

        public static DataRefElement Flightmodel2GearOnNoisy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/on_noisy",
                    Units = "boolean",
                    Description = "Is this wheel on some kind of noisy surface like gravel",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2GearTireSkidSpeedMtrSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/gear/tire_skid_speed_mtr_sec",
                    Units = "meters/second",
                    Description = "This is how fast the skidding part of the tire is dragged across the surface it is skidding on in meters/second.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsLandingLightsBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/landing_lights_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the landing lights, 0 is off, 1 is max.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsGenericLightsBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/generic_lights_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the landing lights, 0 is off, 1 is max.  Was dim [64] until 11.10",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsTaxiLightsBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/taxi_lights_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the taxi light, 0 is off, 1 is max.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsSpotLightsBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/spot_lights_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the spot light, 0 is off, 1 is max.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsNavLightsBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/nav_lights_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the nav lights, 0 is off, 1 is max.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsBeaconBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/beacon_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the beacon, 0 is off, 1 is max.  Use override_beacons_and_strobes",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsStrobeBrightnessRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/strobe_brightness_ratio",
                    Units = "ratio",
                    Description = "Ratio of the brightness of the strobe, 0 is off, 1 is max.  Use override_beacons_and_strobes",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsSpotLightHeadingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/spot_light_heading_deg",
                    Units = "degrees",
                    Description = "Heading offset in degrees of the spot light from its default position, positive is right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsSpotLightPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/spot_light_pitch_deg",
                    Units = "degrees",
                    Description = "Pitch offset in degrees of the spot light from its default position, positive is up.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsStrobeFlashNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/strobe_flash_now",
                    Units = "boolean",
                    Description = "Is any strobe flashing right now?",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2LightsOverrideBeaconsAndStrobes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/lights/override_beacons_and_strobes",
                    Units = "boolean",
                    Description = "override beacon and strobe control",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscCanopyOpenRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/canopy_open_ratio",
                    Units = "ratio",
                    Description = "Canopy position: 0 = closed, 1 = open",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscDoorOpenRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/door_open_ratio",
                    Units = "ratio",
                    Description = "How open is the door, 0 = closed, 1 = open",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscTailhookDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/tailhook_deploy_ratio",
                    Units = "ratio",
                    Description = "Tailhook position: 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscWaterScoopDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/water_scoop_deploy_ratio",
                    Units = "ratio",
                    Description = "Water scoop position: 0 = up, 1 = down",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscWaterDropDeployRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/water_drop_deploy_ratio",
                    Units = "ratio",
                    Description = "Water drop door position: 0 = up, 1=  down",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscWiperAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/wiper_angle_deg",
                    Units = "degrees",
                    Description = "current angle of the wiper.  range of motion is set in PlaneMaker.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscCustomSliderRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/custom_slider_ratio",
                    Units = "ratio",
                    Description = "Misc. traveling items for your use.  You define the meaning.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscPressureOutflowRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/pressure_outflow_ratio",
                    Units = "ratio",
                    Description = "How far is the pressure-outflow valve open?  0=closed, 1=open",
                    
                };
            }
        }

        private static DataRefElement flightmodel2MiscAoAAngleDegrees = null;
        /// <summary>
        /// Angle of attack probe.  Positive means aircracft nose is above the flight path in aircraft coordinates.
        /// </summary>
        public static DataRefElement Flightmodel2MiscAoAAngleDegrees {
            get {
                if (flightmodel2MiscAoAAngleDegrees == null)
                { flightmodel2MiscAoAAngleDegrees = GetDataRefElement("sim/flightmodel2/misc/AoA_angle_degrees", TypeStart_float, UtilConstants.Flag_No, "degrees", "Angle of attack probe.  Positive means aircracft nose is above the flight path in aircraft coordinates."); }
                return flightmodel2MiscAoAAngleDegrees;
            }
        }

        public static DataRefElement Flightmodel2MiscYawStringAngle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/yaw_string_angle",
                    Units = "degrees",
                    Description = "desc",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscYawStringAirspeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/yaw_string_airspeed",
                    Units = "kias",
                    Description = "desc",
                    
                };
            }
        }

        private static DataRefElement flightmodel2MiscGforceNormal = null;
        public static DataRefElement Flightmodel2MiscGforceNormal {
            get {
                if (flightmodel2MiscGforceNormal == null)
                { flightmodel2MiscGforceNormal = GetDataRefElement("sim/flightmodel2/misc/gforce_normal", TypeStart_float, UtilConstants.Flag_No, "todo", "desc"); }
                return flightmodel2MiscGforceNormal;
            }
        }

        public static DataRefElement Flightmodel2MiscGforceAxil
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/gforce_axil",
                    Units = "todo",
                    Description = "desc",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscGforceSide
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/gforce_side",
                    Units = "todo",
                    Description = "desc",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_x",
                    Units = "meters",
                    Description = "lateral offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_y",
                    Units = "meters",
                    Description = "vertical offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_z",
                    Units = "meters",
                    Description = "longitudinal offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerVx
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_vx",
                    Units = "meters",
                    Description = "lateral offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerVy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_vy",
                    Units = "meters",
                    Description = "vertical offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscBouncerVz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/bouncer_vz",
                    Units = "meters",
                    Description = "longitudinal offset in meters from default for this bouncer",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscAutoStartInProgress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/auto_start_in_progress",
                    Units = "boolean",
                    Description = "True while the auto-start sequence is happening.  Plugins that override the auto-start sequence should set this to 1 during the sequence and clear it when done.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscAutoBoardInProgress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/auto_board_in_progress",
                    Units = "boolean",
                    Description = "True while the auto-board sequence is happening.  Plugins that override the auto-board sequence should set this to 1 during the sequence and clear it when done.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscTailhookAngleDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/tailhook_angle_degrees",
                    Units = "degrees",
                    Description = "Angle of the tailhook",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscTowInProgress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/tow_in_progress",
                    Units = "boolean",
                    Description = "True while a tow operation is currently taking place. ",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2MiscHasCrashed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/misc/has_crashed",
                    Units = "boolean",
                    Description = "True if the aircraft is in a crashed state",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionTrueTheta
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/true_theta",
                    Units = "degrees",
                    Description = "The pitch of the aircraft relative to the earth precisely below the aircraft",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionTruePhi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/true_phi",
                    Units = "degrees",
                    Description = "The roll of the aircraft relative to the earth precisely below the aircraft",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionTruePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/true_psi",
                    Units = "degrees",
                    Description = "The heading of the aircraft relative to the earth precisely below the aircraft - true degrees north, always",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionMagPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/mag_psi",
                    Units = "degrees",
                    Description = "The real magnetic heading of the aircraft - the old magpsi dataref was FUBAR",
                    
                };
            }
        }

        private static DataRefElement flightmodel2PositionAlpha = null;
        /// <summary>
        /// The pitch relative to the flown path (angle of attack)
        /// </summary>
        public static DataRefElement Flightmodel2PositionAlpha {
            get {
                if (flightmodel2PositionAlpha == null)
                { flightmodel2PositionAlpha = GetDataRefElement("sim/flightmodel2/position/alpha", TypeStart_float, UtilConstants.Flag_No, "degrees", "The pitch relative to the flown path (angle of attack)"); }
                return flightmodel2PositionAlpha;
            }
        }

        public static DataRefElement Flightmodel2PositionBeta
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/beta",
                    Units = "degrees",
                    Description = "The heading relative to the flown path (yaw)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionVpath
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/vpath",
                    Units = "degrees",
                    Description = "The pitch the aircraft actually flies.  (vpath+alpha=theta)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionHpath
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/hpath",
                    Units = "degrees",
                    Description = "The heading the aircraft actually flies.  (hpath+beta=psi)",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionGroundspeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/groundspeed",
                    Units = "meters/sec",
                    Description = "The ground speed of the aircraft",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionTrueAirspeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/true_airspeed",
                    Units = "meters/sec",
                    Description = "Air speed true - this does not take into account air density at altitude!",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2PositionYAgl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/position/y_agl",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingAileron1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/aileron1_deg",
                    Units = "degrees",
                    Description = "Deflection of the aileron from set #1 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingAileron2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/aileron2_deg",
                    Units = "degrees",
                    Description = "Deflection of the aileron from set #2 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingSpoiler1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/spoiler1_deg",
                    Units = "degrees",
                    Description = "Deflection of the roll-spoilerfrom set #1 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingSpoiler2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/spoiler2_deg",
                    Units = "degrees",
                    Description = "Deflection of the roll-spoilerfrom set #1 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingYawbrakeDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/yawbrake_deg",
                    Units = "degrees",
                    Description = "Deflection of the yaw-brake on this wing. A yaw-brake is a set of spoilers on the top and bottom of the wing that split open symmetrically to drag that wing aft and yaw the plane. They are used on the B-2, for example.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElevator1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elevator1_deg",
                    Units = "degrees",
                    Description = "Deflection of the elevator from set #1 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElevator2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elevator2_deg",
                    Units = "degrees",
                    Description = "Deflection of the elevator from set #2 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingRudder1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/rudder1_deg",
                    Units = "degrees",
                    Description = "Deflection of the rudder from set #1 on this wing. Degrees, positive is trailing-edge right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingRudder2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/rudder2_deg",
                    Units = "degrees",
                    Description = "Deflection of the rudder from set #2 on this wing. Degrees, positive is trailing-edge right.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingFlap1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/flap1_deg",
                    Units = "degrees",
                    Description = "Deflection of the flap from set #1 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingFlap2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/flap2_deg",
                    Units = "degrees",
                    Description = "Deflection of the flap from set #2 on this wing. Degrees, positive is trailing-edge down.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingSpeedbrake1Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/speedbrake1_deg",
                    Units = "degrees",
                    Description = "Deflection of the speedbrakes from set #1 on this wing.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingSpeedbrake2Deg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/speedbrake2_deg",
                    Units = "degrees",
                    Description = "Deflection of the speedbrakes from set #2 on this wing.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingWingTipDeflectionDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/wing_tip_deflection_deg",
                    Units = "degrees",
                    Description = "Degrees of deflection of a line from the wing root (extended to the plane centerline) to the tip.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingWingCondensationRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/wing_condensation_ratio",
                    Units = "ratio",
                    Description = "Strength of wing condensation for this wing, 0 is none, 1 = max",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingWingTipCondensationRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/wing_tip_condensation_ratio",
                    Units = "ratio",
                    Description = "Strength of wing tip vapor trail for this wing, 0 is none, 1 = max",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElementsElementMACMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elements/element_MAC_mtr",
                    Units = "meters",
                    Description = "Width of chord at the MAC.",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElementsElementSurfaceAreaMtrSq
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elements/element_surface_area_mtr_sq",
                    Units = "square_meters",
                    Description = "per element surface area",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElementsElementCondensationRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elements/element_condensation_ratio",
                    Units = "ratio",
                    Description = "Per element condensation ratio",
                    
                };
            }
        }
        public static DataRefElement Flightmodel2WingElementsElementIsStalled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel2/wing/elements/element_is_stalled",
                    Units = "boolean",
                    Description = "Per element - is this element stalled?",
                    
                };
            }
        }
    }
}
