﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement WorldBoatVelocityMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/velocity_msc",
                    Units = "meters/sec",
                    Description = "Velocity of the boat in meters per second in its current direction.  Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/x_mtr",
                    Units = "meters",
                    Description = "X Position of the boat in meters in the local coordinate OpenGL coordinate system. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatYMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/y_mtr",
                    Units = "meters",
                    Description = "Y Position of the boat in meters in the local coordinate OpenGL coordinate system. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/z_mtr",
                    Units = "meters",
                    Description = "Z Position of the boat in meters in the local coordinate OpenGL coordinate system. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatHeadingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/heading_deg",
                    Units = "degrees(true)",
                    Description = "Heading of the boat in degrees from true north. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatPitchFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/pitch_frequency_hz",
                    Units = "hz",
                    Description = "Frequency at which the boat pitches up and down. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatPitchAmplitudeDegMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/pitch_amplitude_deg_mtr",
                    Units = "deg/meter",
                    Description = "Degrees that the boat pitches up for each meter of wave height. Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatRollFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/roll_frequency_hz",
                    Units = "hz",
                    Description = "Frequency at which the boat rolls from side to side.  Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatRollAmplitudeDegMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/roll_amplitude_deg_mtr",
                    Units = "deg/meter",
                    Description = "degrees that the boat rolls from side to side for each meter of wave height.  Index 0=carrier,1=frigate, writable using override_boats.",
                    
                };
            }
        }
        public static DataRefElement WorldBoatFrigateDeckHeightMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/frigate_deck_height_mtr",
                    Units = "meters",
                    Description = "Deck height of the frigate (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatFrigateILSOffsetXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/frigate_ILS_offset_x_mtr",
                    Units = "meters",
                    Description = "X position of the frigate ILS transmitter (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatFrigateILSOffsetZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/frigate_ILS_offset_z_mtr",
                    Units = "meters",
                    Description = "Z position of the frigate ILS transmitter (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierDeckHeightMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_deck_height_mtr",
                    Units = "meters",
                    Description = "Deck height of the carrier (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierILSOffsetXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_ILS_offset_x_mtr",
                    Units = "meters",
                    Description = "X position of the carrier ILS transmitter (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierILSOffsetZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_ILS_offset_z_mtr",
                    Units = "meters",
                    Description = "Z position of the carrier ILS transmitter (in coordinates of the OBJ model)",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierApproachHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_approach_heading",
                    Units = "degrees(true)",
                    Description = "Relative heading of the approach path from the carrier's heading",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_heading",
                    Units = "degrees(true)",
                    Description = "Relative heading of the catshot relative to the carrier's heading",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotStartXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_start_x_mtr",
                    Units = "meters",
                    Description = "X position (in model coordinates) of the start of the cat-shot track",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotStartZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_start_z_mtr",
                    Units = "meters",
                    Description = "Z position (in model coordinates) of the start of the cat-shot track",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotEndXMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_end_x_mtr",
                    Units = "meters",
                    Description = "X position (in model coordinates) of the end of the cat-shot track",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotEndZMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_end_z_mtr",
                    Units = "meters",
                    Description = "Z position (in model coordinates) of the end of the cat-shot track",
                    
                };
            }
        }
        public static DataRefElement WorldBoatCarrierCatshotStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/boat/carrier_catshot_status",
                    Units = "enum",
                    Description = "0=no cat shot set up,1=cat shot waiting to launch,2=in progress",
                    
                };
            }
        }
        public static DataRefElement WorldWinchWinchRampUpTimeSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/winch/winch_ramp_up_time_sec",
                    Units = "seconds",
                    Description = "This is how long it takes the winch to reach maximum speed for a glider winch take-off.",
                    
                };
            }
        }
        public static DataRefElement WorldWinchWinchSpeedKnots
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/winch/winch_speed_knots",
                    Units = "knots",
                    Description = "This is how fast the winch moves at its maximum speed.",
                    
                };
            }
        }
        public static DataRefElement WorldWinchWinchInitialLength
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/winch/winch_initial_length",
                    Units = "meters",
                    Description = "This is the initial length of cable for a winch takeoff.",
                    
                };
            }
        }
        public static DataRefElement WorldWinchWinchMaxBhp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/world/winch/winch_max_bhp",
                    Units = "bhp",
                    Description = "This is the maximum horsepower the winch can deliver reeling in the cable. Cable speed will decay with higher force on the cable to not exceed this limit.",
                    
                };
            }
        }
    }
}