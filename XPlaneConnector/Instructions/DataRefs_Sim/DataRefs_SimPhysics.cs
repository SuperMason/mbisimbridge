﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// The earth's mass-gravity constant
        /// </summary>
        public static DataRefElement PhysicsEarthMu
        { get { return GetDataRefElement("sim/physics/earth_mu", TypeStart_float, UtilConstants.Flag_No, "m^3/s^2", "The earth's mass-gravity constant"); } }
        /// <summary>
        /// Earth's radius
        /// </summary>
        public static DataRefElement PhysicsEarthRadiusM
        { get { return GetDataRefElement("sim/physics/earth_radius_m", TypeStart_float, UtilConstants.Flag_No, "meters", "Earth's radius"); } }
        /// <summary>
        /// Average sea level temp, current planet
        /// </summary>
        public static DataRefElement PhysicsEarthTempC
        { get { return GetDataRefElement("sim/physics/earth_temp_c", TypeStart_float, UtilConstants.Flag_No, "celsius", "Average sea level temp, current planet"); } }
        /// <summary>
        /// average pressure at sea level, current planet
        /// </summary>
        public static DataRefElement PhysicsEarthPressureP
        { get { return GetDataRefElement("sim/physics/earth_pressure_p", TypeStart_float, UtilConstants.Flag_No, "pascals", "average pressure at sea level, current planet"); } }
        /// <summary>
        /// rho at sea level, current planet
        /// </summary>
        public static DataRefElement PhysicsRhoSeaLevel
        { get { return GetDataRefElement("sim/physics/rho_sea_level", TypeStart_float, UtilConstants.Flag_No, "???", "rho at sea level, current planet"); } }
        /// <summary>
        /// gravitational acceleration of earth AT SEA LEVEL
        /// </summary>
        public static DataRefElement PhysicsGSealevel
        { get { return GetDataRefElement("sim/physics/g_sealevel", TypeStart_float, UtilConstants.Flag_No, "???", "gravitational acceleration of earth AT SEA LEVEL"); } }
        /// <summary>
        /// rho of water
        /// </summary>
        public static DataRefElement PhysicsRhoWater
        { get { return GetDataRefElement("sim/physics/rho_water", TypeStart_float, UtilConstants.Flag_No, "???", "rho of water"); } }
        /// <summary>
        /// Are we showing metric temperature indications
        /// </summary>
        public static DataRefElement PhysicsMetricTemp
        { get { return GetDataRefElement("sim/physics/metric_temp", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Are we showing metric temperature indications"); } }
        /// <summary>
        /// Are we showing metric pressure indications
        /// </summary>
        public static DataRefElement PhysicsMetricPress
        { get { return GetDataRefElement("sim/physics/metric_press", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Are we showing metric pressure indications"); } }
    }
}