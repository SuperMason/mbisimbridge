﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement GraphicsAnimationDrawObjectX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/draw_object_x",
                    Units = "float",
                    Description = "X position in cartesian space of currently drawn object",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDrawObjectY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/draw_object_y",
                    Units = "float",
                    Description = "Y position in cartesian space of currently drawn object",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDrawObjectZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/draw_object_z",
                    Units = "float",
                    Description = "Z position in cartesian space of currently drawn object",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDrawObjectPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/draw_object_psi",
                    Units = "float",
                    Description = "rotation in cartesian space of currently drawn object",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDrawLightLevel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/draw_light_level",
                    Units = "float",
                    Description = "This is the default _LIT light level that the object is being drawn at, before ATTR_light_level is applied.",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationAirportBeaconRotation
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/airport_beacon_rotation",
                    Units = "degrees",
                    Description = "angle this beacon is rotating (0-360). Civillian beacons rotate at 11-13 RPM, giving 22-26 flashes/min for an airport and 33-39/min flashes for a heliport. ",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationAirportBeaconRotationMilitary
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/airport_beacon_rotation_military",
                    Units = "degrees",
                    Description = "angle this military airport beacon is rotating (0-360). Military beacons rotate at 8-10 RPM, giving off 16-20 flashes/min where the double white counts as one flash. ",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationWindsockPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/windsock_psi",
                    Units = "degrees",
                    Description = "windsock heading",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationWindsockThe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/windsock_the",
                    Units = "degrees",
                    Description = "windsock pitch",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationWindsockPhi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/windsock_phi",
                    Units = "degrees",
                    Description = "windsock roll",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationCranePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/crane_psi",
                    Units = "ratio",
                    Description = "crane heading",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationBuoyHeight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/buoy_height",
                    Units = "meters",
                    Description = "how high above MSL 0 is this buoy riding?",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationPingPong2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ping_pong_2",
                    Units = "ratio",
                    Description = "-1 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationSinWave2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/sin_wave_2",
                    Units = "ratio",
                    Description = "-1 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationObjWigwagBrightness
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/obj_wigwag_brightness",
                    Units = "ratio",
                    Description = "Brightness of the wig-wag lights - stays in sync with sim/graphics/animation/lights/wigwag",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationCarrierBlastDefLf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/carrier_blast_def_lf",
                    Units = "ratio",
                    Description = "Ratio of deployment of carrier blast door (left front)",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationCarrierBlastDefRf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/carrier_blast_def_rf",
                    Units = "ratio",
                    Description = "Ratio of deployment of carrier blast door (right front)",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationCarrierBlastDefLr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/carrier_blast_def_lr",
                    Units = "ratio",
                    Description = "Ratio of deployment of carrier blast door (left rear)",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationCarrierBlastDefRr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/carrier_blast_def_rr",
                    Units = "ratio",
                    Description = "Ratio of deployment of carrier blast door (right rear)",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLevelCrossingGate
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/level_crossing_gate",
                    Units = "ratio",
                    Description = "Ratio of the position of all level crossing gates, 0 is up, 1 is down",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationBirdsWingFlapDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/birds/wing_flap_deg",
                    Units = "float",
                    Description = "degree of wing flap for the currently drawn bird",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationBirdsFeetRetractDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/birds/feet_retract_deg",
                    Units = "float",
                    Description = "angle the bird's feet are retracted for the flock of birds.",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDeerDeerRunCycle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/deer/deer_run_cycle",
                    Units = "float",
                    Description = "ratio for deer running",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationDeerDeerTurnCycle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/deer/deer_turn_cycle",
                    Units = "float",
                    Description = "ratio for deer turning",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsFlasher
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/flasher",
                    Units = "light",
                    Description = "A flashing light, on only at night, matches v7",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsPulse
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/pulse",
                    Units = "light",
                    Description = "A red pulsing light, on only at night, matches v7",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsStrobeV7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/strobe_v7",
                    Units = "light",
                    Description = "A white strobe light, on only at night, matches v7",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsTrafficLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/traffic_light",
                    Units = "light",
                    Description = "A 3-color traffic light, matches v7",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsNormal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/normal",
                    Units = "light",
                    Description = "A normal on-at-night light, matches v7",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirportBeacon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airport_beacon",
                    Units = "light",
                    Description = "An airport beacon light, RGBA is a homogenous normal vector",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirportBeaconFlash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airport_beacon_flash",
                    Units = "light",
                    Description = "An airport beacon light, RGBA is a homogenous normal vector",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsVasiPapi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/vasi_papi",
                    Units = "light",
                    Description = "VASI light, RG = rescaling of brightness",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsVasiPapiTint
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/vasi_papi_tint",
                    Units = "light",
                    Description = "VASI light, RG = rescaling of brightness",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsVasi3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/vasi3",
                    Units = "light",
                    Description = "VASI light, RG = rescaling of brightness, B= beam+, A=beam-",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsRabbit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/rabbit",
                    Units = "light",
                    Description = "RG = rescaling, B = flash frequency",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsRabbitSp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/rabbit_sp",
                    Units = "light",
                    Description = "RGB = ignored, A = frequency",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsStrobe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/strobe",
                    Units = "light",
                    Description = "RG = rescaling, B = flash frequency, A= phase",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsStrobeSp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/strobe_sp",
                    Units = "light",
                    Description = "RGB = ignored, A = frequency",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsWigwag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/wigwag",
                    Units = "light",
                    Description = "RG = rescaling, B = flash frequency, A= phase",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsWigwagSp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/wigwag_sp",
                    Units = "light",
                    Description = "RGB = ignored, A = phase, dx = frequency",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsCarrierWaveoff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/carrier_waveoff",
                    Units = "light",
                    Description = "A light that turns on when the carrier app is waved off",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsFresnelVertical
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/fresnel_vertical",
                    Units = "light",
                    Description = "A vertical fresnel.  RG = long range, BA = vert range",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsFresnelHorizontal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/fresnel_horizontal",
                    Units = "light",
                    Description = "A horizontal fresnel.  RG = long range, BA = vert range",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneLandingLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_landing_light",
                    Units = "light",
                    Description = "Airplane landing light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneLandingLightFlash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_landing_light_flash",
                    Units = "light",
                    Description = "Airplane landing light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneLandingLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_landing_light_spill",
                    Units = "light",
                    Description = "Airplane landing light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneGenericLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_generic_light",
                    Units = "light",
                    Description = "Airplane generic light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneGenericLightFlash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_generic_light_flash",
                    Units = "light",
                    Description = "Airplane generic light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneGenericLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_generic_light_spill",
                    Units = "light",
                    Description = "Airplane generic light.   RGB = dir, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneTaxiLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_taxi_light",
                    Units = "light",
                    Description = "AIrplane taxi light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneTaxiLightFlash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_taxi_light_flash",
                    Units = "light",
                    Description = "AIrplane taxi light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneTaxiLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_taxi_light_spill",
                    Units = "light",
                    Description = "AIrplane taxi light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneSpotLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_spot_light",
                    Units = "light",
                    Description = "AIrplane spot light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneSpotLightFlash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_spot_light_flash",
                    Units = "light",
                    Description = "AIrplane spot light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneSpotLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_spot_light_spill",
                    Units = "light",
                    Description = "AIrplane spot light.   RGB = dir, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneBeaconLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_beacon_light",
                    Units = "light",
                    Description = "Airplane beacon light.   RGB = ignore, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneNavigationLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_navigation_light",
                    Units = "light",
                    Description = "Airplane navigation light.   RGB = ignore, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneStrobeLight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_strobe_light",
                    Units = "light",
                    Description = "Airplane strobe light.   RGB = ignore, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneBeaconLightDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_beacon_light_dir",
                    Units = "light",
                    Description = "Airplane beacon light.   RGB = direction, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneNavigationLightDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_navigation_light_dir",
                    Units = "light",
                    Description = "Airplane navigation light.   RGB = ignored, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneStrobeLightDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_strobe_light_dir",
                    Units = "light",
                    Description = "Airplane strobe light.   RGB = direction, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneBeaconLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_beacon_light_spill",
                    Units = "light",
                    Description = "Airplane beacon light.   RGB = direction, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneNavigationLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_navigation_light_spill",
                    Units = "light",
                    Description = "Airplane navigation light.   RGB = ignored, A = reserved, use 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneStrobeLightSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_strobe_light_spill",
                    Units = "light",
                    Description = "Airplane strobe light.   RGB = direction, A = light number",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplanePanelSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_panel_spill",
                    Units = "light",
                    Description = "Internal panel floods, A = index",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneInstSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_inst_spill",
                    Units = "light",
                    Description = "Internal instrument post lights, A = index",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneBeaconLightRotate
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_beacon_light_rotate",
                    Units = "light",
                    Description = "Airplane beacon light for rotating beacon billboard.",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationLightsAirplaneBeaconLightRotateSpill
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/lights/airplane_beacon_light_rotate_spill",
                    Units = "light",
                    Description = "Airplane beacon light for rotating beacon spill.",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficTireSteerDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/tire_steer_deg",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficTireRotationAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/tire_rotation_angle_deg",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficWiperAngleDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/wiper_angle_deg",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficTowbarHeadingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/towbar_heading_deg",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficTowbarPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/towbar_pitch_deg",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficBeltLoaderHeightMeters
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/belt_loader_height_meters",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsAnimationGroundTrafficDoorOpen
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/animation/ground_traffic/door_open",
                    Units = "TODOV11",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsBackgroundRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/background_rgb",
                    Units = "RGB",
                    Description = "Background color behind a modal window",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsMenuDarkRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/menu_dark_rgb",
                    Units = "RGB",
                    Description = "Dark tinging for menus",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsMenuHiliteRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/menu_hilite_rgb",
                    Units = "RGB",
                    Description = "Menu color of a selected item",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsMenuLiteRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/menu_lite_rgb",
                    Units = "RGB",
                    Description = "Light tinging for menus",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsMenuTextRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/menu_text_rgb",
                    Units = "RGB",
                    Description = "Menu Item Text Color",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsMenuTextDisabledRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/menu_text_disabled_rgb",
                    Units = "RGB",
                    Description = "Menu Item Text Color When Disabled",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsSubtitleTextRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/subtitle_text_rgb",
                    Units = "RGB",
                    Description = "Subtitle text colors",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsTabFrontRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/tab_front_rgb",
                    Units = "RGB",
                    Description = "Color of text on tabs that are forward",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsTabBackRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/tab_back_rgb",
                    Units = "RGB",
                    Description = "Color of text on tabs that are in the bkgnd",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsCaptionTextRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/caption_text_rgb",
                    Units = "RGB",
                    Description = "Caption text (for on a main window)",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsListTextRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/list_text_rgb",
                    Units = "RGB",
                    Description = "Text Color for scrolling lists",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsGlassTextRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/glass_text_rgb",
                    Units = "RGB",
                    Description = "Text color for on a 'glass' screen",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsPlanePath13DRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/plane_path1_3d_rgb",
                    Units = "RGB",
                    Description = "Color for 3-d plane path",
                    
                };
            }
        }
        public static DataRefElement GraphicsColorsPlanePath23DRgb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/colors/plane_path2_3d_rgb",
                    Units = "RGB",
                    Description = "Striping color for 3-d plane path",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscShowPanelClickSpots
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/show_panel_click_spots",
                    Units = "boolean",
                    Description = "Show the clickable parts of the panel?",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscShowInstrumentDescriptions
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/show_instrument_descriptions",
                    Units = "boolean",
                    Description = "Show instrument descriptions on the panel?",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscCockpitLightLevelR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/cockpit_light_level_r",
                    Units = "ratio",
                    Description = "The red level for the cockpit 'night' tinting, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscCockpitLightLevelG
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/cockpit_light_level_g",
                    Units = "ratio",
                    Description = "The green level for the cockpit 'night' tinting, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscCockpitLightLevelB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/cockpit_light_level_b",
                    Units = "ratio",
                    Description = "The blue level for the cockpit 'night' tinting, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscOutsideLightLevelR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/outside_light_level_r",
                    Units = "ratio",
                    Description = "The red level for the world, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscOutsideLightLevelG
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/outside_light_level_g",
                    Units = "ratio",
                    Description = "The green level for the world, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscOutsideLightLevelB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/outside_light_level_b",
                    Units = "ratio",
                    Description = "The blue level for the world, from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscLightAttenuation
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/light_attenuation",
                    Units = "ratio",
                    Description = "Amount that artificial light is dimmed due to the sun's magnitude",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscUseProportionalFonts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/use_proportional_fonts",
                    Units = "boolean",
                    Description = "Set this to 1 to enable proportional fonts when available.",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscKillMapFmsLine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/kill_map_fms_line",
                    Units = "boolean",
                    Description = "If set to true, the map instruments won't draw the red FMS course line. SET THIS BACK TO 0 WHEN YOUR PLANE UNLOADS!",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscKillG1000Ah
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/kill_g1000_ah",
                    Units = "boolean",
                    Description = "If set to true, the G1000 PFD instrument won't draw the attitude indicator background. SET THIS BACK TO 0 WHEN YOUR PLANE UNLOADS!",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscRedFlashlightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/red_flashlight_on",
                    Units = "boolean",
                    Description = "Is the red flashlight on now.  Note that the flashlight is inop when HDR is off.",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscWhiteFlashlightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/white_flashlight_on",
                    Units = "boolean",
                    Description = "Is the white flashlight on now.  Note that the flashlight is inop when HDR is off.",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscKillRunwaySnow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/kill_runway_snow",
                    Units = "boolean",
                    Description = "Kill blowing snow effect on runways.",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscDefaultScrollPos
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/default_scroll_pos",
                    Units = "pixels",
                    Description = "Default position for a scrolling plane panel",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscCurrentScrollPos
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/current_scroll_pos",
                    Units = "pixels",
                    Description = "Current position of that panel",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscDefaultScrollPosX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/default_scroll_pos_x",
                    Units = "pixels",
                    Description = "Default position for a scrolling plane panel",
                    
                };
            }
        }
        public static DataRefElement GraphicsMiscCurrentScrollPosX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/misc/current_scroll_pos_x",
                    Units = "pixels",
                    Description = "Current position of that panel",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryCurrentPlanet
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/current_planet",
                    Units = "enum",
                    Description = "What planet are we on?  (Earth = 0, mars = 1)",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryPercentLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/percent_lights_on",
                    Units = "percent",
                    Description = "what percentage of city lites are on as night hits",
                    
                };
            }
        }
        public static DataRefElement GraphicsScenerySunPitchDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/sun_pitch_degrees",
                    Units = "degrees",
                    Description = "sun pitch from flat in OGL coordinates",
                    
                };
            }
        }
        public static DataRefElement GraphicsScenerySunHeadingDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/sun_heading_degrees",
                    Units = "degrees",
                    Description = "sun heading from true north in OGL coordinates",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryMoonPitchDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/moon_pitch_degrees",
                    Units = "degrees",
                    Description = "moon pitch from flat in OGL coordinates",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryMoonHeadingDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/moon_heading_degrees",
                    Units = "degrees",
                    Description = "moon heading from true north in OGL coordinates",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryAirportLightLevel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/airport_light_level",
                    Units = "ratio",
                    Description = "Level of airport light illumination, 0 = off, 1 = max brightness",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryAirportLightsOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/airport_lights_on",
                    Units = "boolean",
                    Description = "Are the airport lites on?  set override_airport_lites to 1t o write this.",
                    
                };
            }
        }
        public static DataRefElement GraphicsSceneryAsyncSceneryLoadInProgress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/scenery/async_scenery_load_in_progress",
                    Units = "boolean",
                    Description = "True when some kind of asynchronous scenery load or unload is in progress.",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsRenderingRes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/rendering_res",
                    Units = "int",
                    Description = "Texture Rendering Level",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsDimGload
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/dim_gload",
                    Units = "boolean",
                    Description = "dim under high g-load?",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsDrawForestfires
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/draw_forestfires",
                    Units = "boolean",
                    Description = "draw forest fires?",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsNonProportionalVerticalFOV
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/non_proportional_vertical_FOV",
                    Units = "boolean",
                    Description = "FOV is non proportional, use if sim/graphics/view/vertical_field_of_view_deg needs to be written to.",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsHDROn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/HDR_on",
                    Units = "boolean",
                    Description = "True if HDR rendering is enabled.",
                    
                };
            }
        }
        public static DataRefElement GraphicsSettingsScatteringOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/settings/scattering_on",
                    Units = "boolean",
                    Description = "True if atmospheric scattering is enabled.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_type",
                    Units = "enum",
                    Description = "The current camera view",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelRenderType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_render_type",
                    Units = "enum",
                    Description = "How to draw the panel?  0=2-d panel, 1=3-d, non-lit, 2=3-d, lit",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelRenderNewBlending
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_render_new_blending",
                    Units = "boolean",
                    Description = "True if modern correct RGBA blending is being used to draw panels; false if the legacy alpha-max equation is being used.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewWorldRenderType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/world_render_type",
                    Units = "enum",
                    Description = "What kind of 3-d rendering pass are we on? 0=normal,1=reflections in water",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPlaneRenderType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/plane_render_type",
                    Units = "enum",
                    Description = "What kind of rendering is being done during the xplm_Phase_Airplanes callback.  solid = 1, blended/alpha = 2 (0 = N/A, outside draw callback)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewDrawCallType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/draw_call_type",
                    Units = "enum",
                    Description = "Stereo status of the draw callback. 0 = not in draw callback, 1 = mono, 2 = direct stereo (only use XPLMDrawObjects), 3 = left eye, 4 = right eye",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewIsExternal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_is_external",
                    Units = "boolean",
                    Description = "Is the view in the cockpit or outside?  Affects sound!",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_x",
                    Units = "OGLcoords",
                    Description = "The location of the camera, X coordinate (OpenGL)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_y",
                    Units = "OGLcoords",
                    Description = "The location of the camera, Y coordinate (OpenGL)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_z",
                    Units = "OGLcoords",
                    Description = "The location of the camera, Z coordinate (OpenGL)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_pitch",
                    Units = "degrees",
                    Description = "The camera's pitch",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_roll",
                    Units = "degrees",
                    Description = "The camera's roll",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/view_heading",
                    Units = "degrees",
                    Description = "the camera's heading, CW frmo true north",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewCockpitPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/cockpit_pitch",
                    Units = "degrees",
                    Description = "The cockpit's pitch",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewCockpitRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/cockpit_roll",
                    Units = "degrees",
                    Description = "The cockpit's heading (the dataref name is wrong - this is really true heading)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewCockpitHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/cockpit_heading",
                    Units = "degrees",
                    Description = "the cockpit's roll (the dataref is name wrong - this is really roll)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewFieldOfViewDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/field_of_view_deg",
                    Units = "degrees",
                    Description = "horizontal field of view in degrees",
                    
                };
            }
        }

        private static DataRefElement graphicsViewVerticalFieldOfViewDeg = null;
        /// <summary>
        /// vertical field of view in degrees, see sim/graphics/settings/non_proportional_vertical_FOV.
        /// </summary>
        public static DataRefElement GraphicsViewVerticalFieldOfViewDeg {
            get {
                if (graphicsViewVerticalFieldOfViewDeg == null)
                { graphicsViewVerticalFieldOfViewDeg = GetDataRefElement("sim/graphics/view/vertical_field_of_view_deg", TypeStart_float, UtilConstants.Flag_No, "degrees", "vertical field of view in degrees, see sim/graphics/settings/non_proportional_vertical_FOV."); }
                return graphicsViewVerticalFieldOfViewDeg;
            }
        }

        public static DataRefElement GraphicsViewFieldOfViewVerticalDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/field_of_view_vertical_deg",
                    Units = "degrees",
                    Description = "pitch rotation for multi-monitor setup.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewFieldOfViewHorizontalDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/field_of_view_horizontal_deg",
                    Units = "degrees",
                    Description = "heading rotation for multi-monitor setup.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewFieldOfViewRollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/field_of_view_roll_deg",
                    Units = "degrees",
                    Description = "Roll rotation for multi-monitor setup.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewFieldOfViewHorizontalRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/field_of_view_horizontal_ratio",
                    Units = "ratio",
                    Description = "horizontal frustum shift (xp 6,7,8,10) - 1 unit shifts frustum by screen width",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewWindowWidth
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/window_width",
                    Units = "pixels",
                    Description = "Size of rendering window",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewWindowHeight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/window_height",
                    Units = "pixels",
                    Description = "Size of rendering window",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewVisibilityEffectiveM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/visibility_effective_m",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewVisibilityTerrainM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/visibility_terrain_m",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewVisibilityFramerateRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/visibility_framerate_ratio",
                    Units = "[0.0",
                    Description = "- 1.0]",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewVisibilityMathLevel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/visibility_math_level",
                    Units = "enum",
                    Description = "This used to indicate what level of 3-d force visualization was shown to the user in 3-d.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewCinemaVerite
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/cinema_verite",
                    Units = "boolean",
                    Description = "True if cinema verite camera is on.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalPnlL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_pnl_l",
                    Units = "pixels",
                    Description = "This is the location of the left edge of the panel, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalPnlB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_pnl_b",
                    Units = "pixels",
                    Description = "This is the location of the bottom edge of the panel, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalPnlR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_pnl_r",
                    Units = "pixels",
                    Description = "This is the location of the right edge of the panel, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalPnlT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_pnl_t",
                    Units = "pixels",
                    Description = "This is the location of the top edge of the panel, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisiblePnlL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_pnl_l",
                    Units = "pixels",
                    Description = "This is the location of the left edge of the screen, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisiblePnlB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_pnl_b",
                    Units = "pixels",
                    Description = "This is the location of the bottom edge of the screen, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisiblePnlR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_pnl_r",
                    Units = "pixels",
                    Description = "This is the location of the right edge of the screen, in the coordinate system used during panel drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisiblePnlT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_pnl_t",
                    Units = "pixels",
                    Description = "This is the location of the top edge of the screen, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalWinL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_win_l",
                    Units = "pixels",
                    Description = "This is the location of the left edge of the panel, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalWinB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_win_b",
                    Units = "pixels",
                    Description = "This is the location of the bottom edge of the panel, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalWinR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_win_r",
                    Units = "pixels",
                    Description = "This is the location of the right edge of the panel, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelTotalWinT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_total_win_t",
                    Units = "pixels",
                    Description = "This is the location of the top edge of the panel, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisibleWinL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_win_l",
                    Units = "pixels",
                    Description = "This is the location of the left edge of the screen, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisibleWinB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_win_b",
                    Units = "pixels",
                    Description = "This is the location of the bottom edge of the screen, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisibleWinR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_win_r",
                    Units = "pixels",
                    Description = "This is the location of the right edge of the screen, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPanelVisibleWinT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/panel_visible_win_t",
                    Units = "pixels",
                    Description = "This is the location of the top edge of the screen, in the coordinate system used during plugin window drawing.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_x",
                    Units = "meters",
                    Description = "Position of pilot's head relative to CG, X",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_y",
                    Units = "meters",
                    Description = "Position of pilot's head relative to CG, Y",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_z",
                    Units = "meters",
                    Description = "Position of pilot's head relative to CG, Z",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_psi",
                    Units = "degrees",
                    Description = "Position of pilot's head heading",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadThe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_the",
                    Units = "degrees",
                    Description = "Position of pilot's head pitch",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewPilotsHeadPhi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/pilots_head_phi",
                    Units = "degrees",
                    Description = "Position of the pilot's head roll'",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewClick3dX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/click_3d_x",
                    Units = "0-1",
                    Description = "coordinates of the panel click in 3-d",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewClick3dY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/click_3d_y",
                    Units = "0-1",
                    Description = "as texture coords (E.g. 0-1)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewClick3dXPixels
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/click_3d_x_pixels",
                    Units = "0-1",
                    Description = "coordinates of the panel click in 3-d",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewClick3dYPixels
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/click_3d_y_pixels",
                    Units = "0-1",
                    Description = "as texture coords (E.g. 0-1)",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewLocalMapL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/local_map_l",
                    Units = "pixels",
                    Description = "Bounds of the local map window during callbacks - left",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewLocalMapB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/local_map_b",
                    Units = "pixels",
                    Description = "Bounds of the local map window during callbacks - bottom",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewLocalMapR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/local_map_r",
                    Units = "pixels",
                    Description = "Bounds of the local map window during callbacks - right",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewLocalMapT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/local_map_t",
                    Units = "pixels",
                    Description = "Bounds of the local map window during callbacks - top",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewDomeNumberOfPasses
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/dome_number_of_passes",
                    Units = "count",
                    Description = "This is the number of passes to render for a multi-dome setup.  This x-plane feature requires a level-3 USB KEY",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewDomeOffsetHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/dome_offset_heading",
                    Units = "degrees",
                    Description = "The horizontal offset for this pass, in degrees",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewDomeOffsetPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/dome_offset_pitch",
                    Units = "degrees",
                    Description = "The vertical offset for this pass, in degrees",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewEqTrackir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/eq_trackir",
                    Units = "boolean",
                    Description = "Does the user have trackIR hardware enabled",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewHideYoke
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/hide_yoke",
                    Units = "boolean",
                    Description = "Is the yoke visible in the 3-d cockpit?",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewProjectionMatrix
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/projection_matrix",
                    Units = "Matrix4x4",
                    Description = "The current projection matrix - valid only during draw callbacks",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewProjectionMatrix3D
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/projection_matrix_3d",
                    Units = "Matrix4x4",
                    Description = "The current projection matrix during 3-d rendering - it keeps its value during 2-d rendering so youc can figure out how to apply text labels to 3-d stuff.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewWorldMatrix
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/world_matrix",
                    Units = "Matrix4x4",
                    Description = "The current modelview matrix to draw in standard \"OGL\" coordinates - valid only during draw callbacks",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewAcfMatrix
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/acf_matrix",
                    Units = "Matrix4x4",
                    Description = "A modelview matrix to draw in the user's aircraft coordinates - valid only during draw callbacks",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewModelviewMatrix
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/modelview_matrix",
                    Units = "Matrix4x4",
                    Description = "Current modelview matrix during window callbacks.  use to map boxels to device pixels for scissors.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewViewport
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/viewport",
                    Units = "Pixels",
                    Description = "Current OpenGL viewport in device window coordinates.  Note thiat this is left, bottom, right top, NOT left, bottom, width, height!!",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewIsReverseFloatZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/is_reverse_float_z",
                    Units = "Boolean",
                    Description = "True if current rendering is reverse-float-Z with clip control set ot 0..1.  False for conventional GL rendering.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewIsReverseY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/is_reverse_y",
                    Units = "Boolean",
                    Description = "True if current rendering is reversed-Y (+Y = down) - false for conventional +Y=up rendering.  This indicates how the framebuffer is set up - the transform stack will be set up to correctly flip Y if used.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewUsingModernDriver
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/using_modern_driver",
                    Units = "Boolean",
                    Description = "True if current rendering is being done by a modern Vulkan/Metal driver.  False if we have the GL driver.",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewCurrentGlFbo
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/current_gl_fbo",
                    Units = "GLenum",
                    Description = "During drawing and FL callbacks, this is the correct GL_FRAMEBUFFER binding to restore if you need to render off-screen.  Will be 0 when no bridge FBO is provided",
                    
                };
            }
        }
        public static DataRefElement GraphicsViewHardwareMsaaSamples
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/graphics/view/hardware_msaa_samples",
                    Units = "count",
                    Description = "This is the MSAA sample count for rendered windows.  Note that \"1\" means MSAA is not being used, not that MSAA is being used with a single sample count. ",
                    
                };
            }
        }
    }
}