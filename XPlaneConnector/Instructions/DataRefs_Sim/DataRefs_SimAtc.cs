﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement atcUserAircraftTransmitting = null;
        /// <summary>
        /// Is the Users/P0 aircraft currently transmitting on the radio?
        /// </summary>
        public static DataRefElement AtcUserAircraftTransmitting {
            get {
                if (atcUserAircraftTransmitting == null)
                { atcUserAircraftTransmitting = GetDataRefElement("sim/atc/user_aircraft_transmitting", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is the Users/P0 aircraft currently transmitting on the radio?"); }
                return atcUserAircraftTransmitting;
            }
        }

        private static DataRefElement atcCom1TunedFacility = null;
        /// <summary>
        /// The ATC facility type that the user's aircraft is tuned to. 0=None, 1=Del, 2=Gnd, 3=Twr, 4=Tracon, 5=Ctr
        /// </summary>
        public static DataRefElement AtcCom1TunedFacility {
            get {
                if (atcCom1TunedFacility == null)
                { atcCom1TunedFacility = GetDataRefElement("sim/atc/com1_tuned_facility", TypeStart_int, UtilConstants.Flag_No, "enum", "The ATC facility type that the user's aircraft is tuned to. 0=None, 1=Del, 2=Gnd, 3=Twr, 4=Tracon, 5=Ctr"); }
                return atcCom1TunedFacility;
            }
        }

        private static DataRefElement atcCom2TunedFacility = null;
        /// <summary>
        /// The ATC facility type that the user's aircraft is tuned to. 0=None, 1=Del, 2=Gnd, 3=Twr, 4=Tracon, 5=Ctr
        /// </summary>
        public static DataRefElement AtcCom2TunedFacility {
            get {
                if (atcCom2TunedFacility == null)
                { atcCom2TunedFacility = GetDataRefElement("sim/atc/com2_tuned_facility", TypeStart_int, UtilConstants.Flag_No, "enum", "The ATC facility type that the user's aircraft is tuned to. 0=None, 1=Del, 2=Gnd, 3=Twr, 4=Tracon, 5=Ctr"); }
                return atcCom2TunedFacility;
            }
        }

        private static DataRefElement atcCom1Active = null;
        /// <summary>
        /// Is the user's COM1 radio tuned to a freq that's actively being transmitted on by a pilot or ATC?
        /// </summary>
        public static DataRefElement AtcCom1Active {
            get {
                if (atcCom1Active == null)
                { atcCom1Active = GetDataRefElement("sim/atc/com1_active", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is the user's COM1 radio tuned to a freq that's actively being transmitted on by a pilot or ATC?"); }
                return atcCom1Active;
            }
        }

        private static DataRefElement atcCom2Active = null;
        /// <summary>
        /// Is the user's COM1 radio tuned to a freq that's actively being transmitted on by a pilot or ATC?
        /// </summary>
        public static DataRefElement AtcCom2Active {
            get {
                if (atcCom2Active == null)
                { atcCom2Active = GetDataRefElement("sim/atc/com2_active", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is the user's COM1 radio tuned to a freq that's actively being transmitted on by a pilot or ATC?"); }
                return atcCom2Active;
            }
        }

        private static DataRefElement atcAtisEnabled = null;
        /// <summary>
        /// Is the ATIS system enabled? If not, no ATIS text or audio will appear even when tuned to a proper frequency.
        /// </summary>
        public static DataRefElement AtcAtisEnabled {
            get {
                if (atcAtisEnabled == null)
                { atcAtisEnabled = GetDataRefElement("sim/atc/atis_enabled", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is the ATIS system enabled? If not, no ATIS text or audio will appear even when tuned to a proper frequency."); }
                return atcAtisEnabled;
            }
        }
    }
}