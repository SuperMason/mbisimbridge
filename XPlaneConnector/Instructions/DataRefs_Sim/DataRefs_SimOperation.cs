﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// If true, vertical guidance is a glide slope - otherwise it is a GPS vertical guidance indicator.  Comes from the physical units!
        /// </summary>
        public static DataRefElement OperationG430G430IsVloc
        { get { return GetDataRefElement("sim/operation/g430/G430_is_vloc", TypeStart_int2, UtilConstants.Flag_No, "boolean", "If true, vertical guidance is a glide slope - otherwise it is a GPS vertical guidance indicator.  Comes from the physical units!"); } }
        /// <summary>
        /// This is X-Plane's system native window as an int (either an HWND or WindowRef pre 102)
        /// </summary>
        public static DataRefElement OperationWindowsSystemWindow
        { get { return GetDataRefElement("sim/operation/windows/system_window", TypeStart_int, UtilConstants.Flag_No, "HWND", "This is X-Plane's system native window as an int (either an HWND or WindowRef pre 102)"); } }
        /// <summary>
        /// This is X-Plane's system native window as an array of two ints - low 32 bits first (either an HWND or WindowRef)
        /// </summary>
        public static DataRefElement OperationWindowsSystemWindow64
        { get { return GetDataRefElement("sim/operation/windows/system_window_64", TypeStart_int2, UtilConstants.Flag_No, "HWND", "This is X-Plane's system native window as an array of two ints - low 32 bits first (either an HWND or WindowRef)"); } }
    }
}