﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflClB
        { get { return GetDataRefElement("sim/airfoils/afl_clB", TypeStart_float56_2_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflAlminArray
        { get { return GetDataRefElement("sim/airfoils/afl_almin_array", TypeStart_float56_2_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflAlmaxArray
        { get { return GetDataRefElement("sim/airfoils/afl_almax_array", TypeStart_float56_2_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflReNum
        { get { return GetDataRefElement("sim/airfoils/afl_re_num", TypeStart_float56_2_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflTRat
        { get { return GetDataRefElement("sim/airfoils/afl_t_rat", TypeStart_float56_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflMachDiv
        { get { return GetDataRefElement("sim/airfoils/afl_mach_div", TypeStart_float56_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflClm
        { get { return GetDataRefElement("sim/airfoils/afl_clM", TypeStart_float56_2_2, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflCl
        { get { return GetDataRefElement("sim/airfoils/afl_cl", TypeStart_float56_2_2_721, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflCd
        { get { return GetDataRefElement("sim/airfoils/afl_cd", TypeStart_float56_2_2_721, UtilConstants.Flag_Yes, "???", "???"); } }
        /// <summary>
        /// ???
        /// </summary>
        public static DataRefElement AirfoilsAflCm
        { get { return GetDataRefElement("sim/airfoils/afl_cm", TypeStart_float56_2_2_721, UtilConstants.Flag_Yes, "???", "???"); } }
    }
}