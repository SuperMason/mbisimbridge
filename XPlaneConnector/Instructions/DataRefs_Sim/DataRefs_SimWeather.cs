﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement WeatherCloudType_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_type[0]",
                    Units = "Cloud",
                    Description = "enumeration",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudType_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_type[1]",
                    Units = "Cloud",
                    Description = "enumeration",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudType_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_type[2]",
                    Units = "Cloud",
                    Description = "enumeration",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudCoverage_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_coverage[0]",
                    Units = "Coverage",
                    Description = "0..6",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudCoverage_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_coverage[1]",
                    Units = "Coverage",
                    Description = "0..6",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudCoverage_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_coverage[2]",
                    Units = "Coverage",
                    Description = "0..6",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudBaseMslM_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_base_msl_m[0]",
                    Units = "meters",
                    Description = "MSL >= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudBaseMslM_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_base_msl_m[1]",
                    Units = "meters",
                    Description = "MSL >= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudBaseMslM_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_base_msl_m[2]",
                    Units = "meters",
                    Description = "MSL >= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudTopsMslM_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_tops_msl_m[0]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudTopsMslM_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_tops_msl_m[1]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherCloudTopsMslM_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/cloud_tops_msl_m[2]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherVisibilityReportedM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/visibility_reported_m",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherRainPercent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/rain_percent",
                    Units = "[0.0",
                    Description = "- 1.0]",
                    
                };
            }
        }
        public static DataRefElement WeatherThunderstormPercent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/thunderstorm_percent",
                    Units = "[0.0",
                    Description = "- 1.0]",
                    
                };
            }
        }
        public static DataRefElement WeatherWindTurbulencePercent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_turbulence_percent",
                    Units = "[0.0",
                    Description = "- 1.0]",
                    
                };
            }
        }
        public static DataRefElement WeatherBarometerSealevelInhg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/barometer_sealevel_inhg",
                    Units = "29.92",
                    Description = "+- ....",
                    
                };
            }
        }
        public static DataRefElement WeatherHasRealWeatherBool
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/has_real_weather_bool",
                    Units = "0,1",
                    Description = "Whether a real weather file has been located.",
                    
                };
            }
        }
        public static DataRefElement WeatherUseRealWeatherBool
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/use_real_weather_bool",
                    Units = "0,1",
                    Description = "Whether a real weather file is in use.",
                    
                };
            }
        }
        public static DataRefElement WeatherDownloadRealWeather
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/download_real_weather",
                    Units = "0,1",
                    Description = "If true, the sim will attempt to download real weather files when real weather is enabled.",
                    
                };
            }
        }
        public static DataRefElement WeatherSigma
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/sigma",
                    Units = "???",
                    Description = "The atmospheric density as a ratio compared to sea level.",
                    
                };
            }
        }
        public static DataRefElement WeatherRho
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/rho",
                    Units = "???",
                    Description = "The density of the air in kg/cubic meters.",
                    
                };
            }
        }
        public static DataRefElement WeatherBarometerCurrentInhg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/barometer_current_inhg",
                    Units = "29.92+-....",
                    Description = "This is the barometric pressure at the point the current flight is at.",
                    
                };
            }
        }
        public static DataRefElement WeatherGravityMss
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/gravity_mss",
                    Units = "meters/sec^2",
                    Description = "This is the acceleration of gravity for the current planet.",
                    
                };
            }
        }
        public static DataRefElement WeatherSpeedSoundMs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/speed_sound_ms",
                    Units = "meters/sec",
                    Description = "This is the speed of sound in meters/second at the plane's location.",
                    
                };
            }
        }
        public static DataRefElement WeatherWindAltitudeMslM_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_altitude_msl_m[0]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherWindAltitudeMslM_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_altitude_msl_m[1]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherWindAltitudeMslM_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_altitude_msl_m[2]",
                    Units = "meters",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherWindDirectionDegt_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_direction_degt[0]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherWindDirectionDegt_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_direction_degt[1]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherWindDirectionDegt_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_direction_degt[2]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherWindSpeedKt_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_speed_kt[0]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherWindSpeedKt_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_speed_kt[1]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherWindSpeedKt_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_speed_kt[2]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherShearDirectionDegt_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_direction_degt[0]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherShearDirectionDegt_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_direction_degt[1]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherShearDirectionDegt_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_direction_degt[2]",
                    Units = "[0",
                    Description = "- 360)",
                    
                };
            }
        }
        public static DataRefElement WeatherShearSpeedKt_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_speed_kt[0]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherShearSpeedKt_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_speed_kt[1]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherShearSpeedKt_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/shear_speed_kt[2]",
                    Units = "kts",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherTurbulence_0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/turbulence[0]",
                    Units = "[0",
                    Description = "- 10]",
                    
                };
            }
        }
        public static DataRefElement WeatherTurbulence_1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/turbulence[1]",
                    Units = "[0",
                    Description = "- 10]",
                    
                };
            }
        }
        public static DataRefElement WeatherTurbulence_2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/turbulence[2]",
                    Units = "[0",
                    Description = "- 10]",
                    
                };
            }
        }
        public static DataRefElement WeatherWaveAmplitude
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wave_amplitude",
                    Units = "meters",
                    Description = "Amplitude of waves in the water (height of waves)",
                    
                };
            }
        }
        public static DataRefElement WeatherWaveLength
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wave_length",
                    Units = "meters",
                    Description = "Length of a single wave in the water",
                    
                };
            }
        }
        public static DataRefElement WeatherWaveSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wave_speed",
                    Units = "meters/second",
                    Description = "Speed of water waves",
                    
                };
            }
        }
        public static DataRefElement WeatherWaveDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wave_dir",
                    Units = "degrees",
                    Description = "Direction of waves.",
                    
                };
            }
        }
        public static DataRefElement WeatherTemperatureSealevelC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/temperature_sealevel_c",
                    Units = "degreesC",
                    Description = "The temperature at sea level.",
                    
                };
            }
        }
        public static DataRefElement WeatherTemperatureTropoC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/temperature_tropo_c",
                    Units = "degreesC",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement WeatherTropoAltMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/tropo_alt_mtr",
                    Units = "meters",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement WeatherDewpoiSealevelC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/dewpoi_sealevel_c",
                    Units = "degreesC",
                    Description = "The dew point at sea level.",
                    
                };
            }
        }
        public static DataRefElement WeatherRelativeHumiditySealevelPercent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/relative_humidity_sealevel_percent",
                    Units = "percent",
                    Description = "Relative humidity at sea-level",
                    
                };
            }
        }
        public static DataRefElement WeatherTemperatureAmbientC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/temperature_ambient_c",
                    Units = "degreesC",
                    Description = "The air temperature outside the aircraft (at altitude).",
                    
                };
            }
        }
        public static DataRefElement WeatherTemperatureLeC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/temperature_le_c",
                    Units = "degreesC",
                    Description = "The air temperature at the leading edge of the wings in degrees C.",
                    
                };
            }
        }
        public static DataRefElement WeatherThermalPercent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/thermal_percent",
                    Units = "[0..1]",
                    Description = "The percentage of thermal occurance in the area.",
                    
                };
            }
        }
        public static DataRefElement WeatherThermalRateMs
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/thermal_rate_ms",
                    Units = "m/s",
                    Description = ">= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherThermalAltitudeMslM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/thermal_altitude_msl_m",
                    Units = "m",
                    Description = "MSL >= 0",
                    
                };
            }
        }
        public static DataRefElement WeatherRunwayFriction
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/runway_friction",
                    Units = "0,1,2",
                    Description = "The friction constant for runways (how wet they are).  0 = good, 1 = fair, 2 = poor",
                    
                };
            }
        }
        public static DataRefElement WeatherRunwayIsPatchy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/runway_is_patchy",
                    Units = "Booelan",
                    Description = "0 = uniform conditions, 1 = patchy conditions",
                    
                };
            }
        }
        public static DataRefElement WeatherWindDirectionDegt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_direction_degt",
                    Units = "[0-359)",
                    Description = "The effective direction of the wind at the plane's location.",
                    
                };
            }
        }
        public static DataRefElement WeatherWindSpeedKt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_speed_kt",
                    Units = "msc",
                    Description = ">= 0 The effective speed of the wind at the plane's location. WARNING: this dataref is in meters/second - the dataref NAME has a bug.",
                    
                };
            }
        }
        public static DataRefElement WeatherWindNowXMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_now_x_msc",
                    Units = "meters/sec",
                    Description = "Wind direction vector in OpenGL coordinates, X component.",
                    
                };
            }
        }
        public static DataRefElement WeatherWindNowYMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_now_y_msc",
                    Units = "meters/sec",
                    Description = "Wind direction vector in OpenGL coordinates, Y component.",
                    
                };
            }
        }
        public static DataRefElement WeatherWindNowZMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/wind_now_z_msc",
                    Units = "meters/sec",
                    Description = "Wind direction vector in OpenGL coordinates, Z component.",
                    
                };
            }
        }
        public static DataRefElement WeatherPrecipitationOnAircraftRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weather/precipitation_on_aircraft_ratio",
                    Units = "[0..1]",
                    Description = "The amount of rain on the airplane windshield as a ratio from 0 to 1.",
                    
                };
            }
        }
    }
}