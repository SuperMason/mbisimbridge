﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement graphicsVRButtonAxisX = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: the X deflection (or axis deflection) of each axis on the controller.
        /// </summary>
        public static DataRefElement GraphicsVRButtonAxisX {
            get {
                if (graphicsVRButtonAxisX == null)
                { graphicsVRButtonAxisX = GetDataRefElement("sim/graphics/VR/button_axis_x", TypeStart_float5, UtilConstants.Flag_No, "Ratio", "For OBJs used to draw VR controllers: the X deflection (or axis deflection) of each axis on the controller."); }
                return graphicsVRButtonAxisX;
            }
        }

        private static DataRefElement graphicsVRButtonAxisY = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: for 2-d thumb sticks or dpads, this is the second axis.
        /// </summary>
        public static DataRefElement GraphicsVRButtonAxisY {
            get {
                if (graphicsVRButtonAxisY == null)
                { graphicsVRButtonAxisY = GetDataRefElement("sim/graphics/VR/button_axis_y", TypeStart_float5, UtilConstants.Flag_No, "Ratio", "For OBJs used to draw VR controllers: for 2-d thumb sticks or dpads, this is the second axis."); }
                return graphicsVRButtonAxisY;
            }
        }
        
        private static DataRefElement graphicsVRButtonDown = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: button status of each button on the controller.
        /// </summary>
        public static DataRefElement GraphicsVRButtonDown {
            get {
                if (graphicsVRButtonDown == null)
                { graphicsVRButtonDown = GetDataRefElement("sim/graphics/VR/button_down", TypeStart_int64, UtilConstants.Flag_No, "Boolean", "For OBJs used to draw VR controllers: button status of each button on the controller."); }
                return graphicsVRButtonDown;
            }
        }

        private static DataRefElement graphicsVRBacklightLevel = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: Level of back lighting on the controller.
        /// </summary>
        public static DataRefElement GraphicsVRBacklightLevel {
            get {
                if (graphicsVRBacklightLevel == null)
                { graphicsVRBacklightLevel = GetDataRefElement("sim/graphics/VR/backlight_level", TypeStart_float, UtilConstants.Flag_No, "Ratio", "For OBJs used to draw VR controllers: Level of back lighting on the controller."); }
                return graphicsVRBacklightLevel;
            }
        }

        private static DataRefElement graphicsVRSeekerLightLevel = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: Level of lighting on the tip of the controller.
        /// </summary>
        public static DataRefElement GraphicsVRSeekerLightLevel {
            get {
                if (graphicsVRSeekerLightLevel == null)
                { graphicsVRSeekerLightLevel = GetDataRefElement("sim/graphics/VR/seeker_light_level", TypeStart_float, UtilConstants.Flag_No, "Ratio", "For OBJs used to draw VR controllers: Level of lighting on the tip of the controller."); }
                return graphicsVRSeekerLightLevel;
            }
        }

        private static DataRefElement graphicsVRTouchSpotLightLevel = null;
        /// <summary>
        /// For OBJs used to draw VR controllers: Level of lighting on the D-Pad/joystick part of the controller.
        /// </summary>
        public static DataRefElement GraphicsVRTouchSpotLightLevel {
            get {
                if (graphicsVRTouchSpotLightLevel == null)
                { graphicsVRTouchSpotLightLevel = GetDataRefElement("sim/graphics/VR/touch_spot_light_level", TypeStart_float, UtilConstants.Flag_No, "Ratio", "For OBJs used to draw VR controllers: Level of lighting on the D-Pad/joystick part of the controller."); }
                return graphicsVRTouchSpotLightLevel;
            }
        }

        private static DataRefElement graphicsVREnabled = null;
        /// <summary>
        /// True if VR is enabled, false if it is disabled
        /// </summary>
        public static DataRefElement GraphicsVREnabled {
            get {
                if (graphicsVREnabled == null)
                { graphicsVREnabled = GetDataRefElement("sim/graphics/VR/enabled", TypeStart_int, UtilConstants.Flag_No, "Boolean", "True if VR is enabled, false if it is disabled"); }
                return graphicsVREnabled;
            }
        }

        private static DataRefElement graphicsVRUsing3DMouse = null;
        /// <summary>
        /// True if the 3-d VR mouse is in use
        /// </summary>
        public static DataRefElement GraphicsVRUsing3DMouse {
            get {
                if (graphicsVRUsing3DMouse == null)
                { graphicsVRUsing3DMouse = GetDataRefElement("sim/graphics/VR/using_3d_mouse", TypeStart_int, UtilConstants.Flag_No, "Boolean", "True if the 3-d VR mouse is in use"); }
                return graphicsVRUsing3DMouse;
            }
        }

        private static DataRefElement graphicsVRTeleportOnGround = null;
        /// <summary>
        /// True if the user is teleporting outside the aircraft.  In this case the pilot's head position is not reliable - use the regular camera location
        /// </summary>
        public static DataRefElement GraphicsVRTeleportOnGround {
            get {
                if (graphicsVRTeleportOnGround == null)
                { graphicsVRTeleportOnGround = GetDataRefElement("sim/graphics/VR/teleport_on_ground", TypeStart_int, UtilConstants.Flag_No, "Boolean", "True if the user is teleporting outside the aircraft.  In this case the pilot's head position is not reliable - use the regular camera location"); }
                return graphicsVRTeleportOnGround;
            }
        }
    }
}