﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement JoystickHasJoystick
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_joystick",
                    Units = "boolean",
                    Description = "Do we have a joystick?",
                    
                };
            }
        }
        public static DataRefElement JoystickMouseIsJoystick
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/mouse_is_joystick",
                    Units = "boolean",
                    Description = "Is the mouse acting as a joystick?",
                    
                };
            }
        }
        public static DataRefElement JoystickMouseCanBeJoystick
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/mouse_can_be_joystick",
                    Units = "boolean",
                    Description = "Can the mouse be used to fly?  False if there is any LEGIT hardware plugged in.",
                    
                };
            }
        }
        public static DataRefElement JoystickYokePitchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yoke_pitch_ratio",
                    Units = "[-1..1]",
                    Description = "The deflection of the joystick axis controlling pitch. Use override_joystick or override_joystick_pitch",
                    
                };
            }
        }
        public static DataRefElement JoystickYolkPitchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yolk_pitch_ratio",
                    Units = "[-1..1]",
                    Description = "Legacy - this spelling error is still present for",
                    
                };
            }
        }
        public static DataRefElement JoystickYokeRollRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yoke_roll_ratio",
                    Units = "[-1..1]",
                    Description = "The deflection of the joystick axis controlling roll. Use override_joystick or override_joystick_roll",
                    
                };
            }
        }
        public static DataRefElement JoystickYolkRollRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yolk_roll_ratio",
                    Units = "[-1..1]",
                    Description = "Legacy - this spelling error is still present for backward compatibility with older plugins.",
                    
                };
            }
        }
        public static DataRefElement JoystickYokeHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yoke_heading_ratio",
                    Units = "[-1..1]",
                    Description = "The deflection of the joystick axis controlling yaw. Use override_joystick or override_joystick_heading",
                    
                };
            }
        }
        public static DataRefElement JoystickYolkHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/yolk_heading_ratio",
                    Units = "[-1..1]",
                    Description = "Legacy - this spelling error is still present for backward compatibility with older plugins.",
                    
                };
            }
        }
        public static DataRefElement JoystickArtstabPitchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/artstab_pitch_ratio",
                    Units = "[-1..1]",
                    Description = "The artificial stability input modifications for pitch. Use override_artstab",
                    
                };
            }
        }
        public static DataRefElement JoystickArtstabRollRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/artstab_roll_ratio",
                    Units = "[-1..1]",
                    Description = "The artificial stability input modifications for roll. Use override_artstab",
                    
                };
            }
        }
        public static DataRefElement JoystickArtstabHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/artstab_heading_ratio",
                    Units = "[-1..1]",
                    Description = "The artificial stability input modifications for yaw. Use override_artstab",
                    
                };
            }
        }
        public static DataRefElement JoystickServoPitchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/servo_pitch_ratio",
                    Units = "[-1..1]",
                    Description = "Servo input for pitch",
                    
                };
            }
        }
        public static DataRefElement JoystickServoRollRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/servo_roll_ratio",
                    Units = "[-1..1]",
                    Description = "Servo input for roll",
                    
                };
            }
        }
        public static DataRefElement JoystickServoHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/servo_heading_ratio",
                    Units = "[-1..1]",
                    Description = "Servo input for yaw",
                    
                };
            }
        }
        public static DataRefElement JoystickFCHdng
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/FC_hdng",
                    Units = "[-1..1]",
                    Description = "Flight Control Heading (sum of yoke plus artificial stability)",
                    
                };
            }
        }
        public static DataRefElement JoystickFCPtch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/FC_ptch",
                    Units = "[-1..1]",
                    Description = "Flight Control Pitch (sum of yoke plus artificial stability)",
                    
                };
            }
        }
        public static DataRefElement JoystickFCRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/FC_roll",
                    Units = "[-1..1]",
                    Description = "Flight Control Roll (sum of yoke plus artificial stability)",
                    
                };
            }
        }
        /// <summary>
        /// The nullzone size for the pitch axis (as of 940, one null zone serves all 3 axes)
        /// </summary>
        public static DataRefElement JoystickJoystickPitchNullzone
        { get { return GetDataRefElement("sim/joystick/joystick_pitch_nullzone", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The nullzone size for the pitch axis (as of 940, one null zone serves all 3 axes)"); } }
        /// <summary>
        /// The nullzone size for the roll axis
        /// </summary>
        public static DataRefElement JoystickJoystickRollNullzone
        { get { return GetDataRefElement("sim/joystick/joystick_roll_nullzone", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The nullzone size for the roll axis"); } }
        /// <summary>
        /// The nullzone size for the heading axis
        /// </summary>
        public static DataRefElement JoystickJoystickHeadingNullzone
        { get { return GetDataRefElement("sim/joystick/joystick_heading_nullzone", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The nullzone size for the heading axis"); } }
        /// <summary>
        /// Joystick center for pitch axis
        /// </summary>
        public static DataRefElement JoystickJoystickPitchCenter
        { get { return GetDataRefElement("sim/joystick/joystick_pitch_center", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Joystick center for pitch axis"); } }
        /// <summary>
        /// Joystick center for roll axis
        /// </summary>
        public static DataRefElement JoystickJoystickRollCenter
        { get { return GetDataRefElement("sim/joystick/joystick_roll_center", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Joystick center for roll axis"); } }
        /// <summary>
        /// Joystick center for heading axis
        /// </summary>
        public static DataRefElement JoystickJoystickHeadingCenter
        { get { return GetDataRefElement("sim/joystick/joystick_heading_center", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Joystick center for heading axis"); } }
        /// <summary>
        /// Amount of artificial stability to add to the pitch.  This is AS set by user, not in Plane-Maker.
        /// </summary>
        public static DataRefElement JoystickJoystickPitchAugment
        { get { return GetDataRefElement("sim/joystick/joystick_pitch_augment", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Amount of artificial stability to add to the pitch.  This is AS set by user, not in Plane-Maker."); } }
        /// <summary>
        /// Amount of artificial stability to add to the roll.  This is AS set by user, not in Plane-Maker.
        /// </summary>
        public static DataRefElement JoystickJoystickRollAugment
        { get { return GetDataRefElement("sim/joystick/joystick_roll_augment", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Amount of artificial stability to add to the roll.  This is AS set by user, not in Plane-Maker."); } }
        /// <summary>
        /// Amount of artificial stability to add to the heading.  This is AS set by user, not in Plane-Maker.
        /// </summary>
        public static DataRefElement JoystickJoystickHeadingAugment
        { get { return GetDataRefElement("sim/joystick/joystick_heading_augment", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Amount of artificial stability to add to the heading.  This is AS set by user, not in Plane-Maker."); } }
        /// <summary>
        /// The sensitivity for the pitch axis (the power ratio)
        /// </summary>
        public static DataRefElement JoystickJoystickPitchSensitivity
        { get { return GetDataRefElement("sim/joystick/joystick_pitch_sensitivity", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The sensitivity for the pitch axis (the power ratio)"); } }
        /// <summary>
        /// The sensitivity for the roll axis
        /// </summary>
        public static DataRefElement JoystickJoystickRollSensitivity
        { get { return GetDataRefElement("sim/joystick/joystick_roll_sensitivity", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The sensitivity for the roll axis"); } }
        /// <summary>
        /// The sensitivity for the heading axis
        /// </summary>
        public static DataRefElement JoystickJoystickHeadingSensitivity
        { get { return GetDataRefElement("sim/joystick/joystick_heading_sensitivity", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "The sensitivity for the heading axis"); } }
        public static DataRefElement JoystickJoystickAxisAssignments
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_axis_assignments",
                    Units = "enums",
                    Description = "Assignments for the joystick axes - what does X-Plane think each one is? [Was 15 before 850][Was 24 before 860][was 32 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickButtonAssignments
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_button_assignments",
                    Units = "enums",
                    Description = "Assignments for the joystick buttons - what does each one do? [Was 64 before 850][was 160 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickAxisReverse
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_axis_reverse",
                    Units = "boolean",
                    Description = "Reverse this axis? [Was 15 before 850][Was 24 before 860][was 32 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickAxisValues
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_axis_values",
                    Units = "ratio",
                    Description = "The deflection of this joystick [Was 15 before 850][Was 24 before 860][was 32 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickAxisMinimum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_axis_minimum",
                    Units = "ratio",
                    Description = "Min raw value ever seen [Was 15 before 850][Was 24 before 860][was 32 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickAxisMaximum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_axis_maximum",
                    Units = "ratio",
                    Description = "Max raw value ever seen [Was 15 before 850][Was 24 before 860][was 32 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoystickButtonValues
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joystick_button_values",
                    Units = "boolean",
                    Description = "Is this button pressed? [Was 64 before 850][was 160 before 900]",
                    
                };
            }
        }
        public static DataRefElement JoystickJoyMappedAxisAvail
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joy_mapped_axis_avail",
                    Units = "boolean",
                    Description = "True if there is joystick hardware of some kind providing this axis right now.",
                    
                };
            }
        }
        public static DataRefElement JoystickJoyMappedAxisValue
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/joy_mapped_axis_value",
                    Units = "float",
                    Description = "Fully processed ratio for each axis - either -1 to 1 or 0 to 1.",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPedNobrk
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_ped_nobrk",
                    Units = "boolean",
                    Description = "Hardware settings: Pedal nobrk (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPedWibrk
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_ped_wibrk",
                    Units = "boolean",
                    Description = "Hardware settings: Pedal wlbrk (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcPedals
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_pedals",
                    Units = "boolean",
                    Description = "Equipment settings: PFC pedal (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcYoke
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_yoke",
                    Units = "boolean",
                    Description = "Equipment settings: PFC yoke (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcThrot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_throt",
                    Units = "boolean",
                    Description = "Equipment settings: PFC throttle (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcAvio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_avio",
                    Units = "boolean",
                    Description = "Equipment settings: PFC avionics (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcCentercon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_centercon",
                    Units = "boolean",
                    Description = "Equipment settings: PFC center console (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcElecTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_elec_trim",
                    Units = "boolean",
                    Description = "Equipment settings: PFC electric trim (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcBrakeTog
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_brake_tog",
                    Units = "boolean",
                    Description = "Equipment settings: PFC brake toggle (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickEqPfcDualCowl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/eq_pfc_dual_cowl",
                    Units = "boolean",
                    Description = "Equipment settings: PFC dual cowl (writable since 930 to auto-set user preferences)",
                    
                };
            }
        }
        public static DataRefElement JoystickFireKeyIsDown
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/fire_key_is_down",
                    Units = "boolean",
                    Description = "True if the fire key is held down",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_roll",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the roll.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_pitch",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the pitch.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_heading",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the heading.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedBrakes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_brakes",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the brakes.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedThrottle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_throttle",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the throttle.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_prop",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the pitch.",
                    
                };
            }
        }
        public static DataRefElement JoystickHasCertifiedMixture
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/joystick/has_certified_mixture",
                    Units = "boolean",
                    Description = "True if there is FAA-certified hardware controlling the mixture.",
                    
                };
            }
        }
    }
}
