﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement networkDataoutNetworkDataRate = null;
        /// <summary>
        /// Rate to send data over UDP at, range ::: 0 ~ 99
        /// </summary>
        public static DataRefElement NetworkDataoutNetworkDataRate {
            get {
                if (networkDataoutNetworkDataRate == null)
                { networkDataoutNetworkDataRate = GetDataRefElement("sim/network/dataout/network_data_rate", TypeStart_int, UtilConstants.Flag_Yes, "hz", "Rate to send data over UDP at"); }
                return networkDataoutNetworkDataRate;
            }
        }

        private static DataRefElement networkDataoutDataToInternet = null;
        /// <summary>
        /// Enable data output of this data ref to internet, UI ::: 'Network via UDP'
        /// <para>index 0 : "Frame rate"</para>
        /// <para>index 1 : "Times"</para>
        /// <para>index 2 : "Sim stats"</para>
        /// <para>index ... : ""</para>
        /// <para>index 199 : ""</para>
        /// </summary>
        public static DataRefElement NetworkDataoutDataToInternet {
            get {
                if (networkDataoutDataToInternet == null)
                { networkDataoutDataToInternet = GetDataRefElement("sim/network/dataout/data_to_internet[0]", TypeStart_int200, UtilConstants.Flag_Yes, "boolean", "Enable data output of this data ref to internet, UI ::: 'Network via UDP'"); }
                return networkDataoutDataToInternet;
            }
        }

        private static DataRefElement networkDataoutDataToDisk = null;
        /// <summary>
        /// Enable data output of this data ref to disk file, UI ::: 'Disk (data.txt File)'
        /// <para>index 0 : "Frame rate"</para>
        /// <para>index 1 : "Times"</para>
        /// <para>index 2 : "Sim stats"</para>
        /// <para>index ... : ""</para>
        /// <para>index 199 : ""</para>
        /// </summary>
        public static DataRefElement NetworkDataoutDataToDisk {
            get {
                if (networkDataoutDataToDisk == null)
                { networkDataoutDataToDisk = GetDataRefElement("sim/network/dataout/data_to_disk[0]", TypeStart_int200, UtilConstants.Flag_Yes, "boolean", "Enable data output of this data ref to disk file, UI ::: 'Disk (data.txt File)'"); }
                return networkDataoutDataToDisk;
            }
        }

        private static DataRefElement networkDataoutDataToGraph = null;
        /// <summary>
        /// Enable data output of this data ref to graph, UI ::: 'Data Graph Window'
        /// <para>index 0 : "Frame rate"</para>
        /// <para>index 1 : "Times"</para>
        /// <para>index 2 : "Sim stats"</para>
        /// <para>index ... : ""</para>
        /// <para>index 199 : ""</para>
        /// </summary>
        public static DataRefElement NetworkDataoutDataToGraph {
            get {
                if (networkDataoutDataToGraph == null)
                { networkDataoutDataToGraph = GetDataRefElement("sim/network/dataout/data_to_graph[0]", TypeStart_int200, UtilConstants.Flag_Yes, "boolean", "Enable data output of this data ref to graph, UI ::: 'Data Graph Window'"); }
                return networkDataoutDataToGraph;
            }
        }

        private static DataRefElement networkDataoutDataToScreen = null;
        /// <summary>
        /// Enable data output of this data ref to screen, UI ::: 'Show in Cockpit'
        /// <para>index 0 : "Frame rate"</para>
        /// <para>index 1 : "Times"</para>
        /// <para>index 2 : "Sim stats"</para>
        /// <para>index ... : ""</para>
        /// <para>index 199 : ""</para>
        /// </summary>
        public static DataRefElement NetworkDataoutDataToScreen {
            get {
                if (networkDataoutDataToScreen == null)
                { networkDataoutDataToScreen = GetDataRefElement("sim/network/dataout/data_to_screen", TypeStart_int200, UtilConstants.Flag_Yes, "boolean", "Enable data output of this data ref to screen, UI ::: 'Show in Cockpit'"); }
                return networkDataoutDataToScreen;
            }
        }

        private static DataRefElement networkDataoutDumpPartsProps = null;
        /// <summary>
        /// Dump extra prop data to screen
        /// </summary>
        public static DataRefElement NetworkDataoutDumpPartsProps {
            get {
                if (networkDataoutDumpPartsProps == null)
                { networkDataoutDumpPartsProps = GetDataRefElement("sim/network/dataout/dump_parts_props", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Dump extra prop data to screen"); }
                return networkDataoutDumpPartsProps;
            }
        }

        private static DataRefElement networkDataoutDumpPartsWings = null;
        /// <summary>
        /// Dump wing prop data to screen
        /// </summary>
        public static DataRefElement NetworkDataoutDumpPartsWings {
            get {
                if (networkDataoutDumpPartsWings == null)
                { networkDataoutDumpPartsWings = GetDataRefElement("sim/network/dataout/dump_parts_wings", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Dump wing prop data to screen"); }
                return networkDataoutDumpPartsWings;
            }
        }

        private static DataRefElement networkDataoutDumpPartsVstabs = null;
        /// <summary>
        /// Dump vertical stabilizer prop data to screen
        /// </summary>
        public static DataRefElement NetworkDataoutDumpPartsVstabs {
            get {
                if (networkDataoutDumpPartsVstabs == null)
                { networkDataoutDumpPartsVstabs = GetDataRefElement("sim/network/dataout/dump_parts_vstabs", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Dump vertical stabilizer prop data to screen"); }
                return networkDataoutDumpPartsVstabs;
            }
        }

        private static DataRefElement networkDataoutIsExternalVisual = null;
        /// <summary>
        /// Is this machine running as an external visual for another X-Plane machine.
        /// </summary>
        public static DataRefElement NetworkDataoutIsExternalVisual {
            get {
                if (networkDataoutIsExternalVisual == null)
                { networkDataoutIsExternalVisual = GetDataRefElement("sim/network/dataout/is_external_visual", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is this machine running as an external visual for another X-Plane machine."); }
                return networkDataoutIsExternalVisual;
            }
        }

        private static DataRefElement networkDataoutIsMultiplayerSession = null;
        /// <summary>
        /// Is this machine part of an x-plane built-in multiplayer session?
        /// </summary>
        public static DataRefElement NetworkDataoutIsMultiplayerSession {
            get {
                if (networkDataoutIsMultiplayerSession == null)
                { networkDataoutIsMultiplayerSession = GetDataRefElement("sim/network/dataout/is_multiplayer_session", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is this machine part of an x-plane built-in multiplayer session?"); }
                return networkDataoutIsMultiplayerSession;
            }
        }

        private static DataRefElement networkDataoutMultiplayerIp = null;
        /// <summary>
        /// IP addresses of multiplayer players (or 0 if not in used in v10)
        /// </summary>
        public static DataRefElement NetworkDataoutMultiplayerIp {
            get {
                if (networkDataoutMultiplayerIp == null)
                { networkDataoutMultiplayerIp = GetDataRefElement("sim/network/dataout/multiplayer_ip", TypeStart_int19, UtilConstants.Flag_No, "ip", "IP addresses of multiplayer players (or 0 if not in used in v10)"); }
                return networkDataoutMultiplayerIp;
            }
        }

        private static DataRefElement networkDataoutExternalVisualIp = null;
        /// <summary>
        /// IP addresses of external visuals (or 0 if not in use in v10).  Dim 8 in v9.
        /// </summary>
        public static DataRefElement NetworkDataoutExternalVisualIp {
            get {
                if (networkDataoutExternalVisualIp == null)
                { networkDataoutExternalVisualIp = GetDataRefElement("sim/network/dataout/external_visual_ip", TypeStart_int20, UtilConstants.Flag_No, "ip", "IP addresses of external visuals (or 0 if not in use in v10).  Dim 8 in v9."); }
                return networkDataoutExternalVisualIp;
            }
        }

        private static DataRefElement networkDataoutTrackExternalVisual = null;
        /// <summary>
        /// True if this remote visual exists
        /// </summary>
        public static DataRefElement NetworkDataoutTrackExternalVisual {
            get {
                if (networkDataoutTrackExternalVisual == null)
                { networkDataoutTrackExternalVisual = GetDataRefElement("sim/network/dataout/track_external_visual", TypeStart_int20, UtilConstants.Flag_No, "boolean", "True if this remote visual exists"); }
                return networkDataoutTrackExternalVisual;
            }
        }

        private static DataRefElement networkDataoutExvisTracksView = null;
        /// <summary>
        /// True if this remote visual tracks the master's view changes
        /// </summary>
        public static DataRefElement NetworkDataoutExvisTracksView {
            get {
                if (networkDataoutExvisTracksView == null)
                { networkDataoutExvisTracksView = GetDataRefElement("sim/network/dataout/exvis_tracks_view", TypeStart_int20, UtilConstants.Flag_Yes, "boolean", "True if this remote visual tracks the master's view changes"); }
                return networkDataoutExvisTracksView;
            }
        }

        private static DataRefElement networkMiscOpentransportInited = null;
        /// <summary>
        /// Has x-plane already inited Open Transport?  YOU SHOULD NEVER USE THIS DATAREF!!!
        /// </summary>
        public static DataRefElement NetworkMiscOpentransportInited {
            get {
                if (networkMiscOpentransportInited == null)
                { networkMiscOpentransportInited = GetDataRefElement("sim/network/misc/opentransport_inited", TypeStart_int, UtilConstants.Flag_No, "boolean", "Has x-plane already inited Open Transport?  YOU SHOULD NEVER USE THIS DATAREF!!!"); }
                return networkMiscOpentransportInited;
            }
        }

        private static DataRefElement networkMiscNetworkTimeSec = null;
        /// <summary>
        /// The current elapsed time synched across the network
        /// </summary>
        public static DataRefElement NetworkMiscNetworkTimeSec {
            get {
                if (networkMiscNetworkTimeSec == null)
                { networkMiscNetworkTimeSec = GetDataRefElement("sim/network/misc/network_time_sec", TypeStart_float, UtilConstants.Flag_No, "seconds", "The current elapsed time synched across the network"); }
                return networkMiscNetworkTimeSec;
            }
        }
    }
}