﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement WeaponsWeaponCount
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/weapon_count",
                    Units = "int",
                    Description = "This is the number of weapons available via datarefs.",
                    
                };
            }
        }
        public static DataRefElement WeaponsType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/type",
                    Units = "enum",
                    Description = "(V10 style!) For the geometry we scan right into the acf structure unused array-spaces.",
                    
                };
            }
        }
        public static DataRefElement WeaponsFreeFlyer
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/free_flyer",
                    Units = "enum",
                    Description = "(V10 style!) Weapon can go fly free...",
                    
                };
            }
        }
        public static DataRefElement WeaponsActionMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/action_mode",
                    Units = "enum",
                    Description = "(V10 style!) 0 = In carriage/still in the gun/reloaded, 1 = firing, and 2 = destroyed",
                    
                };
            }
        }
        public static DataRefElement WeaponsXWpnAtt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/x_wpn_att",
                    Units = "???",
                    Description = "This allows us to use our drawing, smoothing, editing, s-t, normal-vector, plotting, etc.",
                    
                };
            }
        }
        public static DataRefElement WeaponsYWpnAtt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/y_wpn_att",
                    Units = "???",
                    Description = "Code to do the weapons as well as everything else.",
                    
                };
            }
        }
        public static DataRefElement WeaponsZWpnAtt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/z_wpn_att",
                    Units = "float",
                    Description = "(placeholder) ",
                    
                };
            }
        }
        public static DataRefElement WeaponsCgy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/cgY",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsCgz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/cgZ",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsLasRange
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/las_range",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsConvRange
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/conv_range",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsBulRoundsPerSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/bul_rounds_per_sec",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsBulRounds
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/bul_rounds",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsBulMuzzleSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/bul_muzzle_speed",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsBulArea
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/bul_area",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsAddedMass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/added_mass",
                    Units = "???",
                    Description = "Mass in addition to ammo... like the gun itself, and bomb racks.",
                    
                };
            }
        }
        public static DataRefElement WeaponsTotalWeaponMassMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/total_weapon_mass_max",
                    Units = "???",
                    Description = "Warhead and casing, fuel and tank",
                    
                };
            }
        }
        public static DataRefElement WeaponsFuelWarheadMassMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/fuel_warhead_mass_max",
                    Units = "???",
                    Description = "Remember we can have drop tanks",
                    
                };
            }
        }
        public static DataRefElement WeaponsWarheadType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/warhead_type",
                    Units = "enum",
                    Description = "(V10 style!) Conventional or nuke",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisDragCo
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_drag_co",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisDragChuteS
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_drag_chute_S",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisConeWidth
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_cone_width",
                    Units = "???",
                    Description = "For heat or radar-guided, you must keep the target within this cone to track him",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisCratPerDegBore
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_crat_per_deg_bore",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisCratPerDegpersecBore
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_crat_per_degpersec_bore",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisCratPerDegpersec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_crat_per_degpersec",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsGunDelPsiDegMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/gun_del_psi_deg_max",
                    Units = "???",
                    Description = "Aimable guns for c130 Spectre, etc",
                    
                };
            }
        }
        public static DataRefElement WeaponsGunDelTheDegMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/gun_del_the_deg_max",
                    Units = "???",
                    Description = "Aimable guns for c130 Spectre, etc",
                    
                };
            }
        }
        public static DataRefElement WeaponsSFrn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/s_frn",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsSSid
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/s_sid",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsSTop
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/s_top",
                    Units = "???",
                    Description = "Area",
                    
                };
            }
        }
        public static DataRefElement WeaponsXBodyAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/X_body_aero",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsYBodyAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Y_body_aero",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsZBodyAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Z_body_aero",
                    Units = "???",
                    Description = "Centroid",
                    
                };
            }
        }
        public static DataRefElement WeaponsJxxUnitmass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Jxx_unitmass",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsJyyUnitmass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Jyy_unitmass",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsJzzUnitmass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Jzz_unitmass",
                    Units = "???",
                    Description = "MI",
                    
                };
            }
        }
        public static DataRefElement WeaponsTargetIndex
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/target_index",
                    Units = "int",
                    Description = "Target index",
                    
                };
            }
        }
        public static DataRefElement WeaponsTargLat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/targ_lat",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsTargLon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/targ_lon",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsTargH
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/targ_h",
                    Units = "???",
                    Description = "Bomb targets",
                    
                };
            }
        }
        public static DataRefElement WeaponsDelPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/del_psi",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsDelThe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/del_the",
                    Units = "???",
                    Description = "Delta to target, for data output",
                    
                };
            }
        }
        public static DataRefElement WeaponsRuddRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/rudd_rat",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsElevRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/elev_rat",
                    Units = "???",
                    Description = "Rudder and elevator steering inputs",
                    
                };
            }
        }
        public static DataRefElement WeaponsVMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/V_msc",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsAVMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/AV_msc",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsDistTarg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/dist_targ",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsDistPoint
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/dist_point",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsTimePoint
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/time_point",
                    Units = "???",
                    Description = "Speed and dist to target for data output",
                    
                };
            }
        }
        public static DataRefElement WeaponsFxAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/fx_axis",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsFyAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/fy_axis",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsFzAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/fz_axis",
                    Units = "???",
                    Description = "Flight-status",
                    
                };
            }
        }
        public static DataRefElement WeaponsVx
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/vx",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsVy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/vy",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsVz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/vz",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/x",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/y",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/z",
                    Units = "???",
                    Description = "Flight-status",
                    
                };
            }
        }
        public static DataRefElement WeaponsL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/L",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/M",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsN
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/N",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsPrad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Prad",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsQrad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Qrad",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsRrad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/Rrad",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsThe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/the",
                    Units = "Deg",
                    Description = "Angle relative to the aircraft",
                    
                };
            }
        }
        public static DataRefElement WeaponsPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/psi",
                    Units = "Deg",
                    Description = "Angle relative to the aircraft",
                    
                };
            }
        }
        public static DataRefElement WeaponsPhi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/phi",
                    Units = "Deg",
                    Description = "Angle relative to the aircraft",
                    
                };
            }
        }
        public static DataRefElement WeaponsNextBullTime
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/next_bull_time",
                    Units = "???",
                    Description = "For guns, this is the next fire time",
                    
                };
            }
        }
        public static DataRefElement WeaponsTotalWeaponMassNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/total_weapon_mass_now",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsFuelWarheadMassNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/fuel_warhead_mass_now",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisThrust1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_thrust1",
                    Units = "???",
                    Description = "delay",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisThrust2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_thrust2",
                    Units = "???",
                    Description = "boost",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisThrust3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_thrust3",
                    Units = "???",
                    Description = "sustain",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisDuration1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_duration1",
                    Units = "???",
                    Description = "delay",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisDuration2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_duration2",
                    Units = "???",
                    Description = "boost",
                    
                };
            }
        }
        public static DataRefElement WeaponsMisDuration3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/mis_duration3",
                    Units = "???",
                    Description = "sustain",
                    
                };
            }
        }
        public static DataRefElement WeaponsQ1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/q1",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsQ2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/q2",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsQ3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/q3",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsQ4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/q4",
                    Units = "float",
                    Description = "(placeholder)",
                    
                };
            }
        }
        public static DataRefElement WeaponsArmed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/armed",
                    Units = "enum",
                    Description = "0 = safe (will NOT explode), 1 = armed (WILL explode)",
                    
                };
            }
        }
        public static DataRefElement WeaponsFiring
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/firing",
                    Units = "enum",
                    Description = "0 = not firing, 1 = firing (This is the launcher that the wpn is attached to, for actual wpn status, also check action_mode and/or next_bull_time)",
                    
                };
            }
        }
        public static DataRefElement WeaponsShellIsAttached
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/shell/is_attached",
                    Units = "V11TODO",
                    Description = "TODO",
                    
                };
            }
        }
        public static DataRefElement WeaponsShellThrustRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/weapons/shell/thrust_rat",
                    Units = "V11TODO",
                    Description = "TODO",
                    
                };
            }
        }
    }
}