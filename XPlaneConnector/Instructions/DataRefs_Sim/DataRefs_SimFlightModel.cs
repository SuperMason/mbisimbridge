﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement flightmodelControlsSbrkrat = null;
        /// <summary>
        /// Actual speed brake deployment [0..1 = schedule for in-air, 1..1.5 = extra deployment when on ground]
        /// </summary>
        public static DataRefElement FlightmodelControlsSbrkrat {
            get {
                if (flightmodelControlsSbrkrat == null)
                { flightmodelControlsSbrkrat = GetDataRefElement("sim/flightmodel/controls/sbrkrat", TypeStart_float, UtilConstants.Flag_Yes, "[0..1.5]", "Actual speed brake deployment [0..1 = schedule for in-air, 1..1.5 = extra deployment when on ground]"); }
                return flightmodelControlsSbrkrat;
            }
        }







        private static DataRefElement flightmodelControlsFlaprqst = null;
        /// <summary>
        /// Requested flap deployment, 0 = off, 1 = max
        /// </summary>
        public static DataRefElement FlightmodelControlsFlaprqst {
            get {
                if (flightmodelControlsFlaprqst == null)
                { flightmodelControlsFlaprqst = GetDataRefElement("sim/flightmodel/controls/flaprqst", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Requested flap deployment, 0 = off, 1 = max"); }
                return flightmodelControlsFlaprqst;
            }
        }

        private static DataRefElement flightmodelControlsTailLockRat = null;
        /// <summary>
        /// This is how locked the tail-wheel is ... 0 is free castoring, 1 is locked.
        /// </summary>
        public static DataRefElement FlightmodelControlsTailLockRat {
            get {
                if (flightmodelControlsTailLockRat == null)
                { flightmodelControlsTailLockRat = GetDataRefElement("sim/flightmodel/controls/tail_lock_rat", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is how locked the tail-wheel is ... 0 is free castoring, 1 is locked."); }
                return flightmodelControlsTailLockRat;
            }
        }

        private static DataRefElement flightmodelControlsAilTrim = null;
        /// <summary>
        /// Current Aileron Trim, -1 = max left, 1 = max right
        /// </summary>
        public static DataRefElement FlightmodelControlsAilTrim {
            get {
                if (flightmodelControlsAilTrim == null)
                { flightmodelControlsAilTrim = GetDataRefElement("sim/flightmodel/controls/ail_trim", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "Current Aileron Trim, -1 = max left, 1 = max right"); }
                return flightmodelControlsAilTrim;
            }
        }

        private static DataRefElement flightmodelControlsDist = null;
        /// <summary>
        /// Distance Traveled
        /// </summary>
        public static DataRefElement FlightmodelControlsDist {
            get {
                if (flightmodelControlsDist == null)
                { flightmodelControlsDist = GetDataRefElement("sim/flightmodel/controls/dist", TypeStart_float, UtilConstants.Flag_Yes, "meters", "Distance Traveled"); }
                return flightmodelControlsDist;
            }
        }

        private static DataRefElement flightmodelControlsElvTrim = null;
        /// <summary>
        /// Elevation Trim, -1 = max nose down, 1 = max nose up
        /// </summary>
        public static DataRefElement FlightmodelControlsElvTrim {
            get {
                if (flightmodelControlsElvTrim == null)
                { flightmodelControlsElvTrim = GetDataRefElement("sim/flightmodel/controls/elv_trim", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "Elevation Trim, -1 = max nose down, 1 = max nose up"); }
                return flightmodelControlsElvTrim;
            }
        }

        private static DataRefElement flightmodelControlsFlaprat = null;
        /// <summary>
        /// Actual flap 1 deployment ratio
        /// </summary>
        public static DataRefElement FlightmodelControlsFlaprat {
            get {
                if (flightmodelControlsFlaprat == null)
                { flightmodelControlsFlaprat = GetDataRefElement("sim/flightmodel/controls/flaprat", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Actual flap 1 deployment ratio"); }
                return flightmodelControlsFlaprat;
            }
        }

        private static DataRefElement flightmodelControlsFlap2rat = null;
        /// <summary>
        /// Actual flap 2 deployment ratio
        /// </summary>
        public static DataRefElement FlightmodelControlsFlap2rat {
            get {
                if (flightmodelControlsFlap2rat == null)
                { flightmodelControlsFlap2rat = GetDataRefElement("sim/flightmodel/controls/flap2rat", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Actual flap 2 deployment ratio"); }
                return flightmodelControlsFlap2rat;
            }
        }

        private static DataRefElement flightmodelControlsLBrakeAdd = null;
        /// <summary>
        /// Left Brake - additional braking force (
        /// </summary>
        public static DataRefElement FlightmodelControlsLBrakeAdd {
            get {
                if (flightmodelControlsLBrakeAdd == null)
                { flightmodelControlsLBrakeAdd = GetDataRefElement("sim/flightmodel/controls/l_brake_add", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Left Brake - additional braking force ("); }
                return flightmodelControlsLBrakeAdd;
            }
        }

        private static DataRefElement flightmodelControlsRBrakeAdd = null;
        /// <summary>
        /// Right Brake
        /// </summary>
        public static DataRefElement FlightmodelControlsRBrakeAdd {
            get {
                if (flightmodelControlsRBrakeAdd == null)
                { flightmodelControlsRBrakeAdd = GetDataRefElement("sim/flightmodel/controls/r_brake_add", TypeStart_float, UtilConstants.Flag_Yes, "[0..1]", "Right Brake"); }
                return flightmodelControlsRBrakeAdd;
            }
        }

        private static DataRefElement flightmodelControlsLail1def = null;
        /// <summary>
        /// Deflection Left Aileron 1
        /// </summary>
        public static DataRefElement FlightmodelControlsLail1def {
            get {
                if (flightmodelControlsLail1def == null)
                { flightmodelControlsLail1def = GetDataRefElement("sim/flightmodel/controls/lail1def", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Left Aileron 1"); }
                return flightmodelControlsLail1def;
            }
        }

        private static DataRefElement flightmodelControlsLail2def = null;
        /// <summary>
        /// Deflection Left Aileron 2
        /// </summary>
        public static DataRefElement FlightmodelControlsLail2def {
            get {
                if (flightmodelControlsLail2def == null)
                { flightmodelControlsLail2def = GetDataRefElement("sim/flightmodel/controls/lail2def", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Left Aileron 2"); }
                return flightmodelControlsLail2def;
            }
        }

        private static DataRefElement flightmodelControlsRail1def = null;
        /// <summary>
        /// Deflection Right Aileron 1
        /// </summary>
        public static DataRefElement FlightmodelControlsRail1def {
            get {
                if (flightmodelControlsRail1def == null)
                { flightmodelControlsRail1def = GetDataRefElement("sim/flightmodel/controls/rail1def", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Right Aileron 1"); }
                return flightmodelControlsRail1def;
            }
        }

        private static DataRefElement flightmodelControlsRail2def = null;
        /// <summary>
        /// Deflection Right Aileron 2
        /// </summary>
        public static DataRefElement FlightmodelControlsRail2def {
            get {
                if (flightmodelControlsRail2def == null)
                { flightmodelControlsRail2def = GetDataRefElement("sim/flightmodel/controls/rail2def", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Right Aileron 2"); }
                return flightmodelControlsRail2def;
            }
        }

        private static DataRefElement flightmodelControlsLdruddef = null;
        /// <summary>
        /// Deflection Left Rudder
        /// </summary>
        public static DataRefElement FlightmodelControlsLdruddef {
            get {
                if (flightmodelControlsLdruddef == null)
                { flightmodelControlsLdruddef = GetDataRefElement("sim/flightmodel/controls/ldruddef", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Left Rudder"); }
                return flightmodelControlsLdruddef;
            }
        }

        private static DataRefElement flightmodelControlsRdruddef = null;
        /// <summary>
        /// Deflection Right Rudder
        /// </summary>
        public static DataRefElement FlightmodelControlsRdruddef {
            get {
                if (flightmodelControlsRdruddef == null)
                { flightmodelControlsRdruddef = GetDataRefElement("sim/flightmodel/controls/rdruddef", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Right Rudder"); }
                return flightmodelControlsRdruddef;
            }
        }

        private static DataRefElement flightmodelControlsLsplrdef = null;
        /// <summary>
        /// Deflection Left Spoiler
        /// </summary>
        public static DataRefElement FlightmodelControlsLsplrdef {
            get {
                if (flightmodelControlsLsplrdef == null)
                { flightmodelControlsLsplrdef = GetDataRefElement("sim/flightmodel/controls/lsplrdef", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Left Spoiler"); }
                return flightmodelControlsLsplrdef;
            }
        }

        private static DataRefElement flightmodelControlsRsplrdef = null;
        /// <summary>
        /// Deflection Right Spoiler
        /// </summary>
        public static DataRefElement FlightmodelControlsRsplrdef {
            get {
                if (flightmodelControlsRsplrdef == null)
                { flightmodelControlsRsplrdef = GetDataRefElement("sim/flightmodel/controls/rsplrdef", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "Deflection Right Spoiler"); }
                return flightmodelControlsRsplrdef;
            }
        }

        private static DataRefElement flightmodelControlsAil1Def = null;
        /// <summary>
        /// [WING] Deflection Aileron 1
        /// </summary>
        public static DataRefElement FlightmodelControlsAil1Def {
            get {
                if (flightmodelControlsAil1Def == null)
                { flightmodelControlsAil1Def = GetDataRefElement("sim/flightmodel/controls/ail1_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Aileron 1"); }
                return flightmodelControlsAil1Def;
            }
        }

        private static DataRefElement flightmodelControlsAil2Def = null;
        /// <summary>
        /// [WING] Deflection Aileron 2
        /// </summary>
        public static DataRefElement FlightmodelControlsAil2Def {
            get {
                if (flightmodelControlsAil2Def == null)
                { flightmodelControlsAil2Def = GetDataRefElement("sim/flightmodel/controls/ail2_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Aileron 2"); }
                return flightmodelControlsAil2Def;
            }
        }

        private static DataRefElement flightmodelControlsSplrDef = null;
        /// <summary>
        /// [WING] Deflection Spoilers
        /// </summary>
        public static DataRefElement FlightmodelControlsSplrDef {
            get {
                if (flightmodelControlsSplrDef == null)
                { flightmodelControlsSplrDef = GetDataRefElement("sim/flightmodel/controls/splr_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Spoilers"); }
                return flightmodelControlsSplrDef;
            }
        }

        private static DataRefElement flightmodelControlsSplr2Def = null;
        /// <summary>
        /// [WING] Deflection Spoilers 2
        /// </summary>
        public static DataRefElement FlightmodelControlsSplr2Def {
            get {
                if (flightmodelControlsSplr2Def == null)
                { flightmodelControlsSplr2Def = GetDataRefElement("sim/flightmodel/controls/splr2_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Spoilers 2"); }
                return flightmodelControlsSplr2Def;
            }
        }

        private static DataRefElement flightmodelControlsYawbDef = null;
        /// <summary>
        /// [WING]
        /// </summary>
        public static DataRefElement FlightmodelControlsYawbDef {
            get {
                if (flightmodelControlsYawbDef == null)
                { flightmodelControlsYawbDef = GetDataRefElement("sim/flightmodel/controls/yawb_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING]"); }
                return flightmodelControlsYawbDef;
            }
        }

        private static DataRefElement flightmodelControlsRuddDef = null;
        /// <summary>
        /// [WING] Deflection Rudders
        /// </summary>
        public static DataRefElement FlightmodelControlsRuddDef {
            get {
                if (flightmodelControlsRuddDef == null)
                { flightmodelControlsRuddDef = GetDataRefElement("sim/flightmodel/controls/rudd_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Rudders"); }
                return flightmodelControlsRuddDef;
            }
        }

        private static DataRefElement flightmodelControlsRudd2Def = null;
        /// <summary>
        /// [WING] Deflection Rudders
        /// </summary>
        public static DataRefElement FlightmodelControlsRudd2Def {
            get {
                if (flightmodelControlsRudd2Def == null)
                { flightmodelControlsRudd2Def = GetDataRefElement("sim/flightmodel/controls/rudd2_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Rudders"); }
                return flightmodelControlsRudd2Def;
            }
        }

        private static DataRefElement flightmodelControlsElv1Def = null;
        /// <summary>
        /// [WING] Deflection Elevators
        /// </summary>
        public static DataRefElement FlightmodelControlsElv1Def {
            get {
                if (flightmodelControlsElv1Def == null)
                { flightmodelControlsElv1Def = GetDataRefElement("sim/flightmodel/controls/elv1_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Elevators"); }
                return flightmodelControlsElv1Def;
            }
        }

        private static DataRefElement flightmodelControlsElv2Def = null;
        /// <summary>
        /// [WING] Deflection Elevators
        /// </summary>
        public static DataRefElement FlightmodelControlsElv2Def {
            get {
                if (flightmodelControlsElv2Def == null)
                { flightmodelControlsElv2Def = GetDataRefElement("sim/flightmodel/controls/elv2_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Elevators"); }
                return flightmodelControlsElv2Def;
            }
        }

        private static DataRefElement flightmodelControlsFla1Def = null;
        /// <summary>
        /// [WING] Deflection Flaps
        /// </summary>
        public static DataRefElement FlightmodelControlsFla1Def {
            get {
                if (flightmodelControlsFla1Def == null)
                { flightmodelControlsFla1Def = GetDataRefElement("sim/flightmodel/controls/fla1_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Flaps"); }
                return flightmodelControlsFla1Def;
            }
        }

        private static DataRefElement flightmodelControlsFla2Def = null;
        /// <summary>
        /// [WING] Deflection Flaps
        /// </summary>
        public static DataRefElement FlightmodelControlsFla2Def {
            get {
                if (flightmodelControlsFla2Def == null)
                { flightmodelControlsFla2Def = GetDataRefElement("sim/flightmodel/controls/fla2_def", TypeStart_float56, UtilConstants.Flag_Yes, "degrees", "[WING] Deflection Flaps"); }
                return flightmodelControlsFla2Def;
            }
        }

        public static DataRefElement FlightmodelControlsSbrkrqst
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/sbrkrqst",
                    Units = "[-0.5..1]",
                    Description = "Speed Brake, -0.5 = armed, 0 = off, 1 = max deployment",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVectrqst
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vectrqst",
                    Units = "[0..1]",
                    Description = "Requested thrust vector",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsSwdi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/swdi",
                    Units = "[0..1]",
                    Description = "Actual wing sweep ratio",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsSwdirqst
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/swdirqst",
                    Units = "[0..1]",
                    Description = "Requested sweep ratio",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsSlatrat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/slatrat",
                    Units = "[0..1]",
                    Description = "Actual slat deployment ratio",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsParkbrake
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/parkbrake",
                    Units = "[0..1]",
                    Description = "Parking Brake, 1 = max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsRotTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/rot_trim",
                    Units = "[-1..1]",
                    Description = "Rotor Trim",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsRudTrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/rud_trim",
                    Units = "[-1..1]",
                    Description = "Rudder Trim, -1 = max left, 1 = max right",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsIncidRqst
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/incid_rqst",
                    Units = "[0..1]",
                    Description = "Requested incidence",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsDihedRqst
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/dihed_rqst",
                    Units = "[0..1]",
                    Description = "Requested dihedral",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVectRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vect_rat",
                    Units = "[0..1]",
                    Description = "Actual thrust vector",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsIncidRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/incid_rat",
                    Units = "[0..1]",
                    Description = "Actual incidence",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsDihedRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/dihed_rat",
                    Units = "[0..1]",
                    Description = "Actual dihedral",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsTailhookRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/tailhook_ratio",
                    Units = "ratio",
                    Description = "tailhook deployment ratio, 0 is up 1 is down",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsCanopyRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/canopy_ratio",
                    Units = "ratio",
                    Description = "canopy deployment ratio, 0 is down 1 is up",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rAil1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_ail1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rAil2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_ail2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rSpo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_spo1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rSpo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_spo2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rFla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_fla1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rFla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_fla2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rYawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab1Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab1_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab1Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab1_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab2Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab2_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab2Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab2_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab1Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab1_rud1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab1Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab1_rud2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab2Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab2_rud1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab2Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab2_rud2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 1 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 2 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 3 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 4 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 5 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 6 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 7 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 8 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 9 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 10 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 11 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 12 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 13 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 14 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 15 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 16 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 17 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 18 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 19 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Ail1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_ail1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Aileron 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Ail2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_ail2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Aileron 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Spo1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_spo1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Spoiler 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Spo2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_spo2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Spoiler 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Fla1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_fla1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Flap 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Fla2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_fla2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Flap 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Yawbdef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_yawbdef",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Yaw Brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Elv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_elv1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Elv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_elv2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Rud1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_rud1def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Rudder 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Rud2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_rud2def",
                    Units = "degrees",
                    Description = "Deflection Misc Wing 20 Rudder 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_retract",
                    Units = "boolean",
                    Description = "Wing 1 Left Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_retract",
                    Units = "boolean",
                    Description = "Wing 1 Right Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_retract",
                    Units = "boolean",
                    Description = "Wing 2 Left Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_retract",
                    Units = "boolean",
                    Description = "Wing 2 Right Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_retract",
                    Units = "boolean",
                    Description = "Wing 3 Left Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_retract",
                    Units = "boolean",
                    Description = "Wing 3 Right Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_retract",
                    Units = "boolean",
                    Description = "Wing 4 Left Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rRetract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_retract",
                    Units = "boolean",
                    Description = "Wing 4 Right Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab1Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab1_retract",
                    Units = "boolean",
                    Description = "Horiz Stab Left Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab2Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab2_retract",
                    Units = "boolean",
                    Description = "Horiz Stab Right Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab1Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab1_retract",
                    Units = "boolean",
                    Description = "Vert Stab 1 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab2Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab2_retract",
                    Units = "boolean",
                    Description = "Vert Stab 2 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_retract",
                    Units = "boolean",
                    Description = "Misc Wing 1 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_retract",
                    Units = "boolean",
                    Description = "Misc Wing 2 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_retract",
                    Units = "boolean",
                    Description = "Misc Wing 3 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_retract",
                    Units = "boolean",
                    Description = "Misc Wing 4 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_retract",
                    Units = "boolean",
                    Description = "Misc Wing 5 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_retract",
                    Units = "boolean",
                    Description = "Misc Wing 6 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_retract",
                    Units = "boolean",
                    Description = "Misc Wing 7 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_retract",
                    Units = "boolean",
                    Description = "Misc Wing 8 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_retract",
                    Units = "boolean",
                    Description = "Misc Wing 9 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_retract",
                    Units = "boolean",
                    Description = "Misc Wing 10 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_retract",
                    Units = "boolean",
                    Description = "Misc Wing 11 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_retract",
                    Units = "boolean",
                    Description = "Misc Wing 12 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_retract",
                    Units = "boolean",
                    Description = "Misc Wing 13 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_retract",
                    Units = "boolean",
                    Description = "Misc Wing 14 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_retract",
                    Units = "boolean",
                    Description = "Misc Wing 15 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_retract",
                    Units = "boolean",
                    Description = "Misc Wing 16 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_retract",
                    Units = "boolean",
                    Description = "Misc Wing 17 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_retract",
                    Units = "boolean",
                    Description = "Misc Wing 18 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_retract",
                    Units = "boolean",
                    Description = "Misc Wing 19 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20Retract
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_retract",
                    Units = "boolean",
                    Description = "Misc Wing 20 Retract",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_retract_now",
                    Units = "ratio",
                    Description = "Wing 1 Left Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_retract_now",
                    Units = "ratio",
                    Description = "Wing 1 Right Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_retract_now",
                    Units = "ratio",
                    Description = "Wing 2 Left Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_retract_now",
                    Units = "ratio",
                    Description = "Wing 2 Right Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_retract_now",
                    Units = "ratio",
                    Description = "Wing 3 Left Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_retract_now",
                    Units = "ratio",
                    Description = "Wing 3 Right Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_retract_now",
                    Units = "ratio",
                    Description = "Wing 4 Left Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rRetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_retract_now",
                    Units = "ratio",
                    Description = "Wing 4 Right Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab1RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab1_retract_now",
                    Units = "ratio",
                    Description = "Horiz Stab Left Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab2RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab2_retract_now",
                    Units = "ratio",
                    Description = "Horiz Stab Right Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab1RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab1_retract_now",
                    Units = "ratio",
                    Description = "Vert Stab 1 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab2RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab2_retract_now",
                    Units = "ratio",
                    Description = "Vert Stab 2 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 1 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 2 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 3 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 4 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 5 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 6 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 7 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 8 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 9 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 10 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 11 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 12 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 13 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 14 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 15 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 16 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 17 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 18 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 19 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20RetractNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_retract_now",
                    Units = "ratio",
                    Description = "Misc Wing 20 Retract Now",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_retract_max",
                    Units = "ratio",
                    Description = "Wing 1 Left Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_retract_max",
                    Units = "ratio",
                    Description = "Wing 1 Right Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_retract_max",
                    Units = "ratio",
                    Description = "Wing 2 Left Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_retract_max",
                    Units = "ratio",
                    Description = "Wing 2 Right Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_retract_max",
                    Units = "ratio",
                    Description = "Wing 3 Left Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_retract_max",
                    Units = "ratio",
                    Description = "Wing 3 Right Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_retract_max",
                    Units = "ratio",
                    Description = "Wing 4 Left Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rRetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_retract_max",
                    Units = "ratio",
                    Description = "Wing 4 Right Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab1RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab1_retract_max",
                    Units = "ratio",
                    Description = "Horiz Stab Left Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsHstab2RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/hstab2_retract_max",
                    Units = "ratio",
                    Description = "Horiz Stab Right Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab1RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab1_retract_max",
                    Units = "ratio",
                    Description = "Vert Stab 1 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsVstab2RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/vstab2_retract_max",
                    Units = "ratio",
                    Description = "Vert Stab 2 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing01RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing01_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 1 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing02RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing02_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 2 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing03RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing03_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 3 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing04RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing04_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 4 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing05RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing05_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 5 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing06RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing06_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 6 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing07RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing07_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 7 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing08RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing08_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 8 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing09RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing09_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 9 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing10RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing10_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 10 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing11RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing11_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 11 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing12RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing12_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 12 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing13RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing13_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 13 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing14RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing14_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 14 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing15RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing15_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 15 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing16RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing16_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 16 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing17RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing17_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 17 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing18RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing18_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 18 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing19RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing19_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 19 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsMwing20RetractMax
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/mwing20_retract_max",
                    Units = "ratio",
                    Description = "Misc Wing 20 Retract Max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1lElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1l_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Left Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing1rElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing1r_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 1 Right Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2lElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2l_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Left Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing2rElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing2r_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 2 Right Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3lElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3l_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Left Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing3rElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing3r_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 3 Right Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4lElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4l_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Left Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rElv1def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_elv1def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Elevator 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelControlsWing4rElv2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/controls/wing4r_elv2def",
                    Units = "degrees",
                    Description = "Deflection Wing 4 Right Elevator 2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicAilnBladAlph
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_ailn_blad_alph",
                    Units = "???",
                    Description = "Blade alpha",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicAilnDiscTilt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_ailn_disc_tilt",
                    Units = "???",
                    Description = "Disc tilt",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicElevBladAlph
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_elev_blad_alph",
                    Units = "???",
                    Description = "Blade alpha",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicElevDiscTilt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_elev_disc_tilt",
                    Units = "???",
                    Description = "Disc tilt",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicElevCommand
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_elev_command",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicCyclicAilnCommand
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/cyclic_ailn_command",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicSidecant
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/sidecant",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelCyclicVertcant
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/cyclic/vertcant",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNN2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_N2_",
                    Units = "percent",
                    Description = "N2 speed as percent of max (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNEGT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_EGT",
                    Units = "ratio",
                    Description = "Exhaust Gas Temperature (ratio from min to max)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNITT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_ITT",
                    Units = "ratio",
                    Description = "Interturbine Temperature per engine (ratio from min to max, min = 0, max = 700)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNCHT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_CHT",
                    Units = "ratio",
                    Description = "Cylinder Head Temperature (ratio from min to max)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNEGTC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_EGT_c",
                    Units = "degc_or_f",
                    Description = "EGT (per engine) in degrees  - units depend on plane, label wrong",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNITTC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_ITT_c",
                    Units = "degc_or_f",
                    Description = "ITT (per engine) in degrees - units depend on plane, label wrong",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNCHTC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_CHT_c",
                    Units = "degc_or_f",
                    Description = "CHT (per engine in degrees  - units depend on plane, label wrong",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNBatAmp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_bat_amp",
                    Units = "???",
                    Description = "Battery amps (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNBatVolt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_bat_volt",
                    Units = "???",
                    Description = "Batery volts (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNCowl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_cowl",
                    Units = "ratio",
                    Description = "Cowl flaps control (per engine) 0 = closed, 1 = open",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNEPR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_EPR",
                    Units = "EPR",
                    Description = "Engine Pressure Ratio (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNFF
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_FF_",
                    Units = "kg/s",
                    Description = "Fuel flow (per engine) in kg/second",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNGenAmp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_gen_amp",
                    Units = "???",
                    Description = "Generator amps (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_heat",
                    Units = "ratio",
                    Description = "Carb Heat Control (per engine), 0 = off, 1 = on",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNMixt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_mixt",
                    Units = "ratio",
                    Description = "Mixture Control (per engine), 0 = cutoff, 1 = full rich",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNMPR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_MPR",
                    Units = "???",
                    Description = "MPR (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNN1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_N1_",
                    Units = "percent",
                    Description = "N1 speed as percent of max (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilPressPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_press_psi",
                    Units = "psi",
                    Description = "Oil pressure (per engine) in PSI",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilTempC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_temp_c",
                    Units = "degc_or_f",
                    Description = "Oil temp (per engine) in degs - units depend on plane, dref name wrong",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilTemp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_temp",
                    Units = "ratio",
                    Description = "Oil pressure (per engine) as ratio of max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilPress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_press",
                    Units = "ratio",
                    Description = "Oil temp (per engine) a ratio of max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNPower
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_power",
                    Units = "???",
                    Description = "Power (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_prop",
                    Units = "rad/sec",
                    Description = "Commanded Prop Speed (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNSigma
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_sigma",
                    Units = "???",
                    Description = "Sigma (per engine) - YOU SHOULD NEVER WRITE TO THIS DATAREF!  Read only in v11.00",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNThro
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_thro",
                    Units = "ratio",
                    Description = "Throttle (per engine) as set by user, 0 = idle, 1 = max",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNThroUse
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_thro_use",
                    Units = "ratio",
                    Description = "Throttle (per engine) when overridden by you, plus with thrust vectors - use override_throttles to change.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNThroOverride
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_thro_override",
                    Units = "ratio",
                    Description = "An override from 0.0 to max fwd thrust for overriding all throttles.  Set to -2.0 to disengage.  DEPRECATED",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNTRQ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_TRQ",
                    Units = "NewtonMeters",
                    Description = "Torque (per engine)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNRunning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_running",
                    Units = "boolean",
                    Description = "Engine on and using fuel (only reliable in 740 and later)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNBurning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_burning",
                    Units = "boolean",
                    Description = "Afterburner on (only reliable in 740 and later)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNPropmode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_propmode",
                    Units = "enum",
                    Description = "Prop mode: feather=0,normal=1,beta=2,reverse=3",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNBurnrat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_burnrat",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilQuan
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_quan",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNOilLubeRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_oil_lube_rat",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNCrbice
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_crbice",
                    Units = "ratio",
                    Description = "Amount of carb ice buildup (0-1)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineENGNTacrad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/ENGN_tacrad",
                    Units = "rad/sec",
                    Description = "Engine speed in radians/second",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_pitch_deg",
                    Units = "degrees",
                    Description = "Prop Pitch as commanded by the user.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTPropEff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_prop_eff",
                    Units = "???",
                    Description = "Efficiency",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTTacrad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_tacrad",
                    Units = "rad/sec",
                    Description = "Prop speed in radians/second",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTThrust
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_thrust",
                    Units = "???",
                    Description = "Thrust",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTDragTRQ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_drag_TRQ",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTDrivTRQ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_driv_TRQ",
                    Units = "newton-meters",
                    Description = "Torque for this prop after transmissions",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTMaxTRQ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_max_TRQ",
                    Units = "newton-meters",
                    Description = "Maximum torque this prop will get after transmissions",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTConeRad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_cone_rad",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTSideWash
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_side_wash",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTXYZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_XYZ",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTPitchDegUse
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_pitch_deg_use",
                    Units = "degrees",
                    Description = "Pitch as we use, after all effects.   Use override_prop_pitch to change.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEnginePOINTPropAngDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/POINT_prop_ang_deg",
                    Units = "degrees",
                    Description = "This is the angle of the prop or engine-fan as it rotates. You will see this value circulate 0 to 360 degrees over and over as the engine runs and the prop or fan turns.  Override with /prop_disc/override per engine!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineBurnerEnabled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/burner_enabled",
                    Units = "boolean",
                    Description = "Burner is on or off",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineBurnerLevel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/burner_level",
                    Units = "boolean",
                    Description = "Burner is high or low",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineAprMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/apr_mode",
                    Units = "enum",
                    Description = "0=Stdby (will arm at sufficient throttle), 1=Armed, 2=Active",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineDescentSpeedRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/descent_speed_ratio",
                    Units = "ratio",
                    Description = "descent-speed ratio of the aircraft in units of propwash-speed",
                    
                };
            }
        }
        public static DataRefElement FlightmodelEngineVortexRingState
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/engine/vortex_ring_state",
                    Units = "ratio",
                    Description = "wash-multiplier - vortex ring state - which are the airflows through the rotor disc normal flow is 0.5 (50%) VRS flow is 1.0 (100%)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresFrmIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/frm_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on wings/airframe - left wing",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresFrmIce2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/frm_ice2",
                    Units = "ratio",
                    Description = "Ratio of icing on wings/airframe - right wing",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresPitotIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/pitot_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on pitot tube",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresPitotIce2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/pitot_ice2",
                    Units = "ratio",
                    Description = "Ratio of icing on pitot tube2",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresPropIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/prop_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on the prop - first prop",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresStatIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/stat_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on the static port - pilot side",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresStatIce2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/stat_ice2",
                    Units = "ratio",
                    Description = "Ratio of icing on the static port - copilot side",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresInletIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/inlet_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on the air inlets - first engine",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresPropIcePerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/prop_ice_per_engine",
                    Units = "ratio",
                    Description = "Ratio of icing on the prop - array access to all props.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresInletIcePerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/inlet_ice_per_engine",
                    Units = "ratio",
                    Description = "Ratio of icing on the air inlets - array access to all engines.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresWindowIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/window_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on the windshield",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresAoaIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/aoa_ice",
                    Units = "ratio",
                    Description = "Ratio of icing on alpha vane - pilot AoA",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresAoaIce2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/aoa_ice2",
                    Units = "ratio",
                    Description = "Ratio of icing on alpha vane - copilot AoA",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresStallwarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/stallwarning",
                    Units = "???",
                    Description = "Stall Warning",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOverG
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/over_g",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOverVne
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/over_vne",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOverVfe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/over_vfe",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOverVle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/over_vle",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOngroundAny
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/onground_any",
                    Units = "???",
                    Description = "User Aircraft is on the ground when this is set to 1",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresOngroundAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/onground_all",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresSmoking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/smoking",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelFailuresLoRotorWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/failures/lo_rotor_warning",
                    Units = "???",
                    Description = "Lo Rotor Warning",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFsideAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fside_aero",
                    Units = "Newtons",
                    Description = "Aerodynamic forces - sideways - ACF X. Override with override_wing_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFnrmlAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fnrml_aero",
                    Units = "Newtons",
                    Description = "Aerodynamic forces - upward - ACF Y.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFaxilAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/faxil_aero",
                    Units = "Newtons",
                    Description = "Aerodynamic forces - backward - ACF Z",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFsideProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fside_prop",
                    Units = "Newtons",
                    Description = "force sideways by all engines on the ACF.  Override with override_engines or override_engine_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFnrmlProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fnrml_prop",
                    Units = "Newtons",
                    Description = "force upward by all engines on the ACF.  Override with override_engines  Writable in v10 only or v11 with override",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFaxilProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/faxil_prop",
                    Units = "Newtons",
                    Description = "force backward by all engines on the ACF (usually this is a negative number).  Override with override_engines",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFsideGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fside_gear",
                    Units = "Newtons",
                    Description = "Gear/ground forces - sideways - ACF X.  Override with override_gear_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFnrmlGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fnrml_gear",
                    Units = "Newtons",
                    Description = "Gear/ground forces - upward - ACF Y",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFaxilGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/faxil_gear",
                    Units = "Newtons",
                    Description = "Gear/ground forces - backward - ACF Z",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFsideTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fside_total",
                    Units = "Newtons",
                    Description = "total/ground forces - ACF X axis.  Override with override_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFnrmlTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fnrml_total",
                    Units = "Newtons",
                    Description = "Total/ground forces - ACF Y axis.  Override with override_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFaxilTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/faxil_total",
                    Units = "Newtons",
                    Description = "total/ground forces - ACF Z axis.  Override with override_forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_aero",
                    Units = "NM",
                    Description = "The roll moment due to aerodynamic forces - positive = right roll.  Override with Override with override_wing_Forces",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_aero",
                    Units = "NM",
                    Description = "The pitch moment due to aerodynamic forces - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNAero
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_aero",
                    Units = "NM",
                    Description = "The yaw moment due to aerodynamic forces - positive = yaw right/clockwise.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_prop",
                    Units = "NM",
                    Description = "The roll moment due to prop forces. Override with override_engines or override_engine_forces - positive = right roll.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_prop",
                    Units = "NM",
                    Description = "The pitch moment due to prop forces. Override with override_engines - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_prop",
                    Units = "NM",
                    Description = "The yaw moment due to prop forces. Override with override_engines - positive = yaw right/clockwise.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_gear",
                    Units = "NM",
                    Description = "The roll moment due to gear forces - positive = right roll.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_gear",
                    Units = "NM",
                    Description = "The pitch moment due to gear forces - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNGear
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_gear",
                    Units = "NM",
                    Description = "The yaw moment due to gear forces positive = yaw right/clockwise.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLMass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_mass",
                    Units = "NM",
                    Description = "The roll moment due to asymmetric loading - positive = right roll.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMMass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_mass",
                    Units = "NM",
                    Description = "The pitch moment due to asymmetric loading - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNMass
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_mass",
                    Units = "NM",
                    Description = "The yaw moment due to asymmetric loading - positive = yaw right/clockwise.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_total",
                    Units = "NM",
                    Description = "The roll moment total.  Override with override_force - positive = right roll.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_total",
                    Units = "NM",
                    Description = "The pitch moment total.  Override with override_force - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_total",
                    Units = "NM",
                    Description = "The yaw moment total.  Override with override_forces - positive = yaw right/clockwise.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFsidePlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fside_plug_acf",
                    Units = "Newtons",
                    Description = "Extra plugin-provided sideways force (ACF X axis, positive pushes airplane to the right). ADD to this dataref to apply extra force.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFnrmlPlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/fnrml_plug_acf",
                    Units = "Newtons",
                    Description = "Extra plugin-provided upward force (ACF Y axis, positive pushes airplane up). ADD to this dataref to apply extra force.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesFaxilPlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/faxil_plug_acf",
                    Units = "Newtons",
                    Description = "Extra plugin-provided forward force.  (ACF Z axis, positive pushes airplane backward). ADD to this dataref to apply extra force.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLPlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/L_plug_acf",
                    Units = "NM",
                    Description = "Extra plugin-provided roll moment - ADD to this dataref to apply extra force - positive = right roll.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesMPlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/M_plug_acf",
                    Units = "NM",
                    Description = "Extra plugin-provided pitch moment - ADD to this dataref to apply extra force - positive = pitch up.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesNPlugAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/N_plug_acf",
                    Units = "NM",
                    Description = "Extra plugin-provided yaw moment - ADD to this dataref to apply extra force - positive = yaw right/clockwise.",
                    
                };
            }
        }

        private static DataRefElement flightmodelForcesGNrml = null;
        /// <summary>
        /// Total g-forces on the plane as a multiple, downward
        /// </summary>
        public static DataRefElement FlightmodelForcesGNrml {
            get {
                if (flightmodelForcesGNrml == null)
                { flightmodelForcesGNrml = GetDataRefElement("sim/flightmodel/forces/g_nrml", TypeStart_float, UtilConstants.Flag_No, "Gs", "Total g-forces on the plane as a multiple, downward"); }
                return flightmodelForcesGNrml;
            }
        }
        
        public static DataRefElement FlightmodelForcesGAxil
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/g_axil",
                    Units = "Gs",
                    Description = "Total g-forces on the plane as a multiple, along the plane",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesGSide
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/g_side",
                    Units = "Gs",
                    Description = "Total g-forces on the plane as a multiple, sideways",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesVxAirOnAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/vx_air_on_acf",
                    Units = "mtr/sec",
                    Description = "Velocity of air relative to airplane",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesVyAirOnAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/vy_air_on_acf",
                    Units = "mtr/sec",
                    Description = "Velocity of air relative to airplane",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesVzAirOnAcf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/vz_air_on_acf",
                    Units = "mtr/sec",
                    Description = "Velocity of air relative to airplane",
                    
                };
            }
        }

        private static DataRefElement flightmodelForcesVxAcfAxis = null;
        /// <summary>
        /// Velocity of aircraft in its own coordinate system : X
        /// </summary>
        public static DataRefElement FlightmodelForcesVxAcfAxis {
            get {
                if (flightmodelForcesVxAcfAxis == null)
                { flightmodelForcesVxAcfAxis = GetDataRefElement("sim/flightmodel/forces/vx_acf_axis", TypeStart_float, UtilConstants.Flag_No, "mtr/sec", "Velocity of aircraft in its own coordinate system"); }
                return flightmodelForcesVxAcfAxis;
            }
        }

        private static DataRefElement flightmodelForcesVyAcfAxis = null;
        /// <summary>
        /// Velocity of aircraft in its own coordinate system : Y
        /// </summary>
        public static DataRefElement FlightmodelForcesVyAcfAxis {
            get {
                if (flightmodelForcesVyAcfAxis == null)
                { flightmodelForcesVyAcfAxis = GetDataRefElement("sim/flightmodel/forces/vy_acf_axis", TypeStart_float, UtilConstants.Flag_No, "mtr/sec", "Velocity of aircraft in its own coordinate system"); }
                return flightmodelForcesVyAcfAxis;
            }
        }

        private static DataRefElement flightmodelForcesVzAcfAxis = null;
        /// <summary>
        /// Velocity of aircraft in its own coordinate system : Z
        /// </summary>
        public static DataRefElement FlightmodelForcesVzAcfAxis {
            get {
                if (flightmodelForcesVzAcfAxis == null)
                { flightmodelForcesVzAcfAxis = GetDataRefElement("sim/flightmodel/forces/vz_acf_axis", TypeStart_float, UtilConstants.Flag_No, "mtr/sec", "Velocity of aircraft in its own coordinate system"); }
                return flightmodelForcesVzAcfAxis;
            }
        }

        public static DataRefElement FlightmodelForcesQRotorRad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/Q_rotor_rad",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesRRotorRad
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/R_rotor_rad",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesLiftPathAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/lift_path_axis",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesDragPathAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/drag_path_axis",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelForcesSidePathAxis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/forces/side_path_axis",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelJetwashDvinc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/jetwash/DVinc",
                    Units = "???",
                    Description = "DVinc",
                    
                };
            }
        }
        public static DataRefElement FlightmodelJetwashRingdvinc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/jetwash/ringDVinc",
                    Units = "???",
                    Description = "ringDVinc",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscEttSize
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/ett_size",
                    Units = "???",
                    Description = "??? Slung/jettisonable load size (this should read JETT size but has a typo)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscJettSize
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/jett_size",
                    Units = "???",
                    Description = "??? Slung/jettisonable load size - this fixes the typo above.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscJettLen
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/jett_len",
                    Units = "???",
                    Description = "??? slung jettisonable load length (length of cable??!)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscGTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/g_total",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscNosewheelSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/nosewheel_speed",
                    Units = "???",
                    Description = "[GEAR]",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscWingTiltPtch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/wing_tilt_ptch",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscWingTiltRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/wing_tilt_roll",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscJatoLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/jato_left",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscDisplaceRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/displace_rat",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscHInd
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/h_ind",
                    Units = "feet",
                    Description = "Indicated barometric altitude",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscHInd2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/h_ind2",
                    Units = "feet",
                    Description = "Indicated barometric altitude",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscHIndCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/h_ind_copilot",
                    Units = "feet",
                    Description = "Indicated barometric altitude",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscHIndCopilot2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/h_ind_copilot2",
                    Units = "feet",
                    Description = "Indicated barometric altitude",
                    
                };
            }
        }

        private static DataRefElement flightmodelMiscMachno = null;
        public static DataRefElement FlightmodelMiscMachno {
            get {
                if (flightmodelMiscMachno == null)
                { flightmodelMiscMachno = GetDataRefElement("sim/flightmodel/misc/machno", TypeStart_float, UtilConstants.Flag_No, "???", "???"); }
                return flightmodelMiscMachno;
            }
        }
        
        public static DataRefElement FlightmodelMiscQstatic
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/Qstatic",
                    Units = "psf",
                    Description = "Ambient Q",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscTurnrateRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/turnrate_roll",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscTurnrateRoll2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/turnrate_roll2",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscTurnrateNoroll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/turnrate_noroll",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscTurnrateNoroll2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/turnrate_noroll2",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscSlip
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/slip",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscRocketMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/rocket_mode",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscRocketTimeout
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/rocket_timeout",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscPropOspeedTestTimeout
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/prop_ospeed_test_timeout",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscBlownFlapEngageRat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/blown_flap_engage_rat",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscLiftFanTotalPower
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/lift_fan_total_power",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscStabPtchTest
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/stab_ptch_test",
                    Units = "???",
                    Description = "Computed stability derivative - pitch",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscStabHdngTest
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/stab_hdng_test",
                    Units = "???",
                    Description = "Computed Stability drivative - heading",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscCgzRefToDefault
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/cgz_ref_to_default",
                    Units = "meters",
                    Description = "Center of Gravity",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscQCentroidMULT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/Q_centroid_MULT",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscCM
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/C_m",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscCN
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/C_n",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscClOverall
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/cl_overall",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }

        private static DataRefElement flightmodelMiscCdOverall = null;
        public static DataRefElement FlightmodelMiscCdOverall {
            get {
                if (flightmodelMiscCdOverall == null)
                { flightmodelMiscCdOverall = GetDataRefElement("sim/flightmodel/misc/cd_overall", TypeStart_float, UtilConstants.Flag_Yes, "???", "???"); }
                return flightmodelMiscCdOverall;
            }
        }
        
        public static DataRefElement FlightmodelMiscLOD
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/l_o_d",
                    Units = "???",
                    Description = "???",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscActFrcPtchLb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/act_frc_ptch_lb",
                    Units = "lbs",
                    Description = "Force feedback: total pounds on yoke by ACF due to pitch",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscActFrcRollLb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/act_frc_roll_lb",
                    Units = "lbs",
                    Description = "Force feedback: total pounds on yoke by ACF due to roll",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscActFrcHdngLb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/act_frc_hdng_lb",
                    Units = "lbs",
                    Description = "Force feedback: total pounds on pedals by ACF due to heading",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscActFrcLbrkLb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/act_frc_lbrk_lb",
                    Units = "lbs",
                    Description = "Force feedback: total pounds on pedals by ACF due to left brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscActFrcRbrkLb
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/act_frc_rbrk_lb",
                    Units = "lbs",
                    Description = "Force feedback: total pounds on pedals by ACF due to right brake",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscSlungLoadX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/slung_load_x",
                    Units = "meters",
                    Description = "Global location of slung load, meters, x coordinate",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscSlungLoadY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/slung_load_y",
                    Units = "meters",
                    Description = "Global location of slung load, meters, y coordinate",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMiscSlungLoadZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/misc/slung_load_z",
                    Units = "meters",
                    Description = "Global location of slung load, meters, z coordinate",
                    
                };
            }
        }
        public static DataRefElement FlightmodelGroundSurfaceTextureType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/ground/surface_texture_type",
                    Units = "enum",
                    Description = "tbd - writable only with override_groundplane",
                    
                };
            }
        }
        public static DataRefElement FlightmodelGroundPluginGroundCenter
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/ground/plugin_ground_center",
                    Units = "meters",
                    Description = "Location of a pt on the ground in local coords",
                    
                };
            }
        }
        public static DataRefElement FlightmodelGroundPluginGroundSlopeNormal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/ground/plugin_ground_slope_normal",
                    Units = "vector",
                    Description = "Normal vector of the terrain (must be normalized)",
                    
                };
            }
        }
        public static DataRefElement FlightmodelGroundPluginGroundTerrainVelocity
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/ground/plugin_ground_terrain_velocity",
                    Units = "m/s",
                    Description = "speed of deck moving under us (this is a velocity vector)",
                    
                };
            }
        }

        private static DataRefElement flightmodelMovingpartsGear1def = null;
        /// <summary>
        /// landing gear1 def
        /// </summary>
        public static DataRefElement FlightmodelMovingpartsGear1def {
            get {
                if (flightmodelMovingpartsGear1def == null)
                { flightmodelMovingpartsGear1def = GetDataRefElement("sim/flightmodel/movingparts/gear1def", TypeStart_float, UtilConstants.Flag_No, "???", "landing gear1 def"); }
                return flightmodelMovingpartsGear1def;
            }
        }
        
        public static DataRefElement FlightmodelMovingpartsGear2def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/movingparts/gear2def",
                    Units = "???",
                    Description = "landing gear2 def",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMovingpartsGear3def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/movingparts/gear3def",
                    Units = "???",
                    Description = "landing gear3 def",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMovingpartsGear4def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/movingparts/gear4def",
                    Units = "???",
                    Description = "landing gear4 def",
                    
                };
            }
        }
        public static DataRefElement FlightmodelMovingpartsGear5def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/movingparts/gear5def",
                    Units = "???",
                    Description = "landing gear5 def",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsVEl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/v_el",
                    Units = "???",
                    Description = "73x10x4 speed is DIFFERENT FOR EACH BLADE!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsAlphaEl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/alpha_el",
                    Units = "???",
                    Description = "73x10x4 alpha is DIFFERENT FOR EACH BLADE!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsDelDir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/del_dir",
                    Units = "???",
                    Description = "73x10x4 corkscrew path is DIFFERENT FOR EACH BLADE!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsClElRaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/cl_el_raw",
                    Units = "???",
                    Description = "56x10x4 [WING]",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsCLGrndeffect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/CL_grndeffect",
                    Units = "???",
                    Description = "73 [WING]",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsCDGrndeffect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/CD_grndeffect",
                    Units = "???",
                    Description = "73 [WING]",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsWashGrndeffect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/wash_grndeffect",
                    Units = "???",
                    Description = "56 [WING]",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsQCentroidLoc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/Q_centroid_loc",
                    Units = "???",
                    Description = "73x4 centroid is TOTALLY DIFFERENT FOR EACH BLADE on helos and gyros!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsQCentroidMULT
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/Q_centroid_MULT",
                    Units = "???",
                    Description = "73x4 centroid is TOTALLY DIFFERENT FOR EACH BLADE on helos and gyros!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireDragDis
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_drag_dis",
                    Units = "???",
                    Description = "73",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireSpeedTerm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_speed_term",
                    Units = "???",
                    Description = "73",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireSpeedNow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_speed_now",
                    Units = "???",
                    Description = "73",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTirePropRot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_prop_rot",
                    Units = "???",
                    Description = "73",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireXNoDeflection
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_x_no_deflection",
                    Units = "meters",
                    Description = "[GEAR] x location of the Nth gear's wheel relative to the CG, airplane coordinates.  This doesn't take into account strut compression.  Writable in v8/9, read-only in v10.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireYNoDeflection
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_y_no_deflection",
                    Units = "meters",
                    Description = "[GEAR] y location of the Nth gear's wheel relative to the CG, airplane coordinates.  This doesn't take into account strut compression.  Writable in v8/9, read-only in v10.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireZNoDeflection
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_z_no_deflection",
                    Units = "meters",
                    Description = "[GEAR] z location of the Nth gear's wheel relative to the CG, airplane coordinates.  This doesn't take into account strut compression.  Writable in v8/9, read-only in v10.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireVrtDefVeh
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_vrt_def_veh",
                    Units = "meters",
                    Description = "[GEAR] This is amount the Nth gear is deflected upwards due to vehicle weight on struts.",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireVrtFrcVeh
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_vrt_frc_veh",
                    Units = "???",
                    Description = "73",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireSteerCmd
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_steer_cmd",
                    Units = "degrees",
                    Description = "Steering command being sent to this gear, degrees positive right.  This takes into account steering algorithms for big planes like 747, but does not free castoring and springiness. Writable in 1030.  Override via override_wheel_steer",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsTireSteerAct
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/tire_steer_act",
                    Units = "degrees",
                    Description = "Steering command actually enacted by the gear, degrees positive right. Writable in 1030.  Override via override_wheel_steer",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsNrmlForce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/nrml_force",
                    Units = "???",
                    Description = "73 [WING] for data output, for the entire flying surface",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsAxilForce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/axil_force",
                    Units = "???",
                    Description = "73 [WING] for data output, for the entire flying surface",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsFlapDef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/flap_def",
                    Units = "degrees",
                    Description = "73 [WING] with flap setting, and then variation with pitch and roll input, on a 4 wing plane, with flaps on every wing, some going up and down with pitch input, we better find the flap def on each surface!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsFlap2Def
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/flap2_def",
                    Units = "degrees",
                    Description = "56 [WING] with flap setting, and then variation with pitch and roll input, on a 4 wing plane, with flaps on every wing, some going up and down with pitch input, we better find the flap def on each surface!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsElevContDef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/elev_cont_def",
                    Units = "degrees",
                    Description = "73 [WING] different for each part since def may be positive on a canard and negative on an aft wing... ON THE SAME PLANE AT THE SAME TIME!",
                    
                };
            }
        }
        public static DataRefElement FlightmodelPartsElevTrimDef
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/parts/elev_trim_def",
                    Units = "degrees",
                    Description = "73 [WING] different for each part since def may be positive on a canard and negative on an aft wing... ON THE SAME PLANE AT THE SAME TIME!",
                    
                };
            }
        }

        private static DataRefElement flightmodelPartsRuddContDef = null;
        /// <summary>
        /// 73 [WING] different for each part since def may be positive on top vstab and neg on the bottom one.... ON THE SAME PLANE AT THE SAME TIME!
        /// </summary>
        public static DataRefElement FlightmodelPartsRuddContDef {
            get {
                if (flightmodelPartsRuddContDef == null)
                { flightmodelPartsRuddContDef = GetDataRefElement("sim/flightmodel/parts/rudd_cont_def", TypeStart_float56, UtilConstants.Flag_No, "degrees", "73 [WING] different for each part since def may be positive on top vstab and neg on the bottom one.... ON THE SAME PLANE AT THE SAME TIME!"); }
                return flightmodelPartsRuddContDef;
            }
        }

        private static DataRefElement flightmodelPartsRudd2ContDef = null;
        /// <summary>
        /// 56 [WING] different for each part since def may be positive on top vstab and neg on the bottom one.... ON THE SAME PLANE AT THE SAME TIME!
        /// </summary>
        public static DataRefElement FlightmodelPartsRudd2ContDef {
            get {
                if (flightmodelPartsRudd2ContDef == null)
                { flightmodelPartsRudd2ContDef = GetDataRefElement("sim/flightmodel/parts/rudd2_cont_def", TypeStart_float56, UtilConstants.Flag_No, "degrees", "56 [WING] different for each part since def may be positive on top vstab and neg on the bottom one.... ON THE SAME PLANE AT THE SAME TIME!"); }
                return flightmodelPartsRudd2ContDef;
            }
        }

        private static DataRefElement flightmodelPartsElemInc = null;
        /// <summary>
        /// 73x10
        /// </summary>
        public static DataRefElement FlightmodelPartsElemInc {
            get {
                if (flightmodelPartsElemInc == null)
                { flightmodelPartsElemInc = GetDataRefElement("sim/flightmodel/parts/elem_inc", TypeStart_float73_10, UtilConstants.Flag_No, "degrees", "73x10"); }
                return flightmodelPartsElemInc;
            }
        }

        private static DataRefElement flightmodelPositionLocalX = null;
        /// <summary>
        /// The location of the plane in OpenGL coordinates : X
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalX {
            get {
                if (flightmodelPositionLocalX == null)
                { flightmodelPositionLocalX = GetDataRefElement("sim/flightmodel/position/local_x", TypeStart_double, UtilConstants.Flag_Yes, "meters", "The location of the plane in OpenGL coordinates"); }
                return flightmodelPositionLocalX;
            }
        }

        private static DataRefElement flightmodelPositionLocalY = null;
        /// <summary>
        /// The location of the plane in OpenGL coordinates : Y
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalY {
            get {
                if (flightmodelPositionLocalY == null)
                { flightmodelPositionLocalY = GetDataRefElement("sim/flightmodel/position/local_y", TypeStart_double, UtilConstants.Flag_Yes, "meters", "The location of the plane in OpenGL coordinates"); }
                return flightmodelPositionLocalY;
            }
        }

        private static DataRefElement flightmodelPositionLocalZ = null;
        /// <summary>
        /// The location of the plane in OpenGL coordinates : Z
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalZ {
            get {
                if (flightmodelPositionLocalZ == null)
                { flightmodelPositionLocalZ = GetDataRefElement("sim/flightmodel/position/local_z", TypeStart_double, UtilConstants.Flag_Yes, "meters", "The location of the plane in OpenGL coordinates"); }
                return flightmodelPositionLocalZ;
            }
        }

        private static DataRefElement flightmodelPositionLatRef = null;
        /// <summary>
        /// The latitude of the point 0,0,0 in OpenGL coordinates (Writing NOT recommended!!)
        /// </summary>
        public static DataRefElement FlightmodelPositionLatRef {
            get {
                if (flightmodelPositionLatRef == null)
                { flightmodelPositionLatRef = GetDataRefElement("sim/flightmodel/position/lat_ref", TypeStart_float, UtilConstants.Flag_No, "degrees", "The latitude of the point 0,0,0 in OpenGL coordinates (Writing NOT recommended!!)"); }
                return flightmodelPositionLatRef;
            }
        }

        private static DataRefElement flightmodelPositionLonRef = null;
        /// <summary>
        /// The longitude of the point 0,0,0 in OpenGL coordinates.
        /// </summary>
        public static DataRefElement FlightmodelPositionLonRef {
            get {
                if (flightmodelPositionLonRef == null)
                { flightmodelPositionLonRef = GetDataRefElement("sim/flightmodel/position/lon_ref", TypeStart_float, UtilConstants.Flag_No, "degrees", "The longitude of the point 0,0,0 in OpenGL coordinates."); }
                return flightmodelPositionLonRef;
            }
        }

        private static DataRefElement flightmodelPositionLatitude = null;
        /// <summary>
        /// The latitude of the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionLatitude {
            get {
                if (flightmodelPositionLatitude == null)
                { flightmodelPositionLatitude = GetDataRefElement("sim/flightmodel/position/latitude", TypeStart_double, UtilConstants.Flag_No, "degrees", "The latitude of the aircraft"); }
                return flightmodelPositionLatitude;
            }
        }

        private static DataRefElement flightmodelPositionLongitude = null;
        /// <summary>
        /// The longitude of the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionLongitude {
            get {
                if (flightmodelPositionLongitude == null)
                { flightmodelPositionLongitude = GetDataRefElement("sim/flightmodel/position/longitude", TypeStart_double, UtilConstants.Flag_No, "degrees", "The longitude of the aircraft"); }
                return flightmodelPositionLongitude;
            }
        }

        private static DataRefElement flightmodelPositionElevation = null;
        /// <summary>
        /// The elevation above MSL of the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionElevation {
            get {
                if (flightmodelPositionElevation == null)
                { flightmodelPositionElevation = GetDataRefElement("sim/flightmodel/position/elevation", TypeStart_double, UtilConstants.Flag_No, "meters", "The elevation above MSL of the aircraft"); }
                return flightmodelPositionElevation;
            }
        }

        private static DataRefElement flightmodelPositionTheta = null;
        /// <summary>
        /// The pitch relative to the plane normal to the Y axis in degrees - OpenGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionTheta {
            get {
                if (flightmodelPositionTheta == null)
                { flightmodelPositionTheta = GetDataRefElement("sim/flightmodel/position/theta", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The pitch relative to the plane normal to the Y axis in degrees - OpenGL coordinates"); }
                return flightmodelPositionTheta;
            }
        }

        private static DataRefElement flightmodelPositionPhi = null;
        /// <summary>
        /// The roll of the aircraft in degrees - OpenGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionPhi {
            get {
                if (flightmodelPositionPhi == null)
                { flightmodelPositionPhi = GetDataRefElement("sim/flightmodel/position/phi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The roll of the aircraft in degrees - OpenGL coordinates"); }
                return flightmodelPositionPhi;
            }
        }

        private static DataRefElement flightmodelPositionPsi = null;
        /// <summary>
        /// The true heading of the aircraft in degrees from the Z axis - OpenGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionPsi {
            get {
                if (flightmodelPositionPsi == null)
                { flightmodelPositionPsi = GetDataRefElement("sim/flightmodel/position/psi", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "The true heading of the aircraft in degrees from the Z axis - OpenGL coordinates"); }
                return flightmodelPositionPsi;
            }
        }
        
        private static DataRefElement flightmodelPositionTrueTheta = null;
        /// <summary>
        /// The pitch of the aircraft relative to the earth precisely below the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionTrueTheta {
            get {
                if (flightmodelPositionTrueTheta == null)
                { flightmodelPositionTrueTheta = GetDataRefElement("sim/flightmodel/position/true_theta", TypeStart_float, UtilConstants.Flag_No, "degrees", "The pitch of the aircraft relative to the earth precisely below the aircraft"); }
                return flightmodelPositionTrueTheta;
            }
        }

        private static DataRefElement flightmodelPositionTruePhi = null;
        /// <summary>
        /// The roll of the aircraft relative to the earth precisely below the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionTruePhi {
            get {
                if (flightmodelPositionTruePhi == null)
                { flightmodelPositionTruePhi = GetDataRefElement("sim/flightmodel/position/true_phi", TypeStart_float, UtilConstants.Flag_No, "degrees", "The roll of the aircraft relative to the earth precisely below the aircraft"); }
                return flightmodelPositionTruePhi;
            }
        }

        private static DataRefElement flightmodelPositionTruePsi = null;
        /// <summary>
        /// The heading of the aircraft relative to the earth precisely below the aircraft - true degrees north, always
        /// </summary>
        public static DataRefElement FlightmodelPositionTruePsi {
            get {
                if (flightmodelPositionTruePsi == null)
                { flightmodelPositionTruePsi = GetDataRefElement("sim/flightmodel/position/true_psi", TypeStart_float, UtilConstants.Flag_No, "degrees", "The heading of the aircraft relative to the earth precisely below the aircraft - true degrees north, always"); }
                return flightmodelPositionTruePsi;
            }
        }

        private static DataRefElement flightmodelPositionMagPsi = null;
        /// <summary>
        /// The real magnetic heading of the aircraft - the old magpsi dataref was FUBAR
        /// </summary>
        public static DataRefElement FlightmodelPositionMagPsi {
            get {
                if (flightmodelPositionMagPsi == null)
                { flightmodelPositionMagPsi = GetDataRefElement("sim/flightmodel/position/mag_psi", TypeStart_float, UtilConstants.Flag_No, "degrees", "The real magnetic heading of the aircraft - the old magpsi dataref was FUBAR"); }
                return flightmodelPositionMagPsi;
            }
        }

        private static DataRefElement flightmodelPositionLocalVx = null;
        /// <summary>
        /// The velocity in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalVx {
            get {
                if (flightmodelPositionLocalVx == null)
                { flightmodelPositionLocalVx = GetDataRefElement("sim/flightmodel/position/local_vx", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec", "The velocity in local OGL coordinates"); }
                return flightmodelPositionLocalVx;
            }
        }

        private static DataRefElement flightmodelPositionLocalVy = null;
        /// <summary>
        /// The velocity in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalVy {
            get {
                if (flightmodelPositionLocalVy == null)
                { flightmodelPositionLocalVy = GetDataRefElement("sim/flightmodel/position/local_vy", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec", "The velocity in local OGL coordinates"); }
                return flightmodelPositionLocalVy;
            }
        }
        
        private static DataRefElement flightmodelPositionLocalVz = null;
        /// <summary>
        /// The velocity in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalVz {
            get {
                if (flightmodelPositionLocalVz == null)
                { flightmodelPositionLocalVz = GetDataRefElement("sim/flightmodel/position/local_vz", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec", "The velocity in local OGL coordinates"); }
                return flightmodelPositionLocalVz;
            }
        }

        private static DataRefElement flightmodelPositionLocalAx = null;
        /// <summary>
        /// The acceleration in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalAx {
            get {
                if (flightmodelPositionLocalAx == null)
                { flightmodelPositionLocalAx = GetDataRefElement("sim/flightmodel/position/local_ax", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec2", "The acceleration in local OGL coordinates"); }
                return flightmodelPositionLocalAx;
            }
        }

        private static DataRefElement flightmodelPositionLocalAy = null;
        /// <summary>
        /// The acceleration in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalAy {
            get {
                if (flightmodelPositionLocalAy == null)
                { flightmodelPositionLocalAy = GetDataRefElement("sim/flightmodel/position/local_ay", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec2", "The acceleration in local OGL coordinates"); }
                return flightmodelPositionLocalAy;
            }
        }

        private static DataRefElement flightmodelPositionLocalAz = null;
        /// <summary>
        /// The acceleration in local OGL coordinates
        /// </summary>
        public static DataRefElement FlightmodelPositionLocalAz {
            get {
                if (flightmodelPositionLocalAz == null)
                { flightmodelPositionLocalAz = GetDataRefElement("sim/flightmodel/position/local_az", TypeStart_float, UtilConstants.Flag_Yes, "mtr/sec2", "The acceleration in local OGL coordinates"); }
                return flightmodelPositionLocalAz;
            }
        }

        private static DataRefElement flightmodelPositionAlpha = null;
        /// <summary>
        /// The pitch relative to the flown path (angle of attack)
        /// </summary>
        public static DataRefElement FlightmodelPositionAlpha {
            get {
                if (flightmodelPositionAlpha == null)
                { flightmodelPositionAlpha = GetDataRefElement("sim/flightmodel/position/alpha", TypeStart_float, UtilConstants.Flag_No, "degrees", "The pitch relative to the flown path (angle of attack)"); }
                return flightmodelPositionAlpha;
            }
        }

        private static DataRefElement flightmodelPositionBeta = null;
        /// <summary>
        /// The heading relative to the flown path (yaw)
        /// </summary>
        public static DataRefElement FlightmodelPositionBeta {
            get {
                if (flightmodelPositionBeta == null)
                { flightmodelPositionBeta = GetDataRefElement("sim/flightmodel/position/beta", TypeStart_float, UtilConstants.Flag_No, "degrees", "The heading relative to the flown path (yaw)"); }
                return flightmodelPositionBeta;
            }
        }

        private static DataRefElement flightmodelPositionVpath = null;
        /// <summary>
        /// The pitch the aircraft actually flies.  (vpath+alpha=theta)
        /// </summary>
        public static DataRefElement FlightmodelPositionVpath {
            get {
                if (flightmodelPositionVpath == null)
                { flightmodelPositionVpath = GetDataRefElement("sim/flightmodel/position/vpath", TypeStart_float, UtilConstants.Flag_No, "degrees", "The pitch the aircraft actually flies.  (vpath+alpha=theta)"); }
                return flightmodelPositionVpath;
            }
        }

        private static DataRefElement flightmodelPositionHpath = null;
        /// <summary>
        /// The heading the aircraft actually flies.  (hpath+beta=psi)
        /// </summary>
        public static DataRefElement FlightmodelPositionHpath {
            get {
                if (flightmodelPositionHpath == null)
                { flightmodelPositionHpath = GetDataRefElement("sim/flightmodel/position/hpath", TypeStart_float, UtilConstants.Flag_No, "degrees", "The heading the aircraft actually flies.  (hpath+beta=psi)"); }
                return flightmodelPositionHpath;
            }
        }

        private static DataRefElement flightmodelPositionGroundspeed = null;
        /// <summary>
        /// The ground speed of the aircraft
        /// </summary>
        public static DataRefElement FlightmodelPositionGroundspeed {
            get {
                if (flightmodelPositionGroundspeed == null)
                { flightmodelPositionGroundspeed = GetDataRefElement("sim/flightmodel/position/groundspeed", TypeStart_float, UtilConstants.Flag_No, "meters/sec", "The ground speed of the aircraft"); }
                return flightmodelPositionGroundspeed;
            }
        }

        private static DataRefElement flightmodelPositionIndicatedAirspeed = null;
        /// <summary>
        /// Air speed indicated - this takes into account air density and wind direction
        /// </summary>
        public static DataRefElement FlightmodelPositionIndicatedAirspeed {
            get {
                if (flightmodelPositionIndicatedAirspeed == null)
                { flightmodelPositionIndicatedAirspeed = GetDataRefElement("sim/flightmodel/position/indicated_airspeed", TypeStart_float, UtilConstants.Flag_Yes, "kias", "Air speed indicated - this takes into account air density and wind direction"); }
                return flightmodelPositionIndicatedAirspeed;
            }
        }

        private static DataRefElement flightmodelPositionIndicatedAirspeed2 = null;
        /// <summary>
        /// Air speed indicated - this takes into account air density and wind direction
        /// </summary>
        public static DataRefElement FlightmodelPositionIndicatedAirspeed2 {
            get {
                if (flightmodelPositionIndicatedAirspeed2 == null)
                { flightmodelPositionIndicatedAirspeed2 = GetDataRefElement("sim/flightmodel/position/indicated_airspeed2", TypeStart_float, UtilConstants.Flag_Yes, "kias", "Air speed indicated - this takes into account air density and wind direction"); }
                return flightmodelPositionIndicatedAirspeed2;
            }
        }

        private static DataRefElement flightmodelPositionTrueAirspeed = null;
        /// <summary>
        /// Air speed true - this does not take into account air density at altitude!
        /// </summary>
        public static DataRefElement FlightmodelPositionTrueAirspeed {
            get {
                if (flightmodelPositionTrueAirspeed == null)
                { flightmodelPositionTrueAirspeed = GetDataRefElement("sim/flightmodel/position/true_airspeed", TypeStart_float, UtilConstants.Flag_No, "meters/sec", "Air speed true - this does not take into account air density at altitude!"); }
                return flightmodelPositionTrueAirspeed;
            }
        }

        private static DataRefElement flightmodelPositionMagneticVariation = null;
        /// <summary>
        /// The local magnetic variation
        /// </summary>
        public static DataRefElement FlightmodelPositionMagneticVariation {
            get {
                if (flightmodelPositionMagneticVariation == null)
                { flightmodelPositionMagneticVariation = GetDataRefElement("sim/flightmodel/position/magnetic_variation", TypeStart_float, UtilConstants.Flag_No, "degrees", "The local magnetic variation"); }
                return flightmodelPositionMagneticVariation;
            }
        }

        private static DataRefElement flightmodelPositionM = null;
        /// <summary>
        /// The angular momentum of the aircraft (relative to flight axis).
        /// </summary>
        public static DataRefElement FlightmodelPositionM {
            get {
                if (flightmodelPositionM == null)
                { flightmodelPositionM = GetDataRefElement("sim/flightmodel/position/M", TypeStart_float, UtilConstants.Flag_No, "NM", "The angular momentum of the aircraft (relative to flight axis)."); }
                return flightmodelPositionM;
            }
        }

        private static DataRefElement flightmodelPositionN = null;
        /// <summary>
        /// The angular momentum of the aircraft (relative to flight axis)
        /// </summary>
        public static DataRefElement FlightmodelPositionN {
            get {
                if (flightmodelPositionN == null)
                { flightmodelPositionN = GetDataRefElement("sim/flightmodel/position/N", TypeStart_float, UtilConstants.Flag_No, "NM", "The angular momentum of the aircraft (relative to flight axis)"); }
                return flightmodelPositionN;
            }
        }

        private static DataRefElement flightmodelPositionL = null;
        /// <summary>
        /// The angular momentum of the aircraft (relative to flight axis)
        /// </summary>
        public static DataRefElement FlightmodelPositionL {
            get {
                if (flightmodelPositionL == null)
                { flightmodelPositionL = GetDataRefElement("sim/flightmodel/position/L", TypeStart_float, UtilConstants.Flag_No, "NM", "The angular momentum of the aircraft (relative to flight axis)"); }
                return flightmodelPositionL;
            }
        }

        private static DataRefElement flightmodelPositionP = null;
        /// <summary>
        /// The roll rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionP {
            get {
                if (flightmodelPositionP == null)
                { flightmodelPositionP = GetDataRefElement("sim/flightmodel/position/P", TypeStart_float, UtilConstants.Flag_Yes, "deg/sec", "The roll rotation rates (relative to the flight)"); }
                return flightmodelPositionP;
            }
        }

        private static DataRefElement flightmodelPositionQ = null;
        /// <summary>
        /// The pitch rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionQ {
            get {
                if (flightmodelPositionQ == null)
                { flightmodelPositionQ = GetDataRefElement("sim/flightmodel/position/Q", TypeStart_float, UtilConstants.Flag_Yes, "deg/sec", "The pitch rotation rates (relative to the flight)"); }
                return flightmodelPositionQ;
            }
        }

        private static DataRefElement flightmodelPositionR = null;
        /// <summary>
        /// The yaw rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionR {
            get {
                if (flightmodelPositionR == null)
                { flightmodelPositionR = GetDataRefElement("sim/flightmodel/position/R", TypeStart_float, UtilConstants.Flag_Yes, "deg/sec", "The yaw rotation rates (relative to the flight)"); }
                return flightmodelPositionR;
            }
        }

        private static DataRefElement flightmodelPositionPDot = null;
        /// <summary>
        /// The roll angular acceleration (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionPDot {
            get {
                if (flightmodelPositionPDot == null)
                { flightmodelPositionPDot = GetDataRefElement("sim/flightmodel/position/P_dot", TypeStart_float, UtilConstants.Flag_No, "deg/sec2", "The roll angular acceleration (relative to the flight)"); }
                return flightmodelPositionPDot;
            }
        }

        private static DataRefElement flightmodelPositionQDot = null;
        /// <summary>
        /// The pitch angular acceleration (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionQDot {
            get {
                if (flightmodelPositionQDot == null)
                { flightmodelPositionQDot = GetDataRefElement("sim/flightmodel/position/Q_dot", TypeStart_float, UtilConstants.Flag_No, "deg/sec2", "The pitch angular acceleration (relative to the flight)"); }
                return flightmodelPositionQDot;
            }
        }

        private static DataRefElement flightmodelPositionRDot = null;
        /// <summary>
        /// The yaw angular acceleration rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionRDot {
            get {
                if (flightmodelPositionRDot == null)
                { flightmodelPositionRDot = GetDataRefElement("sim/flightmodel/position/R_dot", TypeStart_float, UtilConstants.Flag_No, "deg/sec2", "The yaw angular acceleration rates (relative to the flight)"); }
                return flightmodelPositionRDot;
            }
        }

        private static DataRefElement flightmodelPositionPrad = null;
        /// <summary>
        /// The roll rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionPrad {
            get {
                if (flightmodelPositionPrad == null)
                { flightmodelPositionPrad = GetDataRefElement("sim/flightmodel/position/Prad", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "The roll rotation rates (relative to the flight)"); }
                return flightmodelPositionPrad;
            }
        }

        private static DataRefElement flightmodelPositionQrad = null;
        /// <summary>
        /// The pitch rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionQrad {
            get {
                if (flightmodelPositionQrad == null)
                { flightmodelPositionQrad = GetDataRefElement("sim/flightmodel/position/Qrad", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "The pitch rotation rates (relative to the flight)"); }
                return flightmodelPositionQrad;
            }
        }

        private static DataRefElement flightmodelPositionRrad = null;
        /// <summary>
        /// The yaw rotation rates (relative to the flight)
        /// </summary>
        public static DataRefElement FlightmodelPositionRrad {
            get {
                if (flightmodelPositionRrad == null)
                { flightmodelPositionRrad = GetDataRefElement("sim/flightmodel/position/Rrad", TypeStart_float, UtilConstants.Flag_Yes, "rad/sec", "The yaw rotation rates (relative to the flight)"); }
                return flightmodelPositionRrad;
            }
        }

        private static DataRefElement flightmodelPositionq = null;
        /// <summary>
        /// A quaternion representing the rotation from local OpenGL coordinates to the aircraft's coordinates.
        /// </summary>
        public static DataRefElement FlightmodelPositionq {
            get {
                if (flightmodelPositionq == null)
                { flightmodelPositionq = GetDataRefElement("sim/flightmodel/position/q", TypeStart_float4, UtilConstants.Flag_Yes, "quaternion", "A quaternion representing the rotation from local OpenGL coordinates to the aircraft's coordinates."); }
                return flightmodelPositionq;
            }
        }

        private static DataRefElement flightmodelPositionVhInd = null;
        /// <summary>
        /// VVI (vertical velocity in meters per second)
        /// </summary>
        public static DataRefElement FlightmodelPositionVhInd {
            get {
                if (flightmodelPositionVhInd == null)
                { flightmodelPositionVhInd = GetDataRefElement("sim/flightmodel/position/vh_ind", TypeStart_float, UtilConstants.Flag_No, "meters/second", "VVI (vertical velocity in meters per second)"); }
                return flightmodelPositionVhInd;
            }
        }

        private static DataRefElement flightmodelPositionVhIndFpm = null;
        /// <summary>
        /// VVI (vertical velocity in feet per second)
        /// </summary>
        public static DataRefElement FlightmodelPositionVhIndFpm {
            get {
                if (flightmodelPositionVhIndFpm == null)
                { flightmodelPositionVhIndFpm = GetDataRefElement("sim/flightmodel/position/vh_ind_fpm", TypeStart_float, UtilConstants.Flag_Yes, "fpm", "VVI (vertical velocity in feet per second)"); }
                return flightmodelPositionVhIndFpm;
            }
        }

        private static DataRefElement flightmodelPositionVhIndFpm2 = null;
        /// <summary>
        /// VVI (vertical velocity in feet per second)
        /// </summary>
        public static DataRefElement FlightmodelPositionVhIndFpm2 {
            get {
                if (flightmodelPositionVhIndFpm2 == null)
                { flightmodelPositionVhIndFpm2 = GetDataRefElement("sim/flightmodel/position/vh_ind_fpm2", TypeStart_float, UtilConstants.Flag_Yes, "fpm", "VVI (vertical velocity in feet per second)"); }
                return flightmodelPositionVhIndFpm2;
            }
        }
        
        
        public static DataRefElement FlightmodelPositionYAgl
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/position/y_agl",
                    Units = "meters",
                    Description = "AGL",
                    
                };
            }
        }
        public static DataRefElement FlightmodelTransmissionsXmsnPress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/transmissions/xmsn_press",
                    Units = "???",
                    Description = "transmission pressure",
                    
                };
            }
        }
        public static DataRefElement FlightmodelTransmissionsXmsnTemp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/transmissions/xmsn_temp",
                    Units = "???",
                    Description = "transmission temperature",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFixed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fixed",
                    Units = "kgs",
                    Description = "Payload Weight",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_total",
                    Units = "kgs",
                    Description = "Total Weight",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFuel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fuel",
                    Units = "kgs",
                    Description = "Fuel Tank Weight - for 9 tanks",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFuel1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fuel1",
                    Units = "kgs",
                    Description = "Fuel Tank 1 Weight",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFuel2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fuel2",
                    Units = "kgs",
                    Description = "Fuel Tank 2 Weight",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFuel3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fuel3",
                    Units = "kgs",
                    Description = "Fuel Tank 3 Weight",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMJettison
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_jettison",
                    Units = "kgs",
                    Description = "Jettison",
                    
                };
            }
        }
        public static DataRefElement FlightmodelWeightMFuelTotal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/flightmodel/weight/m_fuel_total",
                    Units = "kgs",
                    Description = "Fuel Total Weight",
                    
                };
            }
        }
    }
}
