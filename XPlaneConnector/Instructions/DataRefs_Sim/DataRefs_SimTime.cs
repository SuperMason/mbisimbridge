﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement timeTimerIsRunningSec = null;
        /// <summary>
        /// Is the timer running?
        /// </summary>
        public static DataRefElement TimeTimerIsRunningSec {
            get {
                if (timeTimerIsRunningSec == null)
                { timeTimerIsRunningSec = GetDataRefElement("sim/time/timer_is_running_sec", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is the timer running?"); }
                return timeTimerIsRunningSec;
            }
        }

        private static DataRefElement timeTotalRunningTimeSec = null;
        /// <summary>
        /// Total time the sim has been up
        /// </summary>
        public static DataRefElement TimeTotalRunningTimeSec {
            get {
                if (timeTotalRunningTimeSec == null)
                { timeTotalRunningTimeSec = GetDataRefElement("sim/time/total_running_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Total time the sim has been up"); }
                return timeTotalRunningTimeSec;
            }
        }

        private static DataRefElement timeTotalFlightTimeSec = null;
        /// <summary>
        /// Total time since the flight got reset by something
        /// </summary>
        public static DataRefElement TimeTotalFlightTimeSec {
            get {
                if (timeTotalFlightTimeSec == null)
                { timeTotalFlightTimeSec = GetDataRefElement("sim/time/total_flight_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Total time since the flight got reset by something"); }
                return timeTotalFlightTimeSec;
            }
        }

        private static DataRefElement timeTimerElapsedTimeSec = null;
        /// <summary>
        /// Total time elapsed on the timer
        /// </summary>
        public static DataRefElement TimeTimerElapsedTimeSec {
            get {
                if (timeTimerElapsedTimeSec == null)
                { timeTimerElapsedTimeSec = GetDataRefElement("sim/time/timer_elapsed_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Total time elapsed on the timer"); }
                return timeTimerElapsedTimeSec;
            }
        }

        private static DataRefElement timeLocalTimeSec = null;
        /// <summary>
        /// Local time  (seconds since midnight??)
        /// </summary>
        public static DataRefElement TimeLocalTimeSec {
            get {
                if (timeLocalTimeSec == null)
                { timeLocalTimeSec = GetDataRefElement("sim/time/local_time_sec", TypeStart_float, UtilConstants.Flag_No, "seconds", "Local time  (seconds since midnight??)"); }
                return timeLocalTimeSec;
            }
        }

        private static DataRefElement timeZuluTimeSec = null;
        /// <summary>
        /// Zulu time  (seconds since midnight??)
        /// </summary>
        public static DataRefElement TimeZuluTimeSec {
            get {
                if (timeZuluTimeSec == null)
                { timeZuluTimeSec = GetDataRefElement("sim/time/zulu_time_sec", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "Zulu time  (seconds since midnight??)"); }
                return timeZuluTimeSec;
            }
        }

        private static DataRefElement timeLocalDateDays = null;
        /// <summary>
        /// Date in days since January 1st
        /// </summary>
        public static DataRefElement TimeLocalDateDays {
            get {
                if (timeLocalDateDays == null)
                { timeLocalDateDays = GetDataRefElement("sim/time/local_date_days", TypeStart_int, UtilConstants.Flag_Yes, "days", "Date in days since January 1st"); }
                return timeLocalDateDays;
            }
        }

        private static DataRefElement timeUseSystemTime = null;
        /// <summary>
        /// Use system date and time for local time
        /// </summary>
        public static DataRefElement TimeUseSystemTime {
            get {
                if (timeUseSystemTime == null)
                { timeUseSystemTime = GetDataRefElement("sim/time/use_system_time", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Use system date and time for local time"); }
                return timeUseSystemTime;
            }
        }

        private static DataRefElement timePaused = null;
        /// <summary>
        /// Is the sim paused?  Use cmd keys to change this.
        /// </summary>
        public static DataRefElement TimePaused {
            get {
                if (timePaused == null)
                { timePaused = GetDataRefElement("sim/time/paused", TypeStart_int, UtilConstants.Flag_No, "boolean", "Is the sim paused?  Use cmd keys to change this."); }
                return timePaused;
            }
        }

        private static DataRefElement timeSimSpeed = null;
        /// <summary>
        /// This is the multiplier for real-time...1 = realtime, 2 = 2x, 0 = paused, etc.
        /// </summary>
        public static DataRefElement TimeSimSpeed {
            get {
                if (timeSimSpeed == null)
                { timeSimSpeed = GetDataRefElement("sim/time/sim_speed", TypeStart_int, UtilConstants.Flag_Yes, "ratio", "This is the multiplier for real-time...1 = realtime, 2 = 2x, 0 = paused, etc."); }
                return timeSimSpeed;
            }
        }

        private static DataRefElement timeSimSpeedActual = null;
        /// <summary>
        /// The actual time-speed increase the sim has achieved - takes into account fps limiting.
        /// </summary>
        public static DataRefElement TimeSimSpeedActual {
            get {
                if (timeSimSpeedActual == null)
                { timeSimSpeedActual = GetDataRefElement("sim/time/sim_speed_actual", TypeStart_float, UtilConstants.Flag_No, "ratio", "The actual time-speed increase the sim has achieved - takes into account fps limiting."); }
                return timeSimSpeedActual;
            }
        }

        private static DataRefElement timeSimSpeedActualOgl = null;
        /// <summary>
        /// The actual time-speed increase the sim has achieved - takes into account fps limiting and time dilation if fps<19.
        /// </summary>
        public static DataRefElement TimeSimSpeedActualOgl {
            get {
                if (timeSimSpeedActualOgl == null)
                { timeSimSpeedActualOgl = GetDataRefElement("sim/time/sim_speed_actual_ogl", TypeStart_float, UtilConstants.Flag_No, "ratio", "The actual time-speed increase the sim has achieved - takes into account fps limiting and time dilation if fps<19."); }
                return timeSimSpeedActualOgl;
            }
        }

        private static DataRefElement timeGroundSpeed = null;
        /// <summary>
        /// This is the multiplier on ground speed, for faster travel via double-distance
        /// </summary>
        public static DataRefElement TimeGroundSpeed {
            get {
                if (timeGroundSpeed == null)
                { timeGroundSpeed = GetDataRefElement("sim/time/ground_speed", TypeStart_int, UtilConstants.Flag_Yes, "ratio", "This is the multiplier on ground speed, for faster travel via double-distance"); }
                return timeGroundSpeed;
            }
        }

        private static DataRefElement timeGroundSpeedFlt = null;
        /// <summary>
        /// This is the multiplier on ground speed, as float value, for faster travel via double-distance
        /// </summary>
        public static DataRefElement TimeGroundSpeedFlt {
            get {
                if (timeGroundSpeedFlt == null)
                { timeGroundSpeedFlt = GetDataRefElement("sim/time/ground_speed_flt", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the multiplier on ground speed, as float value, for faster travel via double-distance"); }
                return timeGroundSpeedFlt;
            }
        }

        private static DataRefElement timeHobbsTime = null;
        /// <summary>
        /// elapsed time on the Hobbs meter
        /// </summary>
        public static DataRefElement TimeHobbsTime {
            get {
                if (timeHobbsTime == null)
                { timeHobbsTime = GetDataRefElement("sim/time/hobbs_time", TypeStart_float, UtilConstants.Flag_Yes, "seconds", "elapsed time on the Hobbs meter"); }
                return timeHobbsTime;
            }
        }

        private static DataRefElement timeIsInReplay = null;
        /// <summary>
        /// true if we are in replay mode, false if we are flying
        /// </summary>
        public static DataRefElement TimeIsInReplay {
            get {
                if (timeIsInReplay == null)
                { timeIsInReplay = GetDataRefElement("sim/time/is_in_replay", TypeStart_int, UtilConstants.Flag_No, "boolean", "true if we are in replay mode, false if we are flying"); }
                return timeIsInReplay;
            }
        }

        private static DataRefElement timeFrameratePeriod = null;
        /// <summary>
        /// Smoothed time to draw each full sim frame in seconds.  This is 1.0 / FPS.
        /// </summary>
        public static DataRefElement TimeFrameratePeriod {
            get {
                if (timeFrameratePeriod == null)
                { timeFrameratePeriod = GetDataRefElement("sim/time/framerate_period", TypeStart_float, UtilConstants.Flag_No, "seconds", "Smoothed time to draw each full sim frame in seconds.  This is 1.0 / FPS."); }
                return timeFrameratePeriod;
            }
        }

        private static DataRefElement timeGpuTimePerFrameSecApprox = null;
        /// <summary>
        /// This is an approximate estimate of GPU time spent per each frame in seconds. Note that it can be RADICALLY wrong if any thing is done wrong by plugins or the driver is weird. It's for entertainment value only !!!
        /// </summary>
        public static DataRefElement TimeGpuTimePerFrameSecApprox {
            get {
                if (timeGpuTimePerFrameSecApprox == null)
                { timeGpuTimePerFrameSecApprox = GetDataRefElement("sim/time/gpu_time_per_frame_sec_approx", TypeStart_float, UtilConstants.Flag_No, "seconds", "This is an approximate estimate of GPU time spent per each frame in seconds.  Note that it can be RADICALLY wrong if anything is done wrong by plugins or the driver is weird.  It's for entertainment value only!!!"); }
                return timeGpuTimePerFrameSecApprox;
            }
        }
    }
}