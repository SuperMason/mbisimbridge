﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement versionSimBuildString = null;
        /// <summary>
        /// This string contains the date and time that this x-plane was built.
        /// </summary>
        public static DataRefElement VersionSimBuildString {
            get {
                if (versionSimBuildString == null)
                { versionSimBuildString = GetDataRefElement("sim/version/sim_build_string", TypeStart_byte256, UtilConstants.Flag_No, "string", "This string contains the date and time that this x-plane was built."); }
                return versionSimBuildString;
            }
        }

        private static DataRefElement versionXplmBuildString = null;
        /// <summary>
        /// This string contains the date and time that the 'XPLM' DLL was built.
        /// </summary>
        public static DataRefElement VersionXplmBuildString {
            get {
                if (versionXplmBuildString == null)
                { versionXplmBuildString = GetDataRefElement("sim/version/xplm_build_string", TypeStart_byte256, UtilConstants.Flag_No, "string", "This string contains the date and time that the 'XPLM' DLL was built."); }
                return versionXplmBuildString;
            }
        }

        private static DataRefElement versionXplaneeInternalVersion = null;
        /// <summary>
        /// This is the internal build number - it is a unique integer that always increases and is unique with each beta.  For example, 10.51b5 might be 105105.  There is no guarantee that the build numbe (last 2 digits) are in sync with the official beta number.
        /// </summary>
        public static DataRefElement VersionXplaneeInternalVersion {
            get {
                if (versionXplaneeInternalVersion == null)
                { versionXplaneeInternalVersion = GetDataRefElement("sim/version/xplanee_internal_version", TypeStart_int, UtilConstants.Flag_No, "integer", "This is the internal build number - it is a unique integer that always increases and is unique with each beta.  For example, 10.51b5 might be 105105.  There is no guarantee that the build numbe (last 2 digits) are in sync with the official beta number."); }
                return versionXplaneeInternalVersion;
            }
        }

        private static DataRefElement versionXplaneInternalVersion = null;
        /// <summary>
        /// This is the internal build number - it is a unique integer that always increases and is unique with each beta.  For example, 10.51b5 might be 105105.  There is no guarantee that the build numbe (last 2 digits) are in sync with the official beta number.
        /// </summary>
        public static DataRefElement VersionXplaneInternalVersion {
            get {
                if (versionXplaneInternalVersion == null)
                { versionXplaneInternalVersion = GetDataRefElement("sim/version/xplane_internal_version", TypeStart_int, UtilConstants.Flag_No, "integer", "This is the internal build number - it is a unique integer that always increases and is unique with each beta.  For example, 10.51b5 might be 105105.  There is no guarantee that the build numbe (last 2 digits) are in sync with the official beta number."); }
                return versionXplaneInternalVersion;
            }
        }
    }
}