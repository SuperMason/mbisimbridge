﻿using MBILibrary.Util;
using System.Collections.Generic;
using System.Reflection;
using XPlaneConnector.DataSwitch.MbiEvtInterfaces;
using XPlaneConnector.DataRef;
using System;
using MBILibrary.Enums;

namespace XPlaneConnector.Instructions
{
    /// <summary>
    /// X-Plane DataRef 指令集
    /// </summary>
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Global Event Interface for subscribing Event Handler
        /// </summary>
        public static EvtInterfaceService StaticEvtInterface { get; set; }

        #region Properties, 指令集的版本 ::: ver. 1151 (2020-11-23 19:30:15)
        public DateTime RevDate { get; } = new DateTime(2020, 11, 23, 19, 30, 15);
        public int RevBuild { get; } = 1151;
        #endregion

        #region All DataRef's Types ::: byte, double, float, int, string
        public const string TypeNotDefineErrMsg = "ERROR '{0}' {1} ::: '{2}' is NOT Defined, please check it.";
        public const string ManuallyDefinitionType = "Manually Definition Type";
        public const string SystemClassType = "System Class Type";
        public const string TypeStart_byte = "byte";
        public const string TypeStart_byte8 = "byte[8]";
        public const string TypeStart_byte24 = "byte[24]";
        public const string TypeStart_byte40 = "byte[40]";
        public const string TypeStart_byte96 = "byte[96]";
        public const string TypeStart_byte150 = "byte[150]";
        public const string TypeStart_byte240 = "byte[240]";
        public const string TypeStart_byte256 = "byte[256]";
        public const string TypeStart_byte260 = "byte[260]";
        public const string TypeStart_byte500 = "byte[500]";
        public const string TypeStart_byte512 = "byte[512]";
        public const string TypeStart_byte1024 = "byte[1024]";
        public const string TypeStart_byte2920 = "byte[2920]";
        /// <summary>
        /// 官網沒有 double[] 的 DataRef 參數, 只有 double
        /// </summary>
        public const string TypeStart_double = "double";
        public const string TypeStart_float = "float";
        /// <summary>
        /// 此 type 值無法寫入 X-Plane, 既不是單一值, 也不是 array 值, 其他 array 值皆正常寫入
        /// <para>1. 此類 DataRef, 軟體實際顯示 float[1]</para>
        /// <para>2. 但 X-Plane 官網卻定義為 float[20], 有出入</para>
        /// </summary>
        public const string TypeStart_float1 = "float[1]";
        public const string TypeStart_float2 = "float[2]";
        public const string TypeStart_float3 = "float[3]";
        public const string TypeStart_float4 = "float[4]";
        public const string TypeStart_float5 = "float[5]";
        public const string TypeStart_float6 = "float[6]";
        public const string TypeStart_float8 = "float[8]";
        public const string TypeStart_float8_10 = "float[8][10]";
        public const string TypeStart_float8_3 = "float[8][3]";
        public const string TypeStart_float9 = "float[9]";
        public const string TypeStart_float10 = "float[10]";
        public const string TypeStart_float12 = "float[12]";
        public const string TypeStart_float14 = "float[14]";
        public const string TypeStart_float16 = "float[16]";
        public const string TypeStart_float20 = "float[20]";
        public const string TypeStart_float24 = "float[24]";
        public const string TypeStart_float25 = "float[25]";
        public const string TypeStart_float32 = "float[32]";
        public const string TypeStart_float50 = "float[50]";
        public const string TypeStart_float56 = "float[56]";
        public const string TypeStart_float56_2 = "float[56][2]";
        public const string TypeStart_float56_2_2 = "float[56][2][2]";
        public const string TypeStart_float56_10_4 = "float[56][10][4]";
        public const string TypeStart_float56_2_2_721 = "float[56][2][2][721]";
        public const string TypeStart_float64 = "float[64]";
        public const string TypeStart_float67 = "float[67]";
        public const string TypeStart_float73_4 = "float[73][4]";
        public const string TypeStart_float73_10 = "float[73][10]";
        public const string TypeStart_float73_10_4 = "float[73][10][4]";
        public const string TypeStart_float95 = "float[95]";
        public const string TypeStart_float128 = "float[128]";
        public const string TypeStart_float320 = "float[320]";
        public const string TypeStart_float500 = "float[500]";
        public const string TypeStart_float730 = "float[730]";
        public const string TypeStart_float2920 = "float[2920]";
        public const string TypeStart_int = "int";
        public const string TypeStart_int2 = "int[2]";
        public const string TypeStart_int3 = "int[3]";
        public const string TypeStart_int4 = "int[4]";
        public const string TypeStart_int6 = "int[6]";
        public const string TypeStart_int8 = "int[8]";
        public const string TypeStart_int9 = "int[9]";
        public const string TypeStart_int10 = "int[10]";
        public const string TypeStart_int12 = "int[12]";
        public const string TypeStart_int19 = "int[19]";
        public const string TypeStart_int20 = "int[20]";
        public const string TypeStart_int24 = "int[24]";
        public const string TypeStart_int25 = "int[25]";
        public const string TypeStart_int50 = "int[50]";
        public const string TypeStart_int56 = "int[56]";
        public const string TypeStart_int64 = "int[64]";
        public const string TypeStart_int67 = "int[67]";
        public const string TypeStart_int73_10 = "int[73][10]";
        public const string TypeStart_int95 = "int[95]";
        public const string TypeStart_int200 = "int[200]";
        public const string TypeStart_int500 = "int[500]";
        public const string TypeStart_int564 = "int[564]";
        public const string TypeStart_int3200 = "int[3200]";
        public const string TypeStart_string = "string";
        #endregion

        /// <summary>
        /// 統一控制 X-Plane 的 Response Time, UDPClient 的 ReceiveAsync() 速度
        /// <para>定義 ::: Times per seconds X-Plane will be sending this value</para>
        /// <para>意即 ::: 要求 X-Plane 每秒送出該 DataRef value 的次數頻率, Frequency 值越大, X-Plane 更新 DataRef 越快</para>
        /// <para></para>
        /// <para>value 越小, ReceiveAsync() 越慢. EX: value= 1, 平均 ReceiveAsync() 約 1500 ms</para>
        /// <para>value 越大, ReceiveAsync() 越快. EX: value=50, 平均 ReceiveAsync() 約   70 ms</para>
        /// <para>value 大於 50 之後, 速度頂多 70 ms, 無法再快了, 要從別處調整效能</para>
        /// <para></para>
        /// <para>例如: </para>
        /// <para>value =  500, 即  500次/1000毫秒, 也就是要求 X-Plane 每 2 毫秒回覆 1 次</para>
        /// <para>value = 1000, 即 1000次/1000毫秒, 也就是要求 X-Plane 每 1 毫秒回覆 1 次</para>
        /// <para>value = 1000000, 即 1000000次/1000毫秒, 也就是要求 X-Plane 每 1 微秒回覆 1 次</para>
        /// </summary>
        public const int DefaultFrequency = 1000;

        /// <summary>
        /// 取得必要的 DataRefs
        /// </summary>
        public static List<DataRefElementBase> GetMustNeedDataRefs() {
            var initDataRefs = new List<DataRefElementBase>() {
                NetworkDataoutNetworkDataRate,
                AircraftAutopilotVviStepFt,
                FlightmodelPositionLatitude,
                FlightmodelPositionLongitude,
                FlightmodelPositionTruePhi,
                FlightmodelPositionTrueTheta,
                FlightmodelPositionTruePsi,
                FlightmodelPositionPrad,
                FlightmodelPositionQrad,
                FlightmodelPositionRrad,
                FlightmodelPositionVhIndFpm,
                FlightmodelPositionElevation,
                FlightmodelPositionGroundspeed,
                FlightmodelMiscMachno,
                FlightmodelMovingpartsGear1def,
                FlightmodelMiscCdOverall,
                Flightmodel2MiscGforceNormal,
                Flightmodel2PositionAlpha,
                Flightmodel2GearOnGround,
                CockpitSwitchesAntiIceSurfHeatLeft,
                CockpitRadiosCom1FreqHz,
                CockpitWeaponsGunsArmed,
                CockpitElectricalHUDOn,
                Cockpit2ControlsFlapRatio,
                Cockpit2ControlsThrustVectorRatio,
                Cockpit2CameraCameraFieldOfView,
                Cockpit2ElectricalBatteryOn,
                Cockpit2SwitchesNavigationLightsOn,
                Cockpit2EngineActuatorsThrottleRatioAll,
                Cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot,
                Cockpit2GaugesIndicatorsSideslipDegrees,
                
                // For electrical system
                CockpitElectricalNavLightsOn,
                CockpitElectricalLandingLightsOn,
                CockpitElectricalTaxiLightOn,

                // HUD
                FlightmodelForcesVxAcfAxis,
                FlightmodelForcesVyAcfAxis,
                FlightmodelForcesVzAcfAxis,
                FlightmodelPositionIndicatedAirspeed,
                FlightmodelPositionTrueAirspeed,
                CockpitElectricalHUDBrightness,
                Cockpit2GaugesIndicatorsAltitudeFtPilot,
                Cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot,
                FlightmodelForcesGNrml,
                FlightmodelPositionMagPsi,
                FlightmodelPositionPhi,
                CockpitSwitchesGearHandleStatus,
                Cockpit2RadiosIndicatorsNav1VdefDotsPilot,
                GraphicsViewVerticalFieldOfViewDeg,
                MultiplayerPositionPlane2X,
                MultiplayerPositionPlane2Y,
                MultiplayerPositionPlane3X,
                MultiplayerPositionPlane3Y,
                MultiplayerPositionPlane3Z,

                // FCR
                CockpitWeaponsPlaneTargetIndex,
                Cockpit2TcasIndicatorsRelativeBearingDegs,
                Cockpit2TcasIndicatorsRelativeDistanceMtrs,
                Cockpit2TcasTargetsPositionEle,
                Cockpit2TcasTargetsPositionThe,
                Cockpit2TcasTargetsPositionHpath,
            };
#if DEBUG
            // 00. double, int, float, int[], float[] 皆完成寫入驗證成功, 可由 Air Manager 寫入
            // 01. double, int, float, int[], float[] 皆完成寫入驗證成功, 也可由 EventHandler 寫入, 並且帶入 offset 位移量
            // 02. byte, double[] 官網沒有此 DataType, 已有 APIs, 但尚未驗證官方 DataRefs I/O
            // 03. string (byte[]) 官網註記為系統 bug... 待解, 說明如下 :::

            /**
                 * 目前無法正常寫入 byte[] 的字串值, 這是 X-Plane 的 bug, 之後官方會修正
                 * 查詢資料來源:
                 *              https://forums.x-plane.org/index.php?/forums/topic/251469-udp-setdataref-dref-with-byte-arrays-does-it-work/   2021/08/28 15:05:00 posted
                 *              Laminar responds:
                 *                              This is a design limitation--the packet can't write byte-array datarefs. 
                 *                              This has been filed as feature request XPD-11353. 
                 *                              That number will be listed in release notes if or when it is included in an X-Plane update. 
                 **/
            if (UtilConstants.appConfig.Debug4BigMason) {
                //initDataRefs.Add(Cockpit2RadiosActuatorsFlightId);        // 測試寫 byte[]
                //initDataRefs.Add(Cockpit2TcasTargetsFlightId);            // 測試寫 byte[]
                //initDataRefs.Add(Cockpit2TcasTargetsIcaoType);            // 測試寫 byte[]
                initDataRefs.Add(AircraftViewAcfTailnum);                 // 測試寫 byte[]
                //initDataRefs.Add(AircraftViewAcfNotes);                   // 測試寫 byte[]
                initDataRefs.Add(NetworkDataoutDataToScreen);               // 測試寫 int[]
                initDataRefs.Add(FlightmodelPositionLocalX);                // 測試寫 double
                initDataRefs.Add(FlightmodelPositionLocalY);                // 測試寫 double
                initDataRefs.Add(FlightmodelPositionLocalZ);                // 測試寫 double
                initDataRefs.Add(MultiplayerControlsEngineThrottleRequest); // 測試寫 float
                initDataRefs.Add(AircraftForcefeedbackAcfFfHydraulic);      // 測試寫 int
                initDataRefs.Add(AircraftViewAcfModesId);                   // 測試寫 int
                initDataRefs.Add(MultiplayerPositionPlane6X);               // 測試寫 double
                initDataRefs.Add(MultiplayerPositionPlane6The);             // 測試寫 float
                initDataRefs.Add(MultiplayerPositionPlane6BeaconLightsOn);  // 測試寫 int
                initDataRefs.Add(MultiplayerControlsEngineMixtureRequest);  // 測試寫 float[1]
                initDataRefs.Add(MultiplayerPositionPlane1Throttle);        // 測試寫 float[8]
                initDataRefs.Add(MultiplayerPositionPlane6Throttle);        // 測試寫 float[8]
                initDataRefs.Add(MultiplayerPositionPlane6GearDeploy);      // 測試寫 float[10]
                initDataRefs.Add(TimeTotalRunningTimeSec);                  // 驗證 X-Plane 的 Response 正常
                initDataRefs.Add(TimeFrameratePeriod);
                initDataRefs.Add(NetworkMiscNetworkTimeSec);
            }
#endif
            return initDataRefs;
        }

        /// <summary>
        /// 訂閱全部的 DataRefs, 共 4687 個, 壓力測試先用 2000 個測試, 一般情況最多使用 200 個
        /// </summary>
        public static List<DataRefElementBase> GetAllDataRefs() {
            var outList = new List<DataRefElementBase>();
            var allStaticProperties = GetAllStaticProperties();
            int cnt = 0;
            foreach (var propInfo in allStaticProperties) {
                if (cnt == UtilConstants.MaxSubscribeCNT) // 超過 2000 個 C++ 會出問題, 所以先弄 2000 個測試
                { UtilGlobalFunc.Log($"'DataRef' Subscribing Counter ::: {cnt}"); break; }
                ++cnt;

                object propObj = propInfo.GetValue(null, null);
                if (propObj.GetType() == typeof(DataRefElement))
                { outList.Add((DataRefElement)propObj); }
                else if (propObj.GetType() == typeof(StringDataRefElement))
                { outList.Add((StringDataRefElement)propObj); }
                else { --cnt; }
            }

            return outList;
        }

        #region Function()
        /// <summary>
        /// 取得所有 X-Plane 的 DataRef 定義
        /// </summary>
        private static PropertyInfo[] GetAllStaticProperties()
        { return typeof(DataRefs).GetProperties(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy); }
        /// <summary>
        /// 取得 'string' 的 DataRefElement
        /// <para>DataType 意義轉換</para>
        /// <para>Air Manager 端是 :: 'STRING'</para>
        /// <para>X-Plane     端是 :: 'byte[n]', 每一個 byte, X-Plane 會先 response byte[4] 的 float data, 再自行轉成 char (ASCII Code)</para>
        /// <para>例如:</para>
        /// <para>'9' -> [0, 0, 100, 66] => byte[4] 轉成 float 為 57, 代表 '9' char </para>
        /// <para>'a' -> [0, 0, 194, 66] => byte[4] 轉成 float 為 97, 代表 'a' char</para>
        /// <para>'A' -> [0, 0, 130, 66] => byte[4] 轉成 float 為 65, 代表 'A' char</para>
        /// </summary>
        public static StringDataRefElement GetStringDataRefElement(string dataRef, string type = TypeStart_string, string writable = UtilConstants.Flag_Yes
            , string units = "units", string description = "description", int strLen = UtilConstants.DefaultStringLength, int frequency = DefaultFrequency) {
            return new StringDataRefElement(strLen) {
                DataRef = dataRef,
                Type = type,
                Writable = writable,
                Units = units,
                Description = description,
                StringLength = strLen,
                Frequency = frequency
            };
        }
        /// <summary>
        /// 取得 float 的 DataRefElement
        /// </summary>
        public static DataRefElement GetDataRefElement(string dataRef, string type, string writable = UtilConstants.Flag_Yes
            , string units = "units", string description = "description", int frequency = DefaultFrequency) {
            return new DataRefElement {
                DataRef = dataRef,
                Type = type,
                Writable = writable,
                Units = units,
                Description = description,
                Frequency = frequency
            };
        }
        #endregion


        #region DataRefs
        private static DataRefElement aircraftSystemsFdirNeededToEngageServos = null;
        /// <summary>
        /// If this is true, commands to engage the AP servos will be ignored if the FD is not on.
        /// </summary>
        public static DataRefElement AircraftSystemsFdirNeededToEngageServos {
            get {
                if (aircraftSystemsFdirNeededToEngageServos == null)
                { aircraftSystemsFdirNeededToEngageServos = GetDataRefElement("sim/aircraft/systems/fdir_needed_to_engage_servos", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "If this is true, commands to engage the AP servos will be ignored if the FD is not on."); }
                return aircraftSystemsFdirNeededToEngageServos;
            }
        }

        private static DataRefElement cockpitG430G430NavComSel = null;
        /// <summary>
        /// enter com=0, enter_nav=1
        /// </summary>
        public static DataRefElement CockpitG430G430NavComSel {
            get {
                if (cockpitG430G430NavComSel == null)
                { cockpitG430G430NavComSel = GetDataRefElement("sim/cockpit/g430/g430_nav_com_sel", TypeStart_int2, UtilConstants.Flag_Yes, "enum", "enter com=0, enter_nav=1"); }
                return cockpitG430G430NavComSel;
            }
        }
        
        private static DataRefElement cockpitG1000Gcu478InputSel = null;
        /// <summary>
        /// FMS=0 XPDR=1 COM=2 NAV=3
        /// </summary>
        public static DataRefElement CockpitG1000Gcu478InputSel {
            get {
                if (cockpitG1000Gcu478InputSel == null)
                { cockpitG1000Gcu478InputSel = GetDataRefElement("sim/cockpit/g1000/gcu478_input_sel", TypeStart_int, UtilConstants.Flag_Yes, "enum", "FMS=0 XPDR=1 COM=2 NAV=3"); }
                return cockpitG1000Gcu478InputSel;
            }
        }

        private static DataRefElement multiplayerCombatTeamStatus = null;
        /// <summary>
        /// Is the aircraft friend or foe? (0 = neutral, 1 = friend, 2 = enemy)
        /// </summary>
        public static DataRefElement MultiplayerCombatTeamStatus {
            get {
                if (multiplayerCombatTeamStatus == null)
                { multiplayerCombatTeamStatus = GetDataRefElement("sim/multiplayer/combat/team_status", TypeStart_int20, UtilConstants.Flag_No, "enum", "Is the aircraft friend or foe? (0 = neutral, 1 = friend, 2 = enemy)"); }
                return multiplayerCombatTeamStatus;
            }
        }

        private static DataRefElement testTestFloat = null;
        /// <summary>
        /// this test dataref is used internally for testing our models
        /// </summary>
        public static DataRefElement TestTestFloat {
            get {
                if (testTestFloat == null)
                { testTestFloat = GetDataRefElement("sim/test/test_float", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "this test dataref is used internally for testing our models"); }
                return testTestFloat;
            }
        }
        #endregion
    }
}