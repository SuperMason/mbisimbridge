﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Nav radio 1 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav1Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav1_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Nav radio 1 off or on, 0 or 1."); } }
        /// <summary>
        /// Nav radio 2 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav2Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav2_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Nav radio 2 off or on, 0 or 1."); } }
        /// <summary>
        /// Nav radio N off or on, 0 or 1. Was 0-10 pre-11.10. Radios 11 and 12 are WAAS/EGNOS receivers
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNavPower
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav_power", TypeStart_int12, UtilConstants.Flag_Yes, "boolean", "Nav radio N off or on, 0 or 1. Was 0-10 pre-11.10. Radios 11 and 12 are WAAS/EGNOS receivers"); } }
        /// <summary>
        /// Com radio 1 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsCom1Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/com1_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Com radio 1 off or on, 0 or 1."); } }
        /// <summary>
        /// Com radio 2 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsCom2Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/com2_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Com radio 2 off or on, 0 or 1."); } }
        /// <summary>
        /// ADF radio 1 off or on, 0 = off, 1 = antenna, 2 = on, 3 = tone, 4 = test
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsAdf1Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/adf1_power", TypeStart_int, UtilConstants.Flag_Yes, "enum", "ADF radio 1 off or on, 0 = off, 1 = antenna, 2 = on, 3 = tone, 4 = test"); } }
        /// <summary>
        /// ADF radio 2 off or on, 0 = off, 1 = antenna, 2 = on, 3 = tone, 4 = test
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsAdf2Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/adf2_power", TypeStart_int, UtilConstants.Flag_Yes, "enum", "ADF radio 2 off or on, 0 = off, 1 = antenna, 2 = on, 3 = tone, 4 = test"); } }
        /// <summary>
        /// GPS 1 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsGpsPower
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/gps_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "GPS 1 off or on, 0 or 1."); } }
        /// <summary>
        /// GPS 2 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsGps2Power
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/gps2_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "GPS 2 off or on, 0 or 1."); } }
        /// <summary>
        /// DME radio 1 off or on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsDmePower
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/dme_power", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "DME radio 1 off or on, 0 or 1."); } }
        /// <summary>
        /// Nav radio 1 frequency, hz
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav1FrequencyHz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav1_frequency_hz", TypeStart_int, UtilConstants.Flag_Yes, "10hertz", "Nav radio 1 frequency, hz"); } }
        /// <summary>
        /// Nav radio 1 frequency, Mhz component only
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav1FrequencyMhz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav1_frequency_Mhz", TypeStart_int, UtilConstants.Flag_Yes, "Mhz", "Nav radio 1 frequency, Mhz component only"); } }
        /// <summary>
        /// Nav radio 1 frequency, khz component only
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav1FrequencyKhz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav1_frequency_khz", TypeStart_int, UtilConstants.Flag_Yes, "khz", "Nav radio 1 frequency, khz component only"); } }
        /// <summary>
        /// Nav radio 2 frequency, hz
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav2FrequencyHz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav2_frequency_hz", TypeStart_int, UtilConstants.Flag_Yes, "10hertz", "Nav radio 2 frequency, hz"); } }
        /// <summary>
        /// Nav radio 2 frequency, Mhz component only
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav2FrequencyMhz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav2_frequency_Mhz", TypeStart_int, UtilConstants.Flag_Yes, "Mhz", "Nav radio 2 frequency, Mhz component only"); } }
        /// <summary>
        /// Nav radio 2 frequency, khz component only
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsNav2FrequencyKhz
        { get { return GetDataRefElement("sim/cockpit2/radios/actuators/nav2_frequency_khz", TypeStart_int, UtilConstants.Flag_Yes, "khz", "Nav radio 2 frequency, khz component only"); } }
        public static DataRefElement Cockpit2RadiosActuatorsNavFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_frequency_hz",
                    Units = "10hertz",
                    Description = "Nav radio N frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Nav radio N frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_frequency_khz",
                    Units = "khz",
                    Description = "Nav radio N frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavDmeHold
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_dme_hold",
                    Units = "boolean",
                    Description = "Nav radio N in DME hold, stops slaving DME frequency to NAV frequency",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavDmeFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_dme_frequency_hz",
                    Units = "10hertz",
                    Description = "DME radio N frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavDmeFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_dme_frequency_Mhz",
                    Units = "Mhz",
                    Description = "DME radio N frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavDmeFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_dme_frequency_khz",
                    Units = "khz",
                    Description = "DME radio N frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1FrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_frequency_hz",
                    Units = "10hertz",
                    Description = "Com radio 1 frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1FrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Com radio 1 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1FrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_frequency_khz",
                    Units = "khz",
                    Description = "Com radio 1 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1FrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_frequency_hz_833",
                    Units = "hertz",
                    Description = "Com radio 1 frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2FrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_frequency_hz",
                    Units = "10hertz",
                    Description = "Com radio 2 frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2FrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Com radio 2 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2FrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_frequency_khz",
                    Units = "khz",
                    Description = "Com radio 2 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2FrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_frequency_hz_833",
                    Units = "hertz",
                    Description = "Com radio 2 frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1FrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_frequency_hz",
                    Units = "10hertz",
                    Description = "ADF radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2FrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_frequency_hz",
                    Units = "10hertz",
                    Description = "ADF radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDmeFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/dme_frequency_hz",
                    Units = "10hertz",
                    Description = "DME radio frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "Nav radio 1 standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1StandbyFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_standby_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Standby Nav radio 1 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1StandbyFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_standby_frequency_khz",
                    Units = "khz",
                    Description = "Standby Nav radio 1 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "Nav radio 2 standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2StandbyFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_standby_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Standby Nav radio 2 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2StandbyFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_standby_frequency_khz",
                    Units = "khz",
                    Description = "Standby Nav radio 2 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavStandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "Nav radio N standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavStandbyFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_standby_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Standby Nav radio N frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavStandbyFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_standby_frequency_khz",
                    Units = "khz",
                    Description = "Standby Nav radio N frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "Com radio 1 standby frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1StandbyFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_standby_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Standby Com radio 1 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1StandbyFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_standby_frequency_khz",
                    Units = "khz",
                    Description = "Standby Com radio 1 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1StandbyFrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_standby_frequency_hz_833",
                    Units = "hertz",
                    Description = "Com radio 1 standby frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "Com radio 2 standby frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2StandbyFrequencyMhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_standby_frequency_Mhz",
                    Units = "Mhz",
                    Description = "Standby Com radio 2 frequency, Mhz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2StandbyFrequencyKhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_standby_frequency_khz",
                    Units = "khz",
                    Description = "Standby com radio 2 frequency, khz component only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2StandbyFrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_standby_frequency_hz_833",
                    Units = "hertz",
                    Description = "Com radio 2 standby frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "ADF radio 1 standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2StandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "ADF radio 2 standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDmeStandbyFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/dme_standby_frequency_hz",
                    Units = "10hertz",
                    Description = "DME radio standby frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1ObsDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_obs_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "OBS 1 (pilot side) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2ObsDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_obs_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "OBS 2 (pilot isde) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavObsDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_obs_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "OBS N (pilot isde) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1ObsDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_obs_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "OBS 1 (copilot side) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2ObsDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_obs_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "OBS 2 (copilot isde) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavObsDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_obs_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "OBS N (copilot isde) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1CardHeadingDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_card_heading_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "CARD selection for ADF 1, pilot side, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2CardHeadingDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_card_heading_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "CARD selection for ADF 2, pilot side, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1CardHeadingDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_card_heading_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "CARD selection for ADF 1, copilot side, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2CardHeadingDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_card_heading_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "CARD selection for ADF 2, copilot side, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1CourseDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_course_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on nav1, pilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2CourseDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_course_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on nav2, pilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavCourseDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_course_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on navN, pilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1CourseDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_course_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on nav1, copilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2CourseDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_course_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on nav2, copilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavCourseDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_course_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Selected course based on navN, copilot, degrees magnetic.  This is OBS for VORs, or localizer heading for ILS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsHSISourceSelectPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/HSI_source_select_pilot",
                    Units = "enum",
                    Description = "HSI source to display: 0 for Nav1, 1 for Nav2, 2 for GPS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsHSISourceSelectCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/HSI_source_select_copilot",
                    Units = "enum",
                    Description = "HSI source to display: 0 for Nav1, 1 for Nav2, 2 for GPS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMISourceSelectPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_source_select_pilot",
                    Units = "enum",
                    Description = "RMI source to display: 0 for Nav1, 1 for Nav2, 2 for GPS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMISourceSelectCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_source_select_copilot",
                    Units = "enum",
                    Description = "RMI source to display: 0 for Nav1, 1 for Nav2, 2 for GPS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMILeftUseAdfPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_left_use_adf_pilot",
                    Units = "enum",
                    Description = "RMI is taking ADF (1) or VOR (0).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMILeftUseAdfCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_left_use_adf_copilot",
                    Units = "enum",
                    Description = "RMI is taking ADF (1) or VOR (0).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMIRightUseAdfPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_right_use_adf_pilot",
                    Units = "enum",
                    Description = "RMI is taking ADF (1) or VOR (0).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsRMIRightUseAdfCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/RMI_right_use_adf_copilot",
                    Units = "enum",
                    Description = "RMI is taking ADF (1) or VOR (0).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDMEMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/DME_mode",
                    Units = "enum",
                    Description = "DME display mode, where 0 is remote, 1 is frequency, and 2 is groundspeed-time.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDMESlaveSource
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/DME_slave_source",
                    Units = "enum",
                    Description = "DME display selection of what NAV radio to display. 0 for Nav1, 1for Nav2.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNavComAdfMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav_com_adf_mode",
                    Units = "enum",
                    Description = "Frequency-to-change selection for the all-in-one radio, 0->5 are Nav1, Nav2, Com1, Com2, ADF1, ADF2.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsTransponderCode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/transponder_code",
                    Units = "transponder_code",
                    Description = "Current Transponder code (0000-7777)",
                    
                };
            }
        }

        private static DataRefElement cockpit2RadiosActuatorsFlightId = null;
        /// <summary>
        /// Flight ID, as transmitted in Mode-S. Up to 7 characters, like N844X or KLM511. This is NOT the Mode-S hexcode!
        /// <para>這個參數值與 AircraftViewAcfTailnum 一樣</para>
        /// </summary>
        public static DataRefElement Cockpit2RadiosActuatorsFlightId {
            get {
                if (cockpit2RadiosActuatorsFlightId == null)
                { cockpit2RadiosActuatorsFlightId = GetDataRefElement("sim/cockpit2/radios/actuators/flight_id", TypeStart_byte8, UtilConstants.Flag_Yes, TypeStart_byte8, "Flight ID, as transmitted in Mode-S. Up to 7 characters, like N844X or KLM511. This is NOT the Mode-S hexcode!"); }
                return cockpit2RadiosActuatorsFlightId;
            }
        }

        
        public static DataRefElement Cockpit2RadiosActuatorsTransponderMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/transponder_mode",
                    Units = "enum",
                    Description = "Transponder mode (off=0,stdby=1,on=2,test=3)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioComSelection
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_com_selection",
                    Units = "enum",
                    Description = "6=com1,7=com2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioNavSelection
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_nav_selection",
                    Units = "enum",
                    Description = "0=nav1,1=nav2,2=adf1,3=adf2,9=none",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioComSelectionMan
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_com_selection_man",
                    Units = "enum",
                    Description = "6=com1,7=com2 - this is for old audio panels that do not automatically select the listener as well",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionComAuto
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_com_auto",
                    Units = "boolean",
                    Description = "is com automatically selected for listening (selects the same as the transmit selector)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionCom1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_com1",
                    Units = "boolean",
                    Description = "is com1 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionCom2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_com2",
                    Units = "boolean",
                    Description = "is com2 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionNav1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_nav1",
                    Units = "boolean",
                    Description = "is nav1 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionNav2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_nav2",
                    Units = "boolean",
                    Description = "is nav2 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionAdf1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_adf1",
                    Units = "boolean",
                    Description = "is adf1 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionAdf2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_adf2",
                    Units = "boolean",
                    Description = "is adf2 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioDmeEnabled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_dme_enabled",
                    Units = "boolean",
                    Description = "Is DME audio enabled?  This only listens to the dedicated DME receiver",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionDme1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_dme1",
                    Units = "boolean",
                    Description = "Is DME portion on NAV1 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioSelectionDme2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_selection_dme2",
                    Units = "boolean",
                    Description = "Is DME portion on NAV2 selected for listening",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioMarkerEnabled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_marker_enabled",
                    Units = "boolean",
                    Description = "Is audio for the marker beacons enabled?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeCom1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_com1",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for com1 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeCom2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_com2",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for com2 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeNav1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_nav1",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for nav1 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeNav2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_nav2",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for nav2 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeAdf1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_adf1",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for adf1 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeAdf2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_adf2",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for adf2 audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_dme",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for dme audio on standalone DME receiver",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeDme1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_dme1",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for dme audio on NAV1 receiver",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeDme2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_dme2",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for dme audio on NAV2 receiver",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAudioVolumeMark
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/audio_volume_mark",
                    Units = "[0..1]",
                    Description = "Audio level (0 is off, 1 is max) for marker beacon audio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsHsiObsDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/hsi_obs_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "HSI OBS (pilot side) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsHsiObsDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/hsi_obs_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "HSI OBS (copilot side) selection, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left Nav radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left Nav radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left Com radio 1 frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1LeftFrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_left_frequency_hz_833",
                    Units = "hertz",
                    Description = "Left Nav radio 1 frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left Com radio 2 frequency, 10 hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2LeftFrequencyHz833
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_left_frequency_hz_833",
                    Units = "hertz",
                    Description = "Left Nav radio 2 frequency, hz, supports 8.3 khz spacing",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left ADF radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2LeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left ADF radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDmeLeftFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/dme_left_frequency_hz",
                    Units = "10hertz",
                    Description = "Left DME radio frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right Nav radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right Nav radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right Com radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right Com radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right ADF radio 1 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2RightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right ADF radio 2 frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDmeRightFrequencyHz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/dme_right_frequency_hz",
                    Units = "10hertz",
                    Description = "Right DME radio frequency, hz",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav1RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav1_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right nav 1 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsNav2RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/nav2_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right nav 2 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom1RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com1_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right com 1 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsCom2RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/com2_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right com 2 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf1RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf1_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right adf 1 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsAdf2RightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/adf2_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right adf 2 radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsDmeRightIsSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/dme_right_is_selected",
                    Units = "boolean",
                    Description = "1 if right DME radio is selected, 0 if left is selected",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosActuatorsMarkerSens
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/actuators/marker_sens",
                    Units = "enum",
                    Description = "0 = Hi Sens, 1 = Low sens",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1BearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the nav1 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2BearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the nav2 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavBearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the nav N navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1BearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the adf1 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2BearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the adf2 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsBearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the selected GPS 1 destination",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2BearingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_bearing_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the selected GPS 2 destination",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the nav1 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the nav2 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the nav N navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1RelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the adf1 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2RelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the adf2 navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the selected GPS 1 destination",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeBearingDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_bearing_deg",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the selected GPS 2 destination",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1FlagFromToPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_flag_from_to_pilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav1, pilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2FlagFromToPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_flag_from_to_pilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav2, pilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavFlagFromToPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_flag_from_to_pilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, navN, pilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1FlagFromToCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_flag_from_to_copilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav1, copilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2FlagFromToCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_flag_from_to_copilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav2, copilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavFlagFromToCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_flag_from_to_copilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, navN, copilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1FlagGlideslope
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_flag_glideslope",
                    Units = "boolean",
                    Description = "Glide slope flag, nav1 - EFIS style flag that shows when glideslope is expected, but not received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2FlagGlideslope
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_flag_glideslope",
                    Units = "boolean",
                    Description = "Glide slope flag, nav2 - EFIS style flag that shows when glideslope is expected, but not received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavFlagGlideslope
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_flag_glideslope",
                    Units = "boolean",
                    Description = "Glide slope flag, nav N - EFIS style flag that shows when glideslope is expected, but not received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1FlagGlideslopeMech
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_flag_glideslope_mech",
                    Units = "boolean",
                    Description = "Glide slope flag, nav1 - mechanical instrument flag that shows whenever no glideslope signal is received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2FlagGlideslopeMech
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_flag_glideslope_mech",
                    Units = "boolean",
                    Description = "Glide slope flag, nav2 - mechanical instrument flag that shows whenever no glideslope signal is received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavFlagGlideslopeMech
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_flag_glideslope_mech",
                    Units = "boolean",
                    Description = "Glide slope flag, nav N - mechanical instrument flag that shows whenever no glideslope signal is received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DisplayHorizontal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_display_horizontal",
                    Units = "boolean",
                    Description = "Is there some kind of horizontal signal on nav1",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DisplayHorizontal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_display_horizontal",
                    Units = "boolean",
                    Description = "Is there some kind of horizontal signal on nav2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavDisplayHorizontal
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_display_horizontal",
                    Units = "boolean",
                    Description = "Is there some kind of horizontal signal on nav N",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DisplayVertical
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_display_vertical",
                    Units = "boolean",
                    Description = "Is there some kind of vertical signal on nav1",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DisplayVertical
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_display_vertical",
                    Units = "boolean",
                    Description = "Is there some kind of vertical signal on nav2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavDisplayVertical
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_display_vertical",
                    Units = "boolean",
                    Description = "Is there some kind of vertical signal on nav N",
                    
                };
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav1HdefDotsPilot = null;
        /// <summary>
        /// CDI lateral deflection in dots, nav1, pilot
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav1HdefDotsPilot {
            get {
                if (cockpit2RadiosIndicatorsNav1HdefDotsPilot == null)
                { cockpit2RadiosIndicatorsNav1HdefDotsPilot = GetDataRefElement("sim/cockpit2/radios/indicators/nav1_hdef_dots_pilot", TypeStart_float, UtilConstants.Flag_No, "dots", "CDI lateral deflection in dots, nav1, pilot"); }
                return cockpit2RadiosIndicatorsNav1HdefDotsPilot;
            }
        }

        public static DataRefElement Cockpit2RadiosIndicatorsNav2HdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_hdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, nav2, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavHdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_hdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, navN, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsHdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_hdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, gps 1, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2HdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_hdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, gps 2, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1HdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, nav1, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2HdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, nav2, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavHdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, navN, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsHdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, gps, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2HdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, gps 2, copilot",
                    
                };
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav1VdefDotsPilot = null;
        /// <summary>
        /// CDI vertical deflection in dots, nav1, pilot
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav1VdefDotsPilot {
            get {
                if (cockpit2RadiosIndicatorsNav1VdefDotsPilot == null)
                { cockpit2RadiosIndicatorsNav1VdefDotsPilot = GetDataRefElement("sim/cockpit2/radios/indicators/nav1_vdef_dots_pilot", TypeStart_float, UtilConstants.Flag_No, "dots", "CDI vertical deflection in dots, nav1, pilot"); }
                return cockpit2RadiosIndicatorsNav1VdefDotsPilot;
            }
        }

        public static DataRefElement Cockpit2RadiosIndicatorsNav2VdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_vdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, nav2, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavVdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_vdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, navN, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1VdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_vdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, nav1, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2VdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_vdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, nav2, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavVdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_vdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, navN, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from nav1's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from nav2's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavHasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from navN's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from ADF1's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from ADF2's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsHasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from GPS 1?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2HasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from GPS 2?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsDmeHasDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/dme_has_dme",
                    Units = "boolean",
                    Description = "Is there a DME signal from standalone DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "nav1 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "nav2 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavDmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "navN DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1DmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "adf1 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2DmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "adf2 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsDmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "gps 1 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2DmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "gps 2 DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsDmeDmeDistanceNm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/dme_dme_distance_nm",
                    Units = "nautical_miles",
                    Description = "standalone dme DME distance in nautical miles.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_dme_speed_kts",
                    Units = "knots",
                    Description = "nav1 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_dme_speed_kts",
                    Units = "knots",
                    Description = "nav2 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavDmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_dme_speed_kts",
                    Units = "knots",
                    Description = "navN DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_dme_speed_kts",
                    Units = "knots",
                    Description = "adf1 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_dme_speed_kts",
                    Units = "knots",
                    Description = "adf2 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsDmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_dme_speed_kts",
                    Units = "knots",
                    Description = "gps 1 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2DmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_dme_speed_kts",
                    Units = "knots",
                    Description = "gps 2 DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsDmeDmeSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/dme_dme_speed_kts",
                    Units = "knots",
                    Description = "standalone dme DME speed in knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_dme_time_min",
                    Units = "minutes",
                    Description = "nav1 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_dme_time_min",
                    Units = "minutes",
                    Description = "nav2 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavDmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_dme_time_min",
                    Units = "minutes",
                    Description = "navN DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1DmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf1_dme_time_min",
                    Units = "minutes",
                    Description = "adf1 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2DmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/adf2_dme_time_min",
                    Units = "minutes",
                    Description = "adf2 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsDmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_dme_time_min",
                    Units = "minutes",
                    Description = "gps 1 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2DmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_dme_time_min",
                    Units = "minutes",
                    Description = "gps 2 DME time in minutes.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsDmeDmeTimeMin
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/dme_dme_time_min",
                    Units = "minutes",
                    Description = "standalone dme DME time in minutes.",
                    
                };
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav1NavId = null;
        /// <summary>
        /// current selected navID - nav radio 1
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav1NavId {
            get {
                if (cockpit2RadiosIndicatorsNav1NavId == null)
                { cockpit2RadiosIndicatorsNav1NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav1_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 1"); }
                return cockpit2RadiosIndicatorsNav1NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav2NavId = null;
        /// <summary>
        /// current selected navID - nav radio 2
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav2NavId {
            get {
                if (cockpit2RadiosIndicatorsNav2NavId == null)
                { cockpit2RadiosIndicatorsNav2NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav2_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 2"); }
                return cockpit2RadiosIndicatorsNav2NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsAdf1NavId = null;
        /// <summary>
        /// current selected navID - ADF 1
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsAdf1NavId {
            get {
                if (cockpit2RadiosIndicatorsAdf1NavId == null)
                { cockpit2RadiosIndicatorsAdf1NavId = GetDataRefElement("sim/cockpit2/radios/indicators/adf1_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - ADF 1"); }
                return cockpit2RadiosIndicatorsAdf1NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsAdf2NavId = null;
        /// <summary>
        /// current selected navID - ADF 2
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsAdf2NavId {
            get {
                if (cockpit2RadiosIndicatorsAdf2NavId == null)
                { cockpit2RadiosIndicatorsAdf2NavId = GetDataRefElement("sim/cockpit2/radios/indicators/adf2_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - ADF 2"); }
                return cockpit2RadiosIndicatorsAdf2NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsGpsNavId = null;
        /// <summary>
        /// current selected navID - GPS 1
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsGpsNavId {
            get {
                if (cockpit2RadiosIndicatorsGpsNavId == null)
                { cockpit2RadiosIndicatorsGpsNavId = GetDataRefElement("sim/cockpit2/radios/indicators/gps_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - GPS 1"); }
                return cockpit2RadiosIndicatorsGpsNavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsGps2NavId = null;
        /// <summary>
        /// current selected navID - GPS 2
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsGps2NavId
        {
            get {
                if (cockpit2RadiosIndicatorsGps2NavId == null)
                { cockpit2RadiosIndicatorsGps2NavId = GetDataRefElement("sim/cockpit2/radios/indicators/gps2_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - GPS 2"); }
                return cockpit2RadiosIndicatorsGps2NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsDmeNavId = null;
        /// <summary>
        /// current selected navID - DME
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsDmeNavId {
            get {
                if (cockpit2RadiosIndicatorsDmeNavId == null)
                { cockpit2RadiosIndicatorsDmeNavId = GetDataRefElement("sim/cockpit2/radios/indicators/dme_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - DME"); }
                return cockpit2RadiosIndicatorsDmeNavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav3NavId = null;
        /// <summary>
        /// current selected navID - nav radio 3
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav3NavId {
            get {
                if (cockpit2RadiosIndicatorsNav3NavId == null)
                { cockpit2RadiosIndicatorsNav3NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav3_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 3"); }
                return cockpit2RadiosIndicatorsNav3NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav4NavId = null;
        /// <summary>
        /// current selected navID - nav radio 4
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav4NavId {
            get {
                if (cockpit2RadiosIndicatorsNav4NavId == null)
                { cockpit2RadiosIndicatorsNav4NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav4_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 4"); }
                return cockpit2RadiosIndicatorsNav4NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav5NavId = null;
        /// <summary>
        /// current selected navID - nav radio 5
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav5NavId {
            get {
                if (cockpit2RadiosIndicatorsNav5NavId == null)
                { cockpit2RadiosIndicatorsNav5NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav5_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 5"); }
                return cockpit2RadiosIndicatorsNav5NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav6NavId = null;
        /// <summary>
        /// current selected navID - nav radio 6
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav6NavId {
            get {
                if (cockpit2RadiosIndicatorsNav6NavId == null)
                { cockpit2RadiosIndicatorsNav6NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav6_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 6"); }
                return cockpit2RadiosIndicatorsNav6NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav7NavId = null;
        /// <summary>
        /// current selected navID - nav radio 7
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav7NavId {
            get {
                if (cockpit2RadiosIndicatorsNav7NavId == null)
                { cockpit2RadiosIndicatorsNav7NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav7_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 7"); }
                return cockpit2RadiosIndicatorsNav7NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav8NavId = null;
        /// <summary>
        /// current selected navID - nav radio 8
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav8NavId {
            get {
                if (cockpit2RadiosIndicatorsNav8NavId == null)
                { cockpit2RadiosIndicatorsNav8NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav8_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 8"); }
                return cockpit2RadiosIndicatorsNav8NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav9NavId = null;
        /// <summary>
        /// current selected navID - nav radio 9
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav9NavId {
            get {
                if (cockpit2RadiosIndicatorsNav9NavId == null)
                { cockpit2RadiosIndicatorsNav9NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav9_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 9"); }
                return cockpit2RadiosIndicatorsNav9NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav10NavId = null;
        /// <summary>
        /// current selected navID - nav radio 10
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav10NavId {
            get {
                if (cockpit2RadiosIndicatorsNav10NavId == null)
                { cockpit2RadiosIndicatorsNav10NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav10_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 10"); }
                return cockpit2RadiosIndicatorsNav10NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav11NavId = null;
        /// <summary>
        /// current selected navID - nav radio 11. WAAS/EGNOS receiver
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav11NavId {
            get {
                if (cockpit2RadiosIndicatorsNav11NavId == null)
                { cockpit2RadiosIndicatorsNav11NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav11_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 11. WAAS/EGNOS receiver"); }
                return cockpit2RadiosIndicatorsNav11NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav12NavId = null;
        /// <summary>
        /// current selected navID - nav radio 12. WAAS/EGNOS receiver
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav12NavId {
            get {
                if (cockpit2RadiosIndicatorsNav12NavId == null)
                { cockpit2RadiosIndicatorsNav12NavId = GetDataRefElement("sim/cockpit2/radios/indicators/nav12_nav_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected navID - nav radio 12. WAAS/EGNOS receiver"); }
                return cockpit2RadiosIndicatorsNav12NavId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav1DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 1
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav1DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav1DmeId == null)
                { cockpit2RadiosIndicatorsNav1DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav1_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 1"); }
                return cockpit2RadiosIndicatorsNav1DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav2DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 2
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav2DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav2DmeId == null)
                { cockpit2RadiosIndicatorsNav2DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav2_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 2"); }
                return cockpit2RadiosIndicatorsNav2DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav3DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 3
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav3DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav3DmeId == null)
                { cockpit2RadiosIndicatorsNav3DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav3_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 3"); }
                return cockpit2RadiosIndicatorsNav3DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav4DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 4
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav4DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav4DmeId == null)
                { cockpit2RadiosIndicatorsNav4DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav4_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 4"); }
                return cockpit2RadiosIndicatorsNav4DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav5DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 5
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav5DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav5DmeId == null)
                { cockpit2RadiosIndicatorsNav5DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav5_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 5"); }
                return cockpit2RadiosIndicatorsNav5DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav6DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 6
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav6DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav6DmeId == null)
                { cockpit2RadiosIndicatorsNav6DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav6_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 6"); }
                return cockpit2RadiosIndicatorsNav6DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav7DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 7
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav7DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav7DmeId == null)
                { cockpit2RadiosIndicatorsNav7DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav7_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 7"); }
                return cockpit2RadiosIndicatorsNav7DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav8DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 8
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav8DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav8DmeId == null)
                { cockpit2RadiosIndicatorsNav8DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav8_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 8"); }
                return cockpit2RadiosIndicatorsNav8DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav9DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 9
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav9DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav9DmeId == null)
                { cockpit2RadiosIndicatorsNav9DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav9_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 9"); }
                return cockpit2RadiosIndicatorsNav9DmeId;
            }
        }

        private static DataRefElement cockpit2RadiosIndicatorsNav10DmeId = null;
        /// <summary>
        /// current selected dme ID - nav radio 10
        /// </summary>
        public static DataRefElement Cockpit2RadiosIndicatorsNav10DmeId {
            get {
                if (cockpit2RadiosIndicatorsNav10DmeId == null)
                { cockpit2RadiosIndicatorsNav10DmeId = GetDataRefElement("sim/cockpit2/radios/indicators/nav10_dme_id", TypeStart_byte150, UtilConstants.Flag_No, "string", "current selected dme ID - nav radio 10"); }
                return cockpit2RadiosIndicatorsNav10DmeId;
            }
        }


        public static DataRefElement Cockpit2RadiosIndicatorsNav1Type
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_type",
                    Units = "enum",
                    Description = "Type of navaid that's currently tuned in and being received - nav1",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2Type
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_type",
                    Units = "enum",
                    Description = "Type of navaid that's currently tuned in and being received - nav2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavType
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_type",
                    Units = "enum",
                    Description = "Type of navaid that's currently tuned in and being received - any nav receiver",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsOverOuterMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/over_outer_marker",
                    Units = "boolean",
                    Description = "Over the marker, 0 or 1.  This stays on when over the marker",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsOverMiddleMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/over_middle_marker",
                    Units = "boolean",
                    Description = "Over the marker, 0 or 1.  This stays on when over the marker",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsOverInnerMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/over_inner_marker",
                    Units = "boolean",
                    Description = "Over the marker, 0 or 1.  This stays on when over the marker",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsOuterMarkerSignalRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/outer_marker_signal_ratio",
                    Units = "ratio",
                    Description = "Received signal strength of the marker. 0 when not receiving, 1 when at max strength",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMiddleMarkerSignalRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/middle_marker_signal_ratio",
                    Units = "ratio",
                    Description = "Received signal strength of the marker. 0 when not receiving, 1 when at max strength",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsInnerMarkerSignalRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/inner_marker_signal_ratio",
                    Units = "ratio",
                    Description = "Received signal strength of the marker. 0 when not receiving, 1 when at max strength",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsOuterMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/outer_marker_lit",
                    Units = "boolean",
                    Description = "Marker light actually lit, 0 or 1.  This flashes as we go over.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMiddleMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/middle_marker_lit",
                    Units = "boolean",
                    Description = "Marker light actually lit, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsInnerMarkerLit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/inner_marker_lit",
                    Units = "boolean",
                    Description = "Marker light actually lit, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneNav1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_nav1",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneNav2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_nav2",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneAdf1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_adf1",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneAdf2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_adf2",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_dme",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneDme1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_dme1",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsMorseIdToneDme2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/morse_id_tone_dme2",
                    Units = "enum",
                    Description = "The morse-code id output for the tuned radio. Only if the audio is being monitored. 0 for silent. 1 for dot. 2 for dash.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiBearingDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_bearing_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the pilot's HSI-selected navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiBearingDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_bearing_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated bearing to the copilot's HSI-selected navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeBearingDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_bearing_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the pilot's HSI-selected navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeBearingDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_bearing_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated relative bearing to the copilot's HSI-selected navaid",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagFromToPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_from_to_pilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav1, pilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagFromToCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_from_to_copilot",
                    Units = "enum",
                    Description = "Nav-To-From indication, nav1, copilot, 0 is flag, 1 is to, 2 is from.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiHdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_hdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, nav1, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiHdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_hdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI lateral deflection in dots, nav1, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiVdefDotsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_vdef_dots_pilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, nav1, pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiVdefDotsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_vdef_dots_copilot",
                    Units = "dots",
                    Description = "CDI vertical deflection in dots, nav1, copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiHasDmePilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_has_dme_pilot",
                    Units = "boolean",
                    Description = "Is there a DME signal from nav1's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiHasDmeCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_has_dme_copilot",
                    Units = "boolean",
                    Description = "Is there a DME signal from nav1's DME?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeDistanceNmPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_distance_nm_pilot",
                    Units = "nautical_miles",
                    Description = "nav1 DME distance in nautical miles, pilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeDistanceNmCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_distance_nm_copilot",
                    Units = "nautical_miles",
                    Description = "nav1 DME distance in nautical miles. copilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeSpeedKtsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_speed_kts_pilot",
                    Units = "knots",
                    Description = "nav1 DME speed in knots. pilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeSpeedKtsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_speed_kts_copilot",
                    Units = "knots",
                    Description = "nav1 DME speed in knots. copilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeTimeMinPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_time_min_pilot",
                    Units = "minutes",
                    Description = "nav1 DME time in minutes. pilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDmeTimeMinCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_dme_time_min_copilot",
                    Units = "minutes",
                    Description = "nav1 DME time in minutes. copilot HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagGlideslopePilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_glideslope_pilot",
                    Units = "boolean",
                    Description = "Glide slope flag, pilot side HSI - EFIS style flag that shows when glideslope is expected, but not received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagGlideslopeCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_glideslope_copilot",
                    Units = "boolean",
                    Description = "Glide slope flag, copilot side HSI - EFIS style flag that shows when glideslope is expected, but not received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagGlideslopePilotMech
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_glideslope_pilot_mech",
                    Units = "boolean",
                    Description = "Glide slope flag, pilot side HSI - mechanical instrument flag that shows whenever no glideslope signal is received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiFlagGlideslopeCopilotMech
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_flag_glideslope_copilot_mech",
                    Units = "boolean",
                    Description = "Glide slope flag, copilot side HSI - mechanical instrument flag that shows whenever no glideslope signal is received",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDisplayHorizontalPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_display_horizontal_pilot",
                    Units = "boolean",
                    Description = "Is there some kind of horizontal signal on pilot side HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDisplayHorizontalCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_display_horizontal_copilot",
                    Units = "boolean",
                    Description = "Is there some kind of horizontal signal on copilot side HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDisplayVerticalPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_display_vertical_pilot",
                    Units = "boolean",
                    Description = "Is there some kind of vertical signal on pilot side HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiDisplayVerticalCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_display_vertical_copilot",
                    Units = "boolean",
                    Description = "Is there some kind of vertical signal on copilot side HSI",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsTransponderId
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/transponder_id",
                    Units = "bool",
                    Description = "Whether we are squawking ident right now.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsTransponderBrightness
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/transponder_brightness",
                    Units = "ratio",
                    Description = "Transponder light brightness ratio from 0 to 1",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav1RelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav1_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNav2RelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav2_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav2 OBS to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsNavRelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/nav_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav N OBS to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGpsRelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 1 course to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsGps2RelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/gps2_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the GPS 2 course to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the vacuum driven gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the vacuum driven gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_electric_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the electric gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_electric_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to the electric gyro for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to AHARS gyro for the pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsHsiRelativeHeadingAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/hsi_relative_heading_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "This is the relative heading of the nav1 OBS to AHARS for the copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsExecLightPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_exec_light_pilot",
                    Units = "Boolean",
                    Description = "Is the exec FMS light/warning lit for the pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsExecLightCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_exec_light_copilot",
                    Units = "Boolean",
                    Description = "Is the exec FMS light/warning lit for the co-pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsFptaPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_fpta_pilot",
                    Units = "Feet",
                    Description = "FMS Flight Plan Target Altitude",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsVpaPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_vpa_pilot",
                    Units = "Degrees",
                    Description = "FMS Vertical Path Angle",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsVtkPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_vtk_pilot",
                    Units = "Degrees",
                    Description = "FMS Vertical Track Error",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsDistanceToTodPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_distance_to_tod_pilot",
                    Units = "nm",
                    Description = "FMS Distance to Top of Descent",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsTodBeforeIndexPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_tod_before_index_pilot",
                    Units = "index",
                    Description = "Index of waypoint in flightplan before which top of descent is located",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsTodBeforeDistancePilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_tod_before_distance_pilot",
                    Units = "nm",
                    Description = "Distance from top of descent to Nth waypoint (wayoint found via fms_tod_before_index_pilot)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line0",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line1",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line2",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line3",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line4",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line5",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line6",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line7",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line8",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine9
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line9",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine10
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line10",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine11
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line11",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine12
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line12",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine13
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line13",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine14
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line14",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1TextLine15
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_text_line15",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line0",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line1",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line2",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line3",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line4",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line5",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line6",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line7",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line8",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine9
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line9",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine10
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line10",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine11
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line11",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine12
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line12",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine13
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line13",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine14
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line14",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu1StyleLine15
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu1_style_line15",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line0",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line1",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line2",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line3",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line4",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line5",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line6",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line7",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line8",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine9
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line9",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine10
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line10",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine11
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line11",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine12
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line12",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine13
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line13",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine14
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line14",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2TextLine15
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_text_line15",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line0",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line1",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line2",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line3",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line4",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line5",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line6",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line7",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line8",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine9
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line9",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine10
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line10",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine11
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line11",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine12
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line12",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine13
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line13",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine14
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line14",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2RadiosIndicatorsFmsCdu2StyleLine15
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/radios/indicators/fms_cdu2_style_line15",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
    }
}
