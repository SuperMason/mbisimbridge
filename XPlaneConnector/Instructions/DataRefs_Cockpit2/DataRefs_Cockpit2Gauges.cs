﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2GaugesActuatorsBarometerSettingInHgPilot = null;
        /// <summary>
        /// Barometric pressure setting, inches HG. Pilot side.
        /// </summary>
        public static DataRefElement Cockpit2GaugesActuatorsBarometerSettingInHgPilot {
            get {
                if (cockpit2GaugesActuatorsBarometerSettingInHgPilot == null)
                { cockpit2GaugesActuatorsBarometerSettingInHgPilot = GetDataRefElement("sim/cockpit2/gauges/actuators/barometer_setting_in_hg_pilot", TypeStart_float, UtilConstants.Flag_Yes, "inches_hg", "Barometric pressure setting, inches HG. Pilot side."); }
                return cockpit2GaugesActuatorsBarometerSettingInHgPilot;
            }
        }


        public static DataRefElement Cockpit2GaugesActuatorsBarometerSettingInHgCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/barometer_setting_in_hg_copilot",
                    Units = "inches_hg",
                    Description = "Barometric pressure setting, inches HG.  Copilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsBarometerSettingInHgStby
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/barometer_setting_in_hg_stby",
                    Units = "inches_hg",
                    Description = "Barometric pressure setting, inches HG.  Standby instrument.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsRadioAltimeterBugFtPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/radio_altimeter_bug_ft_pilot",
                    Units = "feet",
                    Description = "Radio altitude bug entered into the radio altimeter. Pilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsRadioAltimeterBugFtCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/radio_altimeter_bug_ft_copilot",
                    Units = "feet",
                    Description = "Radio altitude bug entered into the radio altimeter. Copilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsBaroAltimeterBugFtPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/baro_altimeter_bug_ft_pilot",
                    Units = "feet",
                    Description = "Baro altitude bug. Pilot side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsBaroAltimeterBugFtCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/baro_altimeter_bug_ft_copilot",
                    Units = "feet",
                    Description = "Baro altitude bug. Copilot side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsArtificialHorizonAdjustDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/artificial_horizon_adjust_deg_pilot",
                    Units = "degrees",
                    Description = "Artificial horizon pitch-reference adjustment. Pilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsArtificialHorizonAdjustDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/artificial_horizon_adjust_deg_copilot",
                    Units = "degrees",
                    Description = "Artificial horizon pitch-reference adjustment. Copilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesActuatorsAirspeedBugDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/actuators/airspeed_bug_deg",
                    Units = "degrees",
                    Description = "ASI bug location on the dial, in degrees, 0 straight up, positive clockwise.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsSlipDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/slip_deg",
                    Units = "degrees",
                    Description = "Slip indication, in degrees of ball deflection from centered.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsCompassHeadingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/compass_heading_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Indicated heading of the wet compass, in degrees.",
                    
                };
            }
        }

        private static DataRefElement cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot = null;
        /// <summary>
        /// acceleration of airspeed (ASI trend)
        /// </summary>
        public static DataRefElement Cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot {
            get {
                if (cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot == null)
                { cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot = GetDataRefElement("sim/cockpit2/gauges/indicators/airspeed_acceleration_kts_sec_pilot", TypeStart_float, UtilConstants.Flag_Yes, "knots/second", "acceleration of airspeed (ASI trend)"); }
                return cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecPilot;
            }
        }
        
        public static DataRefElement Cockpit2GaugesIndicatorsAirspeedAccelerationKtsSecCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/airspeed_acceleration_kts_sec_copilot",
                    Units = "knots/second",
                    Description = "acceleration of airspeed (ASI trend)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsAirspeedKtsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/airspeed_kts_pilot",
                    Units = "knots",
                    Description = "Indicated airspeed in knots, pilot. Writeable with override_IAS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsAirspeedKtsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/airspeed_kts_copilot",
                    Units = "knots",
                    Description = "Indicated airspeed in knots, copilot. Writeable with override_IAS",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsAirspeedKtsStby
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/airspeed_kts_stby",
                    Units = "knots",
                    Description = "Indicated airspeed in knots, standby instrument. Writeable with override_IAS ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsCalibratedAirspeedKtsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/calibrated_airspeed_kts_pilot",
                    Units = "knots",
                    Description = "Calibrated airspeed in knots, pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsCalibratedAirspeedKtsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/calibrated_airspeed_kts_copilot",
                    Units = "knots",
                    Description = "Calibrated airspeed in knots, copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsCalibratedAirspeedKtsStby
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/calibrated_airspeed_kts_stby",
                    Units = "knots",
                    Description = "Calibrated airspeed in knots, standby instrument ",
                    
                };
            }
        }

        private static DataRefElement cockpit2GaugesIndicatorsAltitudeFtPilot = null;
        public static DataRefElement Cockpit2GaugesIndicatorsAltitudeFtPilot {
            get {
                if (cockpit2GaugesIndicatorsAltitudeFtPilot == null)
                { cockpit2GaugesIndicatorsAltitudeFtPilot = GetDataRefElement("sim/cockpit2/gauges/indicators/altitude_ft_pilot", TypeStart_float, UtilConstants.Flag_No, "feet", "Indicated height, MSL, in feet, primary system, based on pilots barometric pressure input."); }
                return cockpit2GaugesIndicatorsAltitudeFtPilot;
            }
        }

        public static DataRefElement Cockpit2GaugesIndicatorsAltitudeFtCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/altitude_ft_copilot",
                    Units = "feet",
                    Description = "Indicated height, MSL, in feet, primary system, based on co-pilots barometric pressure input.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsAltitudeFtStby
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/altitude_ft_stby",
                    Units = "feet",
                    Description = "Indicated height, MSL, in feet, primary system, based on standby instrument barometric pressure input.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsVviFpmPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/vvi_fpm_pilot",
                    Units = "feet/minute",
                    Description = "Indicated vertical speed in feet per minute, pilot system.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsVviFpmCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/vvi_fpm_copilot",
                    Units = "feet/minute",
                    Description = "Indicated vertical speed in feet per minute, copilot system.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTurnRateRollDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/turn_rate_roll_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated rate-of-turn, in degrees deflection, for newer roll-augmented turn-indicators.  Pilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTrueAirspeedKtsPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/true_airspeed_kts_pilot",
                    Units = "knots",
                    Description = "True airspeed in knots, for pilot pitot/static, calculated by ADC, requires pitot, static, oat sensor and ADC all to work correctly to give correct value",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTrueAirspeedKtsCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/true_airspeed_kts_copilot",
                    Units = "knots",
                    Description = "True airspeed in knots, for copilot pitot/static, calculated by ADC, requires pitot, static, oat sensor and ADC all to work correctly to give correct value",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsMachPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/mach_pilot",
                    Units = "mach",
                    Description = "number",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsMachCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/mach_copilot",
                    Units = "mach",
                    Description = "number",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTurnRateRollDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/turn_rate_roll_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated rate-of-turn, in degrees deflection, for newer roll-augmented turn-indicators.  Copilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTurnRateHeadingDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/turn_rate_heading_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated rate-of-turn, in degrees deflection, for old-style turn-indicators.  Pilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTurnRateHeadingDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/turn_rate_heading_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated rate-of-turn, in degrees deflection, for old-style turn-indicators.  Copilot side.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingAHARSDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_AHARS_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: AHARS.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingAHARSDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_AHARS_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: AHARS.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingElectricDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_electric_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: electric gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingElectricDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_electric_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: electric gyro.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingVacuumDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_vacuum_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: vacuum gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsHeadingVacuumDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/heading_vacuum_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Indicated magnetic heading, in degrees.  Source: vacuum gyro.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: AHARS.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: AHARS.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_electric_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: electric gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_electric_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: electric gyro.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: vacuum gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsPitchVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/pitch_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated pitch, in degrees, positive up.  Source: vacuum gyro.  Side: Copilot",
                    
                };
            }
        }

        private static DataRefElement cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot = null;
        public static DataRefElement Cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot {
            get {
                if (cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot == null)
                { cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot = GetDataRefElement("sim/cockpit2/gauges/indicators/radio_altimeter_height_ft_pilot", TypeStart_float, UtilConstants.Flag_No, "feet", "Radio-altimeter indicated height in feet, pilot-side"); }
                return cockpit2GaugesIndicatorsRadioAltimeterHeightFtPilot;
            }
        }

        public static DataRefElement Cockpit2GaugesIndicatorsRadioAltimeterHeightFtCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/radio_altimeter_height_ft_copilot",
                    Units = "feet",
                    Description = "Radio-altimeter indicated height in feet, copilot-side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRadioAltimeterDhLitPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/radio_altimeter_dh_lit_pilot",
                    Units = "boolean",
                    Description = "Radio-altimeter decision warning light, pilot-side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRadioAltimeterDhLitCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/radio_altimeter_dh_lit_copilot",
                    Units = "boolean",
                    Description = "Radio-altimeter decision warning light, copilot-side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsBaroAltimeterMdaLitPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/baro_altimeter_mda_lit_pilot",
                    Units = "boolean",
                    Description = "Baro-altimeter decision warning light, pilot-side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsBaroAltimeterMdaLitCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/baro_altimeter_mda_lit_copilot",
                    Units = "boolean",
                    Description = "Baro-altimeter decision warning light, copilot-side",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsBaroAltimeterAlertLitPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/baro_altimeter_alert_lit_pilot",
                    Units = "enum",
                    Description = "Baro-altimeter altitude alert light, as per autopilot altitude warn configuration, pilot. 0 = off, 1 = lit, 2 = blinking",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsBaroAltimeterAlertLitCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/baro_altimeter_alert_lit_copilot",
                    Units = "enum",
                    Description = "Baro-altimeter altitude alert light, as per autopilot altitude warn configuration, copilot. 0 = off, 1 = lit, 2 = blinking",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollAHARSDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_AHARS_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.  Source: AHARS.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollAHARSDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_AHARS_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.  Source: AHARS.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollElectricDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_electric_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.  Source: electric gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollElectricDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_electric_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.  Source: electric gyro.  Side: Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollVacuumDegPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_vacuum_deg_pilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.  Source: vacuum gyro.  Side: Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsRollVacuumDegCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/roll_vacuum_deg_copilot",
                    Units = "degrees",
                    Description = "Indicated roll, in degrees, positive right.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsWindHeadingDegMag
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/wind_heading_deg_mag",
                    Units = "degrees_magnetic",
                    Description = "Wind direction currently acting on the plane, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsWindSpeedKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/wind_speed_kts",
                    Units = "knots",
                    Description = "Wind speed currently acting on the plane, in knots true.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsSuction1Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/suction_1_ratio",
                    Units = "ratio",
                    Description = "Vacuum system 1 suction as ratio of max available in plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsSuction2Ratio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/suction_2_ratio",
                    Units = "ratio",
                    Description = "Vacuum system 2 suction as ratio of max available in plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsTotalEnergyFpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/total_energy_fpm",
                    Units = "feet/minute",
                    Description = "Total energy in feet/minute",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsWaterRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/water_ratio",
                    Units = "ratio",
                    Description = "Ratio of releasable water still in the tank, 0..1.",
                    
                };
            }
        }

        private static DataRefElement cockpit2GaugesIndicatorsSideslipDegrees = null;
        /// <summary>
        /// Sideslip in degrees
        /// </summary>
        public static DataRefElement Cockpit2GaugesIndicatorsSideslipDegrees {
            get {
                if (cockpit2GaugesIndicatorsSideslipDegrees == null)
                { cockpit2GaugesIndicatorsSideslipDegrees = GetDataRefElement("sim/cockpit2/gauges/indicators/sideslip_degrees", TypeStart_float, UtilConstants.Flag_No, "degrees", "Sideslip in degrees"); }
                return cockpit2GaugesIndicatorsSideslipDegrees;
            }
        }

        public static DataRefElement Cockpit2GaugesIndicatorsPropSyncDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/prop_sync_degrees",
                    Units = "degrees",
                    Description = "Degrees difference between two prop.  Can be used for prop sync display.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsCGIndicator
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/CG_indicator",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsGroundTrackMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/ground_track_mag_pilot",
                    Units = "degrees",
                    Description = "The ground track of the aircraft in degrees magnetic",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsGroundTrackMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/ground_track_mag_copilot",
                    Units = "degrees",
                    Description = "The ground track of the aircraft in degrees magnetic",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsMaxMachNumberInKias
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/max_mach_number_in_kias",
                    Units = "knots",
                    Description = "For planes with a max mach number, this is the max mach number converted to KIAS for the current plane altitude, etc. - useful for adaptive needles and limit markers on tapes",
                    
                };
            }
        }
        public static DataRefElement Cockpit2GaugesIndicatorsMaxMachNumberOrSpeedKias
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/gauges/indicators/max_mach_number_or_speed_kias",
                    Units = "knots",
                    Description = "For planes with a max mach number, this is the lesser of the max mach number converted to KIAS for the current plane altitude and V-never-exceed.",
                    
                };
            }
        }
    }
}
