﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2AnnunciatorsMasterCaution
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/master_caution",
                    Units = "boolean",
                    Description = "This goes off any time there is a major problem with the bird: like it is on fire or something    // array of annunciators... kept as array for UDP IO",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsMasterWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/master_warning",
                    Units = "boolean",
                    Description = "This goes off whenever there is some more moderate problem with the bird... maybe lo fuel pressure ot qty",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsMasterAccept
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/master_accept",
                    Units = "boolean",
                    Description = "Hit this button to CLEAR the master caution and warning: it says that you hear the buzzer and to shut it up already",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotDisconnect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_disconnect",
                    Units = "boolean",
                    Description = "This goes off for a few moments when the autopilot disconnects.. you wanna KNOW if otto just quit on you!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsLowVacuum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/low_vacuum",
                    Units = "boolean",
                    Description = "The vacuum pressure, which drives the instruments in the old prop planes, is low: the artificial horizon may loose it is orientation!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsLowVoltage
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/low_voltage",
                    Units = "boolean",
                    Description = "The voltage os too high or low on one of the electrical buses",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFuelQuantity
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/fuel_quantity",
                    Units = "boolean",
                    Description = "Fuel qty. no points for guessing if it is too high or too low to trigger this one",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsHydraulicPressure
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/hydraulic_pressure",
                    Units = "boolean",
                    Description = "Hydraulic pressure. should indicate if press is lo or hi",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsSpeedbrake
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/speedbrake",
                    Units = "boolean",
                    Description = "Speedbrake deployed: good to know so you do not forget it..",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGPWS
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/GPWS",
                    Units = "boolean",
                    Description = "Ground proximity warning system: PULL UP! PULL UP! my lancair does this during my typical red-baron approaches!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsIce
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/ice",
                    Units = "boolean",
                    Description = "Ice detected on the plane.. most people agree this is NOT a good thing.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsLowRotor
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/low_rotor",
                    Units = "boolean",
                    Description = "Rotor rpm on the helo has dropped below normal operating: power availability will be reduced!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsHighRotor
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/high_rotor",
                    Units = "boolean",
                    Description = "Rotor rpm on the helo has exceeded normal operating: power availability will be reduced, and god knows if the blades will stay ON!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsPitotHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/pitot_heat",
                    Units = "boolean",
                    Description = "Pitot heat is OFF (YUP! OFF!) turn it on to heat up the pitot tube, which measures air pressure to give you air speed.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsTransonic
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/transonic",
                    Units = "boolean",
                    Description = "You are about to break the speed of sound when accelerating, or drop below the speed of sound if decelerating! the handling of the plane will change!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsSlats
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/slats",
                    Units = "boolean",
                    Description = "slats deployed",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFlightDirector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/flight_director",
                    Units = "boolean",
                    Description = "flight director failure",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot",
                    Units = "boolean",
                    Description = "autopilot failure",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsYawDamper
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/yaw_damper",
                    Units = "boolean",
                    Description = "yaw damper failure",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFuelPressureLow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/fuel_pressure_low",
                    Units = "boolean",
                    Description = "fuel pressure low - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsOilPressureLow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/oil_pressure_low",
                    Units = "boolean",
                    Description = "fuel pressure low - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsOilTemperatureHigh
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/oil_temperature_high",
                    Units = "boolean",
                    Description = "oil temperature high - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGeneratorOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/generator_off",
                    Units = "boolean",
                    Description = "generator off - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsChipDetected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/chip_detected",
                    Units = "boolean",
                    Description = "chip detected - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsEngineFires
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/engine_fires",
                    Units = "boolean",
                    Description = "engine fire - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsIgniterOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/igniter_on",
                    Units = "boolean",
                    Description = "igniter on - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsReverserOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/reverser_on",
                    Units = "boolean",
                    Description = "reverser on - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsBeta
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/beta",
                    Units = "boolean",
                    Description = "propeller in beta mode - per engine, double as NTS test light in fixed turboprops",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsBurnerOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/burner_on",
                    Units = "boolean",
                    Description = "burner on - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsInverterOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/inverter_off",
                    Units = "boolean",
                    Description = "inverter off - per 2 inverters",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsN1Low
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/N1_low",
                    Units = "boolean",
                    Description = "N1 of engine is too low for AC - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsN1High
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/N1_high",
                    Units = "boolean",
                    Description = "N1 too high - per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsReverserNotReady
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/reverser_not_ready",
                    Units = "boolean",
                    Description = "reversers not ready",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsIceVaneExtend
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/ice_vane_extend",
                    Units = "boolean",
                    Description = "ice vain extended (per engine)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsIceVaneFail
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/ice_vane_fail",
                    Units = "boolean",
                    Description = "ice vain failed (per engine)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsBleedAirOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/bleed_air_off",
                    Units = "boolean",
                    Description = "bleed air off (per engine)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsBleedAirFail
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/bleed_air_fail",
                    Units = "boolean",
                    Description = "bleed air failed (per engine)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutoFeatherArm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/auto_feather_arm",
                    Units = "boolean",
                    Description = "auto feather armed (per engine)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFuelTransfer
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/fuel_transfer",
                    Units = "boolean",
                    Description = "fuel transfer on (per tank)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsHvac
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/hvac",
                    Units = "boolean",
                    Description = "duct overheated",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsBatteryChargeHi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/battery_charge_hi",
                    Units = "boolean",
                    Description = "battery is charging too rapidly - may overheat",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsCabinAltitude12500
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/cabin_altitude_12500",
                    Units = "boolean",
                    Description = "cabin altitude at or above 12500",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotTrimFail
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_trim_fail",
                    Units = "boolean",
                    Description = "autopilot trim failure",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsElectricTrimOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/electric_trim_off",
                    Units = "boolean",
                    Description = "electric trim is off",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsCrossfeedOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/crossfeed_on",
                    Units = "boolean",
                    Description = "crossfeed on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsLandingTaxiLite
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/landing_taxi_lite",
                    Units = "boolean",
                    Description = "landing or taxiway light on but gear up",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsCabinDoorOpen
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/cabin_door_open",
                    Units = "boolean",
                    Description = "cabin door is open",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsExternalPowerOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/external_power_on",
                    Units = "boolean",
                    Description = "external power is on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsPassengerOxyOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/passenger_oxy_on",
                    Units = "boolean",
                    Description = "passenger oxygen on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGearUnsafe
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/gear_unsafe",
                    Units = "boolean",
                    Description = "gear is unsafe",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotTrimDown
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_trim_down",
                    Units = "boolean",
                    Description = "autopilot trimming down",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotTrimUp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_trim_up",
                    Units = "boolean",
                    Description = "autopilot trimming up",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotBankLimit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_bank_limit",
                    Units = "boolean",
                    Description = "autopilot bank limit is turned ON, autopilot will keep bank below 12.5 degrees of bank",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAutopilotSoftRide
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/autopilot_soft_ride",
                    Units = "boolean",
                    Description = "autopilot soft ride is on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsNoInverters
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/no_inverters",
                    Units = "boolean",
                    Description = "no inverters are on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFuelPressure
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/fuel_pressure",
                    Units = "bitfield",
                    Description = "Fuel pressure is lo, or maybe hi, for this engine    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsOilPressure
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/oil_pressure",
                    Units = "bitfield",
                    Description = "Oil pressure is lo, or maybe hi, for this engine    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsOilTemperature
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/oil_temperature",
                    Units = "bitfield",
                    Description = "Oil temperature hi for this engine    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGenerator
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/generator",
                    Units = "bitfield",
                    Description = "The generator has failed! the plane cannot charge up. this may happen at lo rpm and go away as the engine revs. happens with my plane    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsChipDetect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/chip_detect",
                    Units = "bitfield",
                    Description = "We detected chips of metal in the engine somewhere. most people agree this is not good.    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsEngineFire
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/engine_fire",
                    Units = "bitfield",
                    Description = "The engine is on FIRE! this adds to excitement    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsReverserDeployed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/reverser_deployed",
                    Units = "bitfield",
                    Description = "Thrust-reverse deployed!    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsPropBeta
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/prop_beta",
                    Units = "bitfield",
                    Description = "Propeller in beta mode    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsAfterburner
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/afterburner",
                    Units = "bitfield",
                    Description = "Afterburners on!    // x8",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsInverter
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/inverter",
                    Units = "bitfield",
                    Description = "The inverter has failed! the plane cannot convert ac from the generators to dc for the instruments.     // x2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsStallWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/stall_warning",
                    Units = "boolean",
                    Description = "Stall warning going off, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsStallWarningRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/stall_warning_ratio",
                    Units = "0..1",
                    Description = "For aircraft with variable stall warnings, 0 = no stall, 1 = full stall",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGearWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/gear_warning",
                    Units = "boolean",
                    Description = "Gear warning going off, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsGearWarningAural
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/gear_warning_aural",
                    Units = "boolean",
                    Description = "Gear aural warning going off, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsNoSmoking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/no_smoking",
                    Units = "boolean",
                    Description = "No smoking alert on, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsFastenSeatbelt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/fasten_seatbelt",
                    Units = "boolean",
                    Description = "Seatbelt sign on, yes or no.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsWindshearWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/windshear_warning",
                    Units = "boolean",
                    Description = "True if there's a windshear warning active",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsPluginMasterWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/plugin_master_warning",
                    Units = "boolean",
                    Description = "Writeable without override. Write 1 to trigger master warning.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AnnunciatorsPluginMasterCaution
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/annunciators/plugin_master_caution",
                    Units = "boolean",
                    Description = "Writeable without override. Write 1 to trigger master caution.",
                    
                };
            }
        }
    }
}
