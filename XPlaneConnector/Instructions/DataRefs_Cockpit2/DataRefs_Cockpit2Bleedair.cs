﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Engine bleed air shut off valve, close or open
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsEngineBleedSov
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/engine_bleed_sov", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "Engine bleed air shut off valve, close or open"); } }
        /// <summary>
        /// APU bleed air valve, close or open. APU must be running at 100%N1 to provide bleed air
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsApuBleed
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/apu_bleed", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "APU bleed air valve, close or open. APU must be running at 100%N1 to provide bleed air"); } }
        /// <summary>
        /// GPU bleed air valve, close or open. A GPU is supposed to be always available.
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsGpuBleed
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/gpu_bleed", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "GPU bleed air valve, close or open. A GPU is supposed to be always available."); } }
        /// <summary>
        /// Isolation Valve for left duct, close or open. This separates all engines on the left side of the plane, the left wing, and the left pack from the rest of the system
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsIsolValveLeft
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/isol_valve_left", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Isolation Valve for left duct, close or open. This separates all engines on the left side of the plane, the left wing, and the left pack from the rest of the system"); } }
        /// <summary>
        /// Isolation Valve for right duct, close or open. This separates all engines on the right side of the plane, the right wing, and the right pack from the rest of the system
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsIsolValveRight
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/isol_valve_right", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Isolation Valve for right duct, close or open. This separates all engines on the right side of the plane, the right wing, and the right pack from the rest of the system"); } }
        /// <summary>
        /// Left pressurization pack, off or on. The left pack is supplied from the left side of the plane or through the left isolation valve and only available for airplanes made for 11.35 or newer
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsPackLeft
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/pack_left", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Left pressurization pack, off or on. The left pack is supplied from the left side of the plane or through the left isolation valve and only available for airplanes made for 11.35 or newer"); } }
        /// <summary>
        /// Center pressurization pack, off or on. The center pack is supplied from center duct, which can be supplied from a center engine, APU, GPU, or, via the isolation valves, the left and/or right ducts. This pack is the only pack available for airplanes made for X-Plane 11.33 or older
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsPackCenter
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/pack_center", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Center pressurization pack, off or on. The center pack is supplied from center duct, which can be supplied from a center engine, APU, GPU, or, via the isolation valves, the left and/or right ducts. This pack is the only pack available for airplanes made for X-Plane 11.33 or older"); } }
        /// <summary>
        /// Right pressurization pack, off or on. The right pack is supplied from the right side of the plane or through the right isolation valve and only available for airplanes made for 11.35 or newer
        /// </summary>
        public static DataRefElement Cockpit2BleedairActuatorsPackRight
        { get { return GetDataRefElement("sim/cockpit2/bleedair/actuators/pack_right", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Right pressurization pack, off or on. The right pack is supplied from the right side of the plane or through the right isolation valve and only available for airplanes made for 11.35 or newer"); } }
        /// <summary>
        /// Bleed air available in the left duct, which can come from left engines or through the left isolation valve.
        /// </summary>
        public static DataRefElement Cockpit2BleedairIndicatorsBleedAvailableLeft
        { get { return GetDataRefElement("sim/cockpit2/bleedair/indicators/bleed_available_left", TypeStart_float, UtilConstants.Flag_No, "ratio", "Bleed air available in the left duct, which can come from left engines or through the left isolation valve."); } }
        /// <summary>
        /// Bleed air available in the center duct, which can come from a center engine, APU, GPU, or, via the isolation valves, the left and/or right ducts.
        /// </summary>
        public static DataRefElement Cockpit2BleedairIndicatorsBleedAvailableCenter
        { get { return GetDataRefElement("sim/cockpit2/bleedair/indicators/bleed_available_center", TypeStart_float, UtilConstants.Flag_No, "ratio", "Bleed air available in the center duct, which can come from a center engine, APU, GPU, or, via the isolation valves, the left and/or right ducts."); } }
        /// <summary>
        /// Bleed air available in the right duct, which can come from right engines or through the right isolation valve.
        /// </summary>
        public static DataRefElement Cockpit2BleedairIndicatorsBleedAvailableRight
        { get { return GetDataRefElement("sim/cockpit2/bleedair/indicators/bleed_available_right", TypeStart_float, UtilConstants.Flag_No, "ratio", "Bleed air available in the right duct, which can come from right engines or through the right isolation valve."); } }
        /// <summary>
        /// Bleed air being sapped from the engine, stealing efficiency from the compressor. Writeable only with override_pressurization set
        /// </summary>
        public static DataRefElement Cockpit2BleedairIndicatorsEngineLossFromBleedAirRatio
        { get { return GetDataRefElement("sim/cockpit2/bleedair/indicators/engine_loss_from_bleed_air_ratio", TypeStart_float8, UtilConstants.Flag_Yes, "ratio", "Bleed air being sapped from the engine, stealing efficiency from the compressor. Writeable only with override_pressurization set"); } }
        /// <summary>
        /// Bleed air being sapped from the APU, stealing efficiency from the compressor. Writeable only with override_pressurization set
        /// </summary>
        public static DataRefElement Cockpit2BleedairIndicatorsAPULossFromBleedAirRatio
        { get { return GetDataRefElement("sim/cockpit2/bleedair/indicators/APU_loss_from_bleed_air_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Bleed air being sapped from the APU, stealing efficiency from the compressor. Writeable only with override_pressurization set"); } }
    }
}