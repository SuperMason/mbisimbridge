﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2OxygenActuatorsO2ValveOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/actuators/o2_valve_on",
                    Units = "boolean",
                    Description = "crew oxygen valve open",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenActuatorsDemandFlowSetting
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/actuators/demand_flow_setting",
                    Units = "enum",
                    Description = "0 = off, 1 = unregulated, 2 = now/night, 3 = day/delay 5, 4 = day/delay 10, 5 = fat/face mask 1, 6 = fat/face mask 2, 7 = fat/face mask 3, 8 = fat/face mask 4",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenActuatorsNumPluggedInO2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/actuators/num_plugged_in_o2",
                    Units = "int",
                    Description = "number of people plugged into the bottle O2 system - note that airliners usually have a chemical oxygen system, so this is for the two pilots, while the PAX a served by a chemical oxygenator",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenIndicatorsO2BottleRemLiter
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/indicators/o2_bottle_rem_liter",
                    Units = "liter",
                    Description = "liters of O2 remaining in the crew bottle. Writeable with override_oxygen_system",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenIndicatorsO2BottlePressurePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/indicators/o2_bottle_pressure_psi",
                    Units = "lb/in2",
                    Description = "pressure in the O2 bottle. Writeable with override_oxygen_system",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenIndicatorsPilotFeltAltitudeFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/indicators/pilot_felt_altitude_ft",
                    Units = "feet",
                    Description = "pressure altitude felt by the pilot's body. Will be lower than cabin pressure altitude when on oxygen. This is what triggers the hypoxia black-out effect. Writeable with override_oxygen_system",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenIndicatorsPassOxygenatorWorking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/indicators/pass_oxygenator_working",
                    Units = "boolean",
                    Description = "passenger chemical oxygen generator on and supplying passengers' O2 masks. Note that the dataref to drop the masks manually is sim/operation/failures/rel_pass_o2_on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2OxygenIndicatorsPassOxygenatorMinRem
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/oxygen/indicators/pass_oxygenator_min_rem",
                    Units = "minutes",
                    Description = "the chemical oxygen generator can still supply the passenger oxygen masks with O2 for this many minutes",
                    
                };
            }
        }
    }
}
