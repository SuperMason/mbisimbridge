﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2AutopilotAutopilot2Avail
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot2_avail",
                    Units = "boolean",
                    Description = "Autopilot sources are hardwired to respective side, and two autopilots are available",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotMasterFlightDirector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/master_flight_director",
                    Units = "enum",
                    Description = "If aputopilot2_avail is true, this selects the master FD: 0 = pilot, 1 = copilot, 2 = both indipendent",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilotSource
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot_source",
                    Units = "enum",
                    Description = "Autopilot source system, pilots (0) or copilots (1). With two indipendent autopilots, selects the one that is flying.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutothrottleEnabled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autothrottle_enabled",
                    Units = "enum",
                    Description = "Auto-throttle: 0=servos declutched (arm, hold), 1=airspeed hold, 2=N1 target hold, 3=retard, 4=reserved for future use",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutothrottleOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autothrottle_on",
                    Units = "boolean",
                    Description = "Auto-throttle really working?  Takes into account failures, esys, etc.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotElectricTrimOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/electric_trim_on",
                    Units = "boolean",
                    Description = "Electric pitch trim on - if off, autopilot flies with pitch servo only, and can't trim!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotPitchMistrim
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/pitch_mistrim",
                    Units = "enum",
                    Description = "Manual trim required: 0=None, -1=Pitch down, 1=Pitch up",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotOttoFailWarn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/otto_fail_warn",
                    Units = "boolean",
                    Description = "fail warn all lights on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotOttoReady
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/otto_ready",
                    Units = "boolean",
                    Description = "whether the autopilot is ready, but not actually turned on. This is true when the autopilot has powered, has no failures, and is ready to be engaged but not engaged yet.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_mode",
                    Units = "enum",
                    Description = "Autopilot heading mode.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_mode",
                    Units = "enum",
                    Description = "Autopilot altitude mode.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotBankAngleMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/bank_angle_mode",
                    Units = "enum",
                    Description = "Maximum bank angle mode, 0->6. Higher number is steeper allowable bank.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirectorMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director_mode",
                    Units = "enum",
                    Description = "Flight director mode, 0 is off, 1 is on, 2 is on with autopilot servos.  Good for the FD swich.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirector2Mode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director2_mode",
                    Units = "enum",
                    Description = "Flight director 2 mode, 0 is off, 1 is on, 2 is on with autopilot servos.  Good for the FD swich.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirector3Mode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director3_mode",
                    Units = "enum",
                    Description = "Autopilot 3 mode, 0 is off, 1 has no meaning, 2 is on with autopilot servos.  Good for the AP swich.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilotOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot_on",
                    Units = "boolean",
                    Description = "Is the autopilot really on? Takes into account electrical system, failures, etc.;",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilot2On
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot2_on",
                    Units = "boolean",
                    Description = "Is the autopilot 2 really on? Takes into account electrical system, failures, etc.;",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilot3On
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot3_on",
                    Units = "boolean",
                    Description = "Is the autopilot 3 really on? Takes into account electrical system, failures, etc.;",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilotOnOrCws
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot_on_or_cws",
                    Units = "boolean",
                    Description = "Is the autopilot really on? Like servos_on, but stays on even the servos are temporarily off for CWS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilot2OnOrCws
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot2_on_or_cws",
                    Units = "boolean",
                    Description = "Is the autopilot 2 really on? Like servos_on, but stays on even the servos are temporarily off for CWS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAutopilot3OnOrCws
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/autopilot3_on_or_cws",
                    Units = "boolean",
                    Description = "Is the autopilot 2 really on? Like servos_on, but stays on even the servos are temporarily off for CWS.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotServosOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/servos_on",
                    Units = "boolean",
                    Description = "Are the servos on?  Takes into account FD mode and control-wheel-steering, failures, etc.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotServos2On
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/servos2_on",
                    Units = "boolean",
                    Description = "Are the servos on?  Takes into account FD2 mode and control-wheel-steering, failures, etc.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotServos3On
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/servos3_on",
                    Units = "boolean",
                    Description = "Are the servos on?  Takes into account AP3 mode and control-wheel-steering, failures, etc.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAirspeedIsMach
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/airspeed_is_mach",
                    Units = "boolean",
                    Description = "Autopilot airspeed is Mach number rather than knots.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltVviIsShowingVvi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/alt_vvi_is_showing_vvi",
                    Units = "boolean",
                    Description = "Is the combined alt/vvi selector showing VVI?",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotVnavArmed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/vnav_armed",
                    Units = "boolean",
                    Description = "Vnav is armed, 0 or 1 - this is different from the \"FMS\" button - it is used ONLY for a physical G1000 connected by ethernet.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeHoldArmed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_hold_armed",
                    Units = "boolean",
                    Description = "Altitude is armed, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHnavArmed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/hnav_armed",
                    Units = "boolean",
                    Description = "Localizer is armed, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotGlideslopeArmed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/glideslope_armed",
                    Units = "boolean",
                    Description = "Glideslope is armed, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotBackcourseOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/backcourse_on",
                    Units = "boolean",
                    Description = "Back course is armed, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAirspeedDialKtsMach
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/airspeed_dial_kts_mach",
                    Units = "knots/mach",
                    Description = "Airspeed hold value, knots or Mach depending on km_is_mach.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAirspeedDialKts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/airspeed_dial_kts",
                    Units = "Knots",
                    Description = "Airspeed hold bug, always in knots, even if the AP is in mach-hold mode.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingDialDegMagPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_dial_deg_mag_pilot",
                    Units = "degrees_magnetic",
                    Description = "Heading hold commanded, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingDialDegMagCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_dial_deg_mag_copilot",
                    Units = "degrees_magnetic",
                    Description = "Heading hold commanded, in degrees magnetic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingIsGpss
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_is_gpss",
                    Units = "boolean",
                    Description = "0 = Heading bug supplies HDG to autopilot, 1 = GPSS supplies heading to autopilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTrkFpa
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/trk_fpa",
                    Units = "boolean",
                    Description = "0 = HDG/VS, 1 = TRK/FPA",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotVviDialFpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/vvi_dial_fpm",
                    Units = "feet/minute",
                    Description = "VVI commanded in FPM.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFpa
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/fpa",
                    Units = "degrees",
                    Description = "FPA commanded in degrees.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeDialFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_dial_ft",
                    Units = "feet",
                    Description = "VVI commanded in FPM.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeHoldFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_hold_ft",
                    Units = "feet",
                    Description = "Altitude hold commanded in feet indicated.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeVnavFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_vnav_ft",
                    Units = "feet",
                    Description = "Target altitude hold in VNAV mode.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotBarometerSettingInHgAltPreselector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/barometer_setting_in_hg_alt_preselector",
                    Units = "inHg",
                    Description = "baro setting of the altitude preselector",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeReadoutPreselector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_readout_preselector",
                    Units = "feet",
                    Description = "current barometric altitude sensed by the altitude preselector",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotClimbAdjust
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/climb_adjust",
                    Units = "knots",
                    Description = "climb mode adjust base speed knots (by default 150KIAS below 10.000ft)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotDesAdjust
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/des_adjust",
                    Units = "feet/minute",
                    Description = "descend mode adjust target vertical speed (by default -1500ft/min)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotSyncHoldPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/sync_hold_pitch_deg",
                    Units = "degrees",
                    Description = "Pitch-hold commanded in degrees up.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotSyncHoldRollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/sync_hold_roll_deg",
                    Units = "degrees",
                    Description = "Roll-hold commanded in degrees right.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotSetRollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/set_roll_deg",
                    Units = "degrees",
                    Description = "Roll-hold command in degrees right, turn knob that won't turn with other modes",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTurnRateDegSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/turn_rate_deg_sec",
                    Units = "deg/sec",
                    Description = "Turn rate commanded in degrees per second, positive right",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirectorPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director_pitch_deg",
                    Units = "degrees",
                    Description = "Flight director pitch deflection in degrees, pos up.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirectorRollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director_roll_deg",
                    Units = "degrees",
                    Description = "Flight director roll deflection in degrees, pos right.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirector2PitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director2_pitch_deg",
                    Units = "degrees",
                    Description = "Flight director 2 pitch deflection in degrees, pos up.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlightDirector2RollDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flight_director2_roll_deg",
                    Units = "degrees",
                    Description = "Flight director 2 roll deflection in degrees, pos right.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTOGAPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/TOGA_pitch_deg",
                    Units = "degrees",
                    Description = "Nose-up pitch to hold when TOGA operations are in effect.  Set this back when your aircraft unloads!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotRollStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/roll_status",
                    Units = "enum",
                    Description = "Autopilot Roll-Hold Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotRateStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/rate_status",
                    Units = "enum",
                    Description = "Autopilot turn rate hold status: 0=off, 2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_status",
                    Units = "enum",
                    Description = "Autopilot Heading Select Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotHeadingHoldStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/heading_hold_status",
                    Units = "enum",
                    Description = "Autopilot Heading Hold Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTrackStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/track_status",
                    Units = "enum",
                    Description = "Autopilot Track Select Status: 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotNavStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/nav_status",
                    Units = "enum",
                    Description = "Autopilot Nav Status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotGpssStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/gpss_status",
                    Units = "enum",
                    Description = "Autopilot GPSS Status. 0=off, 2=active",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotRolloutStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/rollout_status",
                    Units = "enum",
                    Description = "Autopilot Rollout Status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFlareStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/flare_status",
                    Units = "enum",
                    Description = "Autopilot Flare Status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotBackcourseStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/backcourse_status",
                    Units = "enum",
                    Description = "Autopilot Back-course Status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTOGALateralStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/TOGA_lateral_status",
                    Units = "enum",
                    Description = "Autopilot Lateral TOGA mode: 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotPitchStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/pitch_status",
                    Units = "enum",
                    Description = "Autopilot Pitch-Hold Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotVviStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/vvi_status",
                    Units = "enum",
                    Description = "Autopilot VVI Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFpaStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/fpa_status",
                    Units = "enum",
                    Description = "Autopilot FPA Status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotSpeedStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/speed_status",
                    Units = "enum",
                    Description = "Autopilot Speed-hold (via pitch) status. 0=off,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotAltitudeHoldStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/altitude_hold_status",
                    Units = "enum",
                    Description = "Autopilot Altitude hold status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotGlideslopeStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/glideslope_status",
                    Units = "enum",
                    Description = "Autopilot glideslope status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotVnavStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/vnav_status",
                    Units = "enum",
                    Description = "Autopilot VNAV status. 0=off,1=armed,2=captured - for a physical hardware g1000 connected by Ethernet only!!!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotFmsVnav
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/fms_vnav",
                    Units = "boolean",
                    Description = "Enables the FMS to take over vertical control of the autopilot.   This matches the command \"sim/autopilot/FMS\".\"",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotTOGAStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/TOGA_status",
                    Units = "enum",
                    Description = "Autopilot TOGA vertical (go-around) status. 0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotApproachStatus
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/approach_status",
                    Units = "enum",
                    Description = "Autopilot approach status.  0=off,1=armed,2=captured",
                    
                };
            }
        }
        public static DataRefElement Cockpit2AutopilotDeadReckoning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/autopilot/dead_reckoning",
                    Units = "boolean",
                    Description = "Whether the autopilot is using dead-reckoning to fly over a VOR station",
                    
                };
            }
        }
    }
}
