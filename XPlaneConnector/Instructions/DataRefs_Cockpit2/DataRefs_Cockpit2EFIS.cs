﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2EFISMapMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/map_mode",
                    Units = "enum",
                    Description = "Map mode. 0=approach, 1=vor,2=map,3=nav,4=plan",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISMapModeIsHSI
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/map_mode_is_HSI",
                    Units = "boolean",
                    Description = "Map is in HSI mode, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISMapRange
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/map_range",
                    Units = "enum",
                    Description = "Map range, 1->6, where 6 is the longest range.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISArgusMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/argus_mode",
                    Units = "enum",
                    Description = "Argus mode, 7=departure,8=enroute,9=approach,10=radar_on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEcamMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/ecam_mode",
                    Units = "enum",
                    Description = "ECAM mode, 0=engine,1=fuel,2=controls,3=hydraulics,4=failures",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISWeatherOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_weather_on",
                    Units = "boolean",
                    Description = "On the moving map, show the weather or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISTcasOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_tcas_on",
                    Units = "boolean",
                    Description = "On the moving map, show the TCAS or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISAirportOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_airport_on",
                    Units = "boolean",
                    Description = "On the moving map, show the airports or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISFixOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_fix_on",
                    Units = "boolean",
                    Description = "On the moving map, show the fixes or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISVorOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_vor_on",
                    Units = "boolean",
                    Description = "On the moving map, show the VORs or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISNdbOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_ndb_on",
                    Units = "boolean",
                    Description = "On the moving map, show the NDBs or not.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFIS1SelectionPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_1_selection_pilot",
                    Units = "enum",
                    Description = "EFIS waypoint 1 is showing: 0=ADF1, 1=OFF, or 2=VOR1 -- Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFIS1SelectionCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_1_selection_copilot",
                    Units = "enum",
                    Description = "EFIS waypoint 2 is showing: 0=ADF1, 1=OFF, or 2=VOR1 -- Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFIS2SelectionPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_2_selection_pilot",
                    Units = "enum",
                    Description = "EFIS waypoint 1 is showing: 0=ADF2, 1=OFF, or 2=VOR2 -- Pilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFIS2SelectionCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_2_selection_copilot",
                    Units = "enum",
                    Description = "EFIS waypoint 2 is showing: 0=ADF2, 1=OFF, or 2=VOR2 -- Copilot",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EFISEFISPage
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/EFIS/EFIS_page",
                    Units = "boolean",
                    Description = "An array of EFIS page switches for selecting which EFIS page is visible.",
                    
                };
            }
        }
    }
}
