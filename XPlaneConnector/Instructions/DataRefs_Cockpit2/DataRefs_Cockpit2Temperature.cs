﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2TemperatureOutsideAirTempDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_temp_deg",
                    Units = "degrees",
                    Description = "outside air temperature, pilot selects units",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirTempDegc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_temp_degc",
                    Units = "degreesC",
                    Description = "outside air temperature, celsius",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirTempDegf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_temp_degf",
                    Units = "degreesF",
                    Description = "outside air temperature, fahrenheit",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirLETempDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_LE_temp_deg",
                    Units = "degrees",
                    Description = "outside air temperature with leading edge, pilot selects units",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirLETempDegc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_LE_temp_degc",
                    Units = "degreesC",
                    Description = "outside air temperature with leading edge, celsius",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirLETempDegf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_LE_temp_degf",
                    Units = "degreesF",
                    Description = "outside air temperature with leading edge, fahrenheit",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TemperatureOutsideAirTempIsMetric
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/temperature/outside_air_temp_is_metric",
                    Units = "boolean",
                    Description = "1 if thermo is metric, 0 if fahrenheit.",
                    
                };
            }
        }
    }
}
