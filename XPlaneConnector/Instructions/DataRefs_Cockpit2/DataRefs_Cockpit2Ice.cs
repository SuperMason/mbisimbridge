﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2IceIceAllOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_all_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceInletHeatOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_inlet_heat_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - inlet heat. This switch turns on de-icing heat in the engine air inlet keep ice from choking your engine. Engine 1 only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIcePropHeatOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_prop_heat_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - prop heat. This switch turns on de-icing of the propeller(s) to keep ice from building up on your prop. Engine 1 only",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceInletHeatOnPerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_inlet_heat_on_per_engine",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - inlet heat. This switch turns on de-icing heat in the engine air inlet keep ice from choking your engine.  Per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIcePropHeatOnPerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_prop_heat_on_per_engine",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - prop heat. This switch turns on de-icing of the propeller(s) to keep ice from building up on your prop.  Per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceWindowHeatOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_window_heat_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - windshield heat. This switch turns on windshield de-icing to keep ice from blocking your vision.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIcePitotHeatOnPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_pitot_heat_on_pilot",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - pitot tube heat. This switch turns on de-icing heat in the pitot tube. If it freezes up your airspeed indicator and altimeter stop working. Airspeed and altitude related autopilot functions are also affected.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIcePitotHeatOnCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_pitot_heat_on_copilot",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  Pitot tube heat, co-pilot. This switch turns on de-icing heat in the co-pilot side pitot tube. If it freezes up your airspeed indicator and altimeter stop working. Airspeed and altitude related autopilot functions are also affected.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceAOAHeatOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_AOA_heat_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  AOA sensor heat. This switch turns on de-icing heat for the AOA sensor. If it freezes up your AOA indicator stops working. AOA sensor failure will also affect the aircraft's artificial stability system if it has one.  Pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceAOAHeatOnCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_AOA_heat_on_copilot",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  AOA sensor heat. This switch turns on de-icing heat for the AOA sensor. If it freezes up your AOA indicator stops working. AOA sensor failure will also affect the aircraft's artificial stability system if it has one.  Copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceStaticHeatOnPilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_static_heat_on_pilot",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  Static port heat. This switch turns on de-icing heat for the static sensor. If it freezes up your altimeter, vvi and airspeed indicator stop working. Pilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceStaticHeatOnCopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_static_heat_on_copilot",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  Static port heat. This switch turns on de-icing heat for the static sensor. If it freezes up your altimeter, vvi and airspeed indicator stop working. Copilot.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfceHeatOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surfce_heat_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - electric heat. This switch engages electrically heated leading edges. (All wings)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfceHeatLeftOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surfce_heat_left_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - electric heat. This switch engages electrically heated leading edges. (Left wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfceHeatRightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surfce_heat_right_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - electric heat. This switch engages electrically heated leading edges. (Right wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceBootOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_boot_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - pneumatic. This switch inflates flexible bladders on the wing leading edges to pop off accumulated ice. (All wings)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceBootLeftOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_boot_left_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - pneumatic. This switch inflates flexible bladders on the wing leading edges to pop off accumulated ice. (Left wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceBootRightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_boot_right_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - pneumatic. This switch inflates flexible bladders on the wing leading edges to pop off accumulated ice. (Right wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceHotBleedAirOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_hot_bleed_air_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - bleed air. This switch directs warm air from the engines into the wing leading edges to keep them free of ice. (All wings)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceHotBleedAirLeftOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_hot_bleed_air_left_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - bleed air. This switch directs warm air from the engines into the wing leading edges to keep them free of ice. (Left wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceHotBleedAirRightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_hot_bleed_air_right_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - bleed air. This switch directs warm air from the engines into the wing leading edges to keep them free of ice. (Right wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceTksOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_tks_on",
                    Units = "enum",
                    Description = "De-ice switch, 0, 1, or 2.  De-ice - TKS fluid. This switch activates the pump for the weeping wing, dissipating TKS fluid on the leading edges to keep them free of ice. 0 = Off, 1 = Norm, 2 = High. (All wings)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceTksLeftOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_tks_left_on",
                    Units = "enum",
                    Description = "De-ice switch, 0, 1, or 2.  De-ice - TKS fluid. This switch activates the pump for the weeping wing, dissipating TKS fluid on the leading edges to keep them free of ice. 0 = Off, 1 = Norm, 2 = High. (Left wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceSurfaceTksRightOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_surface_tks_right_on",
                    Units = "enum",
                    Description = "De-ice switch, 0, 1, or 2.  De-ice - TKS fluid. This switch activates the pump for the weeping wing, dissipating TKS fluid on the leading edges to keep them free of ice. 0 = Off, 1 = Norm, 2 = High. (Right wing)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceCowlingThermalAntiIcePerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/cowling_thermal_anti_ice_per_engine",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - inlet heat. This switch turns on thermal anti ice of the engine cowling and inlet. This takes hot bleed air from the HP compressor.  Per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIcePropTksOnPerEngine
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_prop_tks_on_per_engine",
                    Units = "enum",
                    Description = "De-ice switch, 0, 1, or 2.  De-ice - prop TKS. This switch turns on de-icing of the propeller(s) with TKS fluid to keep ice from building up on your prop. 0 = Off, 1 = Norm, 2 = High. Per engine",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceAntiIceEngineAir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/anti_ice_engine_air",
                    Units = "ratio",
                    Description = "De-Ice ratio.  (Description needed)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceAntiIceEngineAirB
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/anti_ice_engine_air_b",
                    Units = "ratio",
                    Description = "De-Ice ratio.  (Description needed)",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceAutoIgniteOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_auto_ignite_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  De-ice - auto-ignition. This switch turns on a continuous ignition source in the engine to automatically relight it if there is a flameout.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceDetectOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_detect_on",
                    Units = "boolean",
                    Description = "De-ice switch, 0 or 1.  Ice-detection: Turn this switch on to enable ice-detection... if the system detects ice, it will light up the ICE annunciator.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2IceIceTksFluidLiter
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/ice/ice_tks_fluid_liter",
                    Units = "liter",
                    Description = "TKS de-icing fluid reserve. See sim/aircraft/specialcontrols/acf_tks_cap_liter for the plane's total capacity.",
                    
                };
            }
        }
    }
}
