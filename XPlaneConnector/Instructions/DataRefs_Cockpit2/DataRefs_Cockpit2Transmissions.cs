﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2TransmissionsIndicatorsOilTemperature
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/transmissions/indicators/oil_temperature",
                    Units = "any",
                    Description = "Transmission oil temperature.  Units are the same as the max oil temperature in ACF file.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TransmissionsIndicatorsOilPressure
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/transmissions/indicators/oil_pressure",
                    Units = "any",
                    Description = "Transmission oil pressure.  Units are the same as the max oil pressure in ACF file.",
                    
                };
            }
        }
    }
}
