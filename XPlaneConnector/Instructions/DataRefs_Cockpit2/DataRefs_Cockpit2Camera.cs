﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2CameraCameraOffsetPitch = null;
        /// <summary>
        /// In-cockpit camera angular offset relative to airplane orientation (Pitch)
        /// </summary>
        public static DataRefElement Cockpit2CameraCameraOffsetPitch {
            get {
                if (cockpit2CameraCameraOffsetPitch == null)
                { cockpit2CameraCameraOffsetPitch = GetDataRefElement("sim/cockpit2/camera/camera_offset_pitch", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "In-cockpit camera angular offset relative to airplane orientation (Pitch)"); }
                return cockpit2CameraCameraOffsetPitch;
            }
        }

        private static DataRefElement cockpit2CameraCameraOffsetHeading = null;
        /// <summary>
        /// In-cockpit camera angular offset relative to airplane orientation (Heading)
        /// </summary>
        public static DataRefElement Cockpit2CameraCameraOffsetHeading {
            get {
                if (cockpit2CameraCameraOffsetHeading == null)
                { cockpit2CameraCameraOffsetHeading = GetDataRefElement("sim/cockpit2/camera/camera_offset_heading", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "In-cockpit camera angular offset relative to airplane orientation (Heading)"); }
                return cockpit2CameraCameraOffsetHeading;
            }
        }

        private static DataRefElement cockpit2CameraCameraOffsetRoll = null;
        /// <summary>
        /// In-cockpit camera angular offset relative to airplane orientation (Roll)
        /// </summary>
        public static DataRefElement Cockpit2CameraCameraOffsetRoll {
            get {
                if (cockpit2CameraCameraOffsetRoll == null)
                { cockpit2CameraCameraOffsetRoll = GetDataRefElement("sim/cockpit2/camera/camera_offset_roll", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "In-cockpit camera angular offset relative to airplane orientation (Roll)"); }
                return cockpit2CameraCameraOffsetRoll;
            }
        }

        private static DataRefElement cockpit2CameraCameraFieldOfView = null;
        /// <summary>
        /// In-cockpit camera field of view
        /// </summary>
        public static DataRefElement Cockpit2CameraCameraFieldOfView {
            get {
                if (cockpit2CameraCameraFieldOfView == null)
                { cockpit2CameraCameraFieldOfView = GetDataRefElement("sim/cockpit2/camera/camera_field_of_view", TypeStart_float, UtilConstants.Flag_Yes, "degrees", "In-cockpit camera field of view"); }
                return cockpit2CameraCameraFieldOfView;
            }
        }
    }
}