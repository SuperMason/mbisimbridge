﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2TcasIndicatorsRelativeBearingDegs = null;
        /// <summary>
        /// Relative bearing of each other plane in degrees for TCAS - relative to sim/flightmodel/position/true_psi not ground track or anything else
        /// </summary>
        public static DataRefElement Cockpit2TcasIndicatorsRelativeBearingDegs {
            get {
                if (cockpit2TcasIndicatorsRelativeBearingDegs == null)
                { cockpit2TcasIndicatorsRelativeBearingDegs = GetDataRefElement("sim/cockpit2/tcas/indicators/relative_bearing_degs", TypeStart_float64, UtilConstants.Flag_Yes, "degrees", "Relative bearing of each other plane in degrees for TCAS - relative to sim/flightmodel/position/true_psi not ground track or anything else"); }
                return cockpit2TcasIndicatorsRelativeBearingDegs;
            }
        }

        /// <summary>
        /// Distance to each other plane in meters for TCAS
        /// </summary>
        public static DataRefElement Cockpit2TcasIndicatorsRelativeDistanceMtrs
        { get { return GetDataRefElement("sim/cockpit2/tcas/indicators/relative_distance_mtrs", TypeStart_float64, UtilConstants.Flag_Yes, "meters", "Distance to each other plane in meters for TCAS"); } }
        /// <summary>
        /// Relative altitude (positive means above us) for TCAS
        /// </summary>
        public static DataRefElement Cockpit2TcasIndicatorsRelativeAltitudeMtrs
        { get { return GetDataRefElement("sim/cockpit2/tcas/indicators/relative_altitude_mtrs", TypeStart_float64, UtilConstants.Flag_Yes, "meters", "Relative altitude (positive means above us) for TCAS"); } }
        /// <summary>
        /// True if a TCAS alert is currently issued.
        /// </summary>
        public static DataRefElement Cockpit2TcasIndicatorsTcasAlert
        { get { return GetDataRefElement("sim/cockpit2/tcas/indicators/tcas_alert", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "True if a TCAS alert is currently issued."); } }
        /// <summary>
        /// If TCAS is not overriden by plgugin, returns the number of planes in X-Plane, which might be under plugin control or X-Plane control. If TCAS is overriden, returns how many targets are actually being written to with the override. These are not necessarily consecutive entries in the TCAS arrays.
        /// </summary>
        public static DataRefElement Cockpit2TcasIndicatorsTcasNumAcf
        { get { return GetDataRefElement("sim/cockpit2/tcas/indicators/tcas_num_acf", TypeStart_int, UtilConstants.Flag_No, "integer", "If TCAS is not overriden by plgugin, returns the number of planes in X-Plane, which might be under plugin control or X-Plane control. If TCAS is overriden, returns how many targets are actually being written to with the override. These are not necessarily consecutive entries in the TCAS arrays."); } }
        /// <summary>
        /// 24bit (0-16777215 or 0 - 0xFFFFFF) unique ID of the airframe. This is also known as the ADS-B \"hexcode\".
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsModesId
        { get { return GetDataRefElement("sim/cockpit2/tcas/targets/modeS_id", TypeStart_int64, UtilConstants.Flag_Yes, "integer", "24bit (0-16777215 or 0 - 0xFFFFFF) unique ID of the airframe. This is also known as the ADS-B \"hexcode\"."); } }
        public static DataRefElement Cockpit2TcasTargetsModecCode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/modeC_code",
                    Units = "integer",
                    Description = "Mode C transponder code 0000 to 7777. This is not really an integer, this is an octal number.",
                };
            }
        }

        private static DataRefElement cockpit2TcasTargetsFlightId = null;
        /// <summary>
        /// 7 character Flight ID, terminated by 0 byte. ICAO flightplan item 7.
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsFlightId {
            get {
                if (cockpit2TcasTargetsFlightId == null)
                { cockpit2TcasTargetsFlightId = GetDataRefElement("sim/cockpit2/tcas/targets/flight_id", TypeStart_byte512, UtilConstants.Flag_Yes, "string", "7 character Flight ID, terminated by 0 byte. ICAO flightplan item 7."); }
                return cockpit2TcasTargetsFlightId;
            }
        }

        private static DataRefElement cockpit2TcasTargetsIcaoType = null;
        /// <summary>
        /// 7 character ICAO code, terminated by 0 byte. C172, B738, etc... see https://www.icao.int/publications/DOC8643/Pages/Search.aspx
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsIcaoType {
            get {
                if (cockpit2TcasTargetsIcaoType == null)
                { cockpit2TcasTargetsIcaoType = GetDataRefElement("sim/cockpit2/tcas/targets/icao_type", TypeStart_byte512, UtilConstants.Flag_Yes, "string", "7 character ICAO code, terminated by 0 byte. C172, B738, etc..."); }
                return cockpit2TcasTargetsIcaoType;
            }
        }

        public static DataRefElement Cockpit2TcasTargetsPositionX
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/x",
                    Units = "meter",
                    Description = "local X coordinate, meter. Writeable only when override_TCAS is set.",
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionY
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/y",
                    Units = "meter",
                    Description = "local Y coordinate, meter. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionZ
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/z",
                    Units = "meter",
                    Description = "local Z coordinate, meter. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionLat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionLon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees.",
                    
                };
            }
        }

        private static DataRefElement cockpit2TcasTargetsPositionEle = null;
        /// <summary>
        /// Global coordinate, meter.
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsPositionEle {
            get {
                if (cockpit2TcasTargetsPositionEle == null)
                { cockpit2TcasTargetsPositionEle = GetDataRefElement("sim/cockpit2/tcas/targets/position/ele", TypeStart_float64, UtilConstants.Flag_Yes, "meter", "global coordinate, meter."); }
                return cockpit2TcasTargetsPositionEle;
            }
        }

        public static DataRefElement Cockpit2TcasTargetsPositionVx
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/vx",
                    Units = "meter/s",
                    Description = "local X velocity. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionVy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/vy",
                    Units = "meter/s",
                    Description = "local Y velocity. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionVz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/vz",
                    Units = "meter/s",
                    Description = "local Z velocity. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionVerticalSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/vertical_speed",
                    Units = "feet/min",
                    Description = "absolute vertical speed feet per minute. Writeable only when override_TCAS is set.",
                    
                };
            }
        }

        private static DataRefElement cockpit2TcasTargetsPositionHpath = null;
        /// <summary>
        /// Ground track in true degrees. Derived from velocity vector.
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsPositionHpath {
            get {
                if (cockpit2TcasTargetsPositionHpath == null)
                { cockpit2TcasTargetsPositionHpath = GetDataRefElement("sim/cockpit2/tcas/targets/position/hpath", TypeStart_float64, UtilConstants.Flag_Yes, "degrees", "Ground track in true degrees. Derived from velocity vector."); }
                return cockpit2TcasTargetsPositionHpath;
            }
        }

        public static DataRefElement Cockpit2TcasTargetsPositionVpath
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/vpath",
                    Units = "degrees",
                    Description = "flight path angle in degrees. Derived from velocity vector.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionVMsc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/V_msc",
                    Units = "meter/s",
                    Description = "total true speed, norm of local velocity vector. That means it includes vertical speed!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/psi",
                    Units = "degrees",
                    Description = "true heading orientation. Writeable only when override_TCAS is set.",
                    
                };
            }
        }

        private static DataRefElement cockpit2TcasTargetsPositionThe = null;
        /// <summary>
        /// Pitch angle. Writeable only when override_TCAS is set.
        /// </summary>
        public static DataRefElement Cockpit2TcasTargetsPositionThe {
            get {
                if (cockpit2TcasTargetsPositionThe == null)
                { cockpit2TcasTargetsPositionThe = GetDataRefElement("sim/cockpit2/tcas/targets/position/the", TypeStart_float64, UtilConstants.Flag_Yes, "degrees", "pitch angle. Writeable only when override_TCAS is set."); }
                return cockpit2TcasTargetsPositionThe;
            }
        }

        public static DataRefElement Cockpit2TcasTargetsPositionPhi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/phi",
                    Units = "degrees",
                    Description = "bank angle. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionWeightOnWheels
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/weight_on_wheels",
                    Units = "boolean",
                    Description = "ground/flight logic. Writeable only when override_TCAS is set.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionGearDeploy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/gear_deploy",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_gear_deploy[0]",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionFlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/flap_ratio",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_flap_ratio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionFlapRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/flap_ratio2",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_flap_ratio2",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionSpeedbrakeRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/speedbrake_ratio",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_speedbrake_ratio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionSlatRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/slat_ratio",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_slat_ratio",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionWingSweep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/wing_sweep",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_wing_sweep",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionThrottle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/throttle",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_throttle[0]",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionYolkPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/yolk_pitch",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_yolk_pitch ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionYolkRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/yolk_roll",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_yolk_roll",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionYolkYaw
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/yolk_yaw",
                    Units = "ratio",
                    Description = "mirror of sim/multiplayer/position/planeN_yolk_yaw",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionLights
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/lights",
                    Units = "bitfield",
                    Description = "beacon=1, land=2, nav=4, strobe=8, taxi=16",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane1Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane1_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane1Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane1_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane1Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane1_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane2Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane2_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane2Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane2_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane2Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane2_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane3Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane3_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane3Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane3_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane3Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane3_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane4Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane4_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane4Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane4_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane4Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane4_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane5Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane5_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane5Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane5_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane5Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane5_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane6Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane6_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane6Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane6_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane6Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane6_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane7Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane7_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane7Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane7_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane7Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane7_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane8Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane8_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane8Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane8_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane8Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane8_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane9Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane9_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane9Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane9_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane9Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane9_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane10Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane10_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane10Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane10_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane10Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane10_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane11Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane11_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane11Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane11_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane11Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane11_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane12Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane12_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane12Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane12_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane12Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane12_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane13Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane13_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane13Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane13_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane13Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane13_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane14Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane14_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane14Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane14_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane14Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane14_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane15Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane15_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane15Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane15_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane15Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane15_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane16Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane16_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane16Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane16_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane16Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane16_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane17Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane17_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane17Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane17_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane17Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane17_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane18Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane18_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane18Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane18_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane18Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane18_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane19Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane19_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane19Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane19_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane19Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane19_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane20Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane20_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane20Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane20_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane20Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane20_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane21Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane21_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane21Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane21_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane21Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane21_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane22Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane22_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane22Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane22_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane22Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane22_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane23Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane23_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane23Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane23_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane23Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane23_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane24Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane24_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane24Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane24_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane24Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane24_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane25Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane25_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane25Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane25_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane25Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane25_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane26Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane26_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane26Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane26_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane26Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane26_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane27Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane27_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane27Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane27_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane27Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane27_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane28Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane28_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane28Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane28_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane28Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane28_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane29Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane29_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane29Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane29_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane29Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane29_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane30Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane30_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane30Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane30_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane30Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane30_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane31Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane31_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane31Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane31_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane31Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane31_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane32Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane32_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane32Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane32_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane32Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane32_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane33Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane33_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane33Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane33_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane33Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane33_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane34Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane34_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane34Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane34_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane34Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane34_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane35Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane35_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane35Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane35_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane35Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane35_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane36Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane36_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane36Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane36_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane36Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane36_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane37Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane37_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane37Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane37_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane37Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane37_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane38Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane38_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane38Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane38_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane38Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane38_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane39Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane39_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane39Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane39_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane39Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane39_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane40Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane40_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane40Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane40_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane40Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane40_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane41Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane41_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane41Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane41_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane41Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane41_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane42Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane42_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane42Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane42_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane42Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane42_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane43Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane43_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane43Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane43_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane43Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane43_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane44Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane44_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane44Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane44_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane44Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane44_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane45Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane45_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane45Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane45_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane45Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane45_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane46Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane46_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane46Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane46_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane46Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane46_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane47Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane47_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane47Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane47_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane47Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane47_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane48Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane48_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane48Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane48_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane48Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane48_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane49Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane49_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane49Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane49_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane49Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane49_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane50Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane50_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane50Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane50_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane50Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane50_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane51Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane51_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane51Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane51_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane51Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane51_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane52Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane52_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane52Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane52_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane52Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane52_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane53Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane53_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane53Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane53_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane53Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane53_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane54Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane54_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane54Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane54_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane54Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane54_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane55Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane55_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane55Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane55_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane55Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane55_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane56Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane56_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane56Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane56_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane56Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane56_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane57Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane57_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane57Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane57_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane57Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane57_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane58Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane58_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane58Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane58_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane58Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane58_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane59Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane59_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane59Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane59_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane59Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane59_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane60Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane60_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane60Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane60_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane60Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane60_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane61Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane61_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane61Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane61_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane61Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane61_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane62Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane62_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane62Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane62_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane62Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane62_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane63Lat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane63_lat",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                    
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane63Lon
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane63_lon",
                    Units = "degrees",
                    Description = "global coordinate, degrees",
                };
            }
        }
        public static DataRefElement Cockpit2TcasTargetsPositionDoublePlane63Ele
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/tcas/targets/position/double/plane63_ele",
                    Units = "meter",
                    Description = "global coordinate, meter. ",
                };
            }
        }
    }
}