﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2ElectricalInverterOn = null;
        /// <summary>
        /// Inverter turned on, 0 or 1. (Was 8, but should be 2 total.)
        /// </summary>
        public static DataRefElement Cockpit2ElectricalInverterOn {
            get {
                if (cockpit2ElectricalInverterOn == null)
                { cockpit2ElectricalInverterOn = GetDataRefElement("sim/cockpit2/electrical/inverter_on", TypeStart_int2, UtilConstants.Flag_Yes, "boolean", "Inverter turned on, 0 or 1. (Was 8, but should be 2 total.)"); }
                return cockpit2ElectricalInverterOn;
            }
        }

        private static DataRefElement cockpit2ElectricalBatteryOn = null;
        /// <summary>
        /// Battery turned on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBatteryOn {
            get {
                if (cockpit2ElectricalBatteryOn == null)
                { cockpit2ElectricalBatteryOn = GetDataRefElement("sim/cockpit2/electrical/battery_on", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "Battery turned on, 0 or 1."); }
                return cockpit2ElectricalBatteryOn;
            }
        }

        private static DataRefElement cockpit2ElectricalBatteryAmps = null;
        /// <summary>
        /// Battery amperage, in (surprisingly) amps.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBatteryAmps {
            get {
                if (cockpit2ElectricalBatteryAmps == null)
                { cockpit2ElectricalBatteryAmps = GetDataRefElement("sim/cockpit2/electrical/battery_amps", TypeStart_float8, UtilConstants.Flag_No, "amps", "Battery amperage, in (surprisingly) amps."); }
                return cockpit2ElectricalBatteryAmps;
            }
        }

        private static DataRefElement cockpit2ElectricalBatteryVoltageActualVolts = null;
        /// <summary>
        /// Actual battery voltage in, umm, volts?
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBatteryVoltageActualVolts {
            get {
                if (cockpit2ElectricalBatteryVoltageActualVolts == null)
                { cockpit2ElectricalBatteryVoltageActualVolts = GetDataRefElement("sim/cockpit2/electrical/battery_voltage_actual_volts", TypeStart_float8, UtilConstants.Flag_No, "volts", "Actual battery voltage in, umm, volts?"); }
                return cockpit2ElectricalBatteryVoltageActualVolts;
            }
        }

        private static DataRefElement cockpit2ElectricalBatteryVoltageIndicatedVolts = null;
        /// <summary>
        /// Indicated battery voltage in, umm, volts? ...The indication may be different than the actual voltage!
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBatteryVoltageIndicatedVolts {
            get {
                if (cockpit2ElectricalBatteryVoltageIndicatedVolts == null)
                { cockpit2ElectricalBatteryVoltageIndicatedVolts = GetDataRefElement("sim/cockpit2/electrical/battery_voltage_indicated_volts", TypeStart_float8, UtilConstants.Flag_No, "volts", "Indicated battery voltage in, umm, volts? ...The indication may be different than the actual voltage!"); }
                return cockpit2ElectricalBatteryVoltageIndicatedVolts;
            }
        }

        private static DataRefElement cockpit2ElectricalGeneratorOn = null;
        /// <summary>
        /// Generator turned on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalGeneratorOn {
            get {
                if (cockpit2ElectricalGeneratorOn == null)
                { cockpit2ElectricalGeneratorOn = GetDataRefElement("sim/cockpit2/electrical/generator_on", TypeStart_int8, UtilConstants.Flag_Yes, "boolean", "Generator turned on, 0 or 1."); }
                return cockpit2ElectricalGeneratorOn;
            }
        }
        
        
        /// <summary>
        /// Generator amperage.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalGeneratorAmps
        { get { return GetDataRefElement("sim/cockpit2/electrical/generator_amps", TypeStart_float8, UtilConstants.Flag_No, "amps", "Generator amperage."); } }
        /// <summary>
        /// APU generator is turned on, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUGeneratorOn
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_generator_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "APU generator is turned on, 0 or 1."); } }
        /// <summary>
        /// APU generator amperage.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUGeneratorAmps
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_generator_amps", TypeStart_float, UtilConstants.Flag_Yes, "amps", "APU generator amperage."); } }
        /// <summary>
        /// APU power switch, 0 is off, 1 is on, 2 is start-er-up!
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUStarterSwitch
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_starter_switch", TypeStart_int, UtilConstants.Flag_Yes, "enum", "APU power switch, 0 is off, 1 is on, 2 is start-er-up!"); } }
        /// <summary>
        /// N1 of the APU
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUN1Percent
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_N1_percent", TypeStart_float, UtilConstants.Flag_No, "percent", "N1 of the APU"); } }
        /// <summary>
        /// EGT of the APU
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUEGTC
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_EGT_c", TypeStart_float, UtilConstants.Flag_No, "celsius", "EGT of the APU"); } }
        /// <summary>
        /// APU door open ratio
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPUDoor
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_door", TypeStart_float, UtilConstants.Flag_No, "ratio", "APU door open ratio"); } }
        /// <summary>
        /// APU actually running, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalAPURunning
        { get { return GetDataRefElement("sim/cockpit2/electrical/APU_running", TypeStart_int, UtilConstants.Flag_No, "boolean", "APU actually running, 0 or 1."); } }
        /// <summary>
        /// Switch to connect the two busses together - power from one feeds the other.  0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalCrossTie
        { get { return GetDataRefElement("sim/cockpit2/electrical/cross_tie", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch to connect the two busses together - power from one feeds the other.  0 or 1."); } }
        /// <summary>
        /// Volt-meter selection (0=external,1=ctr,2=lft,3=rgt,4=tpl,5=bat - use cmnds to set!
        /// </summary>
        public static DataRefElement Cockpit2ElectricalDcVoltmeterSelection
        { get { return GetDataRefElement("sim/cockpit2/electrical/dc_voltmeter_selection", TypeStart_int, UtilConstants.Flag_No, "enum", "Volt-meter selection (0=external,1=ctr,2=lft,3=rgt,4=tpl,5=bat - use cmnds to set!"); } }
        /// <summary>
        /// volt-meter value
        /// </summary>
        public static DataRefElement Cockpit2ElectricalDcVoltmeterValue
        { get { return GetDataRefElement("sim/cockpit2/electrical/dc_voltmeter_value", TypeStart_float, UtilConstants.Flag_No, "voltage", "volt-meter value"); } }
        /// <summary>
        /// Bus voltage for given bus - dim 4 until 1030
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBusVolts
        { get { return GetDataRefElement("sim/cockpit2/electrical/bus_volts", TypeStart_float6, UtilConstants.Flag_No, "voltage", "Bus voltage for given bus - dim 4 until 1030"); } }
        /// <summary>
        /// Bus load in amps per bus - dim 4 until 1030
        /// </summary>
        public static DataRefElement Cockpit2ElectricalBusLoadAmps
        { get { return GetDataRefElement("sim/cockpit2/electrical/bus_load_amps", TypeStart_float6, UtilConstants.Flag_No, "amps", "Bus load in amps per bus - dim 4 until 1030"); } }
        /// <summary>
        /// Add additional load to this dataref to create additional custom bus loads.
        /// </summary>
        public static DataRefElement Cockpit2ElectricalPluginBusLoadAmps
        { get { return GetDataRefElement("sim/cockpit2/electrical/plugin_bus_load_amps", TypeStart_float6, UtilConstants.Flag_Yes, "amps", "Add additional load to this dataref to create additional custom bus loads."); } }
        /// <summary>
        /// This is the actual panel brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalPanelBrightnessRatio
        { get { return GetDataRefElement("sim/cockpit2/electrical/panel_brightness_ratio", TypeStart_float4, UtilConstants.Flag_No, "ratio", "This is the actual panel brightness, taking into account failures"); } }
        /// <summary>
        /// This is the actual panel brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalPanelBrightnessRatioAuto
        { get { return GetDataRefElement("sim/cockpit2/electrical/panel_brightness_ratio_auto", TypeStart_float4, UtilConstants.Flag_No, "ratio", "This is the actual panel brightness, taking into account failures"); } }
        /// <summary>
        /// This is the actual panel brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalPanelBrightnessRatioManual
        { get { return GetDataRefElement("sim/cockpit2/electrical/panel_brightness_ratio_manual", TypeStart_float4, UtilConstants.Flag_No, "ratio", "This is the actual panel brightness, taking into account failures"); } }
        /// <summary>
        /// Actual instrument brightness, taking into account failures - was [16] until 11.10
        /// </summary>
        public static DataRefElement Cockpit2ElectricalInstrumentBrightnessRatio
        { get { return GetDataRefElement("sim/cockpit2/electrical/instrument_brightness_ratio", TypeStart_float32, UtilConstants.Flag_No, "ratio", "Actual instrument brightness, taking into account failures - was [16] until 11.10"); } }
        /// <summary>
        /// Actual instrument brightness, taking into account failures - was [16] until 11.10
        /// </summary>
        public static DataRefElement Cockpit2ElectricalInstrumentBrightnessRatioAuto
        { get { return GetDataRefElement("sim/cockpit2/electrical/instrument_brightness_ratio_auto", TypeStart_float32, UtilConstants.Flag_No, "ratio", "Actual instrument brightness, taking into account failures - was [16] until 11.10"); } }
        /// <summary>
        /// Actual instrument brightness, taking into account failures - was [16] until 11.10
        /// </summary>
        public static DataRefElement Cockpit2ElectricalInstrumentBrightnessRatioManual
        { get { return GetDataRefElement("sim/cockpit2/electrical/instrument_brightness_ratio_manual", TypeStart_float32, UtilConstants.Flag_No, "ratio", "Actual instrument brightness, taking into account failures - was [16] until 11.10"); } }
        /// <summary>
        /// Actual HUD brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalHUDBrightnessRatio
        { get { return GetDataRefElement("sim/cockpit2/electrical/HUD_brightness_ratio", TypeStart_float, UtilConstants.Flag_No, "ratio", "Actual HUD brightness, taking into account failures"); } }
        /// <summary>
        /// Actual HUD brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalHUDBrightnessRatioAuto
        { get { return GetDataRefElement("sim/cockpit2/electrical/HUD_brightness_ratio_auto", TypeStart_float, UtilConstants.Flag_No, "ratio", "Actual HUD brightness, taking into account failures"); } }
        /// <summary>
        /// Actual HUD brightness, taking into account failures
        /// </summary>
        public static DataRefElement Cockpit2ElectricalHUDBrightnessRatioManual
        { get { return GetDataRefElement("sim/cockpit2/electrical/HUD_brightness_ratio_manual", TypeStart_float, UtilConstants.Flag_No, "ratio", "Actual HUD brightness, taking into account failures"); } }
    }
}
