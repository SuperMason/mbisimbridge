﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2EngineActuatorsCyclicElevatorDeg = null;
        /// <summary>
        /// This is the longitudinal cyclic COMMAND, in degrees. Positive forwards cyclic.
        /// </summary>
        public static DataRefElement Cockpit2EngineActuatorsCyclicElevatorDeg {
            get {
                if (cockpit2EngineActuatorsCyclicElevatorDeg == null)
                { cockpit2EngineActuatorsCyclicElevatorDeg = GetDataRefElement("sim/cockpit2/engine/actuators/cyclic_elevator_deg", TypeStart_float8, UtilConstants.Flag_Yes, "degrees", "This is the longitudinal cyclic COMMAND, in degrees. Positive forwards cyclic."); }
                return cockpit2EngineActuatorsCyclicElevatorDeg;
            }
        }


        public static DataRefElement Cockpit2EngineActuatorsCyclicAileronDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/cyclic_aileron_deg",
                    Units = "degrees",
                    Description = "This is the lateral cyclic COMMAND, in degrees. Positive right cyclic.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsThrottleRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/throttle_ratio",
                    Units = "ratio",
                    Description = "Throttle position of the handle itself, from 0.0 (idle) to 1.0 (max normal).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsThrottleBetaRevRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/throttle_beta_rev_ratio",
                    Units = "ratio",
                    Description = "Throttle position of the handle as a ratio with reverse and beta, -2..-1 = rev, -1..0=beta, 0..1=alpha",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsThrottleJetRevRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/throttle_jet_rev_ratio",
                    Units = "ratio",
                    Description = "Throttle position of the handle as a ratio with reverse, -1..0 = rev, 0..1=fwd",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsBetaRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/beta_ratio",
                    Units = "ratio",
                    Description = "Throttle position of the handle as a ratio, 0.0 is minimum beta, 1.0 = maximum beta.",
                    
                };
            }
        }

        private static DataRefElement cockpit2EngineActuatorsThrottleRatioAll = null;
        /// <summary>
        /// Throttle position of the handle itself - this controls all the handles at once.
        /// </summary>
        public static DataRefElement Cockpit2EngineActuatorsThrottleRatioAll {
            get {
                if (cockpit2EngineActuatorsThrottleRatioAll == null)
                { cockpit2EngineActuatorsThrottleRatioAll = GetDataRefElement("sim/cockpit2/engine/actuators/throttle_ratio_all", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Throttle position of the handle itself - this controls all the handles at once."); }
                return cockpit2EngineActuatorsThrottleRatioAll;
            }
        }
        
        public static DataRefElement Cockpit2EngineActuatorsThrottleBetaRevRatioAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/throttle_beta_rev_ratio_all",
                    Units = "ratio",
                    Description = "Controls the throttle handle position, but includes beta and reverse.  -2..-1 is reverse, -1..0 is beta, 0..1 is alpha.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsThrottleJetRevRatioAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/throttle_jet_rev_ratio_all",
                    Units = "ratio",
                    Description = "Controls the throttle handle position, but includes reverse.  -1..0 is reverse, 0..1 is fwd.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropRotationSpeedRadSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_rotation_speed_rad_sec",
                    Units = "radians/second",
                    Description = "Prop handle position, in radians per second of requested prop rpm.  Use this if your plane has a constant speed prop.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropRotationSpeedRadSecAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_rotation_speed_rad_sec_all",
                    Units = "radians/second",
                    Description = "Prop handle position, this controls all props at once.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropAngleDegrees
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_angle_degrees",
                    Units = "degrees",
                    Description = "Prop handle position, in degrees.  Use this if your plane has a manual-adjust prop.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropAngleDegreesAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_angle_degrees_all",
                    Units = "degrees",
                    Description = "Prop handle position, in degrees, set all at once.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_ratio",
                    Units = "ratio",
                    Description = "Prop handle position, in ratio. NOTE: This is also used for helicopter collective!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropRatioAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_ratio_all",
                    Units = "ratio",
                    Description = "Prop handle position, in ratio. This controls all handles at once. NOTE: This is also used for helicopter collective!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsMixtureRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/mixture_ratio",
                    Units = "ratio",
                    Description = "Mixture handle position, 0.0 (cutoff) to 1.0 (full rich).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsMixtureRatioAll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/mixture_ratio_all",
                    Units = "ratio",
                    Description = "Mixture handle position, this controls all at once.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsCarbHeatRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/carb_heat_ratio",
                    Units = "ratio",
                    Description = "Carb-heat handle position, 0.0 (none) to 1.0 (full).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsCowlFlapRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/cowl_flap_ratio",
                    Units = "ratio",
                    Description = "Cowl-flap handle position, 0.0 (none) to 1.0 (full open).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPrimerRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/primer_ratio",
                    Units = "ratio",
                    Description = "Primer handle position, 0.0 (none) to 1.0 (on).",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsAfterburnerEnabled
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/afterburner_enabled",
                    Units = "boolean",
                    Description = "Afterburner enabled, on or off.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsIgniterOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/igniter_on",
                    Units = "boolean",
                    Description = "Igniter, on or off.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsAutoIgniteOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/auto_ignite_on",
                    Units = "boolean",
                    Description = "Auto-igniter switch, 0 or 1. Auto-ignition. This switch turns on a continuous ignition source in the engine to automatically relight it if there is a flameout.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsIgnitionOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/ignition_on",
                    Units = "enum",
                    Description = "0 = off, 1 = left, 2 = right, 3 = both",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsIgnitionKey
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/ignition_key",
                    Units = "enum",
                    Description = "0 = off, 1 = left, 2 = right, 3 = both, 4 = starting",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsStarterHit
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/starter_hit",
                    Units = "boolean",
                    Description = "True while the starter motor is engaged",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsFadecOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/fadec_on",
                    Units = "boolean",
                    Description = "Fadec, ok or off.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPrimerOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/primer_on",
                    Units = "boolean",
                    Description = "Primer button, on or off.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsFuelPumpOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/fuel_pump_on",
                    Units = "boolean",
                    Description = "Fuel pump, on or off.  This is the electric per-engine fuel pump!",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsIdleSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/idle_speed",
                    Units = "boolean",
                    Description = "Idle speed, hi=1 or lo=0.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsIdleSpeedRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/idle_speed_ratio",
                    Units = "[0..1]",
                    Description = "Idle speed, hi=1 or lo=0 - continuous control",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_mode",
                    Units = "enum",
                    Description = "This is the propeller and engine operation mode. It is used for props and jets. Mode 0 is feathered, 1 is normal, 2 is in beta, and reverse (prop or jet) is mode 3.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsPropPitchDeg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/prop_pitch_deg",
                    Units = "degrees",
                    Description = "This is the REQUESTED pitch of the prop in degrees from its flat-pitch setting.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsStartLockEngaged
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/start_lock_engaged",
                    Units = "boolean",
                    Description = "Start lock to lock prop into fine pitch position so it doesn't feather on loss of oil pressure, for fixed turbofan engines.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsUnfeatherPumpRunning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/unfeather_pump_running",
                    Units = "boolean",
                    Description = "Manual unfeather pump can provide oil pressure to the governor, to unfeather a stopped prop when the startlocks weren't engaged. Running the pump first unfeathers the prop and then engages the startlock",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsGovernorOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/governor_on",
                    Units = "boolean",
                    Description = "Governor on-off switch.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsFireExtinguisherOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/fire_extinguisher_on",
                    Units = "boolean",
                    Description = "Fire extinguisher on",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsClutchEngage
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/clutch_engage",
                    Units = "boolean",
                    Description = "XP10: the on/off status of the clutch, either per engine or per prop, depending on voodoo, dim 8. XP11: there is only ONE clutch for the whle airplane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsClutchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/clutch_ratio",
                    Units = "ratio",
                    Description = "XP10: the ratio of the clutch, either per engine or per prop, depending on voodoo, dim 8. XP11: there is only ONE clutch for the whole airplane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsManualFeatherProp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/manual_feather_prop",
                    Units = "boolean",
                    Description = "override and activate the feather, no matter what any auto-feather logic does.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsN1TargetBug
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/N1_target_bug",
                    Units = "percent",
                    Description = "N1 bug (target for thrust reference), %",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineActuatorsEPRTargetBug
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/actuators/EPR_target_bug",
                    Units = "percent",
                    Description = "EPR bug (target for thrust reference), %",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsN1Percent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/N1_percent",
                    Units = "percent",
                    Description = "N1, %.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsN2Percent
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/N2_percent",
                    Units = "percent",
                    Description = "N2, %.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsMPRInHg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/MPR_in_hg",
                    Units = "inches_hg",
                    Description = "Manifold pressure, inches HG.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsEPRRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/EPR_ratio",
                    Units = "ratio",
                    Description = "EPR, ratio.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsTorqueNMtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/torque_n_mtr",
                    Units = "newton_meters",
                    Description = "Torque, NM.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsFuelFlowKgSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/fuel_flow_kg_sec",
                    Units = "kilograms/second",
                    Description = "total fuel flow, kilograms per second.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsFuelFlowDryKgSec
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/fuel_flow_dry_kg_sec",
                    Units = "kilograms/second",
                    Description = "dry fuel flow, kilograms per second.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsITTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/ITT_deg_C",
                    Units = "degrees_C_or_F",
                    Description = "ITT, deg.  Dataref label is wrong, units vary by plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsEGTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/EGT_deg_C",
                    Units = "degrees_C_or_F",
                    Description = "EGT, deg.  Dataref label is wrong, units vary by plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsCHTDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/CHT_deg_C",
                    Units = "degrees_C_or_F",
                    Description = "CHT, deg.  Dataref label is wrong, units vary by plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsFuelPressurePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/fuel_pressure_psi",
                    Units = "pounds/square_inch",
                    Description = "Fuel pressure, psi.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsOilPressurePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/oil_pressure_psi",
                    Units = "pounds/square_inch",
                    Description = "Oil pressure, psi.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsOilTemperatureDegC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/oil_temperature_deg_C",
                    Units = "degrees_C_or_F",
                    Description = "Oil temp, deg.    Dataref label is wrong, units vary by plane.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsOilQuantityRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/oil_quantity_ratio",
                    Units = "ratio",
                    Description = "Oil quantity, 0.0 to 1.0.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsPowerWatts
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/power_watts",
                    Units = "watts",
                    Description = "Actual engine power output.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsThrustN
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/thrust_n",
                    Units = "newtons",
                    Description = "Total engine thrust in Newtons",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsThrustDryN
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/thrust_dry_n",
                    Units = "newtons",
                    Description = "Dry engine thrust in Newtons",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsEngineSpeedRpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/engine_speed_rpm",
                    Units = "revolutions/minute",
                    Description = "Engine speed, radians per second",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsPropSpeedRpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/prop_speed_rpm",
                    Units = "revolutions/minute",
                    Description = "Prop speed, radians per second                                                                                                                                                                                                         boolean",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsCarburetorTemperatureC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/carburetor_temperature_C",
                    Units = "degrees_C",
                    Description = "Carburator temperature in degrees C",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsIgniterSparking
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/igniter_sparking",
                    Units = "boolean",
                    Description = "Whether igniters are sparking, either because the pilot or system logic turned them on, power is available and they are not failed",
                    
                };
            }
        }
        public static DataRefElement Cockpit2EngineIndicatorsIgniterVolt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/engine/indicators/igniter_volt",
                    Units = "volt",
                    Description = "Voltage we have to charge the ignition, useful for sound effects",
                    
                };
            }
        }
    }
}
