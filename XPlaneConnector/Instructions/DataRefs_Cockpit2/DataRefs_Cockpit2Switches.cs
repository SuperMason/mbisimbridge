﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2SwitchesAvionicsPowerOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesAvionicsPowerOn {
            get {
                if (cockpit2SwitchesAvionicsPowerOn == null)
                { cockpit2SwitchesAvionicsPowerOn = GetDataRefElement("sim/cockpit2/switches/avionics_power_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesAvionicsPowerOn;
            }
        }

        private static DataRefElement cockpit2SwitchesNavigationLightsOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesNavigationLightsOn {
            get {
                if (cockpit2SwitchesNavigationLightsOn == null)
                { cockpit2SwitchesNavigationLightsOn = GetDataRefElement("sim/cockpit2/switches/navigation_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesNavigationLightsOn;
            }
        }

        private static DataRefElement cockpit2SwitchesBeaconOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesBeaconOn{
            get {
                if (cockpit2SwitchesBeaconOn == null)
                { cockpit2SwitchesBeaconOn = GetDataRefElement("sim/cockpit2/switches/beacon_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesBeaconOn;
            }
        }

        private static DataRefElement cockpit2SwitchesStrobeLightsOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesStrobeLightsOn {
            get {
                if (cockpit2SwitchesStrobeLightsOn == null)
                { cockpit2SwitchesStrobeLightsOn = GetDataRefElement("sim/cockpit2/switches/strobe_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesStrobeLightsOn;
            }
        }

        private static DataRefElement cockpit2SwitchesLandingLightsOn = null;
        /// <summary>
        /// Switch, 0 or 1.  This affects the first landing light.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesLandingLightsOn {
            get {
                if (cockpit2SwitchesLandingLightsOn == null)
                { cockpit2SwitchesLandingLightsOn = GetDataRefElement("sim/cockpit2/switches/landing_lights_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1.  This affects the first landing light."); }
                return cockpit2SwitchesLandingLightsOn;
            }
        }

        private static DataRefElement cockpit2SwitchesLandingLightsSwitch = null;
        /// <summary>
        /// Switch, 0 is off, 0.5 is half-intensity, etc. for any of the landing lights.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesLandingLightsSwitch {
            get {
                if (cockpit2SwitchesLandingLightsSwitch == null)
                { cockpit2SwitchesLandingLightsSwitch = GetDataRefElement("sim/cockpit2/switches/landing_lights_switch", TypeStart_float16, UtilConstants.Flag_Yes, "ratio", "Switch, 0 is off, 0.5 is half-intensity, etc. for any of the landing lights."); }
                return cockpit2SwitchesLandingLightsSwitch;
            }
        }

        private static DataRefElement cockpit2SwitchesGenericLightsSwitch = null;
        /// <summary>
        /// Switch, 0 is off, 0.5 is half-intensity, etc. for any of the generic lights.  Was [64] until 11.10
        /// </summary>
        public static DataRefElement Cockpit2SwitchesGenericLightsSwitch {
            get {
                if (cockpit2SwitchesGenericLightsSwitch == null)
                { cockpit2SwitchesGenericLightsSwitch = GetDataRefElement("sim/cockpit2/switches/generic_lights_switch", TypeStart_float128, UtilConstants.Flag_Yes, "ratio", "Switch, 0 is off, 0.5 is half-intensity, etc. for any of the generic lights.  Was [64] until 11.10"); }
                return cockpit2SwitchesGenericLightsSwitch;
            }
        }

        private static DataRefElement cockpit2SwitchesTaxiLightOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesTaxiLightOn {
            get {
                if (cockpit2SwitchesTaxiLightOn == null)
                { cockpit2SwitchesTaxiLightOn = GetDataRefElement("sim/cockpit2/switches/taxi_light_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesTaxiLightOn;
            }
        }

        private static DataRefElement cockpit2SwitchesSpotLightOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesSpotLightOn {
            get {
                if (cockpit2SwitchesSpotLightOn == null)
                { cockpit2SwitchesSpotLightOn = GetDataRefElement("sim/cockpit2/switches/spot_light_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesSpotLightOn;
            }
        }

        private static DataRefElement cockpit2SwitchesDumpFuel = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesDumpFuel {
            get {
                if (cockpit2SwitchesDumpFuel == null)
                { cockpit2SwitchesDumpFuel = GetDataRefElement("sim/cockpit2/switches/dump_fuel", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesDumpFuel;
            }
        }

        private static DataRefElement cockpit2SwitchesPuffersOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesPuffersOn {
            get {
                if (cockpit2SwitchesPuffersOn == null)
                { cockpit2SwitchesPuffersOn = GetDataRefElement("sim/cockpit2/switches/puffers_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesPuffersOn;
            }
        }

        private static DataRefElement cockpit2SwitchesPropSyncOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesPropSyncOn {
            get {
                if (cockpit2SwitchesPropSyncOn == null)
                { cockpit2SwitchesPropSyncOn = GetDataRefElement("sim/cockpit2/switches/prop_sync_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesPropSyncOn;
            }
        }

        private static DataRefElement cockpit2SwitchesJetSyncMode = null;
        /// <summary>
        /// Switch, 0 or 1. fan=0, off=1, turbine=2#WHAT ARE THESE?
        /// </summary>
        public static DataRefElement Cockpit2SwitchesJetSyncMode {
            get {
                if (cockpit2SwitchesJetSyncMode == null)
                { cockpit2SwitchesJetSyncMode = GetDataRefElement("sim/cockpit2/switches/jet_sync_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Switch, 0 or 1. fan=0, off=1, turbine=2#WHAT ARE THESE?"); }
                return cockpit2SwitchesJetSyncMode;
            }
        }

        private static DataRefElement cockpit2SwitchesElectricHydraulicPumpOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesElectricHydraulicPumpOn {
            get {
                if (cockpit2SwitchesElectricHydraulicPumpOn == null)
                { cockpit2SwitchesElectricHydraulicPumpOn = GetDataRefElement("sim/cockpit2/switches/electric_hydraulic_pump_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesElectricHydraulicPumpOn;
            }
        }

        private static DataRefElement cockpit2SwitchesRamAirTurbineOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesRamAirTurbineOn {
            get {
                if (cockpit2SwitchesRamAirTurbineOn == null)
                { cockpit2SwitchesRamAirTurbineOn = GetDataRefElement("sim/cockpit2/switches/ram_air_turbine_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesRamAirTurbineOn;
            }
        }

        private static DataRefElement cockpit2SwitchesYawDamperOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesYawDamperOn {
            get {
                if (cockpit2SwitchesYawDamperOn == null)
                { cockpit2SwitchesYawDamperOn = GetDataRefElement("sim/cockpit2/switches/yaw_damper_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesYawDamperOn;
            }
        }

        private static DataRefElement cockpit2SwitchesArtificialStabilityOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesArtificialStabilityOn {
            get {
                if (cockpit2SwitchesArtificialStabilityOn == null)
                { cockpit2SwitchesArtificialStabilityOn = GetDataRefElement("sim/cockpit2/switches/artificial_stability_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesArtificialStabilityOn;
            }
        }

        private static DataRefElement cockpit2SwitchesArtificialStabilityPitchOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesArtificialStabilityPitchOn {
            get {
                if (cockpit2SwitchesArtificialStabilityPitchOn == null)
                { cockpit2SwitchesArtificialStabilityPitchOn = GetDataRefElement("sim/cockpit2/switches/artificial_stability_pitch_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesArtificialStabilityPitchOn;
            }
        }

        private static DataRefElement cockpit2SwitchesArtificialStabilityRollOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesArtificialStabilityRollOn {
            get {
                if (cockpit2SwitchesArtificialStabilityRollOn == null)
                { cockpit2SwitchesArtificialStabilityRollOn = GetDataRefElement("sim/cockpit2/switches/artificial_stability_roll_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesArtificialStabilityRollOn;
            }
        }

        private static DataRefElement cockpit2SwitchesHUDOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesHUDOn {
            get {
                if (cockpit2SwitchesHUDOn == null)
                { cockpit2SwitchesHUDOn = GetDataRefElement("sim/cockpit2/switches/HUD_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesHUDOn;
            }
        }

        private static DataRefElement cockpit2SwitchesParachuteDeploy = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesParachuteDeploy {
            get {
                if (cockpit2SwitchesParachuteDeploy == null)
                { cockpit2SwitchesParachuteDeploy = GetDataRefElement("sim/cockpit2/switches/parachute_deploy", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesParachuteDeploy;
            }
        }

        private static DataRefElement cockpit2SwitchesJatoOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesJatoOn {
            get {
                if (cockpit2SwitchesJatoOn == null)
                { cockpit2SwitchesJatoOn = GetDataRefElement("sim/cockpit2/switches/jato_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesJatoOn;
            }
        }

        private static DataRefElement cockpit2SwitchesTailhookDeploy = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesTailhookDeploy {
            get {
                if (cockpit2SwitchesTailhookDeploy == null)
                { cockpit2SwitchesTailhookDeploy = GetDataRefElement("sim/cockpit2/switches/tailhook_deploy", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesTailhookDeploy;
            }
        }

        private static DataRefElement cockpit2SwitchesCanopyOpen = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesCanopyOpen {
            get {
                if (cockpit2SwitchesCanopyOpen == null)
                { cockpit2SwitchesCanopyOpen = GetDataRefElement("sim/cockpit2/switches/canopy_open", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesCanopyOpen;
            }
        }

        private static DataRefElement cockpit2SwitchesDoorOpen = null;
        /// <summary>
        /// Switch, 0 or 1. 0 is closed, 1 is open
        /// </summary>
        public static DataRefElement Cockpit2SwitchesDoorOpen {
            get {
                if (cockpit2SwitchesDoorOpen == null)
                { cockpit2SwitchesDoorOpen = GetDataRefElement("sim/cockpit2/switches/door_open", TypeStart_int10, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1. 0 is closed, 1 is open"); }
                return cockpit2SwitchesDoorOpen;
            }
        }

        private static DataRefElement cockpit2SwitchesWaterScoopDeploy = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesWaterScoopDeploy {
            get {
                if (cockpit2SwitchesWaterScoopDeploy == null)
                { cockpit2SwitchesWaterScoopDeploy = GetDataRefElement("sim/cockpit2/switches/water_scoop_deploy", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesWaterScoopDeploy;
            }
        }

        private static DataRefElement cockpit2SwitchesDumpWater = null;
        /// <summary>
        /// This will be set to true when water is in the process of dumping out of a water-bomber.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesDumpWater {
            get {
                if (cockpit2SwitchesDumpWater == null)
                { cockpit2SwitchesDumpWater = GetDataRefElement("sim/cockpit2/switches/dump_water", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "This will be set to true when water is in the process of dumping out of a water-bomber."); }
                return cockpit2SwitchesDumpWater;
            }
        }

        private static DataRefElement cockpit2SwitchesNoSmoking = null;
        /// <summary>
        /// No Smoking
        /// </summary>
        public static DataRefElement Cockpit2SwitchesNoSmoking {
            get {
                if (cockpit2SwitchesNoSmoking == null)
                { cockpit2SwitchesNoSmoking = GetDataRefElement("sim/cockpit2/switches/no_smoking", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "No Smoking"); }
                return cockpit2SwitchesNoSmoking;
            }
        }

        private static DataRefElement cockpit2SwitchesFastenSeatBelts = null;
        /// <summary>
        /// Fasten Seat Belts
        /// </summary>
        public static DataRefElement Cockpit2SwitchesFastenSeatBelts {
            get {
                if (cockpit2SwitchesFastenSeatBelts == null)
                { cockpit2SwitchesFastenSeatBelts = GetDataRefElement("sim/cockpit2/switches/fasten_seat_belts", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Fasten Seat Belts"); }
                return cockpit2SwitchesFastenSeatBelts;
            }
        }

        private static DataRefElement cockpit2SwitchesTotalEnergyAudio = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesTotalEnergyAudio {
            get {
                if (cockpit2SwitchesTotalEnergyAudio == null)
                { cockpit2SwitchesTotalEnergyAudio = GetDataRefElement("sim/cockpit2/switches/total_energy_audio", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesTotalEnergyAudio;
            }
        }

        private static DataRefElement cockpit2SwitchesHSIIsArc = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesHSIIsArc {
            get {
                if (cockpit2SwitchesHSIIsArc == null)
                { cockpit2SwitchesHSIIsArc = GetDataRefElement("sim/cockpit2/switches/HSI_is_arc", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesHSIIsArc;
            }
        }

        private static DataRefElement cockpit2SwitchesAutoBrakeLevel = null;
        /// <summary>
        /// Switch, 0 is RTO (Rejected Take-Off), 1 is off, 2->5 are increasing auto-brake levels.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesAutoBrakeLevel {
            get {
                if (cockpit2SwitchesAutoBrakeLevel == null)
                { cockpit2SwitchesAutoBrakeLevel = GetDataRefElement("sim/cockpit2/switches/auto_brake_level", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Switch, 0 is RTO (Rejected Take-Off), 1 is off, 2->5 are increasing auto-brake levels."); }
                return cockpit2SwitchesAutoBrakeLevel;
            }
        }

        private static DataRefElement cockpit2SwitchesAutoReverseOn = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesAutoReverseOn {
            get {
                if (cockpit2SwitchesAutoReverseOn == null)
                { cockpit2SwitchesAutoReverseOn = GetDataRefElement("sim/cockpit2/switches/auto_reverse_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesAutoReverseOn;
            }
        }

        private static DataRefElement cockpit2SwitchesPropFeatherMode = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesPropFeatherMode {
            get {
                if (cockpit2SwitchesPropFeatherMode == null)
                { cockpit2SwitchesPropFeatherMode = GetDataRefElement("sim/cockpit2/switches/prop_feather_mode", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesPropFeatherMode;
            }
        }

        private static DataRefElement cockpit2SwitchesPreRotateLevel = null;
        /// <summary>
        /// Switch, 0 or 1.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesPreRotateLevel {
            get {
                if (cockpit2SwitchesPreRotateLevel == null)
                { cockpit2SwitchesPreRotateLevel = GetDataRefElement("sim/cockpit2/switches/pre_rotate_level", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1."); }
                return cockpit2SwitchesPreRotateLevel;
            }
        }

        private static DataRefElement cockpit2SwitchesClutchEngage = null;
        /// <summary>
        /// Switch, 0 or 1 - engage/disengage rotor clutch
        /// </summary>
        public static DataRefElement Cockpit2SwitchesClutchEngage {
            get {
                if (cockpit2SwitchesClutchEngage == null)
                { cockpit2SwitchesClutchEngage = GetDataRefElement("sim/cockpit2/switches/clutch_engage", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1 - engage/disengage rotor clutch"); }
                return cockpit2SwitchesClutchEngage;
            }
        }

        private static DataRefElement cockpit2SwitchesRotorBrake = null;
        /// <summary>
        /// Switch, 0 or 1 - turns on the rotor brake.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesRotorBrake {
            get {
                if (cockpit2SwitchesRotorBrake == null)
                { cockpit2SwitchesRotorBrake = GetDataRefElement("sim/cockpit2/switches/rotor_brake", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1 - turns on the rotor brake."); }
                return cockpit2SwitchesRotorBrake;
            }
        }

        private static DataRefElement cockpit2SwitchesHotelMode = null;
        /// <summary>
        /// Switch, 0 or 1 - turns on hotel mode on free turbines.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesHotelMode {
            get {
                if (cockpit2SwitchesHotelMode == null)
                { cockpit2SwitchesHotelMode = GetDataRefElement("sim/cockpit2/switches/hotel_mode", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Switch, 0 or 1 - turns on hotel mode on free turbines."); }
                return cockpit2SwitchesHotelMode;
            }
        }

        private static DataRefElement cockpit2SwitchesRotorBrakeRatio = null;
        /// <summary>
        /// This is the relative strength of the rotor brake compared to X-Plane default.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesRotorBrakeRatio {
            get {
                if (cockpit2SwitchesRotorBrakeRatio == null)
                { cockpit2SwitchesRotorBrakeRatio = GetDataRefElement("sim/cockpit2/switches/rotor_brake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the relative strength of the rotor brake compared to X-Plane default."); }
                return cockpit2SwitchesRotorBrakeRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesHotelModeRatio = null;
        /// <summary>
        /// This is the relative strength of the hotel mode brake compared to X-Plane default.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesHotelModeRatio {
            get {
                if (cockpit2SwitchesHotelModeRatio == null)
                { cockpit2SwitchesHotelModeRatio = GetDataRefElement("sim/cockpit2/switches/hotel_mode_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the relative strength of the hotel mode brake compared to X-Plane default."); }
                return cockpit2SwitchesHotelModeRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesClutchRatio = null;
        /// <summary>
        /// Current clutch ratio, 0.0 is off, 1.0 is on - follows clutch engage with some lag.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesClutchRatio {
            get {
                if (cockpit2SwitchesClutchRatio == null)
                { cockpit2SwitchesClutchRatio = GetDataRefElement("sim/cockpit2/switches/clutch_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Current clutch ratio, 0.0 is off, 1.0 is on - follows clutch engage with some lag."); }
                return cockpit2SwitchesClutchRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesRocketMode = null;
        /// <summary>
        /// This will have an enumeration based on the direction of fire of any maneuvering rockets.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesRocketMode {
            get {
                if (cockpit2SwitchesRocketMode == null)
                { cockpit2SwitchesRocketMode = GetDataRefElement("sim/cockpit2/switches/rocket_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "This will have an enumeration based on the direction of fire of any maneuvering rockets."); }
                return cockpit2SwitchesRocketMode;
            }
        }

        private static DataRefElement cockpit2SwitchesBurnerLevel = null;
        /// <summary>
        /// This can be 0 (half-power afterburners) or 1 (full-power burner).
        /// </summary>
        public static DataRefElement Cockpit2SwitchesBurnerLevel {
            get {
                if (cockpit2SwitchesBurnerLevel == null)
                { cockpit2SwitchesBurnerLevel = GetDataRefElement("sim/cockpit2/switches/burner_level", TypeStart_int, UtilConstants.Flag_Yes, "enum", "This can be 0 (half-power afterburners) or 1 (full-power burner)."); }
                return cockpit2SwitchesBurnerLevel;
            }
        }

        private static DataRefElement cockpit2SwitchesAlternateStaticAirRatio = null;
        /// <summary>
        /// Alternate static air ratio, 0.0 is off, 1,.0 is on.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesAlternateStaticAirRatio {
            get {
                if (cockpit2SwitchesAlternateStaticAirRatio == null)
                { cockpit2SwitchesAlternateStaticAirRatio = GetDataRefElement("sim/cockpit2/switches/alternate_static_air_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Alternate static air ratio, 0.0 is off, 1,.0 is on."); }
                return cockpit2SwitchesAlternateStaticAirRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesStandbyVacuumPump = null;
        public static DataRefElement Cockpit2SwitchesStandbyVacuumPump {
            get {
                if (cockpit2SwitchesStandbyVacuumPump == null)
                { cockpit2SwitchesStandbyVacuumPump = GetDataRefElement("sim/cockpit2/switches/standby_vacuum_pump", TypeStart_int, UtilConstants.Flag_Yes, "", ""); }
                return cockpit2SwitchesStandbyVacuumPump;
            }
        }

        private static DataRefElement cockpit2SwitchesWiperSpeed = null;
        /// <summary>
        /// 0=off,1=25%speed,2=50%speed,3=100%speed.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesWiperSpeed {
            get {
                if (cockpit2SwitchesWiperSpeed == null)
                { cockpit2SwitchesWiperSpeed = GetDataRefElement("sim/cockpit2/switches/wiper_speed", TypeStart_int, UtilConstants.Flag_Yes, "enum", "0=off,1=25%speed,2=50%speed,3=100%speed."); }
                return cockpit2SwitchesWiperSpeed;
            }
        }

        private static DataRefElement cockpit2SwitchesCustomSliderOn = null;
        /// <summary>
        /// custom sliders.  When flipped, slider moves based on timing in planemaker.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesCustomSliderOn {
            get {
                if (cockpit2SwitchesCustomSliderOn == null)
                { cockpit2SwitchesCustomSliderOn = GetDataRefElement("sim/cockpit2/switches/custom_slider_on", TypeStart_int24, UtilConstants.Flag_Yes, "boolean", "custom sliders.  When flipped, slider moves based on timing in planemaker."); }
                return cockpit2SwitchesCustomSliderOn;
            }
        }

        private static DataRefElement cockpit2SwitchesPanelBrightnessRatio = null;
        /// <summary>
        /// Rheostat controlling panel brightness.  0 = flood, 1-3 = spot lights.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesPanelBrightnessRatio {
            get {
                if (cockpit2SwitchesPanelBrightnessRatio == null)
                { cockpit2SwitchesPanelBrightnessRatio = GetDataRefElement("sim/cockpit2/switches/panel_brightness_ratio", TypeStart_float4, UtilConstants.Flag_Yes, "ratio", "Rheostat controlling panel brightness.  0 = flood, 1-3 = spot lights."); }
                return cockpit2SwitchesPanelBrightnessRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesInstrumentBrightnessRatio = null;
        /// <summary>
        /// Rheostat controlling instruments brightnesss.  0 = default pilot, 1 = default copilot, 2-15 = custom - was [16] until 11.10
        /// </summary>
        public static DataRefElement Cockpit2SwitchesInstrumentBrightnessRatio {
            get {
                if (cockpit2SwitchesInstrumentBrightnessRatio == null)
                { cockpit2SwitchesInstrumentBrightnessRatio = GetDataRefElement("sim/cockpit2/switches/instrument_brightness_ratio", TypeStart_float32, UtilConstants.Flag_Yes, "ratio", "Rheostat controlling instruments brightnesss.  0 = default pilot, 1 = default copilot, 2-15 = custom - was [16] until 11.10"); }
                return cockpit2SwitchesInstrumentBrightnessRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesHUDBrightnessRatio = null;
        /// <summary>
        /// Rheostat controlling HUD brightness.
        /// </summary>
        public static DataRefElement Cockpit2SwitchesHUDBrightnessRatio {
            get {
                if (cockpit2SwitchesHUDBrightnessRatio == null)
                { cockpit2SwitchesHUDBrightnessRatio = GetDataRefElement("sim/cockpit2/switches/HUD_brightness_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Rheostat controlling HUD brightness."); }
                return cockpit2SwitchesHUDBrightnessRatio;
            }
        }

        private static DataRefElement cockpit2SwitchesCameraPowerOn = null;
        /// <summary>
        /// Camera power on
        /// </summary>
        public static DataRefElement Cockpit2SwitchesCameraPowerOn {
            get {
                if (cockpit2SwitchesCameraPowerOn == null)
                { cockpit2SwitchesCameraPowerOn = GetDataRefElement("sim/cockpit2/switches/camera_power_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Camera power on"); }
                return cockpit2SwitchesCameraPowerOn;
            }
        }

        private static DataRefElement cockpit2SwitchesTotalEnergyAudioOn = null;
        /// <summary>
        /// Variometer audio on
        /// </summary>
        public static DataRefElement Cockpit2SwitchesTotalEnergyAudioOn {
            get {
                if (cockpit2SwitchesTotalEnergyAudioOn == null)
                { cockpit2SwitchesTotalEnergyAudioOn = GetDataRefElement("sim/cockpit2/switches/total_energy_audio_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Variometer audio on"); }
                return cockpit2SwitchesTotalEnergyAudioOn;
            }
        }
    }
}
