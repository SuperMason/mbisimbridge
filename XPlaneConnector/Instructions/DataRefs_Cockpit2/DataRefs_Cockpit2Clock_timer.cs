﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Elapsed time on the timer, hours
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerElapsedTimeHours
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/elapsed_time_hours", TypeStart_int, UtilConstants.Flag_No, "hours", "Elapsed time on the timer, hours"); } }
        /// <summary>
        /// Elapsed time on the timer, minutes
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerElapsedTimeMinutes
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/elapsed_time_minutes", TypeStart_int, UtilConstants.Flag_No, "minutes", "Elapsed time on the timer, minutes"); } }
        /// <summary>
        /// Elapsed time on the timer, seconds
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerElapsedTimeSeconds
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/elapsed_time_seconds", TypeStart_int, UtilConstants.Flag_No, "seconds", "Elapsed time on the timer, seconds"); } }
        /// <summary>
        /// Zulu time, hours
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerZuluTimeHours
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/zulu_time_hours", TypeStart_int, UtilConstants.Flag_No, "hours", "Zulu time, hours"); } }
        /// <summary>
        /// Zulu time, minutes
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerZuluTimeMinutes
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/zulu_time_minutes", TypeStart_int, UtilConstants.Flag_No, "minutes", "Zulu time, minutes"); } }
        /// <summary>
        /// Zulu time, seconds
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerZuluTimeSeconds
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/zulu_time_seconds", TypeStart_int, UtilConstants.Flag_No, "seconds", "Zulu time, seconds"); } }
        /// <summary>
        /// Local time, hours
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerLocalTimeHours
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/local_time_hours", TypeStart_int, UtilConstants.Flag_No, "hours", "Local time, hours"); } }
        /// <summary>
        /// Local time, minutes
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerLocalTimeMinutes
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/local_time_minutes", TypeStart_int, UtilConstants.Flag_No, "minutes", "Local time, minutes"); } }
        /// <summary>
        /// Local time, seconds
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerLocalTimeSeconds
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/local_time_seconds", TypeStart_int, UtilConstants.Flag_No, "seconds", "Local time, seconds"); } }
        /// <summary>
        /// Hobbs meter time, hours
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerHobbsTimeHours
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/hobbs_time_hours", TypeStart_int, UtilConstants.Flag_No, "hours", "Hobbs meter time, hours"); } }
        /// <summary>
        /// Hobbs meter time, minutes
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerHobbsTimeMinutes
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/hobbs_time_minutes", TypeStart_int, UtilConstants.Flag_No, "minutes", "Hobbs meter time, minutes"); } }
        /// <summary>
        /// Hobbs meter time, seconds
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerHobbsTimeSeconds
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/hobbs_time_seconds", TypeStart_int, UtilConstants.Flag_No, "seconds", "Hobbs meter time, seconds"); } }
        /// <summary>
        /// True if timer is running
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerTimerRunning
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/timer_running", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if timer is running"); } }
        /// <summary>
        /// True if time shown is GMT
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerTimerIsGMT
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/timer_is_GMT", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if time shown is GMT"); } }
        /// <summary>
        /// True if date is showing (date button pressed recently)
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerDateIsShowing
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/date_is_showing", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if date is showing (date button pressed recently)"); } }
        /// <summary>
        /// Numeric day of month
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerCurrentDay
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/current_day", TypeStart_int, UtilConstants.Flag_No, "day", "Numeric day of month"); } }
        /// <summary>
        /// Numeric month of the year
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerCurrentMonth
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/current_month", TypeStart_int, UtilConstants.Flag_No, "month", "Numeric month of the year"); } }
        /// <summary>
        /// TODO
        /// </summary>
        public static DataRefElement Cockpit2ClockTimerTimerMode
        { get { return GetDataRefElement("sim/cockpit2/clock_timer/timer_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "TODO"); } }
    }
}