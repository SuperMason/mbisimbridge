﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2FuelFuelTankSelectorLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_selector_left",
                    Units = "enum",
                    Description = "This is the left-engine fuel-tank selector.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTankSelectorRight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_selector_right",
                    Units = "enum",
                    Description = "This is the right-engine fuel-tank selector.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTankSelector
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_selector",
                    Units = "enum",
                    Description = "0=none,1=left,2=center,3=right,4=all",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTankTransferTo
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_transfer_to",
                    Units = "enum",
                    Description = "0=none,1=left,2=center,3=right,5=aft",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTankTransferFrom
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_transfer_from",
                    Units = "enum",
                    Description = "0=none,1=left,2=center,3=right,5=aft",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTankPumpOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_tank_pump_on",
                    Units = "bool",
                    Description = "True if the pump for this tank is on.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelShowingAux
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/showing_aux",
                    Units = "bool",
                    Description = "True if user is holding down the aux-tank button.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelQuantity
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_quantity",
                    Units = "kgs",
                    Description = "Indicated fuel level per tank",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTotalizerInitKg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_totalizer_init_kg",
                    Units = "kg",
                    Description = "Total fuel on board the fuel totalizer was initialized with",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelTotalizerSumKg
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_totalizer_sum_kg",
                    Units = "kg",
                    Description = "Total accumulated fuel used by all engines since totalizer initialization",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelTransferPumpLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/transfer_pump_left",
                    Units = "enum",
                    Description = "Transfer from left AUXes to left FEEDers: 0: Off, 1: Auto, 2: On/Override ",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelTransferPumpRight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/transfer_pump_right",
                    Units = "enum",
                    Description = "Transfer from right AUXes to right FEEDers: 0: Off, 1: Auto, 2: On/Override",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelTransferPumpActivation
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/transfer_pump_activation",
                    Units = "kg",
                    Description = "Automatically transfer from AUXes to FEEDers in auto mode when feeder has more than X kg left to full",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelLevelIndicatedLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_level_indicated_left",
                    Units = "kg",
                    Description = "Indicated fuel level left, shows total or only nacelle tanks depending if user is holding down the aux-tank button.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFuelLevelIndicatedRight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/fuel_level_indicated_right",
                    Units = "kg",
                    Description = "Indicated fuel level right, shows total or only nacelle tanks depending if user is holding down the aux-tank button.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFirewallClosedLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/firewall_closed_left",
                    Units = "boolean",
                    Description = "Firewall valve closed, left",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelFirewallClosedRight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/firewall_closed_right",
                    Units = "boolean",
                    Description = "Firewall valve closed, right",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelAutoCrossfeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/auto_crossfeed",
                    Units = "enum",
                    Description = "0=Off 1=Auto 2=On - If fuel pressure on one side is low, due to fuel pump failure for example, cross-feed is opened to allow one pump to supply pressure to both engines.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelNoTransferLeft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/no_transfer_left",
                    Units = "boolean",
                    Description = "Warning light, will illuminate when transfer from aux to feeder is requested, but aux tank is empty",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelNoTransferRight
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/no_transfer_right",
                    Units = "boolean",
                    Description = "Warning light, will illuminate when transfer from aux to feeder is requested, but aux tank is empty",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelTransferTest
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/transfer_test",
                    Units = "boolean",
                    Description = "Transfer test switch. 0 = normal, -1 = test left, +1 = test right",
                    
                };
            }
        }
        public static DataRefElement Cockpit2FuelTankPumpPressurePsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/fuel/tank_pump_pressure_psi",
                    Units = "psi",
                    Description = "Pressure generated by the fuel pump per tank. If multiple tanks are accesible per the fuel selector, fuel will be consumed from the tanks in order of pump pressure",
                    
                };
            }
        }
    }
}
