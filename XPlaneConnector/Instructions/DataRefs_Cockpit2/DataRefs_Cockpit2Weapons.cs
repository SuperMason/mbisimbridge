﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2WeaponsWeaponSelectConsoleIndex
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/weapon_select_console_index",
                    Units = "index",
                    Description = "(V10 style!) Weapon index selected on the weapon console.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsWeaponSelectConsoleName
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/weapon_select_console_name",
                    Units = "",
                    Description = "",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsFireMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/fire_mode",
                    Units = "enum",
                    Description = "Weapon fire-mode, 0=single, 1=pair, 2=ripple, 3=salvo.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsFireRate
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/fire_rate",
                    Units = "enum",
                    Description = "Weapon fire-rate, 0, 1, 2, 3 depending on fire rate.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsWeaponSelected
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/weapon_selected",
                    Units = "enum",
                    Description = "Switch position for each weapon-arming switch (there can be several). These are the rotaries that can be dialed up or down to select various systems.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsGunOffsetHeadingRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/gun_offset_heading_ratio",
                    Units = "ratio",
                    Description = "Heading offset of the gun from within its maximum heading range",
                    
                };
            }
        }
        public static DataRefElement Cockpit2WeaponsGunOffsetPitchRatio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/weapons/gun_offset_pitch_ratio",
                    Units = "ratio",
                    Description = "Pitch offset of the gun from within its maximum pitch range",
                    
                };
            }
        }
    }
}
