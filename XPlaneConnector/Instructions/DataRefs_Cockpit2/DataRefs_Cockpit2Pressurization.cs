﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2PressurizationActuatorsBleedAirMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/bleed_air_mode",
                    Units = "enum",
                    Description = "Bleed air mode, 0=of, 1=left,2=both,3=right,4=apu,5=auto",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsDumpAllOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/dump_all_on",
                    Units = "boolean",
                    Description = "Dump all pressurization, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsDumpToAltitudeOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/dump_to_altitude_on",
                    Units = "feet",
                    Description = "Dump pressurization to the current altitude, 0 or 1.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsCabinAltitudeFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/cabin_altitude_ft",
                    Units = "feet",
                    Description = "Cabin altitude commanded, feet.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsCabinVviFpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/cabin_vvi_fpm",
                    Units = "feet/minute",
                    Description = "Cabin VVI commanded, feet.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsMaxAllowableAltitudeFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/max_allowable_altitude_ft",
                    Units = "feet",
                    Description = "Maximum allowable altitude for this airplane to maintain the requested cabin altitude.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsAirCondOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/air_cond_on",
                    Units = "boolean",
                    Description = "Electrical air conditioning compressor on, consuming all the amps of rel_HVAC - not needed on airplanes with air cycle machines that drive the air conditioner off the bleed air power itself.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsHeaterOn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/heater_on",
                    Units = "boolean",
                    Description = "Electrical heater grid on, 0 = off, 1 = flight max (consumes rel_HVAC amps), 2 = ground max (consumes 2x rel_HVAC amps, turned off by weight-off-wheels) - not needed on airplanes that are using hot bleed air and have no heaters",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationActuatorsFanSetting
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/actuators/fan_setting",
                    Units = "enum",
                    Description = "Electric fan (vent blower) setting, consuming 0.1 of rel_HVAVC amps when running. 0 = Auto (Runs whenever air_cond_on or heater_on is on), 1 = Low, 2 = High",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationIndicatorsCabinAltitudeFt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/indicators/cabin_altitude_ft",
                    Units = "feet",
                    Description = "Cabin altitude actually occurring, feet.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationIndicatorsCabinVviFpm
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/indicators/cabin_vvi_fpm",
                    Units = "feet/minute",
                    Description = "Cabin VVI actually occurring, fpm.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationIndicatorsPressureDiffentialPsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/indicators/pressure_diffential_psi",
                    Units = "pounds/square_inch",
                    Description = "Cabin differential pressure, psi.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationIndicatorsOutflowValve
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/indicators/outflow_valve",
                    Units = "ratio",
                    Description = "Pressurization outflow valve ratio. 0 for fully closed, 1 for fully open. Writeable with override_pressurization",
                    
                };
            }
        }
        public static DataRefElement Cockpit2PressurizationIndicatorsFanSpeed
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/pressurization/indicators/fan_speed",
                    Units = "ratio",
                    Description = "Electric fan speed, will be 1.0 or 2.0 depending on fan setting or 0 when there's no power or the fan is turned off. Writeable with override_pressurization",
                    
                };
            }
        }
    }
}
