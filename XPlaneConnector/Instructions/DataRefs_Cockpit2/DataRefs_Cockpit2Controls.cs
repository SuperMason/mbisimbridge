﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        private static DataRefElement cockpit2ControlsYokePitchRatio = null;
        /// <summary>
        /// This is how much the user input has deflected the yoke in the cockpit, in ratio, where -1.0 is full down, and 1.0 is full up.
        /// </summary>
        public static DataRefElement Cockpit2ControlsYokePitchRatio {
            get {
                if (cockpit2ControlsYokePitchRatio == null)
                { cockpit2ControlsYokePitchRatio = GetDataRefElement("sim/cockpit2/controls/yoke_pitch_ratio", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "This is how much the user input has deflected the yoke in the cockpit, in ratio, where -1.0 is full down, and 1.0 is full up."); }
                return cockpit2ControlsYokePitchRatio;
            }
        }
        
        
        /// <summary>
        /// This is how much the user input has deflected the yoke in the cockpit, in ratio, where -1.0 is full left, and 1.0 is full right.
        /// </summary>
        public static DataRefElement Cockpit2ControlsYokeRollRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/yoke_roll_ratio", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "This is how much the user input has deflected the yoke in the cockpit, in ratio, where -1.0 is full left, and 1.0 is full right."); } }
        /// <summary>
        /// This is how much the user input has deflected the rudder in the cockpit, in ratio, where -1.0 is full left, and 1.0 is full right.
        /// </summary>
        public static DataRefElement Cockpit2ControlsYokeHeadingRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/yoke_heading_ratio", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "This is how much the user input has deflected the rudder in the cockpit, in ratio, where -1.0 is full left, and 1.0 is full right."); } }
        /// <summary>
        /// Total rudder control input (sum of user pedal plus autopilot servo plus artificial stability)
        /// </summary>
        public static DataRefElement Cockpit2ControlsTotalHeadingRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/total_heading_ratio", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "Total rudder control input (sum of user pedal plus autopilot servo plus artificial stability)"); } }
        /// <summary>
        /// Total pitch control input (sum of user yoke plus autopilot servo plus artificial stability)
        /// </summary>
        public static DataRefElement Cockpit2ControlsTotalPitchRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/total_pitch_ratio", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "Total pitch control input (sum of user yoke plus autopilot servo plus artificial stability)"); } }
        /// <summary>
        /// Total roll control input (sum of user yoke plus autopilot servo plus artificial stability)
        /// </summary>
        public static DataRefElement Cockpit2ControlsTotalRollRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/total_roll_ratio", TypeStart_float, UtilConstants.Flag_Yes, "[-1..1]", "Total roll control input (sum of user yoke plus autopilot servo plus artificial stability)"); } }
        /// <summary>
        /// This is how much the speebrake HANDLE is deflected, in ratio, where 0.0 is fully retracted, 0.5 is halfway down, and 1.0 is fully down, and -0.5 is speedbrakes ARMED.
        /// </summary>
        public static DataRefElement Cockpit2ControlsSpeedbrakeRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/speedbrake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is how much the speebrake HANDLE is deflected, in ratio, where 0.0 is fully retracted, 0.5 is halfway down, and 1.0 is fully down, and -0.5 is speedbrakes ARMED."); } }

        private static DataRefElement cockpit2ControlsWingsweepRatio = null;
        /// <summary>
        /// Requested sweep, in ratio. 0.0 is no sweep requested, 1 is max sweep requested.
        /// </summary>
        public static DataRefElement Cockpit2ControlsWingsweepRatio {
            get {
                if (cockpit2ControlsWingsweepRatio == null)
                { cockpit2ControlsWingsweepRatio = GetDataRefElement("sim/cockpit2/controls/wingsweep_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Requested sweep, in ratio. 0.0 is no sweep requested, 1 is max sweep requested."); }
                return cockpit2ControlsWingsweepRatio;
            }
        }

        private static DataRefElement cockpit2ControlsThrustVectorRatio = null;
        /// <summary>
        /// Requested thrust vector, in ratio. 0.0 is no thrust vector requested, 1 is max thrust vector requested.
        /// </summary>
        public static DataRefElement Cockpit2ControlsThrustVectorRatio {
            get {
                if (cockpit2ControlsThrustVectorRatio == null)
                { cockpit2ControlsThrustVectorRatio = GetDataRefElement("sim/cockpit2/controls/thrust_vector_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Requested thrust vector, in ratio. 0.0 is no thrust vector requested, 1 is max thrust vector requested."); }
                return cockpit2ControlsThrustVectorRatio;
            }
        }

        private static DataRefElement cockpit2ControlsDihedralRatio = null;
        /// <summary>
        /// Requested dihedral, in ratio. 0.0 is no dihedral requested, 1 is max dihedral requested.
        /// </summary>
        public static DataRefElement Cockpit2ControlsDihedralRatio {
            get {
                if (cockpit2ControlsDihedralRatio == null)
                { cockpit2ControlsDihedralRatio = GetDataRefElement("sim/cockpit2/controls/dihedral_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Requested dihedral, in ratio. 0.0 is no dihedral requested, 1 is max dihedral requested."); }
                return cockpit2ControlsDihedralRatio;
            }
        }

        private static DataRefElement cockpit2ControlsIncidenceRatio = null;
        /// <summary>
        /// Requested incidence, in ratio. 0.0 is no incidence requested, 1 is max incidence requested.
        /// </summary>
        public static DataRefElement Cockpit2ControlsIncidenceRatio {
            get {
                if (cockpit2ControlsIncidenceRatio == null)
                { cockpit2ControlsIncidenceRatio = GetDataRefElement("sim/cockpit2/controls/incidence_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Requested incidence, in ratio. 0.0 is no incidence requested, 1 is max incidence requested."); }
                return cockpit2ControlsIncidenceRatio;
            }
        }

        private static DataRefElement cockpit2ControlsWingRetractionRatio = null;
        /// <summary>
        /// Requested wing-retraction, in ratio. 0.0 is no wing-retraction requested, 1 is max wing-retraction requested.
        /// </summary>
        public static DataRefElement Cockpit2ControlsWingRetractionRatio {
            get {
                if (cockpit2ControlsWingRetractionRatio == null)
                { cockpit2ControlsWingRetractionRatio = GetDataRefElement("sim/cockpit2/controls/wing_retraction_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Requested wing-retraction, in ratio. 0.0 is no wing-retraction requested, 1 is max wing-retraction requested."); }
                return cockpit2ControlsWingRetractionRatio;
            }
        }

        private static DataRefElement cockpit2ControlsFlapRatio = null;
        /// <summary>
        /// This is the flap HANDLE location, in ratio, where 0.0 is handle fully retracted, and 1.0 is handle fully extended.
        /// </summary>
        public static DataRefElement Cockpit2ControlsFlapRatio {
            get {
                if (cockpit2ControlsFlapRatio == null)
                { cockpit2ControlsFlapRatio = GetDataRefElement("sim/cockpit2/controls/flap_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the flap HANDLE location, in ratio, where 0.0 is handle fully retracted, and 1.0 is handle fully extended."); }
                return cockpit2ControlsFlapRatio;
            }
        }
        
        /// <summary>
        /// This is the overall brake requested by the parking brake button... 0.0 is none, 1.0 is complete.
        /// </summary>
        public static DataRefElement Cockpit2ControlsParkingBrakeRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/parking_brake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is the overall brake requested by the parking brake button... 0.0 is none, 1.0 is complete."); } }
        /// <summary>
        /// This is additional left brake requested by pedal deflection, 0.0 to 1.0.
        /// </summary>
        public static DataRefElement Cockpit2ControlsLeftBrakeRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/left_brake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is additional left brake requested by pedal deflection, 0.0 to 1.0."); } }
        /// <summary>
        /// This is additional right brake requested by pedal deflection, 0.0 to 1.0.
        /// </summary>
        public static DataRefElement Cockpit2ControlsRightBrakeRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/right_brake_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "This is additional right brake requested by pedal deflection, 0.0 to 1.0."); } }
        /// <summary>
        /// Gear handle position. 0 is up. 1 is down.
        /// </summary>
        public static DataRefElement Cockpit2ControlsGearHandleDown
        { get { return GetDataRefElement("sim/cockpit2/controls/gear_handle_down", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Gear handle position. 0 is up. 1 is down."); } }
        /// <summary>
        /// Is the nosewheel steering turned on? 0 or 1.  This must be off AND the tail wheel must be unlocked to free castor.
        /// </summary>
        public static DataRefElement Cockpit2ControlsNosewheelSteerOn
        { get { return GetDataRefElement("sim/cockpit2/controls/nosewheel_steer_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is the nosewheel steering turned on? 0 or 1.  This must be off AND the tail wheel must be unlocked to free castor."); } }
        /// <summary>
        /// Tail-wheel lockig ratio. 0 for free-castoring, 1 for totally locked.
        /// </summary>
        public static DataRefElement Cockpit2ControlsTailwheelLockRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/tailwheel_lock_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Tail-wheel lockig ratio. 0 for free-castoring, 1 for totally locked."); } }
        /// <summary>
        /// Aileron trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the aileron trim is deflected enough to move the ailerons through 30% of their travel, then that is an aileron trim of 0.3. -1=left 1=right
        /// </summary>
        public static DataRefElement Cockpit2ControlsAileronTrim
        { get { return GetDataRefElement("sim/cockpit2/controls/aileron_trim", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "Aileron trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the aileron trim is deflected enough to move the ailerons through 30% of their travel, then that is an aileron trim of 0.3. -1=left 1=right"); } }
        /// <summary>
        /// Elevator trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the elevator trim is deflected enough to move the elevators through 30% of their travel, then that is an elevator trim of 0.3. -1=down 1=up
        /// </summary>
        public static DataRefElement Cockpit2ControlsElevatorTrim
        { get { return GetDataRefElement("sim/cockpit2/controls/elevator_trim", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "Elevator trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the elevator trim is deflected enough to move the elevators through 30% of their travel, then that is an elevator trim of 0.3. -1=down 1=up"); } }
        /// <summary>
        /// Rudder trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rudder trim is deflected enough to move the rudders through 30% of their travel, then that is an rudder trim of 0.3. -1=left 1=right
        /// </summary>
        public static DataRefElement Cockpit2ControlsRudderTrim
        { get { return GetDataRefElement("sim/cockpit2/controls/rudder_trim", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "Rudder trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rudder trim is deflected enough to move the rudders through 30% of their travel, then that is an rudder trim of 0.3. -1=left 1=right"); } }
        /// <summary>
        /// Rotor trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rotor trim is deflected enough to move the rotor through 30% of its travel, then that is a rotor trim of 0.3. -1=down 1=up
        /// </summary>
        public static DataRefElement Cockpit2ControlsRotorTrim
        { get { return GetDataRefElement("sim/cockpit2/controls/rotor_trim", TypeStart_float, UtilConstants.Flag_Yes, "-1..1", "Rotor trim, in part of MAX FLIGHT CONTROL DEFLECTION. So, if the rotor trim is deflected enough to move the rotor through 30% of its travel, then that is a rotor trim of 0.3. -1=down 1=up"); } }
        /// <summary>
        /// Deployment of the water rudder, 0 is none, 1 is max
        /// </summary>
        public static DataRefElement Cockpit2ControlsWaterRudderHandleRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/water_rudder_handle_ratio", TypeStart_float, UtilConstants.Flag_Yes, "ratio", "Deployment of the water rudder, 0 is none, 1 is max"); } }
        /// <summary>
        /// This is how far the indicator of flap deployment has moved in the cockpit.
        /// </summary>
        public static DataRefElement Cockpit2ControlsFlapHandleDeployRatio
        { get { return GetDataRefElement("sim/cockpit2/controls/flap_handle_deploy_ratio", TypeStart_float, UtilConstants.Flag_No, "ratio", "This is how far the indicator of flap deployment has moved in the cockpit."); } }
    }
}
