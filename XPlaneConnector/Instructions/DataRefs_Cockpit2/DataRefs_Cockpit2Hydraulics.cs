﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement Cockpit2HydraulicsIndicatorsHydraulicPressure1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/hydraulics/indicators/hydraulic_pressure_1",
                    Units = "any",
                    Description = "Hydraulic system 1 pressure, units set by Plane-Maker.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2HydraulicsIndicatorsHydraulicPressure2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/hydraulics/indicators/hydraulic_pressure_2",
                    Units = "any",
                    Description = "Hydraulic system 2 pressure, units set by Plane-Maker.",
                    
                };
            }
        }
        public static DataRefElement Cockpit2HydraulicsIndicatorsHydraulicFluidRatio1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/hydraulics/indicators/hydraulic_fluid_ratio_1",
                    Units = "ratio",
                    Description = "Hydraulic fluid quantity, ratio of max, system 1",
                    
                };
            }
        }
        public static DataRefElement Cockpit2HydraulicsIndicatorsHydraulicFluidRatio2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/cockpit2/hydraulics/indicators/hydraulic_fluid_ratio_2",
                    Units = "ratio",
                    Description = "Hydraulic fluid quantity, ratio of max, system 2",
                    
                };
            }
        }
    }
}
