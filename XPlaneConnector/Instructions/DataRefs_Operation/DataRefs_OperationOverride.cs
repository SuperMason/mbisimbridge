﻿using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        public static DataRefElement OperationOverrideOverrideJoystick
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_joystick",
                    Units = "boolean",
                    Description = "Override control of the joystick deflections (overrides stick, yoke, pedals, keys, mouse, and auto-coordination)",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideArtstab
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_artstab",
                    Units = "boolean",
                    Description = "Override control of the artificial stability system",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFlightcontrol
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_flightcontrol",
                    Units = "boolean",
                    Description = "Override all parts of the flight system at once",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideGearbrake
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_gearbrake",
                    Units = "boolean",
                    Description = "Override gear and brake status",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverridePlanepath
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_planepath",
                    Units = "boolean",
                    Description = "Override position updates of this plane",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverridePlaneAiAutopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_plane_ai_autopilot",
                    Units = "boolean",
                    Description = "Override the AI's control of the plane via the autopilot",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideNavneedles
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_navneedles",
                    Units = "boolean",
                    Description = "Override navcom radios",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideNav1Needles
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_nav1_needles",
                    Units = "boolean",
                    Description = "Override nav1 receiver",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideNav2Needles
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_nav2_needles",
                    Units = "boolean",
                    Description = "Override nav2 receiver",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideAdf
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_adf",
                    Units = "boolean",
                    Description = "Override ADF radios",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_dme",
                    Units = "boolean",
                    Description = "Override DME distances",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideGps
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_gps",
                    Units = "boolean",
                    Description = "Override GPS computer",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideNavHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_nav_heading",
                    Units = "boolean",
                    Description = "Override raw heading flown by nav (for GPS that fly by roll commands)",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFlightdir
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_flightdir",
                    Units = "boolean",
                    Description = "Override flight director needles (both axes)",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFlightdirPtch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_flightdir_ptch",
                    Units = "boolean",
                    Description = "Override flight director needles (pitch only)",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFlightdirRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_flightdir_roll",
                    Units = "boolean",
                    Description = "Override flight director needles (roll only)",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideCamera
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_camera",
                    Units = "boolean",
                    Description = "Override camera control.  NOTE: DO NOT USE, USE XPLMCAMERA!!",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideAnnunciators
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_annunciators",
                    Units = "boolean",
                    Description = "Override annunciators",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideAutopilot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_autopilot",
                    Units = "boolean",
                    Description = "Override the autopilot's brains",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideJoystickHeading
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_joystick_heading",
                    Units = "boolean",
                    Description = "Override just heading (yaw) control (disables auto-coordination). Use yoke_heading_ratio.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideJoystickPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_joystick_pitch",
                    Units = "boolean",
                    Description = "Override just pitch control. Use yoke_pitch_ratio.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideJoystickRoll
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_joystick_roll",
                    Units = "boolean",
                    Description = "Override just roll control. Use yoke_roll_ratio.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideThrottles
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_throttles",
                    Units = "boolean",
                    Description = "Override the throttles. Use ENGN_thro_use to control them.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverridePropPitch
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_prop_pitch",
                    Units = "boolean",
                    Description = "Override the prop pitch.  Use POINT_pitch_deg_use to edit.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverridePropMode
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_prop_mode",
                    Units = "boolean",
                    Description = "Override the prop mode.  Use ENGN_propmode to edit.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideMixture
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_mixture",
                    Units = "boolean",
                    Description = "Override the mixture controls.  Use ENGN_mixt to edit.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideGroundplane
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_groundplane",
                    Units = "boolean",
                    Description = "Override ground interactions (see sim/flightmodel/ground) - in v11 this was gone until 11.30.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFmsAdvance
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_fms_advance",
                    Units = "boolean",
                    Description = "Override the FMS going to the next waypoint.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFuelFlow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_fuel_flow",
                    Units = "boolean",
                    Description = "overrides fuel flow variable sim/flightmodel/engine/ENGN_FF_",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideIttEgt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_itt_egt",
                    Units = "boolean",
                    Description = "overrides engine temp vars sim/flightmodel/engine/ENGN_EGT_c and ENGN_ITT_c",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideChtOilt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_cht_oilt",
                    Units = "boolean",
                    Description = "overrides engine temp vars sim/flightmodel/engine/ENGN_CHT_c and ENGN_oilT_c",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideIas
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_ias",
                    Units = "boolean",
                    Description = "overrides indicated airspeed dataref, so it is no longer update 1:1 from calibrated airspeed. Plugin can introduce installation error to indicated airspeed",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideControlSurfaces
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_control_surfaces",
                    Units = "boolean",
                    Description = "overrides individual control surfaces, e.g. sim/flightmodel/controls/lail1def",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideEngines
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_engines",
                    Units = "boolean",
                    Description = "overrides all engine calculations - write to LMN and g_nrml/side/axil.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideTorqueMotors
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_torque_motors",
                    Units = "boolean",
                    Description = "overrides all engine calculations but not the prop - write the torque your motor generates while the prop will still be simulated by X-Plane",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideForces
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_forces",
                    Units = "boolean",
                    Description = "overrides all force calculations - write to LMN and g_nrml/side/axil.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideWingForces
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_wing_forces",
                    Units = "boolean",
                    Description = "overrides all wing calculations - write to aero LMN and g_nrml/side/axil.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideEngineForces
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_engine_forces",
                    Units = "boolean",
                    Description = "overrides all engine calculations - write to prop LMN and g_nrml/side/axil.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideGearForces
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_gear_forces",
                    Units = "boolean",
                    Description = "overrides all gear calculations - write to gear LMN and g_nrml/side/axil.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideBoats
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_boats",
                    Units = "boolean",
                    Description = "overrides speed, heading, and rocking of boats. index 0=carrier, 1=frigate",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideClouds
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_clouds",
                    Units = "boolean",
                    Description = "overrides building and drawing of clouds as well as white-out-in-cloud effects",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideWheelSteer
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_wheel_steer",
                    Units = "boolean",
                    Description = "overrides the steering of individual gear from tiller/rudder.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideAirportLites
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_airport_lites",
                    Units = "boolean",
                    Description = "overrides when the airport lites go on and off.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideFuelSystem
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_fuel_system",
                    Units = "boolean",
                    Description = "this override turns off transfer and dump and lets the plugin decide if the engine gets fuel",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideTCAS
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_TCAS",
                    Units = "boolean",
                    Description = "this override lets third party add-ons write to the TCAS source data array so you can have plugin controlled instances as TCAS targets. Only writeable by the plugin that has the AI planes acquired.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideMultiplayerMapLayer
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_multiplayer_map_layer",
                    Units = "boolean",
                    Description = "this override lets third party add-ons turn off X-Plane's map layer that shows icons for other planes. This allows the plugin to supply their own custom map layer and not conflict with X-Plane. Only writeable by the plugin that has also set override_TCAS.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideToeBrakes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_toe_brakes",
                    Units = "boolean",
                    Description = "this override gives plugins control of sim/cockpit2/controls/left_brake_ratio and right_brake_ratio",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideGroundTrucks
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_ground_trucks",
                    Units = "boolean",
                    Description = "set this override to disable Austin's trucks.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideTimestep
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_timestep",
                    Units = "boolean",
                    Description = "this overrides the sim's time step.  when enabled, xtim.prd_sim, sim/operation/misc/frame_rate_period is plugin-writable",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverridePressurization
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_pressurization",
                    Units = "boolean",
                    Description = "override the sims pressurization control, so a plugin can control cabin pressurization itself.",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideOxygenSystem
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_oxygen_system",
                    Units = "boolean",
                    Description = "override the sims crew and passenger oxygen system, so a plugin can control the oxygen system itself",
                    
                };
            }
        }
        public static DataRefElement OperationOverrideOverrideSlungLoadSize
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/override/override_slung_load_size",
                    Units = "boolean",
                    Description = "override slung load size calculation - makes sim/flightmodel/misc/jett_size writable.",
                    
                };
            }
        }
    }
}
