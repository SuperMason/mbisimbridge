﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Enable logging of network data
        /// </summary>
        public static DataRefElement OperationMiscDebugNetwork
        { get { return GetDataRefElement("sim/operation/misc/debug_network", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Enable logging of network data"); } }
        /// <summary>
        /// The frame rate period. Use the reciprocal to get the frame rate (e.g. 1/mnw.prd).  Writable via override_timestep in 11.10
        /// </summary>
        public static DataRefElement OperationMiscFrameRatePeriod
        { get { return GetDataRefElement("sim/operation/misc/frame_rate_period", TypeStart_float, UtilConstants.Flag_Yes, "secs", "The frame rate period. Use the reciprocal to get the frame rate (e.g. 1/mnw.prd).  Writable via override_timestep in 11.10"); } }
        /// <summary>
        /// This is how close XP time matches real time. Ideal ratio is 1.  NOTE: in 930 and later time ratio is always 1.0.
        /// </summary>
        public static DataRefElement OperationMiscTimeRatio
        { get { return GetDataRefElement("sim/operation/misc/time_ratio", TypeStart_float, UtilConstants.Flag_No, "secs", "This is how close XP time matches real time. Ideal ratio is 1.  NOTE: in 930 and later time ratio is always 1.0."); } }
    }
}