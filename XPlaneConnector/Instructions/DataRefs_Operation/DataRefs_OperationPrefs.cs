﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Start up with the plane running?
        /// </summary>
        public static DataRefElement OperationPrefsStartupRunning
        { get { return GetDataRefElement("sim/operation/prefs/startup_running", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Start up with the plane running?"); } }
        /// <summary>
        /// Warn if we exceed max airframe speed
        /// </summary>
        public static DataRefElement OperationPrefsWarnOverspeed
        { get { return GetDataRefElement("sim/operation/prefs/warn_overspeed", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Warn if we exceed max airframe speed"); } }
        /// <summary>
        /// Warn if we exceed max g-forces on aircraft
        /// </summary>
        public static DataRefElement OperationPrefsWarnOvergforce
        { get { return GetDataRefElement("sim/operation/prefs/warn_overgforce", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Warn if we exceed max g-forces on aircraft"); } }
        /// <summary>
        /// Warn if we exceed max flap extended speed
        /// </summary>
        public static DataRefElement OperationPrefsWarnOverspeedFlaps
        { get { return GetDataRefElement("sim/operation/prefs/warn_overspeed_flaps", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Warn if we exceed max flap extended speed"); } }
        /// <summary>
        /// Warn if we exceed max gear deployed speed
        /// </summary>
        public static DataRefElement OperationPrefsWarnOverspeedGear
        { get { return GetDataRefElement("sim/operation/prefs/warn_overspeed_gear", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Warn if we exceed max gear deployed speed"); } }
        /// <summary>
        /// On crash, do we reset the AC to the nearest airport? - gone in v11, read only dref returns false for compatibility.
        /// </summary>
        public static DataRefElement OperationPrefsResetOnCrash
        { get { return GetDataRefElement("sim/operation/prefs/reset_on_crash", TypeStart_int, UtilConstants.Flag_No, "boolean", "On crash, do we reset the AC to the nearest airport? - gone in v11, read only dref returns false for compatibility."); } }
        /// <summary>
        /// Show text warning for otherwise hard to see things like carb-icing?
        /// </summary>
        public static DataRefElement OperationPrefsWarnNonobviousStuff
        { get { return GetDataRefElement("sim/operation/prefs/warn_nonobvious_stuff", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Show text warning for otherwise hard to see things like carb-icing?"); } }
        /// <summary>
        /// show text ATC messages?
        /// </summary>
        public static DataRefElement OperationPrefsTextOut
        { get { return GetDataRefElement("sim/operation/prefs/text_out", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "show text ATC messages?"); } }
        /// <summary>
        /// Are we in replay mode?
        /// </summary>
        public static DataRefElement OperationPrefsReplayMode
        { get { return GetDataRefElement("sim/operation/prefs/replay_mode", TypeStart_int, UtilConstants.Flag_Yes, "enum", "Are we in replay mode?"); } }
        /// <summary>
        /// Controls whether the AI controls the user's plane
        /// </summary>
        public static DataRefElement OperationPrefsAiFliesAircraft
        { get { return GetDataRefElement("sim/operation/prefs/ai_flies_aircraft", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Controls whether the AI controls the user's plane"); } }
        /// <summary>
        /// True if the user has enabled the experimental flight model.
        /// </summary>
        public static DataRefElement OperationPrefsUsingExperimentalFm
        { get { return GetDataRefElement("sim/operation/prefs/using_experimental_fm", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if the user has enabled the experimental flight model."); } }
        /// <summary>
        /// Current language
        /// </summary>
        public static DataRefElement OperationPrefsMiscLanguage
        { get { return GetDataRefElement("sim/operation/prefs/misc/language", TypeStart_int, UtilConstants.Flag_No, "enum", "Current language"); } }
        /// <summary>
        /// Returns true if the sim provides a low-mem lua allocator via inter-plugin messaging.
        /// </summary>
        public static DataRefElement OperationPrefsMiscHasLuaAlloc
        { get { return GetDataRefElement("sim/operation/prefs/misc/has_lua_alloc", TypeStart_int, UtilConstants.Flag_No, "bool", "Returns true if the sim provides a low-mem lua allocator via inter-plugin messaging."); } }
    }
}