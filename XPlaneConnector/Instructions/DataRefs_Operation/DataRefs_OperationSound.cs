﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Does this machine have sound hardware that X-Plane understands?
        /// </summary>
        public static DataRefElement OperationSoundHasSound
        { get { return GetDataRefElement("sim/operation/sound/has_sound", TypeStart_int, UtilConstants.Flag_No, "boolean", "Does this machine have sound hardware that X-Plane understands?"); } }
        /// <summary>
        /// Does this machine have speech synth capabilities?
        /// </summary>
        public static DataRefElement OperationSoundHasSpeechSynth
        { get { return GetDataRefElement("sim/operation/sound/has_speech_synth", TypeStart_int, UtilConstants.Flag_No, "boolean", "Does this machine have speech synth capabilities?"); } }
        /// <summary>
        /// Is sound on (set by user)
        /// </summary>
        public static DataRefElement OperationSoundSoundOn
        { get { return GetDataRefElement("sim/operation/sound/sound_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is sound on (set by user)"); } }
        /// <summary>
        /// Is speech synth on (set by user)
        /// </summary>
        public static DataRefElement OperationSoundSpeechOn
        { get { return GetDataRefElement("sim/operation/sound/speech_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Is speech synth on (set by user)"); } }
        /// <summary>
        /// Master Volume
        /// </summary>
        public static DataRefElement OperationSoundMasterVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/master_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Master Volume"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundExteriorVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/exterior_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundInteriorVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/interior_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundCopilotVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/copilot_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for the com radios, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundRadioVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/radio_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for the com radios, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundEnviroVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/enviro_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundUiVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/ui_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundEngineVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/engine_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for engine sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for propeller sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundPropVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/prop_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for propeller sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for ground contact effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundGroundVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/ground_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for ground contact effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for weather sound effects, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundWeatherVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/weather_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for weather sound effects, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for warning systems, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundWarningVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/warning_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for warning systems, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// Volume level for the avionics fan, ratio from 0 (silent) to 1 (max loudness)
        /// </summary>
        public static DataRefElement OperationSoundFanVolumeRatio
        { get { return GetDataRefElement("sim/operation/sound/fan_volume_ratio", TypeStart_float, UtilConstants.Flag_Yes, "0-1", "Volume level for the avionics fan, ratio from 0 (silent) to 1 (max loudness)"); } }
        /// <summary>
        /// 1 if we are in this space, 0 if outside, and a fraction as we transition through the borer region
        /// </summary>
        public static DataRefElement OperationSoundInsideRatio
        { get { return GetDataRefElement("sim/operation/sound/inside_ratio", TypeStart_float64, UtilConstants.Flag_No, "0-1", "1 if we are in this space, 0 if outside, and a fraction as we transition through the borer region"); } }
        /// <summary>
        /// True if the camera is inside ANY of the defined sound volumes for the aircraft.
        /// </summary>
        public static DataRefElement OperationSoundInsideAny
        { get { return GetDataRefElement("sim/operation/sound/inside_any", TypeStart_int, UtilConstants.Flag_No, "boolean", "True if the camera is inside ANY of the defined sound volumes for the aircraft."); } }
        /// <summary>
        /// Canopy position: 0 = closed, 1 = open - these ALWAYS reflect the USER's plane (even when used on an AI) so they are appropriate for SOUND MIXING.
        /// </summary>
        public static DataRefElement OperationSoundUsersCanopyOpenRatio
        { get { return GetDataRefElement("sim/operation/sound/users_canopy_open_ratio", TypeStart_float, UtilConstants.Flag_No, "ratio", "Canopy position: 0 = closed, 1 = open - these ALWAYS reflect the USER's plane (even when used on an AI) so they are appropriate for SOUND MIXING."); } }
        /// <summary>
        /// How open is the door, 0 = closed, 1 = open - these ALWAYS reflect the USER's plane (even when used on an AI) so they are appropriate for SOUND MIXING.
        /// </summary>
        public static DataRefElement OperationSoundUsersDoorOpenRatio
        { get { return GetDataRefElement("sim/operation/sound/users_door_open_ratio", TypeStart_float10, UtilConstants.Flag_No, "ratio", "How open is the door, 0 = closed, 1 = open - these ALWAYS reflect the USER's plane (even when used on an AI) so they are appropriate for SOUND MIXING."); } }
    }
}