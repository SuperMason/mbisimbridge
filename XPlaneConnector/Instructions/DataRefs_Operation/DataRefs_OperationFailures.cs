﻿using MBILibrary.Util;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Instructions
{
    public sealed partial class DataRefs
    {
        /// <summary>
        /// Hydraulic pressure ratio
        /// </summary>
        public static DataRefElement OperationFailuresHydraulicPressureRatio
        { get { return GetDataRefElement("sim/operation/failures/hydraulic_pressure_ratio", TypeStart_float, UtilConstants.Flag_No, "ratio", "Hydraulic pressure ratio"); } }
        /// <summary>
        /// Hydraulic pressure ratio
        /// </summary>
        public static DataRefElement OperationFailuresHydraulicPressureRatio2
        { get { return GetDataRefElement("sim/operation/failures/hydraulic_pressure_ratio2", TypeStart_float, UtilConstants.Flag_No, "ratio", "Hydraulic pressure ratio"); } }
        /// <summary>
        /// Oil power or thrust ratio
        /// </summary>
        public static DataRefElement OperationFailuresOilPowerThrustRatio
        { get { return GetDataRefElement("sim/operation/failures/oil_power_thrust_ratio", TypeStart_float8, UtilConstants.Flag_No, "ratio", "Oil power or thrust ratio"); } }
        /// <summary>
        /// Enable random failures based on mean time between failures
        /// </summary>
        public static DataRefElement OperationFailuresEnableRandomFalures
        { get { return GetDataRefElement("sim/operation/failures/enable_random_falures", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Enable random failures based on mean time between failures"); } }
        /// <summary>
        /// Enable random failures basde on mean time between failures
        /// </summary>
        public static DataRefElement OperationFailuresEnableRandomFailures
        { get { return GetDataRefElement("sim/operation/failures/enable_random_failures", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Enable random failures basde on mean time between failures"); } }
        /// <summary>
        /// Mean time between failures
        /// </summary>
        public static DataRefElement OperationFailuresMeanTimeBetweenFailureHrs
        { get { return GetDataRefElement("sim/operation/failures/mean_time_between_failure_hrs", TypeStart_float, UtilConstants.Flag_Yes, "hours", "Mean time between failures"); } }
        /// <summary>
        /// Ram air turbine is on?
        /// </summary>
        public static DataRefElement OperationFailuresRamAirTurbineOn
        { get { return GetDataRefElement("sim/operation/failures/ram_air_turbine_on", TypeStart_int, UtilConstants.Flag_Yes, "boolean", "Ram air turbine is on?"); } }
        /// <summary>
        /// 564 separate failure codes [was 137]
        /// </summary>
        public static DataRefElement OperationFailuresFailures
        { get { return GetDataRefElement("sim/operation/failures/failures", TypeStart_int564, UtilConstants.Flag_No, "enum", "564 separate failure codes [was 137]"); } }
        /// <summary>
        /// Controls locked
        /// </summary>
        public static DataRefElement OperationFailuresRelConlock
        { get { return GetDataRefElement("sim/operation/failures/rel_conlock", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Controls locked"); } }
        /// <summary>
        /// Door Open
        /// </summary>
        public static DataRefElement OperationFailuresRelDoorOpen
        { get { return GetDataRefElement("sim/operation/failures/rel_door_open", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Door Open"); } }
        /// <summary>
        /// External power is on
        /// </summary>
        public static DataRefElement OperationFailuresRelExPowerOn
        { get { return GetDataRefElement("sim/operation/failures/rel_ex_power_on", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "External power is on"); } }
        /// <summary>
        /// Passenger oxygen on
        /// </summary>
        public static DataRefElement OperationFailuresRelPassO2On
        { get { return GetDataRefElement("sim/operation/failures/rel_pass_o2_on", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Passenger oxygen on"); } }
        /// <summary>
        /// Fuel Cap left off
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelcap
        { get { return GetDataRefElement("sim/operation/failures/rel_fuelcap", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel Cap left off"); } }
        /// <summary>
        /// Water in fuel
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelWater
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_water", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Water in fuel"); } }
        /// <summary>
        /// Wrong fuel type - JetA for props or Avgas for jets!
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelType
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_type", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Wrong fuel type - JetA for props or Avgas for jets!"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 1
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock0
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block0", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 1"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 2
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock1
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block1", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 2"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 3
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock2
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block2", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 3"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 4
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock3
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block3", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 4"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 5
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock4
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block4", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 5"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 6
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock5
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block5", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 6"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 7
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock6
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block6", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 7"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 8
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock7
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block7", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 8"); } }
        /// <summary>
        /// Fuel tank filter is blocked - tank 9
        /// </summary>
        public static DataRefElement OperationFailuresRelFuelBlock8
        { get { return GetDataRefElement("sim/operation/failures/rel_fuel_block8", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Fuel tank filter is blocked - tank 9"); } }
        /// <summary>
        /// VASIs Inoperative
        /// </summary>
        public static DataRefElement OperationFailuresRelVasi
        { get { return GetDataRefElement("sim/operation/failures/rel_vasi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "VASIs Inoperative"); } }
        /// <summary>
        /// Runway lites Inoperative
        /// </summary>
        public static DataRefElement OperationFailuresRelRwyLites
        { get { return GetDataRefElement("sim/operation/failures/rel_rwy_lites", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Runway lites Inoperative"); } }
        /// <summary>
        /// Bird has hit the plane
        /// </summary>
        public static DataRefElement OperationFailuresRelBirdStrike
        { get { return GetDataRefElement("sim/operation/failures/rel_bird_strike", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Bird has hit the plane"); } }
        /// <summary>
        /// Wind shear/microburst
        /// </summary>
        public static DataRefElement OperationFailuresRelWindShear
        { get { return GetDataRefElement("sim/operation/failures/rel_wind_shear", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Wind shear/microburst"); } }
        /// <summary>
        /// Smoke in cockpit
        /// </summary>
        public static DataRefElement OperationFailuresRelSmokeCpit
        { get { return GetDataRefElement("sim/operation/failures/rel_smoke_cpit", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Smoke in cockpit"); } }
        /// <summary>
        /// Induce dusty brown-out
        /// </summary>
        public static DataRefElement OperationFailuresRelBrownOut
        { get { return GetDataRefElement("sim/operation/failures/rel_brown_out", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Induce dusty brown-out"); } }
        /// <summary>
        /// Electrical System (Bus 1)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys
        { get { return GetDataRefElement("sim/operation/failures/rel_esys", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 1)"); } }
        /// <summary>
        /// Electrical System (Bus 2)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys2
        { get { return GetDataRefElement("sim/operation/failures/rel_esys2", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 2)"); } }
        /// <summary>
        /// Electrical System (Bus 3)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys3
        { get { return GetDataRefElement("sim/operation/failures/rel_esys3", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 3)"); } }
        /// <summary>
        /// Electrical System (Bus 4)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys4
        { get { return GetDataRefElement("sim/operation/failures/rel_esys4", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 4)"); } }
        /// <summary>
        /// Electrical System (Bus 5)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys5
        { get { return GetDataRefElement("sim/operation/failures/rel_esys5", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 5)"); } }
        /// <summary>
        /// Electrical System (Bus 6)
        /// </summary>
        public static DataRefElement OperationFailuresRelEsys6
        { get { return GetDataRefElement("sim/operation/failures/rel_esys6", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Electrical System (Bus 6)"); } }
        /// <summary>
        /// Inverter 1
        /// </summary>
        public static DataRefElement OperationFailuresRelInvert0
        { get { return GetDataRefElement("sim/operation/failures/rel_invert0", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Inverter 1"); } }
        /// <summary>
        /// Inverter 2 (also in 740)
        /// </summary>
        public static DataRefElement OperationFailuresRelInvert1
        { get { return GetDataRefElement("sim/operation/failures/rel_invert1", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Inverter 2 (also in 740)"); } }
        /// <summary>
        /// Generator 0 voltage low
        /// </summary>
        public static DataRefElement OperationFailuresRelGen0Lo
        { get { return GetDataRefElement("sim/operation/failures/rel_gen0_lo", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Generator 0 voltage low"); } }
        /// <summary>
        /// Generator 0 voltage hi
        /// </summary>
        public static DataRefElement OperationFailuresRelGen0Hi
        { get { return GetDataRefElement("sim/operation/failures/rel_gen0_hi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Generator 0 voltage hi"); } }
        /// <summary>
        /// Generator 1 voltage low
        /// </summary>
        public static DataRefElement OperationFailuresRelGen1Lo
        { get { return GetDataRefElement("sim/operation/failures/rel_gen1_lo", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Generator 1 voltage low"); } }
        /// <summary>
        /// Generator 1 voltage hi
        /// </summary>
        public static DataRefElement OperationFailuresRelGen1Hi
        { get { return GetDataRefElement("sim/operation/failures/rel_gen1_hi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Generator 1 voltage hi"); } }
        /// <summary>
        /// Battery 0 voltage low
        /// </summary>
        public static DataRefElement OperationFailuresRelBat0Lo
        { get { return GetDataRefElement("sim/operation/failures/rel_bat0_lo", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Battery 0 voltage low"); } }
        /// <summary>
        /// Battery 0 voltage hi
        /// </summary>
        public static DataRefElement OperationFailuresRelBat0Hi
        { get { return GetDataRefElement("sim/operation/failures/rel_bat0_hi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Battery 0 voltage hi"); } }
        /// <summary>
        /// Battery 1 voltage low
        /// </summary>
        public static DataRefElement OperationFailuresRelBat1Lo
        { get { return GetDataRefElement("sim/operation/failures/rel_bat1_lo", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Battery 1 voltage low"); } }
        /// <summary>
        /// Battery 1 voltage hi
        /// </summary>
        public static DataRefElement OperationFailuresRelBat1Hi
        { get { return GetDataRefElement("sim/operation/failures/rel_bat1_hi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Battery 1 voltage hi"); } }
        /// <summary>
        /// Nav lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesNav
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_nav", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Nav lights"); } }
        /// <summary>
        /// Strobe lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesStrobe
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_strobe", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Strobe lights"); } }
        /// <summary>
        /// Beacon lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesBeac
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_beac", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Beacon lights"); } }
        /// <summary>
        /// Taxi lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesTaxi
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_taxi", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Taxi lights"); } }
        /// <summary>
        /// Landing Lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesLand
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_land", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Landing Lights"); } }
        /// <summary>
        /// Instrument Lighting
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesIns
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_ins", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Instrument Lighting"); } }
        /// <summary>
        /// Cockpit Lights
        /// </summary>
        public static DataRefElement OperationFailuresRelClights
        { get { return GetDataRefElement("sim/operation/failures/rel_clights", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Cockpit Lights"); } }
        /// <summary>
        /// HUD lights
        /// </summary>
        public static DataRefElement OperationFailuresRelLitesHud
        { get { return GetDataRefElement("sim/operation/failures/rel_lites_hud", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "HUD lights"); } }
        /// <summary>
        /// Stability Augmentation
        /// </summary>
        public static DataRefElement OperationFailuresRelStbaug
        { get { return GetDataRefElement("sim/operation/failures/rel_stbaug", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Stability Augmentation"); } }
        /// <summary>
        /// autopilot servos failed - rudder/yaw damper
        /// </summary>
        public static DataRefElement OperationFailuresRelServoRudd
        { get { return GetDataRefElement("sim/operation/failures/rel_servo_rudd", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "autopilot servos failed - rudder/yaw damper"); } }
        /// <summary>
        /// AutoPilot (Computer)
        /// </summary>
        public static DataRefElement OperationFailuresRelOtto
        { get { return GetDataRefElement("sim/operation/failures/rel_otto", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "AutoPilot (Computer)"); } }
        /// <summary>
        /// AutoPilot (Runaway!!!)
        /// </summary>
        public static DataRefElement OperationFailuresRelAutoRunaway
        { get { return GetDataRefElement("sim/operation/failures/rel_auto_runaway", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "AutoPilot (Runaway!!!)"); } }
        /// <summary>
        /// AutoPilot (Servos)
        /// </summary>
        public static DataRefElement OperationFailuresRelAutoServos
        { get { return GetDataRefElement("sim/operation/failures/rel_auto_servos", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "AutoPilot (Servos)"); } }
        /// <summary>
        /// autopilot servos failed - ailerons
        /// </summary>
        public static DataRefElement OperationFailuresRelServoAiln
        { get { return GetDataRefElement("sim/operation/failures/rel_servo_ailn", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "autopilot servos failed - ailerons"); } }
        /// <summary>
        /// autopilot servos failed - elevators
        /// </summary>
        public static DataRefElement OperationFailuresRelServoElev
        { get { return GetDataRefElement("sim/operation/failures/rel_servo_elev", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "autopilot servos failed - elevators"); } }
        /// <summary>
        /// autopilot servos failed - throttles
        /// </summary>
        public static DataRefElement OperationFailuresRelServoThro
        { get { return GetDataRefElement("sim/operation/failures/rel_servo_thro", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "autopilot servos failed - throttles"); } }
        /// <summary>
        /// Yaw Left Control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcRudL
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_rud_L", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Yaw Left Control"); } }
        /// <summary>
        /// Yaw Right control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcRudR
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_rud_R", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Yaw Right control"); } }
        /// <summary>
        /// Roll left control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcAilL
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_ail_L", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Roll left control"); } }
        /// <summary>
        /// Roll Right Control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcAilR
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_ail_R", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Roll Right Control"); } }
        /// <summary>
        /// Pitch Up Control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcElvU
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_elv_U", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Pitch Up Control"); } }
        /// <summary>
        /// Pitch Down Control
        /// </summary>
        public static DataRefElement OperationFailuresRelFcElvD
        { get { return GetDataRefElement("sim/operation/failures/rel_fc_elv_D", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Pitch Down Control"); } }
        /// <summary>
        /// Yaw Trim
        /// </summary>
        public static DataRefElement OperationFailuresRelTrimRud
        { get { return GetDataRefElement("sim/operation/failures/rel_trim_rud", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Yaw Trim"); } }
        /// <summary>
        /// roll trim
        /// </summary>
        public static DataRefElement OperationFailuresRelTrimAil
        { get { return GetDataRefElement("sim/operation/failures/rel_trim_ail", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "roll trim"); } }
        /// <summary>
        /// Pitch Trim
        /// </summary>
        public static DataRefElement OperationFailuresRelTrimElv
        { get { return GetDataRefElement("sim/operation/failures/rel_trim_elv", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Pitch Trim"); } }
        /// <summary>
        /// Yaw Trim Runaway
        /// </summary>
        public static DataRefElement OperationFailuresRelRudTrimRun
        { get { return GetDataRefElement("sim/operation/failures/rel_rud_trim_run", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Yaw Trim Runaway"); } }
        /// <summary>
        /// Pitch Trim Runaway
        /// </summary>
        public static DataRefElement OperationFailuresRelAilTrimRun
        { get { return GetDataRefElement("sim/operation/failures/rel_ail_trim_run", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Pitch Trim Runaway"); } }
        /// <summary>
        /// Elevator Trim Runaway
        /// </summary>
        public static DataRefElement OperationFailuresRelElvTrimRun
        { get { return GetDataRefElement("sim/operation/failures/rel_elv_trim_run", TypeStart_int, UtilConstants.Flag_Yes, "failure_enum", "Elevator Trim Runaway"); } }
        public static DataRefElement OperationFailuresRelFcSlt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fc_slt",
                    Units = "failure_enum",
                    Description = "Slats",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFlapAct
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_flap_act",
                    Units = "failure_enum",
                    Description = "Flap Actuator",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFcLFlp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fc_L_flp",
                    Units = "failure_enum",
                    Description = "Left flap activate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFcRFlp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fc_R_flp",
                    Units = "failure_enum",
                    Description = "Right Flap activate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLFlpOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_L_flp_off",
                    Units = "failure_enum",
                    Description = "Left flap remove",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRFlpOff
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_R_flp_off",
                    Units = "failure_enum",
                    Description = "Right flap remove",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGearAct
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gear_act",
                    Units = "failure_enum",
                    Description = "Landing Gear actuator",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGearInd
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gear_ind",
                    Units = "failure_enum",
                    Description = "Landing Gear indicator",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLbrakes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lbrakes",
                    Units = "failure_enum",
                    Description = "Left Brakes",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRbrakes
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_rbrakes",
                    Units = "failure_enum",
                    Description = "Right Brakes",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLagear1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lagear1",
                    Units = "failure_enum",
                    Description = "Landing Gear 1 retract",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLagear2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lagear2",
                    Units = "failure_enum",
                    Description = "Landing Gear 2 retract",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLagear3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lagear3",
                    Units = "failure_enum",
                    Description = "Landing Gear 3 retract",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLagear4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lagear4",
                    Units = "failure_enum",
                    Description = "Landing Gear 4 retract",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLagear5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_lagear5",
                    Units = "failure_enum",
                    Description = "Landing Gear 5 retract",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse1",
                    Units = "failure_enum",
                    Description = "Landing gear 1 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse2",
                    Units = "failure_enum",
                    Description = "Landing gear 2 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse3",
                    Units = "failure_enum",
                    Description = "Landing gear 3 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse4",
                    Units = "failure_enum",
                    Description = "Landing gear 4 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse5",
                    Units = "failure_enum",
                    Description = "Landing gear 5 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse6",
                    Units = "failure_enum",
                    Description = "Landing gear 6 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse7",
                    Units = "failure_enum",
                    Description = "Landing gear 7 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse8",
                    Units = "failure_enum",
                    Description = "Landing gear 8 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse9
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse9",
                    Units = "failure_enum",
                    Description = "Landing gear 9 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCollapse10
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_collapse10",
                    Units = "failure_enum",
                    Description = "Landing gear 10 gear collapse",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTire1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_tire1",
                    Units = "failure_enum",
                    Description = "Landing gear 1 tire blowout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTire2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_tire2",
                    Units = "failure_enum",
                    Description = "Landing gear 2 tire blowout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTire3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_tire3",
                    Units = "failure_enum",
                    Description = "Landing gear 3 tire blowout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTire4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_tire4",
                    Units = "failure_enum",
                    Description = "Landing gear 4 tire blowout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTire5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_tire5",
                    Units = "failure_enum",
                    Description = "Landing gear 5 tire blowout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHVAC
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_HVAC",
                    Units = "failure_enum",
                    Description = "air conditioning failed",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBleedAirLft
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_bleed_air_lft",
                    Units = "failure_enum",
                    Description = "The left engine is not providing enough pressure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBleedAirRgt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_bleed_air_rgt",
                    Units = "failure_enum",
                    Description = "The right engine is not providing enough pressure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAPUPress
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_APU_press",
                    Units = "failure_enum",
                    Description = "APU is not providing bleed air for engine start or pressurization",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelDepresSlow
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_depres_slow",
                    Units = "failure_enum",
                    Description = "Slow cabin leak - descend or black out",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelDepresFast
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_depres_fast",
                    Units = "failure_enum",
                    Description = "catastrophic decompression - yer dead",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmpEle
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp_ele",
                    Units = "failure_enum",
                    Description = "Hydraulic pump (electric)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp",
                    Units = "failure_enum",
                    Description = "Hydraulic System 1 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp2",
                    Units = "failure_enum",
                    Description = "Hydraulic System 2 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp3",
                    Units = "failure_enum",
                    Description = "Hydraulic System 3 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp4",
                    Units = "failure_enum",
                    Description = "Hydraulic System 4 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp5",
                    Units = "failure_enum",
                    Description = "Hydraulic System 5 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp6",
                    Units = "failure_enum",
                    Description = "Hydraulic System 6 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp7",
                    Units = "failure_enum",
                    Description = "Hydraulic System 7 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydpmp8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydpmp8",
                    Units = "failure_enum",
                    Description = "Hydraulic System 8 (pump fail)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydleak
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydleak",
                    Units = "failure_enum",
                    Description = "Hydraulic System 1 (leak)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydleak2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydleak2",
                    Units = "failure_enum",
                    Description = "Hydraulic System 2 (leak)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydoverp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydoverp",
                    Units = "failure_enum",
                    Description = "Hydraulic System 1 (over pressure)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHydoverp2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hydoverp2",
                    Units = "failure_enum",
                    Description = "Hydraulic System 2 (over pressure)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelThrotLo
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_throt_lo",
                    Units = "failure_enum",
                    Description = "Throttle inop giving min thrust",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelThrotHi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_throt_hi",
                    Units = "failure_enum",
                    Description = "Throttle inop giving max thrust",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFcThr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fc_thr",
                    Units = "failure_enum",
                    Description = "Throttle failure at current position",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPropSync
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prop_sync",
                    Units = "failure_enum",
                    Description = "Prop sync",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFeather
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_feather",
                    Units = "failure_enum",
                    Description = "Autofeather system inop",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTrotor
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_trotor",
                    Units = "failure_enum",
                    Description = "Tail rotor transmission",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAntice
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_antice",
                    Units = "failure_enum",
                    Description = "Anti-ice",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceDetect
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_detect",
                    Units = "failure_enum",
                    Description = "Ice detector",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePitotHeat1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_pitot_heat1",
                    Units = "failure_enum",
                    Description = "Pitot heat 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePitotHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_pitot_heat2",
                    Units = "failure_enum",
                    Description = "Pitot heat 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceStaticHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_static_heat",
                    Units = "failure_enum",
                    Description = "Static heat 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceStaticHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_static_heat2",
                    Units = "failure_enum",
                    Description = "Static heat 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceAOAHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_AOA_heat",
                    Units = "failure_enum",
                    Description = "AOA indicator heat",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceAOAHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_AOA_heat2",
                    Units = "failure_enum",
                    Description = "AOA indicator heat",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceWindowHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_window_heat",
                    Units = "failure_enum",
                    Description = "Window Heat",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceSurfBoot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_surf_boot",
                    Units = "failure_enum",
                    Description = "Surface Boot - Deprecated - Do Not Use",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceSurfHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_surf_heat",
                    Units = "failure_enum",
                    Description = "Surface Heat",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceSurfHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_surf_heat2",
                    Units = "failure_enum",
                    Description = "Surface Heat",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceBrakeHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_brake_heat",
                    Units = "failure_enum",
                    Description = "Brakes anti-ice",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceAltAir1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_alt_air1",
                    Units = "failure_enum",
                    Description = "Alternate Air",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceAltAir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_alt_air2",
                    Units = "failure_enum",
                    Description = "Alternate Air",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelVacuum
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_vacuum",
                    Units = "failure_enum",
                    Description = "Vacuum System 1 - Pump Failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelVacuum2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_vacuum2",
                    Units = "failure_enum",
                    Description = "Vacuum System 2 - Pump Failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelElecGyr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_elec_gyr",
                    Units = "failure_enum",
                    Description = "Electric gyro system 1 - gyro motor Failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelElecGyr2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_elec_gyr2",
                    Units = "failure_enum",
                    Description = "Electric gyro system 2 - gyro motor Failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPitot
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pitot",
                    Units = "failure_enum",
                    Description = "Pitot 1 - Blockage",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPitot2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pitot2",
                    Units = "failure_enum",
                    Description = "Pitot 2 - Blockage",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStatic
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_static",
                    Units = "failure_enum",
                    Description = "Static 1 - Blockage",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStatic2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_static2",
                    Units = "failure_enum",
                    Description = "Static 2 - Blockage",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStatic1Err
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_static1_err",
                    Units = "failure_enum",
                    Description = "Static system 1 - Error",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStatic2Err
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_static2_err",
                    Units = "failure_enum",
                    Description = "Static system 2 - Error",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGOat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_oat",
                    Units = "failure_enum",
                    Description = "OAT",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGFuel
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_fuel",
                    Units = "failure_enum",
                    Description = "fuel quantity",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsAsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_asi",
                    Units = "failure_enum",
                    Description = "Airspeed Indicator (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsAhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_ahz",
                    Units = "failure_enum",
                    Description = "Artificial Horizon (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsAlt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_alt",
                    Units = "failure_enum",
                    Description = "Altimeter (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsTsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_tsi",
                    Units = "failure_enum",
                    Description = "Turn indicator (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsDgy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_dgy",
                    Units = "failure_enum",
                    Description = "Directional Gyro (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSsVvi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ss_vvi",
                    Units = "failure_enum",
                    Description = "Vertical Velocity Indicator (Pilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopAsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_asi",
                    Units = "failure_enum",
                    Description = "Airspeed Indicator (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopAhz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_ahz",
                    Units = "failure_enum",
                    Description = "Artificial Horizon (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopAlt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_alt",
                    Units = "failure_enum",
                    Description = "Altimeter (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopTsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_tsi",
                    Units = "failure_enum",
                    Description = "Turn indicator (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopDgy
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_dgy",
                    Units = "failure_enum",
                    Description = "Directional Gyro (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCopVvi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_cop_vvi",
                    Units = "failure_enum",
                    Description = "Vertical Velocity Indicator (Copilot)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEfis1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_efis_1",
                    Units = "failure_enum",
                    Description = "Primary EFIS",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEfis2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_efis_2",
                    Units = "failure_enum",
                    Description = "Secondary EFIS",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAOA
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_AOA",
                    Units = "failure_enum",
                    Description = "AOA",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStallWarn
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_stall_warn",
                    Units = "failure_enum",
                    Description = "Stall warning has failed",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGearWarning
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gear_warning",
                    Units = "failure_enum",
                    Description = "gear warning audio is broken",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelNavcom1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_navcom1",
                    Units = "failure_enum",
                    Description = "Nav and com 1 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelNavcom2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_navcom2",
                    Units = "failure_enum",
                    Description = "Nav and com 2 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelNav1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_nav1",
                    Units = "failure_enum",
                    Description = "Nav-1 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelNav2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_nav2",
                    Units = "failure_enum",
                    Description = "Nav-2 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCom1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_com1",
                    Units = "failure_enum",
                    Description = "Com-1 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCom2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_com2",
                    Units = "failure_enum",
                    Description = "Com-2 radio",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAdf1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_adf1",
                    Units = "failure_enum",
                    Description = "ADF 1 (only one ADF failure in 830!)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAdf2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_adf2",
                    Units = "failure_enum",
                    Description = "ADF 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGps
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gps",
                    Units = "failure_enum",
                    Description = "GPS",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGps2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gps2",
                    Units = "failure_enum",
                    Description = "GPS",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelDme
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_dme",
                    Units = "failure_enum",
                    Description = "DME",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelLoc
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_loc",
                    Units = "failure_enum",
                    Description = "Localizer",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGls
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gls",
                    Units = "failure_enum",
                    Description = "Glide Slope",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gp",
                    Units = "failure_enum",
                    Description = "WAAS GPS receiver",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelXpndr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_xpndr",
                    Units = "failure_enum",
                    Description = "Transponder failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMarker
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_marker",
                    Units = "failure_enum",
                    Description = "Marker Beacons",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRPMInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_RPM_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - rpm engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRPMInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_RPM_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - rpm engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelN1Ind0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_N1_ind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - n1 engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelN1Ind1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_N1_ind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - n1 engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelN2Ind0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_N2_ind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - n2 engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelN2Ind1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_N2_ind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - n2 engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMPInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_MP_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - mp engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMPInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_MP_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - mp engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTRQind0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_TRQind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - trq engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelTRQind1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_TRQind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - trq engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEPRind0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_EPRind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - epr engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEPRind1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_EPRind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - epr engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCHTInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_CHT_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - cht engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelCHTInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_CHT_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - cht engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelITTind0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ITTind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - itt engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelITTind1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ITTind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - itt engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEGTInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_EGT_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - egt engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEGTInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_EGT_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - egt engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFFInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_FF_ind0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - ff engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFFInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_FF_ind1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - ff engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFpInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fp_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Fuel Pressure 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFpInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fp_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Fuel Pressure 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilp_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Oil Pressure 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilp_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Oil Pressure 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOiltInd0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilt_ind_0",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Oil Temperature 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOiltInd1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilt_ind_1",
                    Units = "failure_enum",
                    Description = "Panel Indicator Inop - Oil Temperature 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelG430Gps1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g430_gps1",
                    Units = "failure_enum",
                    Description = "G430 GPS 1 Inop",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelG430Gps2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g430_gps2",
                    Units = "failure_enum",
                    Description = "G430 GPS 2 Inop",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelG430Rad1Tune
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g430_rad1_tune",
                    Units = "failure_enum",
                    Description = "G430 Nav/Com Tuner 1 Inop",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelG430Rad2Tune
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g430_rad2_tune",
                    Units = "failure_enum",
                    Description = "G430 Nav/Com Tuner 2 Inop",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGGia1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_gia1",
                    Units = "failure_enum",
                    Description = "GIA 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGGia2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_gia2",
                    Units = "failure_enum",
                    Description = "GIA 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGGea
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_gea",
                    Units = "failure_enum",
                    Description = "gea",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAdcComp
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_adc_comp",
                    Units = "failure_enum",
                    Description = "air data computer",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGArthorz
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_arthorz",
                    Units = "failure_enum",
                    Description = "AHRS",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGAsi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_asi",
                    Units = "failure_enum",
                    Description = "airspeed",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGAlt
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_alt",
                    Units = "failure_enum",
                    Description = "altimeter",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGMagmtr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_magmtr",
                    Units = "failure_enum",
                    Description = "magnetometer",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGVvi
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_vvi",
                    Units = "failure_enum",
                    Description = "vvi",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGNavrad1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_navrad1",
                    Units = "failure_enum",
                    Description = "nav radio 1 - removed from 10.00 - 10.36, compatibility only in 10.40 - DO NOT USE - use rel_nav1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGNavrad2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_navrad2",
                    Units = "failure_enum",
                    Description = "nav radio 2 - removed from 10.00 - 10.36, compatibility only in 10.40 - DO NOT USE - use rel_nav2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGComrad1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_comrad1",
                    Units = "failure_enum",
                    Description = "com radio 1 - removed from 10.00 - 10.36, compatibility only in 10.40 - DO NOT USE - use rel_com1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGComrad2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_comrad2",
                    Units = "failure_enum",
                    Description = "com radio 2 - removed from 10.00 - 10.36, compatibility only in 10.40 - DO NOT USE - use rel_com2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGXpndr
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_xpndr",
                    Units = "failure_enum",
                    Description = "transponder removed from 10.00 - 10.36, compatibility only in 10.40+ - DO NOT USE - use rel_xpndr",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGGen1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_gen1",
                    Units = "failure_enum",
                    Description = "generator amperage 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGGen2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_gen2",
                    Units = "failure_enum",
                    Description = "generator amperage 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGBat1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_bat1",
                    Units = "failure_enum",
                    Description = "battery voltage 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGBat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_bat2",
                    Units = "failure_enum",
                    Description = "battery voltage 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGBus1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_bus1",
                    Units = "failure_enum",
                    Description = "bus voltage 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGBus2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_bus2",
                    Units = "failure_enum",
                    Description = "bus voltage 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGMfd
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_mfd",
                    Units = "failure_enum",
                    Description = "MFD screen failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGPfd
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_pfd",
                    Units = "failure_enum",
                    Description = "PFD screen failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGPfd2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_g_pfd2",
                    Units = "failure_enum",
                    Description = "PFD2 screen failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT0",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT1",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT2",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT3",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT4",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT5",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT6",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagLFT7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magLFT7",
                    Units = "failure_enum",
                    Description = "Left Magneto Fail - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT0",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT1",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT2",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT3",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT4",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT5",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT6",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMagRGT7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_magRGT7",
                    Units = "failure_enum",
                    Description = "Right Magneto Fail - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir0",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 1 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir1",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 2 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir2",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 3 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir3",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 4 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir4",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 5 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir5",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 6 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir6",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 7 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfir7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfir7",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 8 Fire",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla0",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 1 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla1",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 2 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla2",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 3 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla3",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 4 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla4",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 5 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla5",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 6 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla6",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 7 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfla7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfla7",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 8 Flameout",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai0",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 1 loss of power without smoke",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai1",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai2",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai3",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai4",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai5",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai6",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngfai7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engfai7",
                    Units = "failure_enum",
                    Description = "Engine Failure - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep0",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep1",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep2",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep3",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep4",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep5",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep6",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngsep7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_engsep7",
                    Units = "failure_enum",
                    Description = "Engine Separation - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp0",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 1 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp1",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 2 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp2",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 3 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp3",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 4 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp4",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 5 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp5",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 6 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp6",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 7 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuepmp7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuepmp7",
                    Units = "failure_enum",
                    Description = "Fuel Pump Inop - engine 8 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp0",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 1 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp1",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 2 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp2",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 3 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp3",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 4 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp4",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 5 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp5",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 6 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp6",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 7 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEleFuepmp7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ele_fuepmp7",
                    Units = "failure_enum",
                    Description = "Fuel Pump - engine 8 (electric driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo0",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 1 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo1",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 2 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo2",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 3 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo3",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 4 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo4",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 5 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo5",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 6 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo6",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 7 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelEngLo7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_eng_lo7",
                    Units = "failure_enum",
                    Description = "Fuel Pump Low Pressure - engine 8 (engine driven)",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres0",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres1",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres2",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres3",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres4",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres5",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres6",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAirres7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_airres7",
                    Units = "failure_enum",
                    Description = "Airflow restricted - Engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl0",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl1",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl2",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl3",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl4",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl5",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl6",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFuelfl7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fuelfl7",
                    Units = "failure_enum",
                    Description = "Fuel Flow Fluctuation - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta0",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta1",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta2",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta3",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta4",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta5",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta6",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelComsta7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_comsta7",
                    Units = "failure_enum",
                    Description = "Engine Compressor Stall - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr0",
                    Units = "failure_enum",
                    Description = "Starter - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr1",
                    Units = "failure_enum",
                    Description = "Starter - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr2",
                    Units = "failure_enum",
                    Description = "Starter - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr3",
                    Units = "failure_enum",
                    Description = "Starter - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr4",
                    Units = "failure_enum",
                    Description = "Starter - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr5",
                    Units = "failure_enum",
                    Description = "Starter - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr6",
                    Units = "failure_enum",
                    Description = "Starter - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelStartr7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_startr7",
                    Units = "failure_enum",
                    Description = "Starter - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr0",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr1",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr2",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr3",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr4",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr5",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr6",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIgnitr7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ignitr7",
                    Units = "failure_enum",
                    Description = "Ignitor - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta0",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 0",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta1",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta2",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta3",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta4",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta5",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta6",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHunsta7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hunsta7",
                    Units = "failure_enum",
                    Description = "Hung Start - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz0",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz1",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz2",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz3",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz4",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz5",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz6",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelClonoz7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_clonoz7",
                    Units = "failure_enum",
                    Description = "Hung start (clogged nozzles) - Engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta0",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 0",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta1",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta2",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta3",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta4",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta5",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta6",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHotsta7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hotsta7",
                    Units = "failure_enum",
                    Description = "Hot Start - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT0",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT1",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT2",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT3",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT4",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT5",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT6",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRunITT7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_runITT7",
                    Units = "failure_enum",
                    Description = "Runway Hot ITT - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera0",
                    Units = "failure_enum",
                    Description = "Generator - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera1",
                    Units = "failure_enum",
                    Description = "Generator - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera2",
                    Units = "failure_enum",
                    Description = "Generator - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera3",
                    Units = "failure_enum",
                    Description = "Generator - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera4",
                    Units = "failure_enum",
                    Description = "Generator - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera5",
                    Units = "failure_enum",
                    Description = "Generator - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera6",
                    Units = "failure_enum",
                    Description = "Generator - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenera7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_genera7",
                    Units = "failure_enum",
                    Description = "Generator - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter0",
                    Units = "failure_enum",
                    Description = "Battery 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter1",
                    Units = "failure_enum",
                    Description = "Battery 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter2",
                    Units = "failure_enum",
                    Description = "Battery 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter3",
                    Units = "failure_enum",
                    Description = "Battery 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter4",
                    Units = "failure_enum",
                    Description = "Battery 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter5",
                    Units = "failure_enum",
                    Description = "Battery 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter6",
                    Units = "failure_enum",
                    Description = "Battery 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelBatter7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_batter7",
                    Units = "failure_enum",
                    Description = "Battery 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_0",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_1",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_2",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_3",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_4",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_5",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_6",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGovnr7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_govnr_7",
                    Units = "failure_enum",
                    Description = "Governor throttle fail - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_0",
                    Units = "failure_enum",
                    Description = "Fadec - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_1",
                    Units = "failure_enum",
                    Description = "Fadec - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_2",
                    Units = "failure_enum",
                    Description = "Fadec - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_3",
                    Units = "failure_enum",
                    Description = "Fadec - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_4",
                    Units = "failure_enum",
                    Description = "Fadec - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_5",
                    Units = "failure_enum",
                    Description = "Fadec - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_6",
                    Units = "failure_enum",
                    Description = "Fadec - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelFadec7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_fadec_7",
                    Units = "failure_enum",
                    Description = "Fadec - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp0",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp1",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp2",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp3",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp4",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp5",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp6",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelOilpmp7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_oilpmp7",
                    Units = "failure_enum",
                    Description = "Oil Pump - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde0",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde1",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde2",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde3",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde4",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde5",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde6",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelChipde7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_chipde7",
                    Units = "failure_enum",
                    Description = "Chip Detected - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin0",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin1",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin2",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin3",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin4",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin5",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin6",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpfin7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpfin7",
                    Units = "failure_enum",
                    Description = "Prop governor fail to fine - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs0",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs1",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs2",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs3",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs4",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs5",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs6",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPrpcrs7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_prpcrs7",
                    Units = "failure_enum",
                    Description = "Prop governor fail to coarse - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft0",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft1",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft2",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft3",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft4",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft5",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft6",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPshaft7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pshaft7",
                    Units = "failure_enum",
                    Description = "Drive Shaft - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_0",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_1",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_2",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_3",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_4",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_5",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_6",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelSeize7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_seize_7",
                    Units = "failure_enum",
                    Description = "Engine Seize - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers0",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers1",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers2",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers3",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers4",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers5",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers6",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevers7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revers7",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Inop - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep0",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep1",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep2",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep3",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep4",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep5",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep6",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevdep7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revdep7",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Deploy - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc0",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc1",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc2",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc3",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc4",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc5",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc6",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelRevloc7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_revloc7",
                    Units = "failure_enum",
                    Description = "Thrust Reversers Locked - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur0
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur0",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur1",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur2",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur3",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur4",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur5",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur6",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelAftbur7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_aftbur7",
                    Units = "failure_enum",
                    Description = "Afterburners - engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat2",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat3",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat4",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat5",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat6",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat7",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIceInletHeat8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_inlet_heat8",
                    Units = "failure_enum",
                    Description = "Inlet heat, engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat2",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat3",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat4",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat5",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 5",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat6",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 6",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat7",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 7",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelIcePropHeat8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_ice_prop_heat8",
                    Units = "failure_enum",
                    Description = "Prop Heat, engine 8",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing1L
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing1L",
                    Units = "failure_enum",
                    Description = "Wing separations - left wing 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing1R
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing1R",
                    Units = "failure_enum",
                    Description = "Wing separations - right wing 1",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing2L
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing2L",
                    Units = "failure_enum",
                    Description = "Wing separations - left wing 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing2R
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing2R",
                    Units = "failure_enum",
                    Description = "Wing separations - right wing 2",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing3L
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing3L",
                    Units = "failure_enum",
                    Description = "Wing separations - left wing 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing3R
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing3R",
                    Units = "failure_enum",
                    Description = "Wing separations - right wing 3",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing4L
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing4L",
                    Units = "failure_enum",
                    Description = "Wing separations - left wing 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelWing4R
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_wing4R",
                    Units = "failure_enum",
                    Description = "Wing separations - right wing 4",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHstbL
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hstbL",
                    Units = "failure_enum",
                    Description = "Left horizontal stabilizer separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelHstbR
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_hstbR",
                    Units = "failure_enum",
                    Description = "Right horizontal stabilizer separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelVstb1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_vstb1",
                    Units = "failure_enum",
                    Description = "Vertical stabilizer 1 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelVstb2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_vstb2",
                    Units = "failure_enum",
                    Description = "Vertical stabilizer 2 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing1
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing1",
                    Units = "failure_enum",
                    Description = "Misc wing 1 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing2
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing2",
                    Units = "failure_enum",
                    Description = "Misc wing 2 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing3
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing3",
                    Units = "failure_enum",
                    Description = "Misc wing 3 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing4
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing4",
                    Units = "failure_enum",
                    Description = "Misc wing 4 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing5
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing5",
                    Units = "failure_enum",
                    Description = "Misc wing 5 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing6
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing6",
                    Units = "failure_enum",
                    Description = "Misc wing 6 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing7
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing7",
                    Units = "failure_enum",
                    Description = "Misc wing 7 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelMwing8
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_mwing8",
                    Units = "failure_enum",
                    Description = "Misc wing 8 separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl1a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl1a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 1a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl2a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl2a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 2a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl3a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl3a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 3a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl4a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl4a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 4a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl5a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl5a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 5a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl6a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl6a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 6a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl7a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl7a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 7a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl8a
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl8a",
                    Units = "failure_enum",
                    Description = "Engine Pylon 8a Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl1b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl1b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 1b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl2b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl2b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 2b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl3b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl3b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 3b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl4b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl4b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 4b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl5b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl5b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 5b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl6b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl6b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 6b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl7b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl7b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 7b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelPyl8b
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_pyl8b",
                    Units = "failure_enum",
                    Description = "Engine Pylon 8b Separate",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenEsys
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gen_esys",
                    Units = "failure_enum",
                    Description = "General electrical failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelGenAvio
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_gen_avio",
                    Units = "failure_enum",
                    Description = "General avionics bus failure",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelApu
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_apu",
                    Units = "failure_enum",
                    Description = "APU failure to start or run",
                    
                };
            }
        }
        public static DataRefElement OperationFailuresRelApuFire
        {
            get
            {
                return new DataRefElement
                {
                    DataRef = "sim/operation/failures/rel_apu_fire",
                    Units = "failure_enum",
                    Description = "APU catastrophic failure with fire",
                    
                };
            }
        }

    }
}
