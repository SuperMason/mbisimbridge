﻿using MBILibrary.Enums;
using MBILibrary.Util;
using System;
using System.Globalization;
using System.Linq;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.Models
{
    public class ArrayDrVal
    {
        public delegate void CallBackFunc(DataRefElementBase srcDR, XPlaneResponseStatusEnum xPlaneResponseStatus, int[] intAry, float[] floatAry, double[] doubleAry);
        private CallBackFunc OnValueChange = null;

        /// <summary>
        /// 傳入 Air Manager 的 callback function
        /// </summary>
        public void AMcallbackFunction(CallBackFunc callBackFunc) => OnValueChange = callBackFunc;

        /// <summary>
        /// 原始的 DataRefElement
        /// </summary>
        public DataRefElementBase DrBase = null;
        /// <summary>
        /// 填寫 int[] 的每一個 index value
        /// </summary>
        public int[] IntAryVal = null;
        /// <summary>
        /// 填寫 float[] 的每一個 index value
        /// </summary>
        public float[] FloatAryVal = null;
        /// <summary>
        /// 填寫 double[] 的每一個 index value
        /// </summary>
        public double[] DoubleAryVal = null;

        /// <summary>
        /// 紀錄已經接收到的 value, 及其所屬位置 index
        /// <para>一旦坐滿 ArrayLen 定義的所有位置, 就會馬上 fire 給上層</para>
        /// </summary>
        private bool[] IdxChecked = null;
        /// <summary>
        /// 是否已經取得全部的 n 個 value ? 所有位置都已經有 value 更新上去了 ?
        /// <para>滿足條件依據 ArrayLen 定義的數量</para>
        /// </summary>
        public bool IsAllCompletelyReceived {
            get {
                if (UtilGlobalFunc.IsArrayEmpty(IdxChecked)) { return false; }

                int NotReceivedCNT = IdxChecked.Where(b => !b).Count();
                //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "NotReceivedCNT : {0}, '{1}' ArrayLen({2})", NotReceivedCNT, DrBase.DataRef, DrBase.ArrayLen));
                return NotReceivedCNT == 0; // 全部 index value 皆到齊了... 可以 fireEvent
            }
        }
        /// <summary>
        /// X-Plane 的回覆狀態, Fully? or Partial?
        /// </summary>
        public XPlaneResponseStatusEnum xPlaneResponseStatus => IsAllCompletelyReceived ? XPlaneResponseStatusEnum.Fully : XPlaneResponseStatusEnum.Partial;

        /// <summary>
        /// 此 Array DataRef Value 距離上一次最後更新的時間, 是否有超過自定義的最慢更新時間限制 1 秒 ?
        /// <para>最慢每 1 秒把資料刷出去, 如果資料在 1 秒內全部到齊, 就會提前執行</para>
        /// </summary>
        protected internal bool IsOverMaxAge => Age > MaxAge;
        /// <summary>
        /// 此 DataRef 最後的更新時間
        /// </summary>
        public DateTime LastUpdateTime { get; protected set; }
        /// <summary>
        /// 該 DataRef 是否需要更新 value 了 ?
        /// <para>與 MaxAge 比較</para>
        /// </summary>
        private TimeSpan Age => DateTime.Now - LastUpdateTime;
        /// <summary>
        /// 最多等待 1 秒
        /// </summary>
        private static TimeSpan MaxAge => TimeSpan.FromMilliseconds(1000);


        public ArrayDrVal(DataRefElementBase drBase) {
            DrBase = drBase;
            InitialValue();
        }
        /// <summary>
        /// 清空儲存的字串, 重新初始化設定
        /// </summary>
        private void InitialValue() {
            IdxChecked = new bool[DrBase.ArrayLen];
            if (DrBase.IsArrayIntType) { IntAryVal = new int[DrBase.ArrayLen]; }
            else if (DrBase.IsArrayFloatType) { FloatAryVal = new float[DrBase.ArrayLen]; }
            else if (DrBase.IsArrayDoubleType) { DoubleAryVal = new double[DrBase.ArrayLen]; }
        }
        /// <summary>
        /// 依據 index, 將 value 填入適當的位置
        /// </summary>
        public void FillinValue(int index, object value) {
            if (value == null) { return; }

            if (!IdxChecked[index]) {   // 此 index 尚未更新過, 所以進入更新
                IdxChecked[index] = true;
                if (DrBase.IsArrayIntType && value.ValueIsInt()) { if (index < IntAryVal.Length) { LastUpdateTime = DateTime.Now; IntAryVal[index] = value.ConvertObjectToInt32(); } }
                else if (DrBase.IsArrayFloatType && value.ValueIsFloat()) { if (index < FloatAryVal.Length) { LastUpdateTime = DateTime.Now; FloatAryVal[index] = value.ConvertObjectToFloat(); } }
                else if (DrBase.IsArrayDoubleType && value.ValueIsDouble()) { if (index < DoubleAryVal.Length) { LastUpdateTime = DateTime.Now; DoubleAryVal[index] = value.ConvertObjectToDouble(); } }
            }
            
            if (IsAllCompletelyReceived || IsOverMaxAge) {
                // 全部 index value 皆到齊了... 可以 fireEvent
                OnValueChange?.Invoke(DrBase, xPlaneResponseStatus, IntAryVal, FloatAryVal, DoubleAryVal); // data 有更新, 同步通知 "Air Manager"
                InitialValue();// 已經送出值, 所以清空, 等待下一次更新
            }
        }
    }
}
