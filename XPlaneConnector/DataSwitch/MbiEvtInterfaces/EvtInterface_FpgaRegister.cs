﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// FPGA Register 寫入
        /// </summary>
        public void OnWriteRegister(object serder, MessageEventArgs e) { WriteRegister?.Invoke(serder, e); }
        /// <summary>
        /// FPGA Register 讀出
        /// </summary>
        public void OnReadRegister(object serder, MessageEventArgs e) { ReadRegister?.Invoke(serder, e); }
        /// <summary>
        /// RegisterWrote Session updated, 完成整個 Register Wrote 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public void OnWriteRegisterSessionUpdated(object serder, MessageEventArgs e) { WriteRegisterSessionUpdated?.Invoke(serder, e); }
        /// <summary>
        /// RegisterReaded Session updated, 完成整個 Register Readed 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public void OnReadRegisterSessionUpdated(object serder, MessageEventArgs e) { ReadRegisterSessionUpdated?.Invoke(serder, e); }
        #endregion

        #region event definition
        /// <summary>
        /// FPGA Register 寫入
        /// </summary>
        public EventHandler<MessageEventArgs> WriteRegister;
        /// <summary>
        /// FPGA Register 讀出
        /// </summary>
        public EventHandler<MessageEventArgs> ReadRegister;
        /// <summary>
        /// RegisterWrote Session updated, 完成整個 Register Wrote 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public EventHandler<MessageEventArgs> WriteRegisterSessionUpdated;
        /// <summary>
        /// RegisterReaded Session updated, 完成整個 Register Readed 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public EventHandler<MessageEventArgs> ReadRegisterSessionUpdated;
        #endregion
    }
}