﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// 啟動空戰系統
        /// </summary>
        public void OnAircraftDataRefRead(object serder, DataRefEventArgs e) { AircraftDataRefRead?.Invoke(serder, e); }
        #endregion

        #region event definition
        /// <summary>
        /// 啟動空戰系統
        /// </summary>
        public EventHandler<DataRefEventArgs> AircraftDataRefRead; // event fire 後, 沒人承接處理
        #endregion
    }
}