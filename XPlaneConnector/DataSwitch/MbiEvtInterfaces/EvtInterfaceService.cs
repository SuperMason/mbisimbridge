﻿using System;
using System.Threading;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    /// <summary>
    /// All Events Interface Switcher
    /// </summary>
    public sealed partial class EvtInterfaceService
    {
        #region constructor
        /// <summary>
        /// All Events Interface Switcher
        /// </summary>
        public EvtInterfaceService()
        { ResetVerificationCode(); }
        #endregion


        #region properties
        public string VerificationCode { get; private set; } = string.Empty;
        #endregion


        #region function()
        public void ResetVerificationCode()
        { VerificationCode = (new Random()).Next(100000, 999999).ToString(); }
        #endregion


        #region 需要等待過程的 Events
        /// <summary>
        /// 等待 WebSocket 的連線結果 :: 成功? 失敗?
        /// <para>開始 ::: WaitOne()</para>
        /// <para>結束 ::: Set()</para>
        /// </summary>
        public EventWaitHandle OnWSConnecting = new AutoResetEvent(false);
        /// <summary>
        /// 等待 "註冊結果" :: 已註冊? 未註冊?
        /// <para>開始 ::: WaitOne()</para>
        /// <para>結束 ::: Set()</para>
        /// </summary>
        public EventWaitHandle OnCheckingHostRegistrationStatus = new AutoResetEvent(false);
        /// <summary>
        /// 等待 "執行註冊" :: 成功? 失敗?
        /// <para>開始 ::: WaitOne()</para>
        /// <para>結束 ::: Set()</para>
        /// </summary>
        public EventWaitHandle OnSigningUp = new AutoResetEvent(false);
        /// <summary>
        /// 等待 "解除註冊" :: 成功? 失敗?
        /// <para>開始 ::: WaitOne()</para>
        /// <para>結束 ::: Set()</para>
        /// </summary>
        public EventWaitHandle OnUnSigningUp = new AutoResetEvent(false);
        #endregion
    }
}