﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        #region 已經完成 xxxxxx, 所以紀錄在 cloud 上
        /// <summary>
        /// 已經完成將 Register Write to FPGA, 所以紀錄在 cloud 上
        /// </summary>
        public void OnRegisterWrote(object serder, MessageEventArgs e) { RegisterWrote?.Invoke(serder, e); }
        /// <summary>
        /// 已經完成將 Register Read from FPGA, 所以紀錄在 cloud 上
        /// </summary>
        public void OnRegisterReaded(object serder, MessageEventArgs e) { RegisterReaded?.Invoke(serder, e); }
        /// <summary>
        /// 已經完成將 Test Pattern 打至螢幕上, 所以紀錄在 cloud 上
        /// </summary>
        public void OnTestPatternWrote(object serder, MessageEventArgs e) { TestPatternWrote?.Invoke(serder, e); }
        #endregion

        #region 控制 Session 作業
        /// <summary>
        /// 有作業正在執行, 所以捨棄掉該次 Request, 並記錄在 cloud 上
        /// </summary>
        public void OnAbandonSession(object serder, MessageEventArgs e) { AbandonSession?.Invoke(serder, e); }
        #endregion

        #region 進行 Host 註冊 解註冊
        /// <summary>
        /// 由 RESTClientService 去問是否已註冊 ?
        /// </summary>
        public void OnCheckHostRegistrationStatus(object serder, MessageEventArgs e) { CheckHostRegistrationStatus?.Invoke(serder, e); }
        /// <summary>
        /// 執行註冊
        /// </summary>
        public void OnSignup(object serder, MessageEventArgs e) { Signup?.Invoke(serder, e); }
        /// <summary>
        /// 執行解除註冊
        /// </summary>
        public void OnUnSignup(object serder, MessageEventArgs e) { UnSignup?.Invoke(serder, e); }
        #endregion
        #endregion

        #region event definition
        #region 已經完成 xxxxxx, 所以紀錄在 cloud 上
        /// <summary>
        /// 已經完成將 Register Write to FPGA, 所以紀錄在 cloud 上
        /// </summary>
        public EventHandler<MessageEventArgs> RegisterWrote;
        /// <summary>
        /// 已經完成將 Register Read from FPGA, 所以紀錄在 cloud 上
        /// </summary>
        public EventHandler<MessageEventArgs> RegisterReaded;
        /// <summary>
        /// 已經完成將 Test Pattern 打至螢幕上, 所以紀錄在 cloud 上
        /// </summary>
        public EventHandler<MessageEventArgs> TestPatternWrote;
        #endregion

        #region 控制 Session 作業
        /// <summary>
        /// 有作業正在執行, 所以捨棄掉該次 Request, 並記錄在 cloud 上
        /// </summary>
        public EventHandler<MessageEventArgs> AbandonSession;
        #endregion

        #region 進行 Host 註冊 解註冊
        /// <summary>
        /// 由 RESTClientService 去問是否已註冊 ?
        /// </summary>
        public EventHandler<MessageEventArgs> CheckHostRegistrationStatus;
        /// <summary>
        /// 執行註冊
        /// </summary>
        public EventHandler<MessageEventArgs> Signup;
        /// <summary>
        /// 執行解除註冊
        /// </summary>
        public EventHandler<MessageEventArgs> UnSignup;
        #endregion
        #endregion
    }
}