﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        public void OnXPlaneConnected(object sender, MessageEventArgs e) { XPlaneConnected?.Invoke(sender, e); }
        /// <summary>
        /// 空戰已開始
        /// </summary>
        public void OnAirForceStarted(object sender, MessageEventArgs e) { AirForceStarted?.Invoke(sender, e); }
        #endregion

        #region event definition
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        public EventHandler<MessageEventArgs> XPlaneConnected;
        /// <summary>
        /// 空戰已開始
        /// </summary>
        public EventHandler<MessageEventArgs> AirForceStarted;
        #endregion
    }
}
