﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region DataRef
        #region MFD
        public void OnInstrumentPanelDataRefWrite(object sender, DataRefEventArgs e) { InstrumentPanelDataRefWrtie?.Invoke(sender, e); }
        public EventHandler<DataRefEventArgs> InstrumentPanelDataRefWrtie;
        public void OnInstrumentPanelDataRefRead(object sender, DataRefEventArgs e) { InstrumentPanelDataRefRead?.Invoke(sender, e); }
        public EventHandler<DataRefEventArgs> InstrumentPanelDataRefRead;

        // MFD
        public void OnLeftMFDbuttonPressed(object sender, MessageEventArgs e) { LeftMFDbuttonPressed?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> LeftMFDbuttonPressed;
        public void OnLeftMFDpageChanged(object sender, IntEventArgs e) { LeftMFDpageChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LeftMFDpageChanged;
        public void OnLeftMFDpageReported(object sender, IntEventArgs e) { LeftMFDpageReported?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LeftMFDpageReported;
        public void OnLeftMFDtabsReported(object sender, MessageEventArgs e) { LeftMFDtabsReported?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> LeftMFDtabsReported;
        public void OnLeftMFDindexReported(object sender, IntEventArgs e) { LeftMFDindexReported?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LeftMFDindexReported;

        public void OnRightMFDbuttonPressed(object sender, MessageEventArgs e) { RightMFDbuttonPressed?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> RightMFDbuttonPressed;
        public void OnRightMFDpageChanged(object sender, IntEventArgs e) { RightMFDpageChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> RightMFDpageChanged;
        public void OnRightMFDpageReported(object sender, IntEventArgs e) { RightMFDpageReported?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> RightMFDpageReported;
        public void OnRightMFDtabsReported(object sender, MessageEventArgs e) { RightMFDtabsReported?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> RightMFDtabsReported;
        public void OnRightMFDindexReported(object sender, IntEventArgs e) { RightMFDindexReported?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> RightMFDindexReported;

        // FCR
        public void OnPlaneTargetIndexChanged(object sender, MessageEventArgs e) { PlaneTargetIndexChanged?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> PlaneTargetIndexChanged;
        public void OnChangeRadarRange(object sender, IntEventArgs e) { ChangeRadarRange?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> ChangeRadarRange;
        public void OnChangeRadarHorizon(object sender, EventArgs e) { ChangeRadarHorizon?.Invoke(sender, e); }
        public EventHandler ChangeRadarHorizon;
        public void OnChangeRadarElevation(object sender, EventArgs e) { ChangeRadarElevation?.Invoke(sender, e); }
        public EventHandler ChangeRadarElevation;

        // UFC
        public void OnICPbuttonPressed(object sender, MessageEventArgs e) { ICPbuttonPressed?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> ICPbuttonPressed;
        public void OnICPswitchToggled(object sender, IntEventArgs e) { ICPswitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> ICPswitchToggled;
        public void OnAPswitchToggled(object sender, IntEventArgs e) { APswitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> APswitchToggled;
        public void OnCOM1switchToggled(object sender, IntEventArgs e) { COM1switchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM1switchToggled;
        public void OnCOM2switchToggled(object sender, IntEventArgs e) { COM2switchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM2switchToggled;
        public void OnIFFswitchToggled(object sender, IntEventArgs e) { IFFswitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> IFFswitchToggled;
        public void OnLISTswitchToggled(object sender, IntEventArgs e) { LISTswitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LISTswitchToggled;
        /// <summary>
        /// 0: not selected, 1: AA, 2: AG
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void OnMasterModeChanged(object sender, IntEventArgs e) { MasterModeChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MasterModeChanged;
        // CNI
        public void OnCNIButtonPressed(object sender, IntEventArgs e) { CNIButtonPressed?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> CNIButtonPressed;
        public void OnReturnToCNIpage(object sender, EventArgs e) { ReturnToCNIpage?.Invoke(sender, e); }
        public EventHandler<EventArgs> ReturnToCNIpage;
        // STPT
        public void OnSTPTButtonPressed(object sender, IntEventArgs e) { STPTButtonPressed?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> STPTButtonPressed;
        public void OnSTPTChanged(object sender, IntEventArgs e) { STPTChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> STPTChanged;
        public void OnSTPTSetLatitude(object sender, DegreeEventArgs e) { STPTSetLatitude?.Invoke(sender, e); }
        public EventHandler<DegreeEventArgs> STPTSetLatitude;
        public void OnSTPTSetLongitude(object sender, DegreeEventArgs e) { STPTSetLongitude?.Invoke(sender, e); }
        public EventHandler<DegreeEventArgs> STPTSetLongitude;
        //COM1
        public void OnCOM1ButtonPressed(object sender, IntEventArgs e) { COM1ButtonPressed?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM1ButtonPressed;
        public void OnCOM1PreChanged(object sender, IntEventArgs e) { COM1PreChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM1PreChanged;
        //COM2
        public void OnCOM2ButtonPressed(object sender, IntEventArgs e) { COM2ButtonPressed?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM2ButtonPressed;
        public void OnCOM2PreChanged(object sender, IntEventArgs e) { COM2PreChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> COM2PreChanged;

        public void OnMasterArmSwitchToggled(object sender, IntEventArgs e) { MasterArmSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MasterArmSwitchToggled;
        public void OnMasterArmSwitchPosChanged(object sender, IntEventArgs e) { MasterArmSwitchPosChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MasterArmSwitchPosChanged;
        #endregion
        #endregion

        #region Command
        public void OnInstrumentPanelCommand(object sender, CommandEvevntArgs e) { InstrumentPanelCommand?.Invoke(sender, e); }
        public EventHandler<CommandEvevntArgs> InstrumentPanelCommand;

        // MFD
        public void OnMFDswap(object sender, EventArgs e) { MFDswap?.Invoke(sender, e); }
        public EventHandler MFDswap;
        // STPT
        public void OnSTPTUpdate(object sender, EventArgs e) { STPTUpdate?.Invoke(sender, e); }
        public EventHandler STPTUpdate;
        public void OnSTPTBlinkStart(object sender, EventArgs e) { STPTBlinkStart?.Invoke(sender, e); }
        public EventHandler STPTBlinkStart;
        public void OnSTPTBlinkStop(object sender, EventArgs e) { STPTBlinkStop?.Invoke(sender, e); }
        public EventHandler STPTBlinkStop;
        // COM1
        public void OnCOM1Update(object sender, EventArgs e) { COM1Update?.Invoke(sender, e); }
        public EventHandler COM1Update;
        public void OnCOM1BlinkStart(object sender, EventArgs e) { COM1BlinkStart?.Invoke(sender, e); }
        public EventHandler COM1BlinkStart;
        public void OnCOM1BlinkStop(object sender, EventArgs e) { COM1BlinkStop?.Invoke(sender, e); }
        public EventHandler COM1BlinkStop;
        // COM2
        public void OnCOM2Update(object sender, EventArgs e) { COM2Update?.Invoke(sender, e); }
        public EventHandler COM2Update;
        public void OnCOM2BlinkStart(object sender, EventArgs e) { COM2BlinkStart?.Invoke(sender, e); }
        public EventHandler COM2BlinkStart;
        public void OnCOM2BlinkStop(object sender, EventArgs e) { COM2BlinkStop?.Invoke(sender, e); }
        public EventHandler COM2BlinkStop;
        // TIME
        public void OnXPlaneTimeSecondChanged(object sender, IntEventArgs e) { XPlaneTimeSecondChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> XPlaneTimeSecondChanged;
        public void OnXPlaneTimeMinuteChanged(object sender, IntEventArgs e) { XPlaneTimeMinuteChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> XPlaneTimeMinuteChanged;
        public void OnXPlaneTimeHourChanged(object sender, IntEventArgs e) { XPlaneTimeHourChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> XPlaneTimeHourChanged;
        public void OnXPlaneDayChanged(object sender, IntEventArgs e) { XPlaneDayChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> XPlaneDayChanged;
        public void OnXPlaneMonthChanged(object sender, IntEventArgs e) { XPlaneMonthChanged?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> XPlaneMonthChanged;
        public void OnTimeBlinkStart(object sender, EventArgs e) { TimeBlinkStart?.Invoke(sender, e); }
        public EventHandler TimeBlinkStart;
        public void OnTimeBlinkStop(object sender, EventArgs e) { TimeBlinkStop?.Invoke(sender, e); }
        public EventHandler TimeBlinkStop;
        #endregion
    }
}