﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// 啟動空戰系統
        /// </summary>
        public void OnStartAirForceSystem(object sender, MessageEventArgs e) { StartAirForceSystem?.Invoke(sender, e); }
        #endregion

        #region event definition
        /// <summary>
        /// 啟動空戰系統
        /// </summary>
        public EventHandler<MessageEventArgs> StartAirForceSystem;
        #endregion
    }
}
