﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// 進行 WebSocket 連線, 可能連線成功, 也可能連線失敗
        /// </summary>
        public void OnConnectWS(object serder, MessageEventArgs e) { ConnectWS?.Invoke(serder, e); }
        /// <summary>
        /// 關閉 WebSocket
        /// </summary>
        public void OnCloseWS(object serder, MessageEventArgs e) { CloseWS?.Invoke(serder, e); }
        /// <summary>
        /// WebSocket 連線成功後, 馬上送一個訊息給 Cloud, 告知 Host 連線資訊
        /// </summary>
        public void OnSendWSConnectMessage(object serder, MessageEventArgs e) { SendWSConnectMessage?.Invoke(serder, e); }
        /// <summary>
        /// Release Session Locked
        /// </summary>
        public void OnSessionAlreadyIdle(object serder, MessageEventArgs e) { SessionAlreadyIdle?.Invoke(serder, e); }
        #endregion

        #region event definition
        /// <summary>
        /// 進行 WebSocket 連線, 可能連線成功, 也可能連線失敗
        /// <para>AirManager fire event(開機後主動連線)</para>
        /// <para>WSClientService fire event(自己的 WatchDog 偵測到斷線後, 自行再次連線)</para>
        /// <para>WSClientService += handle function</para>
        /// </summary>
        public EventHandler<MessageEventArgs> ConnectWS;
        /// <summary>
        /// 關閉 WebSocket
        /// </summary>
        public EventHandler<MessageEventArgs> CloseWS;
        /// <summary>
        /// WebSocket 連線成功後, 馬上送一個訊息給 Cloud, 告知 Host 連線資訊
        /// </summary>
        public EventHandler<MessageEventArgs> SendWSConnectMessage;
        /// <summary>
        /// Release Session Locked
        /// </summary>
        public EventHandler<MessageEventArgs> SessionAlreadyIdle;
        #endregion
    }
}