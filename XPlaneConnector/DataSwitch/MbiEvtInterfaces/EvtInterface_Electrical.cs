﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        /// <summary>
        /// 啟動電力系統檢查
        /// </summary>
        public void OnStartElectricalCheck(object sender, MessageEventArgs e) { StartElectricalCheck?.Invoke(sender, e); }
        public EventHandler<MessageEventArgs> StartElectricalCheck;
        public void OnElectricalDataRefWrite(object sender, DataRefEventArgs e) { ElectricalDataRefWrtie?.Invoke(sender, e); }
        public EventHandler<DataRefEventArgs> ElectricalDataRefWrtie;
        public void OnElectricalDataRefRead(object sender, DataRefEventArgs e) { ElectricalDataRefRead?.Invoke(sender, e); }
        public EventHandler<DataRefEventArgs> ElectricalDataRefRead;

        #region PowerUnit
        #region Battery
        public void OnBatteryOn(object sender, EventArgs e) { BatteryOn?.Invoke(sender, e); }
        public EventHandler BatteryOn;
        public void OnBatteryOff(object sender, EventArgs e) { BatteryOff?.Invoke(sender, e); }
        public EventHandler BatteryOff;
        public void OnConnectToBattery(object sender, EventArgs e) { ConnectToBattery?.Invoke(sender, e); }
        public EventHandler ConnectToBattery;
        public void OnBatteryAllConnect(object sender, EventArgs e) { BatteryAllConnect?.Invoke(sender, e); }
        public EventHandler BatteryAllConnect;
        public void OnDisconnectFromBattery(object sender, EventArgs e) { DisconnectFromBattery?.Invoke(sender, e); }
        public EventHandler DisconnectFromBattery;
        public void OnBatteryAllDisconnect(object sender, EventArgs e) { BatteryAllDisconnect?.Invoke(sender, e); }
        public EventHandler BatteryAllDisconnect;
        public void OnBatteryFailure(object sender, EventArgs e) { BatteryFailure?.Invoke(sender, e); }
        public EventHandler BatteryFailure;
        #endregion

        #region MainGenerator
        public void OnMainGeneratorOn(object sender, EventArgs e) { MainGeneratorOn?.Invoke(sender, e); }
        public EventHandler MainGeneratorOn;
        public void OnMainGeneratorOff(object sender, EventArgs e) { MainGeneratorOff?.Invoke(sender, e); }
        public EventHandler MainGeneratorOff;
        public void OnConnectToMainGenerator(object sender, EventArgs e) { ConnectToMainGenerator?.Invoke(sender, e); }
        public EventHandler ConnectToMainGenerator;
        public void OnMainGeneratorAllConnect(object sender, EventArgs e) { MainGeneratorAllConnect?.Invoke(sender, e); }
        public EventHandler MainGeneratorAllConnect;
        public void OnDisconnectFromMainGenerator(object sender, EventArgs e) { DisconnectFromMainGenerator?.Invoke(sender, e); }
        public EventHandler DisconnectFromMainGenerator;
        public void OnMainGeneratorAllDisconnect(object sender, EventArgs e) { MainGeneratorAllDisconnect?.Invoke(sender, e); }
        public EventHandler MainGeneratorAllDisconnect;
        public void OnMainGeneratorFailure(object sender, EventArgs e) { MainGeneratorFailure?.Invoke(sender, e); }
        public EventHandler MainGeneratorFailure;
        #endregion

        #region STBYGenerator
        public void OnSTBYGeneratorOn(object sender, EventArgs e) { STBYGeneratorOn?.Invoke(sender, e); }
        public EventHandler STBYGeneratorOn;
        public void OnSTBYGeneratorOff(object sender, EventArgs e) { STBYGeneratorOff?.Invoke(sender, e); }
        public EventHandler STBYGeneratorOff;
        public void OnConnectToSTBYGenerator(object sender, EventArgs e) { ConnectToSTBYGenerator?.Invoke(sender, e); }
        public EventHandler ConnectToSTBYGenerator;
        public void OnDisconnectFromSTBYGenerator(object sender, EventArgs e) { DisconnectFromSTBYGenerator?.Invoke(sender, e); }
        public EventHandler DisconnectFromSTBYGenerator;
        public void OnSTBYGeneratorFailure(object sender, EventArgs e) { STBYGeneratorFailure?.Invoke(sender, e); }
        public EventHandler STBYGeneratorFailure;
        #endregion

        #region EPUGenerator
        public void OnEPUGeneratorOn(object sender, EventArgs e) { EPUGeneratorOn?.Invoke(sender, e); }
        public EventHandler EPUGeneratorOn;
        public void OnEPUGeneratorOff(object sender, EventArgs e) { EPUGeneratorOff?.Invoke(sender, e); }
        public EventHandler EPUGeneratorOff;
        public void OnConnectToEPUGenerator(object sender, EventArgs e) { ConnectToEPUGenerator?.Invoke(sender, e); }
        public EventHandler ConnectToEPUGenerator;
        public void OnDisconnectFromEPUGenerator(object sender, EventArgs e) { DisconnectFromEPUGenerator?.Invoke(sender, e); }
        public EventHandler DisconnectFromEPUGenerator;
        public void OnEPUGeneratorFailure(object sender, EventArgs e) { EPUGeneratorFailure?.Invoke(sender, e); }
        public EventHandler EPUGeneratorFailure;
        #endregion
        #endregion

        #region Bus
        #region AC
        #region OverCurrentSensingContactor1
        public void OnConnectToOverCurrentSensingContactor1(object sender, EventArgs e) { ConnectToOverCurrentSensingContactor1?.Invoke(sender, e); }
        public EventHandler ConnectToOverCurrentSensingContactor1;
        #endregion

        #region OverCurrentSensingContactor2
        public void OnOverCurrentSensingContactor2Enabled(object sender, EventArgs e) { OverCurrentSensingContactor2Enabled?.Invoke(sender, e); }
        public EventHandler OverCurrentSensingContactor2Enabled;
        public void OnOverCurrentSensingContactor2Disabled(object sender, EventArgs e) { OverCurrentSensingContactor2Disabled?.Invoke(sender, e); }
        public EventHandler OverCurrentSensingContactor2Disabled;
        public void OnConnectToOverCurrentSensingContactor2(object sender, EventArgs e) { ConnectToOverCurrentSensingContactor2?.Invoke(sender, e); }
        public EventHandler ConnectToOverCurrentSensingContactor2;
        #endregion

        #region FCR
        public void OnFCREnabled(object sender, EventArgs e) { FCREnabled?.Invoke(sender, e); }
        public EventHandler FCREnabled;
        public void OnFCRDisabled(object sender, EventArgs e) { FCRDisabled?.Invoke(sender, e); }
        public EventHandler FCRDisabled;
        public void OnConnectToFCR(object sender, EventArgs e) { ConnectToFCR?.Invoke(sender, e); }
        public EventHandler ConnectToFCR;
        #endregion

        #region NonEssentialAC1
        public void OnNonEssentialAC1Enabled(object sender, EventArgs e) { NonEssentialAC1Enabled?.Invoke(sender, e); }
        public EventHandler NonEssentialAC1Enabled;
        public void OnNonEssentialAC1Disabled(object sender, EventArgs e) { NonEssentialAC1Disabled?.Invoke(sender, e); }
        public EventHandler NonEssentialAC1Disabled;
        public void OnConnectToNonEssentialAC1(object sender, EventArgs e) { ConnectToNonEssentialAC1?.Invoke(sender, e); }
        public EventHandler ConnectToNonEssentialAC1;
        public void OnDisconnectFromNonEssentialAC1(object sender, EventArgs e) { DisconnectFromNonEssentialAC1?.Invoke(sender, e); }
        public EventHandler DisconnectFromNonEssentialAC1;
        #endregion

        #region NonEssentialAC2
        public void OnNonEssentialAC2Enabled(object sender, EventArgs e) { NonEssentialAC2Enabled?.Invoke(sender, e); }
        public EventHandler NonEssentialAC2Enabled;
        public void OnNonEssentialAC2Disabled(object sender, EventArgs e) { NonEssentialAC2Disabled?.Invoke(sender, e); }
        public EventHandler NonEssentialAC2Disabled;
        public void OnConnectToNonEssentialAC2(object sender, EventArgs e) { ConnectToNonEssentialAC2?.Invoke(sender, e); }
        public EventHandler ConnectToNonEssentialAC2;
        public void OnDisconnectFromNonEssentialAC2(object sender, EventArgs e) { DisconnectFromNonEssentialAC2?.Invoke(sender, e); }
        public EventHandler DisconnectFromNonEssentialAC2;
        #endregion

        #region EssentialAC
        public void OnEssentialACEnabled(object sender, EventArgs e) { EssentialACEnabled?.Invoke(sender, e); }
        public EventHandler EssentialACEnabled;
        public void OnEssentialACDisabled(object sender, EventArgs e) { EssentialACDisabled?.Invoke(sender, e); }
        public EventHandler EssentialACDisabled;
        public void OnConnectToEssentialAC(object sender, EventArgs e) { ConnectToEssentialAC?.Invoke(sender, e); }
        public EventHandler ConnectToEssentialAC;
        public void OnDisconnectFromEssentialAC(object sender, EventArgs e) { DisconnectFromEssentialAC?.Invoke(sender, e); }
        public EventHandler DisconnectFromEssentialAC;
        #endregion

        #region EmergencyAC1
        public void OnEmergencyAC1Enabled(object sender, EventArgs e) { EmergencyAC1Enabled?.Invoke(sender, e); }
        public EventHandler EmergencyAC1Enabled;
        public void OnEmergencyAC1Disabled(object sender, EventArgs e) { EmergencyAC1Disabled?.Invoke(sender, e); }
        public EventHandler EmergencyAC1Disabled;
        public void OnConnectToEmergencyAC1(object sender, EventArgs e) { ConnectToEmergencyAC1?.Invoke(sender, e); }
        public EventHandler ConnectToEmergencyAC1;
        #endregion

        #region EmergencyAC2
        public void OnEmergencyAC2Enabled(object sender, EventArgs e) { EmergencyAC2Enabled?.Invoke(sender, e); }
        public EventHandler EmergencyAC2Enabled;
        public void OnEmergencyAC2Disabled(object sender, EventArgs e) { EmergencyAC2Disabled?.Invoke(sender, e); }
        public EventHandler EmergencyAC2Disabled;
        public void OnConnectToEmergencyAC2(object sender, EventArgs e) { ConnectToEmergencyAC2?.Invoke(sender, e); }
        public EventHandler ConnectToEmergencyAC2;
        #endregion
        #endregion

        #region Converter
        #region Converter1
        public void OnConverter1Enabled(object sender, EventArgs e) { Converter1Enabled?.Invoke(sender, e); }
        public EventHandler Converter1Enabled;
        public void OnConverter1Disabled(object sender, EventArgs e) { Converter1Disabled?.Invoke(sender, e); }
        public EventHandler Converter1Disabled;
        public void OnConnectToConverter1(object sender, EventArgs e) { ConnectToConverter1?.Invoke(sender, e); }
        public EventHandler ConnectToConverter1;
        #endregion
        #region Converter2
        public void OnConverter2Enabled(object sender, EventArgs e) { Converter2Enabled?.Invoke(sender, e); }
        public EventHandler Converter2Enabled;
        public void OnConverter2Disabled(object sender, EventArgs e) { Converter2Disabled?.Invoke(sender, e); }
        public EventHandler Converter2Disabled;
        public void OnConnectToConverter2(object sender, EventArgs e) { ConnectToConverter2?.Invoke(sender, e); }
        public EventHandler ConnectToConverter2;
        #endregion
        #endregion

        #region DC
        #region Battery1
        public void OnBattery1Enabled(object sender, EventArgs e) { Battery1Enabled?.Invoke(sender, e); }
        public EventHandler Battery1Enabled;
        public void OnBattery1Disabled(object sender, EventArgs e) { Battery1Disabled?.Invoke(sender, e); }
        public EventHandler Battery1Disabled;
        public void OnConnectToBattery1(object sender, EventArgs e) { ConnectToBattery1?.Invoke(sender, e); }
        public EventHandler ConnectToBattery1;
        public void OnDisconnectFromBattery1(object sender, EventArgs e) { DisconnectFromBattery1?.Invoke(sender, e); }
        public EventHandler DisconnectFromBattery1;
        #endregion

        #region Battery2
        public void OnBattery2Enabled(object sender, EventArgs e) { Battery2Enabled?.Invoke(sender, e); }
        public EventHandler Battery2Enabled;
        public void OnBattery2Disabled(object sender, EventArgs e) { Battery2Disabled?.Invoke(sender, e); }
        public EventHandler Battery2Disabled;
        public void OnConnectToBattery2(object sender, EventArgs e) { ConnectToBattery2?.Invoke(sender, e); }
        public EventHandler ConnectToBattery2;
        public void OnDisconnectFromBattery2(object sender, EventArgs e) { DisconnectFromBattery2?.Invoke(sender, e); }
        public EventHandler DisconnectFromBattery2;
        #endregion

        #region EssentialDC
        public void OnEssentialDCEnabled(object sender, EventArgs e) { EssentialDCEnabled?.Invoke(sender, e); }
        public EventHandler EssentialDCEnabled;
        public void OnConnectToEssentialDC(object sender, EventArgs e) { ConnectToEssentialDC?.Invoke(sender, e); }
        public EventHandler ConnectToEssentialDC;
        #endregion

        #region EmergencyDC1
        public void OnEmergencyDC1Enabled(object sender, EventArgs e) { EmergencyDC1Enabled?.Invoke(sender, e); }
        public EventHandler EmergencyDC1Enabled;
        public void OnEmergencyDC1Disabled(object sender, EventArgs e) { EmergencyDC1Disabled?.Invoke(sender, e); }
        public EventHandler EmergencyDC1Disabled;
        public void OnConnectToEmergencyDC1(object sender, EventArgs e) { ConnectToEmergencyDC1?.Invoke(sender, e); }
        public EventHandler ConnectToEmergencyDC1;
        public void OnDisconnectFromEmergencyDC1(object sender, EventArgs e) { DisconnectFromEmergencyDC1?.Invoke(sender, e); }
        public EventHandler DisconnectFromEmergencyDC1;
        #endregion

        #region EmergencyDC2
        public void OnEmergencyDC2Enabled(object sender, EventArgs e) { EmergencyDC2Enabled?.Invoke(sender, e); }
        public EventHandler EmergencyDC2Enabled;
        public void OnEmergencyDC2Disabled(object sender, EventArgs e) { EmergencyDC2Disabled?.Invoke(sender, e); }
        public EventHandler EmergencyDC2Disabled;
        public void OnConnectToEmergencyDC2(object sender, EventArgs e) { ConnectToEmergencyDC2?.Invoke(sender, e); }
        public EventHandler ConnectToEmergencyDC2;
        public void OnDisconnectFromEmergencyDC2(object sender, EventArgs e) { DisconnectFromEmergencyDC2?.Invoke(sender, e); }
        public EventHandler DisconnectFromEmergencyDC2;
        #endregion
        #endregion
        #endregion

        #region Instrument
        
        

        #region NonEssentialAC2
        public void OnTaxiLightSwitchToggled(object sender, IntEventArgs e) { TaxiLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> TaxiLightSwitchToggled;
        public void OnTaxiLightPower(object sender, IntEventArgs e) { TaxiLightPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> TaxiLightPower;
        #endregion

        #region EssentialAC
        public void OnMFDSwitchToggled(object sender, IntEventArgs e) { MFDSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MFDSwitchToggled;
        public void OnMFDpower(object sender, IntEventArgs e) { MFDpower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MFDpower;
        #endregion

        #region EmergencyAC1Instrument
        public void OnAltimeterSwitchToggled(object sender, IntEventArgs e) { AltimeterSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> AltimeterSwitchToggled;
        public void OnAOAIndicatorSwitchToggled(object sender, IntEventArgs e) { AOAIndicatorSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> AOAIndicatorSwitchToggled;
        public void OnEGISwitchToggled(object sender, IntEventArgs e) { EGISwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> EGISwitchToggled;
        public void OnFuelFlowIndicatorSwitchToggled(object sender, IntEventArgs e) { FuelFlowIndicatorSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FuelFlowIndicatorSwitchToggled;

        #endregion




        #region EmergencyAC2
        public void OnLandingLightSwitchToggled(object sender, IntEventArgs e) { LandingLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LandingLightSwitchToggled;
        public void OnLandingLightPower(object sender, IntEventArgs e) { LandingLightPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LandingLightPower;
        public void OnNavigationLightSwitchToggled(object sender, IntEventArgs e) { NavigationLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> NavigationLightSwitchToggled;
        public void OnNavigationLightPower(object sender, IntEventArgs e) { NavigationLightPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> NavigationLightPower;
        public void OnHUDSwitchToggled(object sender, IntEventArgs e) { HUDSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> HUDSwitchToggled;
        public void OnHUDPower(object sender, IntEventArgs e) { HUDPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> HUDPower;
        public void OnGunPower(object sender, IntEventArgs e) { GunPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> GunPower;
        #endregion

        #region EmergencyDC1Instrument
        public void OnAOAIndexerSwitchToggled(object sender, IntEventArgs e) { AOAIndexerSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> AOAIndexerSwitchToggled;
        public void OnFLCSPowerSwitchToggled(object sender, IntEventArgs e) { FLCSPowerSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FLCSPowerSwitchToggled;
        public void OnFLCSResetSwitchToggled(object sender, IntEventArgs e) { FLCSResetSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FLCSResetSwitchToggled;
        public void OnFLCSWarningLightSwitchToggled(object sender, IntEventArgs e) { FLCSWarningLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FLCSWarningLightSwitchToggled;
        public void OnLGLightSwitchToggled(object sender, IntEventArgs e) { LGLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LGLightSwitchToggled;
        public void OnMasterArmSwitchSwitchToggled(object sender, IntEventArgs e) { MasterArmSwitchSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MasterArmSwitchSwitchToggled;
        public void OnSpeedbrakesSwitchToggled(object sender, IntEventArgs e) { SpeedbrakesSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> SpeedbrakesSwitchToggled;
        public void OnStickTrimSwitchToggled(object sender, IntEventArgs e) { StickTrimSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> StickTrimSwitchToggled;

        #endregion

        #region EmergencyDC2Instrument
        public void OnUFCSwitchToggled(object sender, IntEventArgs e) { UFCSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> UFCSwitchToggled;
        #endregion
        #region Battery1Instrument
        public void OnEngineWarningLightSwitchToggled(object sender, IntEventArgs e) { EngineWarningLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> EngineWarningLightSwitchToggled;
        public void OnFLCS_RLYSwitchToggled(object sender, IntEventArgs e) { FLCS_RLYSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FLCS_RLYSwitchToggled;
        public void OnHYD_OIL_PressWarningLightSwitchToggled(object sender, IntEventArgs e) { HYD_OIL_PressWarningLightSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> HYD_OIL_PressWarningLightSwitchToggled;
        public void OnJFSSwitchToggled(object sender, IntEventArgs e) { JFSSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> JFSSwitchToggled;
        public void OnLGDownlockRelaysSwitchToggled(object sender, IntEventArgs e) { LGDownlockRelaysSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LGDownlockRelaysSwitchToggled;
        public void OnLGUplockDownlockSwitchToggled(object sender, IntEventArgs e) { LGUplockDownlockSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> LGUplockDownlockSwitchToggled;
        public void OnMasterArmSwitchPower(object sender, IntEventArgs e) { MasterArmSwitchPower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> MasterArmSwitchPower;
        public void OnUFCpower(object sender, IntEventArgs e) { UFCpower?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> UFCpower;


        #endregion

        #region Others
        public void OnAAModeSwitchToggled(object sender, IntEventArgs e) { AAModeSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> AAModeSwitchToggled;
        #endregion

        #region FCRInstrument 
        public void OnFCRSwitchToggled(object sender, IntEventArgs e) { FCRSwitchToggled?.Invoke(sender, e); }
        public EventHandler<IntEventArgs> FCRSwitchToggled;
        #endregion




        #region Commands 
        public void OnElectricalCommand(object sender, CommandEvevntArgs e) { ElectricalCommand?.Invoke(sender, e); }
        public EventHandler<CommandEvevntArgs> ElectricalCommand;
        public void OnLandingGearUp(object sender, EventArgs e) { LandingGearUp?.Invoke(sender, e); }
        public EventHandler LandingGearUp;
        public void OnLandingGearDown(object sender, EventArgs e) { LandingGearDown?.Invoke(sender, e); }
        public EventHandler LandingGearDown;
        #endregion
        #endregion
    }
}
