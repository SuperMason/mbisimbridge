﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        /// <summary>
        /// 將 Test Pattern 打至螢幕上
        /// </summary>
        public void OnWriteTestPattern(object serder, MessageEventArgs e) { WriteTestPattern?.Invoke(serder, e); }
        /// <summary>
        /// TestPatternWrote Session updated, 完成整個 write pattern 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public void OnWritePatternSessionUpdated(object serder, MessageEventArgs e) { WritePatternSessionUpdated?.Invoke(serder, e); }
        #endregion

        #region event definition
        /// <summary>
        /// 將 Test Pattern 打至螢幕上
        /// </summary>
        public EventHandler<MessageEventArgs> WriteTestPattern;
        /// <summary>
        /// TestPatternWrote Session updated, 完成整個 write pattern 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        public EventHandler<MessageEventArgs> WritePatternSessionUpdated;
        #endregion
    }
}