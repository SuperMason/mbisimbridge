﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace XPlaneConnector.DataSwitch.MbiEvtInterfaces
{
    public sealed partial class EvtInterfaceService
    {
        #region event Invoke function()
        #region 從 Air Manager 取得 Command, DataRef
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        public void OnExecuteAirManagerCommand(object serder, CommandEvevntArgs e) { ExecuteAirManagerCommand?.Invoke(serder, e); }
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
        /// </summary>
        public void OnExecuteAirManagerDataRefWrite(object serder, DataRefEventArgs e) { ExecuteAirManagerDataRefWrite?.Invoke(serder, e); }
        #endregion

        #region 主動更新 "Air Manager" DataRef value :: '單一值', Array值
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: '單一值' int, float, double, string
        /// <para>使用情境 :::</para>
        /// <para>1. X-Plane 直接回覆更新值</para>
        /// <para>2. AirForceSystem / GRPC 觸發更新 DataRef 值</para>
        /// </summary>
        public void OnUpdateAirManagerDataRefSingleVal(object sender, DataRefEventArgs e) { UpdateAirManagerDataRefSingleVal?.Invoke(sender, e); }
        /// <summary>
        /// 手動更新 "Air Manager" DataRef value :: Array值 int[], float[], double[]
        /// <para>1. X-Plane 背景自動更新, 自行由 BackgroundWorker 組裝執行完成, 因 X-Plane 分拆 array element[i] 回覆時間有時間差, 必須等待到齊後, 才能統一寫入 AM</para>
        /// <para>2. AirForceSystem / GRPC 手動更新, 自行控制 array element[i] 完整到齊後, 統一寫入 AM</para>
        /// <para>Array DataRef value updated 同步至 Air Manager 的 C++ Wrapper</para>
        /// </summary>
        public void OnManualUpdateAirManagerDataRefArrayVal(object sender, DataRefEventArgs e) { ManualUpdateAirManagerDataRefArrayVal?.Invoke(sender, e); }
        /// <summary>
        /// 通知 AirManager 已經更新完成的 DataRef value
        /// </summary>
        public void OnInfoAirManagerDataRefValueUpdated(object sender, DataRefEventArgs e) { InfoAirManagerDataRefValueUpdated?.Invoke(sender, e); }
        #endregion

        #region 控制 WebSocket
        /// <summary>
        /// WebSocket 連線成功
        /// </summary>
        public void OnWSConnected(object serder, MessageEventArgs e) { WSConnected?.Invoke(serder, e); }
        /// <summary>
        /// WebSocket 連線發生錯誤
        /// </summary>
        public void OnWSConnectionError(object serder, MessageEventArgs e) { WSConnectionError?.Invoke(serder, e); }
        /// <summary>
        /// WebSocket 連線已經關閉
        /// </summary>
        public void OnWSConnectionClosed(object serder, MessageEventArgs e) { WSConnectionClosed?.Invoke(serder, e); }
        #endregion

        #region 檢查 Host 已註冊? 未註冊?
        /// <summary>
        /// Host 未註冊
        /// </summary>
        public void OnHostNotRegistered(object serder, MessageEventArgs e) { HostNotRegistered?.Invoke(serder, e); }
        /// <summary>
        /// Host 已註冊
        /// </summary>
        public void OnHostRegistered(object serder, MessageEventArgs e) { HostRegistered?.Invoke(serder, e); }
        #endregion
        #endregion

        #region event definition
        #region 從 Air Manager 取得 Command, DataRef
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        public EventHandler<CommandEvevntArgs> ExecuteAirManagerCommand;
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
        /// </summary>
        public EventHandler<DataRefEventArgs> ExecuteAirManagerDataRefWrite;
        #endregion

        #region 主動更新 "Air Manager" DataRef value :: '單一值', Array值
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: '單一值' int, float, double, string
        /// <para>使用情境 :::</para>
        /// <para>1. X-Plane 直接回覆更新值</para>
        /// <para>2. AirForceSystem / GRPC 觸發更新 DataRef 值</para>
        /// </summary>
        public EventHandler<DataRefEventArgs> UpdateAirManagerDataRefSingleVal;
        /// <summary>
        /// 手動更新 "Air Manager" DataRef value :: Array值 int[], float[], double[]
        /// <para>1. X-Plane 背景自動更新, 自行由 BackgroundWorker 組裝執行完成, 因 X-Plane 分拆 array element[i] 回覆時間有時間差, 必須等待到齊後, 才能統一寫入 AM</para>
        /// <para>2. AirForceSystem / GRPC 手動更新, 自行控制 array element[i] 完整到齊後, 統一寫入 AM</para>
        /// <para>Array DataRef value updated 同步至 Air Manager 的 C++ Wrapper</para>
        /// </summary>
        public EventHandler<DataRefEventArgs> ManualUpdateAirManagerDataRefArrayVal;
        /// <summary>
        /// 通知 AirManager 已經更新完成的 DataRef value
        /// </summary>
        public EventHandler<DataRefEventArgs> InfoAirManagerDataRefValueUpdated;
        #endregion

        #region 控制 WebSocket
        /// <summary>
        /// WebSocket 連線成功
        /// </summary>
        public EventHandler<MessageEventArgs> WSConnected;
        /// <summary>
        /// WebSocket 連線發生錯誤
        /// </summary>
        public EventHandler<MessageEventArgs> WSConnectionError;
        /// <summary>
        /// WebSocket 連線已經關閉
        /// </summary>
        public EventHandler<MessageEventArgs> WSConnectionClosed;
        #endregion

        #region 檢查 Host 已註冊? 未註冊?
        /// <summary>
        /// Host 未註冊
        /// </summary>
        public EventHandler<MessageEventArgs> HostNotRegistered;
        /// <summary>
        /// Host 已註冊
        /// </summary>
        public EventHandler<MessageEventArgs> HostRegistered;
        #endregion
        #endregion
    }
}