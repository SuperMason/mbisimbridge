﻿using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    public class MessageToWS
    {
        #region field
        #endregion

        #region constructor
        public MessageToWS(string data)
        { Data = data; }
        #endregion

        [JsonProperty("message")]
        public string Message { get { return "sendmessage"; } }

        [JsonProperty("data")]
        public string Data { get; }
    }
}