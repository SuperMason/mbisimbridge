﻿using MBILibrary.Util;
using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage {
    /// <summary>
    /// WebSocket 連線成功後, send connect SUCCESS message to cloud
    /// </summary>
    public class ConnectMessage {
        #region constructor
        /// <summary>
        /// WebSocket 連線成功後, send connect SUCCESS message to cloud
        /// </summary>
        /// <param name="verificationCode">驗證碼</param>
        /// <param name="networkType">使用的 "網路類型" ? 三擇一 ::: Ethernet / Wi-Fi / Mobile</param>
        public ConnectMessage(string verificationCode, string networkType) {
            HostLicenseKey = UtilConstants.UniqueKeyOfMachine;
            VerificationCode = verificationCode;
            NetworkType = networkType;
        }
        #endregion

        [JsonProperty("action")]
        public string Action { get { return "connect"; } }

        [JsonProperty("hostLicenseKey")]
        public string HostLicenseKey { get; }

        [JsonProperty("verificationCode")]
        public string VerificationCode { get; }

        [JsonProperty("networkType")]
        public string NetworkType { get; }
    }
}