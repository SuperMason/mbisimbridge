﻿using MBILibrary.Util.CustomJsonConverter;
using Newtonsoft.Json;
using System;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    public class PatternProperty
    {
        [JsonProperty("layer")]
        public string Layer { get; set; }

        [JsonProperty("x-coordinate")]
        public string Xcoordinate { get; set; }

        [JsonProperty("y-coordinate")]
        public string Ycoordinate { get; set; }

        [JsonProperty("width")]
        public string Width { get; set; }

        [JsonProperty("height")]
        public string Height { get; set; }

        [JsonProperty("sole-color")]
        public string SoreColor { get; set; }

        [JsonProperty("sole-grayscale")]
        public string SoreGrayscale { get; set; }

        [JsonProperty("line-type")]
        public string LineType { get; set; }

        [JsonProperty("line-width")]
        public string LineWidth { get; set; }

        [JsonProperty("line-gap")]
        public string LineGap { get; set; }

        [JsonProperty("line-color")]
        public string LineColor { get; set; }

        [JsonProperty("line-grayscale")]
        public string LineGrayscale { get; set; }

        [JsonProperty("dynamic-line")]
        public bool DynamicLine { get; set; }

        [JsonProperty("line-overlaping")]
        public bool LineOverlaping { get; set; }


        #region 自動更新相關: Auto Reboot Setting
        /// <summary>
        /// 自動更新相關: The time of checking new version delivered or not, need to reboot or not, 資料範例： "23:59:59"
        /// </summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty("AutoRebootTime4Updating")]
        public DateTime AutoRebootTime4Updating { get; set; }
        /// <summary>
        /// 自動更新相關: Auto check new version delivered enable
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("AutoReboot4UpdatingEnable")]
        public bool AutoReboot4UpdatingEnable { get; set; }
        #endregion

        /// <summary>
        /// Update Facial Analysis Time, 資料範例： "2000-01-01 00:00:01"
        /// </summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty("mAnalysisTime")]
        public DateTime mAnalysisTime { get; set; }

    }
}
