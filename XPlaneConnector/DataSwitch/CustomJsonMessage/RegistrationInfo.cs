﻿using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    /// <summary>
    /// 本機的註冊資訊
    /// </summary>
    public class RegistrationInfo
    {
        /// <summary>
        /// 本機名稱
        /// </summary>
        [JsonProperty("hostName")]
        public string HostName { get; set; }

        /// <summary>
        /// 本機的 unique key
        /// <para> 帶入 UtilConstants.UniqueKeyOfMachine</para>
        /// </summary>
        [JsonProperty("hostLicenseKey")]
        public string HostLicenseKey { get; set; }
    }
}