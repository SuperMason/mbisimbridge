﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs
{
    public class IntEventArgs: EventArgs
    {

        #region Porperties
        public int value { get; set; }
        #endregion
        #region Constructor
        public IntEventArgs(int value)
        {
            this.value = value;
        }
        #endregion
    }
}
