﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs
{
    public struct Degree
    {
        public Degree(int dir, int deg, float min)
        {
            Dir = dir;
            Deg = deg;
            Min = min;
        }
        public int Dir { get; set; }
        public int Deg { get; set; }
        public float Min { get; set; }
    }

    public class DegreeEventArgs : EventArgs
    {

        #region Porperties
        public Degree degree { get; set; }
        #endregion
        #region Constructor
        public DegreeEventArgs(Degree degree)
        {
            this.degree = degree;
        }
        #endregion
    }
}
