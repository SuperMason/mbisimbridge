﻿using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs
{
    /// <summary>
    /// X-Plane Command 指令
    /// </summary>
    public class CommandEvevntArgs : MessageEventArgs
    {
        #region X-Plane Command 指令
        /// <summary>
        /// X-Plane Command 指令
        /// </summary>
        [JsonIgnore]
        private readonly XPlaneCommand xCommand;
        /// <summary>
        /// X-Plane Command 指令
        /// </summary>
        [JsonIgnore]
        public XPlaneCommand Command { get { return xCommand; } }

        /// <summary>
        /// Event 傳送的資訊 (X-Plane Command 指令)
        /// </summary>
        /// <param name="data">自定義的訊息</param>
        /// <param name="xPlaneCommand">準備下給 X-Plane 執行的指令</param>
        public CommandEvevntArgs(string data, XPlaneCommand xPlaneCommand)
        {
            mData = data;
            xCommand = xPlaneCommand;
        }
        #endregion
    }
}