﻿using MBILibrary.Enums;
using Newtonsoft.Json;
using XPlaneConnector.DataRef;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs
{
    /// <summary>
    /// Data Bus for 'Single', 'Array' value
    /// <para>Value Type ::: int, byte, float, double, int[], float[], double[]</para>
    /// </summary>
    public class DataRefEventArgs : MessageEventArgs
    {
        #region Properties
        /// <summary>
        /// int[], float[], double[] 擇一
        /// </summary>
        [JsonIgnore]
        private object AryValue = null;
        /// <summary>
        /// int[], float[], double[] 擇一
        /// </summary>
        [JsonIgnore]
        public object ArrayValue => AryValue;

        /// <summary>
        /// 當下 DataRef 'ObjVal' Show String
        /// </summary>
        public object DataRefObjValShowString => DataBusIsBytes ? DataBus4Bytes.ObjValShowString : DataBusIsString ? DataBus4String.ObjValShowString : DataBusIsObject ? DataBus4Object.ObjValShowString : default;
        /// <summary>
        /// 當下 DataRef Type
        /// </summary>
        public string DataRefType => DataBusIsBytes ? DataBus4Bytes.Type : DataBusIsString ? DataBus4String.Type : DataBusIsObject ? DataBus4Object.Type : default;
        /// <summary>
        /// 當下 DataRef Description
        /// </summary>
        public string DataRefDescription => DataBusIsBytes ? DataBus4Bytes.Description : DataBusIsString ? DataBus4String.Description : DataBusIsObject ? DataBus4Object.Description : default;
        /// <summary>
        /// 當下 DataRef StringLength
        /// </summary>
        public int DataRefStringLength => DataBusIsBytes ? DataBus4Bytes.StringLength : DataBusIsString ? DataBus4String.StringLength : DataBusIsObject ? DataBus4Object.StringLength : default;
        /// <summary>
        /// 當下 DataRef is writable ?
        /// </summary>
        public bool DataRefIsWritable => DataBusIsBytes ? DataBus4Bytes.IsWritable : DataBusIsString ? DataBus4String.IsWritable : DataBusIsObject ? DataBus4Object.IsWritable : default;
        /// <summary>
        /// 取得當下的 DataRef Name
        /// <para>單一值 int, float, double 格式為 :: xxx/xx</para>
        /// <para>Array值 int[], float[], double[], string 格式為 :: xxx/xx[i]</para>
        /// </summary>
        public string DataRefName => DataBusIsBytes ? DataBus4Bytes.DataRef : DataBusIsString ? DataBus4String.DataRef : DataBusIsObject ? DataBus4Object.DataRef : default;
        /// <summary>
        /// 取得當下的 DataRef Value
        /// </summary>
        public object ObjVal => DataBusIsBytes ? DataBus4Bytes.ObjVal2Float : DataBusIsString ? (object)DataBus4String.RawStringValue : DataBusIsObject ? DataBus4Object.ObjVal : default;
        /// <summary>
        /// 動態取得 DataBus4Object 的 ObjVal 值
        /// <para>1. DataBus4ObjectIsBytes/DataBus4Bytes :: return ObjVal</para>
        /// <para>2. DataBus4ObjectIsString/DataBus4String :: return ObjVal2String</para>
        /// <para>3. Others return null</para>
        /// </summary>
        public object ObjCheck => DataBusIsBytes ? DataBus4Bytes.ObjVal : DataBusIsString ? DataBus4String.ObjVal2String : DataBus4ObjectIsBytes ? DataBus4Object.ObjVal : DataBus4ObjectIsString ? DataBus4Object.ObjVal2String : default;

        /// <summary>
        /// 由哪一個系統觸發
        /// </summary>
        [JsonIgnore]
        private TriggerByWhatSystem TriggerBy = TriggerByWhatSystem.FromInitial;
        /// <summary>
        /// 由哪一個系統觸發
        /// </summary>
        [JsonIgnore]
        public TriggerByWhatSystem TriggerByWhichSystem => TriggerBy;

        /// <summary>
        /// Data Bus for 'byte[]', 'string'
        /// </summary>
        [JsonIgnore]
        private DataRefElementBase allDataRef = null;
        /// <summary>
        /// Data Bus for 'byte[]', 'string'
        /// </summary>
        [JsonIgnore]
        public DataRefElementBase DataBus4Object => allDataRef;

        /// <summary>
        /// 'DataBus4Object' 是 DataRefElement ?
        /// </summary>
        public bool DataBus4ObjectIsBytes => DataBusIsObject && DataBus4Object.IsDataRefElement;
        /// <summary>
        /// 'DataBus4Object' 是 StringDataRefElement ?
        /// </summary>
        public bool DataBus4ObjectIsString => DataBusIsObject && DataBus4Object.IsStringDataRefElement;

        /// <summary>
        /// 當下是以 'DataBus4Object' 為主
        /// <para>可以 cast 成 'DataRefElement' or 'StringDataRefElement'</para>
        public bool DataBusIsObject => DataBus4Object != null;
        /// <summary>
        /// 當下是以 'DataBus4Bytes' 為主
        /// <para>'DataBus4Bytes', 'DataBus4String' 擇一</para>
        /// </summary>
        public bool DataBusIsBytes => DataBus4Bytes != null && DataBus4String == null;
        /// <summary>
        /// 當下是以 'DataBus4String' 為主
        /// <para>'DataBus4Bytes', 'DataBus4String' 擇一</para>
        /// </summary>
        public bool DataBusIsString => DataBus4Bytes == null && DataBus4String != null;

        /// <summary>
        /// Data Bus for 'byte[]'
        /// </summary>
        [JsonIgnore]
        private DataRefElement byteDataRef = null;
        /// <summary>
        /// Data Bus for 'byte[]'
        /// </summary>
        [JsonIgnore]
        public DataRefElement DataBus4Bytes => byteDataRef;

        /// <summary>
        /// Data Bus for 'string'
        /// </summary>
        [JsonIgnore]
        private StringDataRefElement stringDataRef = null;
        /// <summary>
        /// Data Bus for 'string'
        /// </summary>
        [JsonIgnore]
        public StringDataRefElement DataBus4String => stringDataRef;
        #endregion


        #region Constructor
        /// <summary>
        /// Data Bus for Array DataRef 'int[]', 'float[]', 'double[]'
        /// </summary>
        public DataRefEventArgs(string data, DataRefElementBase allDR, object aryValue, TriggerByWhatSystem triggerBy) => InitConstructor(data, allDR, aryValue, triggerBy);
        /// <summary>
        /// Data Bus for 'byte[]', 'string'
        /// </summary>
        public DataRefEventArgs(string data, DataRefElementBase allDR, TriggerByWhatSystem triggerBy) => InitConstructor(data, allDR, null, null, triggerBy);
        /// <summary>
        /// Data Bus for 'byte[]', 'string'
        /// </summary>
        public DataRefEventArgs(string data, DataRefElementBase allDR, DataRefElement byteDR, StringDataRefElement stringDR, TriggerByWhatSystem triggerBy) => InitConstructor(data, allDR, byteDR, stringDR, triggerBy);
        /// <summary>
        /// Data Bus for 'byte[]'
        /// </summary>
        public DataRefEventArgs(string data, DataRefElement byteDR, TriggerByWhatSystem triggerBy) => InitConstructor(data, null, byteDR, null, triggerBy);
        /// <summary>
        /// Data Bus for 'string'
        /// </summary>
        public DataRefEventArgs(string data, StringDataRefElement stringDR, TriggerByWhatSystem triggerBy) => InitConstructor(data, null, null, stringDR, triggerBy);

        /// <summary>
        /// Data Bus for 'byte[]', only for OLD AircraftSystem
        /// </summary>
        public DataRefEventArgs(TriggerByWhatSystem triggerBy, string data, DataRefElement byteDR) => InitConstructor(data, null, byteDR, null, triggerBy);
        /// <summary>
        /// Data Bus for 'string', only for OLD AircraftSystem
        /// </summary>
        public DataRefEventArgs(TriggerByWhatSystem triggerBy, string data, StringDataRefElement stringDR) => InitConstructor(data, null, null, stringDR, triggerBy);

        private void InitConstructor(string data, DataRefElementBase allDR, DataRefElement byteDR, StringDataRefElement stringDR, TriggerByWhatSystem triggerBy) {
            mData = data;
            allDataRef = allDR;
            byteDataRef = byteDR;
            stringDataRef = stringDR;
            AryValue = null;
            TriggerBy = triggerBy;
        }
        private void InitConstructor(string data, DataRefElementBase allDR, object aryValue, TriggerByWhatSystem triggerBy) {
            mData = data;
            allDataRef = allDR;
            byteDataRef = null;
            stringDataRef = null;
            AryValue = aryValue;
            TriggerBy = triggerBy;
        }
        #endregion
    }
}
