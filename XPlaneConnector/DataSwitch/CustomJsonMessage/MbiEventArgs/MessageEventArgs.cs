﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs
{
    /// <summary>
    /// Event 交易過程的資訊傳遞
    /// </summary>
    public class MessageEventArgs : EventArgs {
        #region Data(自定義的訊息)
        /// <summary>
        /// 自定義的訊息
        /// </summary>
        [JsonIgnore]
        protected string mData;
        /// <summary>
        /// 自定義的訊息
        /// </summary>
        [JsonIgnore]
        public string Data { get { return mData; } set { mData = value; } }
        #endregion


        #region Constructor
        public MessageEventArgs() { }
        /// <summary>
        /// Event 傳送的資訊
        /// <para>data :: 自定義的訊息</para>
        /// </summary>
        /// <param name="data">自定義的訊息</param>
        public MessageEventArgs(string data) { mData = data; }
        #endregion


        #region JSON Property
        [JsonProperty("action")]
        public string Action { get; set; }

        [JsonProperty("actionProperty")]
        public string ActionProperty { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("hostLicenseKey")]
        public string HostLicenseKey { get; set; }

        [JsonIgnore]
        public string PropertyBody { get; set; }

        [JsonIgnore]
        public List<PatternProperty> Patterns { get; set; }

        [JsonIgnore]
        public List<string> ParamToRead { get; set; }

        [JsonIgnore]
        public ParamProperty ParamToWrite { get; set; }

        /// <summary>
        /// 本機的註冊資訊
        /// </summary>
        [JsonIgnore]
        public RegistrationInfo RegistrationInfo { get; set; }

        /// <summary>
        /// WebSocket 連線成功後, send connect SUCCESS message to cloud
        /// </summary>
        [JsonIgnore]
        public ConnectMessage Cmsg { get; set; }

        [JsonIgnore]
        public ParamProperty ParamReaded { get; set; }

        [JsonIgnore]
        public ElectricalProperty electricalProperty { get; set; }
        #endregion
    }
}