﻿using MBILibrary.Util.CustomJsonConverter;
using Newtonsoft.Json;
using System;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    public class ElectricalProperty : IResponseBody
    {
        public ElectricalProperty() { }
        public ElectricalProperty(string component, int value)
        {
            Component = component;
            Value = value;
        }
        [JsonProperty("component")]
        public string Component { get; set; }

        [JsonProperty("value")]
        public int Value { get; set; }

        [JsonProperty("a1")]
        public string A1 { get; set; }

        [JsonProperty("a2")]
        public int A2 { get; set; }

        [JsonProperty("a3")]
        public float A3 { get; set; }

        [JsonProperty("a4")]
        public double A4 { get; set; }

        [JsonProperty("abc-def")]
        public bool AbcDef { get; set; }

        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty("UpdatingTime")]
        public DateTime UpdatingTime { get; set; }
        /// <summary>
        /// enable ?
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("UpdatingEnable")]
        public bool UpdatingEnable { get; set; }
    }
}