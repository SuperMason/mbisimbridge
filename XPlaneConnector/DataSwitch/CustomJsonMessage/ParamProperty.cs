﻿using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    public class ParamProperty : IResponseBody
    {
        #region To conditionally serialize a property
        public bool ShouldSerializeParam1() { return Param1 != null; }
        public bool ShouldSerializeParam2() { return Param2 != null; }
        public bool ShouldSerializeParam3() { return Param3 != null; }
        public bool ShouldSerializeParam4() { return Param4 != null; }
        public bool ShouldSerializeParam5() { return Param5 != null; }
        public bool ShouldSerializeParam6() { return Param6 != null; }
        public bool ShouldSerializeParam7() { return Param7 != null; }
        public bool ShouldSerializeParam8() { return Param8 != null; }
        public bool ShouldSerializeParam9() { return Param9 != null; }
        #endregion

        [JsonProperty("param1")]
        public string Param1 { get; set; }

        [JsonProperty("param2")]
        public string Param2 { get; set; }

        [JsonProperty("param3")]
        public string Param3 { get; set; }

        [JsonProperty("param4")]
        public string Param4 { get; set; }

        [JsonProperty("param5")]
        public string Param5 { get; set; }

        [JsonProperty("param6")]
        public string Param6 { get; set; }

        [JsonProperty("param7")]
        public string Param7 { get; set; }

        [JsonProperty("param8")]
        public string Param8 { get; set; }

        [JsonProperty("param9")]
        public string Param9 { get; set; }

    }
}