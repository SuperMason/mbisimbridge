﻿using Newtonsoft.Json;

namespace XPlaneConnector.DataSwitch.CustomJsonMessage
{
    public class ResponseEntity
    {
        [JsonIgnore]
        public string Action { get; set; }

        [JsonIgnore]
        public string ActionProperty { get; set; }

        [JsonProperty("deviceId")]
        public string DeviceId { get; set; }

        [JsonProperty("sessionId")]
        public string SessionId { get; set; }

        [JsonProperty("SessionStatus")]
        public string SessionStatus { get; set; }

        [JsonProperty("responseBody")]
        public IResponseBody ResponseBody { get; set; }
    }
}