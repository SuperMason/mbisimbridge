﻿using MBILibrary.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using XPlaneConnector.DataRef;
using XPlaneConnector.Instructions;
using XPlaneConnector.Models;

namespace XPlaneConnector.Controllers
{
    /// <summary>
    /// 初始化系統參數
    /// </summary>
    public class InitParamsCtrller
    {
        #region 在 Air Manager SimBus 上, 所註冊訂閱的 DataRefs, Commands
        /// <summary>
        /// 在 Air Manager SimBus 上, 所註冊訂閱的 DataRefs
        /// </summary>
        public static List<DataRefElementBase> SubscribedDataRefs = null;
        /// <summary>
        /// 在 Air Manager SimBus 上, 所註冊訂閱的 Commands
        /// </summary>
        public static List<XPlaneCommand> SubscribedCommands = null;
        /// <summary>
        /// 用來承接 X-Plane Response 的 int[], float[], double[] DataRefElements
        /// </summary>
        public static List<ArrayDrVal> InitArrayDrBase = null;
        #endregion


        #region Get 'DataRefElement' and 'StringDataRefElement'
        /// <summary>
        /// Get 'DataRefElement' from Initializing
        /// </summary>
        public static DataRefElement GetDataRefElementFromInit(DataRefElementBase drBase) => GetDataRefElementFromInit(drBase.RawDataRef);
        /// <summary>
        /// Get 'DataRefElement' from Initializing
        /// </summary>
        public static DataRefElement GetDataRefElementFromInit(string name) {
            var res = GetDataRefElementBaseFromInit(name);
            return res == null ? null : res.GetType() == typeof(DataRefElement) ? (DataRefElement)res : null;
        }
        /// <summary>
        /// Get 'StringDataRefElement' from Initializing
        /// </summary>
        public static StringDataRefElement GetStringDataRefElementFromInit(DataRefElementBase drBase) => GetStringDataRefElementFromInit(drBase.RawDataRef);
        /// <summary>
        /// Get 'StringDataRefElement' from Initializing
        /// </summary>
        public static StringDataRefElement GetStringDataRefElementFromInit(string name) {
            var res = GetDataRefElementBaseFromInit(name);
            return res == null ? null : res.GetType() == typeof(StringDataRefElement) ? (StringDataRefElement)res : null;
        }
        private static DataRefElementBase GetDataRefElementBaseFromInit(string name) {
            var res = SubscribedDataRefs == null ? null : SubscribedDataRefs.ToArray().Where(d => d != null && d.RawDataRef.Equals(name)).FirstOrDefault();
            if (res == null) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "DataRef({0}) 並沒有註冊, 無法存取使用 !!!", name)); }
            return res;
        }
        #endregion

        #region constructor
        /// <summary>
        /// 初始化系統參數
        /// </summary>
        static InitParamsCtrller() {
            #region initializing, 將自定義的 DataRef, Command 指令 Load into List, 準備註冊到 Air Manager, X-Plane
            SubscribedDataRefs = new List<DataRefElementBase>();
            SubscribedCommands = new List<XPlaneCommand>();
            InitArrayDrBase = new List<ArrayDrVal>();

            SelfParamsDefinition SelfParams = JsonUtil.GetJsonFileSettings<SelfParamsDefinition>(UtilGlobalFunc.GetCurrentDirectoryJsonFilePath(nameof(SelfParamsDefinition)));
            AddSubscribedCommands(SelfParams.SelfCommands);
            FilterMbiDR(SelfParams.SelfDataRefs); // 新增自定義的 DataRefs (從 SelfParamsDefinition.json 讀取)
            #endregion
        }
        /// <summary>
        /// 將 SelfParamsDefinition.json 裡, 自定義的 DataRef 進行分類
        /// <para>DataRefElement ::: int, float, double, int[], float[], double[]</para>
        /// <para>StringDataRefElement ::: string</para>
        /// </summary>
        private static void FilterMbiDR(List<FilterSelfDR> selfDRs) {
            List<DataRefElementBase> oList = new List<DataRefElementBase>();
            selfDRs.ForEach(sdr => {
                if (sdr.IsStringType)
                { oList.Add(DataRefs.GetStringDataRefElement(sdr.DataRef, sdr.Type.ToLower(), sdr.Writable, sdr.Units, sdr.Description, sdr.StringLength, sdr.Frequency)); }
                else
                { oList.Add(DataRefs.GetDataRefElement(sdr.DataRef, sdr.Type.ToLower(), sdr.Writable, sdr.Units, sdr.Description, sdr.Frequency)); }
            });
            AddSubscribedDataRefs(oList);
        }

        /// <summary>
        /// 初始化系統參數 :: DataRefs, Commands
        /// </summary>
        public InitParamsCtrller() {
            AddSubscribedDataRefs(UtilConstants.TestingMode ? DataRefs.GetAllDataRefs() : DataRefs.GetMustNeedDataRefs());// Subscribe DataRefs for Read/Write
            AddSubscribedCommands(UtilConstants.TestingMode ? Commands.GetAllCommands() : Commands.GetMustNeedCommands());// Add Commands
            UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Initializing... Load in DataRefs({0}), Commands({1})", SubscribedDataRefs.Count, SubscribedCommands.Count));
        }
        #endregion

        /// <summary>
        /// 送去 X-Plane 或 Air Manager 之前, 再檢查一次, 是否存在於註冊清單中 ?
        /// </summary>
        public static bool IsSubscribedDR(string dataRefName) => SubscribedDataRefs.FirstOrDefault(d => d != null && d.RawDataRef.Equals(dataRefName)) != null;

        #region filter duplicated element
        /// <summary>
        /// filter duplicated element
        /// </summary>
        private static void AddSubscribedCommands(List<XPlaneCommand> CmdList) {
            CmdList.ForEach(o => {
                bool contains = SubscribedCommands.ToArray().Any(c => c.Name.Equals(o.Name, StringComparison.OrdinalIgnoreCase));
                if (!contains) { SubscribedCommands.Add(o); }

                #region 相同判斷方法
                //int index = SubscribedCommands.FindIndex(c => c.Name.Equals(o.Name, StringComparison.OrdinalIgnoreCase));
                //if (index < 0) { SubscribedCommands.Add(o); }

                //bool isExist = SubscribedCommands.Exists(c => c.Name.Equals(o.Name, StringComparison.OrdinalIgnoreCase));
                //if (!isExist) { SubscribedCommands.Add(o); }

                //var match = SubscribedCommands.FirstOrDefault(c => c.Name.Equals(o.Name, StringComparison.OrdinalIgnoreCase));
                //if (match == null) { SubscribedCommands.Add(o); }
                #endregion
            });
        }
        /// <summary>
        /// filter duplicated element
        /// <para>注意!!</para>
        /// <para>byte[] 的 data type 會轉換成 string data type</para>
        /// </summary>
        private static void AddSubscribedDataRefs(List<DataRefElementBase> drList) {
            drList.ForEach(o => {
                bool contains = SubscribedDataRefs.ToArray().Any(c => c.DataRef.Equals(o.DataRef, StringComparison.OrdinalIgnoreCase));
                if (!contains) {

                    // 指定 "字串" 的方式 :::
                    // 定義 Type 為 string , 則要給定 StringLength, 否則預設長度為 255
                    // 定義 Type 為 byte[8], 則表示為 string Type, 且 StringLength 為 8

                    // DataType 意義轉換, AM 與 XP 兩套軟體, 針對 string 定義的意義不同
                    // Air Manager 端是 :: 'STRING'
                    // X-Plane     端是 :: 'byte[n]', 每 1 個 byte, X-Plane 都會分拆成 1 個 DataRef, 共 n 個各自獨立監聽回報, 回報會先 response byte[4] 的 float data, 再自行轉成 char (ASCII Code), 取得/設定該 char
                    //                      '9'	-> [0, 0, 100, 66] => byte[4] 轉成 float 為 57, 代表 '9' char 
                    //                      'a' -> [0, 0, 194, 66] => byte[4] 轉成 float 為 97, 代表 'a' char
                    //                      'A' -> [0, 0, 130, 66] => byte[4] 轉成 float 為 65, 代表 'A' char

                    if (o.IsStringType) {
                        SubscribedDataRefs.Add(DataRefs.GetStringDataRefElement(o.DataRef, o.Type, o.Writable, o.Units, o.Description, o.StringLength, o.Frequency)); // 指定 'string' Type
                    } else {
                        if (o.IsArrayByteType) {
                            SubscribedDataRefs.Add(DataRefs.GetStringDataRefElement(o.DataRef, DataRefs.TypeStart_string, o.Writable, o.Units, o.Description, o.ArrayLen, o.Frequency)); // 指定 'byte[8]' Type
                        } else {
                            SubscribedDataRefs.Add(DataRefs.GetDataRefElement(o.DataRef, o.Type, o.Writable, o.Units, o.Description, o.Frequency));
                            if (o.TypeIsArray) { InitArrayDrBase.Add(new ArrayDrVal(o)); }
                        }
                    }
                }
            });
        }
        #endregion
    }
}
