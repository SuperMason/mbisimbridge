﻿using MBILibrary.Enums;
using MBILibrary.Util;
using System.Globalization;
using XPlaneConnector.DataRef;
using XPlaneConnector.Instructions;

namespace XPlaneConnector.Controllers
{
    /// <summary>
    /// Update "X-Plane", "Air Manager" Data
    /// </summary>
    public class WriteDataRefCtrller
    {
        /// <summary>
        /// Update "X-Plane", "Air Manager" Data
        /// </summary>
        public WriteDataRefCtrller()
        { }

        /// <summary>
        /// 從 "初始註冊" 清單中, 撈出已註冊的 DataRef 資訊
        /// </summary>
        public DataRefElementBase GetInitDataRefElementBase(string rawDataRef, object val) {
            DataRefElementBase drBase;
            if (val.ValueIsString()) { drBase = InitParamsCtrller.GetStringDataRefElementFromInit(rawDataRef); } // 註冊總表 ::: string
            else { drBase = InitParamsCtrller.GetDataRefElementFromInit(rawDataRef); } // 註冊總表 ::: int, float, double, int[], float[], double[]
            return drBase;
        }

        /// <summary>
        /// 依據 DataRef 的 指定 Type, 轉換成對應的 data type
        /// </summary>
        public static object AlignDataRefValue(bool TypeIsArray, string drType, object rawValue) {
            object alignValue;
            if (TypeIsArray) {
                // int[], float[], double[]
                if (UtilGlobalFunc.IsTheTypeArray(drType, DataRefs.TypeStart_int)) { alignValue = rawValue.ConvertObjectToIntArray(); } // Convert2Int[]
                else if (UtilGlobalFunc.IsTheTypeArray(drType, DataRefs.TypeStart_byte)) { alignValue = rawValue.ConvertObjectToByteArray(); } // Convert2Byte[]
                else if (UtilGlobalFunc.IsTheTypeArray(drType, DataRefs.TypeStart_float)) { alignValue = rawValue.ConvertObjectToFloatArray(); } // Convert2Float[]
                else if (UtilGlobalFunc.IsTheTypeArray(drType, DataRefs.TypeStart_double)) { alignValue = rawValue.ConvertObjectToDoubleArray(); } // Convert2Double[]
                else { alignValue = rawValue.ConvertObjectToString(); } // Convert2String
            } else {
                // int, float, double, string
                if (UtilGlobalFunc.IsTheSingleType(drType, DataRefs.TypeStart_int)) { alignValue = rawValue.ConvertObjectToInt32(); } // Convert2Int
                else if (UtilGlobalFunc.IsTheSingleType(drType, DataRefs.TypeStart_byte)) { alignValue = rawValue.ConvertObjectToByte(); } // Convert2Byte
                else if (UtilGlobalFunc.IsTheSingleType(drType, DataRefs.TypeStart_float)) { alignValue = rawValue.ConvertObjectToFloat(); } // Convert2Float
                else if (UtilGlobalFunc.IsTheSingleType(drType, DataRefs.TypeStart_double)) { alignValue = rawValue.ConvertObjectToDouble(); } // Convert2Double
                else { alignValue = rawValue.ConvertObjectToString(); } // Convert2String
            }
            return alignValue;
        }

        #region (從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane') for 7 種 "DataRef Write" ::: int, float, double, int[], float[], double[], string
        /// <summary>
        /// 從 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. 必須成功註冊至 'X-Plane' 後, 才能寫入</para>
        /// <para>2. offset 是位移量, 值從 0 開始, user 可以透過 offset 調整寫入 DataRef[i] 的起始位置</para>
        /// <para>3. 與原本的 'Air Manager' interface 銜接, 但用來修改的物件要個別獨立新增一個進行作業, 避免影響初始註冊物件的 read/write</para>
        /// </summary>
        public void NotifyXPlaneValueChangedFromGRPC(TriggerByWhatSystem triggerBy, string funcName, string rawDataRef, object val, int offset = UtilConstants.NullOffset) {
            bool WriteSuccess = false;
            DataRefElementBase drBase = GetInitDataRefElementBase(rawDataRef, val);
            object alignValue = AlignDataRefValue(drBase.TypeIsArray, drBase.Type, val);
            if (drBase != null) { WriteSuccess = true; UpdateXPlaneValue(triggerBy, funcName, drBase, alignValue, offset); }
            if (!WriteSuccess) { UtilGlobalFunc.Log(rawDataRef, triggerBy, funcName); }
        }
        /// <summary>
        /// 更新 X-Plane DataRef value
        /// <para>因為有時候已經送出完成了, 但 X-Plane 值依然沒更新, 所以多送幾遍, 應該就不會掉資料了</para>
        /// <para>預設送 UpdateDataRefValueResendCNT 次</para>
        /// </summary>
        public void UpdateXPlaneValue(TriggerByWhatSystem triggerBy, string funcName, DataRefElementBase dr, object val, int offset = UtilConstants.NullOffset) {
            for (int i = 0; i < UtilConstants.UpdateDataRefValueResendCNT; i++) {
                //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "No.{0} 更新 {1}, value = {2}", i + 1, dr.DataRef, val));
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "No.{0} 更新 {1}, value = {2}", i + 1, dr.DataRef, val));
                NotifyXPlaneValueChanged(triggerBy, funcName, dr, val, offset);
            }
        }
        /// <summary>
        /// 從 'AirForceSystem' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. 必須成功註冊至 'X-Plane' 後, 才能寫入</para>
        /// <para>2. offset 是位移量, 值從 0 開始, user 可以透過 offset 調整寫入 DataRef[i] 的起始位置</para>
        /// <para>3. 與原本的 'Air Manager' interface 銜接, 但用來修改的物件要個別獨立新增一個進行作業, 避免影響初始註冊物件的 read/write</para>
        /// </summary>
        public void NotifyXPlaneValueChanged(TriggerByWhatSystem triggerBy, string funcName, DataRefElementBase drBase, object val, int offset = UtilConstants.NullOffset) {
            if (drBase == null || val == null) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "DataRefObj({0}) or value({1}) is null, {2}.", drBase, val, UtilGlobalFunc.GetTriggerByWhatFunc(triggerBy, funcName))); return; }

            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} : '{1}' Value({2}), offset({3})", UtilGlobalFunc.GetTriggerByWhatFunc(triggerBy, funcName), drBase.RawDataRef, UtilGlobalFunc.GetPrecisionValue(val), offset));
            bool WriteSuccess = false;
            if (drBase.IsStringDataRefElement && drBase.IsStringType && val.ValueIsString()) {
                var sdr = InitParamsCtrller.GetStringDataRefElementFromInit(drBase); // 註冊總表 ::: string
                if (sdr != null) { WriteSuccess = true; sdr.DataRefWriteString(val.ConvertObjectToString(), triggerBy); }
            } else if (drBase.IsDataRefElement) {
                var dr = InitParamsCtrller.GetDataRefElementFromInit(drBase); // 註冊總表 ::: int, float, double, int[], float[], double[]
                if (dr != null) {
                    dr.XPDataRefOffset = offset;
                    if (drBase.IsSingleType && val.ValueIsSingle()) {
                        if (drBase.IsSingleIntType && val.ValueIsInt()) { WriteSuccess = true; dr.DataRefWriteInt(val.ConvertObjectToInt32(), triggerBy); }
                        else if (drBase.IsSingleByteType && val.ValueIsByte()) { WriteSuccess = true; dr.DataRefWriteByte(val.ConvertObjectToByte(), triggerBy); }
                        else if (drBase.IsSingleFloatType && val.ValueIsFloat()) { WriteSuccess = true; dr.DataRefWriteFloat(val.ConvertObjectToFloat(), triggerBy); }
                        else if (drBase.IsSingleDoubleType && val.ValueIsDouble()) { WriteSuccess = true; dr.DataRefWriteDouble(val.ConvertObjectToDouble(), triggerBy); }
                    } else if (drBase.TypeIsArray && val.ValueIsArray()) {
                        if (drBase.IsArrayIntType && val.ValueIsIntArray()) { WriteSuccess = true; dr.DataRefWriteIntArray(val.ConvertObjectToIntArray(), triggerBy, offset); }
                        else if (drBase.IsArrayFloatType && val.ValueIsFloatArray()) { WriteSuccess = true; dr.DataRefWriteFloatArray(val.ConvertObjectToFloatArray(), triggerBy, offset); }
                        else if (drBase.IsArrayDoubleType && val.ValueIsDoubleArray()) { WriteSuccess = true; dr.DataRefWriteDoubleArray(val.ConvertObjectToDoubleArray(), triggerBy, offset); }
                    }
                }
            }

            if (!WriteSuccess) { UtilGlobalFunc.Log(drBase.RawDataRef, triggerBy, funcName); }
        }
        #endregion

    }
}
