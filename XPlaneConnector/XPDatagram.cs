﻿using MBILibrary.Util;
using System;
using System.Collections.Generic;

namespace XPlaneConnector
{
    /// <summary>
    /// 對 X-Plane 傳送 Command, DataRef 指令
    /// </summary>
    public class XPDatagram
    {
        public List<byte> Bytes { get; set; }
        public int Len { get { return Bytes.Count; } }

        #region Constructor
        /// <summary>
        /// 對 X-Plane 傳送 Command, DataRef 指令
        /// </summary>
        public XPDatagram() { Bytes = new List<byte>(); }
        #endregion

        /// <summary>
        /// 取得所有的 Bytes
        /// </summary>
        public byte[] Get() { return Bytes.ToArray(); }

        /// <summary>
        /// 加 1 個 char
        /// <para>把 char 轉成 float 格式的 4 bytes</para>
        /// </summary>
        public void Add(char ch) { Add(ch.ConvertCharToInt32ToFloat().ConvertToByteArray()); }
        /// <summary>
        /// 加 1 個 byte
        /// </summary>
        public void Add(byte value) { Bytes.Add(value); }

        /* 不可以加 double, 因為 X-Plane 的 資料傳送格式為 一組 id 只有 4 bytes 空間可以使用, 而 double 為 8 bytes, 超過限制
        /// <summary>
        /// 加 1 個 double 值, 內部會轉換成 8 個 bytes
        /// <para>double DataType Length :: 8 bytes</para>
        /// </summary>
        public void Add(double value) {
            var b = BitConverter.GetBytes(value);
            if (BitConverter.IsLittleEndian) {
                Bytes.Add(b[0]);
                Bytes.Add(b[1]);
                Bytes.Add(b[2]);
                Bytes.Add(b[3]);
                Bytes.Add(b[4]);
                Bytes.Add(b[5]);
                Bytes.Add(b[6]);
                Bytes.Add(b[7]);
            } else {
                Bytes.Add(b[7]);
                Bytes.Add(b[6]);
                Bytes.Add(b[5]);
                Bytes.Add(b[4]);
                Bytes.Add(b[3]);
                Bytes.Add(b[2]);
                Bytes.Add(b[1]);
                Bytes.Add(b[0]);
            }
        } */
        /// <summary>
        /// 加 1 個 int 值, 內部會轉換成 4 個 bytes
        /// <para>int DataType Length :: 4 bytes</para>
        /// </summary>
        public void Add(int value) { AddBytes(value.ConvertToByteArray()); }
        /// <summary>
        /// 加 1 個 float 值, 內部會轉換成 4 個 bytes
        /// <para>float DataType length :: 4 bytes</para>
        /// </summary>
        public void Add(float value) { AddBytes(value.ConvertToByteArray()); }
        private void AddBytes(byte[] b) {
            if (BitConverter.IsLittleEndian) {
                Bytes.Add(b[0]);
                Bytes.Add(b[1]);
                Bytes.Add(b[2]);
                Bytes.Add(b[3]);
            } else {
                Bytes.Add(b[3]);
                Bytes.Add(b[2]);
                Bytes.Add(b[1]);
                Bytes.Add(b[0]);
            }
        }
        
        /// <summary>
        /// 加 1 個 string 值, 內部會轉換成 n 個 bytes
        /// </summary>
        public void Add(string value) {
            foreach (var character in value)
            { Bytes.Add((byte)character); }
            Bytes.Add(0x00);
        }

        /// <summary>
        /// 外部已經轉好 byte[], 準備寫入 X-Plane
        /// </summary>
        public void Add(byte[] value) {
            foreach (var character in value)
            { Bytes.Add(character); }
        }
        /// <summary>
        /// 補齊 0, 至指定的 bytes 數量
        /// </summary>
        /// <param name="count">指定的 bytes 數量</param>
        /// <param name="filler">預設補 0</param>
        public void FillTo(int count, byte filler = 0x00) {
            for (var i = Bytes.Count; i < count; i++)
            { Bytes.Add(filler); }
        }
    }
}