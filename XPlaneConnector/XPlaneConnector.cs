﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using MBILibrary.Enums;
using MBILibrary.Util;
using System.Globalization;

namespace XPlaneConnector
{
    /// <summary>
    /// 建立與 X-Plane System 的連線溝通橋梁
    /// </summary>
    public class XPlaneConnector : IDisposable
    {
        #region Properties
        /// <summary>
        /// 本機端 : 負責監聽 X-Plane's Response
        /// </summary>
        private UdpClient ListenXPlaneServer = null;
        /// <summary>
        /// 本機端 : 負責發送訊息給 X-Plane
        /// </summary>
        public UdpClient LocalSendMsgToXPlane { get; private set; } = null;
        /// <summary>
        /// X-Plane 的 Address
        /// <para>IP : 127.0.0.1</para>
        /// <para>Port : 49000</para>
        /// </summary>
        private readonly IPEndPoint XPlaneEP = null;

        /// <summary>
        /// 計算測試連線的次數
        /// </summary>
        private int WorkerCounter = 0;

#if DEBUG
        #region [Debug Mode], 監控 UDP Receiving, Parsing 時間
        /// <summary>
        /// [Debug Mode], 監控 UDP 取得的平均花費時間
        /// </summary>
        private readonly long[] UdpRecv = null;
        /// <summary>
        /// [Debug Mode], 監控 UDP 處理的平均花費時間
        /// </summary>
        private readonly long[] UdpParser = null;
        /// <summary>
        /// [Debug Mode], Receive UDP data, Average spending Ticks
        /// </summary>
        private readonly List<float> GetReceivingUDPResultAvgVal = null;
        /// <summary>
        /// [Debug Mode], Parse UDP data, Average spending Ticks
        /// </summary>
        private readonly List<float> GetParsingUDPResultAvgVal = null;
        #endregion
#endif


        #region 判斷是否成功與 X-Plane 連線的重要 Flag 參數
        /// <summary>
        /// 取得 X-Plane 的回覆動作
        /// <para>true  : 取得回覆, 有 2 種情況 :: 1.進入飛航畫面後(正規訊息回覆),  2.都沒有開啟 X-Plane(Socket沒有連上)</para>
        /// <para>false : 尚未回覆, 有 1 種情況 :: 1.X-Plane剛開啟, 還停留在選單畫面上</para>
        /// </summary>
        public bool GetXPlaneReply = false;
        /// <summary>
        /// 是否已經與 X-Plane 連線成功 ?
        /// <para>true : when using 1 DataRef and Testing connection Success</para>
        /// <para>false : when using n DataRef and X-Plane be shut down</para>
        /// </summary>
        public bool ConnectOK = false;
        #endregion


        #region 取得本機正在聽的 IP:Port
        private IPEndPoint InitIPAddress => new IPEndPoint(IPAddress.Any, 0);
        /// <summary>
        /// 取得本機正在聽的 IP:Port
        /// </summary>
        private IPEndPoint ClientEP => (LocalSendMsgToXPlane != null && LocalSendMsgToXPlane.Client != null && !SocketIsDisposed(LocalSendMsgToXPlane.Client))
                    ? (IPEndPoint)LocalSendMsgToXPlane.Client.LocalEndPoint : InitIPAddress;
        /// <summary>
        /// 取得本機正在聽的 IP:Port
        /// </summary>
        public string LocalIPsendPort => string.Format(CultureInfo.CurrentCulture, UtilConstants.IPPortFormat, ClientEP.Address, string.Format(CultureInfo.CurrentCulture, UtilXPlane.highlightPattern, ClientEP.Port));
        #endregion


        #region Tasks ::: serverReceiveAsyncTask, obServerTask
        /// <summary>
        /// 用來同時控制 serverReceiveAsyncTask, obServerTask
        /// </summary>
        private CancellationTokenSource CancellTokenSource = null;
        /// <summary>
        /// 取得 serverReceiveAsyncTask 的當下狀態
        /// </summary>
        public TaskStatus ServerReceiveAsyncTaskStatus => (serverReceiveAsyncTask == null) ? TaskStatus.Canceled : serverReceiveAsyncTask.Status;
        /// <summary>
        /// 取得 obServerTask 的當下狀態
        /// </summary>
        public TaskStatus ObserverTaskStatus => (obServerTask == null) ? TaskStatus.Canceled : obServerTask.Status;
        /// <summary>
        /// Initialize the communication with X-Plane
        /// </summary>
        private Task serverReceiveAsyncTask = null;
        /// <summary>
        /// Start listening for "DataRefs", 每隔 1 秒, 就將已經訂閱的所有參數掃描過一次, 去問 X-Plane 是否有值的異動
        /// </summary>
        private Task obServerTask = null;
        #endregion


        /// <summary>
        /// 當下已經訂閱的所有參數列表 ::: 單一值、Array值
        /// <para>注意 !!!</para>
        /// <para>1. 單一值 :: xxx/xxx/xxx。 DataType ::: 'byte', 'int', 'float', 'double'</para>
        /// <para>2. Array值 :: xxx/xxx/xxx[i]。 DataType ::: 'string', 'byte[]', 'int[]', 'float[]', 'double[]'</para>
        /// <para>注意 !!</para>
        /// <para>Array值 會改成 xxx/xxx/xxx[i] 的主因是, X-Plane 是以個別 [i] 為回覆對象</para>
        /// <para>所以才會在註冊時, 自動分拆 [0] ~ [i] 個</para>
        /// </summary>
        private static List<DataRefElement> DataRefs = null;
        /// <summary>
        /// 取得目前已經訂閱的所有參數個數 :: DataRefs.Count
        /// </summary>
        public static int SubscribeListeningCount => DataRefs.IsListEmpty() ? 0 : DataRefs.Count;

        /// <summary>
        /// 取得 MBISimBridge 與 X-Plane 之間的互動狀態
        /// </summary>
        public static XPlaneCurrentStatusEnum ConnStatus => (SubscribeListeningCount <= 1) ? XPlaneCurrentStatusEnum.WaitingToConnect : XPlaneCurrentStatusEnum.BeClosed;

        /// <summary>
        /// 紀錄 XPlaneConnector 與 X-Plane Communication 的最後一次更新時間
        /// <para>外部程式, 可透過此參數取得與 X-Plane 互動的最新資訊</para>
        /// </summary>
        public DateTime LastReceive { get; internal set; }
        /// <summary>
        /// 紀錄 XPlaneConnector 與 X-Plane Communication 的最後一次更新的 response.Buffer data
        /// <para>外部程式, 可透過此參數取得與 X-Plane 互動的最新資訊</para>
        /// </summary>
        public byte[] LastBuffer { get; internal set; }
        #endregion


        #region Event Handler
        public delegate void RawReceiveHandler(byte[] raw);
        /// <summary>
        /// 取得當下 X-Plane 的 feedback raw data
        /// </summary>
        public event RawReceiveHandler OnRawReceive;

        public delegate void DataRefReceived(DataRefElement dataRef);
        /// <summary>
        /// DataRef 的 value 有異動發生時, 才會觸發此 event
        /// </summary>
        public event DataRefReceived OnDataRefReceived;

        /// <summary>
        /// X-Plane 的 Log
        /// </summary>
        /// <param name="message"></param>
        public delegate void LogHandler(string message);
        /// <summary>
        /// X-Plane 的 Log
        /// </summary>
        public event LogHandler OnLog;

        /// <summary>
        /// X-Plane 的連線狀態
        /// </summary>
        /// <param name="sender">connection物件本身</param>
        /// <param name="status">連線狀態</param>
        /// <param name="connStatus">連線敘述</param>
        /// <param name="currentStatus">取得 MBISimBridge 與 X-Plane 之間的互動狀態</param>
        public delegate void NotifyConnectionStatus(XPlaneConnector sender, bool status, XPlaneConnStatusEnum connStatus, XPlaneCurrentStatusEnum currentStatus);
        /// <summary>
        /// X-Plane 的連線狀態
        /// </summary>
        public event NotifyConnectionStatus OnXPlaneConnectionStatus;
        #endregion

        /// <summary>
        /// 這邊必須準確抓到 Single or Array[i] DataRef Object
        /// </summary>
        public static DataRefElement GetDataRef(string DataRefName) => DataRefs.ToArray().Where(d => d != null && d.DataRef.Equals(DataRefName)).FirstOrDefault();

        #region Constructor
        /// <summary>
        /// Constructor
        /// <para>X-Plane default IP : 127.0.0.1 (localhost)</para>
        /// <para>X-Plane default Udp Port : 49000</para>
        /// </summary>
        /// <param name="ip">IP of the machine running X-Plane, default 127.0.0.1 (localhost)</param>
        /// <param name="xplanePort">Port the machine running X-Plane is listening for, default 49000</param>
        public XPlaneConnector(string ip = UtilConstants.IP_127_0_0_1, int xplanePort = UtilConstants.DefaultXPlaneUDPPort) {
            XPlaneEP = new IPEndPoint(IPAddress.Parse(ip), xplanePort);
            DataRefs = new List<DataRefElement>();
            OnRawReceive += GetRawReceived;
            OnDataRefReceived += GetDataRefReceived;
#if DEBUG
            if (UtilConstants.appConfig.Debug4BigMason) {
                UdpRecv = new long[2] { 0, 0 };
                UdpParser = new long[2] { 0, 0 };
                GetReceivingUDPResultAvgVal = new List<float>();
                GetParsingUDPResultAvgVal = new List<float>();
            }
#endif
        }
        #endregion


        #region Start & Stop X-Plane Connection
        /// <summary>
        /// 解除所有的 DataRefs Subscribe(訂閱)
        /// </summary>
        private void UnSubscribeAllDataRefs() {
            if (SubscribeListeningCount == 0) { return; } // 已經沒有任何訂閱

            #region 持續做到全部都取消訂閱為止
            int startCNT = SubscribeListeningCount;
            while (SubscribeListeningCount != 0) {
                foreach (var dr in DataRefs.ToArray())
                { UnSubscribe(dr); }
            }
            //OnLog?.Invoke($"No.{WorkerCounter} UnSubscribe ALL done, DataRefs Count({startCNT} -> {SubscribeListeningCount})");
            #endregion
        }

        /// <summary>
        /// Stop the communication with the X-Plane
        /// </summary>
        /// <param name="timeout">2 秒內, 所有的 Task 皆必須完成作業</param>
        public void Stop(int timeout = 2000) {
            if (ConnectOK && DataRefs.Count == 1) { UnSubscribeAllDataRefs(); } // 解除所有的 Subscribe(訂閱)
            Dispose();

            #region 等待 serverReceiveAsyncTask, obServerTask 2 項作業完成後, 將 Cancell Token 解除, 確保 2 項作業真的結束了
            Task.WaitAll(new[] { serverReceiveAsyncTask, obServerTask }, timeout);
            //OnLog?.Invoke($"No.{WorkerCounter} serverReceiveAsyncTask({ServerReceiveAsyncTaskStatus}), obServerTask({ObserverTaskStatus})");
            serverReceiveAsyncTask.Dispose();
            obServerTask.Dispose();
            #endregion
        }
        /// <summary>
        /// <para>如果 "尚未" 連線時, 會以 1 個 DataRef 進行連線測試</para>
        /// <para>如果 "確認" 連線時, 會以 n 個 DataRefs 進行連線讀取</para>
        /// <para>1. Initialize the communication with X-Plane</para>
        /// <para>2. Start listening for "DataRefs"</para>
        /// </summary>
        public void Start(int workerCounterNo) {
            try {
                WorkerCounter = workerCounterNo;
                // Create & Initialize 2 個 Local UDP Clients :::
                // 1. LocalSendMsgToXPlane 負責 send data,
                // 2. ListenXPlaneServer 負責 recevie data
                LocalSendMsgToXPlane = new UdpClient(); // 本機端 : 負責發送訊息給 X-Plane
                //LocalSendMsgToXPlane.ExclusiveAddressUse = false;
                //LocalSendMsgToXPlane.Client.SetSocketOption(SocketOptionLevel.Udp, SocketOptionName.NoDelay, 1);
                //LocalSendMsgToXPlane.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
                //LocalSendMsgToXPlane.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 1);
                LocalSendMsgToXPlane.Connect(XPlaneEP.Address, XPlaneEP.Port); // 本機端 : 嘗試與 X-Plane 建立連線
                if (ClientEP == InitIPAddress)
                { OnLog?.Invoke($"UDP Client 沒有與 X-Plane 連線成功 : {LocalIPsendPort}"); }
                else {
                    ListenXPlaneServer = new UdpClient(ClientEP); // 本機端 : 負責監聽 X-Plane's Response, 在 Local 的 Port 與 LocalSendMsgToXPlane 一樣
                    //ListenXPlaneServer = new UdpClient();
                    //ListenXPlaneServer.ExclusiveAddressUse = false;
                    //ListenXPlaneServer.Client.SetSocketOption(SocketOptionLevel.Udp, SocketOptionName.NoDelay, 1);
                    //ListenXPlaneServer.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
                    //ListenXPlaneServer.Client.Bind(ClientEP);
                    //ListenXPlaneServer.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastTimeToLive, 1);
                    CancellTokenSource = new CancellationTokenSource();
                    string communicationWithXPlaneMsg = $"No.{WorkerCounter} Listening for X-Plane at Local({LocalIPsendPort})";
                    string listeningDataRefsMsg = $"No.{WorkerCounter} Listening for updating 'DataRefs'";

                    if (!ConnectOK) {   // 尚未確認已經與 X-Plane 連線, 所以用 1 個 DataRef 進行連線測試
                        #region Listening for X-Plane Server
                        if (CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource)) {
                            serverReceiveAsyncTask = Task.Factory.StartNew(async () => {
                                while (!ConnectOK && CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource) && !CancellTokenSource.IsCancellationRequested) {
                                    try {
                                        string tmpIP = XPlaneEP.Address.ToString();
                                        if (!UtilNetwork.PingTarget(tmpIP))
                                        { throw new Exception(string.Format(CultureInfo.CurrentCulture, "'{0}' is NOT available.", tmpIP)); }

                                        if (ListenXPlaneServer != null && ListenXPlaneServer.Client != null && !SocketIsDisposed(ListenXPlaneServer.Client)) {
                                            GetXPlaneReply = false;// false : X-Plane尚未回覆, 情況 :: 1.X-Plane剛開啟, 還停留在選單畫面上
                                            OnLog?.Invoke($"No.{WorkerCounter} waiting for X-Plane...");
                                            var response = await ListenXPlaneServer.ReceiveAsync().ConfigureAwait(false);
                                            GetXPlaneReply = true; // true : 取得X-Plane回覆, 情況 :: 1.進入飛航畫面後(正規訊息回覆)
                                            ConnectOK = true;
                                            OnXPlaneConnectionStatus?.Invoke(this, ConnectOK, XPlaneConnStatusEnum.ConnectedGetDataRefs, ConnStatus);
                                            OnLog?.Invoke($"{communicationWithXPlaneMsg} is Ready !!! ReNew Connection for ALL DataRefs");
                                            Stop();
                                            break;
                                        }
                                    } catch (Exception ex) {
                                        GetXPlaneReply = true; // true : 取得X-Plane回覆, 情況 :: 2.都沒有開啟 X-Plane(Socket沒有連上)
                                        LogException($"{communicationWithXPlaneMsg} is NOT available({ConnStatus}), Message : {ex.Message}", null);
                                        OnXPlaneConnectionStatus?.Invoke(this, false, XPlaneConnStatusEnum.DisConnected, ConnStatus); // false 尚未取得連線, 通知 "XPlaneSystem"
                                        Stop();
                                        break;
                                    }
                                }
                            }, CancellTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                        }
                        #endregion

                        #region 監控 X-Plane 是否有被關掉, 每隔 0.1 秒檢查一遍
                        if (CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource)) {
                            obServerTask = Task.Factory.StartNew(async () => {
                                while (!ConnectOK && CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource) && !CancellTokenSource.IsCancellationRequested) {
                                    try {
                                        if (SubscribeListeningCount != 0)
                                        { DataRefs.ToArray().Where(dr => dr != null && dr.IsOverMaxAge).ToList().ForEach(dr => RequestDataRef(dr)); }
                                        await Task.Delay(DataRefElementBase.MaxAge).ConfigureAwait(false);
                                    } catch (Exception ex) {
                                        LogException($"{listeningDataRefsMsg}", ex);
                                        OnXPlaneConnectionStatus?.Invoke(this, false, XPlaneConnStatusEnum.DisConnected, ConnStatus); // false 尚未取得連線, 通知 "XPlaneSystem"
                                        Stop();
                                        break;
                                    }
                                }
                            }, CancellTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                        }
                        #endregion
                    } else {   // 這邊一定是已經確認成功與 X-Plane 連線, 會將 All DataRefs Load in
                        #region Listening for X-Plane Server
                        if (CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource)) {
                            serverReceiveAsyncTask = Task.Factory.StartNew(async () => {
                                while (ConnectOK && CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource) && !CancellTokenSource.IsCancellationRequested) {
                                    try {
                                        if (ListenXPlaneServer != null && ListenXPlaneServer.Client != null && !SocketIsDisposed(ListenXPlaneServer.Client)) {
#if DEBUG
                                            if (UtilConstants.appConfig.Debug4BigMason)
                                            { UtilConstants.DebugMeasureProcessTime.Start(); }
#endif
                                            var response = await ListenXPlaneServer.ReceiveAsync().ConfigureAwait(false); // async receive 方式, 如果 X-Plane 尚未回覆時, 會 pending 在這一行
                                            if (response == null || response.Buffer == null || response.Buffer.Length == 0) { continue; }
                                            LastBuffer = response.Buffer; // 真的接到 X-Plane response data 才會繼續 run 到這一行, response data 一次最多 1469 bytes (183 個 IDs)
                                            //LastBuffer = ListenXPlaneServer.Receive(ref XPlaneEP); // sync receive 方式

#if DEBUG
                                            if (UtilConstants.appConfig.Debug4BigMason) {
                                                UtilConstants.DebugMeasureProcessTime.Stop();
                                                if (UdpRecv[0] == 0 && UdpRecv[1] == 0) { // 第 1 次取得值
                                                    UdpRecv[0] = UtilConstants.DebugMeasureProcessTime.ElapsedTicks;
                                                    UdpRecv[1] = UdpRecv[0];
                                                } else { // 第 2 次之後
                                                    UdpRecv[0] = (UdpRecv[0] + UdpRecv[1]) / 2;
                                                    UdpRecv[1] = UtilConstants.DebugMeasureProcessTime.ElapsedTicks;
                                                }
                                                GetReceivingUDPResultAvgVal.Add(((UdpRecv[0] + UdpRecv[1]) / 2) * 1000000F / Stopwatch.Frequency);
                                                //OnLog?.Invoke(string.Format(CultureInfo.CurrentCulture, "'Receiving UDP streaming data' Average spending Ticks : {0:n3} MicroSeconds", GetReceivingUDPResultAvgVal.Average()));
                                            }
#endif
                                            // X-Plane Response 的機制是
                                            //      1. 一次最多 1469 bytes, 如果超過就會切成多個 1469, 然後 第一包 1469 平均回覆時間約 100 ms, 剩下的 1469, 每包的平均回覆時間是 10 ms
                                            //      2. 所以一次註冊多 or 少個 DataRefs, 每次全部刷新的速度皆為 100 ms
                                            //      3. 看要如何要求 X-Plane 的 Response Time 再快一點
                                            //      4. 剛開啟 X-Plane 時, Response Time 非常快, 平均回覆時間是 小於 10 ms
                                            //      4.1. 所以如果 X-Plane 開啟經過很長時間之後, 平均回覆時間約 100 ms, 如同第 1 點提的...
#if DEBUG
                                            if (UtilConstants.appConfig.Debug4BigMason)
                                            { UtilConstants.DebugMeasureProcessTime.Restart(); }
#endif
                                            var header = Encoding.UTF8.GetString(LastBuffer, 0, 4); // 取前 4 個 bytes 檢查訊息是否為 X-Plane 發出的 UDP data
                                            if (header.Equals(UtilXPlane.XPlaneResponseHeader)) { // 只處理 "RREF" 開頭的 UDP data, 其餘訊息一律忽略不處理
                                                LastReceive = DateTime.Now;
                                                OnRawReceive?.Invoke(LastBuffer);
                                                ParsingResponse(header, LastBuffer);
                                                if (!UtilConstants.appConfig.CpuUsageFullSpeed) { SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(UtilConstants.appConfig.CpuUsageWaitingMS)); }
                                            } else { OnLog?.Invoke($"Not X-Plane's Response Message in serverReceiveAsyncTask while loop : {header}"); }
#if DEBUG
                                            if (UtilConstants.appConfig.Debug4BigMason) {
                                                UtilConstants.DebugMeasureProcessTime.Stop();
                                                if (UdpParser[0] == 0 && UdpParser[1] == 0) { // 第 1 次取得值
                                                    UdpParser[0] = UtilConstants.DebugMeasureProcessTime.ElapsedTicks;
                                                    UdpParser[1] = UdpParser[0];
                                                } else { // 第 2 次之後
                                                    UdpParser[0] = (UdpParser[0] + UdpParser[1]) / 2;
                                                    UdpParser[1] = UtilConstants.DebugMeasureProcessTime.ElapsedTicks;
                                                }
                                                GetParsingUDPResultAvgVal.Add(((UdpParser[0] + UdpParser[1]) / 2) * 1000000F / Stopwatch.Frequency);
                                                //OnLog?.Invoke(string.Format(CultureInfo.CurrentCulture, "'Parsing UDP streaming data' Average spending Ticks : {0:n3} MicroSeconds", GetParsingUDPResultAvgVal.Average()));
                                            }
#endif
                                        }
                                    } catch (Exception ex) {
                                        ConnectOK = false;
                                        LogException($"{communicationWithXPlaneMsg} is NOT available({ConnStatus}), Message : {ex.Message}", null);
                                        OnXPlaneConnectionStatus?.Invoke(this, false, XPlaneConnStatusEnum.DisConnected, ConnStatus); // false 尚未取得連線, 通知 "XPlaneSystem"
                                        Stop();
                                        break;
                                    }
                                }
                            }, CancellTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                        }
                        #endregion

                        #region 監控 X-Plane 是否有被關掉, 每隔 0.1 秒檢查一遍
                        if (CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource)) {
                            obServerTask = Task.Factory.StartNew(async () => {
                                while (ConnectOK && CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource) && !CancellTokenSource.IsCancellationRequested) {
                                    try {
                                        if (SubscribeListeningCount != 0)
                                        { DataRefs.ToArray().Where(dr => dr != null && dr.IsOverMaxAge).ToList().ForEach(dr => RequestDataRef(dr)); }
                                        await Task.Delay(DataRefElementBase.MaxAge).ConfigureAwait(false);
                                    } catch (Exception ex) {
                                        ConnectOK = false;
                                        LogException($"{listeningDataRefsMsg}", ex);
                                        OnXPlaneConnectionStatus?.Invoke(this, false, XPlaneConnStatusEnum.DisConnected, ConnStatus); // false 尚未取得連線, 通知 "XPlaneSystem"
                                        Stop();
                                        break;
                                    }
                                }
                            }, CancellTokenSource.Token, TaskCreationOptions.LongRunning, TaskScheduler.Default);
                        }
                        #endregion
                    }
                }
            } catch (Exception ex) { LogException($"No.{WorkerCounter} Start() function", ex); }
        }
        #endregion


        /// <summary>
        /// 監控 X-Plane 是否有被關掉, 每隔 0.1 秒檢查一遍, 將已經訂閱的所有參數 send to X-Plane
        /// <para>X-Plane 會依據每一個 DataRef.Frequency 的設定值, response DataRef</para>
        /// <para>A dataref Request should be 413 bytes long</para>
        /// </summary>
        private void RequestDataRef(DataRefElement dr) {
            if (dr != null) {
                //X-Plane will expect a packet of 413 bytes made up of:
                //  (1)    5 byte header "RREF\0"。  label: null terminated 4 chars (5 bytes), e.g. "RREF\0"
                //  (2)    4 bytes for the XINT of dref_freq。  frequency: int (4 bytes)
                //  (3)    4 bytes for the XINT of dref_sender_index。  index: int (4 bytes)
                //  (4)    400 bytes for the dref_string[400] - the dataref padded with '\0' to the last element。  dataref name: char (400 bytes)
                // 固定的 bytes 順序 :: Header -> Frequency -> Id -> DataRef, 剩下的補滿至 413 bytes
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneResponseHeader); // header
                dg.Add(dr.Frequency); // dref_freq ::: IS ACTUALLY 'THE NUMBER OF TIMES PER SECOND', YOU WANT X-PLANE TO SEND THIS DATA!
                dg.Add(dr.Id); // dref_sender_index ::: is the integer code you want X-Plane to send back with the dataref value, so you can tell WHICH dataref X-Plane is giving you. (since you are likely to ask for MANY different datarefs.)
                dg.Add(dr.DataRef); // dref_string[400] ::: is the 'dataref string' that you want X-Plane to send to you.
                dg.FillTo(UtilXPlane.XPlaneRREFMessageBodyLength); // dref_string[400] 不足補齊
                ClientSend(dg);
                //OnLog?.Invoke($"Requested {dataref.DataRef}@{dataref.Frequency} Frequency with Id:{dataref.Id}");
            }
        }

        /// <summary>
        /// 處理由 X-Plane 回覆的訊息, 依據 id 去更新 DataRef 的值
        /// <para>1. Single(Float type) have a maximum precision of 7 digits</para>
        /// <para>2. Float Type 的 大小為 4 個 bytes</para>
        /// <para>3. X-Plane 回覆的值都是以 float 為格式, 所以只有 1 種 :: 全部用 float 處理, 因為一組只有 4 個 bytes</para>
        /// <para>4. X-Plane 不可能回覆 Double 值, 因為一組只有 4 個 bytes, 而 Double 必須要 8 bytes 才能儲存</para>
        /// <para>5. Air Manager 需要的回覆值, 皆以 Float Data 為基礎, 再進行 Converter to 即可</para>
        /// </summary>
        /// <param name="header">XPlaneResponseHeader :: 'RREF'</param>
        /// <param name="buffer">X-Plane 當次的 feedback response data</param>
        private void ParsingResponse(string header, byte[] buffer) {
            if (!header.Equals(UtilXPlane.XPlaneResponseHeader)) { return; } // Ignore other messages, only care "RREF" messages

            byte[] IdValue = buffer.Skip(5).Take(buffer.Length - 5).ToArray(); // X-Plane 每次回覆的 bytes 數量不一定, 但 buffer 前 5 bytes 為 header('RREF'), 實際 data 從第 5 byte 開始, Including tailing 0
            foreach (byte[] DataRefBytes in IdValue.Slices(8)) { // 每 8 個 bytes 為一組 ::: ID + Value
                int ID = DataRefBytes.ConvertByteArrayToInt(); // 取得 DataRef's ID, 前 4 個 bytes 為 ID
                var DR = DataRefs.ToArray().FirstOrDefault(dr => dr != null && dr.Id == ID); // 依據 ID 去撈出對應的 DataRef 進行值的更新
                if (DR != null) {
                    byte[] Value = DataRefBytes.Skip(4).Take(4).ToArray(); // 後 4 個 bytes 為 Value :: X-Plane 回覆的 Value 值都是以 float 為格式, 所以只有 1 種 :: 先用 float 處理, 因為一組只有 4 個 bytes 儲存 value
                    float RawFloat = Value.ConvertByteArrayToFloat();
                    if (!DR.RawType.ToLower().Equals(Instructions.DataRefs.TypeStart_float))
                    { Value = ConvertFloat2RightTypeBytes(DR, RawFloat); } // Value 應該置換成正確的 Type byte[4] data
#if DEBUG
                    if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage) {
                        //OnLog?.Invoke(string.Format(CultureInfo.CurrentCulture, "第一時間 {0} 的 Response Data :: ID({1}), '{2}', Value({3}), '{4}'", UtilConstants.XPlaneSoftware, DR.Id, DR.RawType, RawFloat, DR.DataRef));
                    }
#endif
                    // Value 已經依據 Type 置換成正確的 byte[4] data
                    if (DR.Update(ID, Value)) { OnDataRefReceived?.Invoke(DR); } // value 有更新時, 才會同步通知 Air Manager, 若沒有更新, 該次就不通知

                }
            }
        }
        /// <summary>
        /// X-Plane 聽出來的 byte[4] 都是 float 轉出來的, 所以要先把 byte[4] -> float, 再把 float 轉成該 DataRef RawType 的值
        /// <para>將 raw float data, 依據該 DataRefElement 的 RawType, 轉置成 int, byte, float, double 所應該呈現的 byte[4]</para>
        /// </summary>
        private byte[] ConvertFloat2RightTypeBytes(DataRefElement dr, float RawFloat) {
            byte[] OutputByDataRefRawType = UtilConstants.InitialBytesValue4Float;
            // Debug 使用... 抓 非 0 的 DataRef 進行值轉換驗證
            //if (RawFloat != 0) { Console.WriteLine(RawFloat); }
            if (dr.IsSingleIntType) { OutputByDataRefRawType = RawFloat.ConvertFloatToInt32().ConvertToByteArray(); }
            else if (dr.IsSingleByteType) { OutputByDataRefRawType = RawFloat.ConvertFloatToByte().ConvertToByteArray(); }
            else if (dr.IsSingleFloatType) { OutputByDataRefRawType = RawFloat.ConvertToByteArray(); }
            else if (dr.IsSingleDoubleType) { OutputByDataRefRawType = RawFloat.ConvertFloatToDouble().ConvertToByteArray(); }
            return OutputByDataRefRawType;
        }

        #region Command Function()
        /// <summary>
        /// Send a command
        /// </summary>
        /// <param name="command">Command to send</param>
        public void SendCommand(XPlaneCommand command) {
            if (command == null) { throw new ArgumentNullException(nameof(command)); }

            try {
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneCommandHeader);
                dg.Add(command.Name);
                SendDatagramToXP(dg);
            } catch (Exception ex) { LogException("SendCommand() function", ex); }
        }
        /// <summary>
        /// Send a Command continuously. Use return parameter to cancel the sending cycle
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <returns>Token to cancel the executing, User 可以在外面, 透過 token cancel 後馬上強制停止</returns>
        public CancellationTokenSource StartContinuouslyCommand(XPlaneCommand command) {
            var tokenSource = new CancellationTokenSource();

            if (tokenSource != null && !CancellationTokenSourceIsDisposed(tokenSource)) {
                Task.Run(() => {
                    while (tokenSource != null && !CancellationTokenSourceIsDisposed(tokenSource) && !tokenSource.IsCancellationRequested) {
                        try
                        { SendCommand(command); }
                        catch (Exception ex) {
                            LogException("StartContinuouslyCommand() function", ex);
                            StopContinuouslyCommand(tokenSource);
                            break;
                        }
                    }
                }, tokenSource.Token);
            }

            return tokenSource;
        }
        public void StopContinuouslyCommand(CancellationTokenSource tokenSource) {
            if (tokenSource != null && !CancellationTokenSourceIsDisposed(tokenSource)) {
                tokenSource?.Cancel();
                tokenSource?.Dispose();
            }
        }
        #endregion


        private void LogException(string msg, Exception ex) {
            if (ex == null) { OnLog?.Invoke($"{msg}"); }
            else { OnLog?.Invoke($"{msg}, Exception ::: {ex.Message}{Environment.NewLine}StackTrace Exception ::: {ex.StackTrace}"); }
        }


        #region Subscribe(訂閱), UnSubscribe(取消訂閱) Function
        /// <summary>
        /// 'byte[]' Subscribe
        /// <para>notification will be sent every time the value changes</para>
        /// </summary>
        public void Subscribe(DataRefElement dr, int frequency = -1, Action<DataRefElement, byte[]> onChange = null) {
#if DEBUG
            if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage) {
                string ShowArrayIdx = dr.ArrayIdx == DataRefElementBase.ArrayIdxDefault ? string.Empty : string.Format(CultureInfo.CurrentCulture, ", ArrayIdx({0})", dr.ArrayIdx);
                OnLog?.Invoke($"No.{WorkerCounter} {UtilConstants.XPlaneSoftware} Subscribe 'byte[]' :: ID({dr.Id}), '{dr.DataRef}', '{dr.Type}'{ShowArrayIdx}");
            }
#endif
            if (dr == null) { throw new ArgumentNullException(nameof(dr)); }
            if (onChange != null) { dr.OnValueChange += (e, v) => { onChange(e, v); }; } // 當 data 有異動時, 會由該 DataRef 自行 fire event 出來, 這邊再同步至 UI
            if (frequency > 0) { dr.Frequency = frequency; }
            if (dr != null) {
                var existed = DataRefs.ToArray().Where(d => d != null && d.DataRef.Equals(dr.DataRef)).FirstOrDefault();
                if (existed == null) { DataRefs.Add(dr); }
            }
        }
        /// <summary>
        /// 'string' Subscribe
        /// <para>notification will be sent every time the value changes</para>
        /// </summary>
        /// <param name="sdr">DataRef to subscribe to</param>
        /// <param name="frequency">Times per seconds X-Plane will be sending this value</param>
        /// <param name="onChange">Callback invoked every time a change in the value is detected</param>
        public void Subscribe(StringDataRefElement sdr, int frequency = -1, Action<StringDataRefElement, string> onChange = null) {
#if DEBUG
            if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage)
            { OnLog?.Invoke($"No.{WorkerCounter} {UtilConstants.XPlaneSoftware} Subscribe 'string' :: '{sdr.DataRef}', '{sdr.Type}', StringLength({sdr.StringLength})"); }
#endif
            if (sdr == null) { throw new ArgumentNullException(nameof(sdr)); }
            if (onChange != null) { sdr.OnValueChange += (e, v) => { onChange(e, v); }; } // 當 data 有異動時, 會由該 DataRef 自行 fire event 出來, 這邊再同步至 UI

            #region 將 1 個 'string' 分拆成 n 個 char 個別監聽, 所以產生 n 個 新的 DataRefElement, 監聽 X-Plane 的回覆, 當個別 char 回覆時, 會等到全部到齊後, 再一併 fire 給上層處理
            for (var idx = 0; idx < sdr.StringLength; idx++) {
                string idxDataRefName = string.Format(CultureInfo.CurrentCulture, UtilConstants.ArrayDataRefNameFormat, sdr.DataRef, idx);
                var CharDR = Instructions.DataRefs.GetDataRefElement(idxDataRefName, Instructions.DataRefs.TypeStart_float, sdr.Writable, sdr.Units, sdr.Description);
                CharDR.StringSplitToChar = true;// Type 還是維持 float, 因為 X-Plane Response 的 byte[4] 是 float data, 但增加 StringSplitToChar 屬性, 用來識別 IsSingleCharType

                var currentIndex = idx;
                Subscribe(CharDR, frequency, (dr, newValue) => {
                    // StringDataRefElement 所分拆的 n 個 DataRefElement callback 會回到這邊, 不會回給 MbiBaseSystem.cs 那邊, 那邊是專門處理直接註冊 DataRefElement 的物件
                    dr.RawByteValue = newValue.DeepClone();
                    dr.CheckObjValFrRawByteValue();
                    sdr.Update(currentIndex, dr.ObjVal2Char); // 這邊會將 string 拆成 n 個 char 個別監聽, 並且等到全部到齊後, 再一併 fire 給上層處理
                });
            }
            #endregion
        }
        /// <summary>
        /// Deprecated, this method has no effect
        /// </summary>
        [Obsolete]
        private void Subscribe(DataRefElement dataref, int frequency = -1)
        { }

        /// <summary>
        /// Informs X-Plane to Stop sending this DataRef value
        /// </summary>
        public void UnSubscribe(DataRefElement dr) {
            if (dr != null) {
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneResponseHeader); // header
                dg.Add(dr.Id); // dref_sender_index ::: is the integer code you want X-Plane to send back with the dataref value, so you can tell WHICH dataref X-Plane is giving you. (since you are likely to ask for MANY different datarefs.)
                dg.Add(0); // dref_freq ::: IS ACTUALLY 'THE NUMBER OF TIMES PER SECOND', YOU WANT X-PLANE TO SEND THIS DATA! 停止接收 X-Plane Response
                dg.Add(dr.DataRef); // dref_string[400] ::: is the 'dataref string' that you want X-Plane to send to you.
                dg.FillTo(UtilXPlane.XPlaneRREFMessageBodyLength); // dref_string[400] 不足補齊
                SendDatagramToXP(dg);
            } else { OnLog?.Invoke($"No.{WorkerCounter} Remove null({dr}) from DataRefs List"); }

            var existed = DataRefs.ToArray().Where(d => d != null && d.DataRef.Equals(dr.DataRef)).FirstOrDefault();
            if (existed != null) { DataRefs.Remove(existed); }
            //OnLog?.Invoke($"UnSubscribe from {dataref}");
        }
        #endregion


        #region Informs X-Plane to change the value of the DataRef, 主動 Write 更新 DataRef Value
        /// <summary>
        /// Informs X-Plane to change the value of the DataRef
        /// <para>修改值送給 X-Plane 之前, 先檢查所有 data 的 type 是否正確, o 已經是各種 data type 的實際 value</para>
        /// </summary>
        /// <param name="dataRefName">外面已經整理好傳進來 ::: Single(xxx/xx), Array(xxx/xx[i])</param>
        /// <param name="o">各種 data type 的 RawData</param>
        public void SetDataRefValueNEW(string dataRefName, object o) {
            if (!string.IsNullOrEmpty(dataRefName) && o != null) {
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneSetDataRefHeader);

                if (o.ValueIsFloat())
                { dg.Add(o.ConvertObjectToFloat()); }
                else if (o.ValueIsInt())
                { dg.Add(o.ConvertObjectToInt32ToFloat()); }
                else if (o.ValueIsDouble())
                { dg.Add(o.ConvertObjectToDoubleToFloat()); }
                else if (o.ValueIsChar())
                {
                    char ch = o.ConvertObjectToChar();
                    byte[] RawByteValue = ch.ConvertCharToInt32ToFloat().ConvertToByteArray();
                    dg.Add(RawByteValue);
                } // 外部會將 string 切割成 xxx/xx[i], char 後... 傳入

                dg.Add(dataRefName);
                dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                SendDatagramToXP(dg);
            } else { OnLog?.Invoke(string.Format(CultureInfo.CurrentCulture, "SetDataRefValue() fail due to DataRefName({0}) is null or empty? OR value({1}) is null?", dataRefName, o)); }
        }

        /// <summary>
        /// Informs X-Plane to change the value of the DataRef
        /// </summary>
        /// <param name="dataref">DataRef that will be changed</param>
        /// <param name="value">New value of the DataRef</param>
        public void SetDataRefValue(string dataref, string value)
        {
            var dg = new XPDatagram();
            dg.Add("DREF");
            dg.Add(value);
            dg.Add(dataref);
            dg.FillTo(509);
            SendDatagramToXP(dg);
        }

        /// <summary>
        /// Informs X-Plane to change the value of the DataRef
        /// </summary>
        /// <param name="dataref">DataRef that will be changed</param>
        /// <param name="value">New value of the DataRef</param>
        public void SetDataRefValue(DataRefElement dataref, object value) {
            if (dataref == null) { throw new ArgumentNullException(nameof(dataref)); }
            SetDataRefValue(dataref.DataRef, value);
        }
        /// <summary>
        /// Informs X-Plane to change the value of the DataRef (float value)
        /// </summary>
        /// <param name="dataref">DataRef that will be changed</param>
        /// <param name="value">New value of the DataRef</param>
        public void SetDataRefValue(string dataref, object value) {
            if (!string.IsNullOrEmpty(dataref)) {
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneSetDataRefHeader);

                if (value.GetType() == typeof(char))
                { dg.Add(value.ConvertObjectToChar()); }
                else
                { dg.Add(value.ConvertObjectToFloat()); }
                
                dg.Add(dataref);
                dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                SendDatagramToXP(dg);
            }
        }
        /// <summary>
        /// 送出更新值給 X-Plane, 預設送 10 次 UDP 封包, 避免掉資料
        /// </summary>
        private void SendDatagramToXP(XPDatagram dg) { for (int i = 0; i < UtilConstants.SetDataRefValueResendCNT; i++) { ClientSend(dg); } }
        /// <summary>
        /// Informs X-Plane to change the value of the DataRef (string value)
        /// </summary>
        /// <param name="dataref">DataRef that will be changed</param>
        /// <param name="value">New value of the DataRef</param>
        /// <param name="byteLen">string Length</param>
        public void SetDataRefValue(string dataref, string value, int byteLen) {
            if (!string.IsNullOrEmpty(dataref)) {
                /**
                 * 目前無法正常寫入 byte[] 的字串值, 這是 X-Plane 的 bug, 之後官方會修正
                 * 查詢資料來源:
                 *              https://forums.x-plane.org/index.php?/forums/topic/251469-udp-setdataref-dref-with-byte-arrays-does-it-work/   2021/08/28 15:05:00 posted
                 *              Laminar responds:
                 *                              This is a design limitation--the packet can't write byte-array datarefs. 
                 *                              This has been filed as feature request XPD-11353. 
                 *                              That number will be listed in release notes if or when it is included in an X-Plane update. 
                 **/
                
                int idx = 0;
                foreach (var _c in value) {
                    var dg = new XPDatagram();
                    dg.Add(UtilXPlane.XPlaneSetDataRefHeader);
                    //dg.Add(float.Parse(_c.ToString()));
                    dg.Add(Convert.ToSingle((byte)_c)); // 把 1 個 byte 轉成符合 DREF 指令的 4byte 內容填入, 因為 int, double, float 都轉成 float, 所以這邊也如此, 但是無法成功更新值
                    string subDataRef = $"{dataref}[{idx}/{byteLen}]"; // $"{dataref}[{idx}]";
                    dg.Add(subDataRef);
                    dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                    SendDatagramToXP(dg);
                    idx++;
                    //DREF0 + (4byte byte value)+dref_path + 0 + spaces to complete the whole message to 509 bytes
                }

                /*
                byte[] eachChar = Encoding.UTF8.GetBytes(value);
                int idx = 0;
                foreach (var _c in eachChar)
                {
                    var dg = new XPDatagram();
                    dg.Add(UtilXPlane.XPlaneSetDataRefHeader);
                    dg.Add(Convert.ToSingle(_c));
                    string subDataRef = $"{dataref}[{idx}]"; // $"{dataref}[{idx}/{byteLen}]";
                    dg.Add(subDataRef);
                    dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                    SendDatagramToXP(dg);
                    idx++;
                    //DREF0 + (4byte byte value)+dref_path + 0 + spaces to complete the whole message to 509 bytes
                }
                */

                /*
                for (int i = idx; i < byteLen; i++)
                {
                    var dg = new XPDatagram();
                    dg.Add(UtilXPlane.XPlaneSetDataRefHeader);
                    dg.Add((float)0);
                    string subDataRef = $"{dataref}[{i}/{byteLen}]"; // $"{dataref}[{i}]";
                    dg.Add(subDataRef);
                    dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                    SendDatagramToXP(dg);
                    //DREF0 + (4byte byte value)+dref_path + 0 + spaces to complete the whole message to 509 bytes
                }
                */

                /*
                var dg = new XPDatagram();
                dg.Add(UtilXPlane.XPlaneSetDataRefHeader);
                dg.Add(value); //dg.Add(Encoding.UTF8.GetBytes(value));
                dg.Add(dataref);
                dg.FillTo(UtilXPlane.XPlaneDREFMessageBodyLength);
                SendDatagramToXP(dg);
                */
            }
        }
        #endregion

        /// <summary>
        /// UDP Send 指令給 X-Plane
        /// <para> 0 ::: success</para>
        /// <para>-1 ::: failure</para>
        /// </summary>
        private bool ClientSend(XPDatagram dg) {
            bool FailReSend = true;
            try {
                if (LocalSendMsgToXPlane != null && LocalSendMsgToXPlane.Client != null && !SocketIsDisposed(LocalSendMsgToXPlane.Client)) {
                    do {
                        int SentBytesLen = LocalSendMsgToXPlane.Send(dg.Get(), dg.Len);
                        FailReSend = SentBytesLen != dg.Len;
                        if (FailReSend) { OnLog?.Invoke(UtilConstants.UDPsentFailMsg); }
                    } while (FailReSend);
                }
                return !FailReSend;
            //} catch (Exception ex) { LogException("ClientSend() function", ex); return FailReSend; }
            } catch { return FailReSend; }
        }

        #region X-Plane System Method ::: QuitXPlane(), Fail(), Recover()
        /// <summary>
        /// Request X-Plane to close, a notification message will appear
        /// </summary>
        public void QuitXPlane() {
            var dg = new XPDatagram();
            dg.Add("QUIT");
            SendDatagramToXP(dg);
        }
        /// <summary>
        /// Inform X-Plane that a system is failed
        /// </summary>
        /// <param name="system">Integer value representing the system to fail</param>
        public void Fail(int system) {
            var dg = new XPDatagram();
            dg.Add("FAIL");
            dg.Add(system.ToString(UtilConstants.EnCulture));
            SendDatagramToXP(dg);
        }
        /// <summary>
        /// Inform X-Plane that a system is back to normal functioning
        /// </summary>
        /// <param name="system">Integer value representing the system to recover</param>
        public void Recover(int system) {
            var dg = new XPDatagram();
            dg.Add("RECO");
            dg.Add(system.ToString(UtilConstants.EnCulture));
            SendDatagramToXP(dg);
        }
        #endregion


        #region Dispose()
        /// <summary>
        /// 將 CancellTokenSource 處理掉
        /// </summary>
        private void Dispose_CancellTokenSource() {
            if (CancellTokenSource != null && !CancellationTokenSourceIsDisposed(CancellTokenSource)) {
                CancellTokenSource?.Cancel();
                CancellTokenSource?.Dispose();
            }
        }
        /// <summary>
        /// 將 ListenXPlaneServer 處理掉
        /// </summary>
        private void Dispose_ListenXPlaneServer() {
            if (ListenXPlaneServer != null && ListenXPlaneServer.Client != null && !SocketIsDisposed(ListenXPlaneServer.Client)) {
                ListenXPlaneServer?.Close();
                ListenXPlaneServer?.Dispose();
            }
        }
        /// <summary>
        /// 將 LocalSendMsgToXPlane 處理掉
        /// </summary>
        private void Dispose_LocalSendMsgToXPlane() {
            if (LocalSendMsgToXPlane != null && LocalSendMsgToXPlane.Client != null && !SocketIsDisposed(LocalSendMsgToXPlane.Client)) {
                LocalSendMsgToXPlane?.Close();
                LocalSendMsgToXPlane?.Dispose();
            }
        }
        protected virtual void Dispose(bool a) {
            try {
                Dispose_CancellTokenSource();
                Dispose_ListenXPlaneServer();
                Dispose_LocalSendMsgToXPlane();
            } catch (Exception ex) { LogException("virtual Dispose() function", ex); }
        }
        public void Dispose() {
            try {
                Dispose(true);
                GC.SuppressFinalize(this);
            } catch (Exception ex) { LogException("Dispose() function", ex); }
        }
        #endregion


        #region 檢查 Socket, CancellationTokenSource 是否已經處置掉了, IsDisposed ?
        /*
        private bool SocketIsConnected(Socket s) {
            try {
                if (s == null) { return false; }
                BindingFlags bfIsDisposed = BindingFlags.Instance | BindingFlags.Public | BindingFlags.GetProperty;
                PropertyInfo field = s.GetType().GetProperty("Connected", bfIsDisposed); // Retrieve a FieldInfo instance corresponding to the field
                // Retrieve the value of the field, and cast as necessary
                bool val = field == null ? false : (bool)field.GetValue(s, null); // 如果找不到 Property, 一律回傳 true, 告知外面沒有此 Property, 視為已經處置過了 Disposed
                return val;
            } catch (Exception ex) {
                _LogException("SocketIsConnected() function", ex);
                return false;
            }
        }
        */
        private bool SocketIsDisposed(Socket s) { return GetPropertyValue(s, "CleanedUp"); }
        private bool CancellationTokenSourceIsDisposed(CancellationTokenSource s) { return GetPropertyValue(s, "IsDisposed"); }
        private bool GetPropertyValue(object o, string PropertyName) {
            try {
                if (o == null) { return true; }
                BindingFlags bfIsDisposed = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.GetProperty;
                PropertyInfo field = o.GetType().GetProperty(PropertyName, bfIsDisposed); // Retrieve a FieldInfo instance corresponding to the field
                // Retrieve the value of the field, and cast as necessary
                return field == null ? true : (bool)field.GetValue(o, null); // 如果找不到 Property, 一律回傳 true, 告知外面沒有此 Property, 視為已經處置過了 Disposed
            } catch (Exception ex) {
                LogException("GetPropertyValue() function", ex);
                return true;
            }
        }
        #endregion


        #region Event Listen
        /// <summary>
        /// 當下 X-Plane 的 feedback raw data
        /// </summary>
        private void GetRawReceived(byte[] raw) {
            //var rawString = Encoding.UTF8.GetString(raw);
            //Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "當下 X-Plane 的 feedback raw data ::: {0}", rawString));
        }
        /// <summary>
        /// DataRef 的 value 有異動發生時, 才會觸發, 取得 X-Plane 的 feedback DataRefElement
        /// </summary>
        private void GetDataRefReceived(DataRefElement dataRef) {
            //Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "取得 X-Plane updated ID({0}), DataRef({1}), NewValue: {2}", dataRef.Id, dataRef.DataRef, dataRef.Value));
        }
        #endregion

    }
}