﻿using AirManagerAPICallbackDefine;
using MBILibrary.Enums;
using MBILibrary.Util;
using System;

namespace XPlaneConnector.DataRef
{
    /// <summary>
    /// Each X-Plane DataRef 的明細內容 (非 String 型態的所有 DataRef ::: float, double, int, byte, float[], double[], int[], byte[])
    /// </summary>
    public class DataRefElement : DataRefElementBase {
        #region Properties
        /// <summary>
        /// initial 註冊時, 動態產的的序號
        /// <para>X-Plane 會依據此 id, Response 其值的變化狀況</para>
        /// <para>一組 id 4 bytes data, 而且該組 bytes 內容為 float data, 所以必須先轉成 float data</para>
        /// </summary>
        public int Id { get; private set; }
        public bool IsInitialized { get; private set; }
        #endregion

        #region 紀錄此 DataRef 的最新值byte[], 值來源 :: X-Plane, AirManager, AirForceSystem, GrpcServer
        /// <summary>
        /// 最新值, 值來源 :: X-Plane, AirManager, AirForceSystem, GrpcServer
        /// <para>1. X-Plane :: 針對每一個註冊 id 只會回覆 一組 4 bytes data, 而且該組 bytes 內容為 float data, 所以必須先轉成 float data</para>
        /// <para>2. AirManager, AirForceSystem, GrpcServer 可自行轉成 byte[] 存入</para>
        /// </summary>
        private byte[] BytesValue = null;
        /// <summary>
        /// 最新值, 值來源 :: X-Plane, AirManager, AirForceSystem, GrpcServer
        /// <para>1. 取得原始的 'byte[]' data</para>
        /// <para>2. 只記錄 Single value ::: int, float, double</para>
        /// <para>3. int, float ::: 4 bytes</para>
        /// <para>4. double ::: 8 bytes</para>
        /// </summary>
        public byte[] RawByteValue { get { return BytesValue; } set { BytesValue = value; } }
        #endregion

        #region (從 'X-Plane' Get value Response to 'Air Manager') EventHandler, 同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"
        public delegate void NotifyChangeHandler(DataRefElement sender, byte[] newValue);
        /// <summary>
        /// 同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"
        /// <para>1. X-Plane 針對每一個註冊 id 只會回覆 一組 4 bytes data, 而且該組 bytes 內容為 float data, 所以必須先轉成 float data</para>
        /// <para>2. 之後再依據其指定的 Type 將 float data 轉成 byte, int, double</para>
        /// </summary>
        public event NotifyChangeHandler OnValueChange;
        #endregion

        #region (從 'Air Manager' Get value Write into 'X-Plane') EventHandler for 8 種 "DataRef Write" ::: int, float, double, byte, int[], byte[], float[], double[]
        /// <summary>
        /// <para>1. DataRef Write "int" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public IntVariableManagedCallback _callbackInt = null;
        /// <summary>
        /// <para>1. DataRef Write "float" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public FloatVariableManagedCallback _callbackFloat = null;
        /// <summary>
        /// <para>1. DataRef Write "double" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public DoubleVariableManagedCallback _callbackDouble = null;
        /// <summary>
        /// air manager不支援，AM API支援，所以無法測試，但已實現底層
        /// <para>1. DataRef Write "byte" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public ByteVariableManagedCallback _callbackByte = null;
        /// <summary>
        /// <para>1. DataRef Write "int[]" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public IntArrayVariableManagedCallback _callbackIntArray = null;
        /// <summary>
        /// <para>1. DataRef Write "byte[]" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public ByteArrayVariableManagedCallback _callbackByteArray = null;
        /// <summary>
        /// <para>1. DataRef Write "float[]" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public FloatArrayVariableManagedCallback _callbackFloatArray = null;
        /// <summary>
        /// <para>1. DataRef Write "double[]" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public DoubleArrayVariableManagedCallback _callbackDoubleArray = null;
        #endregion

        #region Constructor
        public DataRefElement() {
            lock (lockElement) { Id = ++current_id; }

            _callbackInt = DataRefWriteInt;
            _callbackByte = DataRefWriteByte;
            _callbackFloat = DataRefWriteFloat;
            _callbackDouble = DataRefWriteDouble;
            unsafe {
                _callbackIntArray = DataRefWriteIntArray;
                _callbackByteArray = DataRefWriteByteArray;
                _callbackFloatArray = DataRefWriteFloatArray;
                _callbackDoubleArray = DataRefWriteDoubleArray;
            }
            IsInitialized = false;
            LastUpdateTime = DateTime.MinValue;
        }
        #endregion


        #region (從 'Air Manager' Get value Write into 'X-Plane') EventHandler for 7 種 "DataRef Write" ::: int, float, double, byte, int[], float[], double[]
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        private void DataRefWriteInt(int value) => DataRefWriteInt(value, TriggerByWhatSystem.FromAirManager);
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        public void DataRefWriteInt(int value, TriggerByWhatSystem triggerBy) => WriteToXP(triggerBy, "DataRefWriteInt", DataRef, value.ConvertToByteArray().DeepClone(), value);

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        private void DataRefWriteFloat(float value) => DataRefWriteFloat(value, TriggerByWhatSystem.FromAirManager);
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        public void DataRefWriteFloat(float value, TriggerByWhatSystem triggerBy) => WriteToXP(triggerBy, "DataRefWriteFloat", DataRef, value.ConvertToByteArray().DeepClone(), value);

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        private void DataRefWriteDouble(double value) => DataRefWriteDouble(value, TriggerByWhatSystem.FromAirManager);
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        public void DataRefWriteDouble(double value, TriggerByWhatSystem triggerBy) => WriteToXP(triggerBy, "DataRefWriteDouble", DataRef, value.ConvertToByteArray().DeepClone(), value);

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        private void DataRefWriteByte(byte value) => DataRefWriteByte(value, TriggerByWhatSystem.FromAirManager);
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        public void DataRefWriteByte(byte value, TriggerByWhatSystem triggerBy) => WriteToXP(triggerBy, "DataRefWriteByte", DataRef, value.ConvertToByteArray().DeepClone(), value);

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "int[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. Air Manager DLL Lib 沒有支援 offset 屬性, index 一律從 0 開始</para>
        /// </summary>
        private unsafe void DataRefWriteIntArray(int* value, int length) {
            if (value == null || length == 0) { return; } // 避免空值進入, 造成不必要的問題
            int[] IntA = new int[length];
            for (int i = 0; i < length; i++) { IntA[i] = value[i]; }
            DataRefWriteIntArray(IntA, TriggerByWhatSystem.FromAirManager);
        }
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "int[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. 支援 offset 屬性, index 可由外部決定</para>
        /// </summary>
        public void DataRefWriteIntArray(int[] value, TriggerByWhatSystem triggerBy, int offset = UtilConstants.NullOffset) {
            if (UtilGlobalFunc.IsArrayEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            int StartIdx = GetStartIdx(offset);
            int ValueIdx = 0;
            for (int i = StartIdx; i < ArrayLen; i++) {
                if (ValueIdx < value.Length) {
                    WriteToXP(triggerBy, "DataRefWriteIntArray", GetDataRefIdx(i), value[ValueIdx].ConvertToByteArray().DeepClone(), value[ValueIdx]);
                    ValueIdx++;
                } else { break; }
            }
        }

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "byte[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. Air Manager DLL Lib 沒有支援 offset 屬性, index 一律從 0 開始</para>
        /// </summary>
        public unsafe void DataRefWriteByteArray(byte* value, int length) {
            if (value == null || length == 0) { return; } // 避免空值進入, 造成不必要的問題
            byte[] ByteA = new byte[length];
            for (int i = 0; i < length; i++) { ByteA[i] = value[i]; }
            DataRefWriteByteArray(ByteA, TriggerByWhatSystem.FromAirManager);
        }
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "byte[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. 支援 offset 屬性, index 可由外部決定</para>
        /// </summary>
        public void DataRefWriteByteArray(byte[] value, TriggerByWhatSystem triggerBy, int offset = UtilConstants.NullOffset) {
            if (UtilGlobalFunc.IsArrayEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            int StartIdx = GetStartIdx(offset);
            int ValueIdx = 0;
            for (int i = StartIdx; i < ArrayLen; i++) {
                if (ValueIdx < value.Length) {
                    WriteToXP(triggerBy, "DataRefWriteByteArray", GetDataRefIdx(i), value[ValueIdx].ConvertToByteArray().DeepClone(), value[ValueIdx]);
                    ValueIdx++;
                } else { break; }
            }
        }

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "float[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. Air Manager DLL Lib 沒有支援 offset 屬性, index 一律從 0 開始</para>
        /// </summary>
        private unsafe void DataRefWriteFloatArray(float* value, int length) {
            if (value == null || length == 0) { return; } // 避免空值進入, 造成不必要的問題
            float[] FloatA = new float[length];
            for (int i = 0; i < length; i++) { FloatA[i] = value[i]; }
            DataRefWriteFloatArray(FloatA, TriggerByWhatSystem.FromAirManager);
        }
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "float[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. 支援 offset 屬性, index 可由外部決定</para>
        /// </summary>
        public void DataRefWriteFloatArray(float[] value, TriggerByWhatSystem triggerBy, int offset = UtilConstants.NullOffset) {
            if (UtilGlobalFunc.IsArrayEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            int StartIdx = GetStartIdx(offset);
            int ValueIdx = 0;
            for (int i = StartIdx; i < ArrayLen; i++) {
                if (ValueIdx < value.Length) {
                    WriteToXP(triggerBy, "DataRefWriteFloatArray", GetDataRefIdx(i), value[ValueIdx].ConvertToByteArray().DeepClone(), value[ValueIdx]);
                    ValueIdx++;
                } else { break; }
            }
        }

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "double[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. Air Manager DLL Lib 沒有支援 offset 屬性, index 一律從 0 開始</para>
        /// </summary>
        private unsafe void DataRefWriteDoubleArray(double* value, int length) {
            if (value == null || length == 0) { return; } // 避免空值進入, 造成不必要的問題
            double[] DoubleA = new double[length];
            for (int i = 0; i < length; i++) { DoubleA[i] = value[i]; }
            DataRefWriteDoubleArray(DoubleA, TriggerByWhatSystem.FromAirManager);
        }
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "double[]"</para>
        /// <para>2. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>3. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>4. 支援 offset 屬性, index 可由外部決定</para>
        /// </summary>
        public void DataRefWriteDoubleArray(double[] value, TriggerByWhatSystem triggerBy, int offset = UtilConstants.NullOffset) {
            if (UtilGlobalFunc.IsArrayEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            int StartIdx = GetStartIdx(offset);
            int ValueIdx = 0;
            for (int i = StartIdx; i < ArrayLen; i++) {
                if (ValueIdx < value.Length) {
                    WriteToXP(triggerBy, "DataRefWriteDoubleArray", GetDataRefIdx(i), value[ValueIdx].ConvertToByteArray().DeepClone(), value[ValueIdx]);
                    ValueIdx++;
                } else { break; }
            }
        }
        #endregion


        #region DataRef Value 隨時更新監聽值, 一有變動就會 Invoke() ::: byte, byte[], int, int[], float, float[], double, double[]
        /// <summary>
        /// DataRef Value 隨時更新監聽值, 一有變動就會 Invoke() ::: byte[]
        /// <para>data value 有更新, 才會同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"</para>
        /// <para>X-Plane 針對每一個註冊 id 只會回覆 一組 4 bytes data, 而且該組 bytes 內容為 float data, 所以必須先轉成 float data</para>
        /// </summary>
        public bool Update(int id, byte[] newValueByDataRefRawType) {
            if (id != Id) { return false; } // 只處理自己的 id, 不是直接擋掉...
            if (UtilGlobalFunc.IsArrayEmpty(newValueByDataRefRawType)) { return false; } // 沒有值進來, 直接擋掉...

            // 是自己的 id, 且有值進入, 再開始檢查...
            if (UtilConstants.appConfig.DoNotNeedCompareValue || !CompareArrayValue.ByteArrayCompareEqual(newValueByDataRefRawType, BytesValue)) {
                LastUpdateTime = DateTime.Now;
                if (UtilGlobalFunc.IsArrayEmpty(BytesValue)) { BytesValue = newValueByDataRefRawType; } // 初始階段... 直接給值
                else { Buffer.BlockCopy(newValueByDataRefRawType, 0, BytesValue, 0, newValueByDataRefRawType.Length); } // 新值覆蓋舊值...
                IsInitialized = true;

                // 這邊觸發的點, 有兩個位置, 
                // 如果是直接註冊 DataRefElement 的物件, 會觸發 MbiBaseSystem.cs
                // 如果是 StringDataRefElement 分拆的 n 個 DataRefElement 的物件, 會觸發 XPlaneConnector.cs
                OnValueChange?.Invoke(this, BytesValue);

                return true; // 值有更新
            } else { return false; } // 值一樣, 沒有更新
        }
        #endregion

        /// <summary>
        /// 將 byte[4] RawByteValue 值, 依據 Type 轉成適當的 object value, 寫入 ObjVal
        /// <para>這邊只會處理 Single Value ::: int, char, float, double</para>
        /// </summary>
        public void CheckObjValFrRawByteValue() {
            if (!UtilGlobalFunc.IsArrayEmpty(RawByteValue)) {
                if (IsSingleCharType) {
                    // char 要先判斷, 因為 X-Plane 回覆的 byte[4] 是 Float data, 如果讓 IsSingleFloatType 先判斷, 會造成誤判
                    // 如果是由 string 所分拆的 char, 此屬性值才會確認為 true, 否則永遠為 false, 因為其他 data type 必須如此
                    // 因為 X-Plane 回覆的 byte[4] 是 float, 所以要先轉成 float data 後, 再轉成 int, 最後才可以轉成 char
                    ObjVal = RawByteValue.ConvertByteArrayToFloat().ConvertFloatToInt32().ConvertInt32ToChar();
                } else {
                    // 因為 X-Plane 回覆的 byte[4] 是 float, 但已經有在底層先轉置成對的 data type 所屬的 byte[4] 格式, 所以這邊直接轉成對的 data type
                    if (IsSingleIntType) { ObjVal = RawByteValue.ConvertByteArrayToInt(); }
                    else if (IsSingleFloatType) { ObjVal = RawByteValue.ConvertByteArrayToFloat(); }
                    else if (IsSingleDoubleType) { ObjVal = RawByteValue.ConvertByteArrayToDouble(); }
                    else if (IsArrayIntType) { ObjVal = RawByteValue.ConvertByteArrayToIntArray(); }
                    else if (IsArrayFloatType) { ObjVal = RawByteValue.ConvertByteArrayToFloatArray(); }
                    else if (IsArrayDoubleType) { ObjVal = RawByteValue.ConvertByteArrayToDoubleArray(); }
                }
            }
        }

        /// <summary>
        /// Faster Shallow Clone, 淺層複製物件。 大量 List clone 時, 會比 DeepCopy 快 10 倍以上
        /// </summary>
        public DataRefElement ShallowClone() => (DataRefElement)MemberwiseClone();
    }
}