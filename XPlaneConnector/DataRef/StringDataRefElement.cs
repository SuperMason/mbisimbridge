﻿using AirManagerAPICallbackDefine;
using MBILibrary.Enums;
using MBILibrary.Util;
using System;
using System.Globalization;
using System.Linq;

namespace XPlaneConnector.DataRef
{
    /// <summary>
    /// Each X-Plane DataRef 的明細內容 (String 型態)
    /// </summary>
    public class StringDataRefElement : DataRefElementBase {
        #region Properties
        /// <summary>
        /// 從 X-Plane 監聽回來的資料 (string)
        /// <para>當下已經接收到所有 char 回覆, 才會 fire 給上層使用</para>
        /// </summary>
        public string StringValueFromXP { get; private set; }
        /// <summary>
        /// 紀錄已經接收到的 char, 及其所屬位置 index
        /// <para>一旦坐滿 StringLength 定義的所有位置, 就會馬上 fire 給上層</para>
        /// </summary>
        private bool[] IdxChecked = null;
        /// <summary>
        /// 是否已經取得全部的 n 個 char ? 所有位置都已經有 char 更新上去了 ?
        /// <para>滿足條件依據 StringLength 定義的數量</para>
        /// </summary>
        public bool IsAllCompletelyReceived {
            get {
                if (UtilGlobalFunc.IsArrayEmpty(IdxChecked)) { return false; }

                int NotReceivedCNT = IdxChecked.Where(b => !b).Count();
                //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "NotReceivedCNT : {0}, '{1}' StringLength({2})", NotReceivedCNT, DataRef, StringLength));
                return NotReceivedCNT == 0; // 全部 char 皆到齊了... 可以 fireEvent
            }
        }
        /// <summary>
        /// X-Plane 的回覆狀態, Fully? or Partial?
        /// </summary>
        public XPlaneResponseStatusEnum xPlaneResponseStatus => IsAllCompletelyReceived ? XPlaneResponseStatusEnum.Fully : XPlaneResponseStatusEnum.Partial;
        #endregion

        #region 紀錄此 DataRef 的最新值string, 值來源 :: X-Plane, AirManager, AirForceSystem, GrpcServer
        /// <summary>
        /// 最新值, 值來源 :: X-Plane, AirManager, AirForceSystem, GrpcServer
        /// <para>取得原始的 'string' data</para>
        /// </summary>
        public string RawStringValue { get; set; }
        #endregion

        #region (從 'X-Plane' Get value Response to 'Air Manager') EventHandler, 同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"
        public delegate void NotifyChangeHandler(StringDataRefElement sender, string newValue);
        /// <summary>
        /// 同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"
        /// </summary>
        public event NotifyChangeHandler OnValueChange;
        #endregion

        #region (從 'Air Manager' Get value Write into 'X-Plane') EventHandler for 1 種 "DataRef Write" ::: string
        /// <summary>
        /// <para>1. DataRef Write "string" Callback</para>
        /// <para>2. 給 C++ Wrapper Project 參考宣告使用, 給 C# asign function 使用</para>
        /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
        /// <para>4. 不可以宣告成 static, 因為這樣全部的 callback 都會丟給最後一個註冊的, 全部的 object 都會聽相同的 address</para>
        /// </summary>
        public StringVariableManagedCallback _callbackString;
        #endregion

        #region Constructor
        public StringDataRefElement(int StringLength = UtilConstants.NullStringLength) {
            _callbackString = DataRefWriteString;
            InitialString(StringLength);
            LastUpdateTime = DateTime.MinValue;
        }
        #endregion

        /// <summary>
        /// 清空儲存的字串, 重新初始化設定
        /// </summary>
        private void InitialString(int length) {
            int len = length == UtilConstants.NullStringLength ? 0 : length;
            IdxChecked = new bool[len];
            StringValueFromXP = string.Empty;
        }

        #region (從 'Air Manager' Get value Write into 'X-Plane') EventHandler for 1 種 "DataRef Write" ::: string
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "string"</para>
        /// <para>2. "string" 在註冊 X-Plane 時, 是分拆成 n 個 char, 所以這邊寫入, 也要分拆成 n 個 char</para>
        /// <para>3. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>4. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>5. Air Manager DLL Lib 沒有支援 offset 屬性, index 一律從 0 開始</para>
        /// </summary>
        private void DataRefWriteString(string value)
        { DataRefWriteString(value, TriggerByWhatSystem.FromAirManager); }
        /// <summary>
        /// 從 'AirForceSystem', 'GRPC' 取得 DataRef Value Write into 'X-Plane'
        /// <para>1. DataRef Write "string"</para>
        /// <para>2. "string" 在註冊 X-Plane 時, 是分拆成 n 個 char, 所以這邊寫入, 也要分拆成 n 個 char</para>
        /// <para>3. 此 DataRef 為 xxx/xx/xx, 所以要從 X-Plane XPlaneConnector 撈取初始註冊時的 xxx/xx/xx[i] DataRef 進行個別值的寫入</para>
        /// <para>4. 要先分拆成 xxx/xx/xx[0] ~ xxx/xx/xx[n] 後, 再個別去寫入 X-Plane</para>
        /// <para>5. 支援 offset 屬性, index 可由外部決定</para>
        /// </summary>
        public void DataRefWriteString(string value, TriggerByWhatSystem triggerBy, int offset = UtilConstants.NullOffset) {
            if (string.IsNullOrEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            char[] characters = value.ToCharArray();
            int StartIdx = GetStartIdx(offset);
            int ValueIdx = 0;
            for (int i = StartIdx; i < StringLength; i++) {
                if (ValueIdx < characters.Length) {
                    char ch = characters[ValueIdx];
                    byte[] RawByteValue = ch.ConvertCharToInt32ToFloat().ConvertToByteArray(); // X-Plane 只吃 float type value 轉成的 byte[], 所以 char => int => float => byte[] 才是 X-Plane 可以接受的 data 格式
                    WriteToXP(triggerBy, "DataRefWriteString", GetDataRefIdx(i), RawByteValue, ch);
                    ValueIdx++;
                } else { break; }
            }
        }
        /*
        public void DataRefWriteString(string value, TriggerByWhatSystem triggerBy)
        {
            if (string.IsNullOrEmpty(value)) { return; } // 避免空值進入, 造成不必要的問題
            ObjVal = value;
            WriteToXP(triggerBy, "DataRefWriteString", DataRef, ObjVal2String2StringLengthByteArray, value);
        }*/

        #endregion

        #region DataRef Value 隨時更新監聽值, 一有變動就會 Invoke() ::: string
        /// <summary>
        /// DataRef Value 隨時更新監聽值, 一有變動就會 Invoke() ::: string
        /// <para>data value 有更新, 才會同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"</para>
        /// </summary>
        public void Update(int index, char ch) {
            lock (lockElement) {
                if (!IdxChecked[index]) {   // 此 index 尚未更新過, 所以進入更新
                    IdxChecked[index] = true;
                    
                    #region 有實際的 char 值傳進來, 擺到對的 index 位置, 如果該 index 傳空的 char 就不用理會
                    if (ch > 0) {
                        LastUpdateTime = DateTime.Now;
                        if (StringValueFromXP.Length <= index)
                        { StringValueFromXP = StringValueFromXP.PadRight(index + 1, ' '); }

                        var current = StringValueFromXP[index];
                        if (current != ch) { StringValueFromXP = StringValueFromXP.Remove(index, 1).Insert(index, ch.ToString()); }
                    }
                    #endregion
                }

                if (IsAllCompletelyReceived || IsOverMaxAge) {
#if DEBUG
                    if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage) {
                        Log(string.Format(CultureInfo.CurrentCulture, "'{0}' string value flush 出去, IsAllCompletelyReceived({1}), IsOverMaxAge({2})", DataRef, IsAllCompletelyReceived, IsOverMaxAge));
                        StringValueFromXP = string.Format(CultureInfo.CurrentCulture, "{0}-Test{1}", StringValueFromXP, DateTime.Now.Second.ToString("00")); // 用來測試用.. 讓 Air Manager 可以持續更新
                    }
#endif
                    OnValueChange?.Invoke(this, StringValueFromXP); // data 有更新, 同步通知 "Air Manager" 和 "XPlaneConnector上的訂閱者"
                    InitialString(StringLength);// 已經送出值, 所以清空, 等待下一次更新
                }
            }
        }
        #endregion

        /// <summary>
        /// 將 string RawStringValue 值, 寫入 ObjVal
        /// </summary>
        public void CheckObjValFrRawStringValue() {
            if (!string.IsNullOrEmpty(RawStringValue)) {
                if (IsStringType)
                { ObjVal = RawStringValue; }
            }
        }

        /// <summary>
        /// Faster Shallow Clone, 淺層複製物件。 大量 List clone 時, 會比 DeepCopy 快 10 倍以上
        /// </summary>
        public StringDataRefElement ShallowClone() => (StringDataRefElement)MemberwiseClone();
    }
}