﻿using MBILibrary.Enums;
using MBILibrary.Util;
using System;
using System.Globalization;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace XPlaneConnector.DataRef
{
    /// <summary>
    /// DataRef 的 string, float 共同屬性
    /// </summary>
    public abstract class DataRefElementBase {
        #region static Properties, 控制序號 ID (DataRefElement, StringDataRefElement 共用)
        protected static readonly object lockElement = new object();
        protected static int current_id = 0;
        #endregion

        #region 控制 X-Plane DataRef value 資料新鮮度
        /// <summary>
        /// 此 DataRef Value 距離上一次最後更新的時間, 是否有超過自定義的最慢更新時間限制 0.1 秒 ?
        /// <para>1. DataRefElement 是與 X-Plane 同步, 每 0.1 秒更新資料</para>
        /// <para>2. StringDataRefElement 則是固定等最多 5 秒, 沒有更新值就將值 flush 出去, 因為 X-Plane 不會將全部的 string 所屬的 char element 全部丟出, 會遺漏少數幾個 char, 如果沒有設定 MaxAge, 會造成永遠無法取得 string</para>
        /// </summary>
        protected internal bool IsOverMaxAge {
            get {
                if (IsStringDataRefElement) { return Age > MaxAge4String; }
                else { return Age > MaxAge; }
            }
        }
        /// <summary>
        /// 此 DataRef 最後的更新時間
        /// </summary>
        public DateTime LastUpdateTime { get; protected set; }
        /// <summary>
        /// 該 DataRef 是否需要更新 value 了 ?
        /// <para>與 MaxAge 比較</para>
        /// </summary>
        private TimeSpan Age { get { return DateTime.Now - LastUpdateTime; } }
        /// <summary>
        /// "Others" 決定 X-Plane DataRef value 資料新鮮度, 最快每 0.001 秒就要立即向 X-Plane 取得最新值
        /// <para>注意 !!!</para>
        /// <para>1. 與 "解除註冊", "等待", "再註冊" 都沒關係... 與 UtilConstants.appConfig.CheckXPlaneAliveMS 非常有關係... 太快 X-Plane 來不及更新資料.. 就馬上又把舊值 Response 出來</para>
        /// <para>2. 所以更新時... UtilConstants.appConfig.CheckXPlaneAliveMS 必須從 10 ms 改成 LetXPlaneUpdateDataRefValueWaitingMS</para>
        /// <para>3. 給 X-Plane 有時間更新, 確認更新完成後, 再改回 10 ms</para>
        /// </summary>
        public static TimeSpan MaxAge = TimeSpan.FromMilliseconds(UtilConstants.appConfig.CheckXPlaneAliveMS);
        /// <summary>
        /// "String" 決定 X-Plane DataRef value 資料新鮮度, 最慢每 5 秒就要立即向 X-Plane 取得最新值
        /// <para>過程中, 如果 string 的 all chars 提前全部到齊, 也就馬上更新給 Air Manager 了, 不一定會等到 5 秒</para>
        /// </summary>
        public static TimeSpan MaxAge4String = TimeSpan.FromMilliseconds(5000);
        /// <summary>
        /// 針對 "資料新鮮度", 先改成 0.3 秒, 讓 X-Plane 有時間可以完成更新 DataRef value
        /// </summary>
        public const double LetXPlaneUpdateDataRefValueWaitingMS = 300;
        #endregion

        #region Properties
        /// <summary>
        /// 不是 Array 的 DataRef
        /// </summary>
        public const int ArrayIdxDefault = -1;
        /// <summary>
        /// X-Plane 的 參數名稱, 是否為 Array 值?
        /// <para>1. 如果 DataRef 為 sim/xxx/xxx/xxx[n], 表示此為 Array 值, 所以 ArrayIdx = index</para>
        /// <para>2. 如果 DataRef 為 sim/xxx/xxx/xxx, 表示此為 單一 值, ArrayIdx 固定為 -1</para>
        /// </summary>
        public int ArrayIdx = ArrayIdxDefault;

        /// <summary>
        /// 由 DataRef Name 判斷, 是否為 Array Data
        /// <para>是否為 int[], float[], double[], byte[] 其中一種 ?</para>
        /// </summary>
        public bool DataRefIsArray { get { return !string.IsNullOrEmpty(_dataref) && UtilGlobalFunc.IsArray(_dataref); } }

        /// <summary>
        /// 由 Type Name 判斷, 是否為 Array Data
        /// <para>是否為 int[], float[], double[], byte[] 其中一種 ?</para>
        /// </summary>
        public bool TypeIsArray { get { return !string.IsNullOrEmpty(_type) && UtilGlobalFunc.IsArray(_type); } }

        public const string DatarefNULL = "ErrorDataRefName";
        private string _dataref = null;
        /// <summary>
        /// X-Plane 定義的參數名稱
        /// <para>1. 單一值 :: xxx/xxx/xxx。 DataType ::: 'byte', 'int', 'float', 'double'</para>
        /// <para>2. Array值 :: xxx/xxx/xxx[i]。 DataType ::: 'byte[]', 'int[]', 'float[]', 'double[]'</para>
        /// <para>注意 !!</para>
        /// <para>Array值 會改成 xxx/xxx/xxx[i] 的主因是, X-Plane 是以個別 [i] 為回覆對象</para>
        /// <para>所以才會在註冊時, 自動分拆 [0] ~ [i] 個</para>
        /// <para>Air Manager 用來註冊的 DataRef Name 不需要帶 [i], 原始的 "xxx/xxx/xx" 即可</para>
        /// <para>X-Plane 用來註冊的 DataRef Name, 單一值 "xxx/xxx/xx"</para>
        /// <para>X-Plane 用來註冊的 DataRef Name, Array值 "xxx/xxx/xx[i]"</para>
        /// </summary>
        public string DataRef {
            get { return _dataref; }
            set {
                _dataref = value;
                ArrayIdx = DataRefIsArray ? GetArrayLength(_dataref) : ArrayIdxDefault; // 同步 ArrayIdx 值
            }
        }

        /// <summary>
        /// 此 DataRef 是 Array 的元素之一
        /// </summary>
        public bool IsArrayElement => DataRef.Contains("[") && DataRef.Contains("]");
        /// <summary>
        /// 此 base object 是 DataRefElement
        /// </summary>
        public bool IsDataRefElement => GetType() == typeof(DataRefElement);
        /// <summary>
        /// 此 base object 是 StringDataRefElement
        /// </summary>
        public bool IsStringDataRefElement => GetType() == typeof(StringDataRefElement);

        /// <summary>
        /// 數值的實際單位
        /// </summary>
        public string Units { get; set; }

        /// <summary>
        /// 不是 Array 的 Type
        /// </summary>
        public const int ArrayLenDefault = -1;
        /// <summary>
        /// X-Plane 的 資料型態 Type, 是否為 Array 值?
        /// <para>1. 如果 type 為 float[n], 表示此為 Array 值, 所以 ArrayLen = n</para>
        /// <para>2. 如果 type 為 float, 表示此為 單一 值, ArrayLen 固定為 -1</para>
        /// </summary>
        public int ArrayLen = ArrayLenDefault;

        private string _type = null;
        /// <summary>
        /// 數值的程式 DataType
        /// </summary>
        public string Type {
            get {
                if (string.IsNullOrEmpty(_type))
                { _type = DataRefs.TypeStart_float; } // when initializing null, default 'float'
                return _type;
            }
            set {
                _type = value;
                ArrayLen = TypeIsArray ? GetArrayLength(_type) : ArrayLenDefault; // 同步 ArrayLen 值
            }
        }

        /// <summary>
        /// <para>1. 單一值 : 取 DataRef Name</para>
        /// <para>2. Array : 去除 [n] 後, 取 Raw DataRef Name</para>
        /// <para>注意!!!</para>
        /// <para>Air Manager 用來註冊的 DataRef Name 不需要帶 [i], 原始的 "xxx/xxx/xx" 即可</para>
        /// <para>X-Plane 用來註冊的 DataRef Name, 單一值 "xxx/xxx/xx"</para>
        /// <para>X-Plane 用來註冊的 DataRef Name, Array值 "xxx/xxx/xx[i]"</para>
        /// </summary>
        public string RawDataRef => string.IsNullOrEmpty(_dataref) ? DatarefNULL : DataRefIsArray ? _dataref.Substring(0, _dataref.IndexOf("[")) : _dataref;

        /// <summary>
        /// 取得 xxx/xxx/xxx[i] 的字串
        /// </summary>
        public string GetDataRefIdx(int idx) => string.Format(CultureInfo.CurrentCulture, UtilConstants.ArrayDataRefNameFormat, RawDataRef, idx);

        #region Air Manager 註冊專用
        /*
        #region 註冊至 Air Manager 的名稱 (sbyte*)name
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>0. Send To 'Air Manager' as ::: (sbyte*)name</para>
        /// <para>1. MbiAmAPI.sendXXX(name, value)</para>
        /// <para>2. 把 RawDataRef 轉成 byte[] 再弄成 byte* ptr</para>
        /// </summary>
        public byte[] RawDataRefAsByteArray { get { return RawDataRef.ConvertToByteArray(); } }
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>0. Send To 'Air Manager' as ::: (sbyte*)name</para>
        /// <para>1. MbiAmAPI.AddVariableProvider(name, xxx)</para>
        /// <para>2. 把 DataRef 轉成 byte[] 再弄成 byte* ptr</para>
        /// </summary>
        public byte[] DataRefAsByteArray { get { return DataRef.ConvertToByteArray(); } }
        #endregion
        */

        #region 註冊至 Air Manager 的 Initial Memory Address
        private byte[] emptyStringAddressByteArray = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>依據定義的長度(StringLength), 預留記憶體的 'string' 長度 Address</para>
        /// </summary>
        public byte[] EmptyStringAddressByteArray {
            get {
                if (emptyStringAddressByteArray == null && IsStringType)
                { emptyStringAddressByteArray = new byte[StringLength]; }
                return emptyStringAddressByteArray;
            }
            set { emptyStringAddressByteArray = value; }
        }

        private int[] emptyIntAddressArray = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>依據定義的長度(ArrayLen), 預留記憶體的 'int[]' 長度 Address</para>
        /// </summary>
        public int[] EmptyIntAddressArray {
            get {
                if (emptyIntAddressArray == null && IsArrayIntType && ArrayLen != ArrayLenDefault)
                { emptyIntAddressArray = new int[ArrayLen]; }
                return emptyIntAddressArray;
            }
            set { emptyIntAddressArray = value; }
        }

        private byte[] emptyByteAddressArray = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>依據定義的長度(ArrayLen), 預留記憶體的 'byte[]' 長度 Address</para>
        /// </summary>
        public byte[] EmptyByteAddressArray {
            get {
                if (emptyByteAddressArray == null && IsArrayByteType && ArrayLen != ArrayLenDefault)
                { emptyByteAddressArray = new byte[ArrayLen]; }
                return emptyByteAddressArray;
            }
        }

        private float[] emptyFloatAddressArray = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>依據定義的長度(ArrayLen), 預留記憶體的 'float[]' 長度 Address</para>
        /// </summary>
        public float[] EmptyFloatAddressArray {
            get {
                if (emptyFloatAddressArray == null && IsArrayFloatType && ArrayLen != ArrayLenDefault)
                { emptyFloatAddressArray = new float[ArrayLen]; }
                return emptyFloatAddressArray;
            }
            set { emptyFloatAddressArray = value; }
        }


        private double[] emptyDoubleAddressArray = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>依據定義的長度(ArrayLen), 預留記憶體的 'double[]' 長度 Address</para>
        /// </summary>
        public double[] EmptyDoubleAddressArray {
            get {
                if (emptyDoubleAddressArray == null && IsArrayDoubleType && ArrayLen != ArrayLenDefault)
                { emptyDoubleAddressArray = new double[ArrayLen]; }
                return emptyDoubleAddressArray;
            }
            set { emptyDoubleAddressArray = value; }
        }

        private int[] emptyIntAddress = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>預留記憶體的 'int' Address</para>
        /// </summary>
        public int[] EmptyIntAddress {
            get {
                if (emptyIntAddress == null && IsSingleIntType)
                { emptyIntAddress = new int[1] { 0 }; }
                return emptyIntAddress;
            }
        }

        private byte[] emptyByteAddress = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>預留記憶體的 'byte' Address</para>
        /// </summary>
        public byte[] EmptyByteAddress {
            get {
                if (emptyByteAddress == null && IsSingleByteType)
                { emptyByteAddress = new byte[1] { 0 }; }
                return emptyByteAddress;
            }
        }

        private float[] emptyFloatAddress = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>預留記憶體的 'float' Address</para>
        /// </summary>
        public float[] EmptyFloatAddress {
            get {
                if (emptyFloatAddress == null && IsSingleFloatType)
                { emptyFloatAddress = new float[1] { 0 }; }
                return emptyFloatAddress;
            }
        }

        private double[] emptyDoubleAddress = null;
        /// <summary>
        /// Air Manager 註冊專用
        /// <para>預留記憶體的 'double' Address</para>
        /// </summary>
        public double[] EmptyDoubleAddress {
            get {
                if (emptyDoubleAddress == null && IsSingleDoubleType)
                { emptyDoubleAddress = new double[1] { 0 }; }
                return emptyDoubleAddress;
            }
        }
        #endregion
        #endregion

        /// <summary>
        /// 取得 Array 的 Raw Type
        /// <para>1. 如果不是 Array, 則回傳與 Type 屬性相同</para>
        /// <para>2. 是 Array, 則置換 DataRef Type 字串, 由 "type[]" 換成 "type"</para>
        /// </summary>
        public string RawType {
            get { return (!TypeIsArray) ? _type :
                        UtilGlobalFunc.IsTheTypeArray(_type, DataRefs.TypeStart_int) ? DataRefs.TypeStart_int
                      : UtilGlobalFunc.IsTheTypeArray(_type, DataRefs.TypeStart_byte) ? DataRefs.TypeStart_byte
                      : UtilGlobalFunc.IsTheTypeArray(_type, DataRefs.TypeStart_float) ? DataRefs.TypeStart_float
                      : UtilGlobalFunc.IsTheTypeArray(_type, DataRefs.TypeStart_double) ? DataRefs.TypeStart_double
                      : DataRefs.TypeStart_float;
            }
        }

        #region 判斷是何種 Data Type 的 DataRef
        /// <summary>
        /// 是否是指定的 RawType ?
        /// </summary>
        private bool IsTheRawType(string theRawType) => RawType.StartsWith(theRawType, StringComparison.OrdinalIgnoreCase);
        /// <summary>
        /// 是否是指定的 Type ?
        /// </summary>
        private bool IsTheType(string theType) => Type.StartsWith(theType, StringComparison.OrdinalIgnoreCase);
        private bool stringSplitToChar = false;
        /// <summary>
        /// 由 String 分拆成 n 個 子 DataRef
        /// </summary>
        public bool StringSplitToChar { get { return stringSplitToChar; } set { stringSplitToChar = value; } }
        /// <summary>
        /// Single 'char' 格式的 DataRef ?
        /// </summary>
        public bool IsSingleCharType => IsSingleFloatType && StringSplitToChar;
        /// <summary>
        /// Single 'int' 格式的 DataRef ?
        /// </summary>
        public bool IsSingleIntType => IsSingleType && RawTypeIsNotNull && IsTheRawType(DataRefs.TypeStart_int);
        /// <summary>
        /// Single 'byte' 格式的 DataRef ?
        /// </summary>
        public bool IsSingleByteType => IsSingleType && RawTypeIsNotNull && IsTheRawType(DataRefs.TypeStart_byte);
        /// <summary>
        /// Single 'float' 格式的 DataRef ?
        /// </summary>
        public bool IsSingleFloatType => IsSingleType && RawTypeIsNotNull && IsTheRawType(DataRefs.TypeStart_float);
        /// <summary>
        /// Single 'double' 格式的 DataRef ?
        /// </summary>
        public bool IsSingleDoubleType => IsSingleType && RawTypeIsNotNull && IsTheRawType(DataRefs.TypeStart_double);
        /// <summary>
        /// 'string' 格式的 DataRef ?
        /// </summary>
        public bool IsStringType => TypeIsNotNull && IsTheType(DataRefs.TypeStart_string);
        /// <summary>
        /// 'int[]' 格式的 DataRef ?
        /// </summary>
        public bool IsArrayIntType => TypeIsArray && TypeIsNotNull && IsTheType(DataRefs.TypeStart_int);
        /// <summary>
        /// 'byte[]' 格式的 DataRef ?
        /// </summary>
        public bool IsArrayByteType => TypeIsArray && TypeIsNotNull && IsTheType(DataRefs.TypeStart_byte);
        /// <summary>
        /// 'float[]' 格式的 DataRef ?
        /// </summary>
        public bool IsArrayFloatType => TypeIsArray && TypeIsNotNull && IsTheType(DataRefs.TypeStart_float);
        /// <summary>
        /// 'double[]' 格式的 DataRef ?
        /// </summary>
        public bool IsArrayDoubleType => TypeIsArray && TypeIsNotNull && IsTheType(DataRefs.TypeStart_double);
        /// <summary>
        /// Type 屬性有值
        /// </summary>
        public bool TypeIsNotNull => !string.IsNullOrEmpty(Type);
        /// <summary>
        /// RawType 屬性有值
        /// </summary>
        public bool RawTypeIsNotNull => !string.IsNullOrEmpty(RawType);
        #endregion


        /// <summary>
        /// 此 DataRef 一定是 Single Type, 不是 Array, 且 Type 在定義的範圍內 (int, byte, float, double)
        /// </summary>
        public bool IsSingleType {
            get { return !TypeIsArray
                    && (UtilGlobalFunc.IsTheSingleType(_type, DataRefs.TypeStart_int)
                     || UtilGlobalFunc.IsTheSingleType(_type, DataRefs.TypeStart_byte)  
                     || UtilGlobalFunc.IsTheSingleType(_type, DataRefs.TypeStart_float)
                     || UtilGlobalFunc.IsTheSingleType(_type, DataRefs.TypeStart_double));
            }
        }


        #region 在各個系統之間, 用來傳遞 Object Data
        /// <summary>
        /// 顯示 ObjVal 的值字串
        /// </summary>
        public object ObjValShowString {
            get {
                object ShowObjVal;
                if (ObjVal == null) {
                    ShowObjVal = default;
                } else {
                    if (IsObjValTypeIntArray) { ShowObjVal = ObjVal2IntArrayString; }
                    else if (IsObjValTypeFloatArray) { ShowObjVal = ObjVal2FloatArrayString; }
                    else if (IsObjValTypeDoubleArray) { ShowObjVal = ObjVal2DoubleArrayString; }
                    else if (IsObjValTypeInt) { ShowObjVal = ObjVal2Int; }
                    else if (IsObjValTypeFloat) { ShowObjVal = ObjVal2Float; }
                    else if (IsObjValTypeDouble) { ShowObjVal = ObjVal2Double; }
                    else if (IsObjValTypeString) { ShowObjVal = ObjVal2String; }
                    else { ShowObjVal = default; }
                }
                return ShowObjVal;
            }
        }
        private int xpDataRefOffset = UtilConstants.NullOffset;
        /// <summary>
        /// 控制寫入 'X-Plane' DataRef Value Offset
        /// <para>offset 是位移量, 值從 0 開始, user 可以透過 offset 調整寫入 DataRef[i] 的起始位置</para>
        /// </summary>
        public int XPDataRefOffset { get { return xpDataRefOffset; } set { xpDataRefOffset = value; } }
        /// <summary>
        /// Object Data Bus
        /// </summary>
        public object ObjVal { get; set; }
        /// <summary>
        /// Object Data Bus convert to 'int[]'
        /// </summary>
        public int[] ObjVal2IntArray => ObjVal == null ? default : IsObjValTypeIntArray ? ObjVal.ConvertObjectToIntArray() : default;
        /// <summary>
        /// Object Data Bus convert to show 'int[]' string
        /// </summary>
        public string ObjVal2IntArrayString => ObjVal == null ? default : IsObjValTypeIntArray ? UtilGlobalFunc.GetPrecisionValue(ObjVal2IntArray) : default;
        /// <summary>
        /// Object Data Bus convert to 'float[]'
        /// </summary>
        public float[] ObjVal2FloatArray => ObjVal == null ? default : IsObjValTypeFloatArray ? ObjVal.ConvertObjectToFloatArray() : default;
        /// <summary>
        /// Object Data Bus convert to show 'float[]' string
        /// </summary>
        public string ObjVal2FloatArrayString => ObjVal == null ? default : IsObjValTypeFloatArray ? UtilGlobalFunc.GetPrecisionValue(ObjVal2FloatArray) : default;
        /// <summary>
        /// Object Data Bus convert to 'double[]'
        /// </summary>
        public double[] ObjVal2DoubleArray => ObjVal == null ? default : IsObjValTypeDoubleArray ? ObjVal.ConvertObjectToDoubleArray() : default;
        /// <summary>
        /// Object Data Bus convert to show 'double[]' string
        /// </summary>
        public string ObjVal2DoubleArrayString => ObjVal == null ? default : IsObjValTypeDoubleArray ? UtilGlobalFunc.GetPrecisionValue(ObjVal2DoubleArray) : default;
        /// <summary>
        /// Object Data Bus convert to 'int'
        /// </summary>
        public int ObjVal2Int => ObjVal == null ? default : IsObjValArrayValue ? default : IsObjValTypeString ? default : ObjVal.ConvertObjectToInt32();
        /// <summary>
        /// Object Data Bus convert to 'double'
        /// </summary>
        public double ObjVal2Double => ObjVal == null ? default : IsObjValArrayValue ? default : IsObjValTypeString ? default : IsSingleCharType ? default : ObjVal.ConvertObjectToDouble();
        /// <summary>
        /// Object Data Bus convert to 'byte'
        /// </summary>
        public byte ObjVal2Byte {
            get {
                try { return ObjVal == null ? default : IsObjValTypeByte ? ObjVal.ConvertObjectToByte() : default; }
                catch { return default; } // 超過 255 就 return 0
            }
        }
        /// <summary>
        /// Object Data Bus convert to 'byte[]'
        /// </summary>
        public byte[] ObjVal2Bytes {
            get {
                try { return ObjVal == null ? default : IsObjValTypeByteArray ? ObjVal.ConvertObjectToByteArray() : default; }
                catch { return default; }
            }
        }
        /// <summary>
        /// Object Data Bus convert to 'char'
        /// </summary>
        public char ObjVal2Char => ObjVal == null ? default : IsObjValTypeChar ? ObjVal.ConvertObjectToChar() : default;
        /// <summary>
        /// Object Data Bus convert to 'string'
        /// </summary>
        public string ObjVal2String => IsObjValArrayValue ? default : ObjVal.ConvertObjectToString();
        /// <summary>
        /// 將 ObjVal2String 轉成定義的 byte[] 長度
        /// </summary>
        public byte[] ObjVal2String2StringLengthByteArray {
            get {
                if (string.IsNullOrEmpty(ObjVal2String)) { return null; }
                char[] characters = ObjVal2String.ToCharArray();
                int len = IsStringType ? StringLength : characters.Length;
                byte[] byteA = new byte[len];
                
                int ValueIdx = 0;
                for (var i = 0; i < byteA.Length; i++) {
                    if (ValueIdx < characters.Length) {
                        byteA[i] = characters[ValueIdx].ConvertCharToByte();
                        ValueIdx++;
                    } else { break; }
                }

                return byteA;
            }
        }
        /// <summary>
        /// Object Data Bus convert to 'float'
        /// <para>注意 !!!</para>
        /// <para>1. 實測 byte, float, int, bool, double 都要轉成 float 才能順利寫入 X-Plane</para>
        /// <para>2. X-Plane 的 UDP 格式 :: 一組 id 有 4 個 bytes, 而且是 float => byte[] 才能執行</para>
        /// </summary>
        public float ObjVal2Float => ObjVal == null ? default : IsObjValArrayValue ? default : IsObjValTypeString ? default : IsSingleCharType ? default : ObjVal.ConvertObjectToFloat();
        /// <summary>
        /// Object Data Bus value is Single Value ?
        /// <para>擇一 ::  int, float, double, string, bool, byte</para>
        /// <para>判斷 Object Data Bus value 的 DataType, 是否要轉成 Float 寫入 ?</para>
        /// <para>ObjVal is null, 則為 false</para>
        /// </summary>
        public bool IsObjValSingleValue => ObjVal != null && (IsObjValTypeInt || IsObjValTypeFloat || IsObjValTypeDouble || IsObjValTypeString || IsObjValTypeBool || IsObjValTypeByte);
        /// <summary>
        /// Object Data Bus value is Array Value ?
        /// <para>擇一 ::  int[], float[], double[]</para>
        /// <para>ObjVal is null, 則為 false</para>
        /// </summary>
        public bool IsObjValArrayValue => ObjVal != null && (IsObjValTypeIntArray || IsObjValTypeFloatArray || IsObjValTypeDoubleArray);
        /// <summary>
        /// ObjVal DataType is 'byte'
        /// </summary>
        public bool IsObjValTypeByte => ObjVal.ValueIsByte();
        /// <summary>
        /// ObjVal DataType is 'char'
        /// </summary>
        public bool IsObjValTypeChar => ObjVal.ValueIsChar();
        /// <summary>
        /// ObjVal DataType is 'byte[]'
        /// </summary>
        public bool IsObjValTypeByteArray => ObjVal.ValueIsByteArray();
        /// <summary>
        /// ObjVal DataType is 'bool'
        /// </summary>
        public bool IsObjValTypeBool => ObjVal.ValueIsBool();
        /// <summary>
        /// ObjVal DataType is 'int'
        /// </summary>
        public bool IsObjValTypeInt => ObjVal.ValueIsInt();
        /// <summary>
        /// ObjVal DataType is 'float'
        /// </summary>
        public bool IsObjValTypeFloat => ObjVal.ValueIsFloat();
        /// <summary>
        /// ObjVal DataType is 'double'
        /// </summary>
        public bool IsObjValTypeDouble => ObjVal.ValueIsDouble();
        /// <summary>
        /// ObjVal DataType is 'string'
        /// </summary>
        public bool IsObjValTypeString => ObjVal.ValueIsString();
        /// <summary>
        /// ObjVal DataType is 'int[]'
        /// </summary>
        public bool IsObjValTypeIntArray => ObjVal.ValueIsIntArray();
        /// <summary>
        /// ObjVal DataType is 'float[]'
        /// </summary>
        public bool IsObjValTypeFloatArray => ObjVal.ValueIsFloatArray();
        /// <summary>
        /// ObjVal DataType is 'double[]'
        /// </summary>
        public bool IsObjValTypeDoubleArray => ObjVal.ValueIsDoubleArray();
        #endregion


        #region 是否可以複寫 ???
        /// <summary>
        /// 此 DataRef 是否可以複寫 ?
        /// <para>注意 !!!</para>
        /// <para>因為目前尚未全部的 DataRef 皆透過 function GetDataRefElement() 處理, 所以先允許 Writable 為 null 時, 一律可寫入</para>
        /// </summary>
        public bool IsWritable => string.IsNullOrEmpty(Writable) || Writable.Equals(UtilConstants.Flag_Yes);

        private string _writable = null;
        /// <summary>
        /// 數值是否可以複寫 ?
        /// </summary>
        public string Writable {
            get {
                if (string.IsNullOrEmpty(_writable))
                { _writable = UtilConstants.Flag_Yes; } // when initializing null, default 'y'
                return _writable;
            }
            set { _writable = value; }
        }
        #endregion

        /// <summary>
        /// 紀錄原始 Description 的字串長度
        /// </summary>
        private int RawDescriptionLen = UtilConstants.InitConstValue;
        private string description = null;
        /// <summary>
        /// DataRef 描述說明
        /// </summary>
        public string Description {
            get { return string.IsNullOrEmpty(description) ? string.Empty : description;  }
            set {
                if (description != value) {
                    description = value;
                    if (RawDescriptionLen == UtilConstants.InitConstValue)
                    { RawDescriptionLen = string.IsNullOrEmpty(description) ? 0 : description.Length; }
                }
            }
        }


        /// <summary>
        /// default 1000
        /// </summary>
        private int _frequency = DataRefs.DefaultFrequency;
        /// <summary>
        /// Times per seconds X-Plane will be sending this value
        /// <para>要求 X-Plane 每秒送出此 DataRef value 的次數頻率</para>
        /// <para>Frequency 值越大, X-Plane 更新 DataRef 越快</para>
        /// <para>EX : 1000, 表示 每秒回覆 1000 次, 即每 1 ms 回覆 1 次</para>
        /// </summary>
        public int Frequency { get { return _frequency; } set { _frequency = value; } }

        private int stringLength = UtilConstants.NullStringLength;
        /// <summary>
        /// String DataRef 的 字串值長度
        /// <para>如果沒有設定, 預設值為 255</para>
        /// </summary>
        public int StringLength {
            get { return stringLength == UtilConstants.NullStringLength ? UtilConstants.DefaultStringLength : stringLength; }
            set { stringLength = value; }
        }
        #endregion


        #region Send value to Air Manager, 紀錄前一次的值, 並且判斷是否與前值相同 ?
        /// <summary>
        /// 顯示送給 Air Manager 的訊息字串
        /// </summary>
        public object ShowSendMessage {
            get {
                object msg = null;
                if (IsObjValSingleValue) {
                    if (IsObjValTypeInt) { msg = SendIntValue2AM; }
                    else if (IsObjValTypeByte) { msg = SendByteValue2AM; }
                    else if (IsObjValTypeFloat) { msg = SendFloatValue2AM; }
                    else if (IsObjValTypeDouble) { msg = SendDoubleValue2AM; }
                    else if (IsObjValTypeString) { msg = SendStringValue2AM; }
                } else if (IsObjValArrayValue) {
                    if (IsObjValTypeIntArray) { msg = ObjVal2IntArrayString; }
                    else if (IsObjValTypeFloatArray) { msg = ObjVal2FloatArrayString; }
                    else if (IsObjValTypeDoubleArray) { msg = ObjVal2DoubleArrayString; }
                }
                return msg;
            }
        }
        /// <summary>
        /// 是否真的決定送值給 Air Manager
        /// </summary>
        public bool ValueSendToAM { get; private set; }
        /// <summary>
        /// 是否要將此 DataRef Value Send to Air Manager C++ Lib ?
        /// <para>1. 如果 AppConfig.json 的 DoNotNeedCompareValue 參數設定為 ture, 則 always send, 不會判斷新舊值的異動</para>
        /// <para>2. 如果 AppConfig.json 的 DoNotNeedCompareValue 參數設定為 false, 則依據 value 新舊值而定, 有異動才會 send.</para>
        /// </summary>
        public bool CheckSendValueToAirManager { get { ValueSendToAM = UtilConstants.appConfig.DoNotNeedCompareValue || !TheSameValueAsPrevious; return ValueSendToAM; } }
        /// <summary>
        /// Send value to Air Manager, 是否與前值相同 ?
        /// <para>true :: 與前值相同</para>
        /// <para>false : 與前值不同</para>
        /// </summary>
        private bool TheSameValueAsPrevious { get; set; }
        private int sendIntValue2AM = UtilConstants.InitConstValue;
        /// <summary>
        /// 紀錄當下 Send 'int' value To AM
        /// </summary>
        public int SendIntValue2AM {
            get { return sendIntValue2AM; }
            set {
                if (sendIntValue2AM != value) {
                    sendIntValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private byte sendByteValue2AM = default;
        /// <summary>
        /// 紀錄當下 Send 'byte' value To AM
        /// </summary>
        public byte SendByteValue2AM {
            get { return sendByteValue2AM; }
            set {
                if (sendByteValue2AM != value) {
                    sendByteValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private float sendFloatValue2AM = UtilConstants.InitConstValue;
        /// <summary>
        /// 紀錄當下 Send 'float' value To AM
        /// </summary>
        public float SendFloatValue2AM {
            get { return sendFloatValue2AM; }
            set {
                if (sendFloatValue2AM != value) {
                    sendFloatValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private double sendDoubleValue2AM = UtilConstants.InitConstValue;
        /// <summary>
        /// 紀錄當下 Send 'double' value To AM
        /// </summary>
        public double SendDoubleValue2AM {
            get { return sendDoubleValue2AM; }
            set {
                if (sendDoubleValue2AM != value) {
                    sendDoubleValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private string sendStringValue2AM = null;
        /// <summary>
        /// 紀錄當下 Send 'string' value To AM
        /// </summary>
        public string SendStringValue2AM {
            get { return sendStringValue2AM; }
            set {
                if (sendStringValue2AM != value) {
                    sendStringValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private string sendIntArrayValue2AM = null;
        /// <summary>
        /// 紀錄當下 Send 'int[]' value To AM
        /// </summary>
        public string SendIntArrayValue2AM {
            get { return sendIntArrayValue2AM; }
            set {
                if (sendIntArrayValue2AM != value) {
                    sendIntArrayValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private string sendFloatArrayValue2AM = null;
        /// <summary>
        /// 紀錄當下 Send 'float[]' value To AM
        /// </summary>
        public string SendFloatArrayValue2AM {
            get { return sendFloatArrayValue2AM; }
            set {
                if (sendFloatArrayValue2AM != value) {
                    sendFloatArrayValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        private string sendDoubleArrayValue2AM = null;
        /// <summary>
        /// 紀錄當下 Send 'double[]' value To AM
        /// </summary>
        public string SendDoubleArrayValue2AM {
            get { return sendDoubleArrayValue2AM; }
            set {
                if (sendDoubleArrayValue2AM != value) {
                    sendDoubleArrayValue2AM = value;
                    TheSameValueAsPrevious = false;
                } else {
                    TheSameValueAsPrevious = true;
                }
            }
        }
        #endregion

        /// <summary>
        /// 統一紀錄 DataRefElement 的 Log
        /// </summary>
        protected void Log(string msg) => UtilGlobalFunc.Log(msg);

        /// <summary>
        /// 取得 offset 起始位置
        /// </summary>
        protected int GetStartIdx(int offset) => offset == UtilConstants.NullOffset ? 0 : offset;

        /// <summary>
        /// 將 "string" value 寫入 X-Plane
        /// </summary>
        public void WriteStringIntoXP(TriggerByWhatSystem triggerBy, string value) {
            string FuncName = "WriteStringIntoXP";
            char[] characters = value.ToCharArray();

            for (var i = 0; i < StringLength; i++) {
                string IdxDataRefName = GetDataRefIdx(i);
                char idxChar = (i < characters.Length) ? characters[i] : default;
#if DEBUG
                string DefinitionHeader = string.Format(CultureInfo.CurrentCulture, "{0}() :: value({1}), Definition Length({2}), Actual Length({3})", FuncName, value, StringLength, characters.Length);
                Log(string.Format(CultureInfo.CurrentCulture, "{0}, DataRef({1}), Char({2})", DefinitionHeader, IdxDataRefName, idxChar));
#endif
                WriteElementDataRefValue(triggerBy, FuncName, IdxDataRefName, idxChar, idxChar);
            }
        }

        public void WriteElementIntoDataRefValue(TriggerByWhatSystem triggerBy, string ExeFuncName, string DataRefName, object val)
        { WriteElementDataRefValue(triggerBy, ExeFuncName, DataRefName, val); }
        //把這邊改成使用 WriteToXP(), 方式與 DataRefElement 使用同步


        /// <summary>
        /// 把 AirManager / AirForceSystem / GRPC 傳過來的值, 寫入 "X-Plane"
        /// <para>1. 值種類 ::: char, int, float, double, int[], float[], double[]</para>
        /// <para>2. 因為這邊是要寫入 X-Plane, 所以一定要先判斷是否已經有註冊至 X-Plane, 沒有註冊的一律忽略</para>
        /// </summary>
        /// <param name="triggerBy">AirManager由內部自行聽到, AirForceSystem/GRPC 需透過外部 function 執行</param>
        /// <param name="funcName">執行的 function name</param>
        /// <param name="dataRefName">Single(xxx/xx/xx), Array(xxx/xx/xx[i])</param>
        /// <param name="rawByteValue">各種 DataType 在外面自行依據自己的 DataType 轉成 byte[]</param>
        /// <param name="objVal">各種 DataType 的實際值</param>
        protected void WriteToXP(TriggerByWhatSystem triggerBy, string funcName, string dataRefName, byte[] rawByteValue, object objVal) {
            string data = string.Format(CultureInfo.CurrentCulture, "TriggerBy({0}) :: FuncName({1}), '{2}', value({3})", triggerBy, funcName, dataRefName, objVal);

            // NewOne4Update
            // 產生新的 object 單獨去寫值給 X-Plane,
            // 避免因為沿用舊的 object 而產生 "寫入值" 被更新掉
            // 舊的 object 隨時會監聽到來自 X-Plane 的更新

            var dr = XPlaneConnector.GetDataRef(dataRefName); // 這邊必須準確抓到 Single or Array[i] DataRef Object
            if (dr != null) {
                var NewOne4Update = DataRefs.GetDataRefElement(dr.DataRef, dr.Type, dr.Writable, dr.Units, dr.Description, dr.Frequency);
                NewOne4Update.RawByteValue = rawByteValue;
                NewOne4Update.ObjVal = objVal;
                DataRefs.StaticEvtInterface.OnExecuteAirManagerDataRefWrite(NewOne4Update, new DataRefEventArgs(data, NewOne4Update, null, null, triggerBy));
            } else {
                var sdr = InitParamsCtrller.GetStringDataRefElementFromInit(dataRefName);
                if (sdr != null) {
                    var NewOne4Update = DataRefs.GetStringDataRefElement(sdr.DataRef, sdr.Type, sdr.Writable, sdr.Units, sdr.Description, sdr.StringLength, sdr.Frequency);
                    NewOne4Update.RawStringValue = ObjVal2String;
                    NewOne4Update.ObjVal = objVal;
                    DataRefs.StaticEvtInterface.OnExecuteAirManagerDataRefWrite(NewOne4Update, new DataRefEventArgs(data, NewOne4Update, null, null, triggerBy));
                } else { Log(string.Format(CultureInfo.CurrentCulture, "'{0}' 並沒有註冊至 '{1}', triggerBy({2}), FuncName({3}).", dataRefName, UtilConstants.XPlaneSoftware, triggerBy, funcName)); }
            }
        }

        /// <summary>
        /// <para>0. byte[] 的 DataRef 在初始時, 會轉換成 StringDataRefElement</para>
        /// <para>1. [Single Value] ::: xxx/xx/xx (int, byte, float, double)</para>
        /// <para>2. [Array Value] ::: xxx/xx/xx[i] (int[], byte[], float[], double[])</para>
        /// <para>3. [string Value] ::: 會轉換成 float[] 寫入</para>
        /// </summary>
        protected void WriteElementDataRefValue(TriggerByWhatSystem triggerBy, string exeFuncName, string dataRefName, object val, char c = default) {
            if (this != null) {
                if (DataRefIsArray)
                { Log(string.Format(CultureInfo.CurrentCulture, "Array 分拆 ::: DataRef({0}), Type({1}), Value({2})", dataRefName, Type, val)); }

                if (c != default(char)) {
                    string UpdateCharFormat = string.Format(CultureInfo.CurrentCulture, ", UpdateChar[{0}]", c);
                    int SubStrLen = RawDescriptionLen == UtilConstants.InitConstValue ? Description.Length : RawDescriptionLen;
                    string RawDescription = Description.Substring(0, SubStrLen);
                    Description = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RawDescription, UpdateCharFormat);
                } // 將此 index 的 char 帶入 Description (Debug 使用)
                WriteDataRefValue(exeFuncName, val, triggerBy);
            } else { Log(string.Format(CultureInfo.CurrentCulture, "{0} is null.", this)); }
        }
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
        /// <para>1. 單一值, 帶入 byte, int, float, double</para>
        /// <para>2. Array值, 帶入 xxx/xx/xx[i] 的 value[i] 值 ::: int, float, double</para>
        /// </summary>
        private void WriteDataRefValue(string exeFuncName, object value, TriggerByWhatSystem triggerBy) {
            if (value == null) {
                Log(string.Format(CultureInfo.CurrentCulture, "從 '{0}' 取得 DataRef Value Write into '{1}' ::: input value is null({2})", UtilConstants.AirManagerSoftware, UtilConstants.XPlaneSoftware, value));
                Log(string.Format(CultureInfo.CurrentCulture, "No Executed to '{0}', please check '{1}' value. Trigger Function Name is '{0}()'", UtilConstants.XPlaneSoftware, UtilConstants.AirManagerSoftware, exeFuncName));
            } else {
                ObjVal = value;
                DataRefs.StaticEvtInterface.OnExecuteAirManagerDataRefWrite(this, new DataRefEventArgs(string.Format(CultureInfo.CurrentCulture, "{0}() :: {1}", exeFuncName, Description), this, triggerBy));
            }
        }

        /// <summary>
        /// <para>1. 取得 DataRef 的 Array 長度值</para>
        /// <para>2. 取得 DataRef 的 Index 值</para>
        /// </summary>
        private int GetArrayLength(string DataRefArrayType) {
            string Len = DataRefArrayType.Substring(DataRefArrayType.IndexOf("[") + 1);
            Len = Len.Substring(0, Len.Length - 1);
            return int.TryParse(Len, out int arrayLen) ? arrayLen : 0;
        }

    }
}