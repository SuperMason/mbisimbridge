﻿using Grpc.Core;
using MBILibrary.Enums;
using MBILibrary.refDLL;
using MBILibrary.Util;
using MBIServer.GrpcTesting;
using MBIServer.MbiGrpcService;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBIServer
{
    /// <summary>
    /// 啟動 gRPC Server
    /// </summary>
    public class GrpcServerStartup : BackgroundService
    {
        #region Properties
        /// <summary>
        /// 紀錄所有上線的 Client 端
        /// </summary>
        public static ServerGrpcSubscribers grpcClients = null;
        private readonly IHostApplicationLifetime _hostApplicationLifetime;
        private readonly ILogger<GrpcServerStartup> _logger;
        private Server grpcServer = null;
        /// <summary>
        /// Server 端 Host Name
        /// </summary>
        private string HostingName = string.Empty;
        /// <summary>
        /// 等待 gRPC 連線
        /// </summary>
        private const int WhileLoopingInterval = 10;
        /// <summary>
        /// gRPC Server bind status
        /// </summary>
        private bool BindingSuccess = false;
        private const string InputMessageToBroadcast = "Input Message to Broadcast ::: ";
        /// <summary>
        /// 程式剛啟動時的初始化作業
        /// </summary>
        private bool Init = true;
        /// <summary>
        /// "User Input" 廣播的行為描述
        /// </summary>
        private string description = "Server Broadcast 'User Input' to ALL Connected Clients";
        /// <summary>
        /// 已經有至少一個 Grpc Clients 連上線
        /// </summary>
        public static bool HaveClients
        { get { return grpcClients != null && grpcClients.CurrentSubscribers.Count != 0; } }
        #endregion


        #region Constructor
        static GrpcServerStartup() {
            grpcClients = new ServerGrpcSubscribers();
        }
        /// <summary>
        /// 啟動 gRPC Server
        /// </summary>
        public GrpcServerStartup(IHostApplicationLifetime hostApplicationLifetime, ILogger<GrpcServerStartup> logger) {
            _hostApplicationLifetime = hostApplicationLifetime;
            _logger = logger;
            HostingName = string.Format(CultureInfo.CurrentCulture, "{0} ::: {1}", nameof(GrpcServerStartup), string.Format(CultureInfo.CurrentCulture, UtilConstants.GrpcHostNameFormat, UtilConstants.HostRegistryLicenseGuid, UtilConstants.HostRegistryMachineName));
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, HostingName));

            while (!HaveClients) { // 如果一直沒有任何 Clients 連線, 則會持續 initializing
                DisposeGrpcServer();
                RunGrpcServer();
                SpinWait.SpinUntil(() => HaveClients, TimeSpan.FromMilliseconds(2000));
            }
        }
        #endregion


        /// <summary>
        /// 實際啟動 gRPC Server
        /// </summary>
        private void RunGrpcServer() {
            try {
                #region gRPC Server Start
                if (grpcServer == null) {
                    grpcServer = new Server {
                        Services = { // 同一個 Port 可以啟動多個 Services
                                GreetService.BindService(new GreetServiceImpl()),
                                CalculatorService.BindService(new CalculatorServiceImpl()),
                                HandleAirManager.BindService(new HandleAirManagerImpl()),
                                HandleXPlane.BindService(new HandleXPlaneImpl()),
                                HandleClientServerCommunicate.BindService(new HandleClientServerCommunicateImpl()),
                        },
                        Ports = { new ServerPort(UtilConstants.appConfig.gRPCServerIP, UtilConstants.appConfig.gRPCServerPort, ServerCredentials.Insecure) }
                    };
                    grpcServer.Start();
                }
                #endregion


                #region 訊息說明
                UtilGlobalFunc.Log($"Service 1 : {nameof(GreetService)}");
                UtilGlobalFunc.Log($"Service 2 : {nameof(CalculatorService)}");
                UtilGlobalFunc.Log($"Service 3 : {nameof(HandleAirManager)}");
                UtilGlobalFunc.Log($"Service 4 : {nameof(HandleXPlane)}");
                UtilGlobalFunc.Log($"Service 5 : {nameof(HandleClientServerCommunicate)}");
                #endregion
                BindingSuccess = true;
            } catch (Exception ex) {
                BindingSuccess = false;
                UtilGlobalFunc.Log($"Exception Message : {ex.Message}");
                StopApp();
            }
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            if (BindingSuccess) {
                while (!stoppingToken.IsCancellationRequested) {
                    await ReadMessageToBroadcast(stoppingToken);
                    if (!stoppingToken.IsCancellationRequested)
                    { await Task.Delay(WhileLoopingInterval, stoppingToken); }
                }

                if (stoppingToken.IsCancellationRequested) {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "{0} ShutDown...", UtilConstants.MBIServer));
                    StopApp();
                }
            } else { await StopAsync(stoppingToken); }// Binding gRPC Server Fail
        }

        /// <summary>
        /// 從 MBIServer 輸入訊息, 將訊息即時廣播至 ALL Connected Clients
        /// </summary>
        private async Task ReadMessageToBroadcast(CancellationToken stoppingToken) {
            try {
                string cmdLine = string.Empty;
                do {
                    if (!string.IsNullOrEmpty(cmdLine) && cmdLine.Equals(UtilConstants.ExitPattern))
                    { await StopAsync(stoppingToken); StopApp(); break; }

                    #region User Input Message
                    if (!string.IsNullOrEmpty(cmdLine)) {
                        UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Broadcast 'User Input' Message ::: {0}", cmdLine));

                        // 指定此 User Input Message 是要傳給 "Air Manager(ServerBroadcastToAirManager)" 或是 "X-Plane(ServerBroadcastToXPlane)" ? 擇一
                        //int SendMessageTarget = (int)GrpcSendMessageTargetEnum.ServerBroadcastCheckingClientAlive; // default value
                        string FirstDigital = cmdLine.Substring(0, 1);// 暫時定義 :: 取第一碼來識別
                        if (int.TryParse(FirstDigital, out int SendMessageTarget)) {
                            // 針對 ALL Connected Clients 送出 "User Input" 訊息, 如果已經關閉的 Clients, 就會自動從 Server 上剔除..
                            await BroadcastMessageToClientsAsync(SendMessageTarget.ToEnum<GrpcSendMessageTargetEnum>(), cmdLine, description);
                        } else { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "CommandLine : '{0}' is NOT valid, first char should be digital number.", cmdLine)); }
                    }
                    #endregion

                    if (Init) {
                        Init = false;
                        ThreadPool.QueueUserWorkItem((o) => {
                            var hWnd = Process.GetCurrentProcess().MainWindowHandle;
                            User32.PostMessage(hWnd, User32.WM_KEYDOWN, User32.VK_RETURN, 0);
                            // 程式自動 send "Enter" 指令給 Console.ReadLine()
                        });
                    }

                    Console.Write(InputMessageToBroadcast);
                    cmdLine = Console.ReadLine(); // 等待 User Input 指令
                } while (!string.IsNullOrEmpty(cmdLine));
            } catch (Exception ex) { UtilGlobalFunc.Log(ex.Message); await StopAsync(stoppingToken); }
        }

        /// <summary>
        /// "gRPC Server" Broadcast Message to "ALL Current Connected Clients"
        /// <para>目前有 2 類廣播訊息 ::: </para>
        /// <para>1. (手動) User Input :: 送給 Client 端時, 可能是給 "Air Manager" 或 "X-Plane"</para>
        /// <para>2. (自動) Checking Client Alive</para>
        /// </summary>
        /// <param name="grpcSendMessageTargetEnum">傳送此訊息的對象 : ServerBroadcastCheckingClientAlive, ServerBroadcastToAirManager, ServerBroadcastToXPlane</param>
        /// <param name="broadcastMessage">要廣播出去的 Message</param>
        /// <param name="description">廣播的行為描述</param>
        public static async Task BroadcastMessageToClientsAsync(GrpcSendMessageTargetEnum grpcSendMessageTargetEnum, string broadcastMessage, string description) {
            string grpcClientNum;
            if (HaveClients) {
                //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Current Connected Clients : {0}", grpcClients.CurrentSubscribers.Count));
                foreach (var subscriber in grpcClients.CurrentSubscribers.Values) {
                    var request = UtilServerFunc.GetOrderRequest(grpcSendMessageTargetEnum, subscriber.SubscriberUUID, broadcastMessage, false, description);
                    await grpcClients.BroadcastMessageAsync(request);
                }
                grpcClientNum = string.Format(CultureInfo.CurrentCulture, "({0})", grpcClients.CurrentSubscribers.Count);
            } else {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "No Any Connected Clients !!! Message({0}) is un-useful.", broadcastMessage));
                grpcClientNum = UtilConstants.GrpcClientEmpty;
            }

            UtilGlobalFunc.SetMBIServerGrpcClientNum(grpcClientNum);
        }

        private void DisposeGrpcServer() {
            if (grpcServer != null) {
                grpcServer.ShutdownAsync();
                grpcServer = null;
            }
        }

        /// <summary>
        /// 終止 Application
        /// </summary>
        private void StopApp() {
            DisposeGrpcServer();
            Dispose();
            _logger.LogCritical(string.Format(CultureInfo.CurrentCulture, "Exiting {0}...", UtilConstants.MBIServer));
            _hostApplicationLifetime.StopApplication();
        }
    }
}
