﻿using MBILibrary.Enums;
using MBILibrary.Util;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBIServer
{
    /// <summary>
    /// 專職測試 gRPC Client 寫入 AM 資料 的 Service
    /// </summary>
    public class GrpcSendTestingData2AMService : BackgroundService
    {
        #region Properties
        private readonly ILogger<GrpcSendTestingData2AMService> _logger;
        /// <summary>
        /// 持續測試 gRPC Client 寫入 AM/XP 資料
        /// <para>值必須以毫秒為單位, 轉譯為 -1 (表示無限逾時)、0 或是小於或等於 Int32.MaxValue 的正整數。</para>
        /// <para>避免發生 System.ArgumentOutOfRangeException, 因為是包在 Microsoft.Extensions.Hosting 元件裡, 這應該是元件本身的限制, 因為會死在 IHostBuilder 元件裡</para>
        /// </summary>
        private double WaitingInterval = 500; // 微毫秒
        /// <summary>
        /// "Grpc Send Testing Data 2 AM" 廣播的行為描述
        /// </summary>
        private readonly string description = string.Empty;
        private readonly GrpcSendMessageTargetEnum target = GrpcSendMessageTargetEnum.ServerBroadcastToAirManager;
        #endregion


        #region Constructor
        /// <summary>
        /// 專職測試 gRPC Client 寫入 AM 資料 的 Service
        /// </summary>
        public GrpcSendTestingData2AMService(ILogger<GrpcSendTestingData2AMService> logger) {
            _logger = logger;
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, nameof(GrpcSendTestingData2AMService)));
            description = string.Format(CultureInfo.CurrentCulture, "{0} checking", nameof(GrpcSendTestingData2AMService));
            WaitingInterval = UtilConstants.appConfig.Debug4SendDataGRPC2AM;
        }
        #endregion


        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            while (!stoppingToken.IsCancellationRequested) {
                // 剛開啟 MBIServer 時, 會先送 UtilConstants.IsRegistered 訊息過去, 這邊不能同時送出訊息, 否則會造成 Grpc Exception ERROR ::: "Only one write can be pending at a time", 所以這邊先 sleep 1 秒
                await Task.Delay(TimeSpan.FromMilliseconds(WaitingInterval), stoppingToken);
#if DEBUG
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/instrumentPanel/UFC/TIME/highlightIndex", UtilGenTestingData.GetRandomInt), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/instrumentPanel/UFC/TIME/Date", UtilGenTestingData.GetRandomString), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/aaa/a1", UtilGenTestingData.GetRandomInt), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/aaa/a2", UtilGenTestingData.GetRandomFloat), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/bbb/double", UtilGenTestingData.GetRandomDouble), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/aaa/a3", UtilGenTestingData.GetRandomString), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myInt", UtilGenTestingData.GetRandomInt), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myFloat", UtilGenTestingData.GetRandomFloat), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myDouble", UtilGenTestingData.GetRandomDouble), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myStr", UtilGenTestingData.GetRandomString), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/bbb/ia3", UtilGlobalFunc.GetPrecisionValue(UtilGenTestingData.GetRandomIntArray(3))), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myIntValueA", UtilGlobalFunc.GetPrecisionValue(UtilGenTestingData.GetRandomIntArray(3))), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/bbb/da6", UtilGlobalFunc.GetPrecisionValue(UtilGenTestingData.GetRandomDoubleArray(6))), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myDoubleValueA", UtilGlobalFunc.GetPrecisionValue(UtilGenTestingData.GetRandomDoubleArray(6))), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "mbi/bbb/fa5", UtilServerFunc.ConvertFloatArray2String(UtilGenTestingData.GetRandomFloatArray(5))), description);
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "myFloatValueA", UtilServerFunc.ConvertFloatArray2String(UtilGenTestingData.GetRandomFloatArray(4))), description);
#endif
            }
        }

        
    }
}
