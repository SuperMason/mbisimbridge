﻿using MBILibrary.Enums;
using MBILibrary.Util;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBIServer
{
    /// <summary>
    /// 專職 監聽 gRPC Client 是否連線 ?
    /// </summary>
    public class GrpcCheckClientAliveService : BackgroundService
    {
        #region Properties
        private readonly ILogger<GrpcCheckClientAliveService> _logger;
        /// <summary>
        /// 持續監聽 ALL gRPC Clients Status, Open? Close?
        /// <para>值必須以毫秒為單位, 轉譯為 -1 (表示無限逾時)、0 或是小於或等於 Int32.MaxValue 的正整數。</para>
        /// <para>避免發生 System.ArgumentOutOfRangeException, 因為是包在 Microsoft.Extensions.Hosting 元件裡, 這應該是元件本身的限制, 因為會死在 IHostBuilder 元件裡</para>
        /// </summary>
        private readonly int WaitingInterval = 1000; // 微毫秒
        /// <summary>
        /// "Checking Client Alive" 廣播的行為描述
        /// </summary>
        private readonly string description = string.Empty;
        #endregion


        #region Constructor
        /// <summary>
        /// 專職 監聽 gRPC Client 是否連線 ?
        /// </summary>
        public GrpcCheckClientAliveService(ILogger<GrpcCheckClientAliveService> logger) {
            _logger = logger;
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, nameof(GrpcCheckClientAliveService)));
            description = string.Format(CultureInfo.CurrentCulture, "{0} checking", nameof(GrpcCheckClientAliveService));
        }
        #endregion


        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            while (!stoppingToken.IsCancellationRequested) {
                // 剛開啟 MBIServer 時, 會先送 UtilConstants.IsRegistered 訊息過去, 這邊不能同時送出訊息, 否則會造成 Grpc Exception ERROR ::: "Only one write can be pending at a time", 所以這邊先 sleep 1 秒
                await Task.Delay(WaitingInterval, stoppingToken); // 每 1 秒, 持續監聽 ALL gRPC Clients Status, Open? Close?
                // 針對 ALL Connected Clients 送出 "Checking Client Alive" 訊息, 如果已經關閉的 Clients, 就會自動從 Server 上剔除..
                await GrpcServerStartup.BroadcastMessageToClientsAsync(GrpcSendMessageTargetEnum.ServerBroadcastCheckingClientAlive, UtilConstants.CheckingClientAlive, description);
            }
        }
    }
}
