﻿using MBILibrary.Enums;
using MBILibrary.Util;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBIServer
{
    /// <summary>
    /// 專職測試 gRPC Client 寫入 XP 資料 的 Service
    /// </summary>
    public class GrpcSendTestingData2XPService : BackgroundService
    {
        #region Properties
        private readonly ILogger<GrpcSendTestingData2XPService> _logger;
        /// <summary>
        /// 持續測試 gRPC Client 寫入 AM/XP 資料
        /// <para>值必須以毫秒為單位, 轉譯為 -1 (表示無限逾時)、0 或是小於或等於 Int32.MaxValue 的正整數。</para>
        /// <para>避免發生 System.ArgumentOutOfRangeException, 因為是包在 Microsoft.Extensions.Hosting 元件裡, 這應該是元件本身的限制, 因為會死在 IHostBuilder 元件裡</para>
        /// </summary>
        private double WaitingInterval = 500; // 微毫秒
        /// <summary>
        /// "Grpc Send Testing Data 2 XP" 廣播的行為描述
        /// </summary>
        private readonly string description = string.Empty;
        private readonly GrpcSendMessageTargetEnum target = GrpcSendMessageTargetEnum.ServerBroadcastToXPlane;
        #endregion


        #region Constructor
        /// <summary>
        /// 專職測試 gRPC Client 寫入 XP 資料 的 Service
        /// </summary>
        public GrpcSendTestingData2XPService(ILogger<GrpcSendTestingData2XPService> logger) {
            _logger = logger;
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, nameof(GrpcSendTestingData2XPService)));
            description = string.Format(CultureInfo.CurrentCulture, "{0} checking", nameof(GrpcSendTestingData2XPService));
            WaitingInterval = UtilConstants.appConfig.Debug4SendDataGRPC2XP;
        }
        #endregion


        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            while (!stoppingToken.IsCancellationRequested) {
                // 剛開啟 MBIServer 時, 會先送 UtilConstants.IsRegistered 訊息過去, 這邊不能同時送出訊息, 否則會造成 Grpc Exception ERROR ::: "Only one write can be pending at a time", 所以這邊先 sleep 1 秒
                await Task.Delay(TimeSpan.FromMilliseconds(WaitingInterval), stoppingToken); // 每 0.5 秒
#if DEBUG
                /*
                *X - Plane 指令範例:
                         *(2) float :: 2||-1||9876.6789F||units||sim/aircraft/autopilot/vvi_step_ft||testingFloat...
                         *(3) double :: 2||-1||123.987654321||units||sim/flightmodel/position/local_z||testingDouble...
                         *(4) int[] :: 2||-1||2,1,2,1,2,1||units||sim/cockpit/electrical/generator_on||testingIntArray...
                         *(5) float[] :: 2||-1||9.99F,8.88F,7.77F||units||sim/multiplayer/position/plane1_throttle||testingFloatArray...
                */
                int networkDataRate = UtilGenTestingData.GetRandomInt % 100;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/network/dataout/network_data_rate", networkDataRate), description);
                int acfFfHydraulic = UtilGenTestingData.GetRandomInt;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/aircraft/forcefeedback/acf_ff_hydraulic", acfFfHydraulic), description);
                int antiIceSurfHeatLeft = UtilGenTestingData.GetRandomInt;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/cockpit/switches/anti_ice_surf_heat_left", antiIceSurfHeatLeft), description);
                int chk = UtilGenTestingData.GetRandomInt % 2;
                var dataToScreen = new int[] { chk, chk, chk, chk, chk };
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/network/dataout/data_to_screen", dataToScreen), description);

                float vviStepFt = UtilGenTestingData.GetRandomFloat;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/aircraft/autopilot/vvi_step_ft", vviStepFt), description);
                float plane6The = UtilGenTestingData.GetRandomFloat;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/position/plane6_the", plane6The), description);
                double plane6X = UtilGenTestingData.GetRandomDouble;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/position/plane6_x", plane6X), description);
                double localZ = UtilGenTestingData.GetRandomDouble;
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/flightmodel/position/local_z", localZ), description);

                var plane1Throttle = UtilGenTestingData.GetRandomFloatArray(3); // 在 float[8], 位移至 index 3 後, 連續寫入 3 個值 : [3], [4], [5]
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/position/plane1_throttle", plane1Throttle), description);
                var plane6Throttle = UtilGenTestingData.GetRandomFloatArray(2); // 在 float[8], 位移至 index 2 後, 連續寫入 2 個值 : [2], [3]
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/position/plane6_throttle", plane6Throttle), description);
                var plane6GearDeploy = UtilGenTestingData.GetRandomFloatArray(1); // 在 float[10], 位移至 index 3 後, 連續寫入 1 個值 : [3]
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/position/plane6_gear_deploy", plane6GearDeploy), description);
                var engineThrottleRequest = UtilGenTestingData.GetRandomFloatArray(1);
                engineThrottleRequest[0] = 1 / engineThrottleRequest[0]; // 在 float[1], 此 DataRef 只接受 0 ~ 1 的值
                await GrpcServerStartup.BroadcastMessageToClientsAsync(target, UtilServerFunc.GetFakeInstruction(target, "sim/multiplayer/controls/engine_throttle_request", engineThrottleRequest), description);
                
#endif
            }
        }

    }
}
