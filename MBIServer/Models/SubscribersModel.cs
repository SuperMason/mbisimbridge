﻿using Grpc.Core;
using MBIServer.GrpcTesting;
using MBIServer.MbiGrpcService;
using System;

namespace MBIServer.Models
{
    /// <summary>
    /// This CLASS is used for the 'ALL MBISimBridge Clients' which are Connected to the 'MBIServer'.
    /// The RequireAuthorization method is used to define the authorization in the routing configuration.
    /// </summary>
    public class SubscribersModel
    {
        #region fields
        /// <summary>
        /// Subscriber 的 UUID 序號
        /// </summary>
        public string SubscriberUUID { get; set; }
        /// <summary>
        /// 登記時的當下時間
        /// </summary>
        public DateTime SubscribeTime { get; set; }
        #endregion


        #region Constructor
        /// <summary>
        /// 'ALL MBISimBridge Clients' which are Connected to the 'MBIServer'.
        /// </summary>
        public SubscribersModel() { }
        #endregion


        #region Server Stream Writer ::: TestingSubscriber(測試專用), MBISimBridgeSubscriber(實際 MBISimBridge 專用)
        /// <summary>
        /// Testing ::: Tester Client
        /// <para>gRPC Client : 1 ~ n</para>
        /// </summary>
        public IServerStreamWriter<HelloReply> TestingSubscriber { get; set; }

        /// <summary>
        /// Production ::: MBISimBridge Client
        /// <para>gRPC Client : 1 ~ n</para>
        /// </summary>
        public IServerStreamWriter<OrderResponse> MBISimBridgeSubscriber { get; set; }
        #endregion

    }
}
