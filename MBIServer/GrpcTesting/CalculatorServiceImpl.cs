﻿using Grpc.Core;
using MBILibrary.Util;
using System;
using System.Threading.Tasks;

namespace MBIServer.GrpcTesting
{
    public class CalculatorServiceImpl : CalculatorService.CalculatorServiceBase, IDisposable
    {
        public void Dispose()
        { UtilGlobalFunc.Log("Cleaning up"); }

        public override Task<CalReply> Plus(CalRequest request, ServerCallContext context)
        {
            //UtilGlobalFunc.Log(string.Format("Get Message From Client :: NumA({0}), NumB({1})", request.NumA, request.NumB));
            return Task.FromResult(new CalReply { Result = request.NumA + request.NumB });
        }

    }
}
