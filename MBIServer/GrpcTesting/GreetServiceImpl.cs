﻿using Grpc.Core;
using MBILibrary.Util;
using MBIServer.Models;
using System;
using System.Threading.Tasks;

namespace MBIServer.GrpcTesting
{
    public class GreetServiceImpl : GreetService.GreetServiceBase, IDisposable
    {
        public void Dispose()
        { UtilGlobalFunc.Log("Cleaning up"); }

        public override async Task SayHelloStreamTwoWay(IAsyncStreamReader<HelloRequest> requestStream
            , IServerStreamWriter<HelloReply> responseStream, ServerCallContext context)
        {
            if (!await requestStream.MoveNext()) { return; } // if empty input Streaming
            AddSubscriber(requestStream.Current.UniqueKeyOfMachine, responseStream);
            do { // 馬上回覆給 'gRPC Client' 端, 'gRPC Server' 已經登記好了, 可以開始進行通訊
                await GrpcServerStartup.grpcClients.BroadcastMessageAsync(requestStream.Current);
            } while (await requestStream.MoveNext());
        }

        private void AddSubscriber(string uuid, IServerStreamWriter<HelloReply> responseStream) {
            GrpcServerStartup.grpcClients.AddSubscriber(new SubscribersModel {
                SubscriberUUID = uuid,
                TestingSubscriber = responseStream,
            });
        }

        public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
        {
            //UtilGlobalFunc.Log(string.Format("Get Message From Client :: {0}", request.Name));
            return Task.FromResult(new HelloReply { Message = $"Hello {request.Name}" });
        }

        public override Task<HelloReply> SayHelloStreamRequest(IAsyncStreamReader<HelloRequest> requestStream, ServerCallContext context)
        { return base.SayHelloStreamRequest(requestStream, context); }

        public override Task SayHelloStreamResponse(HelloRequest request, IServerStreamWriter<HelloReply> responseStream, ServerCallContext context)
        { return base.SayHelloStreamResponse(request, responseStream, context); }

    }
}
