﻿using Google.Protobuf;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using MBILibrary.Enums;
using MBILibrary.Models;
using MBILibrary.Util;
using MBIServer.GrpcTesting;
using MBIServer.MbiGrpcService;
using MBIServer.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace MBIServer
{
    /// <summary>
    /// <para>This class implements the BroadcastMessageAsync() method and the ConcurrentDictionary of "Connected Clients" are managed here.</para>
    /// <para>This Service can be used to "Send server MESSAGES" to "ALL Connected Clients".</para>
    /// <para>If when sending a MESSAGE to a Client fails, the Client APP is killed, the broadcast will catch an exception, and Remove this Client Subscriber.</para>
    /// </summary>
    public class ServerGrpcSubscribers
    {
        #region fields
        /// <summary>
        /// 取得目前已經上線的 Client 數量
        /// </summary>
        public ConcurrentDictionary<string, SubscribersModel> CurrentSubscribers { get { return Subscribers ?? null; } }
        /// <summary>
        /// 紀錄所有 Connected Clients
        /// </summary>
        private readonly ConcurrentDictionary<string, SubscribersModel> Subscribers = null;
        /// <summary>
        /// 紀錄當下是否正在廣播? if YES, skip the MESSAGE.
        /// <para>true ::: 開始廣播, 禁止重複廣播行為</para>
        /// <para>false::: 廣播結束, 等待下一次廣播行為</para>
        /// </summary>
        private static bool IsBroadcasting = false;
        #endregion

        #region constructor
        public ServerGrpcSubscribers()
        { Subscribers = new ConcurrentDictionary<string, SubscribersModel>(); }
        #endregion


        #region Broadcast Message to "ALL Connected Clients"
        /// <summary>
        /// Start to Broadcast Message to "ALL Connected Clients"
        /// </summary>
        /// <typeparam name="T">目前有 HelloRequest, OrderRequest 兩種類型</typeparam>
        /// <param name="request">某一 Client 端所發出的 Request</param>
        public async Task BroadcastMessageAsync<T>(T request) where T : class {
            if (!IsBroadcasting) {
                //IsBroadcasting = true; 先取消判斷重複廣播的行為... // 開始廣播, flag 設為 true, 禁止重複廣播行為
                await ForEachClientsToBroadcastMessages(request);
            } else {
                string skipMessage;
                if (typeof(T) == typeof(HelloRequest)) {
                    var obj = (HelloRequest)Convert.ChangeType(request, typeof(HelloRequest));
                    skipMessage = obj.ReqMsg;
                } else if (typeof(T) == typeof(OrderRequest)) {
                    var obj = (OrderRequest)Convert.ChangeType(request, typeof(OrderRequest));
                    skipMessage = obj.CommandLine;
                } else {
                    skipMessage = "Err Type...";
                }
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "正在廣播中... skip the Message ::: {0}", skipMessage));
            }
        }
        /// <summary>
        /// ForEach ALL Connected Clients, sending MESSAGE out.
        /// </summary>
        /// <typeparam name="T">目前有 HelloRequest, OrderRequest 兩種類型</typeparam>
        /// <param name="request">某一 Client 端所發出的 Request</param>
        /// <returns></returns>
        private async Task ForEachClientsToBroadcastMessages<T>(T request) where T : class {
            foreach (var subscriber in Subscribers.Values) {
                var ClientSendingMsgOK = await SendingMessageToSubscriber(subscriber, request);
                if (ClientSendingMsgOK != null) // ClientSendingMsgOK == null, 表示 Client 端還活著, 且有正確送出 Message
                { RemoveSubscriber(ClientSendingMsgOK); }
            }
            IsBroadcasting = false; // 廣播結束, flag 設為 false, 等待下一次廣播行為
            if (Subscribers.Count != 0) {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Broadcast Finished... Subscribers({0}), Lists :::", Subscribers.Count));
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "{0}{1}", string.Join(Environment.NewLine, Subscribers.Keys), Environment.NewLine));
            } else {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "No Clients receiving. Online Clients Count : {0}", Subscribers.Count));
            }
        }
        /// <summary>
        /// 實際將 Message Broadcast to ALL Connected Clients
        /// <para>1. Success ::: return "null"</para>
        /// <para>2. Failure ::: return "subscriber client object"</para>
        /// </summary>
        /// <typeparam name="T">目前有 HelloRequest, OrderRequest 兩種類型</typeparam>
        /// <param name="subscriber">列舉所有 Client 端</param>
        /// <param name="request">某一 Client 端所發出的 Request</param>
        private async Task<SubscribersModel> SendingMessageToSubscriber<T>(SubscribersModel subscriber, T request) where T : class {
            try {
                if (typeof(T) == typeof(HelloRequest)) {
                    var obj = (HelloRequest)Convert.ChangeType(request, typeof(HelloRequest));
                    if (IsTheSameMachineOfRequest(subscriber.SubscriberUUID, obj.UniqueKeyOfMachine)) {
                        UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.SendingMessageToSubscriberFormat, obj.ReqMsg, subscriber.SubscriberUUID));
                        var response = new HelloReply {
                            UniqueKeyOfMachine = obj.UniqueKeyOfMachine,
                            Message = string.Format(CultureInfo.CurrentCulture, "Hello {0}", obj.Name),
                            ResMsg = string.Format(CultureInfo.CurrentCulture, "Server Response :: {0}", obj.ReqMsg),
                        };
                        await subscriber.TestingSubscriber.WriteAsync(response);
                    }
                } else if (typeof(T) == typeof(OrderRequest)) {
                    var obj = (OrderRequest)Convert.ChangeType(request, typeof(OrderRequest));
                    if (obj != null && string.IsNullOrEmpty(obj.CommandLine)) {
                        UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "CommandLine({0}) is null or Empty, skip this broadcasting.", obj.CommandLine));
                        return null;
                    }

                    if (IsTheSameMachineOfRequest(subscriber.SubscriberUUID, obj.UniqueKeyOfMachine)) {
                        UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.SendingMessageToSubscriberFormat, obj.CommandLine, subscriber.SubscriberUUID));
                        IList<Person> pList = new List<Person>(); // List object sample
                        IDictionary<string, CmdResponse> amCmdRes = new Dictionary<string, CmdResponse>(); // Dictionary object sample
                        UserModel addUser = new UserModel();
                        AnyResponse getUsers = new AnyResponse { IsSuccess = true };
#if DEBUG
                        if (UtilConstants.appConfig.Debug4BigMason) {
                            #region IList, IDictionary sample code
                            pList = GetPersons;
                            amCmdRes.Add(Guid.NewGuid().ToString(), new CmdResponse { UniqueKeyOfMachine = obj.UniqueKeyOfMachine, Result = "Testing...1" });
                            amCmdRes.Add(Guid.NewGuid().ToString(), new CmdResponse { UniqueKeyOfMachine = obj.UniqueKeyOfMachine, Result = "Testing...2" });
                            #endregion

                            #region proto Any Type Sample :: Add, Get, Del
                            addUser = UtilGenTestingData.GetUserModel();
                            var users = UtilGenTestingData.GetUserModels();
                            getUsers.ResultMsgs.AddRange(users.Select(a => new Any() { Value = ByteString.CopyFrom(UtilByteString.ToByteArray(a)) }));
                            #endregion
                        }
#endif
                        #region Write DataRef Value To "X-Plane", "Air Manager"
                        /** MbiServer ::
                         *  Air Manager 指令範例 : 
                         *  (1) int :: 1||-1||852||units||mbi/aaa/a1||testingInt...
                         *  (2) float :: 1||-1||741.5678F||units||mbi/aaa/a2||testingFloat...
                         *  (3) double :: 1||-1||951.369||units||mbi/bbb/double||testingDouble...
                         *  (4) int[] :: 1||5||74,34,54||units||mbi/bbb/ia3||testingIntArray...
                         *  (5) float[] :: 1||3||11.123F,22.852F,33.147F,44.852F,55.753F||units||mbi/bbb/fa5||testingFloatArray...
						 *  (6) double[] :: 1||-1||753.369,111.345,12.34,56.32,11.234,67.865||units||mbi/bbb/da6||testingDoubleArray...
						 *  (7) string :: 1||3||MasonTest||units||mbi/aaa/a3||testingString...
						 *  
						 *  X-Plane 指令範例 : 
                         *  (1) int :: 2||-1||12||units||sim/network/dataout/network_data_rate||testingInt...
                         *  (2) float :: 2||-1||9876.6789F||units||sim/aircraft/autopilot/vvi_step_ft||testingFloat...
                         *  (3) double :: 2||-1||123.987654321||units||sim/flightmodel/position/local_z||testingDouble...
                         *  (4) int[] :: 2||2||2,1,2,1,2,1||units||sim/cockpit/electrical/generator_on||testingIntArray...
                         *  (5) float[] :: 2||0||9.99F,8.88F,7.77F||units||sim/multiplayer/position/plane1_throttle||testingFloatArray...
                         *  
                            1. 第 1 區 : 傳送此訊息的對象
                                0 : Server Broadcast To Client for checking alive
                                1 : Server Broadcast To "Air Manager"
                                2 : Server Broadcast To "X-Plane"
                                3 : "gRPC Client" 端, 送 Message 給 "gRPC Server" 端
                            2. 第 2 區 : offset
                                -1, 預設, 從 0 開始
                                 0, 從 index 0 開始
                                 2, 從 index 2 開始
                                 n, 從 index n 開始
                            3. 第 3 區 : Value... int, float, double, int[], float[]
                                int : "99"
                                float : "9876.6789F"
                                double : "33333.987654321"
                                int[] : "1,2,3,4,5"
                                float[] : "9.99F,8.88F,7.77F"
                            4. 第 4 區 : Units = "meters"
                            5. 第 5 區 : Dataref  "sim/network/dataout/network_data_rate"
                            6. 第 6 區 : Description  "testing..."
                        */
                        GrpcSendMessageTargetEnum target = GrpcSendMessageTargetEnum.ServerBroadcastCheckingClientAlive;
                        int offset = UtilConstants.NullOffset;
                        object val = null; // int, float, double, int[], float[]
                        string units = null, dataRef = null, descr = null;
                        UtilServerFunc.SplitCommandLine(obj.CommandLine, ref target, ref offset, ref val, ref units, ref dataRef, ref descr);

                        if (UtilServerFunc.IsUserInputCommandLine(obj.CommandLine)) {
                            if (val == null || string.IsNullOrEmpty(dataRef)) {
                                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "MUST have value, please check it again !!! DataRef({0}), Value({1})", dataRef, val));
                                val = obj.CommandLine;
                                dataRef = UtilConstants.NullDataRefName;
                                units = UtilConstants.NullUnits;
                                descr = UtilConstants.NullDescr;
                                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Replace default ::: DataRef({0}), Value({1})", dataRef, val));
                            }
                        }// else { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "CommandLine({0}) is NOT user input.", obj.CommandLine)); }

                        #region To "X-Plane"
                        WriteDataRefValueToXPlane WriteDataRefValueToXP = null;
                        if (obj.SendMessageTarget.Value.ToEnum<GrpcSendMessageTargetEnum>() == GrpcSendMessageTargetEnum.ServerBroadcastToXPlane) {
                            WriteDataRefValueToXP = new WriteDataRefValueToXPlane {
                                Dataref = dataRef,
                                Description = descr,
                                Units = units,
                                Val = new Any { Value = ByteString.CopyFrom(UtilByteString.ToByteArray(val)) },
                                Offset = offset,
                            };
                        }
                        #endregion

                        #region To "Air Manager"
                        WriteDataRefValueToAirManager WriteDataRefValueToAM = null;
                        if (obj.SendMessageTarget.Value.ToEnum<GrpcSendMessageTargetEnum>() == GrpcSendMessageTargetEnum.ServerBroadcastToAirManager) {
                            WriteDataRefValueToAM = new WriteDataRefValueToAirManager {
                                Dataref = dataRef,
                                Description = descr,
                                Units = units,
                                Val = new Any { Value = ByteString.CopyFrom(UtilByteString.ToByteArray(val)) },
                                Offset = offset,
                            };
                        }
                        #endregion
                        #endregion

                        #region Write to "gRPC Client"
                        var response = new OrderResponse {
                            UniqueKeyOfMachine = obj.UniqueKeyOfMachine,
                            ServerResponse = obj.CommandLine,
                            IsRegistered = obj.IsRegistered,
                            SendMessageTarget = obj.SendMessageTarget,
                            CurrentDateTime = Timestamp.FromDateTimeOffset(DateTimeOffset.Now),
                            CurrentTimeSpan = Duration.FromTimeSpan(TimeSpan.FromMinutes(1)),
                            Persons = { pList },
                            AmCmdRes = { amCmdRes },
                            AnyAddRes = new AnyResponse {
                                IsSuccess = true,
                                ResultMsg = (addUser == null) ? null : new Any { Value = ByteString.CopyFrom(UtilByteString.ToByteArray(addUser)) }
                            },
                            AnyDelRes = new AnyResponse {
                                IsSuccess = true,
                                ResultMsg = new Any { Value = ByteString.CopyFrom(UtilByteString.ToByteArray("User has deleted OK !!!")) }
                            },
                            AnyGetRes = getUsers,
                            WriteDataRefValueToXPlane = WriteDataRefValueToXP,
                            WriteDataRefValueToAirManager = WriteDataRefValueToAM,
                        };
                        await subscriber.MBISimBridgeSubscriber.WriteAsync(response);
                        #endregion
                    }
                } else { throw new Exception(string.Format(CultureInfo.CurrentCulture, "The Request DataType '{0}' is NOT defined, please check it.", typeof(T))); }
                return null; // Success Send Message
            }
            catch (TaskCanceledException ex) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.GrpcTaskCanceledExceptionMsg, nameof(ServerGrpcSubscribers), ex.Message, ex.StackTrace)); }
            catch (RpcException ex) {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "'{0}' sent MESSAGE fail, remove it. Exception Message ::: {1}", subscriber.SubscriberUUID, ex.Message));
                /*RpcExceptionModel rpcException = JsonUtil.GetInstanceFromJSON<RpcExceptionModel>(ex.Status.DebugException.Message);
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "\nStatus ::: {0}\nGrpcMessage ::: {1}\nDescription ::: {2}"
                    , rpcException.GrpcStatus.ToEnumName<StatusCode>(), rpcException.GrpcMessage, rpcException.Description));
                switch (rpcException.GrpcStatus.ToEnum<StatusCode>()) {
                    case StatusCode.Unknown: break;
                    case StatusCode.Aborted: break;
                    case StatusCode.AlreadyExists: break;
                    case StatusCode.Cancelled: break;
                    case StatusCode.DataLoss: break;
                    case StatusCode.DeadlineExceeded: break;
                    case StatusCode.FailedPrecondition: break;
                    case StatusCode.NotFound: break;
                    case StatusCode.Unavailable: break;
                    case StatusCode.Unimplemented: break;
                    default: break;
                }*/
            } catch (Exception ex) {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "'{0}' sent MESSAGE fail, remove it. Exception Message ::: {1}", subscriber.SubscriberUUID, ex.Message));
                if (UtilConstants.SkipErrorException.Contains(ex.Message)) { return null; }
            }

            return subscriber; // Failure Send Message
        }
        /// <summary>
        /// HostRegistryLicenseGuid, HostRegistryMachineName 必須一樣才算是該 Client 端所發出的 Request
        /// <para>1. HostRegistryLicenseGuid Match</para>
        /// <para>2. HostRegistryMachineName Match</para>
        /// </summary>
        /// <param name="ForeachUniqueKeyOfMachine">AppName:UtilConstants.HostRegistryLicenseGuid:UtilConstants.HostRegistryMachineName</param>
        /// <param name="RequestUniqueKeyOfMachine">AppName:UtilConstants.HostRegistryLicenseGuid:UtilConstants.HostRegistryMachineName</param>
        private bool IsTheSameMachineOfRequest(string ForeachUniqueKeyOfMachine, string RequestUniqueKeyOfMachine)
        {
            var foreachUUID = UtilGlobalFunc.GetDetailOfUniqueKeyOfMachine(ForeachUniqueKeyOfMachine);
            var requestUUID = UtilGlobalFunc.GetDetailOfUniqueKeyOfMachine(RequestUniqueKeyOfMachine);
            if (foreachUUID == null || requestUUID == null)
            { return false; }
            else if (foreachUUID.Count != 3 || requestUUID.Count != 3)
            { return false; }
            else {
                return foreachUUID[1].Equals(requestUUID[1]) // HostRegistryLicenseGuid Match
                    && foreachUUID[2].Equals(requestUUID[2]);// HostRegistryMachineName Match
            }
        }
        /// <summary>
        /// 移除已經斷線的 Client 端
        /// <para>廣播失敗, 連線已經中斷, 無法傳送給該 Client 端, 移除該 Client 端, 避免一直傳送失敗</para>
        /// </summary>
        public void RemoveSubscriber(SubscribersModel subscriber) {
            try {
                if (subscriber == null) { return; }
                bool res = Subscribers.TryRemove(subscriber.SubscriberUUID, out SubscribersModel item);
                if (res) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Force Remove ::: '{0}' - no longer works (Dis-Connected)", item.SubscriberUUID)); }
            } catch (Exception ex) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Could NOT remove '{0}', exception ::: {1}", subscriber.SubscriberUUID, ex.Message)); }
        }
        #endregion


        /// <summary>
        /// 取得測試用的 Persons
        /// </summary>
        private List<Person> GetPersons => new List<Person>() { GetPerson(UtilGenTestingData.GetRandomInt), GetPerson(UtilGenTestingData.GetRandomInt), GetPerson(UtilGenTestingData.GetRandomInt), GetPerson(UtilGenTestingData.GetRandomInt), };
        /// <summary>
        /// 取得測試用的 Person
        /// </summary>
        private Person GetPerson(int pid) => new Person() { Pid = pid, Name = string.Format(CultureInfo.CurrentCulture, "Person{0}", pid), Active = UtilGenTestingData.GetRandomBool, CurrentDateTime = Timestamp.FromDateTimeOffset(DateTimeOffset.Now), };


        #region Check & Add new Subscriber
        /// <summary>
        /// 此 Subscriber 是否已經登記過 ?
        /// </summary>
        private bool IsExist(SubscribersModel subscriber) => subscriber == null || Subscribers.ContainsKey(subscriber.SubscriberUUID);
        /// <summary>
        /// 新增已連線的 Client
        /// </summary>
        /// <param name="subscriber">Connected Client</param>
        public void AddSubscriber(SubscribersModel subscriber) {
            if (subscriber == null) { return; }
            if (!IsExist(subscriber)) {
                subscriber.SubscribeTime = DateTime.Now;// 紀錄登記時的時間點
                string MsgFormat = Subscribers.TryAdd(subscriber.SubscriberUUID, subscriber) ? "New Subscriber Connected: '{0}'" : "Could NOT ADD Subscriber: '{0}'";
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, MsgFormat, subscriber.SubscriberUUID));
            }
        }
        #endregion

    }
}
