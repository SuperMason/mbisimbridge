﻿using Google.Protobuf.WellKnownTypes;
using MBILibrary.Enums;
using MBILibrary.Util;
using MBIServer.MbiGrpcService;
using System;
using System.Globalization;

namespace MBIServer
{
    public sealed class UtilServerFunc
    {
        private UtilServerFunc() { }
        /// <summary>
        /// gRPC Server 取得與 gRPC Client 溝通的 Message Package
        /// </summary>
        /// <param name="grpcSendMessageTargetEnum">傳送此訊息的對象 : ServerBroadcastCheckingClientAlive, ServerBroadcastToAirManager, ServerBroadcastToXPlane</param>
        /// <param name="theGrpcClientUniqueKeyOfMachine">gRPC Client 的 UUID</param>
        /// <param name="commandLine">gRPC Server 下達給 gRPC Client 的指令字串</param>
        /// <param name="isRegistered">是否為初始化階段, 進行登記行為???</param>
        /// <param name="description">廣播的行為描述</param>
        public static OrderRequest GetOrderRequest(GrpcSendMessageTargetEnum grpcSendMessageTargetEnum, string theGrpcClientUniqueKeyOfMachine,
                                                   string commandLine, bool isRegistered, string description) {
            return new OrderRequest {
                UniqueKeyOfMachine = theGrpcClientUniqueKeyOfMachine,
                TimeTick = UtilGlobalFunc.GetOnLineTimeTick,
                CommandLine = commandLine,
                IsRegistered = isRegistered,
                Description = description,
                SendMessageTarget = (int)grpcSendMessageTargetEnum,
                CurrentDateTime = Timestamp.FromDateTimeOffset(DateTimeOffset.Now),
                CurrentTimeSpan = Duration.FromTimeSpan(TimeSpan.FromMinutes(1))
            };
        }

        /// <summary>
        /// 假裝 "User Input 指令", 進行壓力測試
        /// </summary>
        public static string GetFakeInstruction(GrpcSendMessageTargetEnum grpcTarget, string DataRefName, object value) => string.Format("{0}||-1||{1}||units||{2}||testing..", (int)grpcTarget, value, DataRefName);
        /// <summary>
        /// Get float[] 字串值 for "User Input 指令" ::: "9.99F,8.88F,7.77F"
        /// </summary>
        public static string ConvertFloatArray2String(float[] value) {
            string result = string.Empty;
            if (value == null) { return result; }
            foreach (var f in value) { result += string.Format("{0}F,", f); }
            return string.IsNullOrEmpty(result) ? result : result.Substring(0, result.Length - 1);
        }

        /// <summary>
        /// 是否為 User 手動輸入的指令 ?
        /// <para>1. 系統指令 ::: "Checking Client Alive", "is Subscribed OK."</para>
        /// <para>2. 其餘皆為 ::: "User Input 指令"</para>
        /// </summary>
        public static bool IsUserInputCommandLine(string srcCommandLine) => string.IsNullOrEmpty(srcCommandLine) || !(srcCommandLine.Equals(UtilConstants.CheckingClientAlive) || srcCommandLine.Equals(UtilConstants.IsRegistered));

        /// <summary>
        /// "User Input 指令", 分拆 6 個區域, 用 "||" 區隔開
        /// <para>指令範例 ::: 2||-1||77||packets||sim/network/dataout/network_data_rate||Description</para>
        /// </summary>
        /// <param name="srcCommandLine">"User Input 指令"</param>
        /// <param name="target">第 1 區 :: 傳送此訊息的對象</param>
        /// <param name="offset">第 2 區 :: offset, Array 起始位置位移</param>
        /// <param name="val">第 3 區 :: DataType ::: int, float, double, int[], float[], double[]</param>
        /// <param name="Units">第 4 區 :: DataRef 單位</param>
        /// <param name="DataRef">第 5 區 :: DataRef Name</param>
        /// <param name="Descr">第 6 區 :: DataRef 描述說明</param>
        public static void SplitCommandLine(string srcCommandLine, ref GrpcSendMessageTargetEnum target, ref int offset, ref object val, ref string units, ref string dataRef, ref string descr) {
            try {
                if (IsUserInputCommandLine(srcCommandLine)) {
                    int PartialNum = 6;
                    var AryRes = srcCommandLine.Split(UtilConstants.CommandLineSplitor);
                    if (AryRes.Length >= PartialNum) {
                        target = int.TryParse(AryRes[0], out int TargetInt) ? TargetInt.ToEnum<GrpcSendMessageTargetEnum>() : GrpcSendMessageTargetEnum.ServerBroadcastCheckingClientAlive;
                        offset = int.TryParse(AryRes[1], out int OffsetInt) ? (OffsetInt > 0) ? OffsetInt : UtilConstants.NullOffset : UtilConstants.NullOffset;
                        object tpVal = GetValue(AryRes[2]);
                        val = tpVal ?? srcCommandLine;
                        units = string.IsNullOrEmpty(AryRes[3]) ? UtilConstants.NullUnits : AryRes[3];
                        dataRef = string.IsNullOrEmpty(AryRes[4]) ? UtilConstants.NullDataRefName : AryRes[4];
                        descr = string.IsNullOrEmpty(AryRes[5]) ? UtilConstants.NullDescr : AryRes[5];
                    } else { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "指令至少要有 {0} 個區域, 中間用 {1} 隔開", PartialNum, UtilConstants.CommandLineSplitor)); }
                }// else { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "CommandLine({0}) is NOT user input.", SrcCommandLine)); }
            } catch (Exception ex) { UtilGlobalFunc.Log(ex.StackTrace); }
        }
        /// <summary>
        /// 先統一用 double, double[] 回傳, 之後再依據該 DataRef's DataType 轉置型態
        /// <para>int ::: 77</para>
        /// <para>float ::: 12345.6789F</para>
        /// <para>double ::: 22222.987654321</para>
        /// <para>int[] ::: 1,1,1,1,1,1</para>
        /// <para>float[] ::: 9.99F,8.88F,7.77F</para>
        /// <para>double[] ::: 9.99,8.88,7.77</para>
        /// <para>string ::: MasonTest</para>
        /// </summary>
        private static object GetValue(string AryRes2) {
            if (string.IsNullOrEmpty(AryRes2)) { return null; }

            string ArraySplitor = ",";
            if (AryRes2.Contains(ArraySplitor)) {
                // Array Value ::: int[], float[], double[]
                string[] tp = AryRes2.Split(ArraySplitor);
                if (tp != null && tp.Length != 0) {
                    if (IsFloat(tp[0])) { return tp.ConvertStringArrayToFloatArray(); }
                    else if (IsDouble(tp[0])) { return tp.ConvertStringArrayToDoubleArray(); }
                    else { return tp.ConvertStringArrayToIntArray(); }
                } else { return null; }
            } else {
                // Single Value ::: int, float, double, string
                if (IsFloat(AryRes2)) { return AryRes2.ConvertToFloat(); }
                else if (IsDouble(AryRes2)) { return AryRes2.ConvertToDouble(); }
                else { try { return AryRes2.ConvertToInt(); } catch { return AryRes2; } }
            }
        }

        private static bool IsFloat(string s) => s.Contains(".") && s.Contains("F");
        private static bool IsDouble(string s) => s.Contains(".") && !s.Contains("F");

    }
}
