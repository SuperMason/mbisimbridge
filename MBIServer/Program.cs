﻿using MBILibrary.Util;
using System.IO;
using System.Reflection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using System;
using System.Globalization;

namespace MBIServer
{
    class Program
    {
        static void Main(string[] args) {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime; // 設定此 app 在 CPU 的執行優先順序為 "即時" 等級
            Process.GetCurrentProcess().Refresh();
#if (!DEBUG)
            UtilGlobalFunc.MiniSizeConsoleWindow();
#endif
            using (var app = new SingletonCheck(UtilConstants.MBIServer)) {
                if (!app.isAnotherInstanceOpen) {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.MutexIdMsg, UtilConstants.MutexId));
                    UtilGlobalFunc.Log(UtilConstants.UniqueKeyOfMachine);
                    UtilGlobalFunc.SetMBIServerGrpcClientNum(UtilConstants.GrpcClientEmpty);

                    if (UtilConstants.appConfig.gRPCon) {
                        RunSingletoneApp(args);
                    } else {
                        string gRPCoffMsg = UtilGlobalFunc.GetGRPCoffMsg(UtilConstants.PressAnyKey);
                        UtilGlobalFunc.SetMBIServerConsoleTitleMessage(gRPCoffMsg);
                        UtilGlobalFunc.Log(gRPCoffMsg);
                        Console.ReadLine();
                    }
                }
            }
        }
        /// <summary>
        /// Start Main Application
        /// </summary>
        private static void RunSingletoneApp(string[] args) {
            #region 實例化 IHostBuilder :: new HostBuilder()
            CreateHostBuilder(args)
                .Build()//只會執行一次
                .Run()//執行應用程式並阻止呼叫執行緒，直到主機關閉
                ;
            #endregion
        }

        #region 實例化 IHostBuilder :: new HostBuilder(), 啟動一個獨立的背景應用程式, 並阻止呼叫執行緒, 直到主程式關閉
        /// <summary>
        /// 實例化 IHostBuilder
        /// <para>主要用來提供應用程式一個標準的啟動，</para>
        /// <para>包含注入DI、紀錄Logger、組態Config，</para>
        /// <para>不同的應用程式框架 (HostService) 有不同的預設啟動設定，</para>
        /// <para>.NET Generic Host 讓我們的應用程式的生命週期的控制，啟動到結束的撰寫方式統一了。</para>
        /// </summary>
        /// <param name="args">Command Line args</param>
        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)//實例化 HostBuilder
                .UseContentRoot(Directory.GetCurrentDirectory())//指定Host要使用的內容根目錄
                .ConfigureServices(services => {
                    if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.Debug4SendDataGRPC2AM != UtilConstants.Debug4SendDataDisable) { services.AddHostedService<GrpcSendTestingData2AMService>(); }// 啟動測試 gRPC Client 寫入 AM 資料 的 Service
                    if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.Debug4SendDataGRPC2XP != UtilConstants.Debug4SendDataDisable) { services.AddHostedService<GrpcSendTestingData2XPService>(); }// 啟動測試 gRPC Client 寫入 XP 資料 的 Service
                    services.AddHostedService<GrpcCheckClientAliveService>();// 啟動監聽 gRPC Client 是否連線 的 Service
                    services.AddHostedService<GrpcServerStartup>(); // 註冊 "GrpcServerStartup" 進去 Host 的 DI Container 裡面
                    // *** 實例化 IHostedService ***
                    // IHostedService 是用來實現來完成應用程序的 "工作"，當 Host 啟動後，也就是呼叫 IHostedService.StartAsync()，
                    // 這時它會執行 DI Container 裡面, 已註冊的所有 BackgroundService、IHostedService 的物件 和 BackgroundService.ExecuteAsync() 方法
                })
                // IHostBuilder 擴充方法, 用來設定應用程式其他的設定，可多次呼叫，結果會累加(後面蓋掉前面)。
                // 1. ConfigureServices() :: 將物件(服務) 加入 DI Container
                // 2. ConfigureLogging() :: 加入 Log Provider
                // 3. ConfigureAppConfiguration() :: 設定其餘的組態
        #region 配置初始化(環境變數、appsettings.json、User Secrets)
                .ConfigureHostConfiguration(config => { //載入 "主機" 組態 (Host Configuration)
                    config.AddEnvironmentVariables(prefix: "DOTNET_");
                    if (args != null)
                    { config.AddCommandLine(args); }
                })
                .ConfigureAppConfiguration((hostingContext, config) => {//載入 "應用程式" 組態 (ASP.NET Core/Console App)
                    var env = hostingContext.HostingEnvironment;
                    bool reloadOnChange = hostingContext.Configuration.GetValue("hostBuilder:reloadConfigOnChange", defaultValue: true);

                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: reloadOnChange)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: reloadOnChange);

                    if (env.IsDevelopment() && !string.IsNullOrEmpty(env.ApplicationName))
                    {
                        var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                        if (appAssembly != null)
                        { config.AddUserSecrets(appAssembly, optional: true); }
                    }

                    config.AddEnvironmentVariables();

                    if (args != null)
                    { config.AddCommandLine(args); }
                })
        #endregion
            ;
        #endregion
    }
}
