﻿using Grpc.Core;
using MBILibrary.Util;
using System;
using System.Threading.Tasks;

namespace MBIServer.MbiGrpcService
{
    public class HandleAirManagerImpl : HandleAirManager.HandleAirManagerBase, IDisposable
    {
        public void Dispose()
        { UtilGlobalFunc.Log("Cleaning up"); }

        public override Task<DataRefResponse> ExecuteDataRefWrite(DataRefRequest request, ServerCallContext context)
        {
            string val = string.IsNullOrEmpty(request.SValue) ? request.FValue.ToString() : request.SValue;
            string msg = string.Format("DataRef({0}), Value({1}), Description({2})", request.DataRefName, val, request.Description);

            //UtilGlobalFunc.Log(string.Format("Get Message From Client By HandleAirManager ExecuteDataRefWrite Service :: {0}", msg));
            return Task.FromResult(new DataRefResponse { Result = $"Yes! Get HandleAirManager ExecuteDataRefWrite Service {msg}" });
        }

        public override Task<CmdResponse> ExecuteCommand(CmdRequest request, ServerCallContext context)
        {
            string msg = string.Format("Command({0}), Value({1}), Description({2})", request.Name, request.Value, request.Description);

            //UtilGlobalFunc.Log(string.Format("Get Message From Client By HandleAirManager ExecuteCommand Service :: {0}", msg));
            return Task.FromResult(new CmdResponse { Result = $"Yes! Get HandleAirManager ExecuteCommand Service {msg}" });
        }

    }
}
