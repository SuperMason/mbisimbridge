﻿using Grpc.Core;
using MBILibrary.Util;
using MBIServer.Models;
using System;
using System.Threading.Tasks;

namespace MBIServer.MbiGrpcService
{
    public class HandleClientServerCommunicateImpl : HandleClientServerCommunicate.HandleClientServerCommunicateBase, IDisposable
    {
        public void Dispose()
        { UtilGlobalFunc.Log("Cleaning up"); }

        public override async Task SendingOrder(IAsyncStreamReader<OrderRequest> requestStream
            , IServerStreamWriter<OrderResponse> responseStream, ServerCallContext context)
        {
            if (!await requestStream.MoveNext()) { return; } // if empty input Streaming
            AddSubscriber(requestStream.Current.UniqueKeyOfMachine, responseStream);
            do { // 馬上回覆給 'gRPC Client' 端, 'gRPC Server' 已經登記好了, 可以開始進行通訊
                await GrpcServerStartup.grpcClients.BroadcastMessageAsync(requestStream.Current);
            } while (await requestStream.MoveNext());
        }

        private void AddSubscriber(string uuid, IServerStreamWriter<OrderResponse> responseStream) {
            GrpcServerStartup.grpcClients.AddSubscriber(new SubscribersModel {
                SubscriberUUID = uuid,
                MBISimBridgeSubscriber = responseStream,
            });
        }

    }
}
