﻿using Grpc.Core;
using MBILibrary.Util;
using System;
using System.Threading.Tasks;

namespace MBIServer.MbiGrpcService
{
    public class HandleXPlaneImpl : HandleXPlane.HandleXPlaneBase, IDisposable
    {
        public void Dispose()
        { UtilGlobalFunc.Log("Cleaning up"); }

        public override Task<ConnResponse> Connected(ConnRequest request, ServerCallContext context)
        {
            string msg = string.Format("Conn Message Response = {0}", request.ConnMessage);
            //UtilGlobalFunc.Log(string.Format("Get Message From Client By HandleXPlane Connected Service :: {0}", msg));
            return Task.FromResult(new ConnResponse { Result = $"Yes! Get HandleXPlane Connected Service {msg}" });
        }

        public override Task<DataRefResponse> ReadDataRef(DataRefRequest request, ServerCallContext context)
        {
            string val = string.IsNullOrEmpty(request.SValue) ? request.FValue.ToString() : request.SValue;
            string msg = string.Format("DataRef({0}), Value({1}), Description({2})", request.DataRefName, val, request.Description);
            //UtilGlobalFunc.Log(string.Format("Get Message From Client By HandleXPlane ReadDataRef Service :: {0}", msg));
            return Task.FromResult(new DataRefResponse { Result = $"Yes! Get HandleXPlane ReadDataRef Service {msg}" });
        }

    }
}
