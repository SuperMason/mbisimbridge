# XPlaneConnector

latest updated date : 2022/07/04 17:00:00

## 完成項目:
01. 與 X-Plane 的即時通訊, 從 WinForm 改成做在 mBlockHandler.XPlaneSystem
02. XPlaneConnector Project 專門處理與 X-Plane 的溝通行為
03. XPlaneConnector.Instructions Project 專門定義與 X-Plane 的 Commands, DataRefs, 用來與 X-Plane 同步指令
04. XPlaneConnectorExample Project 原本用來控制與 X-Plane 互動行為, 現在全部改做到 mBlockHandler.XPlaneSystem
05. 增加 mBlockHandler Project 主要用來當作是 Air Manager 與 X-Plane 兩套軟體之間的 interface, 並且處理主要的企業邏輯
06. 在 mBlockHandler.AirManagerSystem 中, 增加 event delegate 給 C++ 註冊使用
07. 移除 XPlaneConnectorExample WinForm Project
08. 移除上層多餘的 folder : /XPlaneConnector/
09. align and refactor code
10. merge code for Zac
11. 改善讀取 X-Plane DataRef 效能, merge AM stringType
12. 驗證 DataRefs value 已經可以從 X-Plane 同步至 Air Manager, 也可以從 Air Manager 下 Command 至 X-Plane
13. 新增 x64 的 build project, Air Manager 有提供 x64 的 .lib, 所以嘗試以 x64 的方式執行... 看會不會比 x86 效能快一些
14. .NetFramework 全部統一為  v4.6.1
15. 修正與 Air Manager 連線不穩的狀況, 增加 static ManagedCallback _callback, 固定好 function 的記憶體位置
16. 加快與 X-Plane 之間的連線存取速度, 目前 run 在 Server 上平均 10 ~ 25 milliseconds, Client NB 平均 80 ~ 110 milliseconds
	16.1. 增加 Stopwatch 物件進行耗時監控
	16.2. 調整 DataRef 的 Frequency 屬性值, 這個屬性影響 X-Plane response UDP 的速度加快非常多
17. 加速 C# 的 Parsing UDP data 速度
	17.1. 判斷 DataRef 的 Value 是否有變動, 改用 window 的 msvcrt.dll 來比對新舊 byte[], 加速 Parsing UDP data 的速度
	17.2. DataRef.Frequency 可決定 X-Plane 的 response 速度, 現在所有的 DataRef 改成每秒回覆1000次, 也就是說, 每一毫秒回覆1次, 但 server 也頂多平均 20 ms 回覆一次而已, 無法真的 1毫秒回覆1次
	17.3. 把 event 傳遞參數 float 改成 byte[], 因為 X-Plane 就是用 byte[] 在處理, 所以直接 bypass 給 Air Manager
	17.4. 增加 StringDataRef 的邏輯判斷
18. align code
	18.1. align AM's stringType
	18.2. align XPlane's "float" to "byte[]"
	18.3. align XPlane's "DataRef" Request 格式定義, Frequency 為定義要求 XPlane 回覆頻率, 目前要求 XPlane 每秒回覆 1000 次，即每毫秒回覆 1 次
19. align code
	19.1. X-Plane DataRef Write, 只剩 byte[] string 無法寫入(X-Plane 本身的 bug)
	19.2. 用 partial class 分類 DataRefs
	19.3. 區分 build mode, [debug] 的 X-Plane 為 127.0.0.1, [release] 為 192.168.0.2
20. reFactor DataRefs
21. align code
	21.1. code refactor
	21.2. project refactor
	21.3. 將 DataRef read/write, execute Command 與 AirManager SimBus 整併, 改成以 object level 為串接介面
	21.4. Air Manager Wrapper 的 StringProperty 過濾 nullptr, NULL 的 exception
	21.5. 將 Command 指令分類
	21.6. 將 XPlaneConnector Project 的 Project 類型, 從 .NET Standard 2.0 改成 .NET Framework 4.6.1, 這樣才能夠 Reference 外部 Project, 例如 : AirManagerAPICallbackDefine Project
	21.7. 在 APIWrapper Unit Test Project 中, 增加 第 3 點 的範例程式
	21.8. 把 XPlaneConnector.Instructions Project 整併進去 XPlaneConnector Project, 因為這樣才能夠完成 第 3 點 的目標
22.
	22.1. 將 Air Manager 的 SimBus 'Tag', 'UDP port' 拉出來設定, 預設值 : Tag(MBISimBridge), Port(65000), 設定在 UtilConstants.cs 的 global variable
	22.2. 將 開發工具由 VS2017 改成 VS2019, 有調整 C++ Project 的系統設定值 'v141' 改成 'v142', Windows SDK 由 '10.0.19041.0' 改成 '10.0'
23.
	23.1. 增加從外部讀取自定義的 DataRef, Command 參數值 ::: SelfParamsDefinition.json  SelfParamsDefinition.cs
	23.2. 增加 Template Code For 新人參考使用 ::: AirForceSystem, ElectricalSystem
	23.3. ReFactor code, 將 Event Interface 拉至 abstract class
	23.4. 與 Air Manager 連接的 callback function 從 AirManagerSystem.cs 改至 各個 dataref, command object 的 callback function.. 讓每一個 object 自行處理 callback ::: DataRefElementBase.cs, XPlaneCommand.cs
	23.5. 將 Event Interface Handler 用 partial class 方式分拆至各個 System 獨自管理
	23.6. 將 DataRefEventArgs 中的 WriteValue, ReadByteValue, ReadStringValue 都改至 DataRefElementBase, DataRefElement, StringDataRefElement class 裡
24.
	24.01. Add 3 個 Projects for gRPC ::: GrpcProto, MBIServer, MBIClient
	24.02. 原本預計整併 .NET5 的 gRPC, 但既有的 .NET Framework 4.6.1 比 .NET5 舊, 舊平台無法 reference 新平台,
		24.02.1. 但如果將舊版的 .NET Framework 4.6.1 改平台至 .NET5, 變動太大, 連 C++ Lib 也必須重新調整, 所以不改舊版的平台
		24.02.2. 改將 gRPC 執行平台改寫成 .NET Framework 的執行環境, 而 .NET Framework 版本中, 最新的為 4.8, 相對於 4.6.1 執行效能好一些
		24.02.3. 最終結論為 : 舊版 .NET Framework 4.6.1 改版成 4.8, 同時, gRPC service 改寫成 .NET Framework 4.8 可執行的方式
	24.03. MBILibrary, MBISimBridge projects 增加參考的 class, enum, function FOR gRPC 功能使用
	24.04. 增加 icon FOR MBIServer, MBISimBridge Project, 方便執行時, 可以辨識 (icon 暫定, 可再設計調整)
	24.05. MBIServer, MBISimBridge projects 增加 Post Build Event, 在 project build 成功後, 會將全部檔案 copy/paste 至 Solution 根目錄下的 /x64/, /x86/ 資料夾下, 方便日後存取
	24.06. MBIServer, MBISimBridge projects 增加 "調整 Console 視窗位置" 功能, 當初始執行時, 會自動縮下去工具列, 避免 user 不當關閉
	24.07. 在 MBISimBridge project 增加 GrpcClientService 監聽 Air Manager、X-Plane 互動的所有訊息, 將訊息透過 gRPC Client 即時傳送給 gRPC Server
	24.08. 在 Solution 根目錄下, 增加 generate_protos.bat 批次檔, 與 gRPC 的操作說明檔(gRPC_Protos_ReadMe.txt)
	24.09. GrpcProto Project 是管控 gRPC 的定義檔與自動產生的 messages, services, 可供 MBIClient, MBIServer 參考使用
	24.10. MBIClient Project 是 gRPC Client 端的執行角色, 會接收由 MBISimBridge 傳送過來的資訊, 再同步傳遞給 MBIServer
	24.11. MBIServer Project 是 gRPC Server 端的執行角色, 會接收由 MBIClient 傳送過來的資訊, 日後可以在此 server 端, 架設 Razor Page, 進行指令的下達, 將指令透過 gRPC 下達給 Air Manager 或是 X-Plane
25. 將 Joe 的 code 同步下來
26. align Code 2021/12/06
	26.1. add app singleton check. Debug/Release mode, MBIServer/MBISimBridge
	26.2. refactor code
	26.3. 移除 _ElectricalSystem Sample Code, 避免混淆
27. align Code 2021/12/13
	27.1. gRPC client / server 兩個 daemon 程式的非同步穩定度強化
		27.1.1. 當一端斷線後，另一端會暫時 queue 住訊息，等網路連線後，再將 queue 住的訊息送出。 斷線原因可能是，網路瞬斷，也可能是 user 不小心將 daemon 關閉
		27.1.2. 不管 client, server 何者先啟動，皆會等待另一端啟動後，馬上進行連線
	27.2. 增加防呆機制，同一 daemon 程式禁止同時開啟 (Singleton 機制)
	27.3. 增加 daemon 程式的身分認證碼 uuid，MBIServer、MBISimBridge 在該台電腦啟動第一次，就會註冊一組 uuid 進行日後的身分識別，以後可以整合 "註冊流程" 驗證
		27.3.1. 此 uuid 也是用來讓 "教師系統" 可以指定發送命令的對象 uuid 值
	27.4. 將 build 後的所有程式，放在同一 folder 下，方便存取, 原本 Project 下的 /bin/, /obj/ 會清除, (Release Mode 環境下)
	27.5. 統整 X-Plane DataRef 的所有 Type, 提供日後用來實作 listening array value 使用 (目前只能聽取單一值)
	27.6. 當 user 開啟 daemon 後，會馬上縮至工作列下，避免 user 點選到 CMD 介面而讓程式 lock 住，也避免 user 關閉程式 (Release Mode 環境下)
	27.7. 整理 DataRef 的常數定義表
	27.8. code refactor
	27.9. 將 user 可定義的系統參數，拉出來做成 config file (AppConfig.json), 目前提供 10 項設定值 ::: gRPCServer Name, IP, Port、 X-Plane IP, Port、 Air Manager Tag, IP, UDP Port... 其餘的系統參數值
28. align Joe 的 code
29. gRPC 多機通訊
	29.01. MBIServer 透過 MBISimBridge 控制 Air Manager, X-Plane 的程式架構完成
			可直接從 MBIServer 指定複寫 Air Manager, X-Plane 的 Subscribed DataRef Value
			分類: (暫時定義)
					1 開頭的控制 Air Manager
					2 開頭的控制 X-Plane
			目的:
				之後 "教師系統" 可透過 MBIServer 直接控制某一 Client 端的 Air Manager, X-Plane
	29.02. MBIServer 與 MBISimBridge 之間的多機通訊架構完成 (用 gRPC 實作)
			29.02.1. MBIServer, MBISimBridge 在每一台 Device 第一次啟動時, 會自動產生一組唯一的身分證號碼, 在多機通訊架構下, 藉此來區分彼此
					MBIServer 的指令可以廣播給所有線上的 MBISimBridge, 也可以指定線上某些指定的 MBISimBridge
					MBISimBridge 的指令可以回傳給 MBIServer, 日後可以透過 MBIServer 轉傳指令給線上的 MBISimBridge
			29.02.2. MBISimBridge 會隨時回報自己所控制的 Air Manager, X-Plane 的資訊給 MBIServer
					這個資訊可以提供給 "AI 系統" 紀錄學生的行為軌跡資訊, 方便日後進行分析
			29.02.3. MBISimBridge 當聽不到網路上的 MBIServer 時, 會暫時將資訊 cache 在 local 端, 但訊息有上限限制, 避免記憶體爆掉, 上限暫定 20 則訊息, 超出則 skip
			29.02.4. MBISimBridge 會隨時監聽 MBIServer 通訊是否正常, 一旦網路瞬斷, 或者是 MBIServer 被關閉, 或是未知因素而斷訊, 會自動切換成 Local 作業, 等 MBIServer 再度 Ready 好後, 馬上重新連線
			29.02.5. MBIServer 會隨時監聽線上所有的 MBISimBridge 通訊是否正常, 一旦網路瞬斷, 或者是 MBISimBridge 被關閉, 或是未知因素而斷訊, 會自動剔除 MBISimBridge, 等 MBISimBridge 再度 Ready 好後, 馬上重新連線
					這個資訊可以提供給 "教師系統", 即時顯示目前的學生連線狀態
	29.03. MBISimBridge 將與 Air Manager 通訊的 C++ Wrapper 介面獨立抽出做成一個 Hosting Service, 降低程式耦合度.. 
			日後與 X-Plane 通訊的介面也會抽出獨立一個 Hosting Service (待完成)
	29.04. NuGet 套件同步更新
	29.05. 定義 gRPC 通訊協議, 日後會隨著需求進行調整更新
	29.06. gRPC Client 端的連線監控獨立抽出, 降低程式耦合度
			29.06.1. 隨時將連線狀態顯示在 Console Title 上, 可以隨時得知與 MBIServer 的 gRPC OPEN/CLOSE
			29.06.2. 方便接下來增加憑證的通訊驗證
	29.07. 新增 Build Mode 在 Console Title 上, x86/x64, Debug/Release
	29.08. 原本規劃 Release Mode 只限定在同一台機器上, 同時只能開啟一個 app, MBIServer/MBISimBridge 擇一, 因為一開始定義 Client/Server 機器是獨立的
			現在取消此限制, 所以可以在同一台機器上, 同時開啟 MBIServer, MBISimBridge
	29.09. 輸入 "zzz" 則可以切斷通訊 (暫定)
	29.10. 導入 Microsoft 的 IHostBuilder 寫法, 獨立多個 Services, 彼此用 event 方式溝通, 之後會陸續從系統中, 增加/更新成 Service
			MBIServer 有 2 個 Services
				1. 監聽 MBISimBridge 連線狀態
				2. MBIServer 本體
			MBISimBridge 有 3 個 Services
				1. 與 MBIAmAPIWrapper C++ 通訊
				2. 監聽 MBIServer 連線狀態
				3. 與 MBIServer 多機通訊
	29.11. "初始化系統參數, InitParamsCtrller" 獨立抽出, 降低程式耦合度
			日後會繼續從系統中, 將可獨立運作的系統參數放置於此, 方便統一管控
			目前先將 SelfParamsDefinition.json 的 Subscribed DataRefs, Commands 拉到這裡實作
	29.12. 增加一些 Utility Function()
	29.13. gRPC catch Exception 的物件處理
	29.14. code re-factor
30. align Joe 的 code
31. release 效能問題
	31.1. upgrade nuget package
	31.2. 統整 DataRef, 將 自定義的 SelfParamsDefinition.json 與 系統定義的 DataRefs.cs 重複定義的取消, 並且增加 filter duplicated DataRef 機制
	31.3. 區分必要的 DataRef 在 Release Mode, 其餘測試用的 DataRef 則規範在 Debug Mode, 避免影響到系統效能表現
	31.4. MBIAmAPIBase 的 顯示調整一下
	31.5. 取消不必要的 log 紀錄
	31.6. 在 AppConfig.json 定義系統參數中, 增加 2 個控制 CpuUsage 的參數, 可決定 "效能優先", 還是 "硬體資源優先" ?
32. align Zac 的 code
	32.1. 同步底層的邏輯
	32.2. 修正自己發現的 bug
33. add array data i/o for int, float, double, int[], float[]
	33.1. 新增 array data i/o
	33.2. code refactor & debug
	33.3. Grpc Server DataRef i/o
	33.4. Air Manager DataRef i/o
	33.5. Joe's System DataRef i/o
	33.6. Grpc Client/Server
	
## 系統架構更新:
1. 將 mBlockHandler DLL Project 改成 MBISimBridge EXE Project, 程式目錄架構不變
2. XPlaneConnector.sln reName 成 MBISimBridge.sln
3. XPlaneConnectorExample WinForm Project 卸載備查
4. 系統架構圖: (C#)
	MBISimBridge(exe)
	|
	|_ XPlaneConnector(dll) 專門負責處理與 X-Plane 軟體的指令、資訊流 (下 Command 指令、監聽 DataRef Read/Write)
	|
	|_ XPlaneConnector.Instructions(dll) 專門負責更新同步 X-Plane 的最新 Command, DataRef
	|
	|_ Wrapper Air Manager C++ Lib(dll) 專門負責處理與 Air Manager 軟體的指令、資訊流 (下 Command 指令、監聽 DataRef Read/Write)
	|
	|_ /Controllers/ 負責企業邏輯處理
	|
	|_ /DataSwitch/ 各種 Services, Instances 之間的 Event Handler 介面
	|
	|_ /Enums/ 本系統的各種列舉定義
	|
	|_ /InstanceTA/ 各種 Instances, 例如: XPlaneSystem(專職與 X-Plane 互動), AirManagerSystem(專職與 Air Manager 互動)
	|
	|_ /Models/ 本系統的各種 Model 定義
	|
	|_ /Services/ 各種 Services, 例如: RESTClientService(專職與 REST APIs 互動), WSClientServcie(專職與 WebSocket Server 互動)
	|
	|_ /Util/ 本系統的各種 Extension Function(), Global Function(), Converter, Constants...


## Usage
NOTE: Every DataRef is always a float, even if the data type is different (int, bool, double, string, array).
So if you need a bool you will obtain a float that is either 0 or 1.

### Create the connector
The constructor takes the XPlane IP and port as parameters, default is 127.0.0.1 on port 49000

```C#
var connector = new XPlaneConnector(); // Default IP 127.0.0.1 Port 49000
var connector = new XPlaneConnector("192.168.0.100"); 
var connector = new XPlaneConnector("192.168.0.100", 49010); 
```

### Sending a command
Just pass the command.
A list of all the available commands has been created on XPlaneConnector.Commands
Each command has a Description property with a brief description of its meaning.

```C#
connector.SendCommand(XPlaneConnector.Commands.ElectricalBattery1On);
```

### Subscribe to a DataRef
You can subscribe to as many DataRef you want.
In either way you have to call:
```C#
connector.Subscribe(DataRefs.CockpitElectricalBatteryOn, 25, NotifyChangedToAirManager);
connector.Subscribe(DataRefs.CockpitRadiosNav1FreqHz, 25, NotifyChangedToAirManager);
connector.Subscribe(DataRefs.AircraftViewAcfTailnum, 25, NotifyChangedToAirManager);
connector.Start();

/// <summary>
/// 去通知 Air Manager, 此 X-Plane 的參數值 (string)
/// </summary>
/// <param name="sender"></param>
/// <param name="newValue"></param>
private void NotifyChangedToAirManager(StringDataRefElement sender, string newValue)
{}
```

In order to begin communication with X-Plane. Subscribing to DataRef can happen before or after calling Start.

A list of all managed DataRefs has been created inside:
```C#
XPlaneConnector.Instructions
```
Each DataRef has a Description property with a brief description of its meaning.

To obtain DataRef value use the DataRef event:
For DataRef "sim/cockpit/radios/com1_stdby_freq_hz" use XPlaneConnector.Instructions.CockpitRadiosCom1FreqHz

```C#
connector.Subscribe(XPlaneConnector.Instructions.CockpitRadiosCom1FreqHz, 5, (e, v) => {

    Console.WriteLine($"{DateTime.Now:HH:mm:ss.fff} - {e.DataRef} - {v}");
});
```

### Strings (NEW)
If you need a string (example: sim/aircraft/view/acf_tailnum) it is managed as an array of floats containing an ASCII code on each value.
Subscribing to sim/aircraft/view/acf_tailnum won't give you the tailnumber.
In order to get the complete string it's necessary to subscribe to each character individually.
Subscribing to sim/aircraft/view/acf_tailnum[0], sim/aircraft/view/acf_tailnum[1]... and so on (this DataRef is 40 byte long).
A new class StringDataRefElement has been created to automatically manage this process.
See below for usage.

```C#
// XPlaneConnector.Instructions.AircraftViewAcfTailnum is a StringDataRef, in this case value is a string, not a float
connector.Subscribe(XPlaneConnector.Instructions.AircraftViewAcfTailnum, 5, (element, value) =>
{

    Console.WriteLine($"{DateTime.Now:HH:mm:ss.fff} - {e.DataRef} - {v}"); // v is a string
});
```

NOTE: You must have already subscribed to a DataRef using the Subscribe method.

### Press & Hold Commands (NEW 2020)
For commands that have a Press&Hold behaviour like the Ignite command, multiple calls of the SendCommand() method is required.
To simplify this, there's a new method StartCommand that handle the required code in a parallel Task.
It returns a CancellationTokenSource, to stop the Command just call Cancel on this token.

```C#

var token = connector.StartCommand(XPlaneConnector.Commands.EnginesEngageStarters);
// Do other things 
// ...
// When you want it to stop
token.Cancel();

```
