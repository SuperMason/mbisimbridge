#include "pch.h"
#include "MStringPropertyProvider.h"

MStringPropertyProvider::MStringPropertyProvider(const char * id, const char* value) : MPropertyProviderBase(id, "STRING")
{
	if (value == nullptr) {
		value = "";
	}
	//cached_value = value;//20211021-1,Zac
	if (cached_value != nullptr)
	{ strcpy(cached_value, value); }
}

MStringPropertyProvider::~MStringPropertyProvider()
{
}

void MStringPropertyProvider::set(const char* value) {
	if (value == nullptr) {
		return;
	}
	enterCriticalSection();
	if (cached_value != value) {
		//cached_value = value;//20211021-1,Zac
		strcpy(cached_value, value);
		modCount++;
	}
	leaveCriticalSection();
}

const char* MStringPropertyProvider::get() {
	return cached_value;
}
int string_size(const char * str)
{
	size_t Size = strlen(str);
	return (int)Size;
}
struct SiDataPacket* MStringPropertyProvider::read() {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Create a new DataPacket, which we will return to the caller
	//const char data[] = { cached_value };
	struct SiDataPacket* packet = si_data_packet_create_with_type(SI_DATA_PACKET_TYPE_STRING, 1);
	//si_data_packet_set_float(packet, data, 1, 0);
	si_data_packet_set_string(packet, cached_value);
	leaveCriticalSection();

	// Note that the return packet is free'd by the caller after this function call
	return packet;
}

void MStringPropertyProvider::write(struct SiDataPacket* packet, const char* flags) {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Do something with new data
	//if ((si_data_packet_size(packet) == 1) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_STRING)) {//原版，但會因SIZE問題無法觸發//20211021-1,Zac
	if ((si_data_packet_size(packet) >= 1) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_STRING)) {
		//printf("string cached_value : %s   \n", cached_value);
		//printf("string si_data_packet_get_float : %s   \n", si_data_packet_get_float(packet));
		//const char* test = cached_value;
		//const char* test1 = si_data_packet_get_string(packet);
		//printf("*******", test, "****");
		//printf("*******", test1, "****");
		if(strcmp(cached_value, si_data_packet_get_string(packet)) != 0){//20211021-1,Zac
			strcpy(cached_value, si_data_packet_get_string(packet));
			//cached_value = si_data_packet_get_string(packet);
			_callbackFunc(cached_value);
		}
	}

	leaveCriticalSection();

	// Caller will free packet argument after this function call.
	// Make sure to not store a reference to packet here!
}
