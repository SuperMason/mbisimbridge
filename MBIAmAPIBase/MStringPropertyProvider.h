#ifndef MStringPropertyProviderH
#define MStringPropertyProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "MPropertyProviderBase.h"
#include "IVariableProvider.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall *strCallback_function)(const char*);//20211021-1,Zac
class MStringPropertyProvider : MPropertyProviderBase, IVariableProvider
{
private:
	char* cached_value= (char *)malloc((255 + 1) * sizeof(char));

public:
	MStringPropertyProvider(const char* id, const char* value);
	~MStringPropertyProvider();

	void set(const char* value);
	const char* get();

	strCallback_function _callbackFunc;//20211021-1,Zac
	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};


#endif