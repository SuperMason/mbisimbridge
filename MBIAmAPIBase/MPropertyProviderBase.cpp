#include "pch.h"
#include "MPropertyProviderBase.h"

#include "si_log.h"
#include "si_string.h"

	MPropertyProviderBase::MPropertyProviderBase(const char * id, const char * base_type)
	{
		this->id = si_string_format("%s%s", id, base_type);

		this->mutex = si_mutex_create();

		modCount = 0;
	}

	MPropertyProviderBase::~MPropertyProviderBase()
	{
	}

	bool MPropertyProviderBase::isBound() {
		return true;
	}

	void MPropertyProviderBase::close() {
		si_string_free(id);
		si_mutex_close(mutex);
	}

	const char* MPropertyProviderBase::getId() {
		// No immediate need to enter critical section here, since the id should (must!) never change
		return id;
	}

	int MPropertyProviderBase::getModCount() {
		// No immediate need to enter critical section here, since we are only returning an integer
		return modCount;
	}

	void MPropertyProviderBase::forceUpdate() {
		// Increment modCount, so an update is forced automatically
		modCount++;
	}

	void MPropertyProviderBase::setForceWrite(bool) {
		// We don't offer this feature
	}

	void MPropertyProviderBase::enterCriticalSection() {
		si_mutex_enter(mutex);
	}

	void MPropertyProviderBase::leaveCriticalSection() {
		si_mutex_leave(mutex);
	}