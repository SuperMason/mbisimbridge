#ifndef MBytePropertyProviderH
#define MBytePropertyProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "MPropertyProviderBase.h"
#include "IVariableProvider.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall* byteCallback_function)(BYTE);//20211021-1,Zac
class MBytePropertyProvider : MPropertyProviderBase, IVariableProvider
{
private:
	BYTE cached_value;

public:
	MBytePropertyProvider(const char* id, BYTE value);
	~MBytePropertyProvider();

	void set(BYTE value);
	int get();

	byteCallback_function _callbackFunc;//20211021-1,Zac
	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};


#endif