#include "pch.h"
#include "MFloatArrayPropertyProvider.h"

MFloatArrayPropertyProvider::MFloatArrayPropertyProvider(const char* id, float* defaultValue, const char* typeTag, int size) : MPropertyProviderBase(id, typeTag) {
	cached_value = (float*)malloc(size * sizeof(float)); // dynamically allocate memory of size
	if (cached_value == NULL) { fprintf(stderr, "float array Out of memory"); }

	for (int i = 0; i < size; i++)// assign values to the allocated memory
	{ *(cached_value + i) = *(defaultValue + i); }
	array_size = size;
}
MFloatArrayPropertyProvider::~MFloatArrayPropertyProvider()
{
	delete[] cached_value;
}

void MFloatArrayPropertyProvider::set(float* value) {
	if (value == nullptr) {
		return;
	}
	enterCriticalSection();
	bool equal = true;
	for (int i = 0; i < array_size; i++) {
		if (value[i] != cached_value[i]) {
			equal = false;
			break;
		}
	}
	if (!equal) {
		for (int i = 0; i < array_size; i++) {
			cached_value[i] = value[i];
		}
		modCount++;
	}
	leaveCriticalSection();
}

float* MFloatArrayPropertyProvider::get() {
	return cached_value;
}

struct SiDataPacket* MFloatArrayPropertyProvider::read() {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Create a new DataPacket, which we will return to the caller
	float* data = cached_value;
	struct SiDataPacket* packet = si_data_packet_create_with_type(SI_DATA_PACKET_TYPE_FLOAT, array_size);
	si_data_packet_set_float(packet, data, array_size, 0);
	leaveCriticalSection();

	// Note that the return packet is free'd by the caller after this function call
	return packet;
}

void MFloatArrayPropertyProvider::write(struct SiDataPacket* packet, const char* flags) {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	if ((si_data_packet_size(packet) == array_size) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_FLOAT)) {
		float* temp = si_data_packet_get_float(packet);
		bool equal = true;
		for (int i = 0; i < array_size; i++) {
			if (temp[i] != cached_value[i]) {
				equal = false;
				break;
			}
		}
		if (!equal) {
			for (int i = 0; i < array_size; i++) {
				cached_value[i] = temp[i];
			}
			_callbackFunc(cached_value, array_size);
			//printf("value changed");
			//printf("temp packet : %d %d %d %d   \n", cached_value[0], cached_value[1], cached_value[2], cached_value[3]);
		}
	}
	leaveCriticalSection();

	// Caller will free packet argument after this function call.
	// Make sure to not store a reference to packet here!
}