#include "pch.h"
#include "MIntegerPropertyProvider.h"

MIntegerPropertyProvider::MIntegerPropertyProvider(const char * id, int value) : MPropertyProviderBase(id, "INT")
{
	cached_value = value;
}

MIntegerPropertyProvider::~MIntegerPropertyProvider()
{
}

void MIntegerPropertyProvider::set(int value) {
	enterCriticalSection();
	if (cached_value != value) {
		cached_value = value;
		modCount++;
	}
	leaveCriticalSection();
}

int MIntegerPropertyProvider::get() {
	return cached_value;
}

struct SiDataPacket* MIntegerPropertyProvider::read() {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Create a new DataPacket, which we will return to the caller
	int data[] = { cached_value };
	struct SiDataPacket* packet = si_data_packet_create_with_type(SI_DATA_PACKET_TYPE_INT, 1);
	si_data_packet_set_int(packet, data, 1, 0);

	leaveCriticalSection();

	// Note that the return packet is free'd by the caller after this function call
	return packet;
}

void MIntegerPropertyProvider::write(struct SiDataPacket* packet, const char* flags) {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Do something with new data
	//printf("si_data_packet_type : %d   \n", si_data_packet_type(packet));
	if ((si_data_packet_size(packet) == 1) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_INT)) {
		if (cached_value != si_data_packet_get_int(packet)[0]) {//20211021-1,Zac
			cached_value = si_data_packet_get_int(packet)[0];
			_callbackFunc(cached_value);
		}
	}

	leaveCriticalSection();

	// Caller will free packet argument after this function call.
	// Make sure to not store a reference to packet here!
}