#include "pch.h"
#include "MIntegerArrayPropertyProvider.h"

MIntegerArrayPropertyProvider::MIntegerArrayPropertyProvider(const char* id, int* defaultValue, const char* typeTag, int size) : MPropertyProviderBase(id, typeTag) {
	cached_value = (int*)malloc(size * sizeof(int)); // dynamically allocate memory of size
	if (cached_value == NULL) { fprintf(stderr, "int array Out of memory"); }

	for (int i = 0; i < size; i++)// assign values to the allocated memory
	{ *(cached_value + i) = *(defaultValue + i); }
	array_size = size;
}
MIntegerArrayPropertyProvider::~MIntegerArrayPropertyProvider()
{
	delete[] cached_value;
}

void MIntegerArrayPropertyProvider::set(int* value) {
	if (value == nullptr) {
		return;
	}
	enterCriticalSection();
	bool equal = true;
	for (int i = 0; i < array_size; i++) {
		if (value[i] != cached_value[i]) {
			equal = false;
			break;
		}
	}
	if (!equal) {
		for (int i = 0; i < array_size; i++) {
			cached_value[i] = value[i];
		}
		modCount++;
	}
	leaveCriticalSection();
}

int* MIntegerArrayPropertyProvider::get() {
	return cached_value;
}

struct SiDataPacket* MIntegerArrayPropertyProvider::read() {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Create a new DataPacket, which we will return to the caller
	int* data = cached_value;
	struct SiDataPacket* packet = si_data_packet_create_with_type(SI_DATA_PACKET_TYPE_INT, array_size);
	si_data_packet_set_int(packet, data, array_size, 0);
	leaveCriticalSection();

	// Note that the return packet is free'd by the caller after this function call
	return packet;
}

void MIntegerArrayPropertyProvider::write(struct SiDataPacket* packet, const char* flags) {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	if ((si_data_packet_size(packet) == array_size) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_INT)) {
		int* temp = si_data_packet_get_int(packet);
		bool equal = true;
		for (int i = 0; i < array_size; i++) {
			if (temp[i] != cached_value[i]) {
				equal = false;
				break;
			}
		}
		if (!equal) {
			for (int i = 0; i < array_size; i++) {
				cached_value[i] = temp[i];
			}
			_callbackFunc(cached_value,array_size);
			//printf("value changed");
			//printf("temp packet : %d %d %d %d   \n", cached_value[0], cached_value[1], cached_value[2], cached_value[3]);
		}
	}
	leaveCriticalSection();

	// Caller will free packet argument after this function call.
	// Make sure to not store a reference to packet here!
}