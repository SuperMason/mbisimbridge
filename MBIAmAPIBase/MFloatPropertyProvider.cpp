#include "pch.h"
#include "MFloatPropertyProvider.h"

MFloatPropertyProvider::MFloatPropertyProvider(const char * id, float value) : MPropertyProviderBase(id, "FLOAT")
{
	cached_value = value;
}

MFloatPropertyProvider::~MFloatPropertyProvider()
{
}

void MFloatPropertyProvider::set(float value) {
	enterCriticalSection();
	if (cached_value != value) {
		cached_value = value;
		modCount++;
	}
	leaveCriticalSection();
}

float MFloatPropertyProvider::get() {
	return cached_value;
}

struct SiDataPacket* MFloatPropertyProvider::read() {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Create a new DataPacket, which we will return to the caller
	float data[] = { cached_value };
	struct SiDataPacket* packet = si_data_packet_create_with_type(SI_DATA_PACKET_TYPE_FLOAT, 1);
	si_data_packet_set_float(packet, data, 1, 0);

	leaveCriticalSection();

	// Note that the return packet is free'd by the caller after this function call
	return packet;
}

void MFloatPropertyProvider::write(struct SiDataPacket* packet, const char* flags) {
	// Make sure to use a critical section, since this function can be executed from mulitple threads!
	enterCriticalSection();

	// Do something with new data
	if ((si_data_packet_size(packet) == 1) && (si_data_packet_type(packet) == SI_DATA_PACKET_TYPE_FLOAT)) {
		if (cached_value != si_data_packet_get_float(packet)[0]) {//20211021-1,Zac
			cached_value = si_data_packet_get_float(packet)[0];
			_callbackFunc(cached_value);
		}
	}

	leaveCriticalSection();

	// Caller will free packet argument after this function call.
	// Make sure to not store a reference to packet here!
}
