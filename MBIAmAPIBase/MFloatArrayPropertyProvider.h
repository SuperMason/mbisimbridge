#ifndef MFloatArrayPropertyProviderH
#define MFloatArrayPropertyProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "MPropertyProviderBase.h"
#include "IVariableProvider.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall* floatPointCallback_function)(float*, int);//20211021-1,Zac
class MFloatArrayPropertyProvider : MPropertyProviderBase, IVariableProvider//20220125-3,Zac
{
private:
	//int* cached_value;
	float* cached_value = (float*)malloc(1 * sizeof(float));
	int array_size = 1;

public:
	//MIntegerArrayPropertyProvider(const char* id, int value);
	MFloatArrayPropertyProvider(const char* id, float* defaultValue, const char* typeTag, int size);
	~MFloatArrayPropertyProvider();

	void set(float* value);
	float* get();

	floatPointCallback_function _callbackFunc;
	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};


#endif