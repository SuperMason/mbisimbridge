#ifndef MSimpleCommandProviderH
#define MSimpleCommandProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "ICommandProvider.h"

#include "si_mutex.h"
using namespace std;
namespace MBIAmAPI {
	typedef void(__stdcall *callback_function)(int, const char*);
	class MSimpleCommandProvider : ICommandProvider
	{
	private:

		struct SiMutex* mutex;
		void(*callback)(MSimpleCommandProvider* provider, CommandType type, SI_BOOL has_value, int value);
	public:

		const char* id;

		MSimpleCommandProvider(const char* id, void(*callback)(MSimpleCommandProvider* provider, CommandType type, SI_BOOL has_value, int value));
		~MSimpleCommandProvider();
		const char* getId();
		callback_function _callbackFunc;

		void fire(CommandType type);

		void fire(CommandType type, int value);

		int getModCountStart();
		int getModCountOnce();
		int getModCountEnd();

		bool isBound();

		void close();

	};
}
#endif