
#ifndef MIntegerArrayPropertyProviderH
#define MIntegerArrayPropertyProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "MPropertyProviderBase.h"
#include "IVariableProvider.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall* intPointCallback_function)(int*,int);//20211021-1,Zac
class MIntegerArrayPropertyProvider : MPropertyProviderBase, IVariableProvider
{
private:
	//int* cached_value;
	int* cached_value = (int*)malloc(1 * sizeof(int));
	int array_size = 1;

public:
	//MIntegerArrayPropertyProvider(const char* id, int value);
	MIntegerArrayPropertyProvider(const char* id, int* defaultValue, const char* typeTag, int size);
	~MIntegerArrayPropertyProvider();

	void set(int* value);
	int* get();

	intPointCallback_function _callbackFunc;//20211021-1,Zac
	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};


#endif