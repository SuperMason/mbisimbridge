#pragma once

#include <stdint.h>

#include "sim_discover_shared.h"

#ifdef __cplusplus
extern "C" {
#endif
	void sim_discover_server_init(uint32_t device_type);
	void sim_discover_server_tick();
	void sim_discover_server_set_prop_int(const char* key, int value);
	void sim_discover_server_set_prop_string(const char* key, const char* value);

	void sim_discover_server_close(void);

#ifdef __cplusplus
}
#endif