#pragma once

#include "si_base.h"
#include "si_event.h"
#include "si_memory_pool.h"

#define SI_LIST_EVENT_PROP_ITEM_ADDED        0
#define SI_LIST_EVENT_PROP_ITEM_REMOVE_PRE   1
#define SI_LIST_EVENT_PROP_ITEM_REMOVE_POST  2

#define SI_LIST_ITERATOR_FOR_VARIABLE(__variable__) iterator_##__variable__

#define SI_LIST_FOREACH(__type__, __variable__, __list__)           \
struct SiListIterator SI_LIST_ITERATOR_FOR_VARIABLE(__variable__);                \
for(__type__ __variable__ = (__type__) si_list_iter_start(__list__, & iterator_##__variable__, 0); __variable__ != NULL; __variable__ = (__type__) si_list_iter_next(& iterator_##__variable__) )

#define SI_LIST_FOREACH_BACKWARDS(__type__, __variable__, __list__)            \
struct SiListIterator SI_LIST_ITERATOR_FOR_VARIABLE(__variable__);                \
for(__type__ __variable__ = (__type__) si_list_iter_end(__list__, & iterator_##__variable__, 0); __variable__ != NULL; __variable__ = (__type__) si_list_iter_prev(& iterator_##__variable__) )

struct SiListNode;
struct SiList;

struct SiListEventPayload {
	struct SiList* list;

	int index;

	int id;

	void* data;
};

struct SiListIterator {
	struct SiListNode* node;
	struct SiList* list;

    uint32_t mod_count;
};

#ifdef __cplusplus
extern "C" {
#endif
    
struct SiList* si_list_create(void);

int si_list_len(struct SiList* list);
void* si_list_get(struct SiList* list, int index);
void* si_list_get_from_id(struct SiList* list, int id);
void* si_list_first(struct SiList* list);
void* si_list_last(struct SiList* list);
int si_list_index_of(struct SiList* list, void* data);
int si_list_id_of(struct SiList* list, void* data);
void* si_list_remove(struct SiList* list, int index, SI_BOOL free_data);

// Iterator functions

void* si_list_iter_start(struct SiList* list, struct SiListIterator* iterator, int offset);
void* si_list_iter_end(struct SiList* list, struct SiListIterator* iterator, int offset);
void  si_list_iter_remove_current(struct SiListIterator* iterator, SI_BOOL free_data);
int   si_list_iter_cur_index(struct SiListIterator* iterator);
int   si_list_iter_cur_id(struct SiListIterator* iterator);
void* si_list_iter_next(struct SiListIterator* iterator);
void* si_list_iter_prev(struct SiListIterator* iterator);

SI_BOOL si_list_contains(struct SiList* list, void* data);

// Returns id
int si_list_append(struct SiList* list, void* data);

void si_list_inject(struct SiList* list, void* data, int index);

void* si_list_remove_data(struct SiList* list, void* data, SI_BOOL free_data);

void* si_list_to_array(struct SiList* list);
void* si_list_to_array_data(struct SiList* list, int data_size);

struct SiEvent* si_list_event_handler(struct SiList* list);

void si_list_clear(struct SiList* list, SI_BOOL free_data);

void si_list_close(struct SiList* list, SI_BOOL free_data);

#ifdef __cplusplus
}
#endif
