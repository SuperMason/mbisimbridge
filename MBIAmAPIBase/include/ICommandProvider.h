#pragma once

enum CommandType {
	COMMAND_START,
	COMMAND_ONCE,
	COMMAND_END
};

class ICommandProvider
{
public:
	// Get the ID of the Command (unique string)
	virtual const char* getId() = 0;
	
	// Fire the command
	virtual void fire(CommandType type) = 0;

	// Fire the command, with value
	virtual void fire(CommandType type, int value) = 0;

	// Check if the command had been succesfully bound to backend system (like X-plane)
	virtual bool isBound() = 0;

    // Mod count
    virtual int getModCountStart() = 0;
    virtual int getModCountOnce() = 0;
    virtual int getModCountEnd() = 0;

	// Close and stop the command handler
	virtual void close() = 0;
};
