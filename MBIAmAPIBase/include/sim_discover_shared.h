#pragma once

#define SIM_DISCOVER_RECEIVE_BUFFER_SIZE      2048
#define SIM_DISCOVER_PROPERTIES_MAX            256

#define SIM_DISCOVER_PORT                    60000

#define SIM_DISCOVER_OBJECT_FLIGHT_SIM_XPLANE  1000
#define SIM_DISCOVER_OBJECT_FLIGHT_SIM_FSX     1100
#define SIM_DISCOVER_OBJECT_FLIGHT_SIM_P3D     1200
#define SIM_DISCOVER_OBJECT_FLIGHT_SIM_FS2     1300
#define SIM_DISCOVER_OBJECT_EXTERNAL_SOURCE    1900

#define SIM_DISCOVER_OBJECT_AIR_PLAYER_WIN	   3000
#define SIM_DISCOVER_OBJECT_AIR_PLAYER_MAC	   3100
#define SIM_DISCOVER_OBJECT_AIR_PLAYER_LIN	   3200
#define SIM_DISCOVER_OBJECT_AIR_PLAYER_RPI     3300
#define SIM_DISCOVER_OBJECT_AIR_PLAYER_ANDROID 3400

#define SIM_DISCOVER_OBJECT_CAD_V1             4000
#define SIM_DISCOVER_OBJECT_VEMD_V1            4010

#define SIM_DISCOVER_OBJECT_AM_LOCAL_WIN	  20000
#define SIM_DISCOVER_OBJECT_AM_LOCAL_MAC	  20100
#define SIM_DISCOVER_OBJECT_AM_LOCAL_LINUX    20200

#define SIM_DISCOVER_PROP_NAME_FLIGHT_SIM_PORT     "sim_bus_flight_sim_port"
#define SIM_DISCOVER_PROP_NAME_AM_PORT             "sim_bus_am_port"
#define SIM_DISCOVER_PROP_NAME_SIM_BUS_PORT        "sim_bus_port"
#define SIM_DISCOVER_PROP_NAME_SIM_REMOTE_PORT     "sim_remote_port"
#define SIM_DISCOVER_PROP_NAME_EXTERNAL_SOURCE_TAG "external_source_tag"
#define SIM_DISCOVER_PROP_NAME_VERSION_MAJOR       "version_major"
#define SIM_DISCOVER_PROP_NAME_VERSION_MINOR       "version_minor"
#define SIM_DISCOVER_PROP_NAME_VERSION_BUILD       "version_build"
#define SIM_DISCOVER_PROP_NAME_BETA                "beta"
#define SIM_DISCOVER_PROP_NAME_PLATFORM            "platform" 

#define SIM_DISCOVER_MODE_REQUEST                0
#define SIM_DISCOVER_MODE_RESPONSE               1

#define SIM_DISCOVER_PROP_TYPE_STRING            0
#define SIM_DISCOVER_PROP_TYPE_INT               1

struct SimDiscoverProperty {
	const char* key;

	int type;

	union {
		const char* value_string;
		int value_int;
	};
};

struct SimDiscoverObject {

	const char* ip_address_v4;

	const char* hw_id;

	int object_type;

	struct SiList* properties;
};
