#pragma once

#include "si_mutex.h"
#include "si_list.h"

#ifdef __cplusplus
extern "C" {
#endif

struct SiEventCallback {
	void(*callback)(void* caller_tag, void* listener_tag, int prop_id);
	void* listener_tag;
};

struct SiEvent {
	struct SiList* callback_list;
	struct SiList* queue;
};

struct SiEvent* si_event_create(void);

void si_event_raise(struct SiEvent* event, void* caller_tag, int prop_id);

void si_event_enqueue(struct SiEvent* event, void* caller_tag, unsigned int caller_tag_size, int prop_id);
void si_event_flush(struct SiEvent* event);

struct SiEventCallback* si_event_subscribe(struct SiEvent* event, void* listener_tag, void(*callback)(void* caller_tag, void* listener_tag, int prop_id));
void si_event_unsubscribe(struct SiEvent* event, void* listener_tag);
void si_event_unsubscribe_callback(struct SiEvent* event, struct SiEventCallback* callback);
void si_event_unsubscribe_function(struct SiEvent* event, void(*callback)(void* caller_tag, void* listener_tag, int prop_id));

void si_event_close(struct SiEvent* event);

#ifdef __cplusplus
}
#endif