#ifndef SI_STRING_H
#define SI_STRING_H

#include <stdio.h>
#include <string.h>

#ifndef SI_EMBEDDED
#include <wchar.h>
#endif

#include "si_base.h"

struct StringSplitResult {
    int count;
    const char** data;
};

#ifdef __cplusplus
extern "C" {
#endif

// Free after use
const char* si_string_create(const char* str, int len);

struct StringSplitResult* si_string_split(const char* a_str, const char a_delim);
void si_string_split_result_close(struct StringSplitResult* result);

char* si_string_trim(char *str);
SI_BOOL si_string_starts_with(const char *str, const char *pre);
SI_BOOL si_string_ends_with(const char *str, const char *suffix);
SI_BOOL si_string_contains(const char *str, const char* part);

// Free after use
const char* si_string_to_lower(const char *str);

// Free after use
const char* si_string_to_upper(const char *str);

void si_string_make_lower(char *str);
void si_string_make_upper(char *str);

const char* si_string_clone(const char* str);

// Free after use
const char* si_string_pad_to_mod(const char* str, int mod, char pad_char, size_t* result_len);

SI_BOOL si_string_equal(const char* str1, const char* str2);
SI_BOOL si_string_equal_ext(const char* str1, const char* str2, size_t max_len, SI_BOOL case_insensitive);
SI_BOOL si_string_is_empty(const char* str);

const char* si_string_format(const char *fmt, ...);
const char* si_string_format_concat(const char* str, const char* fmt, ...);

int si_starts_with(const char* string, const char* prefix);

// Free after use
char* si_string_replace(const char *orig, const char *rep, const char *with);
    
char* si_string_last(const char* string, const char* find);

// Free after use
const char* si_string_substring(const char* str, int offset, int count);

// Free after use
const char* si_string_hex_from_byte_array(uint8_t* data, int len, SI_BOOL seperator);

// Free after use
uint8_t* si_string_hex_to_byte_array(const char* hex_string);

void si_string_free(const char* str);
void si_string_free_wide(const wchar_t* str);

#ifndef SI_EMBEDDED
const wchar_t* si_string_convert_utf8_to_wchar(const char* utf8_string);
const char* si_string_convert_wchar_to_utf8(const wchar_t* wide_string);
#endif

#ifdef __cplusplus
}
#endif

#endif