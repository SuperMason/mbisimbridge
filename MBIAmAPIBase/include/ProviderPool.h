#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "IVariableProvider.h"
#include "ICommandProvider.h"

#include "si_mutex.h"

#define PROVIDER_POOL_MAX_VARIABLES 2048
#define PROVIDER_POOL_MAX_COMMAND 2048

class ProviderPool {
public:
	ProviderPool(IVariableProvider* (*dataProviderGenerator)(const char* id, const char* unit, SiDataPacketType type, int size), ICommandProvider* (*commandProviderGenerator)(const char* id), int(*availableVariableCount)(), char* (*availableVariableWithIndex)(int index), char* (*availableVariableTypeWithIndexCallback)(int index) );
    ProviderPool();

	IVariableProvider* fetchDataRefProvider(const char* id, const char* unit, SiDataPacketType type, int size);
	IVariableProvider* getDataRefProvider(const char* full_id, const char* id);
	IVariableProvider* getDataRefProvider(int index);

	ICommandProvider* fetchCommandProvider(const char* id);
	ICommandProvider* getCommandProvider(const char* id);
	ICommandProvider* getCommandProvider(int index);

    void registerVariableProvider(IVariableProvider* provider);
    void registerCommandProvider(ICommandProvider* provider);

    void enterCriticalSection();
    void leaveCriticalSection();

	int getVariableCount();
	int getAvailableVariableCount();
	char* getAvailableVariableID(int index);
	char* getAvailableVariableType(int index);

	int getCommandCount();

private:

    struct SiMutex* mutex;

	IVariableProvider* (*dataProviderGenerator)(const char* id, const char* unit, SiDataPacketType type, int size);
	ICommandProvider* (*commandProviderGenerator)(const char* id);

	int(*availableVariableCountCallback)();
	char* (*availableVariableWithIndexCallback)(int index);
	char* (*availableVariableTypeWithIndexCallback)(int index);

	IVariableProvider* variables[PROVIDER_POOL_MAX_VARIABLES];
	int variable_count;

	ICommandProvider* commandList[PROVIDER_POOL_MAX_COMMAND];
	int command_count;
};
