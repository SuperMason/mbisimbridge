#pragma once

#include <stdint.h>

#include "si_base.h"
#include "si_mutex.h"

struct SiMemoryPool {
	
	// Number of bytes for one item
	uint32_t data_size;
	
	// Maximum number of items in one data_block
	uint32_t data_block_capacity;
	
	// Number of memory blocks
	uint32_t nr_data_blocks;

	// Every bit marks if the data block is available
	// LSB bit is item 0, 8, 16 etc.
	uint8_t** data_available_blocks;

	// Memory blocks. This is where the actual data is stored
	uint8_t** data_blocks;

    // Make thread safe
    struct SiMutex* mutex;
};

struct SiMemoryPool* si_memory_pool_create(uint32_t data_size, uint32_t initial_capacity, SI_BOOL use_mutex);

void* si_memory_pool_claim(struct SiMemoryPool* memory_pool);
void si_memory_pool_release(struct SiMemoryPool* memory_pool, void* address);

void si_memory_pool_clear(struct SiMemoryPool* memory_pool);

void si_memory_pool_close(struct SiMemoryPool* memory_pool);
