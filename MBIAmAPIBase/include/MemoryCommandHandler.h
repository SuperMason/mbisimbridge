#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <atomic>

#include "ICommandProvider.h"

#include "si_mutex.h"
#include "si_list.h"

class MemoryCommandHandler : ICommandProvider {
private:
    std::atomic<int> mod_count_start;
    std::atomic<int> mod_count_once;
    std::atomic<int> mod_count_end;

public:
    // AM data ID
	const char* id;

	//Constructor
	MemoryCommandHandler(const char* element);

	// Get the ID of the Command (unique string)
	const char* getId();

	// Fire the command
	void fire(CommandType type);

	// Fire the command, with value
	void fire(CommandType type, int value);

	// Check if the command had been succesfully bound to backend system (like X-plane)
	bool isBound();

	// Close and stop the command handler
	void close();

	// Internal function, called from simulator thread
	void checkNewData();

    int getModCountStart();
    int getModCountOnce();
    int getModCountEnd();
};
