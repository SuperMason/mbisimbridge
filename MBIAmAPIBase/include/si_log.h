#ifndef SI_LOG_H
#define SI_LOG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "si_base.h"

enum SiLogLevel {
    SI_LOG_TRACE = 0,
    SI_LOG_DEBUG = 1,
    SI_LOG_INFO = 2,
    SI_LOG_WARN = 3,
    SI_LOG_ERROR = 4,
	SI_LOG_MAX = 5
};
struct SiLog {
    enum SiLogLevel level;
	const char* tag;
	const char* message;
};

extern const char* log_level_names[SI_LOG_MAX];

struct SiLog* si_log_message_create(enum SiLogLevel level, const char* tag, const char* message);
void si_log_message_close(struct SiLog* log);

void  si_log_config_write_to_static_file(const char* file_path);

void si_log(const char *tag, enum SiLogLevel level, const char *fmt, ...);

SI_BOOL si_log_has(enum SiLogLevel level);

void  si_log_trace(const char *tag, const char *fmt, ...);
void  si_log_debug(const char *tag, const char *fmt, ...);
void  si_log_info(const char *tag, const char *fmt, ...);
void  si_log_warning(const char *tag, const char *fmt, ...);
void  si_log_error(const char *tag, const char *fmt, ...);

void  si_log_set_callback(void(*callback)(int level, const char* tag, const char* message));

enum SiLogLevel si_log_get_log_level(void);
void  si_log_set_log_level(enum SiLogLevel level);

void si_log_close(void);

#ifdef __cplusplus
}
#endif

#endif