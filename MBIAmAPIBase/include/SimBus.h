#pragma once

#include <stdint.h>

#include "SimBusClient.h"
#include "IVariableProvider.h"
#include "ProviderPool.h"

#include "si_thread.h"
#include "si_socket.h"

#ifdef IBM
#include <process.h>
#include <windows.h>
#include <winsock2.h> 
#include <time.h>
#else
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <fcntl.h>
#include <errno.h>
#endif

class SimBus
{
private:
	bool enabled;

    struct SiThread thread;

public:
	SimBus(uint16_t port, ProviderPool* pool);

    bool running;

	ProviderPool* pool;

	void enable();
	void disable();

    struct SiSocket* socket;
    
    uint16_t port;

	// SimBusClient
	struct SiList* clients;
	
	void close_bus();
};
