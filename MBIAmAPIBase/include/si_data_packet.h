#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include "si_base.h"
#include "si_data_packet.h"

#ifdef __cplusplus
extern "C" {
#endif

	struct SiDataPacket;

	enum SiDataPacketType {
		SI_DATA_PACKET_TYPE_UNKNOWN = 0,
		SI_DATA_PACKET_TYPE_INT = 1,
		SI_DATA_PACKET_TYPE_FLOAT = 2,
		SI_DATA_PACKET_TYPE_DOUBLE = 3,
		SI_DATA_PACKET_TYPE_BYTE = 4,
		SI_DATA_PACKET_TYPE_BOOL = 5,
		SI_DATA_PACKET_TYPE_STRING = 6,
		SI_DATA_PACKET_TYPE_VECTOR_2D = 7,
		SI_DATA_PACKET_TYPE_VECTOR_3D = 8,
		SI_DATA_PACKET_TYPE_VECTOR_4D = 9,
		SI_DATA_PACKET_TYPE_EMPTY = 10,
		SI_DATA_PACKET_TYPE_MAX = 11
	};

    struct SiDataType {
        enum SiDataPacketType type;
        int size;
    };

	struct SiDataPacketVector2D {
		double val_0;
		double val_1;
	};

	struct SiDataPacketVector3D {
		double val_0;
		double val_1;
		double val_2;
	};

	struct SiDataPacketVector4D {
		double val_0;
		double val_1;
		double val_2;
		double val_3;
	};

	extern const char* si_data_packet_packet_type_names[SI_DATA_PACKET_TYPE_MAX];

	void si_data_packet_init(struct SiDataPacket* packet);

	struct SiDataPacket* si_data_packet_create(void);
	struct SiDataPacket* si_data_packet_create_with_type(enum SiDataPacketType type, int size);
	struct SiDataPacket* si_data_packet_create_with_string_type(const char* type_string, int size, int offset);

	struct SiDataPacket* si_data_packet_wrap_byte(uint8_t* data, int size);
	struct SiDataPacket* si_data_packet_wrap_integer(int* data, int size);
	struct SiDataPacket* si_data_packet_wrap_int32(int32_t* data, int size);
    struct SiDataPacket* si_data_packet_wrap_bool(SI_BOOL* data, int size);
	struct SiDataPacket* si_data_packet_wrap_float(float* data, int size);
	struct SiDataPacket* si_data_packet_wrap_string(const char* string);
	struct SiDataPacket* si_data_packet_wrap_vector2d(struct SiDataPacketVector2D* vector, int size);
	struct SiDataPacket* si_data_packet_wrap_vector3d(struct SiDataPacketVector3D* vector, int size);
	struct SiDataPacket* si_data_packet_wrap_vector4d(struct SiDataPacketVector4D* vector, int size);

	uint8_t* si_data_packet_get_byte(struct SiDataPacket* packet);
	SI_BOOL* si_data_packet_get_bool(struct SiDataPacket* packet);
	int* si_data_packet_get_int(struct SiDataPacket* packet);
	float* si_data_packet_get_float(struct SiDataPacket* packet);
	double* si_data_packet_get_double(struct SiDataPacket* packet);
	const char* si_data_packet_get_string(struct SiDataPacket* packet);
	struct SiDataPacketVector2D* si_data_packet_get_vector2d(struct SiDataPacket* packet);
	struct SiDataPacketVector3D* si_data_packet_get_vector3d(struct SiDataPacket* packet);
	struct SiDataPacketVector4D* si_data_packet_get_vector4d(struct SiDataPacket* packet);

	int si_data_packet_set(struct SiDataPacket* packet, struct SiDataPacket* import_packet, int size, int offset);
	int si_data_packet_set_byte(struct SiDataPacket* packet, uint8_t* data, int size, int offset);
	int si_data_packet_set_bool(struct SiDataPacket* packet, SI_BOOL* data, int size, int offset);
	int si_data_packet_set_int(struct SiDataPacket* packet, int* data, int size, int offset);
	int si_data_packet_set_int32(struct SiDataPacket* packet, int32_t* data, int size, int offset);
	int si_data_packet_set_float(struct SiDataPacket* packet, float* data, int size, int offset);
	int si_data_packet_set_double(struct SiDataPacket* packet, double* data, int size, int offset);
	int si_data_packet_set_string(struct SiDataPacket* packet, const char* string);
	int si_data_packet_set_vector2d(struct SiDataPacket* packet, struct SiDataPacketVector2D* vector, int size, int offset);
	int si_data_packet_set_vector3d(struct SiDataPacket* packet, struct SiDataPacketVector3D* vector, int size, int offset);
	int si_data_packet_set_vector4d(struct SiDataPacket* packet, struct SiDataPacketVector4D* vector, int size, int offset);

	int si_data_packet_size(struct SiDataPacket* packet);
	int si_data_packet_offset(struct SiDataPacket* packet);
	enum SiDataPacketType si_data_packet_type(struct SiDataPacket* packet);
	const char* si_data_packet_unit(struct SiDataPacket* packet);

	struct SiDataPacket* si_data_packet_clone(struct SiDataPacket* packet);

	void si_data_packet_default(struct SiDataPacket* packet);

	void si_data_packet_load(struct SiDataPacket* packet, const char* type, const char* unit, const char* value);
	void si_data_packet_load_unit(struct SiDataPacket* packet, const char* unit);
    void si_data_packet_load_size(struct SiDataPacket* packet, int size);
	
	void si_data_packet_load_string(struct SiDataPacket* packet, const char* string);

	uint8_t* si_data_packet_load_bytes(struct SiDataPacket* packet, uint8_t* pos);
	char* si_data_packet_save_bytes(struct SiDataPacket* packet, char* buffer, int id);
	size_t si_data_packet_byte_size(struct SiDataPacket* packet);

	void si_data_packet_import_type(struct SiDataPacket* packet, struct SiDataPacket* import_packet);
    void si_data_packet_import_size(struct SiDataPacket* packet, struct SiDataPacket* import_packet);
	void si_data_packet_import_value(struct SiDataPacket* packet, struct SiDataPacket* import_packet);
    void si_data_packet_import_offset(struct SiDataPacket* packet, struct SiDataPacket* import_packet);

	// This function returns the type as string ("INT[5]", "FLOAT", "FLOAT[10]", etc.)
	// Don't free after use
	const char* si_data_packet_get_type_string(struct SiDataPacket* packet);

	// Don't free after use
	const char* si_data_packet_get_value_string(struct SiDataPacket* packet);

	// This function returns the base type as string ("INT", "FLOAT", etc.)
	// Don't free after use
	const char* si_data_packet_type_base_string(struct SiDataPacket* packet);

	// Checks if packets are equal, both in type and value
	SI_BOOL si_data_packet_equals(struct SiDataPacket* packet1, struct SiDataPacket* packet2);

	// Checks if packet contains default value's
	SI_BOOL si_data_is_default(struct SiDataPacket* packet);

	void si_data_packet_close(struct SiDataPacket* packet);

#ifdef __cplusplus
}
#endif