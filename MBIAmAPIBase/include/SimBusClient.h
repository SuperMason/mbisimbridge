#pragma once

#include <stdio.h>
#include <stdarg.h>

#include "si_mutex.h"

#include "IVariableProvider.h"
#include "ICommandProvider.h"

#include "ProviderPool.h"

#ifdef IBM
#define _WINSOCKAPI_
#endif

#include "si_thread.h"
#include "si_socket.h"
#include "si_mutex.h"

#ifdef IBM
#include <process.h>
#include <windows.h>
#include <winsock2.h> 
#include <time.h>
#else
#include <pthread.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#endif

// Max length of a UDP packet
#define AIRBUS_CLIENT_MAX_MSG_LEN 1536

enum SimBusClientState {
	CONNECTED,
	DISCONNECTED,
	CLOSED
};

class SimBusClientDataHandler {
public:
	IVariableProvider* dataProvider;
	int mod_count;
	int id;

	bool dirty;
};

class SimBusClientCommandHandler {
public:
	ICommandProvider* commandProvider;

	int mod_count_start_v1;
	int mod_count_once_v1;
	int mod_count_end_v1;

    int mod_count_start_v2;
    int mod_count_once_v2;
    int mod_count_end_v2;

    int burst;
    int burst_message_id;

    char* AppendDataV1(char * buffer, u_short start_count, u_short once_count, u_short end_count);
    char* AppendDataV2(char * buffer, u_short start_count, u_short once_count, u_short end_count, uint32_t message_id);
    int getDataSize();

    unsigned int last_message_id;

	bool dirty;

    struct SiMutex* mutex;
};

class SimBusClient {
private:
	void handleMsg(const char* key, const char* data);

	// Fetch a certain Dataref provider (will create if not yet available, but can return NULL!)
	SimBusClientDataHandler* fetchDataHandler(const char* id, const char* unit, enum SiDataPacketType type, int size);

	// Get a certain Dataref provider (won't create if not available, will return NULL)
	SimBusClientDataHandler* getDataHandler(const char* id);

	// Fetch a certain Dataref provider (will create if not yet available, but can return NULL!)
	SimBusClientCommandHandler* fetchCommandHandler(const char* id);

	// Get a certain Dataref provider (won't create if not available, will return NULL)
	SimBusClientCommandHandler* getCommandHandler(const char* id);
	SimBusClientCommandHandler* getCommandHandler(ICommandProvider* provider);

    struct SiThread thread;

	// Callback functions from the Command providers

	// Fired on start command
	void newEvent(ICommandProvider* provider, CommandType type);

public:
	int running;
    
    bool enabled;

    struct SiMutex* mutex;
    
	time_t lastUpdateFromClient;

	time_t nextAlive;
    
	struct SiSocket* socket;
    
	// This is the position in the available variable list
	// This is needed since we cannot sent the complete variable list in one UDP packet
	// -1 is considered not active
	int availableVariablesPosition;

	struct SiSocketAddress* remote_address;
    
    // SimBusClientDataHandler
    struct SiList* variable_handlers;

    // SimBusClientCommandHandler
    struct SiList* command_handlers;

	SimBusClientState state;

	ProviderPool* pool;

    uint32_t message_id_current;

	SimBusClient(struct SiSocketAddress* address, struct SiSocket* socket, ProviderPool* _pool);
	~SimBusClient(void);

	void newData(char* data, int len);

	// Close the client
	void close();
    
	//Enable the client
    void enable();

	// Disable the client
    void disable();
};
