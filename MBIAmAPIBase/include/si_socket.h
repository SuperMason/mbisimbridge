#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#include "si_base.h"
#include "si_list.h"

#define SI_SOCKET_ERROR -1

struct SiSocket;
struct SiSocketAddress;

enum SiSocketType {
	SI_SOCKET_TYPE_TCP,
	SI_SOCKET_TYPE_UDP
};

enum SiSocketConnectResult {
    SI_SOCKET_CONNECT_RESULT_ERROR      = -1,
    SI_SOCKET_CONNECT_RESULT_WAIT       = 0,
    SI_SOCKET_CONNECT_RESULT_CONNECTED  = 1
};

struct SiSocketInterfaceDefinition {
	uint32_t index;

	const char* name;
	const char* description;

	const char* ip_address_v4;
	const char* broadcast_ip_address_v4;
	const char* mask;

    struct SiSocketAddress* si_socket_address;
};

struct SiSocketNetworkHostDefinition {
	const char* ip_address;
	const char* host_name;
};

struct SiSocketAcceptResult {
    SI_BOOL finished;
    struct SiSocket* si_socket;
};

void si_socket_init(void);

struct SiSocket* si_socket_create(enum SiSocketType type);
int si_socket_send(struct SiSocket* si_socket, uint8_t* data, size_t len);
int si_socket_send_to(struct SiSocket* si_socket, uint8_t* data, size_t len, const char* ip_address, int port);
int si_socket_send_to_address(struct SiSocket* si_socket, uint8_t* data, size_t len, struct SiSocketAddress* address);
int si_socket_recv(struct SiSocket* si_socket, uint8_t* data, size_t len);
int si_socket_recv_from(struct SiSocket* si_socket, uint8_t* data, size_t len, struct SiSocketAddress** si_socket_address);

enum SiResult si_socket_bind(struct SiSocket* si_socket, int port);
enum SiResult si_socket_listen(struct SiSocket* si_socket, int backlog);

enum SiSocketConnectResult si_socket_connect(struct SiSocket* si_socket, const char* ip_address, int port);
enum SiSocketConnectResult si_socket_connect_blocking(struct SiSocket* si_socket, const char* ip_address, int port);

struct SiSocketAcceptResult si_socket_accept(struct SiSocket* si_socket, struct SiSocketAddress** si_socket_address);

char* si_socket_get_remote_ip_address(struct SiSocketAddress* si_socket_address);

void si_socket_log_error(struct SiSocket* si_socket, const char* tag, const char* message);

// Free after use
const char* si_socket_get_last_error(struct SiSocket* si_socket);

void si_socket_set_send_buffer(struct SiSocket* si_socket, int size);
void si_socket_set_receive_buffer(struct SiSocket* si_socket, int size);
void si_socket_set_blocking(struct SiSocket* si_socket, SI_BOOL blocking);
void si_socket_set_loopback(struct SiSocket* si_socket, SI_BOOL loopback);
void si_socket_set_broadcast(struct SiSocket* si_socket, SI_BOOL broadcast);
void si_socket_set_reuse(struct SiSocket* si_socket, SI_BOOL reuse);
void si_socket_set_receive_timeout(struct SiSocket* si_socket, int timeout_ms);

const char* si_socket_resolve_dns_name(const char* dns_name);

// SiSocketInterfaceDefinition
// Free after use
struct SiList* si_socket_get_interfaces(void);
void si_socket_free_interface_definition_list(struct SiList* interfaces);

// SiSocketNetworkHostDefinition
struct SiList* si_socket_get_network_host_list(void);
void si_socket_free_network_host_list(struct SiList* list);

struct SiSocketAddress* si_socket_address_clone(struct SiSocketAddress* address);
int si_socket_address_get_remote_port(struct SiSocketAddress* address);
SI_BOOL si_socket_address_equal(struct SiSocketAddress* address_a, struct SiSocketAddress* address_b);
void si_socket_address_close(struct SiSocketAddress* address);

void si_socket_close(struct SiSocket* si_socket);

#ifdef __cplusplus
}
#endif
