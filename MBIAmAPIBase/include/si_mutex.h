#pragma once

#include "si_base.h"

#ifdef __cplusplus
extern "C" {
#endif

struct SiMutex;
    
extern struct SiMutex* SI_MUTEX_MAIN;
    
void si_mutex_init(void);

struct SiMutex* si_mutex_create(void);

void si_mutex_enter(struct SiMutex* mutex);
void si_mutex_leave(struct SiMutex* mutex);

void si_mutex_close(struct SiMutex* mutex);

#ifdef __cplusplus
}
#endif
