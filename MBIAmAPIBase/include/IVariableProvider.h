#ifndef IVariableProviderH
#define IVariableProviderH
#pragma once

#include "si_data_packet.h"

	class IVariableProvider
	{
	public:
		// Get the ID of the Dataref (unique string)
		virtual const char* getId() = 0;

		// Get modification count (used to check for changes)
		virtual int getModCount() = 0;

		// Read data from dataref
		virtual struct SiDataPacket* read() = 0;

		// write data to dataref
		virtual void write(struct SiDataPacket* packet, const char* flags) = 0;

		// Force an update (this will also change the mod counter)
		virtual void forceUpdate() = 0;

		// Set force write. When enabled, this will push the write value every sim frame
		virtual void setForceWrite(bool force) = 0;

		// Check if the dataref had been succesfully bound to backend system (like X-plane)
		virtual bool isBound() = 0;

		// Close and stop the variable handler
		virtual void close() = 0;
	};
#endif
