#pragma once

#include "si_base.h"

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN
#define SI_THREAD_AVAILABLE
#include <winsock2.h>
#include <windows.h>
#include <process.h>

struct SiThread {
    HANDLE thread_handle;
    DWORD thread_id;
};

#define SI_THREAD HANDLE
#elif SI_PLATFORM_SAM54
#define SI_THREAD_AVAILABLE
#include "FreeRTOS.h"
#include "task.h"

struct SiThread {
	xTaskHandle task_handle;
};

#else
#define SI_THREAD_AVAILABLE
#include <pthread.h>

struct SiThread {
    pthread_t thread_handle;
};

#endif

enum SiThreadPrio {
	SI_THREAD_PRIO_LOWEST,
	SI_THREAD_PRIO_LOW,
	SI_THREAD_PRIO_NORMAL,
	SI_THREAD_PRIO_HIGH,
	SI_THREAD_PRIO_HIGHEST,
	SI_THREAD_PRIO_REALTIME
};

void si_thread_sleep_ms(uint32_t time_ms);
void si_thread_sleep_us(uint32_t time_us);

struct SiThread si_thread_create_null(void);
enum SiResult si_thread_create(const char* name, void (*worker_function)(void* tag), void* tag, enum SiThreadPrio prio, SI_BOOL detached, struct SiThread* handle);
struct SiThread si_thread_self(void);
SI_BOOL si_thread_equal(struct SiThread handle_a, struct SiThread handle_b);
SI_BOOL si_thread_is_current_thread(struct SiThread handle);
void si_thread_join(struct SiThread handle);

#ifdef __cplusplus
}
#endif