#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "IVariableProvider.h"

#include "si_mutex.h"
#include "si_data_packet.h"

#ifndef IBM
#include <pthread.h>
#include <netinet/in.h>
#else
#include <Winsock2.h>
#endif


class MemoryVariableHandler : IVariableProvider
{
private:

	// AM data ID
	char* id;

	// Modification counter (used to notice data delta's)
	int mod_count;

	// Buffered value
	struct SiDataPacket* value;

    struct SiMutex* mutex;

public:

	// Constructor
	MemoryVariableHandler(const char* id, const char* unit, enum SiDataPacketType type, int size);

	bool isBound();

	// Get packet. WARNING: Return packet needs to be freed by caller!
	struct SiDataPacket* read();
	
	// Write packet
	void write(struct SiDataPacket* packet, const char* flags);

	void forceUpdate();

	void setForceWrite(bool);

	const char* getId();
	int getModCount();

	// Close this dataref handler and everything under its control
	void close();
};
