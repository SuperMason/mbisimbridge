#ifndef MDoubleArrayPropertyProviderH
#define MDoubleArrayPropertyProviderH
#pragma once

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "MPropertyProviderBase.h"
#include "IVariableProvider.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall* doublePointCallback_function)(double*, int);//20211021-1,Zac
class MDoubleArrayPropertyProvider : MPropertyProviderBase, IVariableProvider//20220125-3,Zac
{
private:
	//int* cached_value;
	double* cached_value = (double*)malloc(1 * sizeof(double));
	int array_size = 1;

public:
	//MIntegerArrayPropertyProvider(const char* id, int value);
	MDoubleArrayPropertyProvider(const char* id, double* defaultValue, const char* typeTag, int size);
	~MDoubleArrayPropertyProvider();

	void set(double* value);
	double* get();

	doublePointCallback_function _callbackFunc;
	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};


#endif