#include "pch.h"
#include "MSimpleCommandProvider.h"

#include "si_string.h"
#include "si_log.h"

namespace MBIAmAPI {
	MSimpleCommandProvider::MSimpleCommandProvider(const char * id, void(*callback)(MSimpleCommandProvider *provider, CommandType type, SI_BOOL has_value, int value))
	{
		mutex = si_mutex_create();
		this->id = si_string_clone(id);
		this->callback = callback;
	}

	MSimpleCommandProvider::~MSimpleCommandProvider()
	{
	}

	const char* MSimpleCommandProvider::getId() {
		return id;
	}

	bool MSimpleCommandProvider::isBound() {
		return true;
	}

	void MSimpleCommandProvider::close() {
		si_string_free(id);
		si_mutex_close(mutex);
	}

	void MSimpleCommandProvider::fire(CommandType type, int value) {
		si_mutex_enter(mutex);
		callback(this, type, SI_TRUE, value);
		si_mutex_leave(mutex);
	}

	void MSimpleCommandProvider::fire(CommandType type) {
		si_mutex_enter(mutex);
		callback(this, type, SI_FALSE, 0);
		si_mutex_leave(mutex);
	}

	int MSimpleCommandProvider::getModCountStart() {
		return 0;
	}

	int MSimpleCommandProvider::getModCountOnce() {
		return 0;
	}

	int MSimpleCommandProvider::getModCountEnd() {
		return 0;
	}
}