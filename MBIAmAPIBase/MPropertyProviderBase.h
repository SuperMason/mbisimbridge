#ifndef MPropertyProviderBaseH
#define MPropertyProviderBaseH
#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "IVariableProvider.h"

#include "si_mutex.h"
using namespace std;
	class MPropertyProviderBase
	{
	private:
		// variable ID
		const char* id;

		struct SiMutex* mutex;

	protected:
		// Modification counter (used to notice variable delta's)
		int modCount;
		void initializeCriticalSection();
		void enterCriticalSection();
		void leaveCriticalSection();
	public:
		MPropertyProviderBase(const char* id, const char* base_type);
		~MPropertyProviderBase();
		const char* getId();

		bool isBound();

		void forceUpdate();

		void setForceWrite(bool);

		int getModCount();

		void close();
	};
#endif