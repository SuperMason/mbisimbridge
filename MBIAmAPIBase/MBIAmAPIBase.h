
#ifndef MBIAmAPIBaseH
#define MBIAmAPIBaseH
#pragma once

#include "MIntegerPropertyProvider.h"
#include "MBytePropertyProvider.h"
#include "MIntegerArrayPropertyProvider.h"
#include "MFloatArrayPropertyProvider.h"
#include "MDoubleArrayPropertyProvider.h"
#include "MStringPropertyProvider.h"
#include "MFloatPropertyProvider.h"
#include "MDoublePropertyProvider.h"
#include "MSimpleCommandProvider.h"

#include <list>
#include <map>
#include <string>
#include "SimBus.h"
using namespace std;
namespace MBIAmAPI {
	class __declspec(dllexport) MBIAmAPIBase
	{
	private:
		//MIntegerPropertyProvider* myIntValue;
		//MFloatPropertyProvider* myFloatValue;
		//MSimpleCommandProvider* myCommandName;
		map<string, MFloatPropertyProvider*> floatValueSet;
		map<string, MDoublePropertyProvider*> doubleValueSet;//20220114-2,Zac
		map<string, MIntegerPropertyProvider*> intValueSet;
		map<string, MBytePropertyProvider*> byteValueSet;//20220114-1,Zac
		map<string, MIntegerArrayPropertyProvider*> intArrayValueSet;//20220114-1,Zac
		map<string, MFloatArrayPropertyProvider*> floatArrayValueSet;//20220114-3,Zac
		map<string, MDoubleArrayPropertyProvider*> doubleArrayValueSet;//20220516-1,Zac
		map<string, MStringPropertyProvider*> stringValueSet;
		map<string, MSimpleCommandProvider*> commandSet;
		//list<MFloatPropertyProvider*> floatValueSet;
		//list<MSimpleCommandProvider*> commandSet;
		const char* _LogTag;
		const char* _DATA_SOURCE_NAME;
		int _DATA_SOURCE_UDP_PORT;
		SimBus* bus;
		ProviderPool* pool;
		//callback_function _callbackFunc;
	public:
		MBIAmAPIBase();
		~MBIAmAPIBase();
		//void init(list<pair<const char *, callback_function>> cmdlist);
		void init(const char* DATA_SOURCE_NAME, int DATA_SOURCE_UDP_PORT);
		void start();
		void close();
		void sendFloat(const char * name, float value);
		void sendFloatArray(const char* name, float* value);//20220114-3,Zac
		void sendDoubleArray(const char* name, double* value);//20220516-1,Zac
		void sendDouble(const char* name, double value);//20220114-2,Zac
		void sendInt(const char * name, int value);
		void sendIntArray(const char* name, int* value);
		void sendString(const char * name, const char * value);
		void sendByte(const char* name, BYTE value);//20220114-1,Zac
		//void DoSomething(callback_function callbackFunc);
		void AddCommandProvider(const char * name,  callback_function callbackFuncInput);
		void AddVariableProvider(const char * name, int type, float value);
		void AddVariableProvider(const char * name, int type, int value, intCallback_function callbackFuncInput);
		void AddVariableProvider(const char* name, int type, int* value,int arraySize, intPointCallback_function callbackFuncInput);//20220114-1,Zac
		void AddVariableProvider(const char* name, int type, float* value, int arraySize, floatPointCallback_function callbackFuncInput);//20220114-3,Zac
		void AddVariableProvider(const char* name, int type, double* value, int arraySize, doublePointCallback_function callbackFuncInput);//20220516-1,Zac
		void AddVariableProvider(const char * name, int type, float value, floatCallback_function callbackFuncInput);
		void AddVariableProvider(const char* name, int type, double value, doubleCallback_function callbackFuncInput);//20220114-2,Zac
		void AddVariableProvider(const char * name, int type, const char* value, strCallback_function callbackFuncInput);
		void AddVariableProvider(const char* name, int type, BYTE value, byteCallback_function callbackFuncInput);//20220114-1,Zac
		static void command_callback(MSimpleCommandProvider* provider, CommandType type, SI_BOOL has_value, int value);
	};
}
#endif

