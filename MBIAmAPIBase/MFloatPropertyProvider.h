
#ifndef MFloatPropertyProviderH
#define MFloatPropertyProviderH

#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "IVariableProvider.h"
#include "MPropertyProviderBase.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall *floatCallback_function)(float);//20211021-1,Zac
	class MFloatPropertyProvider : MPropertyProviderBase, IVariableProvider
	{
		private:
			float cached_value;

		public:
			MFloatPropertyProvider(const char* id, float value);
			~MFloatPropertyProvider();
			void set(float value);
			float get();

			struct SiDataPacket* read();
			void write(struct SiDataPacket* packet, const char* flags);

			floatCallback_function _callbackFunc;//20211021-1,Zac
			const char* getId() { return MPropertyProviderBase::getId(); }
			bool isBound() { return MPropertyProviderBase::isBound(); }
			void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
			void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
			int getModCount() { return MPropertyProviderBase::getModCount(); }
			void close() { return MPropertyProviderBase::close(); }
	};
#endif

