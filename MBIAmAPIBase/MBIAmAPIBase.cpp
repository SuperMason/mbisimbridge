#include "pch.h"
#include "MBIAmAPIBase.h"
#include <stdio.h>

#include "si_log.h"
#include "si_thread.h"
#include "si_string.h"
#include "sim_discover_server.h"

#include "SimBus.h"
#include "ProviderPool.h"
#include <string>
#include <sstream>
namespace MBIAmAPI {
	MBIAmAPIBase::MBIAmAPIBase()
	{
	}

	MBIAmAPIBase::~MBIAmAPIBase()
	{
	}
	//void MBIAmAPIBase::init(list<pair<const char *, callback_function>> cmdlist)
	void MBIAmAPIBase::init(const char* DATA_SOURCE_NAME, int DATA_SOURCE_UDP_PORT)
	{
		if (DATA_SOURCE_NAME == nullptr) {
			return;
		}
		_LogTag = "MBIAmAPIBase";
		_DATA_SOURCE_NAME = DATA_SOURCE_NAME;
		_DATA_SOURCE_UDP_PORT = DATA_SOURCE_UDP_PORT;
		/*for each (pair<const char *, callback_function> a in cmdlist)
		{
			printf(a.first);
		}*/
		//myCommandName = new MSimpleCommandProvider("myCommandName", command_callback);
		//myFloatValue = new MFloatPropertyProvider("myFloatValue", 0.0f);
		si_log_config_write_to_static_file("log.txt");
		si_log_set_log_level(SI_LOG_INFO);
		si_socket_init();
		si_log_info(_LogTag, "-------- SimBus Info. --------");
		si_log_info(_LogTag, "Start Listening AM/AP...");
		si_log_info(_LogTag, "Tag :: '%s'", _DATA_SOURCE_NAME);
		si_log_info(_LogTag, "UDP Port :: %d", _DATA_SOURCE_UDP_PORT);
		si_log_info(_LogTag, "------------------------------");

		// Create Provider pool
		pool = new ProviderPool();

		// Register your "Variables" and "Commands"
		//pool->registerVariableProvider((IVariableProvider*)myIntValue);
		//pool->registerVariableProvider((IVariableProvider*)myFloatValue);
		//pool->registerCommandProvider((ICommandProvider*)myCommandName);

		// Init the SimBus
		//si_log_info(_LogTag, "Init SimBus...");
		////static SimBus* bus = new SimBus(_DATA_SOURCE_UDP_PORT, pool);
		//bus = new SimBus(_DATA_SOURCE_UDP_PORT, pool);

		//// Init SimDiscovery server
		//sim_discover_server_init(SIM_DISCOVER_OBJECT_EXTERNAL_SOURCE);
		//sim_discover_server_set_prop_string(SIM_DISCOVER_PROP_NAME_EXTERNAL_SOURCE_TAG, _DATA_SOURCE_NAME);
		//sim_discover_server_set_prop_int(SIM_DISCOVER_PROP_NAME_SIM_BUS_PORT, _DATA_SOURCE_UDP_PORT);
	}
	void MBIAmAPIBase::start() {
		si_log_info(_LogTag, "Init SimBus...");
		//static SimBus* bus = new SimBus(_DATA_SOURCE_UDP_PORT, pool);
		bus = new SimBus(_DATA_SOURCE_UDP_PORT, pool);

		// Init SimDiscovery server
		sim_discover_server_init(SIM_DISCOVER_OBJECT_EXTERNAL_SOURCE);
		sim_discover_server_set_prop_string(SIM_DISCOVER_PROP_NAME_EXTERNAL_SOURCE_TAG, _DATA_SOURCE_NAME);
		sim_discover_server_set_prop_int(SIM_DISCOVER_PROP_NAME_SIM_BUS_PORT, _DATA_SOURCE_UDP_PORT);
	}
	void MBIAmAPIBase::AddCommandProvider(const char* name, callback_function callbackFuncInput) {
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MSimpleCommandProvider* CommandName = new MSimpleCommandProvider(name, command_callback);
		CommandName->_callbackFunc = callbackFuncInput;
		commandSet.insert(pair<string, MSimpleCommandProvider*>(name, CommandName));
		pool->registerCommandProvider((ICommandProvider*)CommandName);
	}

	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, int value, intCallback_function callbackFuncInput) {
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MIntegerPropertyProvider* intValue = new MIntegerPropertyProvider(name, value);
		intValue->_callbackFunc = callbackFuncInput;
		intValueSet.insert(pair<string, MIntegerPropertyProvider*>(name, intValue));
		pool->registerVariableProvider((IVariableProvider*)intValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, BYTE value, byteCallback_function callbackFuncInput) {//20220125-1,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MBytePropertyProvider* byteValue = new MBytePropertyProvider(name, value);
		byteValue->_callbackFunc = callbackFuncInput;
		byteValueSet.insert(pair<string, MBytePropertyProvider*>(name, byteValue));
		pool->registerVariableProvider((IVariableProvider*)byteValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, int* value,int arraySize, intPointCallback_function callbackFuncInput) {//20220125-1,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		const char* typeTag = (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(("INT[" + arraySize.ToString() + "]"))).ToPointer();
		//printf("typeTag : %s   \n", typeTag);
		MIntegerArrayPropertyProvider* intValue = new MIntegerArrayPropertyProvider(name, value, typeTag, arraySize);
		intValue->_callbackFunc = callbackFuncInput;
		intArrayValueSet.insert(pair<string, MIntegerArrayPropertyProvider*>(name, intValue));
		pool->registerVariableProvider((IVariableProvider*)intValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char * name, int type, float value, floatCallback_function callbackFuncInput) {
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MFloatPropertyProvider* FloatValue = new MFloatPropertyProvider(name, value);
		FloatValue->_callbackFunc = callbackFuncInput;
		floatValueSet.insert(pair<string, MFloatPropertyProvider*>(name, FloatValue));
		pool->registerVariableProvider((IVariableProvider*)FloatValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, float* value, int arraySize, floatPointCallback_function callbackFuncInput) {//20220125-3,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		const char* typeTag = (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(("FLOAT[" + arraySize.ToString() + "]"))).ToPointer();
		//printf("typeTag : %s   \n", typeTag);
		MFloatArrayPropertyProvider* floatValue = new MFloatArrayPropertyProvider(name, value, typeTag, arraySize);
		floatValue->_callbackFunc = callbackFuncInput;
		floatArrayValueSet.insert(pair<string, MFloatArrayPropertyProvider*>(name, floatValue));
		pool->registerVariableProvider((IVariableProvider*)floatValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, double* value, int arraySize, doublePointCallback_function callbackFuncInput) {//20220516-1,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		const char* typeTag = (const char*)(System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(("DOUBLE[" + arraySize.ToString() + "]"))).ToPointer();
		//printf("typeTag : %s   \n", typeTag);
		MDoubleArrayPropertyProvider* doubleValue = new MDoubleArrayPropertyProvider(name, value, typeTag, arraySize);
		doubleValue->_callbackFunc = callbackFuncInput;
		doubleArrayValueSet.insert(pair<string, MDoubleArrayPropertyProvider*>(name, doubleValue));
		pool->registerVariableProvider((IVariableProvider*)doubleValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, double value, doubleCallback_function callbackFuncInput) {//20220125-2,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MDoublePropertyProvider* DoubleValue = new MDoublePropertyProvider(name, value);
		DoubleValue->_callbackFunc = callbackFuncInput;
		doubleValueSet.insert(pair<string, MDoublePropertyProvider*>(name, DoubleValue));
		pool->registerVariableProvider((IVariableProvider*)DoubleValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, const char* value, strCallback_function callbackFuncInput) {
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		MStringPropertyProvider* stringValue = new MStringPropertyProvider(name, value);
		stringValue->_callbackFunc = callbackFuncInput;
		stringValueSet.insert(pair<string, MStringPropertyProvider*>(name, stringValue));
		pool->registerVariableProvider((IVariableProvider*)stringValue);
	}
	void MBIAmAPIBase::AddVariableProvider(const char* name, int type, float value) {
		if (name == nullptr) {
			return;
		}
		if (type == 1) {
			MFloatPropertyProvider* FloatValue = new MFloatPropertyProvider(name, value);
			floatValueSet.insert(pair<string, MFloatPropertyProvider*>(name, FloatValue));
			pool->registerVariableProvider((IVariableProvider*)FloatValue);
		}
		else if (type == 2) {
			MIntegerPropertyProvider* intValue = new MIntegerPropertyProvider(name, (int)value);
			intValueSet.insert(pair<string, MIntegerPropertyProvider*>(name, intValue));
			pool->registerVariableProvider((IVariableProvider*)intValue);
		}
		else if (type == 3) {
			MStringPropertyProvider* stringValue = new MStringPropertyProvider(name, "");
			stringValueSet.insert(pair<string, MStringPropertyProvider*>(name, stringValue));
			pool->registerVariableProvider((IVariableProvider*)stringValue);
		}
	}
	void MBIAmAPIBase::command_callback(MSimpleCommandProvider* provider, CommandType type, SI_BOOL has_value, int value) {
		if (provider == nullptr) {
			return;
		}
		//MSimpleCommandProvider* temp = (MSimpleCommandProvider*)pObject;
		//if (provider == temp->myCommandName) { // 過濾 "特定指令", 這個 if 只負責 myCommandName 指令
		// Received command from "Air Manager" or "Air Player"
			//const char *_Format = "\nCommand received with CommandName(%s), value(%i)";
			//si_log_warning("Main", _Format, provider->getId(), value);
			//printf(_Format, provider->getId(), value);
			//int feedbackValue = value + 10;
			//	myIntValue->set(feedbackValue); // 由 AM/AP 傳送過來的 Command value, 處理後, 再透過 myIntValue 回寫回去 AM/AP
			//	printf("\nFeedback +10 value : %i, after Command", feedbackValue);

			//	int sum;
			//	Calculate ^cc = gcnew Calculate();  // create C# object
			//	sum = cc->GetResult(value, feedbackValue); // call C# method()
			//	myIntValue->set(sum); // 將 C# 處理過的結果, 再透過 myIntValue 回寫回去 AM/AP
			//	printf("\nC# 計算結果, 回寫至 AM, raw(%d) + add10(%d) = %d", value, feedbackValue, sum);
			
			provider->_callbackFunc(value, provider->getId());
			//provider->_callbackFunc(value, "aaaa");
		//}

	}
	void MBIAmAPIBase::close()
	{
		si_log_info(_LogTag, "Stopping, Cleaning up now...");
		sim_discover_server_close(); // 關閉 sim_discover_server
		//bus->close_bus();			 // 關閉 sim bus
		si_log_info(_LogTag, "Done cleaning up, exiting now.");
	}

	void MBIAmAPIBase::sendFloat(const char* name, float value)
	{
		if (name == nullptr) {
			return;
		}
		floatValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendDouble(const char* name, double value)//20220125-2,Zac
	{
		if (name == nullptr) {
			return;
		}
		doubleValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendInt(const char* name, int value)
	{
		if (name == nullptr) {
			return;
		}
		intValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendByte(const char* name, BYTE value)//20220125-1,Zac
	{
		if (name == nullptr) {
			return;
		}
		byteValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendIntArray(const char* name, int* value)//20220125-1,Zac
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		intArrayValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendFloatArray(const char* name, float* value)//20220125-3,Zac
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		floatArrayValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendDoubleArray(const char* name, double* value)//20220516-1,Zac
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		doubleArrayValueSet[name]->set(value);
	}
	void MBIAmAPIBase::sendString(const char* name, const char* value)
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		stringValueSet[name]->set(value);
	}
	//void MBIAmAPIBase::DoSomething(callback_function callbackFunc) {
	//	myCommandName->_callbackFunc = callbackFunc;
	//}
}