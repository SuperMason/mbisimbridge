#ifndef MDoublePropertyProviderH
#define MDoublePropertyProviderH

#pragma once
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "IVariableProvider.h"
#include "MPropertyProviderBase.h"

#ifdef WIN
#include <Windows.h>
#endif

#ifdef RPI
#include <pthread.h>
#endif
using namespace std;
typedef void(__stdcall* doubleCallback_function)(double);//20220125-2,Zac
class MDoublePropertyProvider : MPropertyProviderBase, IVariableProvider
{
private:
	double cached_value;

public:
	MDoublePropertyProvider(const char* id, double value);
	~MDoublePropertyProvider();
	void set(double value);
	double get();

	struct SiDataPacket* read();
	void write(struct SiDataPacket* packet, const char* flags);

	doubleCallback_function _callbackFunc;
	const char* getId() { return MPropertyProviderBase::getId(); }
	bool isBound() { return MPropertyProviderBase::isBound(); }
	void forceUpdate() { MPropertyProviderBase::forceUpdate(); }
	void setForceWrite(bool value) { MPropertyProviderBase::setForceWrite(value); }
	int getModCount() { return MPropertyProviderBase::getModCount(); }
	void close() { return MPropertyProviderBase::close(); }
};
#endif

