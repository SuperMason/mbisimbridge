﻿using MBILibrary.Enums;
using MBILibrary.Models;
using MBILibrary.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataRef;

namespace MBISimBridge.bgWorker
{
    /// <summary>
    /// 用來收集 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
    /// <para>只處理 int[], float[], double[]</para>
    /// </summary>
    public class QueueDrArray : BackgroundWorker
    {
        #region properties
        public delegate void CallBackFunc(DataRefElement[] arrayDR, DataRefElementBase srcDR);
        private readonly CallBackFunc CallBackFunction = null;
        /// <summary>
        /// 判斷是否有任何 DataRefElement Object 需要處理 ?
        /// </summary>
        private bool IsDrNotEmpty { get { return !ArrayDR.IsListEmpty(); } }
        /// <summary>
        /// 紀錄 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
        /// </summary>
        private readonly List<DataRefElement> ArrayDR = null;
        /// <summary>
        /// 紀錄每一組 Queue DataRef 的耗時, 超過 QueueMaxMS 就強制補齊其他未到齊的 Elements
        /// <para>1. 因為 X-Plane 只會針對有異動的 Elements 回覆, 所以並不會一次全部的 Elements 都會 Trigger Response 行為</para>
        /// <para>2. 所以這邊自訂上限值, 超過 500 ms 沒有收集齊全所有的 Elements 時, 就會自動補齊其餘缺少的 Elements</para>
        /// <para>3. 自動補齊的 Element 值, 預設為 0</para>
        /// <para>4. 實測結果, 如果一個 int[8] 所有的 elements 的值都有異動, X-Plane 大約耗時 50 ms, 全部會 Response 回來</para>
        /// </summary>
        private readonly List<ArrayElementQueueTime> DataRefQueueSpendingMS = null;
        /// <summary>
        /// 正在執行的 DataRef, 避免重複 Invoke
        /// </summary>
        private readonly List<string> InvokingDataRef = null;
        /// <summary>
        /// <para>1. 有 Element 時, 快速處理 ::: 每 0.001 秒檢查一次</para>
        /// <para>2. 無 Element 時, 等待久一點 ::: 每 1 秒檢查一次</para>
        /// </summary>
        private double SleepMS { get { return IsDrNotEmpty ? 1 : 1000; } }
        /// <summary>
        /// Invoke 發生, 開始執行 Send Message to Air Manager
        /// </summary>
        private static readonly string ProcessSendArrayDrMessage = string.Format(CultureInfo.CurrentCulture, "{0} ::: {1}", nameof(QueueDrArray), "DataRef({0}) GrpDrCNT({1}) {2} Send to Air Manager");
        #endregion

        #region constructor
        /// <summary>
        /// 用來收集 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
        /// <para>只處理 int[], float[], double[]</para>
        /// </summary>
        public QueueDrArray(CallBackFunc callBackFunc) {
            ArrayDR = new List<DataRefElement>();
            DataRefQueueSpendingMS = new List<ArrayElementQueueTime>();
            InvokingDataRef = new List<string>();
            CallBackFunction = callBackFunc;

            WorkerReportsProgress = true;
            WorkerSupportsCancellation = true;
            DoWork += _DoWork;
            RunWorkerCompleted += _RunWorkerCompleted;
            if (!IsBusy) { RunWorkerAsync(); }
        }
        #endregion


        private void _DoWork(object sender, DoWorkEventArgs e) {
            if (IsDrNotEmpty) { CheckDrElementAllArrivable(); }
            _ = SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(SleepMS));
        }
        private void _RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        { if (!IsBusy) { RunWorkerAsync(); } }


        #region Methods
        /// <summary>
        /// 新增每一個 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
        /// <para>只處理 int[], float[], double[]</para>
        /// </summary>
        public void AddDataRefElement(DataRefElement newOne) {
            if (newOne == null)
            { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "'{0}' add null {1}, skip it.", nameof(QueueDrArray), nameof(DataRefElement))); return; }
            AddArrayDR(newOne, AddQueueElementSrcFrom.FromXP);

            #region 紀錄每一組 Array DataRefElement 有沒有 Queue 很久 ?
            var IsExist = DataRefQueueSpendingMS.ToArray().Where(d => d != null && d.RawDataRefName.Equals(newOne.RawDataRef)).FirstOrDefault();
            if (IsExist == null) {
                var o = new ArrayElementQueueTime(newOne.RawDataRef, newOne.ObjVal2Float, newOne.ArrayIdx);
                DataRefQueueSpendingMS.Add(o);
                ShowLogMessage(o, "該組第 1 個回覆回來的 Element");
            } else {
                foreach (var o in DataRefQueueSpendingMS.ToArray().Where(d => d != null && d.RawDataRefName.Equals(newOne.RawDataRef)).ToArray()) {
                    o.UpdateQueueStartDT(newOne.ObjVal2Float, newOne.ArrayIdx);// 該組之後回覆回來的 Elements, 每次都更新 QueueStartDT
                    ShowLogMessage(o, "之後的更新");
                }
            }
            #endregion
        }
        private void ShowLogMessage(ArrayElementQueueTime a, string MessageHeader) {
#if DEBUG
            //if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage) {
            //    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "{0} = {1} :: DataRef({2}[{3}]), Value({4}), QueueStartDT({5}), SpendingMS({6})"
            //     , nameof(ArrayElementQueueTime), MessageHeader, a.RawDataRefName, a.LatestIndex, a.LatestValue, a.QueueStartDT.ToString(UtilDateTime.Format_HHmmssfff), a.SpendingMS));
            //}
#endif
        }

        /// <summary>
        /// 已經送至 Air Manager, 所以移除 Elements
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveDataRefElement(string rmDataRefName) {
            try {
                int RmArrayDrCNT = 0, RmDataRefQueueSpendingMSCNT = 0, RmInvokingDataRefCNT = 0;

                //foreach (var o in ArrayDR.ToArray().Where(d => d != null && d.RawDataRef.Equals(rmDataRefName)).ToArray()) {
                    int rmArrayDrCNT = ArrayDR.RemoveAll(d => d != null && d.RawDataRef.Equals(rmDataRefName));
                    if (rmArrayDrCNT != 0) { RmArrayDrCNT += rmArrayDrCNT; }
                //}

                //foreach (var o in DataRefQueueSpendingMS.ToArray().Where(d => d != null && d.RawDataRefName.Equals(rmDataRefName)).ToArray()) {
                    int rmQueueDrCNT = DataRefQueueSpendingMS.RemoveAll(d => d != null && d.RawDataRefName.Equals(rmDataRefName));
                    if (rmQueueDrCNT != 0) { RmDataRefQueueSpendingMSCNT += rmQueueDrCNT; }
                //}

                //foreach (var o in InvokingDataRef.ToArray().Where(s => !string.IsNullOrEmpty(s) && s.Equals(rmDataRefName)).ToArray()) {
                    int rmInvokeCNT = InvokingDataRef.RemoveAll(s => !string.IsNullOrEmpty(s) && s.Equals(rmDataRefName));
                    if (rmInvokeCNT != 0) { RmInvokingDataRefCNT += rmInvokeCNT; }
                //}

                if (RmArrayDrCNT != 0 || RmDataRefQueueSpendingMSCNT != 0 || RmInvokingDataRefCNT != 0) {
                    //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "{0} ::: DataRef({1}) 已經送至 Air Manager, 所以移除相關 Elements, ArrayDR({2}), DataRefQueueSpendingMS({3}), InvokingDataRef({4})"
                    //    , nameof(QueueDrArray), rmDataRefName, RmArrayDrCNT, RmDataRefQueueSpendingMSCNT, RmInvokingDataRefCNT));
                }
            } catch (Exception ex) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "rmDataRefName({0}) occur ExceptionStackTrace ::: {1}", rmDataRefName, ex.StackTrace)); }
        }

        /// <summary>
        /// 依據 Raw DataRef Name 進行分類, 同一組的收集齊全後, 再統一送至 Air Manager
        /// <para>注意 !!!</para>
        /// <para>1. X-Plane 不會一次全部回覆所有 Array 的 Element 值, 只會更新有異動的 Element, 所以有可能一直無法收集到完整的 Array</para>
        /// <para>2. 補救的方式 :: 當 Queue 超過 QueueMaxMS ms 時, 不足的 Element 自動補 0 回覆</para>
        /// </summary>
        private void CheckDrElementAllArrivable() {
            var GrpRawDataRefList = ArrayDR.ToArray().Where(d => d != null).GroupBy(d => new { d.RawDataRef }).Select(g => g.Key.RawDataRef).ToArray();
            foreach (var RawDataRefName in GrpRawDataRefList) { // 依據原始的 DataRef Name 分類
                //IsOverLimitQueueTime(); // 不需要這個機制了, 因為 X-Plane 會頻繁的 Response, 所以不太可能會漏... 而且 X-Plane 會依序 index 回覆, 所以不用擔心順序問題...  自動補齊超過 Queue Time 的 Elements
                var GrpDR = GetGrpDR(RawDataRefName); // 依據原始的 DataRef Name 取得當下所有 Elements, 因為運作速度快, 所以不會剛好到齊就 Trigger Function, 可能會超過數量, 一樣要 Trigger

                #region 全部到齊就 Trigger Function, 全部送至 Air Manager, 當 AirManagerService 那邊確定送出後, 會回頭刪除已送出的 Elements, RemoveDataRefElement(string rmDataRefName)
                var SrcDR = InitParamsCtrller.GetDataRefElementFromInit(RawDataRefName); // 註冊總表, 用來比對用的
                if (SrcDR != null && SrcDR.ArrayLen <= GrpDR.Length && !InvokingDataRef.ToArray().Any(s => s.Equals(RawDataRefName))) {
                    InvokingDataRef.Add(RawDataRefName); // 紀錄當下 Trigger 的 DataRef Name
                    // Trigger 當下同一組 DataRef 可能會有重複 n 組... 
                    // 全部撈出來回覆給 Air Manager, 會回覆 n 次, 之後第一組回來時, 就會把所有的 Elements 刪除
#if DEBUG
                    //if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage)
                    //{ UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ProcessSendArrayDrMessage, RawDataRefName, GrpDR.Length, "開始")); }
#endif
                    CallBackFunction?.Invoke(GrpDR, SrcDR);
                }
                #endregion
            }
        }

        /// <summary>
        /// 取出該組 DataRefElement 當下的所有 Elements
        /// </summary>
        private DataRefElement[] GetGrpDR(string RawDataRefName)
        { return ArrayDR.ToArray().Where(d => d != null && d.RawDataRef.Equals(RawDataRefName)).ToArray(); }
        /// <summary>
        /// 新增來源 :::
        /// <para>1. 新增每一個 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager</para>
        /// <para>2. 當 Queue 超過 QueueMaxMS 時, 不足的 Element 自動補 0 回覆</para>
        /// </summary>
        private void AddArrayDR(DataRefElement add, AddQueueElementSrcFrom srcFrom) {
            //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Add Queue Elements, srcFrom ::: {0}", srcFrom));
            var existed = ArrayDR.ToArray().Where(d => d != null && d.DataRef.Equals(add.DataRef)).FirstOrDefault();
            if (existed == null)
            { ArrayDR.Add(add); } // 速度太快時, 會造成 element 累積過度.. 所以還是判斷沒有存在時, 再加入...
        }

        /// <summary>
        /// 當 Queue 超過 QueueMaxMS 時, 不足的 Element 自動補 0 回覆
        /// <para>注意 !!! 不需要這個機制了, 因為 X-Plane 會頻繁的 Response, 所以不太可能會漏... 而且 X-Plane 會依序 index 回覆, 所以不用擔心順序問題...</para>
        /// </summary>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private void IsOverLimitQueueTime() {
            foreach (var qDR in DataRefQueueSpendingMS.ToArray()) {
                //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "DataRef({0}), QueueStartDT({1}), CurrentDT({2}), IsOverMaxMS({3}), spending MS :: {4}"
                //    , qDR.RawDataRefName, qDR.QueueStartDT.ToString(UtilDateTime.Format_Defaultfff), DateTime.Now.ToString(UtilDateTime.Format_Defaultfff)
                //    , qDR.IsOverMaxMS, qDR.SpendingMS));
                if (qDR != null && qDR.IsOverMaxMS) {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "'{0}' Queue住的時間({1}) 超過 {2} ms, 補齊不足的 Elements, 初始值為 0"
                        , qDR.RawDataRefName, qDR.SpendingMS, ArrayElementQueueTime.QueueMaxMS));
                    var GrpDR = GetGrpDR(qDR.RawDataRefName);
                    lock (GrpDR) {
                        if (!GrpDR.IsListEmpty()) {
                            var SrcDR = InitParamsCtrller.GetDataRefElementFromInit(qDR.RawDataRefName); // 註冊總表, 用來比對用的
                            if (SrcDR != null && SrcDR.ArrayLen != GrpDR.Length) {
                                // 有缺, 逐一檢查, 看缺哪一個 Element, 缺少的統一用 Element[0] clone 過來, 再把值修改成 0 ::: UtilConstants.InitialBytesValue4Float
                                for (int i = 0; i < SrcDR.ArrayLen; i++) {
                                    var LackElement = GrpDR.ToArray().Where(d => d != null && d.ArrayIdx == i).FirstOrDefault();
                                    if (LackElement == null) {
                                        var FirstElement = GrpDR.FirstOrDefault();
                                        if (FirstElement != null) {
                                            DataRefElement AutoGenDR = FirstElement.ShallowClone();
                                            AutoGenDR.DataRef = string.Format(CultureInfo.CurrentCulture, UtilConstants.ArrayDataRefNameFormat, qDR.RawDataRefName, i); // 更新 DataRef Name
                                            if (AutoGenDR.IsSingleIntType || AutoGenDR.IsSingleFloatType) // 更新 初始值 為 0
                                            { AutoGenDR.RawByteValue = UtilConstants.InitialBytesValue4Float; }
                                            else if (AutoGenDR.IsSingleDoubleType)
                                            { AutoGenDR.RawByteValue = UtilConstants.InitialBytesValue4Double; }
                                            AddArrayDR(AutoGenDR, AddQueueElementSrcFrom.FromAutoGen);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}
