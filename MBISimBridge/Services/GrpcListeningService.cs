﻿using MBIClient;
using MBILibrary.Util;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBISimBridge.Services
{
    /// <summary>
    /// 專職 監聽 gRPC Server 是否連線 ?
    /// </summary>
    public class GrpcListeningService : BackgroundService
    {
        #region fields
        private readonly ILogger<GrpcListeningService> _logger;
        #endregion

        #region constructor
        /// <summary>
        /// 專職 監聽 gRPC Server 是否連線 ?
        /// </summary>
        public GrpcListeningService(ILogger<GrpcListeningService> logger) {
            _logger = logger;
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, nameof(GrpcListeningService)));
        }
        #endregion

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            while (!stoppingToken.IsCancellationRequested) {
                await GrpcClientChannel.IsChannelReadyAsync();
                await Task.Delay(GrpcClientChannel.ChannelConnectAsyncDeadLine, stoppingToken); // 每 0.1 秒, 持續監聽 gRPC Server Status, Open? Close?
            }
        }
    }
}
