﻿using MBIAmAPI;
using MBILibrary.Enums;
using MBILibrary.Util;
using MBISimBridge.MbiSystems;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace MBISimBridge.Services
{
    /// <summary>
    /// 專職處理與 MBIAmAPIWrapper C++ 的通訊
    /// </summary>
    public class AirManagerService : BackgroundService
    {
        #region fields
        /// <summary>
        /// 負責處理與 Air Manager 溝通的介面 (C++ Lib)
        /// </summary>
        private MBIAmAPIWrapper MbiAmAPI = null;
        /// <summary>
        /// 用來收集 X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
        /// </summary>
        //private readonly QueueDrArray oQueueDrArray = null; 改用 ArrayDrVal 物件處理 int[], float[], double[]

        private readonly ILogger<AirManagerService> _logger;
        /// <summary>
        /// 等待 gRPC 連線
        /// <para>值必須以毫秒為單位, 轉譯為 -1 (表示無限逾時)、0 或是小於或等於 Int32.MaxValue 的正整數。</para>
        /// <para>避免發生 System.ArgumentOutOfRangeException, 因為是包在 Microsoft.Extensions.Hosting 元件裡, 這應該是元件本身的限制, 因為會死在 IHostBuilder 元件裡</para>
        /// </summary>
        private int WaitingInterval = -1; // 微毫秒

        /// <summary>
        /// (完成作業) 將訂閱的 DataRefs 註冊到 Air Manager SimBus
        /// <para>X-Plane 會等待這一個 Flag 完成設為 true 後, 才會進行註冊行為</para>
        /// </summary>
        public static bool SubscribedDataRefsFinished = false;
        /// <summary>
        /// (完成作業) 將訂閱的 Commands 註冊到 Air Manager SimBus
        /// </summary>
        public static bool SubscribedCommandsFinished = false;
        #endregion

        #region constructor
        /// <summary>
        /// 專職處理與 MBIAmAPIWrapper C++ 的通訊
        /// </summary>
        public AirManagerService(ILogger<AirManagerService> logger) {
            MbiAmAPI = new MBIAmAPIWrapper();
            //oQueueDrArray = new QueueDrArray(BackgroundWorkerCallBackExecuteSendData); 改用 ArrayDrVal 物件處理 int[], float[], double[]
            InitParamsCtrller.InitArrayDrBase.ForEach(o => { o.AMcallbackFunction(GetXPlaneArrayData); });
            unsafe { MbiAmAPI.init(String2Sbyte(UtilConstants.appConfig.AirManagerTag), UtilConstants.appConfig.AirManagerUDPPort); }  // Response to "C++ Wrapper"

            _logger = logger;
            _logger.LogInformation(string.Format(CultureInfo.CurrentCulture, UtilConstants.HostingServiceFormat, UtilGlobalFunc.GetOnLineTimeTick, nameof(AirManagerService)));
        }
        #endregion

        protected override async Task ExecuteAsync(CancellationToken stoppingToken) {
            while (!stoppingToken.IsCancellationRequested) {
                if (InitParamsCtrller.SubscribedDataRefs.Count != 0 && InitParamsCtrller.SubscribedCommands.Count != 0) {
                    #region 將訂閱的 DataRefs, Commands 逐筆加入 Air Manager SimBus, 並啟動 Air Manager 通訊
                    await Task.Factory.StartNew(() => { DataRefIntoAirManagerSimBus(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
                    await Task.Factory.StartNew(() => { CommandIntoAirManagerSimBus(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
                    SpinWait.SpinUntil(() => SubscribedDataRefsFinished && SubscribedCommandsFinished);
                    StartAirManagerListening();
                    #endregion

                    MbiBaseSystem.EvtInterface.UpdateAirManagerDataRefSingleVal += UpdateAirManagerDataRefSingleVal;
                    MbiBaseSystem.EvtInterface.ManualUpdateAirManagerDataRefArrayVal += ManualUpdateAirManagerDataRefArrayVal;

                    WaitingInterval = -1;// WaitingInterval = -1, 表示持續一直等.. 不會執行 while looping, 這樣才能開始等待 listening Data, 聽到就拋給 Air Manager UI
                } else {
                    WaitingInterval = 1;
                    break;
                }

                await Task.Delay(WaitingInterval, stoppingToken);
            }

            CloseAirManager();
        }

        /// <summary>
        /// Log Message
        /// </summary>
        private void Log(string msg) => UtilGlobalFunc.Log(msg);
        /// <summary>
        /// 將 DataRef, Commands 的 keyword pattern 'string' 轉成 Air Manager 可以讀取的 sbyte*
        /// <para>DataRef : "sim/cockpit/electrical/battery_on"</para>
        /// <para>Command : "sim/electrical/battery_1_on"</para>
        /// </summary>
        private unsafe sbyte* String2Sbyte(string input) {
            return String2Sbyte(Encoding.ASCII.GetBytes(input));
        }
        /// <summary>
        /// 將 DataRef 的 string value 轉成 Air Manager 可以讀取的 sbyte*
        /// <para>DataRef : string value</para>
        /// </summary>
        private unsafe sbyte* String2Sbyte(byte[] bytes) {
            unsafe {
                fixed (byte* p = bytes) {
                    sbyte* sp = (sbyte*)p;
                    return sp;
                }
            }
        }

        #region Start & Stop Air Manager
        /// <summary>
        /// 啟動 Air Manager 通訊
        /// </summary>
        private void StartAirManagerListening() {
            MbiAmAPI.start();
            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' Connected. Subscribed DataRefs({1},{2}), Commands({3},{4}), waiting '{5}' ready..."
                , UtilConstants.AirManagerSoftware
                , InitParamsCtrller.SubscribedDataRefs.Count, SubscribedDataRefsFinished
                , InitParamsCtrller.SubscribedCommands.Count, SubscribedCommandsFinished
                , UtilConstants.XPlaneSoftware));
        }
        /// <summary>
        /// 切斷 Air Manager 通訊
        /// </summary>
        private void CloseAirManager() {
            if (MbiAmAPI != null) {
                MbiAmAPI.close();
                UtilConstants.GetAirManagerStatus = false;
                MbiAmAPI = null;
                Log(string.Format(CultureInfo.CurrentCulture, "'{0}' BE closed due to both of Subscribed DataRefs({1}), Commands({2}) CAN NOT be 0."
                    , UtilConstants.AirManagerSoftware, InitParamsCtrller.SubscribedDataRefs.Count, InitParamsCtrller.SubscribedCommands.Count));
            }
        }
        #endregion

        #region DataRefs for Air Manager SimBus Lib
        /// <summary>
        /// 逐筆加入 DataRef
        /// <para>1. 將 DataRefElementBase 的 callback function 與 C++ DLL 做連結</para>
        /// <para>2. 預留 Air Manager C++ DLL Lib Memory Address</para>
        /// <para>3. 如果沒有預留到 "記憶體位置", 會發生 System.NullReferenceException 錯誤</para>
        /// </summary>
        private bool AddDataRefIntoMbiAmAPI(int CNT, DataRefElementBase drBase) {
            bool AddSuccess = false;

            try {
                unsafe { // 預留 Air Manager C++ DLL Lib Memory Address "C++ Wrapper"
                    if (drBase.IsDataRefElement) {
                        string NullMsg = "'{0}', {1}({2}) is null or 0.";
                        var dr = (DataRefElement)drBase;
                        if (dr.IsArrayIntType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyIntAddressArray)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyIntAddressArray), dr.EmptyIntAddressArray));
                            } else {
                                lock (dr.EmptyIntAddressArray)
                                { fixed (int* pValue = dr.EmptyIntAddressArray) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.intArrayType, pValue, dr.ArrayLen, dr._callbackIntArray); } }
                            }
                        } else if (dr.IsArrayByteType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyByteAddressArray)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyByteAddressArray), dr.EmptyByteAddressArray));
                            } else {
                                Log(string.Format(CultureInfo.CurrentCulture, "{0} :: 尚未支援 byte[] !!! DataRef({1}), Type({2})", UtilConstants.AirManagerSoftware, dr.RawDataRef, dr.Type));
                                /*lock (dr.EmptyByteAddressArray)
                                { fixed (byte* pValue = dr.EmptyByteAddressArray) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.byteArrayType, pValue, dr.ArrayLen, dr._callbackByteArray); } }*/
                            }
                        } else if (dr.IsArrayFloatType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyFloatAddressArray)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyFloatAddressArray), dr.EmptyFloatAddressArray));
                            } else {
                                lock (dr.EmptyFloatAddressArray)
                                { fixed (float* pValue = dr.EmptyFloatAddressArray) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.floatArrayType, pValue, dr.ArrayLen, dr._callbackFloatArray); } }
                            }
                        } else if (dr.IsArrayDoubleType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyDoubleAddressArray)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyDoubleAddressArray), dr.EmptyDoubleAddressArray));
                            } else {
                                lock (dr.EmptyDoubleAddressArray)
                                { fixed (double* pValue = dr.EmptyDoubleAddressArray) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.doubleArrayType, pValue, dr.ArrayLen, dr._callbackDoubleArray); } }
                            }
                        } else if (dr.IsSingleIntType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyIntAddress)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyIntAddress), dr.EmptyIntAddress));
                            } else {
                                lock (dr.EmptyIntAddress)
                                { fixed (int* pValue = dr.EmptyIntAddress) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.intType, pValue[0], dr._callbackInt); } }
                            }
                        } else if (dr.IsSingleByteType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyByteAddress)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyByteAddress), dr.EmptyByteAddress));
                            } else {
                                lock (dr.EmptyByteAddress)
                                { fixed (byte* pValue = dr.EmptyByteAddress) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.byteType, pValue[0], dr._callbackByte); } }
                            }
                        } else if (dr.IsSingleFloatType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyFloatAddress)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyFloatAddress), dr.EmptyFloatAddress));
                            } else {
                                lock (dr.EmptyFloatAddress)
                                { fixed (float* pValue = dr.EmptyFloatAddress) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.floatType, pValue[0], dr._callbackFloat); } }
                            }
                        } else if (dr.IsSingleDoubleType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyDoubleAddress)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyDoubleAddress), dr.EmptyDoubleAddress));
                            } else {
                                lock (dr.EmptyDoubleAddress)
                                { fixed (double* pValue = dr.EmptyDoubleAddress) { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.doubleType, pValue[0], dr._callbackDouble); } }
                            }
                        } else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElement), DataRefs.ManuallyDefinitionType, dr.Type)); }
                    } else if (drBase.IsStringDataRefElement) {
                        string NullMsg = "'{0}', {1}({2}) is null or {3} is 0.";
                        var dr = (StringDataRefElement)drBase;
                        if (dr.IsStringType) {
                            if (UtilGlobalFunc.IsArrayEmpty(dr.EmptyStringAddressByteArray)) {
                                Log(string.Format(CultureInfo.CurrentCulture, NullMsg, drBase.RawDataRef, nameof(DataRefElementBase.EmptyStringAddressByteArray), dr.EmptyStringAddressByteArray, nameof(dr.StringLength)));
                            } else {
                                lock (dr.EmptyStringAddressByteArray)
                                { AddSuccess = true; MbiAmAPI.AddVariableProvider(String2Sbyte(drBase.RawDataRef), (int)variableType.stringType, String2Sbyte(dr.EmptyStringAddressByteArray), dr._callbackString); }
                            }
                        }
                    } else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElementBase), DataRefs.SystemClassType, drBase.GetType())); }
                }
            } catch (Exception ex) { Log($"Exception ::: {ex.Message}"); }

            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "({0}). 註冊至 '{1} dll' {2} : '{3}' {4}", CNT.ToString("0000"), UtilConstants.AirManagerSoftware, UtilGlobalFunc.GetResultMsg(AddSuccess), drBase.Type, drBase.DataRef));
            return AddSuccess;
        }
        /// <summary>
        /// 將訂閱的 DataRefs 註冊到 Air Manager SimBus
        /// </summary>
        private void DataRefIntoAirManagerSimBus() {
            int CNT = 0;
            List<DataRefElementBase> AddFailDR = new List<DataRefElementBase>();
            InitParamsCtrller.SubscribedDataRefs.ForEach(o => {
                bool ResSuccess = AddDataRefIntoMbiAmAPI(++CNT, o);
                if (!ResSuccess) { AddFailDR.Add(o); }
            });

            if (AddFailDR.Count != 0) {
                AddFailDR.ForEach(o => {
                    InitParamsCtrller.SubscribedDataRefs.Remove(o);
                    Log(string.Format(CultureInfo.CurrentCulture, "註冊至 '{0}' 失敗, 直接移除, 避免註冊至 '{1}' 後, 回報失敗, 造成 AM DLL C++ object null Failure... DataRef({2}), Type({3})"
                        , UtilConstants.AirManagerSoftware, UtilConstants.XPlaneSoftware, o.DataRef, o.Type));
                });
            }
            string ResMsg = string.Format(CultureInfo.CurrentCulture, ", {0} 個失敗", AddFailDR.Count);
            Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.SubscribeDataRefSuccessMessage, UtilConstants.AirManagerSoftware, InitParamsCtrller.SubscribedDataRefs.Count, ResMsg));
            UtilConstants.GetAirManagerStatus = true;
            SubscribedDataRefsFinished = true;
        }
        #endregion


        #region Commands for Air Manager SimBus Lib
        /// <summary>
        /// 逐筆加入 Command
        /// </summary>
        private void AddCommandIntoMbiAmAPI(object command) {
            try {
                unsafe // Response to "C++ Wrapper"
                { if (command.GetType() == typeof(XPlaneCommand)) { MbiAmAPI.AddCommandProvider(String2Sbyte(((XPlaneCommand)command).Name), ((XPlaneCommand)command)._callback); } }
            } catch (Exception ex) { Log($"Exception ::: {ex.Message}"); }
        }
        /// <summary>
        /// 將訂閱的 Commands 註冊到 Air Manager SimBus
        /// </summary>
        private void CommandIntoAirManagerSimBus() {
            InitParamsCtrller.SubscribedCommands.ForEach(o => AddCommandIntoMbiAmAPI(o));
            Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.SubscribeCommandSuccessMessage, UtilConstants.AirManagerSoftware, InitParamsCtrller.SubscribedCommands.Count));
            SubscribedCommandsFinished = true;
        }
        #endregion


        #region Subscribe event handler function()
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: '單一值' int, float, double, string
        /// <para>使用情境 :::</para>
        /// <para>1. X-Plane 直接回覆更新值</para>
        /// <para>2. 由外部系統, 例如 AirForceSystem, GrpcServer 直接觸發更新 DataRef 值</para>
        /// <para>注意 !!!</para>
        /// <para>1. 這邊只有將 "單一值" 更新至 Air Manager</para>
        /// <para>2. "Array 值" 統一由 X-Plane 回覆, 專職處理的 function ::: BackgroundWorkerCallBackExecuteSendData()</para>
        /// <para>3. 如果要自行寫 Array value to AM, 請依照以下的 call function()</para>
        /// <para>3.1. SendArrayValue2AM(e.TriggerByWhichSystem, e.DataBus4Bytes, new int[] { 0, 0 });</para>
        /// <para>3.2. SendArrayValue2AM(e.TriggerByWhichSystem, e.DataBus4Bytes, new float[] { 0.0F, 0.0F });</para>
        /// <para>3.3. SendArrayValue2AM(e.TriggerByWhichSystem, e.DataBus4Bytes, new double[] { 0.0, 0.0 });</para>
        /// </summary>
        private void UpdateAirManagerDataRefSingleVal(object sender, DataRefEventArgs e)
        { if (UtilConstants.appConfig.Debug4BigMason) { DebugModeSend2AM(e); } else { ReleaseModeSend2AM(e); } }
        /// <summary>
        /// 保留 C++ DLL 存取記憶體位置出錯的問題
        /// <para>大部分都是正常, 偶而會發生存取相同 address 卻出錯, 原因不明</para>
        /// </summary>
        private void DebugModeSend2AM(DataRefEventArgs e) {
            try { ReleaseModeSend2AM(e); }
            catch (Exception ex) { Log(string.Format(CultureInfo.CurrentCulture, "{0}.{1}(), DataRef({2}), Value({3}) Occur ERROR, Exception.StackTrace({4})"
                                    , nameof(AirManagerService), nameof(UpdateAirManagerDataRefSingleVal), e.DataRefName, e.ObjVal, ex.StackTrace)); }
        }
        /// <summary>
        /// Skip 有時候 C++ DLL 存取記憶體位置出錯的問題
        /// <para>大部分都是正常, 偶而會發生存取相同 address 卻出錯, 原因不明</para>
        /// </summary>
        private void ReleaseModeSend2AM(DataRefEventArgs e) {
            if (e.DataBusIsBytes) { XPSendFloatData2AM(e, e.DataBus4Bytes.ObjVal2Float); }
            else if (e.DataBusIsString) { XPSendStringData2AM(e, e.DataBus4String.RawStringValue); }
            else { Log(string.Format(CultureInfo.CurrentCulture, "Parameters ERROR, please check it. DataRefElement({0}), StringDataRefElement({1})", e.DataBus4Bytes, e.DataBus4String)); }
        }
        /// <summary>
        /// 手動更新 "Air Manager" DataRef value :: Array值 int[], float[], double[]
        /// <para>1. X-Plane 背景自動更新, 自行由 BackgroundWorker 組裝執行完成, 因 X-Plane 分拆 array element[i] 回覆時間有時間差, 必須等待到齊後, 才能統一寫入 AM</para>
        /// <para>2. AirForceSystem / GRPC 手動更新, 自行控制 array element[i] 完整到齊後, 統一寫入 AM</para>
        /// <para>3. X-Plane 回覆可能是 Fully/Partial, 手動更新回覆, 必須是 Fully</para>
        /// <para>Array DataRef value updated 同步至 Air Manager 的 C++ Wrapper</para>
        /// </summary>
        private void ManualUpdateAirManagerDataRefArrayVal(object sender, DataRefEventArgs e) => SendArrayValue2AM(e.TriggerByWhichSystem, e.DataBus4Object, e.ArrayValue, XPlaneResponseStatusEnum.Fully);
        #endregion

        #region 送出 xxxxx Data to 'Air Manager'
        #region 送 "單一值", "Array值" 過去給 Air Manager
        /// <summary>
        /// Send Value to Air Manager ::: int, byte, float, double, string
        /// </summary>
        private void SendSingleValue2AM(TriggerByWhatSystem triggerBy, DataRefElementBase dr, object val) => SendValue2AM("SendSingleValue2AM", triggerBy, dr, val);
        /// <summary>
        /// Send Value to Air Manager ::: int[], float[], double[]
        /// </summary>
        private void SendArrayValue2AM(TriggerByWhatSystem triggerBy, DataRefElementBase dr, object val, XPlaneResponseStatusEnum xPlaneResponseStatus) => SendValue2AM("SendArrayValue2AM", triggerBy, dr, val, xPlaneResponseStatus);
        /// <summary>
        /// Response to "C++ Wrapper", Send Value to Air Manager ::: 共 8 種資料
        /// <para>int, byte, float, double, string, int[], float[], double[]</para>
        /// </summary>
        private void SendValue2AM(string FuncName, TriggerByWhatSystem triggerBy, DataRefElementBase drBase, object val, XPlaneResponseStatusEnum xPlaneResponseStatus = XPlaneResponseStatusEnum.Fully) {
            if (drBase == null) { Log(string.Format(CultureInfo.CurrentCulture, "DataRefElementBase({0}) is NULL, please check it.", drBase)); return; }
            if (!InitParamsCtrller.IsSubscribedDR(drBase.DataRef)) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefIsNotSubscribed, drBase.DataRef)); return; }

            try {
                unsafe {
                    drBase.ObjVal = val;
                    if (drBase.IsObjValSingleValue) {
                        if (drBase.IsObjValTypeInt) {
                            drBase.SendIntValue2AM = drBase.ObjVal2Int;
                            drBase.EmptyIntAddress[0] = drBase.SendIntValue2AM;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyIntAddress)) {
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyIntAddress) { fixed (int* pValue = drBase.EmptyIntAddress) { MbiAmAPI.sendInt(String2Sbyte(drBase.RawDataRef), pValue[0]); } } }
                            }
                        } else if (drBase.IsObjValTypeByte) {
                            drBase.SendByteValue2AM = drBase.ObjVal2Byte;
                            drBase.EmptyByteAddress[0] = drBase.SendByteValue2AM;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyByteAddress)) {
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyByteAddress) { fixed (byte* pValue = drBase.EmptyByteAddress) { MbiAmAPI.sendByte(String2Sbyte(drBase.RawDataRef), pValue[0]); } } }
                            }
                        } else if (drBase.IsObjValTypeFloat) {
                            drBase.SendFloatValue2AM = drBase.ObjVal2Float;//.ShrinkDigitals();
                            drBase.EmptyFloatAddress[0] = drBase.SendFloatValue2AM;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyFloatAddress)) {
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyFloatAddress) { fixed (float* pValue = drBase.EmptyFloatAddress) { MbiAmAPI.sendFloat(String2Sbyte(drBase.RawDataRef), pValue[0]); } } }
                            }
                        } else if (drBase.IsObjValTypeDouble) {
                            drBase.SendDoubleValue2AM = drBase.ObjVal2Double;//.ShrinkDigitals();
                            drBase.EmptyDoubleAddress[0] = drBase.SendDoubleValue2AM;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyDoubleAddress)) {
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyDoubleAddress) { fixed (double* pValue = drBase.EmptyDoubleAddress) { MbiAmAPI.sendDouble(String2Sbyte(drBase.RawDataRef), pValue[0]); } } }
                            }
                        } else if (drBase.IsObjValTypeString) {
                            drBase.SendStringValue2AM = drBase.ObjVal2String;
                            drBase.EmptyStringAddressByteArray = drBase.ObjVal2String2StringLengthByteArray;
                            xPlaneResponseStatus = ((StringDataRefElement)drBase).xPlaneResponseStatus; // 字串有可能是 X-Plane Partial response
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyStringAddressByteArray)) {
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyStringAddressByteArray) { MbiAmAPI.sendString(String2Sbyte(drBase.RawDataRef), String2Sbyte(drBase.EmptyStringAddressByteArray)); } }
                            }
                        }
                    } else if (drBase.IsObjValArrayValue) {
                        if (drBase.IsObjValTypeIntArray) {
                            drBase.EmptyIntAddressArray = drBase.ObjVal2IntArray;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyIntAddressArray)) {
                                drBase.SendIntArrayValue2AM = drBase.ShowSendMessage.ConvertObjectToString();
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyIntAddressArray) { fixed (int* pValue = drBase.EmptyIntAddressArray) { MbiAmAPI.sendIntArray(String2Sbyte(drBase.RawDataRef), pValue); } } }
                            }
                        } else if (drBase.IsObjValTypeFloatArray) {
                            drBase.EmptyFloatAddressArray = drBase.ObjVal2FloatArray;
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyFloatAddressArray)) {
                                drBase.SendFloatArrayValue2AM = drBase.ShowSendMessage.ConvertObjectToString();
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyFloatAddressArray) { fixed (float* pValue = drBase.EmptyFloatAddressArray) { MbiAmAPI.sendFloatArray(String2Sbyte(drBase.RawDataRef), pValue); } } }
                            }
                        } else if (drBase.IsObjValTypeDoubleArray) {
                            drBase.EmptyDoubleAddressArray = drBase.ObjVal2DoubleArray; // 官網沒有 double[] 的 DataRef 參數, 只有 double
                            if (!UtilGlobalFunc.IsArrayEmpty(drBase.EmptyDoubleAddressArray)) {
                                drBase.SendDoubleArrayValue2AM = drBase.ShowSendMessage.ConvertObjectToString();
                                if (drBase.CheckSendValueToAirManager)
                                { lock (drBase.EmptyDoubleAddressArray) { fixed (double* pValue = drBase.EmptyDoubleAddressArray) { MbiAmAPI.sendDoubleArray(String2Sbyte(drBase.RawDataRef), pValue); } } }
                            }
                        }
                    } else { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ErrorDataTypeNotSupportMessage, drBase.ObjVal.GetType())); }

                    // 是 Array Value, 且由 X-Plane Trigger 過來的, 才需要移除 QueueDrArray  改用 ArrayDrVal 物件處理 int[], float[], double[]
                    //if (drBase.IsObjValArrayValue && triggerBy == TriggerByWhatSystem.FromXPlane) { RemoveDataRefElement(drBase.RawDataRef); }

                    if (drBase.ValueSendToAM) {
                        UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.SendData2AMMsg, FuncName, string.Format(CultureInfo.CurrentCulture, "{0},{1}", triggerBy, xPlaneResponseStatus), drBase.Type, drBase.DataRef, drBase.ShowSendMessage));
                        Commands.StaticEvtInterface.OnInfoAirManagerDataRefValueUpdated(drBase, new DataRefEventArgs($"({drBase.DataRef})", drBase, triggerBy));
                    }
                }
            }
            catch (Exception ex)
            { Log(string.Format(CultureInfo.CurrentCulture, "{0}.{1}(), DataRef({2}), Value({3}) Occur ERROR, Exception.StackTrace({4})", nameof(AirManagerService), "SendValue2AM", drBase.DataRef, val, ex.StackTrace)); }
        }
        /// <summary>
        /// 已經送出, 所以清空該 DataRefName 的 Queue Data
        /// </summary>
        //private void RemoveDataRefElement(string DataRefName) { if (oQueueDrArray != null) { oQueueDrArray.RemoveDataRefElement(DataRefName); } } 改用 ArrayDrVal 物件處理 int[], float[], double[]
        #endregion

        #region 分類 "int", "byte", "float", "double", "int[]", "float[]", "double[]", "string"
        /// <summary>
        /// X-Plane 送出 'string' Data to 'Air Manager'
        /// </summary>
        private void XPSendStringData2AM(DataRefEventArgs e, string val) => SendSingleValue2AM(e.TriggerByWhichSystem, e.DataBus4String, val);
        /// <summary>
        /// X-Plane 統一給出 float Data 值, 要送給 'Air Manager' 之前, 依據 Type 轉成相對應的 DataType 值
        /// <para>1. 單一值的回覆, 直接寫入 Air Manager "C++ Wrapper"</para>
        /// <para>2. Array 值的回覆, 因為 Array 不一定全部一次到齊, 或是有順序的到齊, 所以要先 Queue 住後, 全部到齊且排序後, 再送給 'Air Manager'</para>
        /// <para>3. 從 X-Plane 取得的回覆, Array 已經被分拆成 n 的 DataRef, 所以 Type 屬性不會夾帶 [] 的符號</para>
        /// </summary>
        private void XPSendFloatData2AM(DataRefEventArgs e, float val) {
            var oDR = e.DataBus4Bytes;
            if (oDR.DataRefIsArray) {
                // 處理 Array 值的回覆, 因為 Array 不一定全部一次到齊, 或是有順序的到齊,
                // 所以要先 Queue 住後, 全部到齊且排序完成, 再一併送給 'Air Manager' (在 BackgroundWorkerCallBackExecuteSendData() 執行)
                if (oDR.IsSingleCharType)
                { Log(string.Format(CultureInfo.CurrentCulture, "'{0}' is part of string, do NOT need to queue, '{1}' take care it.", oDR.DataRef, nameof(StringDataRefElement))); }
                else {
                    // 只有 int[], float[], double[] 會進入, 先記錄到 QueueDrArray BackgroundWorker 那邊, 到齊後再 CallBack 回來
                    //oQueueDrArray.AddDataRefElement(oDR);  改用 ArrayDrVal 物件處理 int[], float[], double[]
                    foreach (var o in InitParamsCtrller.InitArrayDrBase) {
                        if (o != null && o.DrBase.RawDataRef.Equals(oDR.RawDataRef))
                        { o.FillinValue(oDR.ArrayIdx, oDR.ObjVal); break; }
                    }
                }
            } else {
                // 單一值的回覆 ::: int, byte, float, double
                if (oDR.IsSingleIntType) { SendSingleValue2AM(e.TriggerByWhichSystem, oDR, val.ConvertFloatToInt32()); }
                else if (oDR.IsSingleByteType) { SendSingleValue2AM(e.TriggerByWhichSystem, oDR, val.ConvertFloatToByte()); }
                else if (oDR.IsSingleFloatType) { SendSingleValue2AM(e.TriggerByWhichSystem, oDR, val); }
                else if (oDR.IsSingleDoubleType) { SendSingleValue2AM(e.TriggerByWhichSystem, oDR, val.ConvertFloatToDouble()); }
                else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElement), DataRefs.ManuallyDefinitionType, oDR.Type)); }
            }
        }

        /// <summary>
        /// 從 X-Plane 取得 int[], float[], double[] 的 Response data
        /// <para>X-Plane 回覆可能是 Fully/Partial, 手動更新回覆, 必須是 Fully</para>
        /// </summary>
        private void GetXPlaneArrayData(DataRefElementBase DrBase, XPlaneResponseStatusEnum xPlaneResponseStatus, int[] intAry, float[] floatAry, double[] doubleAry) {
            TriggerByWhatSystem triggerBy = TriggerByWhatSystem.FromXPlane;
            if (DrBase.IsArrayIntType) { SendArrayValue2AM(triggerBy, DrBase, intAry, xPlaneResponseStatus); }
            else if (DrBase.IsArrayFloatType) { SendArrayValue2AM(triggerBy, DrBase, floatAry, xPlaneResponseStatus); }
            else if (DrBase.IsArrayDoubleType) { SendArrayValue2AM(triggerBy, DrBase, doubleAry, xPlaneResponseStatus); }
        }
        /* 改用 ArrayDrVal 物件處理 int[], float[], double[]
        /// <summary>
        /// 先將 X-Plane 回覆的全部 DataRef Queue 在 BackgroundWorker, 等收集完所有的 Array Elements 且 Sorting 後, 再從這邊 Send 過去給 "C++ Wrapper"
        /// <para>1. 確定全部 Array Elements 到齊</para>
        /// <para>2. 排序完成後, 由 float[] 為基礎, 再依據該 DataRef 的 Type 轉置成 int[], double[] 後送出</para>
        /// <para>3. ArrayDR 可能會超過定義的數量</para>
        /// </summary>
        private void BackgroundWorkerCallBackExecuteSendData(DataRefElement[] ArrayDR, DataRefElementBase SrcDR) {
            if (SrcDR == null) { Log(string.Format(CultureInfo.CurrentCulture, "DataRefElementBase({0}) is null.", SrcDR)); return; }
            if (ArrayDR == null) { Log(string.Format(CultureInfo.CurrentCulture, "DataRefElement[]({0}) is null.", ArrayDR)); return; }

            DataRefElement oDR = ArrayDR.FirstOrDefault();
            TriggerByWhatSystem triggerBy = TriggerByWhatSystem.FromXPlane;
            if (oDR != null && !string.IsNullOrEmpty(oDR.Type)) {
                int SendCounter = 0;
                for (int i = 0; i < ArrayDR.Length; i += SrcDR.ArrayLen) {
                    IEnumerable<DataRefElement> drList = ArrayDR.Skip(i).Take(SrcDR.ArrayLen);

                    if (drList.Count() != 0) {
                        SendCounter++;
                        string SendMsg = string.Empty;

                        if (drList.Count() == SrcDR.ArrayLen) {
                            float[] ArrayVal = drList.OrderBy(d => d.ArrayIdx).Select(d => d.ObjVal2Float).ToArray();
                            if (SrcDR.IsArrayIntType || SrcDR.IsArrayByteType) { // int[], byte[] 共用
                                SendArrayValue2AM(triggerBy, SrcDR, ArrayVal.ConvertFloatArrayToIntArray());
                            } else if (SrcDR.IsArrayFloatType) { // float[]
                                SendArrayValue2AM(triggerBy, SrcDR, ArrayVal);
                            } else if (SrcDR.IsArrayDoubleType) {// double[]
                                SendArrayValue2AM(triggerBy, SrcDR, ArrayVal.ConvertFloatArrayToDoubleArray());
                            } else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElement), DataRefs.ManuallyDefinitionType, SrcDR.Type)); }
                            SendMsg = "完整的 Elements, 這次需要傳送給";
                        } else {
                            SendMsg = "有缺少 Elements, 這次不需要傳送給";
                        }
                        UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}() SendCounter({1}), DataRef({2}), 總共({3}), 表定({4}), 這次({5}), {6} {7}"
                                                        , nameof(BackgroundWorkerCallBackExecuteSendData), SendCounter.ToString("00"), SrcDR.RawDataRef
                                                        , ArrayDR.Length, SrcDR.ArrayLen, drList.Count(), SendMsg, UtilConstants.AirManagerSoftware));
                    }
                }
            } else {
                string tpType = (oDR != null) ? oDR.Type : string.Empty;
                Log(string.Format(CultureInfo.CurrentCulture, "{0}() Occur Error, oDR({1}) is null or Type(2) is null", nameof(BackgroundWorkerCallBackExecuteSendData), oDR, tpType));
            }
        }*/
        #endregion
        #endregion

    }
}
