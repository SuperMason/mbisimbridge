﻿using MBILibrary.Util;
using XPlaneConnector.DataSwitch.MbiEvtInterfaces;

namespace MBISimBridge.Services
{
    public abstract class MbiBaseService
    {
        /// <summary>
        /// Global Event Interface for subscribing Event Handler
        /// </summary>
        public static EvtInterfaceService EvtInterface { get; set; }

        /// <summary>
        /// 統一紀錄 MbiBaseService 的 Log
        /// </summary>
        protected void Log(string msg) => UtilGlobalFunc.Log(msg);

    }
}