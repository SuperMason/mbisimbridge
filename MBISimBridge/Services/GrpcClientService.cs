﻿using MBIClient;
using MBILibrary.Util;
using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using XPlaneConnector.DataRef;
using System.Globalization;
using System.Threading;

namespace MBISimBridge.Services
{
    /// <summary>
    /// 將 'Air Manager' 或 'X-Plane' 的訊息, 透過 gRPC Client 傳送給 MBIServer
    /// </summary>
    public class GrpcClientService : MbiBaseService
    {
        #region fields
        /// <summary>
        /// 紀錄 Send to MBIServer Failed, 等再度連線後, 重新寄送一次
        /// </summary>
        private readonly ConcurrentQueue<Dictionary<SendGrpcTypeEnum, MessageEventArgs>> Queue4ProcessFailed = new ConcurrentQueue<Dictionary<SendGrpcTypeEnum, MessageEventArgs>>();
        #endregion

        /// <summary>
        /// Queue4ProcessFailed 已經清空
        /// </summary>
        private bool IsEmptyQueueFailed => Queue4ProcessFailed.Count == 0;
        /// <summary>
        /// 控制處理 Queue 的 Flag, 如果 超過 WaitProcessQueueFailed, 就先停止執行, 等下一 run 再繼續
        /// </summary>
        private bool StopProcessQueueFailed = false;
        private TimeSpan WaitProcessQueueFailed = TimeSpan.FromMilliseconds(500);

        /// <summary>
        /// MBIServer 已連線
        /// </summary>
        private bool MBIServerIsAlive => GrpcClientChannel.IsGrpcChannelAlive && GrpcClientChannel.GrpcServerIsRunning;

        /// <summary>
        /// 將 'Air Manager' 或 'X-Plane' 的訊息, 透過 gRPC Client 傳送給 MBIServer
        /// </summary>
        public GrpcClientService() {
            EvtInterface.XPlaneConnected += OnXPlaneConnected;
            EvtInterface.UpdateAirManagerDataRefSingleVal += ReadLatestDataRefSingleVal;

            #region 與 Air Manager 互動
            EvtInterface.ExecuteAirManagerCommand += ExecuteAirManagerCommand;
            EvtInterface.ExecuteAirManagerDataRefWrite += ExecuteAirManagerDataRefWrite;//從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
            #endregion

            Task.Factory.StartNew(() => { DoWorkAsyncInfiniteLoop(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);// 監控是否有 Grpc 訊息需要傳送 ?
        }

        #region 與 X-Plane 互動
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        private void OnXPlaneConnected(object serder, MessageEventArgs e)
        {
            _ = LaunchGRPC(SendGrpcTypeEnum.XPlaneConnected, e);
        }
        /// <summary>
        /// Listen DataRef value updated :: '單一值' int, float, double, string
        /// <para>只 Read data, 並回報給 Grpc Server</para>
        /// </summary>
        private void ReadLatestDataRefSingleVal(object sender, DataRefEventArgs e)
        {
            _ = LaunchGRPC(SendGrpcTypeEnum.XPlaneReadDataRef, e);
        }
        #endregion

        #region 與 Air Manager 互動
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        private void ExecuteAirManagerCommand(object serder, CommandEvevntArgs e)
        {
            _ = LaunchGRPC(SendGrpcTypeEnum.AirManagerExecuteCommand, e);
        }
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'GrpcClientService'
        /// <para>byte, float, int, bool, double 的更新, 皆以 float 帶入 X-Plane</para>
        /// <para>實際測試結果為 ::: 如果 SPEC 定義 type 為 int, bool 時, 要帶入 float 才能順利寫入值</para>
        /// </summary>
        private void ExecuteAirManagerDataRefWrite(object sender, DataRefEventArgs e) {
            if (e == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefEventArgs), e)); return; }
            if (e.DataBus4Object == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefElementBase), e.DataBus4Object)); return; }

            if (e.DataRefIsWritable) {
                if (e.ObjCheck != null) {
                    bool WriteIt = false;

                    if (e.DataBus4ObjectIsBytes)
                    { WriteIt = true; }
                    else if (e.DataBus4ObjectIsString)
                    { WriteIt = true; }

                    if (WriteIt) {
                        //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueInfo, GetType().Name, e.TriggerByWhichSystem, UtilConstants.XPlaneSoftware, e.DataRefName, e.DataRefType, e.DataRefObjValShowString));
                        _ = LaunchGRPC(SendGrpcTypeEnum.AirManagerExecuteDataRefWrite, e);
                    }
                }
            } else { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefCanNotWrittenMessage, e.DataRefName, UtilConstants.Flag_Yes)); }
        }
        #endregion


        /// <summary>
        /// 透過 "gRPC Client" Send message to MBIServer
        /// <para>true : 訊息成功送出</para>
        /// <para>false : 訊息無法送出, 暫時 cache 在本機</para>
        /// </summary>
        private bool LaunchGRPC(SendGrpcTypeEnum sendType, MessageEventArgs e) {
            if (MBIServerIsAlive) {
                #region "gRPC Server" is running, 可以執行訊息傳送, using GrpcClientSender()
                using (var gClient = new GrpcClientSender(sendType, e)) {
                    //Log(string.Format(CultureInfo.CurrentCulture, "Summary Received '{0}' Response :: Success({1}), Failure({2}), gRPCClientID({3}), SendResultMessage({4}), EventDataMessage({5})"
                    //    , UtilConstants.appConfig.gRPCServerName, GrpcClientSender.GetGrpcServerSuccessResponseCNT, GrpcClientSender.GetGrpcServerFailureResponseCNT
                    //    , gClient.Id, gClient.SendResultMessage, gClient.EventDataMessage));

                    if (gClient.SendStatus == SendGrpcResultEnum.Failure) {
                        if (!e.Data.StartsWith(UtilConstants.GrpcReSendEventDataMessageHeader))
                        { e.Data = UtilConstants.GrpcReSendEventDataMessageHeader + e.Data; }

                        CacheFailedMessage(sendType, e);
                        return false;// 沒有成功送達 gRPC Server, 可能是 Server 斷線, 所以先將 Message Log 起來, 等待連線成功後, 再次重送
                    } else {
                        return true;// 成功送達 gRPC Server
                    }
                }
                #endregion
            } else {
                //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}({1}) NOT Ready, before Enabled, MESSAGE will keep at Local :: {2}", UtilConstants.appConfig.gRPCServerName, UtilConstants.GrpcServer, e.Data));
                CacheFailedMessage(sendType, e);
                return false;
            }
        }

        /// <summary>
        /// 紀錄 Send to Server Failed, 等再度連線後, 重新寄送一次
        /// <para>Queue 訊息有上限筆數, 避免記憶體塞爆, 上限值為 UtilConstants.Queue4ReSendGrpcMsgMaxCNT</para>
        /// </summary>
        private void CacheFailedMessage(SendGrpcTypeEnum sendType, MessageEventArgs e) {
            if (e == null) { Log(string.Format(CultureInfo.CurrentCulture, "CacheFailedMessage() {0} is null :: {1}", nameof(MessageEventArgs), e)); return; }

            if (Queue4ProcessFailed.Count < UtilConstants.Queue4ReSendGrpcMsgMaxCNT) {
                if (!string.IsNullOrEmpty(e.Data)) {
                    Queue4ProcessFailed.Enqueue(new Dictionary<SendGrpcTypeEnum, MessageEventArgs> { { sendType, e } });
                    string msg = string.Format(CultureInfo.CurrentCulture, "Send Message to '{0}' Failed, QueueMax({1}) Message No.{2} at Local, if Connected, ReSend Message : {3}"
                                        , UtilConstants.MBIServer, UtilConstants.Queue4ReSendGrpcMsgMaxCNT, Queue4ProcessFailed.Count, e.Data);
                    UtilGlobalFunc.ShowMsgByDebug4BigMason(msg);
                } else { Log(string.Format(CultureInfo.CurrentCulture, "{0} Data is null or empty.", nameof(MessageEventArgs))); }
            }
        }


        #region 監控是否有 Grpc 訊息需要傳送 ?
        /// <summary>
        /// 每 0.001 秒執行, 監控是否有 Grpc 訊息需要傳送 ?
        /// </summary>
        private async void DoWorkAsyncInfiniteLoop(int DelayMS = 1) {
            string ThreadHeader = string.Format(CultureInfo.CurrentCulture, "ThreadID({0}) ::: ", Thread.CurrentThread.ManagedThreadId);
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}與 {1} 已連線, 開始監控是否有 Grpc 訊息需要傳送 ?", ThreadHeader, UtilConstants.MBIServer));

            while (true) {
                SpinWait.SpinUntil(() => MBIServerIsAlive);
                if (!IsEmptyQueueFailed) {
                    UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}有 Grpc 訊息需要傳送...... CNT({1})", ThreadHeader, Queue4ProcessFailed.Count));
                    TaskRun4DeQueue4ProcessFailed();
                    SpinWait.SpinUntil(() => IsEmptyQueueFailed, WaitProcessQueueFailed);
                    ResetAndBreakTaskRunning4Queue4ProcessFailed();
                }
                await Task.Delay(DelayMS); // 每 0.001 秒 掃描是否有物件需要處理
            }
        }
        /// <summary>
        /// 用來停止 "專門處理 Queue4ProcessFailed" 的 Task.Factory.StartNew()
        /// </summary>
        private void ResetAndBreakTaskRunning4Queue4ProcessFailed() {
            StopProcessQueueFailed = true;
            SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(3));
            StopProcessQueueFailed = false; // reset for 下一次 run
        }
        private void TaskRun4DeQueue4ProcessFailed() {
            Task.Factory.StartNew(() => {
                //int ProcessCNT = 0;
                while (Queue4ProcessFailed.TryDequeue(out Dictionary<SendGrpcTypeEnum, MessageEventArgs> QueueElement)) {
                    //++ProcessCNT;
                    ExecuteReSendMessage(QueueElement);// 取當下頭一個 element 寫入 X-Plane
                    if (StopProcessQueueFailed || IsEmptyQueueFailed) {
                        //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format("該次總共處理 {0} 個", ProcessCNT));
                        break;
                    }
                }
            }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
        }
        /// <summary>
        /// 執行重送 Grpc 訊息
        /// </summary>
        private void ExecuteReSendMessage(Dictionary<SendGrpcTypeEnum, MessageEventArgs> QueueElement) {
            foreach (var kk in QueueElement) {
                if (kk.Value != null) {
                    bool ReSendOK = LaunchGRPC(kk.Key, kk.Value);
                    if (ReSendOK) {
                        UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "ReSend Message to {0} Success, QueueMax({1}) Message Counter({2}) at Local. Message ::: {3}"
                                                    , UtilConstants.MBIServer, UtilConstants.Queue4ReSendGrpcMsgMaxCNT, Queue4ProcessFailed.Count, kk.Value.Data));
                    }
                }
            }
        }
        #endregion

    }
}
