﻿using MBILibrary.Enums;
using Newtonsoft.Json;
using System.Threading;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using System.Globalization;

namespace MBISimBridge.Services
{
    /// <summary>
    /// 負責處理 RESTful call 外部 APIs
    /// </summary>
    public class RESTClientService : MbiBaseService
    {
        #region constructor
        /// <summary>
        /// 負責處理 RESTful call 外部 APIs
        /// </summary>
        public RESTClientService() {
            #region 檢查是否註冊
            EvtInterface.CheckHostRegistrationStatus += _CheckHostRegistrationStatus;
            EvtInterface.Signup += _Signup;
            EvtInterface.UnSignup += _UnSignup;
            #endregion

            #region 把交易過程的 Log, 紀錄在 cloud 上
            EvtInterface.RegisterWrote += _RegisterWrote;
            EvtInterface.RegisterReaded += _RegisterReaded;
            EvtInterface.TestPatternWrote += _TestPatternWrote;
            EvtInterface.AbandonSession += _AbandonSession;
            #endregion
        }
        #endregion


        #region event handler, 處理 從 EvtInterfaceService fire 過來的 event (被動) (主動)
        #region 檢查是否已註冊
        /// <summary>
        /// 由 RESTClientService 去問是否已註冊 ?
        /// </summary>
        private void _CheckHostRegistrationStatus(object serder, MessageEventArgs e) {
            // request to cloud ::: parameter => e.RegistrationInfo.HostLicenseKey, e.RegistrationInfo.HostName
            Thread.Sleep(1); // 假裝去問是否已經註冊的 APIs 所花的時間...
            EvtInterface.OnCheckingHostRegistrationStatus.Set(); // done, 檢查完是否已經註冊

            bool registerResult = true;// 先模擬 "有註冊" 的情況... DateTime.Now.Second;
            e.Data = registerResult ? "Check Registered! Registration Info. here :" : $"Sorry, '{e.RegistrationInfo.HostLicenseKey}' not Registered. You should SignUp first!";
            _CheckRegistered(registerResult, e);
        }
        /// <summary>
        /// 檢查註冊結果
        /// <para>registerResult :: 模擬假裝註冊結果, 偶數(已註冊), 奇數(未註冊)</para>
        /// </summary>
        /// <param name="registerResult">模擬假裝註冊結果, 偶數(已註冊), 奇數(未註冊)</param>
        /// <param name="e"></param>
        private void _CheckRegistered(bool registerResult, MessageEventArgs e) {
            Log(e.Data);
            if (registerResult)
            { EvtInterface.OnHostRegistered(this, e); }
            else
            { EvtInterface.OnHostNotRegistered(this, e); }
        }
        #endregion

        #region 進行註冊, 解除註冊
        /// <summary>
        /// 進行註冊
        /// </summary>
        private void _Signup(object serder, MessageEventArgs e) {
            // request to cloud ::: parameter => e.RegistrationInfo.HostLicenseKey, e.RegistrationInfo.HostName
            Thread.Sleep(1); // 模擬進行註冊行為
            EvtInterface.OnSigningUp.Set(); // 結束 "註冊" 行為

            bool registerResult = true;// 先模擬 "有註冊" 的情況... DateTime.Now.Second;
            string res = registerResult ? "Success" : "Failure";
            e.Data = string.Format(CultureInfo.CurrentCulture, "SignUp {0} !", res);
            _CheckRegistered(registerResult, e);
        }
        /// <summary>
        /// 解除註冊
        /// </summary>
        private void _UnSignup(object serder, MessageEventArgs e) {
            Thread.Sleep(1);
            EvtInterface.OnUnSigningUp.Set(); // 結束 "解除註冊" 行為
            Log("Successfully un-SignUp!");
        }
        #endregion


        #region 把交易過程的 Log, 紀錄在 cloud 上
        /// <summary>
        /// 已經完成將 Register Write to FPGA, 所以紀錄在 cloud 上
        /// </summary>
        private void _RegisterWrote(object serder, MessageEventArgs e) {
            _LogAction(e, null, nameof(_RegisterWrote), SessionStatusEnum.Done);
            e.Data = $"{nameof(_RegisterWrote)} Session updated";
            EvtInterface.OnSessionAlreadyIdle(this, e);
            EvtInterface.OnWriteRegisterSessionUpdated(this, e);
        }
        /// <summary>
        /// 已經完成將 Register Read from FPGA, 所以紀錄在 cloud 上
        /// </summary>
        private void _RegisterReaded(object serder, MessageEventArgs e) {
            _LogAction(e, e.ParamReaded, nameof(_RegisterReaded), SessionStatusEnum.Done);
            e.Data = $"{nameof(_RegisterReaded)} Session updated";
            EvtInterface.OnSessionAlreadyIdle(this, e);
            EvtInterface.OnReadRegisterSessionUpdated(this, e);
        }
        /// <summary>
        /// 已經完成將 Test Pattern 打至螢幕上, 所以紀錄在 cloud 上
        /// </summary>
        private void _TestPatternWrote(object sender, MessageEventArgs e) {
            _LogAction(e, null, nameof(_TestPatternWrote), SessionStatusEnum.Done);
            e.Data = $"{nameof(_TestPatternWrote)} Session updated";
            EvtInterface.OnSessionAlreadyIdle(this, e);
            EvtInterface.OnWritePatternSessionUpdated(this, e);
        }
        /// <summary>
        /// 有作業正在執行, 所以捨棄掉該次 Request, 並記錄在 cloud 上
        /// </summary>
        private void _AbandonSession(object sender, MessageEventArgs e) => _LogAction(e, null, nameof(_AbandonSession), SessionStatusEnum.Abandon);
        /// <summary>
        /// 紀錄該次行為
        /// </summary>
        private void _LogAction(MessageEventArgs e, IResponseBody responseBody, string FuncName, SessionStatusEnum sessionStatus) {
            Log(e.Data);
            ResponseEntity resp = new ResponseEntity {
                DeviceId = e.HostLicenseKey,
                SessionId = e.SessionId,
                SessionStatus = sessionStatus.ToString(),
                ResponseBody = responseBody
            };
            string jsonstr = JsonConvert.SerializeObject(resp);
            Log($"Log '{FuncName}' and POST to cloud Done :: {jsonstr}");
        }
        #endregion
        #endregion
    }
}