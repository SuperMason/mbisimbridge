﻿using MBILibrary.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
using System.Linq;
using System.Threading;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace MBISimBridge.Services
{
    /// <summary>
    /// 負責 WebSocket 通訊作業
    /// </summary>
    public class WSClientServcie : MbiBaseService
    {
        #region 模擬交易行為
        public const string WriteRegister = "write register";
        public const string ReadRegister = "read register";
        public const string WritePattern = "write pattern";
        public const string ReadRegistrationInfo = "read registrationInfo";

        public const string ActionWrite = "write";
        public const string ActionRead = "read";

        public const string ActionPropertyRegister = "register";
        public const string ActionPropertyPattern = "pattern";
        public const string ActionPropertyRegistrationInfo = "registrationInfo";
        #endregion


        #region fields
        /// <summary>
        /// 本機的註冊資訊
        /// <para>保存由 AirManagerSystem 傳過來的 RegistrationInfo</para>
        /// </summary>
        private RegistrationInfo mRegistrationInfo = null;
        /// <summary>
        /// 當有事情要做時, Lock 住, 避免行為被覆蓋掉
        /// <para>false : Session Locked for doing something.</para>
        /// <para>true : Session Release non-Locked</para>
        /// </summary>
        private bool mSessionIdle = true;
        #endregion


        #region constructor
        /// <summary>
        /// 負責 WebSocket 通訊作業
        /// </summary>
        public WSClientServcie() {
            EvtInterface.ConnectWS += _ConnectWS;
            EvtInterface.CloseWS += _CloseWS;
            EvtInterface.SendWSConnectMessage += _SendWSConnectMessage;
            EvtInterface.SessionAlreadyIdle += _SessionAlreadyIdle;
        }
        #endregion

        #region public member functions, 這個 class 的專責工作... (主動處理後, 再透過 EvtInterfaceService fire evnet 給別人處理結果)

        #region 模擬交易行為
        /// <summary>
        /// 模擬交易行為
        /// <para>1. "write register"</para>
        /// <para>2. "read register"</para>
        /// <para>3. "write pattern"</para>
        /// </summary>
        private string MockupJSONMessage(string msg) {
            if (msg.Equals(WriteRegister)) {
                return "{'action':'" + ActionWrite + "','actionProperty':'" + ActionPropertyRegister + "','sessionId':'aaa-ccc','hostLicenseKey':'" + UtilConstants.UniqueKeyOfMachine + "','propertyBody':{'param1':'0','param2':'0','param3':'0'}}";
            } else if (msg.Equals(ReadRegister)) {
                return "{'action':'" + ActionRead + "','actionProperty':'" + ActionPropertyRegister + "','sessionId':'aaa-ccc','hostLicenseKey':'" + UtilConstants.UniqueKeyOfMachine + "','propertyBody': ['param1','param2','param3']}";
            } else if (msg.Equals(WritePattern)) {
                return "{'action':'" + ActionWrite + "','actionProperty':'" + ActionPropertyPattern + "','sessionId':'aaa-ccc','hostLicenseKey':'" + UtilConstants.UniqueKeyOfMachine + "','propertyBody': [{'layer':'bottom','x-coordinate':'0','y-coordinate':'0','width':'100','height': '100','sole-color': 'R','sole-grayscale': '4'},{'layer': 'top','x-coordinate': '50','y-coordinate': '25','width': '50','height': '15','sole-color': 'W','sole-grayscale': '0'}]}";
            } else {
                return "";
            }
        }
        /// <summary>
        /// 模擬交易行為
        /// <para>1. "write register"</para>
        /// <para>2. "read register"</para>
        /// <para>3. "write pattern"</para>
        /// </summary>
        public void MockupMsgReceived(string msg) {
            string _msg = MockupJSONMessage(msg);
            if (string.IsNullOrEmpty(_msg)) { Log("Invalid message."); return; }

            MessageEventArgs msgEvt = MessageToMessageEventArgs(_msg);
            if (msgEvt.Action != "connect" && mRegistrationInfo != null && msgEvt.HostLicenseKey.Equals(mRegistrationInfo.HostLicenseKey)) {
                msgEvt.RegistrationInfo = mRegistrationInfo;

                if (mSessionIdle) {
                    mSessionIdle = false; // Session Locked for doing something.

                    if (msgEvt.Action.Equals(ActionWrite) && msgEvt.ActionProperty.Equals(ActionPropertyRegister)) {
                        msgEvt.Data = "FPGA Register 寫入";
                        EvtInterface.OnWriteRegister(this, msgEvt);
                    } else if (msgEvt.Action.Equals(ActionRead) && msgEvt.ActionProperty.Equals(ActionPropertyRegister)) {
                        msgEvt.Data = "FPGA Register 讀出";
                        EvtInterface.OnReadRegister(this, msgEvt);
                    } else if (msgEvt.Action.Equals(ActionWrite) && msgEvt.ActionProperty.Equals(ActionPropertyPattern)) {
                        msgEvt.Data = "將 Test Pattern 打至螢幕上";
                        EvtInterface.OnWriteTestPattern(this, msgEvt);
                    } else {
                        mSessionIdle = true; // 該行為不是規定內的, 所以不理會, skip it
                    }
                } else {
                    msgEvt.Data = "有作業正在執行, 所以捨棄掉該次 Request, 並記錄在 cloud 上";
                    EvtInterface.OnAbandonSession(this, msgEvt);
                }
            } else {
                Log(string.Format(CultureInfo.CurrentCulture, "WebSocket 無法成功連線, 不能執行 {0} {1}", msgEvt.Action, msgEvt.ActionProperty));
            }
        }
        private MessageEventArgs MessageToMessageEventArgs(string msg) {
            JToken outer = JToken.Parse(msg);
            string p = outer.SelectToken("propertyBody").ToString();
            outer["propertyBody"].Parent.Remove();
            MessageEventArgs msgEvt = JsonConvert.DeserializeObject<MessageEventArgs>(outer.ToString());
            msgEvt.PropertyBody = p;
            if (msgEvt.Action.Equals(ActionWrite) && msgEvt.ActionProperty.Equals(ActionPropertyPattern))
            { msgEvt.Patterns = JsonConvert.DeserializeObject<PatternProperty[]>(msgEvt.PropertyBody).OfType<PatternProperty>().ToList(); }

            if (msgEvt.Action.Equals(ActionRead) && msgEvt.ActionProperty.Equals(ActionPropertyRegister))
            { msgEvt.ParamToRead = JsonConvert.DeserializeObject<string[]>(msgEvt.PropertyBody).OfType<string>().ToList(); }

            if (msgEvt.Action.Equals(ActionWrite) && msgEvt.ActionProperty.Equals(ActionPropertyRegister))
            { msgEvt.ParamToWrite = JsonConvert.DeserializeObject<ParamProperty>(msgEvt.PropertyBody); }

            if (msgEvt.Action.Equals(ActionRead) && msgEvt.ActionProperty.Equals(ActionPropertyRegistrationInfo))
            { msgEvt.RegistrationInfo = JsonConvert.DeserializeObject<RegistrationInfo>(msgEvt.PropertyBody); }

            return msgEvt;
        }
        #endregion


        /// <summary>
        /// WebSocket 連線發生錯誤
        /// </summary>
        private void OnWSConnectionError(MessageEventArgs e) {
            EvtInterface.OnWSConnecting.Set();
            e.Data = "WebSocket OnWSConnectionError() with 連線發生錯誤";
            EvtInterface.OnWSConnectionError(this, e);
        }
        /// <summary>
        /// WebSocket 連線已經關閉
        /// </summary>
        private void OnWSConnectionClosed(MessageEventArgs e) {
            EvtInterface.OnWSConnecting.Set();
            e.Data = "WebSocket OnWSConnectionClosed() with 連線已經關閉";
            EvtInterface.OnWSConnectionClosed(this, e);
        }
        /// <summary>
        /// WebSocket 連線成功
        /// </summary>
        private void OnWSConnected(MessageEventArgs e) {
            EvtInterface.OnWSConnecting.Set(); // Websocket Connected!
            EvtInterface.OnWSConnected(this, e);
        }
        /// <summary>
        /// WebSocket 由 Watch Dog 偵測到斷線了, 馬上自行主動再次連線上去..
        /// </summary>
        private void ReConnect(MessageEventArgs e) {
            e.Data = "ReConnect by watch dog.";
            EvtInterface.OnConnectWS(this, e);
            EvtInterface.OnWSConnecting.WaitOne(); // 等待 WebSocket 連線ing...
        }
        /// <summary>
        /// WebSocket 連線成功後, 馬上送一個訊息給 Cloud, 告知 Host 連線資訊
        /// </summary>
        private void _SendWSConnectMessage(object serder, MessageEventArgs e) {
            mRegistrationInfo = e.RegistrationInfo;
            string s = ConnectMessageToString(e.Cmsg);
            Log("WebSocket Message sent as follow ::");
            Log(s); // ws.send(s);//已經與 Cloud 的 WebSocket 連線成功... 可以開始接收/傳送訊息!
            Log("WebSocket connect to Cloud, start Sending / Receiving message.");
        }
        /// <summary>
        /// WebSocket 連線成功後, 馬上送一個訊息給 Cloud, 告知 Host 連線資訊
        /// </summary>
        private string ConnectMessageToString(ConnectMessage cmsg) {
            MessageToWS msgToWS = new MessageToWS(JsonConvert.SerializeObject(cmsg));
            return JsonConvert.SerializeObject(msgToWS);
        }

        private void Ping() => Log("Ping WS ...");
        #endregion


        #region event handler, 處理 從 EvtInterfaceService fire 過來的 event (被動) (主動)
        /// <summary>
        /// Release Session Locked
        /// </summary>
        private void _SessionAlreadyIdle(object serder, MessageEventArgs e) => mSessionIdle = true;
        /// <summary>
        /// 進行 WebSocket 連線, 可能連線成功, 也可能連線失敗
        /// </summary>
        private void _ConnectWS(object serder, MessageEventArgs e) {
            Thread.Sleep(1); // 假裝進行 WebSocket 連線...

            bool wsConnResult = true; // 先模擬 "連線成功" 的情況... DateTime.Now.Second;
            if (wsConnResult) { // 模擬 :: 連線成功
                e.Data = "WebSocket Connected !";
                Log(e.Data);
                OnWSConnected(e);
            } else { // 模擬 :: 連線失敗
                e.Data = "something error on WebSocket Connecting !";
                Log(e.Data);
                OnWSConnectionError(e);
            }
        }
        /// <summary>
        /// 關閉 WebSocket
        /// </summary>
        private void _CloseWS(object serder, MessageEventArgs e) {
            Log(e.Data);
            Log("Watchdog closed!");
            Log("Ping timer closed!");
            Log("Websocket closed!");
        }
        #endregion
    }
}