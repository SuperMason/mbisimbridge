﻿using MBIClient;
using MBILibrary.Util;
using MBISimBridge.MbiSystems;
using MBISimBridge.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataSwitch.MbiEvtInterfaces;
using XPlaneConnector.Instructions;

namespace MBISimBridge
{
    class Program
    {
        /// <summary>
        /// 初始化系統參數
        /// </summary>
        private static InitParamsCtrller InitParams = new InitParamsCtrller();

        #region services :: "EventHandler Interface", "RESTful", "WebSocket"
        /// <summary>
        /// Global Event Interface for subscribing Event Handler
        /// </summary>
        private static EvtInterfaceService evtInterface = new EvtInterfaceService();
        private static int initOK = _Initialize();

        /// <summary>
        /// 負責處理 RESTful call 外部 APIs
        /// </summary>
        private static RESTClientService restClientService = new RESTClientService();
        /// <summary>
        /// 負責 WebSocket 通訊作業
        /// </summary>
        private static WSClientServcie wsClientService = new WSClientServcie();
        #endregion

        #region Outside System Instances
        /// <summary>
        /// 負責處理與 "Air Manager" 溝通的介面
        /// <para>有註冊, 且 WebSocket 已連線後, 才會啟動 X-Plane instance, 進行連線溝通</para>
        /// </summary>
        private static AirManagerSystem airManagerSystem = new AirManagerSystem();
        /// <summary>
        /// [測試用] 負責將 "TestPattern 打出至螢幕上"
        /// </summary>
        private static TestPatternSystem testPatternSystem = new TestPatternSystem();
        /// <summary>
        /// [測試用] 負責 FPGA Register Read/Write
        /// </summary>
        private static FpgaRegisterSystem fpgaRegisterSystem = new FpgaRegisterSystem();
        #endregion

        private static int _Initialize() {
            new Thread(new ThreadStart(new UtilThreadPool().MonitorThreadPool)).Start();
            //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Network Status ::: {0}", UtilConstants.NetworkAlive ? "Alive" : "Not Available"));
            //Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "{0},{1}", UtilConstants.RunPlatform, UtilConstants.RunConfiguration));
            UtilGlobalFunc.PrintProperties(UtilConstants.appConfig);
            MbiBaseSystem.EvtInterface = evtInterface;  // 給 "子系統" 參考使用
            MbiBaseService.EvtInterface = evtInterface; // 給 "服務" 參考使用
            DataRefs.StaticEvtInterface = evtInterface; // 給 "DataRef" 參考使用
            Commands.StaticEvtInterface = evtInterface; // 給 "Command" 參考使用
            return 1;
        }

        static void Main(string[] args) {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.RealTime; // 設定此 app 在 CPU 的執行優先順序為 "即時" 等級
            Process.GetCurrentProcess().Refresh();
#if (!DEBUG)
            UtilGlobalFunc.MiniSizeConsoleWindow();
#endif
            using (var app = new SingletonCheck(UtilConstants.MBISimBridge)) {
                if (!app.isAnotherInstanceOpen) {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.MutexIdMsg, UtilConstants.MutexId));
                    UtilGlobalFunc.Log(UtilConstants.UniqueKeyOfMachine);
                    UtilGlobalFunc.SetMBISimBridgeConsoleTitleMessage(); // app 剛啟動的狀態更新
                    RunSingletoneApp(args);
                }
            }
        }
        /// <summary>
        /// Start Main Application
        /// </summary>
        private static void RunSingletoneApp(string[] args) {
            Task.Factory.StartNew(() => { airManagerSystem.StartSimulator(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
            
            #region [測試用], 模擬 3 種交易行為
            //wsClientService.MockupMsgReceived(WSClientServcie.WritePattern);
            //wsClientService.MockupMsgReceived(WSClientServcie.ReadRegister);
            //wsClientService.MockupMsgReceived(WSClientServcie.WriteRegister);
            #endregion

            #region 實例化 IHostBuilder :: new HostBuilder(), 傳送自己的註冊資訊過去給 Server
            CreateHostBuilder(args)
                .Build()//只會執行一次
                .Run()//執行應用程式並阻止呼叫執行緒，直到主機關閉
                ;
            #endregion
        }

        #region 實例化 IHostBuilder :: new HostBuilder(), 啟動一個獨立的背景應用程式, 並阻止呼叫執行緒, 直到主程式關閉
        /// <summary>
        /// 實例化 IHostBuilder
        /// <para>主要用來提供應用程式一個標準的啟動，</para>
        /// <para>包含注入DI、紀錄Logger、組態Config，</para>
        /// <para>不同的應用程式框架 (HostService) 有不同的預設啟動設定，</para>
        /// <para>.NET Generic Host 讓我們的應用程式的生命週期的控制，啟動到結束的撰寫方式統一了。</para>
        /// </summary>
        /// <param name="args">Command Line args</param>
        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)//實例化 HostBuilder
                .UseContentRoot(Directory.GetCurrentDirectory())//指定Host要使用的內容根目錄
                .ConfigureServices(services => {
                    services.AddHostedService<AirManagerService>();   // 啟動與 MBIAmAPIWrapper C++ 通訊的 Service
                    if (UtilConstants.appConfig.gRPCon) {
                        services.AddHostedService<GrpcListeningService>();// 啟動監聽 gRPC Server 是否連線 的 Service
                        services.AddSingleton<IHostedService, GrpcCommunicate>(); // 註冊 "GrpcCommunicate" 進去 Host 的 DI Container 裡面
                    } else { UtilGlobalFunc.Log(UtilGlobalFunc.GetGRPCoffMsg(string.Format(CultureInfo.CurrentCulture, "不監聽 {0}", UtilConstants.MBIServer))); }
                    // *** 實例化 IHostedService ***
                    // IHostedService 是用來實現來完成應用程序的 "工作"，當 Host 啟動後，也就是呼叫 IHostedService.StartAsync()，
                    // 這時它會執行 DI Container 裡面, 已註冊的所有 BackgroundService、IHostedService 的物件 和 BackgroundService.ExecuteAsync() 方法
                })
                // IHostBuilder 擴充方法, 用來設定應用程式其他的設定，可多次呼叫，結果會累加(後面蓋掉前面)。
                // 1. ConfigureServices() :: 將物件(服務) 加入 DI Container
                // 2. ConfigureLogging() :: 加入 Log Provider
                // 3. ConfigureAppConfiguration() :: 設定其餘的組態
        #region 配置初始化(環境變數、appsettings.json、User Secrets)
                .ConfigureHostConfiguration(config => { //載入 "主機" 組態 (Host Configuration)
                    config.AddEnvironmentVariables(prefix: "DOTNET_");
                    if (args != null)
                    { config.AddCommandLine(args); }
                })
                .ConfigureAppConfiguration((hostingContext, config) => {//載入 "應用程式" 組態 (ASP.NET Core/Console App)
                    var env = hostingContext.HostingEnvironment;
                    bool reloadOnChange = hostingContext.Configuration.GetValue("hostBuilder:reloadConfigOnChange", defaultValue: true);

                    config.AddJsonFile("appsettings.json", optional: true, reloadOnChange: reloadOnChange)
                          .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: reloadOnChange);

                    if (env.IsDevelopment() && !string.IsNullOrEmpty(env.ApplicationName))
                    {
                        var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                        if (appAssembly != null)
                        { config.AddUserSecrets(appAssembly, optional: true); }
                    }

                    config.AddEnvironmentVariables();

                    if (args != null)
                    { config.AddCommandLine(args); }
                })
        #endregion
            ;
        #endregion
    }
}
