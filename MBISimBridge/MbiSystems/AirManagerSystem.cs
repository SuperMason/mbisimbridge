﻿using MBILibrary.Enums;
using MBILibrary.Util;
using MBISimBridge.Services;
using System.Globalization;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// 負責處理與 Air Manager 溝通的介面
    /// <para>1. 是否註冊?  y/n (預留未來擴充功能, 目前預設為 "已註冊", by pass)</para>
    /// <para>2. WebSocket連線?  y/n (預留未來擴充功能, 目前預設為 "已連線", by pass)</para>
    /// <para>3. X-Plane 已經 Ready?  y/n (可監控得知, X-Plane 軟體是否已經準備好了)</para>
    /// <para>4. 與 X-Plane 資料互動</para>
    /// </summary>
    public class AirManagerSystem : MbiBaseSystem
    {
        #region fields
        /// <summary>
        /// 負責處理與 X-Plane 溝通的介面
        /// </summary>
        private XPlaneSystem xPlaneSystem = null;
        /// <summary>
        /// 將 'Air Manager' 或 'X-Plane' 的訊息, 透過 gRPC Client 傳送給 gRPC Server
        /// </summary>
        private GrpcClientService mGrpcClientService = null;
        /// <summary>
        /// Sample Event Fire Logic
        /// </summary>
        private SampleEvtFireSystem mSampleEvtFireSystem = null;
        private AircraftSystem mAircraftSystem = null;
        /// <summary>
        /// Handle Air Force Logic
        /// </summary>
        private AirForceSystem mAirForceSystem = null;
        /// <summary>
        /// 是否已經註冊?
        /// </summary>
        private bool mHasSignuped = false;
        /// <summary>
        /// WebSocket 是否已經連線?
        /// </summary>
        private bool mWSConnected = false;
        #endregion


        #region 檢查是否已經註冊的次數控制
        /// <summary>
        /// 檢查是否已經註冊的累積次數
        /// </summary>
        private int checkHostRegisterCNT = 0;
        /// <summary>
        /// 檢查是否已經註冊的上限次數
        /// </summary>
        private const int checkHostRegisterMaxCNT = 3;
        #endregion


        #region constructor
        /// <summary>
        /// 負責處理與 Air Manager 溝通的介面
        /// <para>1. 是否註冊?  y/n (預留未來擴充功能, 目前預設為 "已註冊", by pass)</para>
        /// <para>2. WebSocket連線?  y/n (預留未來擴充功能, 目前預設為 "已連線", by pass)</para>
        /// <para>3. X-Plane 已經 Ready?  y/n (可監控得知, X-Plane 軟體是否已經準備好了)</para>
        /// <para>4. 與 X-Plane 資料互動</para>
        /// </summary>
        public AirManagerSystem()
        {
            triggerBySystem = TriggerByWhatSystem.FromAirManager;
            #region 檢查是否註冊, WebSocket 連線
            EvtInterface.HostRegistered += HostRegistered;
            EvtInterface.HostNotRegistered += HostNotRegistered;
            EvtInterface.WSConnectionError += WSConnectionError;
            EvtInterface.WSConnectionClosed += WSConnectionClosed;
            EvtInterface.WSConnected += OnWSConnected;
            #endregion

            EvtInterface.XPlaneConnected += OnXPlaneConnected;
        }
        #endregion


        /// <summary>
        /// 取得本機Host的註冊資訊
        /// </summary>
        private RegistrationInfo GetLocalRegisterInfo()
        { return new RegistrationInfo { HostLicenseKey = UtilConstants.UniqueKeyOfMachine, HostName = UtilConstants.HostRegistryMachineName }; }


        #region 啟動 / 關閉 "飛行模擬器"
        /// <summary>
        /// 啟動 "飛行模擬器"
        /// </summary>
        public void StartSimulator() {
            Log(string.Format(CultureInfo.CurrentCulture, "Starting {0}...", UtilConstants.MBISimBridge));
            //Log("先檢查註冊狀態, 已註冊? 未註冊?");
            //Log("未註冊: 則無法繼續使用飛行模擬器");
            //Log("已註冊: 則繼續開啟 WebSocket 連線..");
            EvtInterface.OnCheckHostRegistrationStatus(this, new MessageEventArgs { RegistrationInfo = GetLocalRegisterInfo() });
            EvtInterface.OnCheckingHostRegistrationStatus.WaitOne(); // 等待 RESTClientService 去問完的結果後, 執行了 EvtInterface.OnCheckingHostRegistrationStatus.Set(); // done
                                                                     // 才會往下一行程式走..
                                                                     // 問完結果後.. 不管是否已經註冊 ? 關閉 UI 上的 Loading 畫面
            Log("========================================================");
        }
        /// <summary>
        /// 關閉  "飛行模擬器"
        /// </summary>
        public void ExitSimulator() {
            Log("Closed MBISimBridge...");
            EvtInterface.OnCloseWS(this, new MessageEventArgs("Please close websocket.")); // 切斷 WebSocket
            EvtInterface.OnExecuteAirManagerCommand(this, new CommandEvevntArgs(Commands.OperationQuit.Description, Commands.OperationQuit)); // 關閉 X-Plane 軟體
            EvtInterface.ResetVerificationCode(); // 更新本機驗證碼
            mHasSignuped = false;
            mWSConnected = false;
        }
        #endregion


        #region Registration event handler, 處理註冊
        /// <summary>
        /// 解除註冊
        /// </summary>
        public void UnSignup() {
            if (mWSConnected) { ExitSimulator(); } // break the ws connection if connected
            Log("解除註冊");
            EvtInterface.OnUnSignup(this, new MessageEventArgs { RegistrationInfo = GetLocalRegisterInfo() });
            EvtInterface.OnUnSigningUp.WaitOne(); // 開始 "解除註冊" 行為
            Log("Close loading after UnSignUp");
        }
        /// <summary>
        /// 執行註冊
        /// </summary>
        public void Signup() {
            Log("執行註冊行為");
            EvtInterface.OnSignup(this, new MessageEventArgs { RegistrationInfo = GetLocalRegisterInfo() });
            EvtInterface.OnSigningUp.WaitOne(); // 開始 "進行註冊" 行為

            if (checkHostRegisterCNT <= checkHostRegisterMaxCNT) {
                //Log("Close loading after SignUp, and wait for Signup.");
            } else {
                Log("超過註冊上限次數, 直接關閉 MBISimBridge...");
                EvtInterface.OnSigningUp.Set();
            }
        }
        /// <summary>
        /// 驗證 Host 已經註冊後, 進一步執行 "WebSocket 連線"
        /// </summary>
        private void HostRegistered(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                mHasSignuped = true;
                UtilGlobalFunc.PrintProperties(e.RegistrationInfo);
                e.Data = "host registered, go to connect WebSocket...";
                Log(e.Data);

                #region 進一步執行 "WebSocket 連線"
                if (mHasSignuped) {
                    EvtInterface.OnConnectWS(this, e);
                    EvtInterface.OnWSConnecting.WaitOne(); // Wait for WebSocket Connecting...
                }
                #endregion
            }
        }
        /// <summary>
        /// 驗證 Host 尚未註冊後, 進一步執行 "註冊行為"
        /// </summary>
        private void HostNotRegistered(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                mHasSignuped = false;
                ++checkHostRegisterCNT; // 計算驗證註冊次數

                if (checkHostRegisterCNT <= checkHostRegisterMaxCNT) {
                    Log($"checking host register... {checkHostRegisterCNT}/{checkHostRegisterMaxCNT}");
                    Signup();
                    // 檢查後, 尚未註冊, 所以主動進行註冊~
                } else { ClosedMBISimBridge(e); } // 檢查次數超過上限, 所以直接關閉 MBISimBridge
            }
        }
        #endregion


        /// <summary>
        /// Verify Successful !!! Create connection between 'Air Manager' and 'X-Plane'
        /// </summary>
        private void CreateAirManagerSimBusConnection(MessageEventArgs e) {
            #region new sub-system
            xPlaneSystem = new XPlaneSystem();
            if (UtilConstants.appConfig.gRPCon) { mGrpcClientService = new GrpcClientService(); } // 將 "X-Plane", "Air Manager" 的 data 傳送給 MBIServer
            else { UtilGlobalFunc.Log(UtilGlobalFunc.GetGRPCoffMsg(string.Format(CultureInfo.CurrentCulture, "不傳送 {0}, {1} data to {2}", UtilConstants.XPlaneSoftware, UtilConstants.AirManagerSoftware, UtilConstants.MBIServer))); }
            mSampleEvtFireSystem = new SampleEvtFireSystem();
            mAircraftSystem = new AircraftSystem();
            mAirForceSystem = new AirForceSystem();
            e.Data = "Checking Electrical System...";
            EvtInterface.OnStartElectricalCheck(this, e);
            #endregion
        }

        #region WebSocket event handler, 處理 WebSocket
        /// <summary>
        /// WebSocket 連線成功後, 馬上送一個訊息給 Cloud, 告知 Host 連線資訊
        /// <para>確定已註冊且WebSocket已連線, 就開啟</para>
        /// <para>1. X-Plane 連線</para>
        /// <para>2. Air Manager 的連線</para>
        /// </summary>
        private void OnWSConnected(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                mWSConnected = true;
                Log("WebSocket Connected, send connect success message to cloud.."); // 通知 WebSocket 即可, 不須等待回覆..
                e.Cmsg = new ConnectMessage(EvtInterface.VerificationCode, UtilConstants.NetworkType);
                EvtInterface.OnSendWSConnectMessage(this, e);

                #region 已註冊, 且 WebSocket 連線成功後, 先註冊 Air Manager 的 DataRefs, Commands, 再開啟與 X-Plane 的連線監聽
                if (mHasSignuped && mWSConnected) {
                    Log("SignUp and WebSocket connected, subscribe DataRefs and Commands on 'Air Manager', connect to 'X-Plane'.");
                    CreateAirManagerSimBusConnection(e);
                }
                #endregion
            }
        }
        /// <summary>
        /// WebSocket 連線發生錯誤
        /// </summary>
        private void WSConnectionError(object serder, MessageEventArgs e)
        {
            // do something while got a WebSocket connection error
            ClosedMBISimBridge(e);
        }
        /// <summary>
        /// WebSocket 連線已經關閉
        /// </summary>
        private void WSConnectionClosed(object serder, MessageEventArgs e)
        {
            // do something while got a WebSocket connection dis-connected self
            ClosedMBISimBridge(e);
        }
        /// <summary>
        /// 關閉 MBISimBridge
        /// </summary>
        private void ClosedMBISimBridge(MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                mWSConnected = false;
                Log(e.Data);
                ExitSimulator();
            }
        }
        #endregion


        #region 與 X-Plane 互動
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        private void OnXPlaneConnected(object serder, MessageEventArgs e) {
            Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.XPlaneConnectedMsg, nameof(AirManagerSystem), e.Data));
            WriteDrCtrller.UpdateXPlaneValue(triggerBySystem, nameof(OnXPlaneConnected), DataRefs.NetworkDataoutNetworkDataRate, 99);
        }
        #endregion

    }
}
