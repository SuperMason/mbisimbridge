﻿using MBILibrary.Enums;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// 負責將 "TestPattern 打出至螢幕上"
    /// </summary>
    public class TestPatternSystem : MbiBaseSystem
    {
        #region constructor
        /// <summary>
        /// 負責將 "TestPattern 打出至螢幕上"
        /// </summary>
        public TestPatternSystem() {
            triggerBySystem = TriggerByWhatSystem.FromTestPattern;
            EvtInterface.WriteTestPattern += WriteTestPattern;
            EvtInterface.WritePatternSessionUpdated += WritePatternSessionUpdated;
        }
        #endregion

        #region Pattern Write event handler, 處理 Pattern 打出至螢幕上
        /// <summary>
        /// 將 Test Pattern 打至螢幕上
        /// </summary>
        private void WriteTestPattern(object sender, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                //e.Patterns.ForEach(i => UtilGlobalFunc.PrintPropreties(i));
                e.Data = "Test Pattern has been set, please help me to update the session status.";
                EvtInterface.OnTestPatternWrote(this, e);
            }
        }
        /// <summary>
        /// TestPatternWrote Session updated, 完成整個 write pattern 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        private void WritePatternSessionUpdated(object sender, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey))
            { Log(e.Data); }
        }
        #endregion
    }
}
