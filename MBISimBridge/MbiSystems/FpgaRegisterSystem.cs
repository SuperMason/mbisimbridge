﻿using MBILibrary.Enums;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// 負責 FPGA Register Read/Write
    /// </summary>
    public class FpgaRegisterSystem : MbiBaseSystem
    {
        #region constructor
        /// <summary>
        /// 負責 FPGA Register Read/Write
        /// </summary>
        public FpgaRegisterSystem() {
            triggerBySystem = TriggerByWhatSystem.FromFpga;
            EvtInterface.WriteRegister += WriteRegister;
            EvtInterface.ReadRegister += ReadRegister;
            EvtInterface.WriteRegisterSessionUpdated += WriteRegisterSessionUpdated;
            EvtInterface.ReadRegisterSessionUpdated += ReadRegisterSessionUpdated;
        }
        #endregion

        #region Register read/write event handler, 處理 Register 讀寫
        /// <summary>
        /// FPGA Register 寫入
        /// </summary>
        private void WriteRegister(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                //UtilGlobalFunc.PrintPropreties(e.ParamToWrite);
                e.Data = "The register has been wrote to FPGA, please help me to update the session status.";
                EvtInterface.OnRegisterWrote(this, e);
            }
        }
        /// <summary>
        /// FPGA Register 讀出
        /// </summary>
        private void ReadRegister(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey)) {
                //e.ParamToRead.ForEach(i => Log(string.Format("{0}\t", i)));
                //Log("");
                e.ParamReaded = new ParamProperty
                { Param1 = "0", Param2 = "1", Param3 = "2" };
                e.Data = "The register has been readed from FPGA, please help me to update the session status.";
                EvtInterface.OnRegisterReaded(this, e);
            }
        }
        /// <summary>
        /// RegisterReaded Session updated, 完成整個 Register Readed 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        private void ReadRegisterSessionUpdated(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey))
            { Log(e.Data); }
        }
        /// <summary>
        /// RegisterWrote Session updated, 完成整個 Register Wrote 行為, 並且已經 Release Session Locked, 可以進行下一次行為
        /// </summary>
        private void WriteRegisterSessionUpdated(object serder, MessageEventArgs e) {
            if (e.RegistrationInfo != null && !string.IsNullOrEmpty(e.RegistrationInfo.HostLicenseKey))
            { Log(e.Data); }
        }
        #endregion

    }
}
