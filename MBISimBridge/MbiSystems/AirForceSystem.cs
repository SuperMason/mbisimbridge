﻿using AircraftSystem.InstrumentPanel;
using MBILibrary.Enums;
using MBILibrary.Util;
using MBISimBridge.Services;
using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// Handle Air Force Logic, 注意 !!! 這是 demo sample code, 別直接複製貼上 !!!
    /// </summary>
    public class AirForceSystem : MbiBaseSystem
    {
        #region properties
        private DataRefElement AF2AMIntDR = null, AF2AMFloatDR = null, AF2AMDoubleDR = null, AF2AMaIntDR = null, AF2AMaFloatDR = null, AF2AMaDoubleDR = null
            , AF2AMMyIntValueDR = null, AF2AMMyFloatValueDR = null, AF2AMMyDoubleValueDR = null, AF2AMMyIntValueADR = null, AF2AMMyFloatValueADR = null, AF2AMMyDoubleValueADR = null;
        private StringDataRefElement AF2AMStringDR = null, AF2AMMyStringValueDR = null;
        #endregion

        #region constructor
        /// <summary>
        /// Handle Air Force Logic, 注意 !!! 這是 demo sample code, 別直接複製貼上 !!!
        /// </summary>
        public AirForceSystem() {
            triggerBySystem = TriggerByWhatSystem.FromAirForce;
            EvtInterface.StartAirForceSystem += StartAirForceSystem;
            EvtInterface.XPlaneConnected += OnXPlaneConnected;
            EvtInterface.UpdateAirManagerDataRefSingleVal += ReadLatestDataRefSingleVal;
            EvtInterface.InfoAirManagerDataRefValueUpdated += InfoAirManagerDataRefValueUpdated;

            #region 與 Air Manager 互動
            EvtInterface.ExecuteAirManagerCommand += ExecuteAirManagerCommand;
            EvtInterface.ExecuteAirManagerDataRefWrite += ExecuteAirManagerDataRefWrite;//從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
            #endregion
#if DEBUG
            // 注意 !!! 這是 demo sample code, 別直接複製貼上 !!!
            if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.Debug4SendDataAF2AM != UtilConstants.Debug4SendDataDisable) {
                InitParamsAF2AM();
                Task.Run(() => TestingWriteDataRefIntoAM()); // 測試寫 DataRef to 'Air Manager'
            }

            if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.Debug4SendDataAF2XP != UtilConstants.Debug4SendDataDisable) {
                Task.Run(() => TestingWriteDataRefIntoXP()); // 測試寫 DataRef to 'X-Plane'
                Task.Run(() => TestingWriteCommandIntoXP()); // 測試寫 Command to 'X-Plane'
            }
#endif
        }
        private void InitParamsAF2AM() {
            AF2AMIntDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/aaa/a1");
            AF2AMFloatDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/aaa/a2");
            AF2AMDoubleDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/bbb/double");
            AF2AMaIntDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/bbb/ia3");
            AF2AMaFloatDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/bbb/fa5");
            AF2AMaDoubleDR = InitParamsCtrller.GetDataRefElementFromInit("mbi/bbb/da6");
            AF2AMMyIntValueDR = InitParamsCtrller.GetDataRefElementFromInit("myInt");
            AF2AMMyFloatValueDR = InitParamsCtrller.GetDataRefElementFromInit("myFloat");
            AF2AMMyDoubleValueDR = InitParamsCtrller.GetDataRefElementFromInit("myDouble");
            AF2AMMyIntValueADR = InitParamsCtrller.GetDataRefElementFromInit("myIntValueA");
            AF2AMMyFloatValueADR = InitParamsCtrller.GetDataRefElementFromInit("myFloatValueA");
            AF2AMMyDoubleValueADR = InitParamsCtrller.GetDataRefElementFromInit("myDoubleValueA");
            AF2AMStringDR = InitParamsCtrller.GetStringDataRefElementFromInit("mbi/aaa/a3");
            AF2AMMyStringValueDR = InitParamsCtrller.GetStringDataRefElementFromInit("myStr");
        }
        #endregion
        /// <summary>
        /// 啟動空戰系統
        /// </summary>
        private void StartAirForceSystem(object sender, MessageEventArgs e) {
            Log($"{e.Data}");
            Log(JsonUtil.Convert2JsonString(e.electricalProperty));
            EvtInterface.OnAirForceStarted(this, new MessageEventArgs("啟動空戰前置作業... 啟動空戰完畢.."));
        }


        #region 與 'X-Plane' 互動, Read/Write DataRef、 Write Command into 'X-Plane'
        #region Write Command, DataRef into 'X-Plane'
        /// <summary>
        /// Write Command into 'X-Plane'
        /// <para>注意 !!! 這是 demo sample code, 別直接複製貼上 !!!</para>
        /// </summary>
        private void TestingWriteCommandIntoXP() {
            SpinWait.SpinUntil(() => AirManagerService.SubscribedDataRefsFinished && XPlaneSystem.SubscribedDataRefsFinished);
            //ExecuteCommandToXPlane(Commands.OperationQuit);
            //ExecuteCommandToXPlane(Commands.ElectricalBattery1On);
        }

        /// <summary>
        /// 寫值測試 : AF -> XP
        /// <para>注意 !!! 這是 demo sample code, 別直接複製貼上 !!!</para>
        /// <para>Offset Write DataRef value into 'X-Plane' 範例程式 ::: "int", "float", "double", "string", "int[]", "float[]"</para>
        /// <para>1. 單一值 ::: int, float, double, string</para>
        /// <para>2. Array值::: int[], float[] 含 offset(optional)</para>
        /// </summary>
        private async void TestingWriteDataRefIntoXP() {
            SpinWait.SpinUntil(() => AirManagerService.SubscribedDataRefsFinished && XPlaneSystem.SubscribedDataRefsFinished);
            SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(1000));

            #region 寫值測試 ::: AF -> XP
            int i = -1;
            while (true) {
                i++;
                string Idx = (i + 1).ToString("00");

                //WriteXP(DataRefs.Aircraft2EngineExhaustDirtinessRatio, 123.456f); // 測試沒註冊的 DataRef, 隨便抓一個 DataRef 要寫入 X-Plane

                #region 測試 int, int[]
                int networkDataRate = UtilGenTestingData.GetRandomInt % 100;
                WriteXP(DataRefs.NetworkDataoutNetworkDataRate, networkDataRate);
                int acfFfHydraulic = UtilGenTestingData.GetRandomInt;
                WriteXP(DataRefs.AircraftForcefeedbackAcfFfHydraulic, acfFfHydraulic);
                int antiIceSurfHeatLeft = UtilGenTestingData.GetRandomInt;
                WriteXP(DataRefs.CockpitSwitchesAntiIceSurfHeatLeft, antiIceSurfHeatLeft);
                int chk = i % 2;
                var dataToScreen = new int[] { chk, chk, chk, chk, chk };
                WriteXP(DataRefs.NetworkDataoutDataToScreen, dataToScreen, 3);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} ::: dataToScreen({1}), networkDataRate({2}), acfFfHydraulic({3}), antiIceSurfHeatLeft({4})"
                    , Idx, GetJoinStr(dataToScreen), networkDataRate, acfFfHydraulic, antiIceSurfHeatLeft));
                #endregion

                #region 測試 float, double
                float vviStepFt = UtilGenTestingData.GetRandomFloat;
                WriteXP(DataRefs.AircraftAutopilotVviStepFt, vviStepFt);
                float plane6The = UtilGenTestingData.GetRandomFloat;
                WriteXP(DataRefs.MultiplayerPositionPlane6The, plane6The);
                double plane6X = UtilGenTestingData.GetRandomDouble;
                WriteXP(DataRefs.MultiplayerPositionPlane6X, plane6X);
                double localZ = UtilGenTestingData.GetRandomDouble;
                WriteXP(DataRefs.FlightmodelPositionLocalZ, localZ);
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} ::: vviStepFt({1}), plane6The({2}), plane6X({3}), localZ({4})"
                    , Idx, vviStepFt, plane6The, plane6X, localZ));
                #endregion

                #region 測試 float[]
                var plane1Throttle = UtilGenTestingData.GetRandomFloatArray(3);
                WriteXP(DataRefs.MultiplayerPositionPlane1Throttle, plane1Throttle, 3); // 在 float[8], 位移至 index 3 後, 連續寫入 3 個值 : [3], [4], [5]
                var plane6Throttle = UtilGenTestingData.GetRandomFloatArray(2);
                WriteXP(DataRefs.MultiplayerPositionPlane6Throttle, plane6Throttle, 2); // 在 float[8], 位移至 index 2 後, 連續寫入 2 個值 : [2], [3]
                var plane6GearDeploy = UtilGenTestingData.GetRandomFloatArray(1);
                WriteXP(DataRefs.MultiplayerPositionPlane6GearDeploy, plane6GearDeploy, 3); // 在 float[10], 位移至 index 3 後, 連續寫入 1 個值 : [3]
                var engineThrottleRequest = UtilGenTestingData.GetRandomFloatArray(1);
                engineThrottleRequest[0] = 1 / engineThrottleRequest[0];
                WriteXP(DataRefs.MultiplayerControlsEngineThrottleRequest, engineThrottleRequest); // 在 float[1], 此 DataRef 只接受 0 ~ 1 的值
                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} ::: plane1Throttle({1}), plane6Throttle({2}), plane6GearDeploy({3}), engineThrottleRequest({4})"
                    , Idx, GetJoinStr(plane1Throttle), GetJoinStr(plane6Throttle), GetJoinStr(plane6GearDeploy), GetJoinStr(engineThrottleRequest)));
                #endregion

                //var acfTailnum = "AB123";
                //WriteXP(DataRefs.AircraftViewAcfTailnum, acfTailnum); // write string

                // 目前 'X-Plane' 沒有 double[] DataRef 可以測試, 日後若有, 仿照 float[] 即可

                await Task.Delay(TimeSpan.FromMilliseconds(UtilConstants.appConfig.Debug4SendDataAF2XP));
            }
            #endregion
        }
        /// <summary>
        /// 寫入 X-Plane
        /// </summary>
        private void WriteXP(DataRefElementBase drBase, object val, int offset = UtilConstants.NullOffset) => WriteDrCtrller.NotifyXPlaneValueChanged(triggerBySystem, nameof(TestingWriteDataRefIntoXP), drBase, val, offset);
        private string GetJoinStr(int[] ia) => string.Join(", ", ia);
        private string GetJoinStr(float[] fa) => string.Join(", ", fa);
        #endregion


        #region 從 X-Plane 取得 DataRef 最新資訊
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        private void OnXPlaneConnected(object serder, MessageEventArgs e) {
            //Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.XPlaneConnectedMsg, nameof(AirForceSystem), e.Data));
        }
        /// <summary>
        /// Listen DataRef value updated :: '單一值' int, float, double, string
        /// <para>只 Read data, 並做後續連動行為</para>
        /// </summary>
        private void ReadLatestDataRefSingleVal(object sender, DataRefEventArgs e) {
            try {
                string msg = string.Empty, MsgFormat = "{0}, DataRefName({1}), Value({2})";

                if (e.DataBusIsBytes) {
                    msg = string.Format(CultureInfo.CurrentCulture, MsgFormat, GetType().Name, e.DataBus4Bytes.DataRef, e.DataBus4Bytes.ObjVal2Float);
                } else if (e.DataBusIsString) {
                    msg = string.Format(CultureInfo.CurrentCulture, MsgFormat, GetType().Name, e.DataBus4String.DataRef, e.DataBus4String.RawStringValue);
                } else { }

                //UtilGlobalFunc.ShowMsgByDebug4BigMason(msg);
            }
            catch (Exception ex) { Log(string.Format(CultureInfo.CurrentCulture, "{0}, {1}(), Exception({2})", GetType().Name, nameof(ReadLatestDataRefSingleVal), ex.Message)); }
        }
        /// <summary>
        /// Listen DataRef value update to 'Air Manager' success
        /// <para>只 Read data, 並做後續連動行為</para>
        /// <para>注意 !!! 這是 demo sample code, 別直接複製貼上 !!!</para>
        /// </summary>
        private void InfoAirManagerDataRefValueUpdated(object sender, DataRefEventArgs e) {
            try {
                //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} : Listening {1}, '{2}', Value({3})", GetType().Name, e.DataBus4Object.Type, e.DataBus4Object.DataRef, e.DataBus4Object.ObjValShowString));
            } catch (Exception ex) { Log(string.Format(CultureInfo.CurrentCulture, "{0}, {1}(), Exception({2})", GetType().Name, nameof(InfoAirManagerDataRefValueUpdated), ex.Message)); }
        }
        #endregion
        #endregion

        #region 與 'Air Manager' 互動, 從 'Air Manager' 讀取 DataRef、寫 DataRef into 'Air Manager'
        /// <summary>
        /// 寫值測試 : AF -> AM
        /// <para>Write DataRef value into 'Air Manager'</para>
        /// <para>注意 !!! 這是 demo sample code, 別直接複製貼上 !!!</para>
        /// </summary>
        private async void TestingWriteDataRefIntoAM() {
            SpinWait.SpinUntil(() => AirManagerService.SubscribedDataRefsFinished);
            string FuncName = nameof(TestingWriteDataRefIntoAM);

            while (true) { // 注意!!! 這是 demo sample code, 別直接複製貼上 !!!
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, MbiDataRefs.InstrumentPanel_UFC_TIME_highlightIndex, UtilGenTestingData.GetRandomInt);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, MbiDataRefs.InstrumentPanel_UFC_TIME_Date, UtilGenTestingData.GetRandomString);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMIntDR, UtilGenTestingData.GetRandomInt);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyIntValueDR, UtilGenTestingData.GetRandomInt);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMFloatDR, UtilGenTestingData.GetRandomFloat);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyFloatValueDR, UtilGenTestingData.GetRandomFloat);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMDoubleDR, UtilGenTestingData.GetRandomDouble);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyDoubleValueDR, UtilGenTestingData.GetRandomDouble);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMaIntDR, UtilGenTestingData.GetRandomIntArray(AF2AMaIntDR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyIntValueADR, UtilGenTestingData.GetRandomIntArray(AF2AMMyIntValueADR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMaFloatDR, UtilGenTestingData.GetRandomFloatArray(AF2AMaFloatDR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyFloatValueADR, UtilGenTestingData.GetRandomFloatArray(AF2AMMyFloatValueADR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMaDoubleDR, UtilGenTestingData.GetRandomDoubleArray(AF2AMaDoubleDR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyDoubleValueADR, UtilGenTestingData.GetRandomDoubleArray(AF2AMMyDoubleValueADR.ArrayLen));
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMStringDR, UtilGenTestingData.GetRandomString);
                NotifyAirManagerValueChanged(triggerBySystem, FuncName, AF2AMMyStringValueDR, UtilGenTestingData.GetRandomString);
                await Task.Delay(TimeSpan.FromMilliseconds(UtilConstants.appConfig.Debug4SendDataAF2AM));
            }
        }
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'AirForceSystem'
        /// </summary>
        private void ExecuteAirManagerCommand(object serder, CommandEvevntArgs e) {
            //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.ExecuteCommandInfo, GetType().Name, e.Command.Name, e.Command.Value, e.Command.Description));
        }
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'AirForceSystem'
        /// <para>byte, float, int, bool, double 的更新, 皆以 float 帶入 X-Plane</para>
        /// <para>實際測試結果為 ::: 如果 SPEC 定義 type 為 int, bool 時, 要帶入 float 才能順利寫入值</para>
        /// </summary>
        private void ExecuteAirManagerDataRefWrite(object sender, DataRefEventArgs e) {
            if (e == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefEventArgs), e)); return; }
            if (e.DataBus4Object == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefElementBase), e.DataBus4Object)); return; }

            if (e.DataRefIsWritable) {
                if (e.ObjCheck != null) {
                    bool WriteIt = false;

                    if (e.DataBus4ObjectIsBytes)
                    { WriteIt = true; }
                    else if (e.DataBus4ObjectIsString)
                    { WriteIt = true; }

                    if (WriteIt) {
                        //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueInfo, GetType().Name, e.TriggerByWhichSystem, UtilConstants.XPlaneSoftware, e.DataRefName, e.DataRefType, e.DataRefObjValShowString));
                    }
                }
            } else { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefCanNotWrittenMessage, e.DataRefName, UtilConstants.Flag_Yes)); }
        }
        #endregion
    }
}
