﻿using MBILibrary.Enums;
using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem
{
    public abstract class Component : MbiBaseSystem
    {
        #region Properties
        public string Name { get; set; }
        /// <summary>
        /// The status of the component, normal, standby or failure
        /// </summary>
        public enum State
        {
            NORMAL,
            STBY,
            FAILURE
        }
        protected virtual State state { get; set; }
        #endregion

        #region Constructor
        public Component()
        {
            triggerBySystem = TriggerByWhatSystem.FromComponent;
            state = State.STBY;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Simulate component malfunction
        /// </summary>
        public abstract void Failure();
        protected abstract void Reset();
        #endregion
    }
}
