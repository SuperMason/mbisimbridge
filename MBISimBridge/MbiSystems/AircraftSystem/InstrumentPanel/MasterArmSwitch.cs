﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class MasterArmSwitch : InstrumentComponent
    {
        #region Fields
        private bool hasPower = false;
        private enum Position
        {
            OFF,
            SIM,
            ARM
        }
        private Position position;
        #endregion
        public MasterArmSwitch()
        {
            EvtInterface.MasterArmSwitchPower += _MasterArmSwitchPower;
            EvtInterface.MasterArmSwitchToggled += _MasterArmSwitchToggled;
            position = Position.OFF;
            Name = "MasterArmSwitch";
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MasterArmSwitch);
            InstrumentPanel.AddDataRefEventToCatelog(Name, EvtInterface.OnMasterArmSwitchToggled);
            EvtInterface.OnMasterArmSwitchPosChanged(this, new IntEventArgs(0));
        }

        private void _MasterArmSwitchPower(object sender, IntEventArgs e)
        {
            hasPower = e.value == 1;
            if (hasPower)
            {
                EvtInterface.OnMasterArmSwitchPosChanged(this, new IntEventArgs((int) position));
                WriteDataRef((int)position);
            }
        }

        private void _MasterArmSwitchToggled(object sender, IntEventArgs e)
        {
            // 0 -> OFF: weapons release is inhibited except for an emergency jettison
            // 1 -> SIM: radar and store management system operate normally, but no weapons can be released
            // 2 -> ARM: radar and store management system operate normally
            position = (Position) e.value;
            // Only has effect if hasPower
            if (hasPower)
			{
                EvtInterface.OnMasterArmSwitchPosChanged(this, e);
                WriteDataRef(e.value);
			}
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
