﻿using MBILibrary.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public abstract class InstrumentComponent: Component
    {
        #region Fields
        /// <summary>
        /// X-Plane DataRef Read/Write
        /// <para>1. DataRef Read :::       DataRefElement( 'float' data)</para>
        /// <para>2. DataRef Read ::: StringDataRefElement('string' data)</para>
        /// <para>3. DataRef Write ::   DataRefElementBase('object' data)</para>
        /// </summary>
        protected DataRefElement dataRef = null;
        protected StringDataRefElement stringDataRef = null;
        #endregion


        /// <summary>
        /// Write dataRef to AircraftSystem
        /// </summary>
        /// <param name="obj"></param>
        protected void WriteDataRef(object obj)
        {
            try
            {
// AircraftDataRefRead; // event fire 後, 沒人承接處理
// 寫了也沒用.. 沒人承接處理
                if (obj.GetType() == typeof(string) && stringDataRef != null)
                {
                    stringDataRef.RawStringValue = obj.ToString();
                    EvtInterface.OnInstrumentPanelDataRefRead(this, new DataRefEventArgs(triggerBySystem, Name, stringDataRef));
                }
                else if ((obj.GetType() == typeof(int) ||
                          obj.GetType() == typeof(float) ||
                          obj.GetType() == typeof(bool) ||
                          obj.GetType() == typeof(double)) && dataRef != null)
                {
                    dataRef.ObjVal = obj;
                    EvtInterface.OnInstrumentPanelDataRefRead(this, new DataRefEventArgs(triggerBySystem, Name, dataRef));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }

        /// <summary>
        /// Write dataRef to Air Manager
        /// </summary>
        /// <param name="obj"></param>
        protected void WriteDataRefToAirManager(object obj)
        {
            try
            {
                if (obj.ValueIsString() && stringDataRef != null)
                {
                    WriteDataRefToAirManager4AircraftSystem(stringDataRef, obj);
                }
                else if (obj.ValueIsSingle() && dataRef != null)
                {
                    WriteDataRefToAirManager4AircraftSystem(dataRef, obj);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }
    }
}
