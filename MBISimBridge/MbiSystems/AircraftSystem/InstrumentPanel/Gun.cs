﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class Gun : InstrumentComponent
    {
        #region Fields
        private bool hasPower = false;
        private bool gunArmed => hasPower && MasterArmed && Mode == AAMode.GUN;
        #endregion
        #region Properties
        public bool MasterArmed { get; set; }
        // 0: Gun, 1: AAM, 2: Missile
        public enum AAMode
        {
            GUN,
            AAM,
            MISSILE
        }
        public AAMode Mode { get; set; }
        #endregion
        public Gun()
        {
            EvtInterface.MasterArmSwitchPosChanged += _MasterArmSwitchPosChanged;
            EvtInterface.AAModeSwitchToggled += _AAModeSwitchToggled;
            EvtInterface.GunPower += _GunPower;
            Mode = AAMode.GUN;
            dataRef = GetDataRefElementFromInit(XPlaneConnector.Instructions.DataRefs.CockpitWeaponsGunsArmed.DataRef);
            WriteDataRef(0);
        }

        #region EventHandlers
        private void _MasterArmSwitchPosChanged(object sender, IntEventArgs e)
        {
            MasterArmed = e.value == 2;
            WriteDataRef(gunArmed ? 1 : 0);
        }

        private void _AAModeSwitchToggled(object sender, IntEventArgs e)
        {
            Mode = (AAMode)e.value;
            WriteDataRef(gunArmed ? 1 : 0);
        }


        private void _GunPower(object sender, IntEventArgs e)
        {
            hasPower = e.value == 1;
            WriteDataRef(gunArmed ? 1 : 0);
        }
        #endregion

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
