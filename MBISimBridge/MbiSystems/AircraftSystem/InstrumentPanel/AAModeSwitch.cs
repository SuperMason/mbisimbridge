﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class AAModeSwitch : InstrumentComponent
    {
        #region Fields
        //private bool hasPower = false;
        private enum Position
        {
            GUN,
            AAM,
            MISSILE
        }
        private Position position;
        #endregion
        public AAModeSwitch()
        {
            // EvtInterface.AAModeSwitchPower += _AAModeSwitchPower;
            EvtInterface.AAModeSwitchToggled += _AAModeSwitchToggled;
            position = Position.GUN;
            Name = "AAModeSwitch";
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_AAModeSwitch);
            InstrumentPanel.AddDataRefEventToCatelog(Name, EvtInterface.OnAAModeSwitchToggled);
        }

        private void _AAModeSwitchToggled(object sender, IntEventArgs e)
        {
            // 0 -> GUN
            // 1 -> AAM
            // 2 -> MISSILE
            // Currently, AAModeSwitch bypasses the examination from ElectricalSystem. If needed, please reference the implementation of MasterArmSwitch.
            position = (Position) e.value;
            WriteDataRefToAirManager(e.value);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
