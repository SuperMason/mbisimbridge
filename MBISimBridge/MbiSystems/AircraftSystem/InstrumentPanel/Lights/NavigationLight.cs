﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class NavigationLight : InstrumentComponent
    {
        public NavigationLight()
        {
            EvtInterface.NavigationLightPower += _NavigationLightPower;
            dataRef = GetDataRefElementFromInit(XPlaneConnector.Instructions.DataRefs.CockpitElectricalNavLightsOn.DataRef);
            WriteDataRef(0);
        }

        private void _NavigationLightPower(object sender, IntEventArgs e)
        {
            WriteDataRef(e.value);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
