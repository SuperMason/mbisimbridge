﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class TaxiLight : InstrumentComponent
    {
        public TaxiLight()
        {
            EvtInterface.TaxiLightPower += _TaxiLightPower;
            dataRef = GetDataRefElementFromInit(XPlaneConnector.Instructions.DataRefs.CockpitElectricalTaxiLightOn.DataRef);
            WriteDataRef(0);
        }

        private void _TaxiLightPower(object sender, IntEventArgs e)
        {
            WriteDataRef(e.value);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
