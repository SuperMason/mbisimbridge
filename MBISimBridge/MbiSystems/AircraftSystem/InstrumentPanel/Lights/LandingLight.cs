﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class LandingLight : InstrumentComponent
    {
        public LandingLight()
        {
            EvtInterface.LandingLightPower += _LandingLightPower;
            dataRef = GetDataRefElementFromInit(XPlaneConnector.Instructions.DataRefs.CockpitElectricalLandingLightsOn.DataRef);
            WriteDataRef(0);
        }

        private void _LandingLightPower(object sender, IntEventArgs e)
        {
            WriteDataRef(e.value);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
