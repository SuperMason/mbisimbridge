﻿using MBISimBridge.MbiSystems;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class RightMFD : MFD
    {
        #region Fields
        
        #endregion
        #region Constructor
        public RightMFD(DirectInput di, Guid id) : base(di, id)
        {
            EvtInterface.RightMFDbuttonPressed += _RightMFDbuttonPressed;
            EvtInterface.LeftMFDpageChanged += _LeftMFDpageChanged;
            EvtInterface.LeftMFDpageReported += _LeftMFDpageReported;
            EvtInterface.LeftMFDtabsReported += _LeftMFDtabsReported;
            EvtInterface.LeftMFDindexReported += _LeftMFDindexReported;
            Name = "MFD_right";
            InstrumentPanel.AddCommandEventToCatelog(Name, EvtInterface.OnRightMFDbuttonPressed);
            // Start with HSD page
            current_page = Page.HSD;
            pageTabs = new List<Page>();
            pageTabs.Add(Page.SMS);
            pageTabs.Add(Page.HSD);
            pageTabs.Add(Page.NON);
            pageIndex = 1;
        }
        #endregion

        private void _RightMFDbuttonPressed(object sender, MessageEventArgs e)
        {
            buttonPressed(Int32.Parse(e.Data));
        }

        /// <summary>
        /// When the left MFD changes to a page that is the same as the right MFD, right MFD changes to BLANK page and update the page tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeftMFDpageChanged(object sender, IntEventArgs e)
		{
            if ((int)current_page == e.value)
            {
                changePage(Page.BLANK);
                stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_PageTab);
                pageTabs[pageIndex] = current_page;
                WriteDataRefToAirManager("");
            }
        }

        /// <summary>
        /// When the SWAP button is pressed, the left MFD will report its current page. This event handler will receive it and change to that page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeftMFDpageReported(object sender, IntEventArgs e)
        {
            EvtInterface.OnRightMFDpageReported(this, new IntEventArgs((int)current_page));
            changePage((Page)e.value);
        }

        /// <summary>
        /// When the SWAP button is pressed, the left MFD will report its page tabs. This event handler will receive them and update them on Air Manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeftMFDtabsReported(object sender, MessageEventArgs e)
		{
            string tabs_str = "";
            for (int i = 0; i < pageTabs.Count(); i++)
            {
                Page page = pageTabs[i];
                tabs_str += page.ToString();
                tabs_str += (i < pageTabs.Count() - 1) ? " " : "";
            }
            EvtInterface.OnRightMFDtabsReported(this, new MessageEventArgs(tabs_str));
            string[] subs = e.Data.Split();
            for (int i = 0; i < pageTabs.Count(); i++)
            {
				if (Enum.IsDefined(typeof(Page), subs[i]))
                    pageTabs[i] = (Page)Enum.Parse(typeof(Page), subs[i]);
            }
            stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_PageTabs);
            WriteDataRefToAirManager(e.Data);
        }

        /// <summary>
        /// When the SWAP button is pressed, the left MFD will report its page index. This event handler will receive and update it on Air Manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _LeftMFDindexReported(object sender, IntEventArgs e)
		{
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_PageIndex);
            WriteDataRefToAirManager(e.value + 1);
            EvtInterface.OnRightMFDindexReported(this, new IntEventArgs(pageIndex));
            pageIndex = e.value;
        }

        protected override void buttonPressed(int button)
		{
            // the middle three of bottom buttons
			if (button >= 11 && button <= 13)
			{
                pageIndex = 13 - button;
                quickSwitchPage(pageTabs[pageIndex]);
                dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_PageIndex);
                WriteDataRefToAirManager(pageIndex+1);
                return;
            }
			base.buttonPressed(button);

            // if current page is no longer the one that page tab records
			if (current_page != pageTabs[pageIndex])
			{
                // only the pages from Page.FCR to Page.HSD will be recorded
				if (current_page  >= Page.FCR && current_page <= Page.HSD)
				{
                    stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_PageTab);
                    pageTabs[pageIndex] = current_page;
                    WriteDataRefToAirManager(current_page.ToString());
				}
			}
		}

		protected override void changePage(Page page)
        {
			if (current_page != page)
			{
                base.changePage(page);
                dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Right_Show_Page);
                EvtInterface.OnRightMFDpageChanged(this, new IntEventArgs((int)page));
                WriteDataRef((int)page);
			}
        }
    }
}
