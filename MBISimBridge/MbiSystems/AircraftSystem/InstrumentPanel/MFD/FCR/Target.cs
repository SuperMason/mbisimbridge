﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AircraftSystem.InstrumentPanel
{
    public class Target
    {
        // records the number of targets
        private static int targetNum;
        private static readonly double nmTokm;
        private static double radarHorizonBegin;
        private static double radarHorizonEnd;
        private static double radarElevationAngle;

        // indicate which target is currently locked
        public static bool IsLocked { get; set; }
        public double Bearing { get; set; }
        public double Distance { get; set; }
        public double Altitude { get; set; }
        public bool IsVisible { 
            get{
                // in the range of MFD
                if ((X > 0 && X < MFD.Width) && (Y > 0 && Y < MFD.Height))
                    if (IsLocked)
                        return true;
                    else
                        return (X > radarHorizonBegin && X < radarHorizonEnd); // in the range of FCR
                else
                    return false;
            }
        }
        public double X => this.Bearing * MFD.Middle / 60 + MFD.Width / 2; 
        public double Y => MFD.Height - (this.Distance / 1000 / nmTokm) * MFD.Height / FCR.RadarRange;
        #region Constructor
        static Target()
        {
            targetNum = 0;
            nmTokm = 1.852;
            radarHorizonBegin = MFD.Middle - FCR.RadarHorizon / 60 * MFD.Middle;
            radarHorizonEnd = MFD.Middle + FCR.RadarHorizon / 60 * MFD.Middle;
            radarElevationAngle = 4.9 * (FCR.RadarElevation + 1) / 2.0;
        }
        public Target()
        {
        }

        #endregion


    }
}
