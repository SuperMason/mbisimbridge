﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class FCR : InstrumentComponent
    {
        // TODO: send dataRefs from SimBridge to AM, including
        // from targets: IsLocked, X, Y,
        // from cursor: cursor_x, cursor_y, max, min
        #region Fields
        private static int target_index;
        private static int[] radarRange;
        private static int radarRangeIndex;
        private static int[] radarHorizon;
        private static int radarHorizonIndex;
        private static int[] radarElevation;
        private static int radarElevationIndex;
        private List<Target> targets;
        private Cursor cursor;
        #endregion

        #region Properties
        public static int RadarRange { get => radarRange[radarRangeIndex]; }
        public static int RadarHorizon { get => radarHorizon[radarHorizonIndex]; }
        public static int RadarElevation { get => radarElevation[radarElevationIndex]; }

        #region Constructor
        static FCR()
        {
            target_index = 0;
            radarRange = new int[6] { 5, 10, 20, 40, 80, 160 };
            radarRangeIndex = 0;
            radarHorizon = new int[3] { 1, 3, 6 };
            radarHorizonIndex = 0;
            radarElevation = new int[3] { 1, 2, 4 };
            radarElevationIndex = 0;
        }
        public FCR()
        {
            EvtInterface.ChangeRadarRange += _ChangeRadarRange;
            EvtInterface.ChangeRadarHorizon += _ChangeRadarHorizon;
            EvtInterface.ChangeRadarElevation += _ChangeRadarElevation;
            EvtInterface.PlaneTargetIndexChanged += _PlaneTargetIndexChanged;
            targets = new List<Target>();
            cursor = new Cursor();
        }
        #endregion

        private void _ChangeRadarRange(object sender, IntEventArgs e)
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Radar_Range);
            if (radarRangeIndex < 5 && e.value == 1)
                radarRangeIndex++;
            else if (radarRangeIndex > 0 && e.value == -1)
                radarRangeIndex--;
            WriteDataRef(RadarRange);
        }

        private void _ChangeRadarHorizon(object sender, EventArgs e)
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Radar_Horizon);
            radarHorizonIndex = (radarHorizonIndex + 1) % 3;
            WriteDataRef(RadarHorizon);
        }

        private void _ChangeRadarElevation(object sender, EventArgs e)
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Radar_Elevation);
            radarElevationIndex = (radarElevationIndex + 1) % 3;
            WriteDataRef(RadarElevation);
        }

        private void _PlaneTargetIndexChanged(object sender, MessageEventArgs e)
        {
            target_index = Int32.Parse(e.Data);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
