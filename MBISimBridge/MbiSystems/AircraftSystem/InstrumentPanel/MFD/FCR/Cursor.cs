﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AircraftSystem.InstrumentPanel
{
    public class Cursor
    {
        public static double Cursor_x;
        public static double Cursor_y;
        // normalize cursor_y from a range of 0-350 to a range of 0-1
        private static double normalized_cursor_y => (MFD.Width-Cursor_y)/MFD.Width;
        private static double vertical_degree => 4.9 * (FCR.RadarElevation + 1) / 2;
        //160 nm = 971520 ft (so swhat??)
        private static double ratio => 296320 * FCR.RadarRange / 160;
        //private static double max => ratio * Math.Tan(Math.PI/180 * vertical_degree/2) * normalized_cursor_y + altitude;
        //private static double min => ratio * Math.Tan(Math.PI/180 * -vertical_degree/2) * normalized_cursor_y + altitude;

        static Cursor()
        {
            Cursor_x = MFD.Middle;
            Cursor_y = MFD.Height / 2;
        }
    }
}
