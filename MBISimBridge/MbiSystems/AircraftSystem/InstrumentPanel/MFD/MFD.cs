﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class MFD: InstrumentComponent
    {
        public static int Width;
        public static int Height;
        public static double Middle => Width / 2.0;
        private bool isWorking = false;
        protected Joystick joystick = null;
        protected Thread thread = null;
        public enum MasterMode
        {
            NAV,
            AA,
            AG
        }
        public MasterMode masterMode = MasterMode.NAV;
        private bool passedSMS = false;
        #region Page
        protected enum Page
        {
            NON,
            BLANK,
            MAIN,
            FCR,
            SMS,
            HSD,
            AAINV,
            AASMS,
            TEST,
            DTE,
        }
        protected Dictionary<Page, string> pageNameRecord;
        /// <summary>
        /// current_page records the current page
        /// </summary>
        protected Page current_page;
        /// <summary>
        /// previous_page records the previous page
        /// </summary>
        protected Page previous_page = Page.NON;
        protected List<Page> pageTabs;
        protected int pageIndex;
        #endregion

        #region SMS
        private Dictionary<string, int> weapons;
        protected HashSet<Page> smsGroup;
        private int weaponIndex = 0;
        private string currentWeaponName => weapons.Keys.ToList()[weaponIndex];
        private int currentWeaponNum => weapons.Values.ToList()[weaponIndex];
        /// <summary>
        /// false: SLAVE, true: BORE
        /// </summary>
        private bool lineOfSight = false;
        #endregion


        #region Constructor
        static MFD()
        {
            Width = 350;
            Height = 350;
        }
        public MFD(DirectInput di, Guid id)
        {
            //joystick = new Joystick(di, id);
            //joystick.Properties.BufferSize = 128;
            //joystick.Acquire();
            EvtInterface.MFDpower += _MFDpower;
            EvtInterface.MasterModeChanged += _MasterModeChanged;
            //thread = new Thread(new ThreadStart(threadProc));
            //thread.Start();
            weapons = new Dictionary<string, int>();
            weapons.Add("GUN", 700);
            weapons.Add("A-9LM", 2);
            weapons.Add("A120B", 2);
            smsGroup = new HashSet<Page>();
            smsGroup.Add(Page.SMS);
            smsGroup.Add(Page.AASMS);
            smsGroup.Add(Page.AAINV);
        }
        #endregion

        private void _MFDpower(object sender, IntEventArgs e)
        {
            isWorking = e.value == 1;
            dataRef = GetDataRefElementFromInit(MbiDataRefs.Electrical_Cockpit_Electrical_MFD_on);
            WriteDataRef(e.value);
        }

        /// <summary>
		/// Either double press AA or AG will return to NAV mode
		/// </summary>
		/// <param name="mode"></param>
        protected void _MasterModeChanged(object sender, IntEventArgs e)
		{
            MasterMode toMode = (masterMode == (MasterMode)e.value) ? MasterMode.NAV : (MasterMode)e.value;
            switch (toMode)
			{
				case MasterMode.NAV:
					switch (current_page)
					{
						case Page.MAIN:
                            if (previous_page == Page.AAINV || previous_page == Page.AASMS)
                                changePage(Page.SMS);
                            break;
						case Page.AAINV:
						case Page.AASMS:
                            if (passedSMS)
                                changePage(Page.SMS);
                            else
                                changePage(Page.MAIN);
                            break;
						default:
							break;
					}
                    break;
				case MasterMode.AA:
                    if (current_page == Page.SMS || (current_page == Page.MAIN && masterMode == MasterMode.NAV))
                        changePage(Page.AAINV);
                    break;
				case MasterMode.AG:
					break;
				default:
                    changePage(previous_page);
                    break;
			}
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_MasterMode);
            masterMode = toMode;
            WriteDataRefToAirManager((int)masterMode);
        }


        /// <summary>
        /// changePage records the current_page as the previous page and change to the "page"
        /// </summary>
        /// <param name="page"></param>
        protected virtual void changePage(Page page) 
        {
            if (page == Page.SMS)
                passedSMS = true;
            else if (page == Page.MAIN)
                passedSMS = false;
            previous_page = current_page;
            current_page = page;
        }

        /// <summary>
        /// When one of the bottom three buttons is pressed, MFD will switch to the page it shows or return to the previous page
        /// </summary>
        /// <param name="page"></param>
        protected void quickSwitchPage(Page page)
        {
            switch (page)
            {
                //case Page.FCR:

                //    break;
                case Page.SMS:
                    if (smsGroup.Contains(current_page))
                        changePage(Page.MAIN);
                    else
                        changePage(Page.SMS);
                    break;

                //case Page.HSD:
                //    if (current_page == Page.HSD)
                //        changePage(Page.MAIN);
                //    else
                //        changePage(Page.HSD);
                //    break;
                default:
                    if (current_page == page || page == Page.NON)
                        changePage(Page.MAIN);
                    else
                        changePage(page);
                    break;
            }
        }


        /// <summary>
        /// buttonPressed is called whenever any of MFD buttons is pressed, and will do the corresponding action according to the current page.
        /// </summary>
        /// <param name="button"></param>
        protected virtual void buttonPressed(int button)
        {
            if (isWorking)
			{
                switch (button)
                {
                    case 14:
                        // SWAP
                        EvtInterface.OnMFDswap(this, EventArgs.Empty);
                        break;
                    default:
                        switch (current_page)
                        {
                            case Page.MAIN:
                                buttonPressedInMain(button);
                                break;
                            case Page.FCR:
                                buttonPressedInFCR(button);
                                break;
                            case Page.SMS:
                                buttonPressedInSMS(button);
                                break;
                            case Page.AAINV:
                                buttonPressedInAAINV(button);
                                break;
                            case Page.AASMS:
                                buttonPressedInAASMS(button);
                                break;
                            case Page.HSD:
                                break;
                            default:
                                break;
                        }
                        break;
                }
			}
        }


        private void buttonPressedInMain(int button)
		{
            switch (button)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    if (masterMode == MasterMode.NAV)
                    { 
                        changePage(Page.SMS);
					} 
                    else if (masterMode == MasterMode.AA)
			        {
                        changePage(Page.AASMS);
			        }
                    break;
                case 6:
                    changePage(Page.HSD);
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    break;
                case 10:
                    break;
                case 11:
                    break;
                case 12:
                    break;
                case 13:
                    break;
                case 14:
                    break;
                case 15:
                    break;
                case 16:
                    break;
                case 17:
                    break;
                case 18:
                    break;
                case 19:
                    changePage(Page.FCR);
                    break;
                default:
					break;
			}
		}

        private void buttonPressedInFCR(int button)
		{
            switch (button)
            {
                case 16:
                    EvtInterface.OnChangeRadarElevation(this, EventArgs.Empty);
                    break;
                case 17:
                    EvtInterface.OnChangeRadarHorizon(this, EventArgs.Empty);
                    break;
                case 18:
                    EvtInterface.OnChangeRadarRange(this, new IntEventArgs(-1));
                    break;
                case 19:
                    EvtInterface.OnChangeRadarRange(this, new IntEventArgs(1));
                    break;
                default:
                    break;
            }
        }

        private void buttonPressedInSMS(int button)
        {
            switch (button)
            {
                case 10: // S-J
                    break;
                default:
                    break;
            }
        }

        private void buttonPressedInAAINV(int button)
        {
            switch (button)
            {
                case 3: // AASMS
                    changePage(Page.AASMS);
                    break;
                case 6:
                    stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Weapon_Info);
                    ++weaponIndex;
                    weaponIndex %= weapons.Count();
                    WriteDataRefToAirManager(string.Format("{0}{1}", currentWeaponNum, currentWeaponName));
                    break;
                case 18:
                    stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_LineOfSight);
                    lineOfSight = !lineOfSight;
                    WriteDataRefToAirManager(lineOfSight ? "BORE" : "SLAVE");
                    break;
                default:
                    break;
            }
        }

        private void buttonPressedInAASMS(int button)
		{
            switch (button)
            {
                case 3: // AAINV
                    changePage(Page.AAINV);
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// The task the thread is running, to handle the situation of MFD hardware button being pressed
        /// </summary>
        private void threadProc()
        {
            while (true)
            {
                joystick.Poll();
                var datas = joystick.GetBufferedData();
                foreach (var state in datas)
                {
                    string str = Regex.Match(state.Offset.ToString(), @"\d+").Value;
                    //Console.WriteLine("{0}: {1} {2}", Name, Int32.Parse(str), state.Value);
                    if (state.Value == 128)
                        buttonPressed(Int32.Parse(str));
                }
            }
        }
        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
