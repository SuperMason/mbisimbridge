﻿using MBISimBridge.MbiSystems;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class LeftMFD : MFD
    {
        #region Fields
        private bool isSwap = false;
        #endregion
        #region Constructor
        public LeftMFD(DirectInput di, Guid id) : base(di, id)
        {
            EvtInterface.LeftMFDbuttonPressed += _LeftMFDbuttonPressed;
            EvtInterface.RightMFDpageChanged += _RightMFDpageChanged;
            EvtInterface.MFDswap += _MFDswap;
            EvtInterface.RightMFDpageReported += _RightMFDpageReported;
            EvtInterface.RightMFDtabsReported += _RightMFDtabsReported;
            EvtInterface.RightMFDindexReported += _RightMFDindexReported;
            Name = "MFD_left";
            InstrumentPanel.AddCommandEventToCatelog(Name, EvtInterface.OnLeftMFDbuttonPressed);
            // Start with FCR page
            current_page = Page.FCR;
            pageTabs = new List<Page>();
            pageTabs.Add(Page.FCR);
            pageTabs.Add(Page.NON);
            pageTabs.Add(Page.NON);
            pageIndex = 0;
        }
        #endregion

        private void _LeftMFDbuttonPressed(object sender, MessageEventArgs e)
        {
            buttonPressed(Int32.Parse(e.Data));
        }

        

        /// <summary>
        /// When the right MFD changes to a page that is the same as the left MFD, left MFD changes to BLANK page and update the page tab
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RightMFDpageChanged(object sender, IntEventArgs e)
		{
			if ((int)current_page == e.value)
			{
                changePage(Page.BLANK);
                stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_PageTab);
                pageTabs[pageIndex] = current_page;
                WriteDataRefToAirManager("");
            }
		}

        /// <summary>
        /// If the SWAP button is pressed, trigger an event that reports the current page, page tabs, and the page index
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MFDswap(object sender, EventArgs e)
        {
            isSwap = true;
            string tabs_str = "";
            EvtInterface.OnLeftMFDpageReported(this, new IntEventArgs((int)current_page));
            for (int i = 0; i < pageTabs.Count(); i++)
			{
                Page page = pageTabs[i];
                tabs_str += page.ToString(); 
                tabs_str += (i < pageTabs.Count() - 1)?" ":"";
			}
            EvtInterface.OnLeftMFDtabsReported(this, new MessageEventArgs(tabs_str));
            EvtInterface.OnLeftMFDindexReported(this, new IntEventArgs((int)pageIndex));
        }

        /// <summary>
        /// When the SWAP button is pressed, the right MFD will report its current page. This event handler will receive it and change to that page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RightMFDpageReported(object sender, IntEventArgs e)
		{
            changePage((Page)e.value);
		}

        /// <summary>
        /// When the SWAP button is pressed, the right MFD will report its page tabs. This event handler will receive them and update them on Air Manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RightMFDtabsReported(object sender, MessageEventArgs e)
		{
            string[] subs = e.Data.Split();
            for (int i = 0; i < pageTabs.Count(); i++)
            {
                if (Enum.IsDefined(typeof(Page), subs[i]))
                    pageTabs[i] = (Page)Enum.Parse(typeof(Page), subs[i]);
            }
            stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_PageTabs);
            WriteDataRefToAirManager(e.Data);
        }

        /// <summary>
        /// When the SWAP button is pressed, the right MFD will report its page index. This event handler will receive and update it on Air Manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _RightMFDindexReported(object sender, IntEventArgs e)
		{
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_PageIndex);
            WriteDataRefToAirManager(e.value + 1);
            pageIndex = e.value;
        }

        protected override void buttonPressed(int button)
        {
            // the middle three of bottom buttons
            if (button >= 11 && button <= 13)
            {
                pageIndex = 13 - button;
                quickSwitchPage(pageTabs[pageIndex]);
                dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_PageIndex);
                WriteDataRefToAirManager(pageIndex + 1);
                return;
            }
            base.buttonPressed(button);

            // if current page is no longer the one that page tab records
            if (current_page != pageTabs[pageIndex])
            {
                // only the pages from Page.FCR to Page.HSD will be recorded
                if (current_page >= Page.FCR && current_page <= Page.HSD)
                {
                    stringDataRef = GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_PageTab);
                    pageTabs[pageIndex] = current_page;
                    WriteDataRefToAirManager(current_page.ToString());
                }
            }
        }

        protected override void changePage(Page page)
        {
			if (current_page != page)
			{
                base.changePage(page);
                dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_MFD_Left_Show_Page);
                // if this page changing is caused by swap, there is no need to fire the LeftMFDpageChanged event
                if (!isSwap)
                    EvtInterface.OnLeftMFDpageChanged(this, new IntEventArgs((int)page));
                else
                    isSwap = false;
                WriteDataRef(((int)page));
			}
        }
    }
}
