﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class HUD : InstrumentComponent
    {
        public HUD()
        {
            EvtInterface.HUDPower += _HUDPower;
            dataRef = GetDataRefElementFromInit(XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn.DataRef);
            WriteDataRef(0);
        }

        private void _HUDPower(object sender, IntEventArgs e)
        {
            WriteDataRef(e.value);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}