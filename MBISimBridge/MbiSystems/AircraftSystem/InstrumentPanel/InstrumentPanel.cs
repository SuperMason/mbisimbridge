﻿using MBILibrary.Enums;
using MBISimBridge.MbiSystems;
using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class InstrumentPanel : MbiBaseSystem
    {
        #region Fields
        // private LeftMFD leftMFD = null;
        #region MFD
        Guid MFD1_Guid = Guid.Empty;
        Guid MFD2_Guid = Guid.Empty;
        private MFD leftMFD = null;
        private MFD rightMFD = null;
        #endregion
        private HUD HUD = null;
        private UFC UFC = null;
        private Gun gun = null;
        private FCR FCR = null;
        private MasterArmSwitch masterArmSwitch = null;
        private AAModeSwitch aaModeSwitch = null;
        private DirectInput directInput = null;
        #region Lights
        private LandingLight landingLight = null;
        private TaxiLight taxiLight = null;
        private NavigationLight navigationLight = null;
        #endregion

        #endregion
        #region Properties
        /// <summary>
        /// EventToBeTriggered sould be the Event declared in EvtInterface_InstrumentPanel, and used to fire the event when the switch of an enabled instrument is toggled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void CommandEvent(object sender, MessageEventArgs e);
        public delegate void DataRefEvent(object sender, IntEventArgs e);
        static public Dictionary<string, CommandEvent> CommandEventCatelog = null;
        static public Dictionary<string, DataRefEvent> DataRefEventCatelog = null;
        #endregion
        #region Constructor
        static InstrumentPanel()
        {
            CommandEventCatelog = new Dictionary<string, CommandEvent>();
            DataRefEventCatelog = new Dictionary<string, DataRefEvent>();
        }
        public InstrumentPanel()
        {
            triggerBySystem = TriggerByWhatSystem.FromInstrumentPanel;
            EvtInterface.InstrumentPanelDataRefWrtie += _InstrumentPanelDataRefWrtie;
            EvtInterface.InstrumentPanelCommand += _InstrumentPanelCommand;
            directInput = new DirectInput();

            // Find Guids for MFD1 and MFD2
            foreach (var deviceInstance in directInput.GetDevices(DeviceType.Supplemental,
                        DeviceEnumerationFlags.AllDevices))
            {
                if (deviceInstance.InstanceGuid == Guid.Parse("f09ab9e0-c481-11ec-8001-444553540000"))
                {
                    MFD1_Guid = deviceInstance.InstanceGuid;
                }
                else if (deviceInstance.InstanceGuid == Guid.Parse("f09b0800-c481-11ec-8002-444553540000"))
                {
                    MFD2_Guid = deviceInstance.InstanceGuid;
                }
            }
            // If MFD1 not found, throws an error
            //if (MFD1_Guid == Guid.Empty)
            //{
            //    Console.WriteLine("MFD1 is not found.");
            //    Console.ReadKey();
            //    Environment.Exit(1);
            //}
            // If MFD2 not found, throws an error
            //if (MFD2_Guid == Guid.Empty)
            //{
            //    Console.WriteLine("MFD2 is not found.");
            //    Console.ReadKey();
            //    Environment.Exit(1);
            //}
            rightMFD = new RightMFD(directInput, MFD1_Guid);
            leftMFD = new LeftMFD(directInput, MFD2_Guid);

            HUD = new HUD();
            UFC = new UFC();
            gun = new Gun();
            FCR = new FCR();
            masterArmSwitch = new MasterArmSwitch();
            aaModeSwitch = new AAModeSwitch();
            landingLight = new LandingLight();
            taxiLight = new TaxiLight();
            navigationLight = new NavigationLight();
        }
        #endregion

        #region Methods
        static public void AddCommandEventToCatelog(string name, CommandEvent eventToBeTriggered)
        {
            if (!CommandEventCatelog.ContainsKey(name)) CommandEventCatelog.Add(name, eventToBeTriggered);
        }

        static public void AddDataRefEventToCatelog(string name, DataRefEvent eventToBeTriggered)
        {
            if (!DataRefEventCatelog.ContainsKey(name)) DataRefEventCatelog.Add(name, eventToBeTriggered);
        }

        #region EventHandlers
        private void _InstrumentPanelDataRefWrtie(object sender, DataRefEventArgs e)
        {
            string theDataRefName = e.DataBus4Object.DataRef;
            string instrument = theDataRefName.Substring(theDataRefName.LastIndexOf('/') + 1);
            int value = e.DataBus4Object.ObjVal2Int;
            // Trigger the corresponding event
            if (DataRefEventCatelog.ContainsKey(instrument))
                DataRefEventCatelog[instrument](this, new IntEventArgs(value));
        }

        private void _InstrumentPanelCommand(object sender, CommandEvevntArgs e)
        {
            string commandName = e.Command.Name;
            int pos = commandName.LastIndexOf('/');
            string button = commandName.Substring(pos + 1);
            commandName = commandName.Substring(0, pos);
            pos = commandName.LastIndexOf('/');
            string instrument = commandName.Substring(pos + 1);
            // Trigger the corresponding event
            if (CommandEventCatelog.ContainsKey(instrument))
                CommandEventCatelog[instrument](this, new MessageEventArgs(button));
        }
        #endregion
        #endregion
    }
}
