﻿using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	public class COM_1: PageBase
	{
		
		/// <summary>
		/// There are 4 entries in COM1 page
		/// </summary>
		private enum Entry
		{
			UHF,
			NB,
			PRE,
			PRE_UHF
		}
		private static Dictionary<Entry, EntryBase> Entries = null;
		private static PRE_entry_COM1 pre_entry => Entries[Entry.PRE] as PRE_entry_COM1;
		private static UHF_entry uhf_entry => Entries[Entry.UHF] as UHF_entry;
		private EntryBase entry => Entries[highlightEntry];
		/// <summary>
		/// highlightEntry represents currently highlighted entry
		/// </summary>
		private Entry highlightEntry => (Entry)(highlightIndex - 1);
		
		static COM_1()
		{
			Entries = new Dictionary<Entry, EntryBase>();
			Entries.Add(Entry.UHF, new UHF_entry());
			Entries.Add(Entry.NB, new NB_entry());
			Entries.Add(Entry.PRE, new PRE_entry_COM1());
			Entries.Add(Entry.PRE_UHF, new PRE_UHF_entry());
		}

		public COM_1()
		{
			triggerBySystem = TriggerByWhatSystem.FromPageBase;
			EvtInterface.COM1BlinkStart += _BlinkStart;
			EvtInterface.COM1BlinkStop += _BlinkStop;
			entryNum = Entries.Count();
		}


		/// <summary>
		/// Update each entry
		/// </summary>
		public override void Update()
		{
			foreach (var _entry in Entries.Values)
			{
				_entry.Update();
			}
		}

		/// <summary>
		/// Make the highlighted entry blink
		/// </summary>
		/// <param name="start">0: stop, 1: start</param>
		public override void _BlinkStart(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_Blink);
			base._BlinkStart(sender, e);
		}

		/// <summary>
		/// Make the highlighted entry stop blinking
		/// </summary>
		public override void _BlinkStop(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_Blink);
			base._BlinkStop(sender, e);
		}

		/// <summary>
		/// When preparing for an entry, reset the temporary data beforehand
		/// </summary>
		public override void Reset()
		{
			foreach (var _entry in Entries.Values)
			{
				_entry.Reset();
			}
		}

		/// <summary>
		/// Before leaving the COM1 page, restore everything in it.
		/// </summary>
		public override void RestorePage()
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_HighlightIndex);
			base.RestorePage();
			// To update UHF entry in CNI page
			uhf_entry.Update();
		}

		/// <summary>
		/// When the enter button is pressed, do the actions according to the current highlight entry
		/// </summary>
		public override void EnterPressed()
		{
			entry.Enter();
		}

		/// <summary>
		/// When the recall button is pressed, do the actions according to the current highlight entry
		/// </summary>
		public override void RecallPressed()
		{
			recalled = !recalled;
			entry.Recall(recalled);
		}

		/// <summary>
		/// When a number button is pressed, do the actions according to the current highight entry
		/// </summary>
		/// <param name="number"></param>
		public override void NumberPressed(Button number)
		{
			entry.Input((int)number);
			recalled = false;
		}

		/// <summary>
		/// When the UP or DOWN button is pressed. this function will be called to change the highlight entry
		/// </summary>
		/// <param name="change">the amount of change</param>
		public override void ChangeHighlightEntry(int change)
		{
			// restore the current entry before changing
			RestoreEntry();
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_HighlightIndex);
			base.ChangeHighlightEntry(change);
			// prepare data for the highlight entry
			PrepareEntry();
		}

		/// <summary>
		/// Prepare datas before changing the highlight entry
		/// </summary>
		public override void PrepareEntry()
		{
			// reset recalled for every entry
			recalled = false;
			entry.Reset();
		}

		/// <summary>
		/// If changing highlight entry while editing without pressing enter button, restore the entry to the original state
		/// </summary>
		public override void RestoreEntry()
		{
			// if the current entry is blinking, make it stop
			_BlinkStop(this, EventArgs.Empty);
			entry.Update();
		}

		public override void ChangeValue(int change)
		{
			pre_entry.Change(change);
		}
	}
}
