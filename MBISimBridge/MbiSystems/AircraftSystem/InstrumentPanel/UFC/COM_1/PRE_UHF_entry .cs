﻿using AircraftSystem.InstrumentPanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public class PRE_UHF_entry : FreqEntryBase
	{
		private float[] preset_UHF;
		private int index;
		public PRE_UHF_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_PRE_UHF)) 
		{
			EvtInterface.COM1PreChanged += _COM1PreChanged;
			preset_UHF = new float[20];
			for (int i = 0; i < 20; i++)
			{
				preset_UHF[i] = 300 + i;
			}
		}

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
		
		private void _COM1PreChanged(object sender, IntEventArgs e)
		{
			index = e.value;
			WriteDataRef(preset_UHF[index].ToString("F2"));
		}

		protected override void setFreq(float frequency)
		{
			preset_UHF[index] = frequency;
		}

		public override void Enter()
		{
			float tmp;
			bool success = float.TryParse(tmp_freq, out tmp);
			if (success)
			{
				if (tmp >= 225 && tmp <= 399.5)
				{
					// make sure that UHF ends with an even digit
					if ((tmp * 100) % 2 == 1)
						tmp = (tmp * 100 - 1) / 100;
					setFreq(tmp);
					Update();
				}
				else
				{
					// out of range -> blink
					EvtInterface.OnCOM1BlinkStart(this, EventArgs.Empty);
				}
			}
			else
			{
				// incorrect format -> blink
				EvtInterface.OnCOM1BlinkStart(this, EventArgs.Empty);
			}
			Reset();
		}

		protected override string getFreqString()
		{
			return preset_UHF[index].ToString("F2");
		}
	}
}
