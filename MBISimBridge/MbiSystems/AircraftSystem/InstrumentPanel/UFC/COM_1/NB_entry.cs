﻿using AircraftSystem.InstrumentPanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class NB_entry : EntryBase
	{
		public NB_entry() : base(GetStringDataRefElementFromInit("TBD")) { } // 無 TBD StringDataRef 定義!!

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}

		public override void Enter()
		{
			throw new NotImplementedException();
		}

		public override void Input(int number)
		{
			throw new NotImplementedException();
		}

		public override void Recall(bool recalled)
		{
			throw new NotImplementedException();
		}

		public override void Reset()
		{
			//throw new NotImplementedException();
		}

		public override void Update()
		{
			//throw new NotImplementedException();
		}
	}
}
