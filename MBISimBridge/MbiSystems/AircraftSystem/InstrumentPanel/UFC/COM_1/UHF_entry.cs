﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class UHF_entry : FreqEntryBase
	{
		public UHF_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_UHF)) 
		{
			freq = 305;
		}
		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
		protected override void setFreq(float frequency)
		{
			base.setFreq(frequency);
			Update();
			// TODO: return to CNI page
			EvtInterface.OnReturnToCNIpage(this, EventArgs.Empty);
		}
		public override void Enter()
		{
			if (tmp_freq == "")
			{
				// TODO: return to the previous page
				EvtInterface.OnReturnToCNIpage(this, EventArgs.Empty);
			}
			else
			{
				float tmp = float.Parse(tmp_freq);
				if (tmp >= 1 && tmp <= 20)
				{
					setFreq(tmp);
				}
				else if (tmp >= 225 && tmp <= 399.5)
				{
					// make sure that UHF ends with an even digit
					if (tmp * 100 % 2 == 1)
						tmp = (tmp * 100 - 1) / 100;
					setFreq(tmp);
				}
				else
				{
					// out of range -> blink
					EvtInterface.OnCOM1BlinkStart(this, EventArgs.Empty);
					return;
				}
				Reset();
			}
		}

		public override void Reset()
		{
			base.Reset();
			EvtInterface.OnCOM1BlinkStop(this, EventArgs.Empty);
		}
        public override void Recall(bool recalled)
        {
			if (!COM_1.blinking)
            {
				base.Recall(recalled);
				if (recalled)
					EvtInterface.OnCOM1BlinkStop(this, EventArgs.Empty);
			}
			else
            {
				tmp_freq = "";
				WriteDataRef(getFreqString());
				EvtInterface.OnCOM1BlinkStop(this, EventArgs.Empty);
			}

		}
    }
}
