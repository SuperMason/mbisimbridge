﻿
using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	public class CNI: PageBase
	{
		private enum Entry
		{
			UHF,
			VHF,
			STPT
		};

		public CNI()
		{
			triggerBySystem = TriggerByWhatSystem.FromPageBase;
			entryNum = 3;
		}

		private Entry highlightEntry => (Entry)(highlightIndex - 1);
		private bool showWind = false;


		public override void ButtonPressed(Button button)
		{
			switch (button)
			{
				case Button.SEQ: // show wind
					dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_CNI_Show_Wind);
					showWind = !showWind;
					WriteDataRef(showWind ? 1 : 0);
					break;
				case Button.UP:
					ChangeHighlightEntry(-1);
					break;
				case Button.DOWN:
					ChangeHighlightEntry(1);
					break;
				case Button.INCR:
					ChangeValue(1);
					break;
				case Button.DECR:
					ChangeValue(-1);
					break;
				default:
					break;
			}
		}

		public override void _BlinkStart(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		public override void _BlinkStop(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		public override void RestorePage()
		{
			// no need to restore
		}

		public override void EnterPressed()
		{
			throw new NotImplementedException();
		}

		public override void NumberPressed(Button number)
		{
			throw new NotImplementedException();
		}

		public override void RecallPressed()
		{
			throw new NotImplementedException();
		}

		public override void ChangeHighlightEntry(int change)
		{
			// change to dataRef of highlightIndex
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_CNI_HighlightIndex);
			base.ChangeHighlightEntry(change);
		}

		public override void PrepareEntry()
		{
			
		}

		public override void RestoreEntry()
		{
			
		}

		public override void Update()
		{
			// no need to restore
		}

		public override void Reset()
		{
			throw new NotImplementedException();
		}

		public override void ChangeValue(int change)
		{
			switch (highlightEntry)
			{
				case Entry.UHF:
					break;
				case Entry.VHF:
					break;
				case Entry.STPT:
					EvtInterface.OnSTPTChanged(this, new IntEventArgs(change));
					break;
				default:
					break;
			}
		}
	}
}
