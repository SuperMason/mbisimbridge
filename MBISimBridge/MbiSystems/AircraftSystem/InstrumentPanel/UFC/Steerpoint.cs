﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{

	public class Steerpoint
	{
		public Degree Lat { get; set; }
		public Degree Lng { get; set; }
		public int Elev { get; set; }
		public Steerpoint(Degree lat, Degree lng, int elev)
		{
			Lat = lat;
			Lng = lng;
			Elev = elev;
		}
	}
}
