﻿using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
    public class COM_2 : PageBase
    {
        /// <summary>
        ///There are 4 entries in COM2 page
        /// </summary>
        ///
        private enum Entry
        {
            VHF,
            NB,
            PRE,
            PRE_VHF
        }
        private static Dictionary<Entry, EntryBase> Entries = null;
        private static PRE_entry_COM2 pre_entry => Entries[Entry.PRE] as PRE_entry_COM2;
        private static VHF_entry vhf_entry => Entries[Entry.VHF] as VHF_entry;
        private EntryBase entry => Entries[highlightEntry];
        /// <summary>
        /// highlightEntry represents currently highlighted entry
        /// </summary>
        private Entry highlightEntry => (Entry)(highlightIndex - 1);
        
        static COM_2()
        {
            Entries = new Dictionary<Entry, EntryBase>();
            Entries.Add(Entry.VHF, new VHF_entry());
            Entries.Add(Entry.NB, new NB_entry());
            Entries.Add(Entry.PRE, new PRE_entry_COM2());
            Entries.Add(Entry.PRE_VHF, new PRE_VHF_entry());
        }

        public COM_2()
        {
            triggerBySystem = TriggerByWhatSystem.FromPageBase;
            EvtInterface.COM2BlinkStart += _BlinkStart;
            EvtInterface.COM2BlinkStop += _BlinkStop;
            entryNum = Entries.Count();
        }

        /// <summary>
        /// Update each entry
        /// </summary>
        public override void Update()
        {
            foreach(var _entry in Entries.Values)
            {
                _entry.Update();
            }
        }

        /// <summary>
        /// Make the highlighted entry blink
        /// </summary>
        /// <param name="start">0: stop, 1: start</param>
        public override void _BlinkStart(object sender, EventArgs e)
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM2_Blink);
            base._BlinkStart(sender, e);
        }

        /// <summary>
        /// Make the highlighted entry stop blinking
        /// </summary>
        public override void _BlinkStop(object sender, EventArgs e)
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM2_Blink);
            base._BlinkStop(sender, e);
        }

        /// <summary>
        /// When preparing for an entry, reset the temporary data beforehand
        /// </summary>
        public override void Reset()
        {
            foreach (var _entry in Entries.Values)
            {
                _entry.Reset();
            }
        }

        /// <summary>
        /// Before leaving the COM2 page, restore everything in it.
        /// </summary>
        public override void RestorePage()
        {
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM2_HighlightIndex);
            base.RestorePage();
            // To update VHF entry in CNI page
            vhf_entry.Update();
        }
        
        /// <summary>
        /// When the enter button is pressed, do the actions according to the current highlight entry
        /// </summary>
        public override void EnterPressed()
        {
            entry.Enter();
        }

        /// <summary>
        /// When the recall button is pressed, do the actions according to the current highlight entry
        /// </summary>
        public override void RecallPressed()
        {
            entry.Recall(recalled);
            recalled = !recalled;
        }

        /// <summary>
        /// When a number button is pressed, do the actions according to the current highight entry
        /// </summary>
        /// <param name="number"></param>
        public override void NumberPressed(Button number)
        {
            entry.Input((int)number);
            recalled = false;
        }

        /// <summary>
        /// When the UP or DOWN button is pressed. this function will be called to change the highlight entry
        /// </summary>
        /// <param name="change">the amount of change</param>
        public override void ChangeHighlightEntry(int change)
        {
            RestoreEntry();
            dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM2_HighlightIndex);
            base.ChangeHighlightEntry(change);
            PrepareEntry();
        }

        /// <summary>
        /// Prepare datas before changing the highlight entry
        /// </summary>
        public override void PrepareEntry()
        {
            // reset recalled for every entry
            recalled = false;
            entry.Reset();
        }

        /// <summary>
        /// If changing highlight entry while editing without pressing enter button, restore the entry to the original state
        /// </summary>
        public override void RestoreEntry()
        {
            // if the current entry is blinking, make it stop
            _BlinkStop(this, EventArgs.Empty);
            entry.Update();
        }

        public override void ChangeValue(int change)
        {
            pre_entry.Change(change);
        }
    }
}
