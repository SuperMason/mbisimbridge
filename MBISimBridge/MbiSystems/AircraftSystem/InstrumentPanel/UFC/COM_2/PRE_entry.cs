﻿using AircraftSystem.InstrumentPanel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public class PRE_entry : EntryBase
	{
		private int tmp_index = 0;
		// 0-based index
		private int index = 0;

		// 1-based index
		public int Index { 
			get=>index+1; 
			set { index = value; }
		}
		public PRE_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM1_PRE)) { }

		public override void Change(int change)
		{
			index += change;
			index = (index + 20) % 20;
			WriteDataRef(Index.ToString());
			EvtInterface.OnCOM1PreChanged(this, new IntEventArgs(index));
		}

		public override void Enter()
		{
			if (tmp_index <= 20)
			{
				index = tmp_index - 1;
				EvtInterface.OnCOM1PreChanged(this, new IntEventArgs(index));
				tmp_index = 0;
			}
			else
			{
				EvtInterface.OnCOM1BlinkStart(this, EventArgs.Empty);
			}
		}

		public override void Input(int number)
		{
			if (tmp_index < 10)
			{
				tmp_index *= 10;
				tmp_index += number;
				WriteDataRef(tmp_index.ToString());
			}
		}

		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				tmp_index /= 10;
				WriteDataRef(tmp_index.ToString());
			}
			else
			{
				tmp_index = 0;
				WriteDataRef(Index.ToString());
			}
		}

		public override void Reset()
		{
			tmp_index = 0;
		}

		public override void Update()
		{
			WriteDataRef(Index.ToString());
		}
	}
}
