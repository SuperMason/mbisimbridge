﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    class PRE_VHF_entry : FreqEntryBase
    {
        private float[] preset_VHF;
        private int index;
        public PRE_VHF_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_COM2_PRE_VHF))
        {
            EvtInterface.COM2PreChanged += _COM2PreChanged;
            preset_VHF = new float[20];
            for (int i = 0; i < 20; i++)
            {
                preset_VHF[i] = 108 + i;
            }

        }
        public override void Change(int change)
        {
            throw new NotImplementedException();
        }

        private void _COM2PreChanged(object sender, IntEventArgs e)
        {
            index = e.value;
            WriteDataRef(preset_VHF[index].ToString("F2"));
        }

        protected override void setFreq(float frequency)
        {
            preset_VHF[index] = frequency;
        }

        public override void Enter()
        {
            float tmp;
            bool success = float.TryParse(tmp_freq, out tmp);
            if(success)
            {
                if (tmp >= 108 && tmp <= 151.96)
                {
                    // make sure that UHF ends with an even digit
                    if ((tmp * 100) % 2 == 1)
                        tmp = (tmp * 100 - 1) / 100;
                    setFreq(tmp);
                    Update();
                }
                else 
                {
                    // out of range -> blink
                    EvtInterface.OnCOM2BlinkStart(this, EventArgs.Empty);
                }
            }
            else
            {
                // incorrect format -> blink
                EvtInterface.OnCOM2BlinkStart(this, EventArgs.Empty);
            }
            Reset();
        }

        protected override string getFreqString()
        {
            return preset_VHF[index].ToString("F2");
        }

    }
}
