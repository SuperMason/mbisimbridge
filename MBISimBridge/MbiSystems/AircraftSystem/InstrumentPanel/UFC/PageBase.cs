﻿using AircraftSystem;
using MBILibrary.Enums;
using MBILibrary.Util;
using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	public abstract class PageBase: MbiBaseSystem, IPage
	{
		protected static DataRefElement dataRef = null;
        protected static StringDataRefElement stringDataRef = null; // 宣告 static 卻給 N 個 object 引用, 會有同時存取, 物件被覆蓋的潛在問題
        protected bool recalled = false;
        public static bool blinking = false;
        /// <summary>
		/// highlightIndex is the index sent to Air Manager
		/// </summary>
		protected int highlightIndex = 1;
		/// <summary>
		/// The number of entries
		/// </summary>
		protected int entryNum;
        /// <summary>
        /// Write dataRef to AircraftSystem
        /// </summary>
        /// <param name="obj"></param>
        protected void WriteDataRef(object obj)
        {
			try
			{
				if (obj.ValueIsString() && stringDataRef != null)
				{
					WriteDataRefToAirManager4AircraftSystem(stringDataRef, obj);
				}
				else if (obj.ValueIsSingle() && dataRef != null)
				{
					WriteDataRefToAirManager4AircraftSystem(dataRef, obj);
				}
			}
			catch (Exception e)
			{
				Console.WriteLine(e.ToString());
				throw;
			}
		}
        
        public abstract void Update();
        public virtual void ButtonPressed(Button button)
		{
			{
				switch (button)
				{
					case Button b when (b >= Button.ZERO && b <= Button.NINE):
						NumberPressed(b);
						break;
					case Button.RCL:
						RecallPressed();
						break;
					case Button.ENTR:
						EnterPressed();
						break;
					case Button.UP:
						ChangeHighlightEntry(-1);
						break;
					case Button.DOWN:
						ChangeHighlightEntry(1);
						break;
					case Button.INCR:
						ChangeValue(1);
						break;
					case Button.DECR:
						ChangeValue(-1);
						break;
					default:
						break;
				}
			}
		}
		public virtual void _BlinkStart(object sender, EventArgs e)
		{
			if (!blinking)
			{
				blinking = true;
				WriteDataRef(1);
			}
		}
		public virtual void _BlinkStop(object sender, EventArgs e)
		{
			if (blinking)
			{
				blinking = false;
				WriteDataRef(0);
			}
		}
		public abstract void Reset();
		public virtual void RestorePage()
		{
			highlightIndex = 1;
			WriteDataRef(highlightIndex);
			Reset();
		}
		public abstract void EnterPressed();
		public abstract void NumberPressed(Button number);
		public abstract void RecallPressed();
		public virtual void ChangeHighlightEntry(int change)
		{
			highlightIndex += change;
			if (highlightIndex == 0)
				highlightIndex = entryNum;
			if (highlightIndex == entryNum + 1)
				highlightIndex = 1;
			WriteDataRef(highlightIndex);
		}
		public abstract void ChangeValue(int change);
		public abstract void PrepareEntry();
		public abstract void RestoreEntry();
	}
}
