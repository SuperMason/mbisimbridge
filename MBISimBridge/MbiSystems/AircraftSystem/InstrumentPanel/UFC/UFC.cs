﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
    public class UFC : InstrumentComponent
    {
		public enum Page
		{
			NOT_FOUND,
			CNI,
			COM1,
			COM2,
			IFF,
			LIST,
			T_ILS,
			ALOW,
			STPT,
			CRUS,
			TIME,
			MARK,
			FIX,
			A_CAL,
			DEST,
			BNGO,
			VIP,
			INTG,
			NAV,
			MAN,
			INS,
			DLINK,
			CMDS,
			MODE,
			VRP,
			MISC
		}

		public enum Button
		{
			ZERO,
			ONE,
			TWO,
			THREE,
			FOUR,
			FIVE,
			SIX,
			SEVEN,
			EIGHT,
			NINE,
			RCL,
			ENTR,
			AA,
			AG,
			RTN,
			SEQ,
			UP,
			DOWN,
			INCR,
			DECR,
			COM1,
			COM2,
			IFF,
			LIST
		}

		private Dictionary<Page, PageBase> pages = null;
		private Page currentPage => (Page)pageStack.Peek();
		private static Dictionary<Button, Page> dictionary;
		private Stack pageStack = null;
		static UFC()
		{
			dictionary = new Dictionary<Button, Page> {
				{ Button.FOUR, Page.STPT },
				{ Button.SIX, Page.TIME },
				{ Button.RTN, Page.CNI },
				{ Button.COM1, Page.COM1 },
				{ Button.COM2, Page.COM2 },
				{ Button.IFF, Page.IFF },
				{ Button.LIST, Page.LIST }
			};
		}

		public UFC()
        {
            EvtInterface.UFCpower += _UFCpower;
			EvtInterface.ICPswitchToggled += _ICPswitchToggled;
			EvtInterface.APswitchToggled += _APswitchToggled;
			EvtInterface.ICPbuttonPressed += _ICPbuttonPressed;
			EvtInterface.ReturnToCNIpage += _ReturnToCNIpage;
			Name = "UFC";
			InstrumentPanel.AddDataRefEventToCatelog(Name, EvtInterface.OnICPswitchToggled);
			InstrumentPanel.AddDataRefEventToCatelog("AP", EvtInterface.OnAPswitchToggled);
			InstrumentPanel.AddCommandEventToCatelog(Name, EvtInterface.OnICPbuttonPressed);
			pageStack = new Stack();
			pageStack.Push(Page.CNI);
			pages = new Dictionary<Page, PageBase>();
			pages.Add(Page.CNI, new CNI());
			pages.Add(Page.COM1, new COM_1());
			pages.Add(Page.COM2, new COM_2());
			pages.Add(Page.STPT, new STPT());
			pages.Add(Page.TIME, new Time());
			dataRef = GetDataRefElementFromInit(MbiDataRefs.Electrical_Cockpit_Electrical_UFC_on);
            WriteDataRef(0);
        }

        private void _UFCpower(object sender, IntEventArgs e)
        {
            WriteDataRef(e.value);
        }

		private void _ICPswitchToggled(object sender, IntEventArgs e)
		{

		}
		private void _APswitchToggled(object sender, IntEventArgs e)
		{

		}

		private void _ReturnToCNIpage(object sender, EventArgs e)
		{
			changePage(Page.CNI);
		}

		private void _ICPbuttonPressed(object sender, MessageEventArgs e)
		{
			int intButton = Int32.Parse(e.Data);
			Button button = (Button)intButton;
			switch (button)
			{
				case Button.RTN:
					changePage(Page.CNI);
					break;
				case Button b when (b >= Button.COM1 && b <= Button.LIST || (b >= Button.ZERO && b <= Button.NINE && currentPage == Page.CNI)):
					// In CNI page, some of the number buttons are multi-function buttons. Pressing them will change page shown on the button. e.g. pressing 4 will go to STPT page
					changePage(getPage(b));
					break;
				case Button.AA:
					EvtInterface.OnMasterModeChanged(this, new IntEventArgs((int)MFD.MasterMode.AA));
					break;
				case Button.AG:
					EvtInterface.OnMasterModeChanged(this, new IntEventArgs((int)MFD.MasterMode.AG));
					break;
				default:
					if (pages.ContainsKey(currentPage))
						pages[currentPage].ButtonPressed(button);
					break;
			}
		}

		/// <summary>
		/// Get the page with the binding button
		/// </summary>
		/// <param name="button"></param>
		/// <returns></returns>
		private Page getPage(Button button)
		{
			if (dictionary.ContainsKey(button))
				return dictionary[button];
			else
				return Page.NOT_FOUND;
		}
		/// <summary>
		/// Before changing page, prepare the updated information to display according to the page about to change to
		/// </summary>
		private void preparePage(Page page)
		{
			if (pages.ContainsKey(page))
				pages[page].Update();
		}

		/// <summary>
		/// Before changing page, restore the current page to its original state. e.g. restore the highlight entry to the first one.
		/// </summary>
		private void restorePage(Page page)
		{
			if (pages.ContainsKey(page))
				pages[page].RestorePage();
		}

		/// <summary>
		/// Make Air Manager change page and use pageStack to track the page activities
		/// </summary>
		/// <param name="page"></param>
		private void changePage(Page page)
		{
			// restore the current page
			restorePage(currentPage);

			if (page == Page.CNI)
			{
				// clear the stack
				pageStack.Clear();
				pageStack.Push(Page.CNI);
			}
			else
			{

				if (currentPage == page)
				{
					// TODO: return to the previous page
					pageStack.Pop();
				}
				else
				{
					pageStack.Push(page);
				}
			}
			// prepare for the next page
			preparePage(currentPage);
			// TODO: Write dataRef to make AM change page
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_page);
			WriteDataRefToAirManager((int)currentPage);
		}

		public override void Failure()
        {
            throw new NotImplementedException();
        }

        protected override void Reset()
        {
            throw new NotImplementedException();
        }
    }
}
