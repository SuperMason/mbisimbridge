﻿using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public abstract class TimeEntryBase : EntryBase
    {
		protected string tmp_time;
		protected string tmp_second;
		protected string tmp_minute;
		protected string tmp_hour;
		protected string input_time = "";
		protected string time_str = "00:00:00";
		protected bool IsEditing = false;
		public TimeEntryBase(StringDataRefElement s): base(s) {}

		public override void Update()
		{
			WriteDataRef(getTimeString());
		}
        public override void Input(int number)
		{
			if (input_time.Length < 8)
			{
				input_time = input_time + number.ToString();
				int str_length = input_time.Length;
				if (str_length == 2 || str_length == 5)
					input_time += ":";
			}
			WriteDataRef(input_time);
		}
		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				if(!input_time.Equals(""))
					input_time = input_time.Substring(0, input_time.Length - ((input_time.Length == 3 || input_time.Length == 6)?2:1));
				WriteDataRef(input_time);
			} else
			{
				input_time = "";
				WriteDataRef(getTimeString());
				EvtInterface.OnTimeBlinkStop(this, EventArgs.Empty);
			}
		}
		public override void Reset()
		{
			input_time = "";
		}

		protected virtual void setTime()
		{
			time_str = tmp_time;
		}
		protected virtual string getTimeString()
		{
			return time_str;
		}
	}
}
