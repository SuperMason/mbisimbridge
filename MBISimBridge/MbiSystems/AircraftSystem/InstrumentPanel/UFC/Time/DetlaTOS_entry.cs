﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage;


namespace AircraftSystem.InstrumentPanel
{
    class DetlaTOS_entry : TimeEntryBase
    {
        public DetlaTOS_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_DeltaTOS))
        {

        }
        public override void Update()
        {
            //throw new NotImplementedException();
        }
        public override void Enter()
        {
            //throw new NotImplementedException();
        }
        public override void Recall(bool recalled)
        {
            //throw new NotImplementedException();
        }
        public override void Reset()
        {
            //throw new NotImplementedException();
        }
        public override void Change(int change)
        {
            throw new NotImplementedException();
        }
        public override void Input(int number)
        {
            //throw new NotImplementedException();
        }

    }
}
