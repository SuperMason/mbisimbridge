﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace AircraftSystem.InstrumentPanel
{
    class Date_entry : EntryBase
    {
        private string tmp_day;
        private string tmp_month;
        private string date => tmp_month + "/" + tmp_day + "/22";
        public Date_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_Date))
        {
            EvtInterface.XPlaneDayChanged += _DayChanged;
            EvtInterface.XPlaneMonthChanged += _MonthChanged;
        }

        public void _DayChanged(object sender, IntEventArgs e)
        {
            tmp_day = e.value.ToString("D2");
        }
        public void _MonthChanged(object sender, IntEventArgs e)
        {
            tmp_month = e.value.ToString("D2");
        }
        public override void Update()
        {
            WriteDataRef(date);
        }
        public override void Enter()
        {
            throw new NotImplementedException();
        }
        public override void Recall(bool recalled)
        {
            throw new NotImplementedException();
        }
        public override void Reset()
        {
            //
        }
        public override void Change(int change)
        {
            //throw new NotImplementedException();
        }
        public override void Input(int number)
        {
            //throw new NotImplementedException();
        }

    }
}
