﻿using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	public class Time : PageBase
	{
		/// <summary>
		/// There are 4 entries in Time page
		/// </summary>
		private enum Entry
		{
			SYS_TIME,
			HACK_TIME,
			DELTA_TOS,
			DATE
		}
		private static Dictionary<Entry, EntryBase> Entries = null;
		/// <summary>
		/// highlightEntry represents currently highlighted entry
		/// </summary>
		
		private EntryBase entry => Entries[highlightEntry];
		private Entry highlightEntry => (Entry)(highlightIndex - 1);
		static Time()
        {
			Entries = new Dictionary<Entry, EntryBase>();
			Entries.Add(Entry.SYS_TIME, new SystemTime_entry());
			Entries.Add(Entry.HACK_TIME, new HackTime_entry());
			Entries.Add(Entry.DELTA_TOS, new DetlaTOS_entry());
			Entries.Add(Entry.DATE, new Date_entry());
        }
		public Time()
        {
			triggerBySystem = TriggerByWhatSystem.FromPageBase;
			EvtInterface.TimeBlinkStart += _BlinkStart;
			EvtInterface.TimeBlinkStop += _BlinkStop;
			entryNum = Entries.Count();
		}
		public override void Update()
		{
			foreach (var _entry in Entries.Values)
			{
				_entry.Update();
			}
		}
		public override void ChangeValue(int change)
		{
			throw new NotImplementedException();
		}
		public override void RestorePage()
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_highlightIndex);
			base.RestorePage();
		}

		public override void EnterPressed()
		{
			entry.Enter();
		}

		public override void NumberPressed(Button number)
		{
			entry.Input((int)number);
			recalled = false;
		}

		public override void PrepareEntry()
		{
			recalled = false;
			entry.Reset();
		}

		public override void RecallPressed()
		{
			entry.Recall(recalled);
			recalled = !recalled;
		}

		public override void Reset()
		{
			foreach (var entry in Entries.Values)
            {
				entry.Reset();
            }
		}

		public override void RestoreEntry()
		{
			recalled = false;
			entry.Update();
		}
		/// <summary>
		/// When the UP or DOWN button is pressed. this function will be called to change the highlight entry
		/// </summary>
		/// <param name="change">the amount of change</param>
		public override void ChangeHighlightEntry(int change)
		{
			// restore the current entry before changing
			RestoreEntry();
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_highlightIndex);
			base.ChangeHighlightEntry(change);
			// prepare data for the highlight entry
			PrepareEntry();
		}
		public override void _BlinkStart(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_blink);
			base._BlinkStart(sender, e);
		}
		public override void _BlinkStop(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_blink);
			base._BlinkStop(sender, e);
		}

	}
}
