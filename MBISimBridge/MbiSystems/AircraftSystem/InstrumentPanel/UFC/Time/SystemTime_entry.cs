﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public sealed class SystemTime_entry : TimeEntryBase
	{
		private string time => tmp_hour + ":" + tmp_minute + ":" + tmp_second;
		public SystemTime_entry(): base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_SystemTime)) 
		{
			SubscribeTimeFromXplane();
		}

		public void _TimeSecondChanged(object sender, IntEventArgs e)
		{
			tmp_second = e.value.ToString("D2");
			setTime();
		}
		public void _TimeMinuteChanged(object sender, IntEventArgs e)
		{
			tmp_minute = e.value.ToString("D2");
			setTime();
		}
		public void _TimeHourChanged(object sender, IntEventArgs e)
		{
			tmp_hour = e.value.ToString("D2");
			setTime();
		}
        protected override void setTime()
        {
			tmp_time = time;
			base.setTime();
			Update();
		}

        public override void Enter()
		{
			tmp_time = input_time;
			setTime();
		}
		public override void Input(int number)
		{
			base.Input(number);
			UnSubscribeTimeFromXplane();
		}
		public override void Recall(bool recalled)
		{
			base.Recall(recalled);
			if (recalled)
				SubscribeTimeFromXplane();

		}
		public override void Reset()
		{
			base.Reset();

		}


		public override void Change(int change)
		{
			throw new NotImplementedException();
		}

		private void SubscribeTimeFromXplane()
        {
			EvtInterface.XPlaneTimeSecondChanged += _TimeSecondChanged;
			EvtInterface.XPlaneTimeMinuteChanged += _TimeMinuteChanged;
			EvtInterface.XPlaneTimeHourChanged += _TimeHourChanged;
		}
		private void UnSubscribeTimeFromXplane()
		{
			EvtInterface.XPlaneTimeSecondChanged -= _TimeSecondChanged;
			EvtInterface.XPlaneTimeMinuteChanged -= _TimeMinuteChanged;
			EvtInterface.XPlaneTimeHourChanged -= _TimeHourChanged;
		}
	}
}
