﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using System.Timers;


namespace AircraftSystem.InstrumentPanel
{
    class HackTime_entry : TimeEntryBase
    {
        private System.Timers.Timer HackTimer;
        int hour;
        int minute;
        int second;
        public HackTime_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_TIME_HackTime))
        {
            HackTimer = new System.Timers.Timer(1000);
            HackTimer.Elapsed += OnTimedEvent;
            HackTimer.AutoReset = true;
            HackTimer.Enabled = true;
            HackTimer.Stop();
        }
        protected override void setTime()
        {
            base.setTime();
            Update();
        }
        public override void Enter()
        {
            if (input_time.Length == 8)
            {
                hour = int.Parse(input_time.Substring(0, 2));
                minute = int.Parse(input_time.Substring(3, 2));
                second = int.Parse(input_time.Substring(6, 2));
            }
            else
            {
                EvtInterface.OnTimeBlinkStart(this, EventArgs.Empty);
                return;
            }

            if (hour < 24 && minute < 60 && second < 60)
            {
                tmp_time = input_time;
                setTime();
                TimerStart();
            }
            else
            {
                EvtInterface.OnTimeBlinkStart(this, EventArgs.Empty);
                return;
            }
            Reset();
        }

        public override void Input(int number)
        {
            base.Input(number);
            TimerStop();
        }
        public override void Recall(bool recalled)
        {
            base.Recall(recalled);
            if (recalled)
                TimerStart();

        }
        public override void Reset()
        {
            base.Reset();
            EvtInterface.OnTimeBlinkStop(this, EventArgs.Empty);
        }
        public override void Change(int change)
        {
            throw new NotImplementedException();
        }
        
        private void TimerStart()
        {
            HackTimer.Start();
        }
        /// <summary>
        /// Timer logic
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            second++;
            minute = (second < 60) ? minute : minute + 1;
            hour = (minute < 60) ? hour : hour + 1;
            second = second % 60;
            minute = minute % 60;
            hour = hour % 24;
            tmp_time = hour.ToString("D2") + ":" + minute.ToString("D2") + ":" + second.ToString("D2");
            setTime();
        }
        private void TimerStop()
        {
            HackTimer.Stop();
        }
    }
}
