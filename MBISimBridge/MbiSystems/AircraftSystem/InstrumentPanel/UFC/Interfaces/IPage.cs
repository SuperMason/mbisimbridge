﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	interface IPage
	{
		/// <summary>
		/// Update each entry
		/// </summary>
		void Update();
		/// <summary>
		/// When preparing for an entry, reset the temporary data beforehand
		/// </summary>
		void Reset();
		/// <summary>
		/// When one of buttons is pressed, this function will be called.
		/// </summary>
		/// <param name="button"></param>
		void ButtonPressed(Button button);
		/// <summary>
		/// Make the highlighted entry blink
		/// </summary>
		/// <param name="start">0: stop, 1: start</param>
		void _BlinkStart(object sender, EventArgs e);
		/// <summary>
		/// Make the highlighted entry stop blinking
		/// </summary>
		void _BlinkStop(object sender, EventArgs e);
		/// <summary>
		/// Before leaving the page, restore everything in it.
		/// </summary>
		void RestorePage();
		/// <summary>
		/// When the enter button is pressed, do the actions according to the current highlight entry
		/// </summary>
		void EnterPressed();
		/// <summary>
		/// When a number button is pressed, do the actions according to the current highight entry
		/// </summary>
		/// <param name="number"></param>
		void NumberPressed(Button number);
		/// <summary>
		/// When the recall button is pressed, do the actions according to the current highlight entry
		/// </summary>
		void RecallPressed();
		/// <summary>
		/// When the UP or DOWN button is pressed. this function will be called to change the highlight entry
		/// </summary>
		/// <param name="change">the amount of change</param>
		void ChangeHighlightEntry(int change);
		/// <summary>
		/// Prepare datas before changing the highlight entry
		/// </summary>
		void PrepareEntry();
		/// <summary>
		/// If changing highlight entry while editing without pressing enter button, restore the entry to the original state
		/// </summary>
		void RestoreEntry();
	}
}
