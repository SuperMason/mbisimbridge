﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AircraftSystem.InstrumentPanel
{
	interface IEntry
	{
		void Update();
		void Input(int number);
		void Enter();
		void Recall(bool recalled);
		void Reset();
		void Change(int change);
	}
}
