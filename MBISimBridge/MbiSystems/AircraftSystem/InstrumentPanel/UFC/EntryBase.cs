﻿using MBILibrary.Enums;
using MBILibrary.Util;
using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public abstract class EntryBase: MbiBaseSystem, IEntry
	{

        protected DataRefElement dataRef = null;
		private StringDataRefElement stringDataRef = null;
		//private bool blinking = false;
		public EntryBase(StringDataRefElement s)
		{
            triggerBySystem = TriggerByWhatSystem.FromEntryBase;
            stringDataRef = s;
		}
        /// <summary>
        /// Write dataRef to AircraftSystem
        /// </summary>
        /// <param name="obj"></param>
        protected void WriteDataRef(object obj)
        {
            try
            {
                if (obj.ValueIsString() && stringDataRef != null)
                {
                    WriteDataRefToAirManager4AircraftSystem(stringDataRef, obj);
                }
                else if (obj.ValueIsSingle() && dataRef != null)
                {
                    WriteDataRefToAirManager4AircraftSystem(dataRef, obj);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                throw;
            }
        }
        /// <summary>
        /// Write the correct data to Air Manager
        /// </summary>
		public abstract void Update();
        /// <summary>
        /// When entering numbers, this funcion will be called
        /// </summary>
        /// <param name="number"></param>
        public abstract void Input(int number);
        /// <summary>
        /// When enter button is pressed, this function will be called
        /// </summary>
        public abstract void Enter();
        /// <summary>
        /// When recall button is pressed, this function will be called
        /// </summary>
        public abstract void Recall(bool recalled);
        /// <summary>
        /// Reset the local properties
        /// </summary>
        public abstract void Reset();
        /// <summary>
        /// When the increment or decrement button is pressed, this function will be called
        /// </summary>
        /// <param name="change"></param>
        public abstract void Change(int change);
    }
}
