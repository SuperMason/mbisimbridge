﻿using MBILibrary.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using static AircraftSystem.InstrumentPanel.UFC;

namespace AircraftSystem.InstrumentPanel
{
	public class STPT: PageBase
	{
		/// <summary>
		/// There are 6 entries in STPT page
		/// </summary>
		private enum Entry
		{
			STPT,
			OPTION,
			LAT,
			LNG,
			ELEV,
			TOS
		}
		private static Dictionary<Entry, EntryBase> Entries = null;
		/// <summary>
		/// highlightEntry represents currently highlighted entry
		/// </summary>
		private Entry highlightEntry => (Entry)(highlightIndex - 1);
		private EntryBase entry => Entries[highlightEntry];
		private static STPT_entry stpt_entry => Entries[Entry.STPT] as STPT_entry;
		/// <summary>
		/// direction dictionary records the numbers and the corresponding directions
		/// </summary>
		public static readonly Dictionary<int, string> direction = new Dictionary<int, string>
		{ 
			{ 2, "N" },
			{ 4, "W" },
			{ 6, "E" },
			{ 8, "S" }
		};
		public static readonly int stptNum = 128;
		// 0-based stpt value
		private static int stpt { 
			get => stpt_entry.Value-1; 
			set { stpt_entry.Value = value; }
		}
		
		
		
		public static List<Steerpoint> steerpoints;
		public static Steerpoint steerpoint => steerpoints[stpt];
		public static string lat_dir => direction[steerpoint.Lat.Dir];
		public static int lat_deg => steerpoint.Lat.Deg;
		public static float lat_min => steerpoint.Lat.Min;
		public static string lng_dir => direction[steerpoint.Lng.Dir];
		public static int lng_deg => steerpoint.Lng.Deg;
		public static float lng_min => steerpoint.Lng.Min;
		public static int elev => steerpoint.Elev;

		static STPT()
		{
			steerpoints = new List<Steerpoint>
			{
				new Steerpoint(new Degree(2, 24, (float)0.76), new Degree(6, 121, (float)36.67), 8000),
				new Steerpoint(new Degree(2, 25, (float)4.05), new Degree(6, 121, (float)33.0), 10000),
				new Steerpoint(new Degree(2, 25, (float)5.1), new Degree(6, 121, (float)13.888), 12000),
				new Steerpoint(new Degree(2, 24, (float)49.248), new Degree(6, 120, (float)57.144), 13000),
				new Steerpoint(new Degree(2, 24, (float)15.72), new Degree(6, 120, (float)37.5), 11000),
				new Steerpoint(new Degree(2, 23, (float)27.276), new Degree(6, 120, (float)24.228), 10000),
				new Steerpoint(new Degree(2, 22, (float)56.886), new Degree(6, 120, (float)12.97), 9000),
				new Steerpoint(new Degree(2, 22, (float)34.482), new Degree(6, 120, (float)20.904), 10000),
				new Steerpoint(new Degree(2, 22, (float)40.347), new Degree(6, 121, (float)28.01), 12000)
			};
			for (int i = steerpoints.Count() - 1; i < stptNum; i++)
			{
				steerpoints.Add(new Steerpoint(new Degree(2, 0, 0), new Degree(6, 0, 0), 0));
			}
			Entries = new Dictionary<Entry, EntryBase>();
			Entries.Add(Entry.STPT, new STPT_entry());
			Entries.Add(Entry.OPTION, new OPTION_entry());
			Entries.Add(Entry.LAT, new LAT_entry());
			Entries.Add(Entry.LNG, new LNG_entry());
			Entries.Add(Entry.ELEV, new ELEV_entry());
			Entries.Add(Entry.TOS, new TOS_entry());
		}

		public STPT()
		{
			triggerBySystem = TriggerByWhatSystem.FromPageBase;
			EvtInterface.STPTBlinkStart += _BlinkStart;
			EvtInterface.STPTBlinkStop += _BlinkStop;
			EvtInterface.STPTSetLatitude += _SetLatitude;
			EvtInterface.STPTSetLongitude += _SetLongitude;
			EvtInterface.STPTChanged += _STPTChanged;
			EvtInterface.STPTUpdate += _STPTUpdate;

			

			entryNum = 6;
		}


		/// <summary>
		/// Update each entry
		/// </summary>
		public override void Update()
		{
			foreach (var _entry in Entries.Values)
			{
				_entry.Update();
			}
		}

		private void _STPTUpdate(object sender, EventArgs e)
		{
			Update();
		}

		/// <summary>
		/// Make the highlighted entry blink
		/// </summary>
		/// <param name="start">0: stop, 1: start</param>
		public override void _BlinkStart(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_Blink);
			base._BlinkStart(sender, e);
		}

		/// <summary>
		/// Make the highlighted entry stop blinking
		/// </summary>
		public override void _BlinkStop(object sender, EventArgs e)
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_Blink);
			base._BlinkStop(sender, e);
		}

		/// <summary>
		/// When preparing for an entry, reset the temporary data beforehand
		/// </summary>
		public override void Reset()
		{
			foreach (var entry in Entries.Values)
			{
				entry.Reset();
			}
		}

		/// <summary>
		/// Before leaving the STPT page, restore everything in it.
		/// </summary>
		public override void RestorePage()
		{
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_HighlightIndex);
			base.RestorePage();
			// To update stpt entry in CNI page
			stpt_entry.Update();
		}

		/// <summary>
		/// When the enter button is pressed, do the actions according to the current highlight entry
		/// </summary>
		public override void EnterPressed()
		{
			entry.Enter();
		}

		/// <summary>
		/// When the recall button is pressed, do the actions according to the current highlight entry
		/// </summary>
		public override void RecallPressed()
		{
			entry.Recall(recalled);
			recalled = !recalled;
		}

		/// <summary>
		/// When a number button is pressed, do the actions according to the current highight entry
		/// </summary>
		/// <param name="number"></param>
		public override void NumberPressed(Button number)
		{
			entry.Input((int)number);
			recalled = false;
		}

		/// <summary>
		/// When the UP or DOWN button is pressed. this function will be called to change the highlight entry
		/// </summary>
		/// <param name="change">the amount of change</param>
		public override void ChangeHighlightEntry(int change)
		{
			// restore the current entry before changing
			RestoreEntry();
			dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_HighlightIndex);
			base.ChangeHighlightEntry(change);
			// Prepare data for the highlight entry
			PrepareEntry();
		}

		/// <summary>
		/// Prepare datas before changing the highlight entry
		/// </summary>
		public override void PrepareEntry()
		{
			// reset recalled for every entry
			recalled = false;
			entry.Reset();
		}

		/// <summary>
		/// If changing highlight entry while editing without pressing enter button, restore the entry to the original state
		/// </summary>
		public override void RestoreEntry()
		{
			// if the current entry is blinking, make it stop
			_BlinkStop(this, EventArgs.Empty);
			entry.Update();
		}

		public override void ChangeValue(int change)
		{
			stpt_entry.Change(change);
		}

		private void _SetLatitude(object sender, DegreeEventArgs e)
		{
			steerpoint.Lat = e.degree;
		}

		private void _SetLongitude(object sender, DegreeEventArgs e)
		{
			steerpoint.Lng = e.degree;
		}
	
		private void _STPTChanged(object sender, IntEventArgs e)
		{
			stpt += e.value;
			stpt %= stptNum;
		}

		
	}
}
