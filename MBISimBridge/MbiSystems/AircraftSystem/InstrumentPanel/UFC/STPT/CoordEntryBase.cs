﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public abstract class CoordEntryBase : EntryBase
	{
		protected int dir;
		protected int deg;
		protected float min;
		protected string str;
		public CoordEntryBase(StringDataRefElement s): base(s) { }
		public abstract override void Input(int number);
		
		public abstract override void Update();

		public abstract override void Enter();

		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				str = str.Substring(0, str.Length - 1);
				WriteDataRef(str);
			}
			else
			{
				Update();
				Reset();
			}
		}

		public override void Reset()
		{
			dir = 0;
			deg = 0;
			min = 0;
			str = "";
		}

	}
}
