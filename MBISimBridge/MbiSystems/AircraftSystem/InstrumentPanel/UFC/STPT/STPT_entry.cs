﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class STPT_entry : EntryBase
	{
		private int tmp_value = 0;
		/// <summary>
		/// 0-based stpt value
		/// </summary>
		private int value { get; set; }
		/// <summary>
		/// 1-based stpt value
		/// </summary>
		public int Value
		{
			get => this.value + 1;
			set
			{
				if (value >= 0 && value < 128)
				{
					this.value = value;
					WriteDataRef(Value.ToString());
				}
			}
		}
		public STPT_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_STPT)) { }

		/// <summary>
		/// Update STPT entry to the original value
		/// </summary>
		public override void Update()
		{
			WriteDataRef((Value).ToString());
		}

		/// <summary>
		/// When STPT is highlighted and user input numbers, this function will be called
		/// </summary>
		/// <param name="number"></param>
		public override void Input(int number)
		{
			if (tmp_value < 100)
			{
				tmp_value *= 10;
				tmp_value += number;
				WriteDataRef(tmp_value.ToString());
			}
		}

		/// <summary>
		/// Assign stpt and write it to Air Manager
		/// </summary>
		public override void Enter()
		{
			if (tmp_value < STPT.stptNum)
			{
				EvtInterface.OnSTPTBlinkStop(this, EventArgs.Empty);
				value = tmp_value - 1;
				tmp_value = 0;
				WriteDataRef((Value).ToString());
				EvtInterface.OnSTPTUpdate(this, EventArgs.Empty);
			}
			else
				EvtInterface.OnSTPTBlinkStart(this, EventArgs.Empty);
		}

		/// <summary>
		/// When STPT is highlighted and an user presses recall button, this function will be called
		/// </summary>
		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				tmp_value /= 10;
				WriteDataRef(tmp_value == 0 ? (value + 1).ToString() : tmp_value.ToString());
			}
			else
			{
				tmp_value = 0;
				WriteDataRef((value + 1).ToString());
			}
		}

		/// <summary>
		/// reset tmp_value
		/// </summary>
		public override void Reset()
		{
			tmp_value = 0;
		}

		/// <summary>
		/// When STPT is highlighted and increment or decrement is pressed, this function will be called
		/// </summary>
		/// <param name="change"></param>
		public override void Change(int change)
		{
			value += change;
			value = (value+128) % 128;
			WriteDataRef((Value).ToString());
			EvtInterface.OnSTPTUpdate(this, EventArgs.Empty);
		}
	}
}
