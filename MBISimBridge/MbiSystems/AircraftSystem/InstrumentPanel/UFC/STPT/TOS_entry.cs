﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class TOS_entry : EntryBase
	{
		public TOS_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_STPT)) { }

		/// <summary>
		/// Update TOS entry to the original value
		/// </summary>
		public override void Update()
		{
		}

		/// <summary>
		/// When TOS is highlighted and user input numbers, this function will be called
		/// </summary>
		/// <param name="number"></param>
		public override void Input(int number)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Assign TOS and write it to Air Manager
		/// </summary>
		public override void Enter()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// When TOS is highlighted and an user presses recall button, this function will be called
		/// </summary>
		public override void Recall(bool recalled) 
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// 
		/// </summary>
		public override void Reset() { }

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
	}
}
