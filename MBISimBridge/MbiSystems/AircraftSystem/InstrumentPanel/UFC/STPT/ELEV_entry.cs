﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class ELEV_entry : EntryBase
	{
		private int value = 0;
		public ELEV_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_ELEV)) { }

		/// <summary>
		/// Update ELEV entry to the original value
		/// </summary>
		public override void Update()
		{
			WriteDataRef(string.Format("{0}FT", STPT.elev));
		}

		/// <summary>
		/// When ELEV is highlighted and user input numbers, this function will be called
		/// </summary>
		/// <param name="number"></param>
		public override void Input(int number)
		{
			value *= 10;
			value += number;
			WriteDataRef(value.ToString() + "FT");
		}

		/// <summary>
		/// Assign ELEV and write it to Air Manager
		/// </summary>
		public override void Enter()
		{
			STPT.steerpoint.Elev = value;
		}

		/// <summary>
		/// When ELEV is highlighted and an user presses recall button, this function will be called
		/// </summary>
		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				value /= 10;
				WriteDataRef(value.ToString() + "FT");
			}
			else
			{
				Update();
				value = 0;
			}
		}

		public override void Reset()
		{
			value = 0;
		}

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
	}
}
