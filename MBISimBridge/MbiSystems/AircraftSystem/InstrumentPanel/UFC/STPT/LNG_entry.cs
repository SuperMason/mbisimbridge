﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public class LNG_entry : CoordEntryBase
	{
		public LNG_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_LNG)) { }

		/// <summary>
		/// Update LNG entry to the original value
		/// </summary>
		public override void Update()
		{
			WriteDataRef(string.Format("{0,1}{1,3}*{2,6}'", STPT.lng_dir, STPT.lng_deg, STPT.lng_min.ToString("F3").PadLeft(6, '0')));
		}
		
		/// <summary>
				/// When LNG is highlighted and user input numbers, this function will be called
				/// </summary>
				/// <param name="number"></param>
		public override void Input(int number)
		{
			if (dir == 0)
			{
				if (number % 2 == 0 && number != 0)
				{
					str += STPT.direction[number];
					dir = number;
				}
			}
			else if (str.Length < 11)
			{
				str += number.ToString();
				int str_length = str.Length;
				if (str_length == 4)
				{
					deg = Int32.Parse(str.Substring(1, 3));
					str += "*";
				}
				else if (str_length > 4 && str_length < 11)
				{
					if (str_length == 7)
					{
						str += ".";
					}
					min = float.Parse(str.Substring(5));
				}
				else if (str_length == 11)
				{
					min = float.Parse(str.Substring(5));
					str += "'";
				}
			}
			WriteDataRef(str);
		}

		/// <summary>
		/// Assign value and write it to Air Manager
		/// </summary>
		public override void Enter()
		{
			if ((dir == 4 || dir == 6) && deg < 180 && min < 60)
			{
				EvtInterface.OnSTPTBlinkStop(this, EventArgs.Empty);
				EvtInterface.OnSTPTSetLongitude(this, new DegreeEventArgs(new Degree(dir, deg, min)));
			}
			else
				EvtInterface.OnSTPTBlinkStart(this, EventArgs.Empty);
		}

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
	}
}
