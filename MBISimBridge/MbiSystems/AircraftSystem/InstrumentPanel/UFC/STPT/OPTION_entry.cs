﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;

namespace AircraftSystem.InstrumentPanel
{
	public class OPTION_entry : EntryBase
	{
		private enum Option
		{
			MAN,
			AUTO
		}
		private static Option option = Option.MAN;
		public OPTION_entry() : base(GetStringDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_STPT)) { }

		/// <summary>
		/// Update STPT entry to the original value
		/// </summary>
		public override void Update()
		{
			WriteDataRef((int)option);
		}

		/// <summary>
		/// When STPT is highlighted and user input numbers, this function will be called
		/// </summary>
		/// <param name="number"></param>
		public override void Input(int number)
		{
			if (number == 0)
			{
				dataRef = GetDataRefElementFromInit(MbiDataRefs.InstrumentPanel_UFC_STPT_OPTION);
				option += 1;
				option = (Option)((int)option % 2);
				WriteDataRef((int)option);
			}
		}

		/// <summary>
		/// Assign stpt and write it to Air Manager
		/// </summary>
		public override void Enter()
		{
			
		}

		/// <summary>
		/// When STPT is highlighted and an user presses recall button, this function will be called
		/// </summary>
		public override void Recall(bool recalled) { }

		/// <summary>
		/// reset tmp_value
		/// </summary>
		public override void Reset() { }

		public override void Change(int change)
		{
			throw new NotImplementedException();
		}
	}
}
