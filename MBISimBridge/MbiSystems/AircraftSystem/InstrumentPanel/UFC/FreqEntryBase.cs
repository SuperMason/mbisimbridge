﻿using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.InstrumentPanel
{
	public abstract class FreqEntryBase : EntryBase
    {

        protected string tmp_freq = "";
		protected float freq = 305;
		public FreqEntryBase(StringDataRefElement s): base(s) { }

		public override void Update()
		{
			WriteDataRef(getFreqString());
		}
        public override void Input(int number)
		{
			if (tmp_freq.Length < 6)
			{
				tmp_freq += number;
				if (tmp_freq.Length == 3)
				{
					tmp_freq += ".";
				}
				WriteDataRef(tmp_freq);
			}
		}
		public override void Recall(bool recalled)
		{
			if (!recalled)
			{
				if(!tmp_freq.Equals(""))
					tmp_freq = tmp_freq.Substring(0, tmp_freq.Length - ((tmp_freq.Length == 4)?2:1));
				WriteDataRef(tmp_freq);
			} else
			{
				tmp_freq = "";
				WriteDataRef(getFreqString());
			}
		}
		public override void Reset()
		{
			tmp_freq = "";
		}

		protected virtual void setFreq(float frequency)
		{
			freq = frequency;
		}
		protected virtual string getFreqString()
		{
			return freq <= 20 ? freq.ToString() : freq.ToString("F2");
		}
	}
}
