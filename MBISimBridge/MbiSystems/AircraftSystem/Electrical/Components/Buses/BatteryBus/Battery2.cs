﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Electrical.Components.Buses.Battery
{
    public class Battery2 : BatteryBus
    {
        #region Fields
        //private Component Altimeter = null;
        //private Component LGWarningLight = null;
        //private Component ParkingBrake = null;
        //private Component ElecSysLight = null;
        #endregion

        #region Constructor
        public Battery2(string name)
        {
            //Altimeter = new Component("Altimeter", this);
            //LGWarningLight = new Component("LGWarningLight", this, 4);
            //ParkingBrake = new Component("ParkingBrake", this);
            //ElecSysLight = new Component("ElecSysLight", this, 4);
            Name = name;
            type = Type.DC;
        }

        #region EventHandlers
        #endregion


        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region EventHandlers

        #endregion

        #region Methods

        #endregion
    }
}
