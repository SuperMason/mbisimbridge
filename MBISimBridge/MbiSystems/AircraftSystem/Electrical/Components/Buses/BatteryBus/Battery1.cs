﻿using Electrical.Components.Instruments.Battery1Instrument;
using Electrical.Interfaces;
using Electrical.Interfaces.Buses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Electrical.Components.Buses.Battery
{
    public class Battery1 : BatteryBus
    {
        #region Fields
        private HYD_OIL_PressWarningLight HYD_OIL_PressWarningLight = null;
        private JFS JFS = null;
        private EngineWarningLight EngineWarningLight = null;
        private FLCS_RLY FLCS_RLY = null;
        private LGDownlockRelays LGDownlockRelays = null;
        private LGUplockDownlock LGUplockDownlock = null;


        #endregion

        #region Constructor
        public Battery1(string name)
        {
            EvtInterface.ConnectToBattery1 += _ConnectToBattery1;
            EvtInterface.DisconnectFromBattery1 += _DisconnectFromBattery1;

            HYD_OIL_PressWarningLight = new HYD_OIL_PressWarningLight("HYD_OIL_PressWarningLight");
            JFS = new JFS("JFS");
            EngineWarningLight = new EngineWarningLight("EngineWarningLight");
            FLCS_RLY = new FLCS_RLY("FLCS_RLY");
            LGDownlockRelays = new LGDownlockRelays("LGDownlockRelays");
            LGUplockDownlock = new LGUplockDownlock("LGUplockDownlock");

            Name = name;
            type = Type.DC;
        }

        #endregion

        #region EventHandlers
        private void _ConnectToBattery1(object sender, EventArgs e)
        {
            AddChild(sender);
        }
        private void _DisconnectFromBattery1(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }

        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnBattery1Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnBattery1Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
