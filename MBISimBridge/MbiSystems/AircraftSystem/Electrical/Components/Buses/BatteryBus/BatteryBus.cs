﻿using Electrical.Interfaces.Buses;
using Electrical.Interfaces.PowerUnits;
using System;

namespace Electrical.Components.Buses.Battery
{
    public abstract class BatteryBus : Bus, IBattery, IEmergencyDC1, IEmergencyDC2
    {
        #region Properties
        public bool ConnectedToBattery { get; set; } = false;
        public bool ConnectedToEmergencyDC1 { get; set; } = false;
        public bool ConnectedToEmergencyDC2 { get; set; } = false;

        #endregion

        #region Constructor
        public BatteryBus()
        {
            EvtInterface.MainGeneratorOn += _MainGeneratorOn;
            EvtInterface.EmergencyDC1Enabled += _EmergencyDC1Enabled;
            EvtInterface.EmergencyDC2Enabled += _EmergencyDC2Enabled;
            EvtInterface.BatteryAllConnect += _BatteryAllConnect;
            EvtInterface.BatteryAllDisconnect += _BatteryAllDisconnect;
            EvtInterface.BatteryFailure += _BatteryFailure;
            EvtInterface.EPUGeneratorFailure += _EPUGeneratorFailure;
            EvtInterface.BatteryOff += _BatteryOff;
            EvtInterface.BatteryOn += _BatteryOn;
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// When MainGenerator is on, connect to EmergencyDC1 and EmergencyDC1, and disconnect from Battery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorOn(object sender, EventArgs e)
        {
            EvtInterface.OnConnectToEmergencyDC1(this, EventArgs.Empty);
            ConnectedToEmergencyDC1 = true;
            EvtInterface.OnConnectToEmergencyDC2(this, EventArgs.Empty);
            ConnectedToEmergencyDC2 = true;
            EvtInterface.OnDisconnectFromBattery(this, EventArgs.Empty);
            ConnectedToBattery = false;
        }

        /// <summary>
        /// When EmergencyDC1 is enabled and be connected to it, enable the bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EmergencyDC1Enabled(object sender, EventArgs e)
        {
            if (ConnectedToEmergencyDC1 && !HasPower) { Enable(); }
        }

        /// <summary>
        /// When EmergencyDC1 is disabled and connects to the bus, disable the bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EmergencyDC1Disabled(object sender, EventArgs e)
        {
            if (ConnectedToEmergencyDC1 && HasPower) { Disable(); }
        }


        /// <summary>
        /// When EmergencyDC2 is enabled and connects to the bus, enable the bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EmergencyDC2Enabled(object sender, EventArgs e)
        {
            EvtInterface.OnConnectToEmergencyDC2(this, EventArgs.Empty);
            ConnectedToEmergencyDC2 = true;
            ConnectedToBattery = false;
            if (!HasPower) { Enable(); }
        }


        /// <summary>
        /// When EmergencyDC2 is disabled and connects to the bus, disable the bus
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EmergencyDC2Disabled(object sender, EventArgs e)
        {
            if (ConnectedToEmergencyDC2 && HasPower) { Disable(); }
        }

        /// <summary>
        /// When the ElecSwitch is switched to BATT, connect to Battery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _BatteryAllConnect(object sender, EventArgs e)
        {
            EvtInterface.OnConnectToBattery(this, EventArgs.Empty);
            ConnectedToBattery = true;
        }

        /// <summary>
        /// When the ElecSwitch is switched to MAINPOWER, disconnect from Battery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _BatteryAllDisconnect(object sender, EventArgs e)
        {
            EvtInterface.OnDisconnectFromBattery(this, EventArgs.Empty);
            ConnectedToBattery = false;
        }

        /// <summary>
        /// When Battery failure and connects to the bus, disable the bus if it has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _BatteryFailure(object sender, EventArgs e)
        {
            if (ConnectedToBattery && HasPower) { Disable(); }
        }

        /// <summary>
        /// When the Battery is off and connects to the bus, disable the bus if it has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _BatteryOff(object sender, EventArgs e)
        {
            if (ConnectedToBattery && HasPower) { Disable(); }
        }

        /// <summary>
        /// When the Battery is on and connects to the bus, enable the bus if it has no power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _BatteryOn(object sender, EventArgs e)
        {
            if (ConnectedToBattery && !HasPower) { Enable(); }
        }

        /// <summary>
        /// When EPUGenerator failure, disconnect from EmergencyDC1 and EmergencyDC1, and connect to Battery
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EPUGeneratorFailure(object sender, EventArgs e)
        {
            EvtInterface.OnDisconnectFromEmergencyDC1(this, EventArgs.Empty);
            ConnectedToEmergencyDC1 = false;
            EvtInterface.OnDisconnectFromEmergencyDC2(this, EventArgs.Empty);
            ConnectedToEmergencyDC2 = false;
            EvtInterface.OnConnectToBattery(this, EventArgs.Empty);
            ConnectedToBattery = true;
        }
        #endregion


        #region Methods
        #endregion
    }
}
