﻿using Electrical.Interfaces.PowerUnits;
using System;

namespace Electrical.Components.Buses.EPUGeneratorBus
{
    public abstract class EPUGeneratorBus : Bus, IEPUGenerator
    {
        #region Fields
        public bool ConnectedToEPUGenerator { get; set; }
        #endregion

        #region Constrtuctor
        public EPUGeneratorBus()
        {
            EvtInterface.STBYGeneratorFailure += _STBYGeneratorFailure;
            EvtInterface.EPUGeneratorFailure += _EPUGeneratorFailure;
            EvtInterface.EPUGeneratorOff += _EPUGeneratorOff;
            EvtInterface.EPUGeneratorOn += _EPUGeneratorOn;
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// When EPUGenerator Failure and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EPUGeneratorFailure(object sender, EventArgs e)
        {
            if (ConnectedToEPUGenerator && HasPower) { Disable(); }
        }

        /// <summary>
        /// When EPUGenerator is off and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EPUGeneratorOff(object sender, EventArgs e)
        {
            if (ConnectedToEPUGenerator && HasPower) { Disable(); }
        }

        /// <summary>
        /// When EPUGenerator is on and connects to the bus, enable the bus if has no power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EPUGeneratorOn(object sender, EventArgs e)
        {
            if (ConnectedToEPUGenerator && !HasPower) { Enable(); }
        }

        /// <summary>
        /// When STBYGenerator Failure, disconnect from STBYGenerator and connects to EPUGenerator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _STBYGeneratorFailure(object sender, EventArgs e)
        {
            Disconnect();
            EvtInterface.OnConnectToEPUGenerator(this, EventArgs.Empty);
            ConnectedToEPUGenerator = true;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Disconnect from the original power supply when STBYGenerator failure
        /// </summary>
        public abstract void Disconnect();
        #endregion
    }
}
