﻿using System;
using Electrical.Components.Instruments.EmergencyAC2Instrument;
using Electrical.Interfaces;
using Electrical.Interfaces.Buses;

namespace Electrical.Components.Buses.EPUGeneratorBus
{
    public class EmergencyAC2 : EPUGeneratorBus, IEssentialAC

    {
        #region Fields
        private HSI HSI = null;
        private Gun GUN = null;
        private HUD HUD = null;
        private HYD HYD = null;
        private LandingLight landing_lights = null;
        private NavigationLight navigation_lights = null;
        #endregion

        #region Constructors
        public EmergencyAC2(string name)
        {
            EvtInterface.ConnectToEmergencyAC2 += _ConnectToEmergencyAC2;
            EvtInterface.EssentialACEnabled += _EssentialACEnabled;
            EvtInterface.EssentialACDisabled += _EssentialACDisabled;
            EvtInterface.EPUGeneratorOn += _EPUGeneratorOn;
            Name = name;
            type = Type.AC;
            HSI = new HSI("HSI");
            GUN = new Gun();
            HUD = new HUD();
            HYD = new HYD("HYD");
            landing_lights = new LandingLight();
            navigation_lights = new NavigationLight();

            // Connect to EssentialAC by default
            EvtInterface.OnConnectToEssentialAC(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Whichever instrument triggers this event will be added into Children list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ConnectToEmergencyAC2(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        /// <summary>
        /// Whenever EssentialAC is enabled, the instruments connected to it will be enabled as well
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EssentialACEnabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }

        /// <summary>
        /// Whenever EssentialAC is disabled, the instruments connected to it will be disabled as well
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _EssentialACDisabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnEmergencyAC2Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnEmergencyAC2Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// This method is called when STBYGeneratorFailure event occrus
        /// EmergencyAC2 will disconnect from EssentialAC
        /// </summary>
        public override void Disconnect()
        {
            EvtInterface.OnDisconnectFromEssentialAC(this, EventArgs.Empty);
        }
        #endregion


    }
}
