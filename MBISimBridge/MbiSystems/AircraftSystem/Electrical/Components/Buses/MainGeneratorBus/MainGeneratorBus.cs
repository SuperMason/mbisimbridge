﻿using Electrical.Interfaces.PowerUnits;
using System;

namespace Electrical.Components.Buses.MainGenerator
{
    public abstract class MainGeneratorBus : Bus, IMainGenerator
    {
        #region Fields
        public bool ConnectedToMainGenerator { get; set; } = false;
        #endregion
        #region Constructor
        public MainGeneratorBus()
        {
            EvtInterface.MainGeneratorAllConnect += _MainGeneratorAllConnect;
            EvtInterface.MainGeneratorAllDisconnect += _MainGeneratorAllDisconnect;
            EvtInterface.MainGeneratorFailure += _MainGeneratorFailure;
            EvtInterface.MainGeneratorOff += _MainGeneratorOff;
            EvtInterface.MainGeneratorOn += _MainGeneratorOn;
        }
        #endregion
        #region EventHandlers
        /// <summary>
        ///When OnMainGeneratorAllConnect event is triggered, connect NonEssentialAC2, OvercurrentSensingContactor1, OvercurrentSensingContactor2 to MainGenerator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorAllConnect(object sender, EventArgs e)
        {
            EvtInterface.OnConnectToMainGenerator(this, EventArgs.Empty);
            ConnectedToMainGenerator = true;
        }

        /// <summary>
        ///When OnMainGeneratorAllDisconnect event is triggered, disconnect NonEssentialAC2, OvercurrentSensingContactor1, OvercurrentSensingContactor2 from MainGenerator.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorAllDisconnect(object sender, EventArgs e)
        {
            EvtInterface.OnDisconnectFromMainGenerator(this, EventArgs.Empty);
            ConnectedToMainGenerator = false;
        }

        /// <summary>
        /// When MainGenerator Failure and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorFailure(object sender, EventArgs e)
        {
            if (ConnectedToMainGenerator && HasPower) { Disable(); }
        }

        /// <summary>
        /// When MainGenerator is off and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorOff(object sender, EventArgs e)
        {
            if (ConnectedToMainGenerator && HasPower) { Disable(); }
        }


        /// <summary>
        /// When MainGenerator is on and connects to the bus, enable the bus if has no power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorOn(object sender, EventArgs e)
        {
            if (ConnectedToMainGenerator && !HasPower) { Enable(); }
        }
        #endregion

    }
}
