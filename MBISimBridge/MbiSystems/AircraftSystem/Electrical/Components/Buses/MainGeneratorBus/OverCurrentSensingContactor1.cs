﻿using Electrical.Interfaces;
using System;

namespace Electrical.Components.Buses.MainGenerator
{
    class OverCurrentSensingContactor1 : MainGeneratorBus
    {
        #region Constructor
        public OverCurrentSensingContactor1(string name)
        {
            Name = name;
            type = Type.AC;
        }
        #endregion

        #region EventHandlers

        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
