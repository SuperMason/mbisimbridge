﻿using System;
using Electrical.Components.Instruments.Others;
using Electrical.Interfaces;
namespace Electrical.Components.Buses.MainGenerator
{
    class OverCurrentSensingContactor2 : MainGeneratorBus
    {
        #region Field
        private FCR FCR = null;
        #endregion

        #region Constructor
        public OverCurrentSensingContactor2(string name)
        {
            EvtInterface.ConnectToOverCurrentSensingContactor2 += _ConnectToOverCurrentSensingContactor2;

            FCR = new FCR("FCR");
            Name = name;
            type = Type.AC;
        }
        #endregion

        #region EventHandlers

        private void _ConnectToOverCurrentSensingContactor2(object sender, EventArgs e)
        {
            AddChild(sender);
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnOverCurrentSensingContactor2Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnOverCurrentSensingContactor2Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
