﻿using Electrical.Components.Instruments;
using Electrical.Components.Instruments.NonEssentialAC2;
using Electrical.Interfaces;
using System;
namespace Electrical.Components.Buses.MainGenerator
{
    public class NonEssentialAC2 : MainGeneratorBus
    {
        #region Fields
        private FuelPumps2n4 FuelPumps2n4 = null;
        private TaxiLight taxi_light = null;
        #endregion
        #region Constructor
        public NonEssentialAC2(string name)
        {
            EvtInterface.ConnectToNonEssentialAC2 += _ConnectToNonEssentialAC2;
            EvtInterface.DisconnectFromNonEssentialAC2 += _DisconnectFromNonEssentialAC2;

            Name = name;
            type = Type.AC;
            FuelPumps2n4 = new FuelPumps2n4("FuelPumps2n4");
            taxi_light = new TaxiLight("taxi_light");
        }
        #region EventHandlers

        private void _ConnectToNonEssentialAC2(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        private void _DisconnectFromNonEssentialAC2(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }
        #endregion

        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnNonEssentialAC2Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnNonEssentialAC2Disabled(this, EventArgs.Empty);
        }
        #endregion

        public override void Failure()
        {
            throw new NotImplementedException();
        }
    }
}


