﻿using AircraftSystem;
using AircraftSystem.Electrical;
using Electrical.Components.Instruments;
using Electrical.Components.PowerUnits;
using Electrical.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Buses
{
    public abstract class Bus : ElectricalComponent, IConductive<Bus.Type>
    {
        #region Properties
        /// <summary>
        /// All components loaded onto the bus
        /// </summary>
        public ArrayList Children;
        /// <summary>
        /// HasPower indicates that whether the component has power supply
        /// </summary>
        public bool HasPower { get; set; } = false;
        public enum Type
        {
            AC,
            DC,
            CONVERTER
        }
        public Type type { get; set; }
        #endregion

        #region Constructor
        public Bus()
        {
            Children = new ArrayList();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Reset the bus
        /// </summary>
        protected override void Reset()
        {
            state = State.STBY;
            Disable();
        }
        /// <summary>
        /// A component will be disabled whenever the power supply is cut off
        /// If the component is working at the moment it is disabled, it will shutdown itself and disable its children no matter its switch is on or off
        /// </summary>
        public virtual void Disable() { 
            HasPower = false;
            state = State.STBY;
        }
        /// <summary>
        /// A component is enabled whenever it has a power supply.
        /// It will also enable all of its children
        /// </summary>
        public virtual void Enable() { 
            HasPower = true;
            state = State.NORMAL;
        }

        /// <summary>
        /// Add a child into Children list
        /// </summary>
        protected void AddChild(object child) {
            if (!Children.Contains(child))
            {
                Children.Add(child);
                Instrument instrumentChild = child as Instrument;
                if (instrumentChild != null)
                {
                    if (HasPower)
                        instrumentChild.Enable();
                    else
                        instrumentChild.Disable();
                }
                else
                {
                    Bus busChild = (Bus)child;
                    if (HasPower)
                        busChild.Enable();
                    else
                        busChild.Disable();
                }
            }
        }

        /// <summary>
        /// Remove a child from the Children list
        /// </summary>
        protected void RemoveChild(object child)
        {
            if (Children.Contains(child))
            {
                Children.Remove(child);
                Instrument instumentChild = child as Instrument;
                if (instumentChild != null)
                {
                    if (instumentChild.HasPower)
                        instumentChild.Disable();
                }
                else
                {
                    Bus busChild = child as Bus;
                    if (busChild.HasPower)
                        busChild.Disable();
                }
            }
        }

        /// <summary>
        /// Traverse through the children to visualize the Component Tree
        /// </summary>
        /// <param name="level"></param>
        public override void Print(int level)
        {
            switch (type)
            {
                case Type.AC:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
                case Type.DC:
                    Console.ForegroundColor = ConsoleColor.Magenta;
                    break;
                case Type.CONVERTER:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;
                default:
                    break;
            }
            
            Console.WriteLine(Name + " " + ( HasPower ? "O" : "X") + " " + state);
            Console.ResetColor();
            foreach (ElectricalComponent child in Children)
            {
                for (int i = 0; i < level; i++)
                    Console.Write('\t');
                child.Print(level + 1);
            }
        }
        #endregion
    }
}
