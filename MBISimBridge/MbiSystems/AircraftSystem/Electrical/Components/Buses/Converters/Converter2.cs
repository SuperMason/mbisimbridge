﻿using System;
using System.Collections;

namespace Electrical.Components.Buses
{
    public class Converter2 : Bus
    {
        #region Constructor
        public Converter2(string name) 
        {
            EvtInterface.ConnectToConverter2 += _ConnectToConverter2;
            EvtInterface.EmergencyAC2Enabled += _EmergencyAC2Enabled;
            EvtInterface.EmergencyAC2Disabled += _EmergencyAC2Disabled;
            Name = name;
            type = Type.CONVERTER;
            Children = new ArrayList();
            
            // Connect to EmergencyAC2 by default
            EvtInterface.OnConnectToEmergencyAC2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _ConnectToConverter2(object sender, EventArgs e)
        {
            AddChild(sender);
        }
        private void _EmergencyAC2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { 
                Enable();
                EvtInterface.OnConverter2Enabled(this, EventArgs.Empty);
            }
        }

        private void _EmergencyAC2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { 
                Disable();
                EvtInterface.OnConverter2Disabled(this, EventArgs.Empty);
            }
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnConverter2Enabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
        
    }
}
