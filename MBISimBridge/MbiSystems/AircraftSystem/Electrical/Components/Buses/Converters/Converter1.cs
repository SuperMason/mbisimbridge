﻿using System;
using System.Collections;

namespace Electrical.Components.Buses
{
    public class Converter1 : Bus
    {
        #region Constructor
        public Converter1(string name)
        {
            EvtInterface.EmergencyAC1Enabled += _EmergencyAC1Enabled;
            EvtInterface.EmergencyAC1Disabled += _EmergencyAC1Disabled;
            EvtInterface.ConnectToConverter1 += _ConnectToConverter1;
            Name = name;
            type = Type.CONVERTER;
            Children = new ArrayList();
            
            // Connect to EmergencyAC1 by default
            EvtInterface.OnConnectToEmergencyAC1(this, EventArgs.Empty);
        }
        #endregion

        private void _EmergencyAC1Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { 
                Enable();
                EvtInterface.OnConverter1Enabled(this, EventArgs.Empty);
            }
        }
        private void _EmergencyAC1Disabled(object sender, EventArgs e)
        {
            if (HasPower) { 
                Disable();
                EvtInterface.OnConverter1Disabled(this, EventArgs.Empty);
            }
        }

        private void _ConnectToConverter1(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnConverter1Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnConverter1Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
