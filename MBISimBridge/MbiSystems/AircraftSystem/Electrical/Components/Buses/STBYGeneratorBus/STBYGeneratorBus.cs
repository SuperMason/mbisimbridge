﻿using Electrical.Interfaces.PowerUnits;
using System;

namespace Electrical.Components.Buses
{
    public abstract class STBYGeneratorBus : Bus, ISTBYGenerator
    {
        #region Fields
        public bool ConnectedToSTBYGenerator { get; set; } = false;
        #endregion
        #region Constructor
        public STBYGeneratorBus()
        {
            EvtInterface.MainGeneratorFailure += _MainGeneratorFailure;
            EvtInterface.STBYGeneratorFailure += _STBYGeneratorFailure;
            EvtInterface.STBYGeneratorOff += _STBYGeneratorOff;
            EvtInterface.STBYGeneratorOn += _STBYGeneratorOn;
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// When MainGenerator Failure, disconnect the bus from MainGenerator, and connect to STBYGenerator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _MainGeneratorFailure(object sender, EventArgs e)
        {
            Disconnect();
            EvtInterface.OnConnectToSTBYGenerator(this, EventArgs.Empty);
            ConnectedToSTBYGenerator = true;
        }

        /// <summary>
        /// When STBYGenerator Failure and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public virtual void _STBYGeneratorFailure(object sender, EventArgs e)
        {
            if (ConnectedToSTBYGenerator && HasPower) { Disable(); }
        }

        /// <summary>
        /// When STBYGenerator is off and connects to the bus, disable the bus if has power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _STBYGeneratorOff(object sender, EventArgs e)
        {
            if (ConnectedToSTBYGenerator && HasPower) { Disable(); }
        }

        /// <summary>
        /// When STBYGenerator is on and connects to the bus, enable the bus if has no power
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void _STBYGeneratorOn(object sender, EventArgs e)
        {
            if (ConnectedToSTBYGenerator && !HasPower) { Enable(); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Disconnect from the original power supply when MainGenerator failure
        /// </summary>
        public abstract void Disconnect();
        #endregion
    }
}
