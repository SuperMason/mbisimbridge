﻿using Electrical.Interfaces;
using Electrical.Interfaces.Buses;
using System;

namespace Electrical.Components.Buses
{
    public class EssentialAC : STBYGeneratorBus, INonEssentialAC2
    {
        #region Fields
        private MFD MFD = null;
        #endregion

        #region Constructor
        public EssentialAC(string name)
        {
            EvtInterface.ConnectToEssentialAC += _ConnectToEssentialAC;
            EvtInterface.DisconnectFromEssentialAC += _DisconnectFromEssentialAC;
            EvtInterface.NonEssentialAC2Enabled += _NonEssentialAC2Enabled;
            EvtInterface.NonEssentialAC2Disabled += _NonEssentialAC2Disabled;
            Name = name;
            type = Type.AC;
            MFD = new MFD("MFD");

            // Connect to NonEssentialAC2 by default
            EvtInterface.OnConnectToNonEssentialAC2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _ConnectToEssentialAC(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        private void _DisconnectFromEssentialAC(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }
        public void _NonEssentialAC2Enabled(object sender, EventArgs e)
        {
            if (!ConnectedToSTBYGenerator && !HasPower) { Enable(); }
        }

        public void _NonEssentialAC2Disabled(object sender, EventArgs e)
        {
            if (!ConnectedToSTBYGenerator && HasPower) { Disable(); }
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnEssentialACEnabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnEssentialACDisabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        public override void Disconnect()
        {
            EvtInterface.OnDisconnectFromNonEssentialAC2(this, EventArgs.Empty);
        }
        #endregion
    }
}
