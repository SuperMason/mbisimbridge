﻿using Electrical.Components.Instruments.EmergencyAC1Instrument;
using Electrical.Interfaces.Buses;
using Electrical.Interfaces.PowerUnits;
using System;

namespace Electrical.Components.Buses
{
    public class EmergencyAC1 : STBYGeneratorBus, INonEssentialAC1, IEPUGenerator
    {
        #region Field
        public bool ConnectedToEPUGenerator { get; set; } = false;
        public Altimeter Altimeter = null;
        public AOAIndicator AOAIndicator = null;
        public EGI EGI = null;
        public FuelFlowIndicator FuelFlowIndicator = null;

        #endregion

        #region Constructor
        public EmergencyAC1(string name)
        {
            EvtInterface.ConnectToEmergencyAC1 += _ConnectToEmergencyAC1;
            EvtInterface.NonEssentialAC1Enabled += _NonEssentialAC1Enabled;
            EvtInterface.NonEssentialAC1Disabled += _NonEssentialAC1Disabled;
            EvtInterface.EPUGeneratorOn += _EPUGeneratorOn;
            Name = name;
            Altimeter = new Altimeter("Altimeter");
            AOAIndicator = new AOAIndicator("AOAIndicator");
            EGI = new EGI("EGI");
            FuelFlowIndicator = new FuelFlowIndicator("FuelFlowIndicator");

            // Connect to NonEssentialAC1 by default
            EvtInterface.OnConnectToNonEssentialAC1(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _ConnectToEmergencyAC1(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        public void _NonEssentialAC1Enabled(object sender, EventArgs e)
        {
            if(!ConnectedToSTBYGenerator && !ConnectedToEPUGenerator) Enable();
        }

        public void _NonEssentialAC1Disabled(object sender, EventArgs e)
        {
            if (!ConnectedToSTBYGenerator && !ConnectedToEPUGenerator) Disable();
        }

        public override void _STBYGeneratorFailure(object sender, EventArgs e)
        {
            base._STBYGeneratorFailure(sender, e);
            Disconnect();
            EvtInterface.OnConnectToEPUGenerator(this, EventArgs.Empty);
            ConnectedToEPUGenerator = true;
        }

        public void _EPUGeneratorFailure(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void _EPUGeneratorOff(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void _EPUGeneratorOn(object sender, EventArgs e)
        {
            if (ConnectedToEPUGenerator) { Enable(); }
        }

        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnEmergencyAC1Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnEmergencyAC1Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        public override void Disconnect()
        {
            if (!ConnectedToSTBYGenerator)
                EvtInterface.OnDisconnectFromNonEssentialAC1(this, EventArgs.Empty);
            else
                EvtInterface.OnDisconnectFromSTBYGenerator(this, EventArgs.Empty);
        }

        public void _EPUGeneratorAllDisconnect(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
