﻿using Electrical.Components.Instruments.EmergencyDC2Instrument;
using System;

namespace Electrical.Components.Buses
{
    public class EmergencyDC2 : Bus
    {
        #region Fields
        //private Component AutoPilot = null;
        //private Component FlareDispenser = null;
        //private Component FLCSResetCnD = null;
        //private Component FLCSPowerCnD = null;
        //private Component FLCSWarningLightCnD = null;
        private MasterArmSwitch masterArmSwitch = null;
        //private Component Gun = null;
        //private Component HUD = null;
        private LandingGear LG = null;
        private UFC UFC = null;
        #endregion

        #region Constructor
        public EmergencyDC2(string name)
        {
            EvtInterface.Converter2Enabled += _Converter2Enabled;
            EvtInterface.Converter2Disabled += _Converter2Disabled;
            EvtInterface.ConnectToEmergencyDC2 += _ConnectToEmergencyDC2;
            EvtInterface.DisconnectFromEmergencyDC2 += _DisconnectFromEmergencyDC2;
            Name = name;
            type = Type.DC;
            LG = new LandingGear("landing_gear");
            UFC = new UFC("UFC");
            //AutoPilot = new Component("AutoPilot", this);
            //FlareDispenser = new Component("FlareDispenser", this);
            //FLCSResetCnD = new Component("FLCSResetCnD", this);
            //FLCSPowerCnD = new Component("FLCSPowerCnD", this);
            //FLCSWarningLightCnD = new Component("FLCSWarningLightCnD", this, 4);
            masterArmSwitch = new MasterArmSwitch();
            //Gun = new Component("Gun", this);
            //HUD = new Component("HUD", this);

            // Connect to Converter2 by default
            EvtInterface.OnConnectToConverter2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _Converter2Enabled(object sender, EventArgs e)
        {
            Enable();
        }

        private void _Converter2Disabled(object sender, EventArgs e)
        {
            Disable();
        }

        private void _ConnectToEmergencyDC2(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        private void _DisconnectFromEmergencyDC2(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnEmergencyDC2Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnEmergencyDC2Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
        
    }
}
