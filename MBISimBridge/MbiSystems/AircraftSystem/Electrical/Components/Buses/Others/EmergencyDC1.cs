﻿

using Electrical.Components.Instruments.EmergencyDC1Instrument;
using System;

namespace Electrical.Components.Buses
{
    public class EmergencyDC1 : Bus
    {
        #region Field
        private AOAIndexer AOAIndexer = null;
        private FLCSReset FLCSReset = null;
        private FLCSPower FLCSPower = null;
        private FLCSWarningLight FLCSWarningLight = null;
        private LGLight LGLight = null;
        private MasterArmSwitch MasterArmSwitch = null;
        private Speedbrakes Speedbrakes = null;
        private StickTrim StickTrim = null;
        #endregion

        #region Constructor
        public EmergencyDC1(string name)
        {
            EvtInterface.Converter1Enabled += _Converter1Enabled;
            EvtInterface.Converter1Disabled += _Converter1Disabled;
            EvtInterface.ConnectToEmergencyDC1 += _ConnectToEmergencyDC1;
            EvtInterface.DisconnectFromEmergencyDC1 += _DisconnectFromEmergencyDC1;
            AOAIndexer = new AOAIndexer("AOAIndexer");
            FLCSReset = new FLCSReset("FLCSReset");
            FLCSPower = new FLCSPower("FLCSPower");
            FLCSWarningLight = new FLCSWarningLight("FLCSWarningLight");
            LGLight = new LGLight("LGLight");
            MasterArmSwitch = new MasterArmSwitch("MasterArmSwitch");
            Speedbrakes = new Speedbrakes("Speedbrakes");
            StickTrim = new StickTrim("StickTrim");
            Name = name;
            type = Type.DC;

            // Connect to Converter1 by default
            EvtInterface.OnConnectToConverter1(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _Converter1Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }

        private void _Converter1Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        private void _ConnectToEmergencyDC1(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        private void _DisconnectFromEmergencyDC1(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            base.Enable();
            EvtInterface.OnEmergencyDC1Enabled(this, EventArgs.Empty);
        }

        public override void Disable()
        {
            base.Disable();
            EvtInterface.OnEmergencyDC1Disabled(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
