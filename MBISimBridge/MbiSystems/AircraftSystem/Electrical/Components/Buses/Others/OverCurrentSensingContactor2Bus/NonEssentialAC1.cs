﻿using Electrical.Components.Buses;
using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Text;

namespace Electrical.AC
{
    public class NonEssentialAC1 : OverCurrentSensingContactor2Bus
    {
        #region Fields
        #endregion

        #region Constructor
        public NonEssentialAC1(string name)
        {
            EvtInterface.ConnectToNonEssentialAC1 += _ConnectToNonEssentialAC1;
            EvtInterface.DisconnectFromNonEssentialAC1 += _DisconnectFromNonEssentialAC1;

            Name = name;
            type = Type.AC;

            // Connect to OverCurrentSensingContactor2 by default
            EvtInterface.OnConnectToOverCurrentSensingContactor2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        private void _ConnectToNonEssentialAC1(object sender, EventArgs e)
        {
            AddChild(sender);
        }

        private void _DisconnectFromNonEssentialAC1(object sender, EventArgs e)
        {
            RemoveChild(sender);
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            if (!HasPower)
            {
                base.Enable();
                EvtInterface.OnNonEssentialAC1Enabled(this, EventArgs.Empty);
            }
        }

        public override void Disable()
        {
            if(HasPower)
            {
                base.Disable();
                EvtInterface.OnNonEssentialAC1Disabled(this, EventArgs.Empty);
            }
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
