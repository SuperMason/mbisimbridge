﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Buses
{
    public abstract class OverCurrentSensingContactor2Bus : Bus, IOverCurrentSensingContactor2
    {
        #region Constructor
        public OverCurrentSensingContactor2Bus()
        {
            EvtInterface.OverCurrentSensingContactor2Enabled += _OverCurrentSensingContactor2Enabled;
            EvtInterface.OverCurrentSensingContactor2Disabled += _OverCurrentSensingContactor2Disabled;
        }
        #endregion
        public void _OverCurrentSensingContactor2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }

        public void _OverCurrentSensingContactor2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }
    }
}
