﻿

namespace Electrical.Components.Buses
{
    public class EssentialDC: Bus
    {
        #region Constructor
        public EssentialDC(string name)
        {
            Name = name;
            type = Type.DC;
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
