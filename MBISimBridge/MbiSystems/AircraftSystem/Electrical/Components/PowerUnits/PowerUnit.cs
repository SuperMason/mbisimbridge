﻿using AircraftSystem;
using AircraftSystem.Electrical;
using Electrical.Components.Buses;
using Electrical.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.PowerUnits
{
    public abstract class PowerUnit : ElectricalComponent, IControllable
    {
        #region Properties
        /// <summary>
        /// All buses loaded onto the power unit
        /// </summary>
        public ArrayList Children;
        /// <summary>
        /// On indicates that whether the power unit switch is on
        /// </summary>
        public bool On { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Reset the powerUnit
        /// </summary>
        protected override void Reset()
        {
            state = State.STBY;
            TurnOff();
        }

        /// <summary>
        /// Simulate toggling switch action
        /// </summary>
        /// <param name="on"></param>
        public void ToggleSwitch(bool on)
        {
            if (on) { TurnOn(); }
            else TurnOff();
        }

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled off.
        /// If the component is working originally, it will shutdown and cut the power supply to its children immediately
        /// </summary>
        public virtual void TurnOff()
        {
            On = false;
            state = State.STBY;
            //WriteDataRef(0);
        }

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled on.
        /// </summary>
        public virtual void TurnOn()
        {
            On = true;
            state = State.NORMAL;
            //WriteDataRef(1);
        }

        /// <summary>
        /// Add a child into the Children list
        /// </summary>
        /// <param name="child"></param>
        protected void AddChild(Bus child)
        {
            if (!Children.Contains(child))
            {
                Children.Add(child);
                if (On)
                    child.Enable();
                else
                    child.Disable();
            }
        }

        /// <summary>
        /// Remove a child from the Children list
        /// </summary>
        protected void RemoveChild(Bus child)
        {
            if (Children.Contains(child))
            {
                Children.Remove(child);
                if (child.HasPower)
                    child.Disable();
            }
        }

        /// <summary>
        /// Take the power unit as the root, traverse through the children to visualize the Component Tree
        /// </summary>
        /// <param name="level"></param>
        public override void Print(int level)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(Name + " " + (On ? "O" : "X") + " " + state);
            Console.ResetColor();
            foreach (ElectricalComponent child in Children)
            {
                for (int i = 0; i < level; i++)
                    Console.Write('\t');
                child.Print(level + 1);
            }
        }

        #endregion
    }
}
