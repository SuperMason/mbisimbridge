﻿using Electrical.AC;
using Electrical.Components.Buses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Electrical.Components.PowerUnits
{
    public class STBYGenerator : PowerUnit
    {
        #region Properties

        #endregion

        #region Constructors
        public STBYGenerator() { }
        public STBYGenerator(string name)
        {
            EvtInterface.ConnectToSTBYGenerator += _ConnectToSTBYGenerator;
            EvtInterface.DisconnectFromSTBYGenerator += _DisconnectFromSTBYGenerator;
            EvtInterface.MainGeneratorFailure += _MainGeneratorFailure;
            EvtInterface.STBYGeneratorFailure += _STBYGeneratorFailure;
            EvtInterface.MainGeneratorOn += _MainGeneratorOn;
            Name = name;
            Children = new ArrayList();
        }

        #endregion

        #region EventHandlers
        /// <summary>
        /// Whoever fires this event will become STBYGenerator's children
        /// Only EssentialAC, EmergencyAC1, should trigger this event when they want to connect to STBYGenerator 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ConnectToSTBYGenerator(object sender, EventArgs e)
        {
            AddChild((Bus)sender);
        }

        /// <summary>
        /// Whoever fires this event will be removed from STBYGenerator's Children list
        /// Only EssentialAC, EmergencyAC1, should trigger this event when they want to disconnect from STBYGenerator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DisconnectFromSTBYGenerator(object sender, EventArgs e)
        {
            RemoveChild((Bus)sender);
        }

        /// <summary>
        /// When MainGenerator failure, switch to NORMAL state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MainGeneratorFailure(object sender, EventArgs e)
        {
            state = State.NORMAL;
        }

        /// <summary>
        /// When STBYGenerator failure, switch to FAILURE state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _STBYGeneratorFailure(object sender, EventArgs e)
        {
            state = State.FAILURE;
        }

        /// <summary>
        /// When MainGenerator is on, switch to STBY state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MainGeneratorOn(object sender, EventArgs e)
        {
            state = State.STBY;
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
