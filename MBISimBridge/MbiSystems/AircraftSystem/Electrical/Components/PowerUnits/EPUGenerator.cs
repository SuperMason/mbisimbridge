﻿using Electrical.AC;
using Electrical.Components.Buses;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Electrical.Components.PowerUnits
{
    public class EPUGenerator :PowerUnit
    {
        #region Field
        #endregion
        #region Constructors
        public EPUGenerator(string name)
        {
            EvtInterface.ConnectToEPUGenerator += _ConnectToEPUGenerator;
            EvtInterface.STBYGeneratorFailure += _STBYGeneratorFailure;
            EvtInterface.EPUGeneratorFailure += _EPUGeneratorFailure;
            Name = name;
            Children = new ArrayList();
        }
        #endregion
        #region EventHandlers
        /// <summary>
        /// Whoever fires this event will become EPUGenerator's children
        /// Only EmergencyAC1, EmergencyAC2, should trigger this event when they want to connect to EPUGenerator 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ConnectToEPUGenerator(object sender, EventArgs e)
        {
            AddChild((Bus)sender);
        }

        /// <summary>
        /// When STBYGenerator failure, turn on EPUGenerator and switch to FAILURE state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _STBYGeneratorFailure(object sender, EventArgs e)
        {
            TurnOn();
        }

        /// <summary>
        /// When EPUGenerator failure, switch to FAILURE state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EPUGeneratorFailure(object sender, EventArgs e)
        {
            state = State.FAILURE;
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
