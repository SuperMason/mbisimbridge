﻿using Electrical.Components.Buses;
using System;
using System.Collections;

namespace Electrical.Components.PowerUnits
{
    public class Battery : PowerUnit
    {
        #region Fields
        /// <summary>
        /// Charged indicates that wheather Battery is being charged by MainGenerator or not
        /// </summary>
        public bool Charged { get; set; }

        /// <summary>
        /// Discharging indicates that wheather Battery is providing power source to BatteryBus or not
        /// </summary>
        public bool Discharging { get; set; }
        #endregion

        #region Constructors
        public Battery(string name)
        {
            EvtInterface.ConnectToBattery += _ConnectToBattery;
            EvtInterface.DisconnectFromBattery += _DisconnectFromBattery;
            EvtInterface.BatteryFailure += _BatteryFailure;
            EvtInterface.MainGeneratorOn += _MainGeneratorOn;
            EvtInterface.EPUGeneratorFailure += _EPUGeneratorFailure;
            Name = name;
            Children = new ArrayList();
            Charged = false;
            Discharging = true;
        }
        #endregion
        #region EventHandlers
        /// <summary>
        /// Whoever fires this event will become Battery's children
        /// Only Battery1, Battery2 should trigger this event when they want to connect to Battery 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ConnectToBattery(object sender, EventArgs e)
        {
            AddChild((Bus)sender);
        }

        /// <summary>
        /// Whoever fires this event will be removed from Battery's Children list
        /// Only Battery1, Battery2 should trigger this event when they want to disconnect from Battery 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DisconnectFromBattery(object sender, EventArgs e)
        {
            RemoveChild((Bus)sender);
        }

        /// <summary>
        /// When Battery failure, switch state to FAILURE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _BatteryFailure(object sender, EventArgs e)
        {
            state = State.FAILURE;
        }

        /// <summary>
        /// When MainGenerator is on, Battery starts to be charged by MainGenerator and stops discharging electricity
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MainGeneratorOn(object sender, EventArgs e)
        {
            Charged = true;
            Discharging = false;
            state = State.STBY;
        }

        /// <summary>
        /// When EPUGenerator failure, Battery starts to discharge electricity, and stop being charged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _EPUGeneratorFailure(object sender, EventArgs e)
        {
            Charged = false;
            Discharging = true;
            state = State.NORMAL;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Turn on the Battery and fire OnBatteryOn event at the same time
        /// </summary>
        public override void TurnOn()
        {
            base.TurnOn();
            EvtInterface.OnBatteryOn(this, EventArgs.Empty);
        }

        /// <summary>
        /// Turn off the Battery and fire OnBatteryOff event at the same time
        /// </summary>
        public override void TurnOff()
        {
            base.TurnOff();
            EvtInterface.OnBatteryOff(this, EventArgs.Empty);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
