﻿using AircraftSystems.Electrical;
using Electrical.Components.Buses;
using System;
using System.Collections;

namespace Electrical.Components.PowerUnits
{
    public class MainGenerator : PowerUnit
    {
        #region Constructors
        public MainGenerator()
        {
            EvtInterface.ConnectToMainGenerator += _ConnectToMainGenerator;
            EvtInterface.DisconnectFromMainGenerator += _DisconnectFromMainGenerator;
            EvtInterface.MainGeneratorFailure += _MainGeneratorFailure;
            Name = "generator";
            Children = new ArrayList();
        }
        #endregion

        #region EventHandlers
        /// <summary>
        /// Whoever fires this event will become MainGenerator's children
        /// Only NonEssentialAC2, OverCurrentSensingContactor1, OverCurrentSensingContactor2 should trigger this event when they want to connect to MainGenerator 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ConnectToMainGenerator(object sender, EventArgs e) {
            AddChild((Bus)sender);
        }

        /// <summary>
        /// Whoever fires this event will be removed from MainGenerator's Children list
        /// Only NonEssentialAC2, OverCurrentSensingContactor1, OverCurrentSensingContactor2 should trigger this event when they want to disconnect from MainGenerator 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DisconnectFromMainGenerator(object sender, EventArgs e) {
            RemoveChild((Bus)sender);
        }

        /// <summary>
        /// When MainGenerator failure, switch to FAILURE state
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _MainGeneratorFailure(object sender, EventArgs e)
        {
            state = State.FAILURE;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Turn on the MainGenerator and fire OnMainGeneratorOn event at the same time
        /// </summary>
        public override void TurnOn()
        {
            base.TurnOn();
            EvtInterface.OnMainGeneratorOn(this, EventArgs.Empty);
        }

        /// <summary>
        /// Turn off the MainGenerator and fire OnMainGeneratorOff event at the same time
        /// </summary>
        public override void TurnOff()
        {
            base.TurnOff();
            EvtInterface.OnMainGeneratorOff(this, EventArgs.Empty);
        }

        /// <summary>
        /// This is called when the MainGenerator failure
        /// It will fire OnMainGeneratorFailure event
        /// </summary>
        public override void Failure() { EvtInterface.OnMainGeneratorFailure(this, EventArgs.Empty); }
        #endregion
    }
}

