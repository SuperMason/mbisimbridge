﻿using Electrical.Interfaces.Buses;
using System;

namespace Electrical.Components.Instruments.EssentialACInstrument
{
    public abstract class EssentialACInstrument : Instrument, IEssentialAC
    {
        #region Constructor
        public EssentialACInstrument()
        {
            EvtInterface.EssentialACEnabled += _EssentialACEnabled;
            EvtInterface.EssentialACDisabled += _EssentialACDisabled;

            // Connect to EssentialAC by default
            EvtInterface.OnConnectToEssentialAC(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        public void _EssentialACDisabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _EssentialACEnabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods

        #endregion
    }
}
