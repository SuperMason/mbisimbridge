﻿using AircraftSystems.Electrical;
using Electrical.Components.Instruments.EssentialACInstrument;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components
{
    public class MFD : EssentialACInstrument
    {
        #region Constructors
        public MFD(string name) 
        {
            EvtInterface.MFDSwitchToggled += _MFDSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            powerEvent = EvtInterface.OnMFDpower;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnMFDSwitchToggled);
        }
        #endregion

        #region EventHandlers
        private void _MFDSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
