﻿using AircraftSystem.Electrical;
using Electrical.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments
{
    public abstract class Instrument : ElectricalComponent, IControllable, IConductive<Instrument.Type>
    {
        #region Properties
        /// <summary>
        /// On indicates that whether the component switch is on
        /// </summary>
        public bool On { get; set; }
        /// <summary>
        /// HasPower indicates that whether the component has power supply
        /// </summary>
        public bool HasPower { get; set; }
        /// <summary>
        /// IsWorking is true when the switch is on, has a power supply, and it is not in a failure state
        /// </summary>
        public bool IsWorking { get => On && HasPower && state != State.FAILURE; }
        /// <summary>
        /// The type of the instrument
        /// 0: Lights
        /// 1: Weapons
        /// 2: Converter
        /// 3: Others
        /// </summary>
        public enum Type
        {
            LIGHT,
            WEAPON,
            SWITCH,
            OTHERS
        }
        public Type type { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Reset the instrument
        /// </summary>
        protected override void Reset()
        {
            state = State.STBY;
            Disable();
            TurnOff();
        }

        /// <summary>
        /// A component will be disabled whenever the power supply is cut off
        /// If the component is working at the moment it is disabled, it will shutdown itself and disable its children no matter its switch is on or off
        /// </summary>
        public virtual void Disable()
        {
            if (IsWorking) {
                state = State.STBY;
                if (powerEvent != null)
                {
                    powerEvent(this, new IntEventArgs(0));
                }
            }
            HasPower = false;
        }

        /// <summary>
        /// A component is enabled whenever it has a power supply.
        /// </summary>
        public virtual void Enable()
        {
            HasPower = true;
            if (type == Type.SWITCH)
                TurnOn();
            if (IsWorking) {
                state = State.NORMAL;
                if (powerEvent != null)
                {
                    powerEvent(this, new IntEventArgs(1));
                }
            }
        }

        /// <summary>
        /// Simulate toggling switch action
        /// </summary>
        /// <param name="on"></param>
        public void ToggleSwitch(bool on)
        {
            if (on) { TurnOn(); }
            else TurnOff();
        }

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled off.
        /// If the component is working originally, it will shutdown and cut the power supply to its children immediately
        /// </summary>
        public void TurnOff()
        {
            if (IsWorking) {
                state = State.STBY;
                if (powerEvent != null)
                {
                    powerEvent(this, new IntEventArgs(0));
                }
            }
            On = false;
        }

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled on.
        /// </summary>
        public void TurnOn()
        {
            On = true;
            if (IsWorking) {
                state = State.NORMAL;
                if (powerEvent != null)
                {
                    powerEvent(this, new IntEventArgs(1));
                }
            }
        }
        /// <summary>
        /// Print out the current instrument as a leaf node with a color corresponding to the type of the instrument
        /// </summary>
        /// <param name="level"></param>
        public override void Print(int level)
        {
            switch (type)
            {
                case Type.LIGHT:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case Type.WEAPON:
                    Console.ForegroundColor = ConsoleColor.Blue;
                    break;
                case Type.SWITCH:
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    break;
                default:
                    break;
            }
            if (IsWorking)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Yellow;
            }
            Console.WriteLine(Name + " " + (On ? "O" : "X") + " " + (HasPower ? "O" : "X") + " " + state);
            Console.ResetColor();
        }


        #endregion
    }
}
