﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AircraftSystems.Electrical;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace Electrical.Components.Instruments.Others
{
    public class FCR : Instrument
    {
        #region Constructor
        public FCR(string name)
        {
            EvtInterface.OverCurrentSensingContactor2Enabled += _OverCurrentSensingContactor2Enabled;
            EvtInterface.OverCurrentSensingContactor2Disabled += _OverCurrentSensingContactor2Disabled;

            // Connect to FCR by default
            EvtInterface.OnConnectToOverCurrentSensingContactor2(this, EventArgs.Empty);
           

            EvtInterface.FCRSwitchToggled += _FCRSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnFCRSwitchToggled);
            //WriteDataRef(0);
        }

        #endregion

        #region EventHandlers
        private void _OverCurrentSensingContactor2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        private void _OverCurrentSensingContactor2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }

        #endregion

        #region Methods
        private void _FCRSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
