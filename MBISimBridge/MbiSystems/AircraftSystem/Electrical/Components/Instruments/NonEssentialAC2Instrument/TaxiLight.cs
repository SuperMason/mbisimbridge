﻿using AircraftSystems.Electrical;
using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Text;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.NonEssentialAC2
{
    public class TaxiLight : NonEssentialAC2Instrument
    {
        #region Constructors
        public TaxiLight(string name)
        {
            EvtInterface.TaxiLightSwitchToggled += _TaxiLightSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            powerEvent = EvtInterface.OnTaxiLightPower;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnTaxiLightSwitchToggled);
        }
        #endregion
        #region EventHandlers
        private void _TaxiLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion
        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
