﻿
using Electrical.Interfaces.Buses;
using System;

namespace Electrical.Components.Instruments.NonEssentialAC2
{
    public class FuelPumps2n4 : NonEssentialAC2Instrument
    {
        #region Constructors
        public FuelPumps2n4(string name)
        {
            EvtInterface.NonEssentialAC2Enabled += _NonEssentialAC2Enabled;
            Name = name;
            type = Type.OTHERS;
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
