﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Instruments.NonEssentialAC2
{
    public abstract class NonEssentialAC2Instrument : Instrument, INonEssentialAC2
    {
        #region Constructor
        public NonEssentialAC2Instrument()
        {
            EvtInterface.NonEssentialAC2Disabled += _NonEssentialAC2Disabled;
            EvtInterface.NonEssentialAC2Enabled += _NonEssentialAC2Enabled;

            // Connect to NonEssentialAC2 by default
            EvtInterface.OnConnectToNonEssentialAC2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        public void _NonEssentialAC2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _NonEssentialAC2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods
        #endregion

        
    }
}
