﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Instruments.EmergencyAC2Instrument
{
    public abstract class EmergencyAC2Instrument : Instrument, IEmergencyAC2
    {
        #region Constructor
        public EmergencyAC2Instrument()
        {
            EvtInterface.EmergencyAC2Enabled += _EmergencyAC2Enabled;
            EvtInterface.EmergencyAC2Disabled += _EmergencyAC2Disabled;

            // Connect to EmergencyAC2 by default
            EvtInterface.OnConnectToEmergencyAC2(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        public void _EmergencyAC2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _EmergencyAC2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods

        #endregion    
    }
}
