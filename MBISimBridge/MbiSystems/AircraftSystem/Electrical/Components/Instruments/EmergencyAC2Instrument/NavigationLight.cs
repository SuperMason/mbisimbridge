﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyAC2Instrument
{
    public class NavigationLight : EmergencyAC2Instrument
    {
        #region Constructors
        public NavigationLight() 
        {
            EvtInterface.NavigationLightSwitchToggled += _NavigationLightSwitchToggled;
            Name = "navigation_lights";
            type = Type.LIGHT;
            powerEvent = EvtInterface.OnNavigationLightPower;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalNavLightsOn;
            ElectricalSystem.AddEventToCatelog(Name, EvtInterface.OnNavigationLightSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion
        #region EventHandlers
        private void _NavigationLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion
        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
