﻿using AircraftSystems.Electrical;
using Electrical.Interfaces.Buses;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyAC2Instrument
{
    public class HUD : EmergencyAC2Instrument, IEmergencyAC2
    {
        #region Constructors
        public HUD()
        {
            EvtInterface.HUDSwitchToggled += _HUDSwitchToggled;
            Name = "HUD";
            type = Type.OTHERS;
            powerEvent = EvtInterface.OnHUDPower;
            ElectricalSystem.AddEventToCatelog(Name, EvtInterface.OnHUDSwitchToggled);
        }
        #endregion

        #region EventHandlers
        private void _HUDSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
