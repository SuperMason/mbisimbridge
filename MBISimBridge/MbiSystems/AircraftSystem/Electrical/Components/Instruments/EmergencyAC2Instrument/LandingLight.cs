﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyAC2Instrument
{
    public class LandingLight : EmergencyAC2Instrument
    {
        #region Constructors
        public LandingLight() 
        {
            EvtInterface.LandingLightSwitchToggled += _LandingLightSwitchToggled;
            Name = "landing_lights";
            type = Type.LIGHT;
            powerEvent = EvtInterface.OnLandingLightPower;
            ElectricalSystem.AddEventToCatelog(Name, EvtInterface.OnLandingLightSwitchToggled);
        }
        #endregion

        #region EventHandlers
        private void _LandingLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
