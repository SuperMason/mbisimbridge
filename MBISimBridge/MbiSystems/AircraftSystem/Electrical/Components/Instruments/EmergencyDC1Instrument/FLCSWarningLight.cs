﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class FLCSWarningLight : EmergencyDC1Instrument
    {
        #region Constructors
        public FLCSWarningLight()
        {
            Name = "FLCSWarningLight";
        }
        public FLCSWarningLight(string name)
        {
            EvtInterface.FLCSWarningLightSwitchToggled += _FLCSWarningLightSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnFLCSWarningLightSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _FLCSWarningLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
        
    }
}
