﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{ 
    public class AOAIndexer : EmergencyDC1Instrument
    {
        #region Constructors
        public AOAIndexer()
        {
            Name = "AOAIndexer";
        }
        public AOAIndexer(string name)
        {
            EvtInterface.AOAIndexerSwitchToggled += _AOAIndexerSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnAOAIndexerSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _AOAIndexerSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion
        #region Methods

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
