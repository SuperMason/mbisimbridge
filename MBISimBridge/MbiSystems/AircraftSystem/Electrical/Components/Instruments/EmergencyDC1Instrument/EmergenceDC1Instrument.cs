﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public abstract class EmergencyDC1Instrument : Instrument, IEmergencyDC1
    {
        #region Constructor
        public EmergencyDC1Instrument()
        {
            EvtInterface.EmergencyDC1Enabled += _EmergencyDC1Enabled;
            EvtInterface.EmergencyDC1Disabled += _EmergencyDC1Disabled;

            // Connect to EmergencyDC1 by default
            EvtInterface.OnConnectToEmergencyDC1(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        public void _EmergencyDC1Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _EmergencyDC1Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods

        #endregion    
    }
}
