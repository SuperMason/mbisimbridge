﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class FLCSPower : EmergencyDC1Instrument
    {
        #region Constructors
        public FLCSPower()
        {
            Name = "FLCSPower";
        }
        public FLCSPower(string name)
        {
            EvtInterface.FLCSPowerSwitchToggled += _FLCSPowerSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnFLCSPowerSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _FLCSPowerSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
        
    }
}
