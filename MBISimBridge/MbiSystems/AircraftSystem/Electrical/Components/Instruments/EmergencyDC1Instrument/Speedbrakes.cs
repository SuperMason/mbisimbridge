﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class Speedbrakes : EmergencyDC1Instrument
    {
        #region Constructors
        public Speedbrakes()
        {
            Name = "Speedbrakes";
        }
        public Speedbrakes(string name)
        {
            EvtInterface.SpeedbrakesSwitchToggled += _SpeedbrakesSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnSpeedbrakesSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _SpeedbrakesSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods


        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
