﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class LGLight : EmergencyDC1Instrument
    {
        #region Constructors
        public LGLight()
        {
            Name = "LGLight";
        }
        public LGLight(string name)
        {
            EvtInterface.LGLightSwitchToggled += _LGLightSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnLGLightSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _LGLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
