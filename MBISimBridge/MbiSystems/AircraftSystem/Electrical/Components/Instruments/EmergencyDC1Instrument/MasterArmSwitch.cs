﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class MasterArmSwitch : EmergencyDC1Instrument
    {
        #region Fields
        private enum Position
        {
            OFF,
            SIM,
            ARM
        }
        private Position position;
        #endregion

        #region Constructors
        public MasterArmSwitch(string name)
        {
            Name = name;
            type = Type.SWITCH;
            position = Position.OFF;
            EvtInterface.OnMasterArmSwitchToggled(this, new IntEventArgs(0));

            // Connect to to EmergencyDC1 by default
            EvtInterface.OnConnectToEmergencyDC1(this, EventArgs.Empty);
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
