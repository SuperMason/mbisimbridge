﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;


namespace Electrical.Components.Instruments.EmergencyDC1Instrument
{
    public class StickTrim : EmergencyDC1Instrument
    {
        #region Constructors
        public StickTrim()
        {
            Name = "StickTrim";
        }
        public StickTrim(string name)
        {
            EvtInterface.StickTrimSwitchToggled += _StickTrimSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnStickTrimSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _StickTrimSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
