﻿
namespace Electrical.Components.Instruments.EmergencyAC1Instrument
{
    public class AOAIndicator : EmergencyAC1Instrument
    {
        #region Constructors
        public AOAIndicator(string name)
        {
            Name = name;
            type = Type.OTHERS;
            // dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            // WriteDataRef(0);
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
