﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Instruments.EmergencyAC1Instrument
{
    public abstract class EmergencyAC1Instrument : Instrument, IEmergencyAC1
    {
        #region Constructor
        public EmergencyAC1Instrument()
        {
            EvtInterface.EmergencyAC1Enabled += _EmergencyAC1Enabled;
            EvtInterface.EmergencyAC1Disabled += _EmergencyAC1Disabled;

            // Connect to  EmergencyAC1 by default
            EvtInterface.OnConnectToEmergencyAC1(this, EventArgs.Empty);
        }
        #endregion

        #region EventHandlers
        public void _EmergencyAC1Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _EmergencyAC1Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods

        #endregion
    }
}
