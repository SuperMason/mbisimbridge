﻿using Electrical.Interfaces.Buses;
using System;

namespace Electrical.Components.Instruments.EmergencyAC1Instrument
{
    public class Altimeter : EmergencyAC1Instrument
    {
        #region Constructors
        public Altimeter(string name) 
        {
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            // WriteDataRef(0);
        }
        #endregion


        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        
    }
}
