﻿using System;
using System.Collections.Generic;
using System.Text;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    public class LandingGear : EmergencyDC2Instrument
    {
        #region Constructors
        public LandingGear(string name)
        {
            EvtInterface.LandingGearDown += _LandingGearDown;
            EvtInterface.LandingGearUp += _LandingGearUp;
            Name = name;
            type = Type.SWITCH;
        }
        #endregion

        #region EventHandlers
        public void _LandingGearDown(object sender, EventArgs e)
        {
            if (HasPower)
            {
                ExecuteCommandToXPlane(Commands.FlightControlsLandingGearDown);
            }
        }
        public void _LandingGearUp(object sender, EventArgs e)
        {
            if (HasPower)
            {
                ExecuteCommandToXPlane(Commands.FlightControlsLandingGearUp);
            }
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
