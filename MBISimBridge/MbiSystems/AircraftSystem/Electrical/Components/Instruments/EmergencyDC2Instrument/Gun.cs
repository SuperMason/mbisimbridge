﻿using Electrical.Interfaces.Buses;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    public class Gun : EmergencyDC2Instrument
    {
        #region Constructors
        public Gun(string name)
        {
            EvtInterface.EmergencyAC2Enabled += _EmergencyDC2Enabled;
            Name = name;
            type = Type.WEAPON;
        }
        #endregion



        #region Methods
        public override void Enable()
        {
            HasPower = true;
            EvtInterface.OnGunPower(this, new IntEventArgs(1));
        }
        public override void Disable()
        {
            HasPower = false;
            EvtInterface.OnGunPower(this, new IntEventArgs(0));
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
