﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    public class MasterArmSwitch : EmergencyDC2Instrument
    {
        

        #region Constructors
        public MasterArmSwitch()
        {
            Name = "MasterArmSwitch";
            type = Type.SWITCH;
            EvtInterface.OnMasterArmSwitchPower(this, new IntEventArgs(0));
        }
        #endregion

        #region Methods
        public override void Enable()
        {
            EvtInterface.OnMasterArmSwitchPower(this, new IntEventArgs(1));
        }

        public override void Disable()
        {
            EvtInterface.OnMasterArmSwitchPower(this, new IntEventArgs(0));
        }

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
