﻿

using AircraftSystems.Electrical;
using Electrical.Interfaces.Buses;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    public class HUD : EmergencyDC2Instrument
    {
        #region Constructors
        public HUD(string name)
        {
            EvtInterface.HUDSwitchToggled += _HUDSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnHUDSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _HUDSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
