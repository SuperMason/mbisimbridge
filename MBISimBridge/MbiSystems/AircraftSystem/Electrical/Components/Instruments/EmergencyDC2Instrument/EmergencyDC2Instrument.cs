﻿using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    public abstract class EmergencyDC2Instrument : Instrument, IEmergencyDC2
    {
        #region Constructor
        public EmergencyDC2Instrument()
        {
            EvtInterface.EmergencyDC2Enabled += _EmergencyDC2Enabled;
            EvtInterface.EmergencyDC2Disabled += _EmergencyDC2Disabled;

            // Connect to EmergencyDC2 by default
            EvtInterface.OnConnectToEmergencyDC2(this, EventArgs.Empty);
        }

        #endregion

        #region EventHandlers
        public void _EmergencyDC2Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _EmergencyDC2Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #endregion

        #region Methods
        #endregion


        
    }
}
