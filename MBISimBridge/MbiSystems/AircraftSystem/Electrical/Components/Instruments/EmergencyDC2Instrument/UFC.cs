﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.EmergencyDC2Instrument
{
    // up front control
    public class UFC : EmergencyDC2Instrument
    {
        #region Fields
        #endregion

        #region Constructors
        public UFC(string name)
        {
            EvtInterface.UFCSwitchToggled += _UFCSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            powerEvent = EvtInterface.OnUFCpower;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnUFCSwitchToggled);
        }
        #endregion

        #region EventHandlers
        private void _UFCSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods

        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
