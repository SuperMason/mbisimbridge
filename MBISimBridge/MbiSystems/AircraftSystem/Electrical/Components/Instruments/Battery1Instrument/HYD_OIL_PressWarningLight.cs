﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class HYD_OIL_PressWarningLight : Battery1Instrument
    {
        #region Constructors
        public HYD_OIL_PressWarningLight()
        {
            Name = "HYD_OIL_PressWarningLight";
        }
        public HYD_OIL_PressWarningLight(string name)
        {
            EvtInterface.HYD_OIL_PressWarningLightSwitchToggled += _HYD_OIL_PressWarningLightSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnHYD_OIL_PressWarningLightSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _HYD_OIL_PressWarningLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
        
    }
}
