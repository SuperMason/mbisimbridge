﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class LGUplockDownlock : Battery1Instrument
    {
        #region Constructors
        public LGUplockDownlock()
        {
            Name = "LGUplockDownlock";
        }
        public LGUplockDownlock(string name)
        {
            EvtInterface.LGUplockDownlockSwitchToggled += _LGUplockDownlockSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnLGUplockDownlockSwitchToggled);
            //WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _LGUplockDownlockSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}