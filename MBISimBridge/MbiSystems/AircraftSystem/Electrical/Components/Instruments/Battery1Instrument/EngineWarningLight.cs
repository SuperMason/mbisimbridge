﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class EngineWarningLight : Battery1Instrument
{
        #region Constructors
        public EngineWarningLight()
        {
            Name = "EngineWarningLight";
        }
        public EngineWarningLight(string name)
        {
            EvtInterface.EngineWarningLightSwitchToggled += _EngineWarningLightSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnEngineWarningLightSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _EngineWarningLightSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
        
    }
}
