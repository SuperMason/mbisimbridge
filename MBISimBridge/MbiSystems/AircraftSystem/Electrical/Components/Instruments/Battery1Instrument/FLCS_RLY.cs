﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class FLCS_RLY : Battery1Instrument
    {
        // TODO: Still don't know to where it connects to, it connects to battery bus1 temporarily
        #region Constructor
        public FLCS_RLY()
        {
            Name = "FLCS_RLY";
        }
        public FLCS_RLY(string name)
        {
            EvtInterface.FLCS_RLYSwitchToggled += _FLCS_RLYSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnFLCS_RLYSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _FLCS_RLYSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new NotImplementedException();
        }
        #endregion
        
    }
}
