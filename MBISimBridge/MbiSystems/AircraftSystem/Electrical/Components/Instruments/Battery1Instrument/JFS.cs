﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class JFS : Battery1Instrument
    {
        #region Constructors
        public JFS()
        {
            Name = "JFS";
        }
        public JFS(string name)
        {
            EvtInterface.JFSSwitchToggled += _JFSSwitchToggled;
            Name = name;
            type = Type.OTHERS;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnJFSSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _JFSSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion
    }
}
