﻿using AircraftSystems.Electrical;
using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace Electrical.Components.Instruments.Battery1Instrument
{
    public class LGDownlockRelays : Battery1Instrument
    {
        #region Constructors
        public LGDownlockRelays()
        {
            Name = "LGDownlockRelays";
        }
        public LGDownlockRelays(string name)
        {
            EvtInterface.LGDownlockRelaysSwitchToggled += _LGDownlockRelaysSwitchToggled;
            Name = name;
            type = Type.LIGHT;
            //dataRef = XPlaneConnector.Instructions.DataRefs.CockpitElectricalHUDOn;
            ElectricalSystem.AddEventToCatelog(name, EvtInterface.OnLGDownlockRelaysSwitchToggled);
            // WriteDataRef(0);
        }
        #endregion

        #region EventHandlers
        private void _LGDownlockRelaysSwitchToggled(object sender, IntEventArgs e)
        {
            ToggleSwitch(e.value == 1);
        }
        #endregion

        #region Methods
        public override void Failure()
        {
            throw new System.NotImplementedException();
        }
        #endregion

    }
}
