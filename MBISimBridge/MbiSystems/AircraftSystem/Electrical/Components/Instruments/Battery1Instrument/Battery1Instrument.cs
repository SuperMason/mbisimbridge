﻿using Electrical.Components.Instruments;
using Electrical.Interfaces;
using Electrical.Interfaces.Buses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Electrical.Components.Instruments.Battery1Instrument
{
    public abstract class Battery1Instrument : Instrument, IBattery1
    {
        #region Constructor
        public Battery1Instrument()
        {
            EvtInterface.Battery1Enabled += _Battery1Enabled;
            EvtInterface.Battery1Disabled += _Battery1Disabled;

            // Connect to  Battery1 by default
            EvtInterface.OnConnectToBattery1(this, EventArgs.Empty);
        }
        #endregion
        public void _Battery1Disabled(object sender, EventArgs e)
        {
            if (HasPower) { Disable(); }
        }

        public void _Battery1Enabled(object sender, EventArgs e)
        {
            if (!HasPower) { Enable(); }
        }
        #region Methods

        #endregion
    }
}
