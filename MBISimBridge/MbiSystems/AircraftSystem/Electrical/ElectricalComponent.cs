﻿using AircraftSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystem.Electrical
{
    public abstract class ElectricalComponent : Component
    {
        /// <summary>
        /// When the power is on or off, trigger the powerEvent with the corresponding value, 1 for on, 0 for off.
        /// The instrument in the InstrumentPanel system will be listening on the event.
        /// </summary>
        protected delegate void PowerEvent(object sender, IntEventArgs e);
        /// <summary>
        /// Set the powerEvent when being constructed where the event should be defined in EvtInterface_Electrical
        /// </summary>
        protected PowerEvent powerEvent { get; set; } = null;
        public abstract void Print(int level);
    }

}
