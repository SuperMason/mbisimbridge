﻿using Electrical.AC;
using Electrical.Components.Buses;
using Electrical.Components.Buses.Battery;
using Electrical.Components.Buses.EPUGeneratorBus;
using Electrical.Components.Buses.MainGenerator;
using Electrical.Components.PowerUnits;
using MBILibrary.Enums;
using MBISimBridge.MbiSystems;
using System;
using System.Collections.Generic;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace AircraftSystems.Electrical
{
    public class ElectricalSystem : MbiBaseSystem
    {
        #region Fields
        #region Buses
        #region AC
        private OverCurrentSensingContactor1 overCurrentSensingContactor1 = null;
        private OverCurrentSensingContactor2 overCurrentSensingContactor2 = null;
        private NonEssentialAC1 nonEssentialAC1 = null;
        private NonEssentialAC2 nonEssentialAC2 = null;
        private EssentialAC essentialAC = null;
        private EmergencyAC1 emergencyAC1 = null;
        private EmergencyAC2 emergencyAC2 = null;
        #endregion
        #region Converter
        private Converter1 converter1 = null;
        private Converter2 converter2 = null;
        #endregion
        #region DC
        private EmergencyDC1 emergencyDC1 = null;
        private EmergencyDC2 emergencyDC2 = null;
        private Battery1 battery1 = null;
        private Battery2 battery2 = null;
        #endregion
        #endregion
        #region PowerUnits
        private MainGenerator mainGenerator = null;
        private STBYGenerator stbyGenerator = null;
        private EPUGenerator epuGenerator = null;
        private Battery battery = null;
        #endregion
        #endregion
        #region Properties
        /// <summary>
        /// EventToBeTriggered sould be the Event declared in EvtInterface_Electrical, and used to fire the event when the switch of an enabled instrument is toggled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void EventToBeTriggered(object sender, IntEventArgs e);
        /// <summary>
        /// EventCatelog stores the name of the instrument and the corresponding EventToBeTriggered
        /// </summary>
        static public Dictionary<string, EventToBeTriggered> EventCatelog = null;
        #endregion

        #region Constructor
        static ElectricalSystem()
        {
            EventCatelog = new Dictionary<string, EventToBeTriggered>();
        }
        public ElectricalSystem()
        {
            triggerBySystem = TriggerByWhatSystem.FromElectricalSystem;
            EvtInterface.ElectricalDataRefWrtie += _ElectricalDataRefWrtie;
            EvtInterface.ElectricalCommand += _ElectricalCommand;

            Init();
        }
        #endregion

        /// <summary>
        /// Initialize the electrical circuit architecture
        /// </summary>
        public void Init()
        {
            #region PowerUnits
            mainGenerator = new MainGenerator();
            stbyGenerator = new STBYGenerator("STBYGenerator");
            epuGenerator = new EPUGenerator("EPUGenerator");
            battery = new Battery("Battery");
            #endregion
            #region Buses
            #region AC
            overCurrentSensingContactor1 = new OverCurrentSensingContactor1("OverCurrentSensingContactor1");
            overCurrentSensingContactor2 = new OverCurrentSensingContactor2("OverCurrentSensingContactor2");
            nonEssentialAC1 = new NonEssentialAC1("NonEssentialAC1");
            nonEssentialAC2 = new NonEssentialAC2("NonEssentialAC2");
            essentialAC = new EssentialAC("EssentialAC");
            emergencyAC1 = new EmergencyAC1("EmergencyAC1");
            emergencyAC2 = new EmergencyAC2("EmergencyAC2");
            #endregion
            #region Converter
            converter1 = new Converter1("Converter1");
            converter2 = new Converter2("Converter2");
            #endregion
            #region DC
            emergencyDC1 = new EmergencyDC1("EmergencyDC1");
            emergencyDC2 = new EmergencyDC2("EmergencyDC2");
            battery1 = new Battery1("Battery1");
            battery2 = new Battery2("Battery2");
            #endregion
            #endregion


            //mainGenerator.TurnOn();
            //EvtInterface.OnMainGeneratorAllConnect(this, EventArgs.Empty);
            //SwitchToBATT();
            //SwitchToMAINPWR();
            //EvtInterface.OnMainGeneratorFailure(this, EventArgs.Empty);
            //EvtInterface.OnSTBYGeneratorFailure(this, EventArgs.Empty);
            //EvtInterface.OnEPUGeneratorFailure(this, EventArgs.Empty);
            //switchtooff();
            //print();
        }

        #region Methods
        /// <summary>
        /// This is called when an instrument wants to add its event to the EventCatelog, typically when it is constructed
        /// </summary>
        /// <param name="name"></param>
        /// <param name="eventToBeTriggered"></param>
        static public void AddEventToCatelog(string name, EventToBeTriggered eventToBeTriggered)
        {
            if (!EventCatelog.ContainsKey(name)) { EventCatelog.Add(name, eventToBeTriggered); }
        }
        #region Elec Switch
        public void SwitchToOFF()
        {
            SwitchToBATT();
            //// In flight
            //if (airborne)
            //{
            //    // Disconnect main generator from electrical system
            //    nonEssentialAC1.Disconnect();
            //    overCurrentSensingContactor1.Disconnect();
            //    overCurrentSensingContactor2.Disconnect();

            //    // Disables standby generator
            //    stbyGenerator.Disable();
            //}
            //// On ground
            //else
            //{
            // Disconnect main generator or external power from aircraft electrical system
            EvtInterface.OnMainGeneratorAllDisconnect(this, EventArgs.Empty);

            // Disables standby generator
            stbyGenerator.TurnOff();

            // Disconnect the aircraft battery from the battery buses
            battery.TurnOff();
            EvtInterface.OnBatteryAllDisconnect(this, EventArgs.Empty);
            //}
            //mainGenerator.Disable();
            //stbyGenerator.Disable();
            //battery.Disable();
            //epuGenerator.Disable();
        }
        public void SwitchToBATT()
        {
            // Connect aircraft battery to the battery buses
            EvtInterface.OnBatteryAllConnect(this, EventArgs.Empty);
            battery.TurnOn();

            // Reset maing generator
            mainGenerator.TurnOff();

            // Disconnect main generator or external power
            EvtInterface.OnMainGeneratorAllDisconnect(this, EventArgs.Empty);

            // Disable standby generator
            stbyGenerator.TurnOff();

            // Determine function of FLCS PWR TEST switch
        }
        public void SwitchToMAINPWR()
        {
            // Enable standby generator
            stbyGenerator.TurnOn();
            mainGenerator.TurnOn();

            // Battery buses' power is now provided by ac power. If ac power is not available, connect battery to the battery buses
            EvtInterface.OnBatteryAllDisconnect(this, EventArgs.Empty);
            EvtInterface.OnMainGeneratorAllConnect(this, EventArgs.Empty);
            ///// <summary>
            ///// Verify lights on:
            ///// ENGINE
            ///// HYD/OIL PRESS
            ///// ELEC SYS
            ///// SEC
            ///// FLCS RLY
            ///// </summary>
            //battery1.EngineWarningLight.Enable();
            //battery1.HYD_OIL_PressWarningLight.Enable();
            //battery2.ElecSysLight.Enable();
        }
        
        #endregion
        private void _ElectricalDataRefWrtie(object sender, DataRefEventArgs e)
        {
            object ObjVal = e.DataBus4Object.ObjVal;
            string theDataRefName = e.DataBus4Object.DataRef;
            string component = theDataRefName.Substring(theDataRefName.LastIndexOf('/') + 1);
            int value = e.DataBus4Object.ObjVal2Int;
            switch (component)
            {
                case "AAModeSwitch":
                    EvtInterface.OnAAModeSwitchToggled(this, new IntEventArgs(value));
                    break;
                case "ElecSwitch":
                    switch (value)
                    {
                        case 0:
                            SwitchToOFF();
                            break;
                        case 1:
                            SwitchToBATT();
                            break;
                        case 2:
                            SwitchToMAINPWR();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    if (EventCatelog.ContainsKey(component)) 
                        EventCatelog[component](this, new IntEventArgs(value));
                    break;
            }
            #if DEBUG
                print();
            #endif
        }

        public void _ElectricalCommand(object sender, CommandEvevntArgs e)
        {
            string command = e.Command.Name;
            string action = command.Substring(command.LastIndexOf('/') + 1);
            //Console.WriteLine(command);
            switch (action)
            {
                case "reset":
                    //Reset();
                    break;
                case "MainGenerator_Failure":
                    EvtInterface.OnMainGeneratorFailure(this, EventArgs.Empty);
                    break;
                case "StandbyGenerator_Failure":
                    EvtInterface.OnSTBYGeneratorFailure(this, EventArgs.Empty);
                    break;
                case "EPUyGenerator_Failure":
                    EvtInterface.OnEPUGeneratorFailure(this, EventArgs.Empty);
                    break;
                case "landing_gear_down":
                    EvtInterface.OnLandingGearDown(this, EventArgs.Empty);
                    break;
                case "landing_gear_up":
                    EvtInterface.OnLandingGearUp(this, EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }

        private void print()
        {
            mainGenerator.Print(1);
            stbyGenerator.Print(1);
            epuGenerator.Print(1);
            battery.Print(1);
        }
        #endregion

    }
}
