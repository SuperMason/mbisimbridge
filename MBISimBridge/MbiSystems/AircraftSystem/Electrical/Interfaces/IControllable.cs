﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Interfaces
{
    /// <summary>
    /// Every component with a corresponding switch is controllable
    /// </summary>
    interface IControllable
    {
        /// <summary>
        /// On indicates that whether the component switch is on
        /// </summary>
        bool On { get; set; }

        /// <summary>
        /// Simulate toggling switch action
        /// </summary>
        /// <param name="on"></param>
        void ToggleSwitch(bool on);

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled on.
        /// </summary>
        void TurnOn();

        /// <summary>
        /// Do the corresponding actions whenever the switch is toggled off.
        /// If the component is working originally, it will shutdown and cut the power supply to its children immediately
        /// </summary>
        void TurnOff();
    }
}
