﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Interfaces
{
    interface IConductive<T>
    {
        /// <summary>
        /// HasPower indicates that whether the component has power supply
        /// </summary>
        bool HasPower { get; set; }

        T type { get; set; }


        /// <summary>
        /// A component is enabled whenever it has a power supply.
        /// It will also enable all of its children
        /// </summary>
        void Enable();

        /// <summary>
        /// A component will be disabled whenever the power supply is cut
        /// If the component is working at the moment it is disabled, it will shutdown itself and disable its children no matter its switch is on or off
        /// </summary>
        void Disable();
    }
}
