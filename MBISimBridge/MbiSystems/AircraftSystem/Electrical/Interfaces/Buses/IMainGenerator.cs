﻿
using System;

namespace Electrical.Interfaces.PowerUnits
{
    interface IMainGenerator
    {
        bool ConnectedToMainGenerator { get; set; }
        void _MainGeneratorOn(object sender, EventArgs e);
        void _MainGeneratorOff(object sender, EventArgs e);
        void _MainGeneratorFailure(object sender, EventArgs e);
        void _MainGeneratorAllConnect(object sender, EventArgs e);
        void _MainGeneratorAllDisconnect(object sender, EventArgs e);
    }
}