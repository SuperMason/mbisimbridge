﻿using System;

namespace Electrical.Interfaces.PowerUnits
{
    interface ISTBYGenerator
    {
        bool ConnectedToSTBYGenerator { get; set; }
        void _STBYGeneratorOn(object sender, EventArgs e);
        void _STBYGeneratorOff(object sender, EventArgs e);
        void _MainGeneratorFailure(object sender, EventArgs e);
        void _STBYGeneratorFailure(object sender, EventArgs e);
    }
}
