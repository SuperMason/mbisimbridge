﻿using System;


namespace Electrical.Interfaces.PowerUnits
{
    interface IBattery
    {
        bool ConnectedToBattery { get; set; }
        bool ConnectedToEmergencyDC1 { get; set; }
        bool ConnectedToEmergencyDC2 { get; set; }
        void _BatteryOn(object sender, EventArgs e);
        void _BatteryOff(object sender, EventArgs e);
        void _BatteryFailure(object sender, EventArgs e);
        void _EPUGeneratorFailure(object sender, EventArgs e);
        void _BatteryAllConnect(object sender, EventArgs e);
    }
}
