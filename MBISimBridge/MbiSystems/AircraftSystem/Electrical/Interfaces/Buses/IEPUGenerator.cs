﻿using System;

namespace Electrical.Interfaces.PowerUnits
{
    interface IEPUGenerator
    {
        bool ConnectedToEPUGenerator { get; set; }
        void _EPUGeneratorOn(object sender, EventArgs e);
        void _EPUGeneratorOff(object sender, EventArgs e);
        void _STBYGeneratorFailure(object sender, EventArgs e);
        void _EPUGeneratorFailure(object sender, EventArgs e);
    }
}

