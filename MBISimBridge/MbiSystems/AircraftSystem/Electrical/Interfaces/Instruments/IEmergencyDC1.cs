﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface IEmergencyDC1
    {
        void _EmergencyDC1Enabled(object sender, EventArgs e);
        void _EmergencyDC1Disabled(object sender, EventArgs e);
    }
}
