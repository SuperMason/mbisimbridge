﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface IEmergencyAC1
    {
        void _EmergencyAC1Enabled(object sender, EventArgs e);
        void _EmergencyAC1Disabled(object sender, EventArgs e);
    }
}
