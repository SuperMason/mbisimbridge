﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface INonEssentialAC1
    {
        void _NonEssentialAC1Enabled(object sender, EventArgs e);
        void _NonEssentialAC1Disabled(object sender, EventArgs e);
    }
}
