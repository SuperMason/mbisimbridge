﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Electrical.Interfaces.Buses
{
    interface IOverCurrentSensingContactor2
    {
        void _OverCurrentSensingContactor2Enabled(object sender, EventArgs e);
        void _OverCurrentSensingContactor2Disabled(object sender, EventArgs e);
    }
}
