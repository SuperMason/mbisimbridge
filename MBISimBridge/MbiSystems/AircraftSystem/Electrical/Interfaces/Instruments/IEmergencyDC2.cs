﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface IEmergencyDC2
    {
        void _EmergencyDC2Enabled(object sender, EventArgs e);
        void _EmergencyDC2Disabled(object sender, EventArgs e);
    }
}
