﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface IEssentialAC
    {
        void _EssentialACEnabled(object sender, EventArgs e);
        void _EssentialACDisabled(object sender, EventArgs e);
    }
}
