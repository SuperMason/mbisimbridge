﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface IEmergencyAC2
    {
        void _EmergencyAC2Enabled(object sender, EventArgs e);
        void _EmergencyAC2Disabled(object sender, EventArgs e);
    }
}
