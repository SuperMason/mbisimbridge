﻿using System;

namespace Electrical.Interfaces.Buses
{
    interface IBattery1
    {
        void _Battery1Enabled(object sender, EventArgs e);
        void _Battery1Disabled(object sender, EventArgs e);
    }
}
