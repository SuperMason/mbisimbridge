﻿using System;
namespace Electrical.Interfaces.Buses
{
    interface INonEssentialAC2
    {
        void _NonEssentialAC2Enabled(object sender, EventArgs e);
        void _NonEssentialAC2Disabled(object sender, EventArgs e);
    }
}
