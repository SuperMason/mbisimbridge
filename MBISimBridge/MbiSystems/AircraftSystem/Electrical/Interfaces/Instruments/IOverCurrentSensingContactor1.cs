﻿using System;

namespace Electrical.Interfaces.Buses
{
    interface IOverCurrentSensingContactor1
    {
        void _OverCurrentSensingContactor1Enabled(object sender, EventArgs e);
        void _OverCurrentSensingContactor1Disabled(object sender, EventArgs e);
    }
}
