﻿using AircraftSystem.InstrumentPanel;
using AircraftSystems.Electrical;
using MBILibrary.Enums;
using MBILibrary.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.Instructions;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// Handle Electrical Logic
    /// </summary>
    public class AircraftSystem : MbiBaseSystem
    {
        #region fields
        private static ElectricalSystem electricalSystem = null;
        private static InstrumentPanel instrumentPanel = null;
        private delegate void EventToBeTriggered(object sender, MessageEventArgs e);
        /// <summary>
        /// pairs of datarefs and the corresponding events
        /// </summary>
        private Dictionary<string, EventToBeTriggered> dataRefEvents = null;
        #endregion

        #region constructor
        /// <summary>
        /// Handle Electrical Logic
        /// </summary>
        public AircraftSystem()
        {
            triggerBySystem = TriggerByWhatSystem.FromAircraftSys;
            electricalSystem = new ElectricalSystem();

            EvtInterface.StartElectricalCheck += _StartElectricalCheck;
            EvtInterface.ElectricalDataRefRead += _ElectricalDataRefRead;
            EvtInterface.InstrumentPanelDataRefRead += _InstrumentPanelDataRefRead;
            instrumentPanel = new InstrumentPanel(); // 上 2 行 code 要先指定委派後, 才能觸發接收處理!!! 否則沒人會承接處理
            #region 與 X-Plane 互動
            EvtInterface.XPlaneConnected += _OnXPlaneConnected;
            EvtInterface.UpdateAirManagerDataRefSingleVal += ReadLatestDataRefSingleVal;
            #endregion

            #region 與 Air Manager 互動
            EvtInterface.ExecuteAirManagerCommand += _ExecuteAirManagerCommand;
            EvtInterface.ExecuteAirManagerDataRefWrite += _ExecuteAirManagerDataRefWrite;
            #endregion

            dataRefEvents = new Dictionary<string, EventToBeTriggered>();
            //addDataRefEvent("sim/cockpit/weapons/plane_target_index", EvtInterface.OnPlaneTargetIndexChanged);
        }
        #endregion

        /// <summary>
        /// Add a pair of dataRef and its corresponding event into the dataRefEvents
        /// </summary>
        /// <param name="dataRef"></param>
        /// <param name="e"></param>
        private void addDataRefEvent(string dataRef, EventToBeTriggered e)
		{
			if (!dataRefEvents.ContainsKey(dataRef))
                dataRefEvents.Add(dataRef, e);
		}
        /// <summary>
        /// 啟動電力系統檢查
        /// </summary>
        private void _StartElectricalCheck(object sender, MessageEventArgs e)
        {
            Console.WriteLine($"{e.Data}");
            Thread.Sleep(1000);
            e.electricalProperty = new ElectricalProperty
            { A3 = 0.123f, A4 = 0.987654321, AbcDef = true, UpdatingTime = DateTime.Now, UpdatingEnable = false  };
            e.Data = "假裝檢查所有電力系統..相關流程參閱 SPEC... 經過 1 秒... 檢查完畢.. 下一步啟動空戰系統";
            EvtInterface.OnStartAirForceSystem(this, e);
        }

        /// <summary>
        /// Forward DataRefs from ElectricalSystem to XPlane and Air Manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _ElectricalDataRefRead(object sender, DataRefEventArgs e)
        {
            // AircraftDataRefRead; // event fire 後, 沒人承接處理
            EvtInterface.OnAircraftDataRefRead(this, e);
        }

        /// <summary>
        /// Forward DataRefs from InstrumentPanel to Air Manager and XPlane
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _InstrumentPanelDataRefRead(object sender, DataRefEventArgs e)
        {
            // AircraftDataRefRead; // event fire 後, 沒人承接處理
            EvtInterface.OnAircraftDataRefRead(this, e);
        }


        #region 與 X-Plane 互動
        /// <summary>
        /// X-Plane 已連線成功, 可以開始與 X-Plane 進行通訊
        /// </summary>
        private void _OnXPlaneConnected(object serder, MessageEventArgs e)
        {
            //UtilGlobalFunc.Log($"{this.GetType().Name}, X-Plane 已經準備好了 ::: {e.Data}");
        }
        /// <summary>
        /// Listen DataRef value updated :: '單一值' int, float, double, string
        /// <para>只 Read data, 並做後續連動行為</para>
        /// </summary>
        private void ReadLatestDataRefSingleVal(object sender, DataRefEventArgs e)
        {
            string theDataRefName = string.Empty;
            string msg = string.Empty;
            try
            {
                if (e.DataBusIsBytes)
                {
                    theDataRefName = e.DataBus4Bytes.DataRef;
					if (dataRefEvents.ContainsKey(theDataRefName))
					{
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        UtilGlobalFunc.Log(msg);
                        dataRefEvents[theDataRefName](this, new MessageEventArgs(val.ToString()));
					}

                    if(theDataRefName.Equals("sim/cockpit2/clock_timer/zulu_time_seconds"))
                    {
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        //msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        //UtilGlobalFunc.Log(msg);
                        EvtInterface.OnXPlaneTimeSecondChanged(this, new IntEventArgs((int)val));
                    }
                    if (theDataRefName.Equals("sim/cockpit2/clock_timer/zulu_time_minutes"))
                    {
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        //msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        //UtilGlobalFunc.Log(msg);
                        EvtInterface.OnXPlaneTimeMinuteChanged(this, new IntEventArgs((int)val));
                    }
                    if (theDataRefName.Equals("sim/cockpit2/clock_timer/zulu_time_hours"))
                    {
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        //msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        //UtilGlobalFunc.Log(msg);
                        EvtInterface.OnXPlaneTimeHourChanged(this, new IntEventArgs((int)val));
                    }
                    if (theDataRefName.Equals("sim/cockpit2/clock_timer/current_day"))
                    {
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        //msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        //UtilGlobalFunc.Log(msg);
                        EvtInterface.OnXPlaneDayChanged(this, new IntEventArgs((int)val));
                    }
                    if (theDataRefName.Equals("sim/cockpit2/clock_timer/current_month"))
                    {
                        float val = e.DataBus4Bytes.ObjVal2Float;
                        //msg = string.Format("{0}, ID({1}), DataRefName({2}), Value({3})", this.GetType().Name, e.DataBus4Bytes.Id, e.DataBus4Bytes.DataRef, val);
                        //UtilGlobalFunc.Log(msg);
                        EvtInterface.OnXPlaneMonthChanged(this, new IntEventArgs((int)val));
                    }

                }
                else if (e.DataBusIsString)
                {
                    theDataRefName = e.DataBus4String.DataRef;
                }
                else { }
            }
            catch (Exception ex) { UtilGlobalFunc.Log($"ReadLatestDataRefSingleVal(), DataRef Name({theDataRefName}) 未註冊於 Air Manager, Exception({ex.Message})"); }
            
        }
        #endregion


        #region 與 Air Manager 互動
        /// <summary>
        /// 從 Air Manager 取得 Command for executing
        /// </summary>
        private void _ExecuteAirManagerCommand(object serder, CommandEvevntArgs e)
        {
            Console.WriteLine(e.Command.Name);
            UtilGlobalFunc.Log(string.Format(UtilConstants.ExecuteCommandInfo, this.GetType().Name, e.Command.Name, e.Command.Value, e.Command.Description));
            if(e.Command.Name.StartsWith("mbi/electrical"))
            {
                EvtInterface.OnElectricalCommand(this, e);
            }
            else if (e.Command.Name.StartsWith("mbi/instrumentPanel"))
            {
                EvtInterface.OnInstrumentPanelCommand(this, e);
            }
        }
        /// <summary>
        /// 從 Air Manager 取得 DataRef Value for writing
        /// </summary>
        private void _ExecuteAirManagerDataRefWrite(object serder, DataRefEventArgs e)
        {
            if (e == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefEventArgs), e)); return; }
            if (e.DataBus4Object == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefElementBase), e.DataBus4Object)); return; }

            string DataRefName = e.DataBus4Object.DataRef;
            if (e.DataBus4Object.IsWritable)
            {
                object ObjCheck = null;
                if (e.DataBus4ObjectIsBytes)
                {
                    ObjCheck = e.DataBus4Object.ObjVal;
                    if (DataRefName.StartsWith("mbi/electrical"))
                    {
                        EvtInterface.OnElectricalDataRefWrite(this, new DataRefEventArgs(e.Data, e.DataBus4Object, triggerBySystem));
                    }
                    else if (DataRefName.StartsWith("mbi/instrumentPanel"))
                    {
                        EvtInterface.OnInstrumentPanelDataRefWrite(this, new DataRefEventArgs(e.Data, e.DataBus4Object, triggerBySystem));
                    }
                }
                else if (e.DataBus4ObjectIsString)
                {
                    ObjCheck = e.DataBus4Object.ObjVal2String;
                }

                if (ObjCheck != null)
                {
                    bool WriteIt = false;
                    if (e.DataBus4ObjectIsBytes)
                    { WriteIt = true; }
                    else if (e.DataBus4ObjectIsString)
                    { WriteIt = true; }

                    if (WriteIt)
                    {
#if DEBUG
                        //Log(string.Format(UtilConstants.WriteDataRefValueInfo, GetType().Name, e.TriggerByWhichSystem, UtilConstants.XPlaneSoftware, e.DataRefName, e.DataRefType, e.DataRefObjValShowString));
#endif
                    }
                }
            }
            else { Log(string.Format(UtilConstants.DataRefCanNotWrittenMessage, DataRefName, UtilConstants.Flag_Yes)); }
        }
        #endregion
    }
}