﻿namespace AircraftSystem.InstrumentPanel
{
    public sealed class MbiDataRefs
    {
        private MbiDataRefs() { }
        public const string Electrical_Cockpit_Electrical_UFC_on = "mbi/electrical/cockpit/electrical/UFC_on";
        public const string Electrical_Cockpit_Electrical_MFD_on = "mbi/electrical/cockpit/electrical/MFD_on";
        public const string InstrumentPanel_UFC_TIME_highlightIndex = "mbi/instrumentPanel/UFC/TIME/highlightIndex";
        public const string InstrumentPanel_UFC_TIME_blink = "mbi/instrumentPanel/UFC/TIME/blink";
        public const string InstrumentPanel_UFC_TIME_Date = "mbi/instrumentPanel/UFC/TIME/Date";
        public const string InstrumentPanel_AAModeSwitch = "mbi/instrumentPanel/AAModeSwitch";
        public const string InstrumentPanel_MasterArmSwitch = "mbi/instrumentPanel/MasterArmSwitch";
        public const string InstrumentPanel_UFC_page = "mbi/instrumentPanel/UFC/page";
        public const string InstrumentPanel_UFC_STPT_HighlightIndex = "mbi/instrumentPanel/UFC/STPT/highlightIndex";
        public const string InstrumentPanel_UFC_STPT_Blink = "mbi/instrumentPanel/UFC/STPT/blink";
        public const string InstrumentPanel_UFC_STPT_OPTION = "mbi/instrumentPanel/UFC/STPT/OPTION";
        public const string InstrumentPanel_UFC_COM2_HighlightIndex = "mbi/instrumentPanel/UFC/COM2/highlightIndex";
        public const string InstrumentPanel_UFC_COM2_Blink = "mbi/instrumentPanel/UFC/COM2/blink";
        public const string InstrumentPanel_UFC_COM1_HighlightIndex = "mbi/instrumentPanel/UFC/COM1/highlightIndex";
        public const string InstrumentPanel_UFC_COM1_Blink = "mbi/instrumentPanel/UFC/COM1/blink";
        public const string InstrumentPanel_UFC_CNI_HighlightIndex = "mbi/instrumentPanel/UFC/CNI/highlightIndex";
        public const string InstrumentPanel_UFC_CNI_Show_Wind = "mbi/instrumentPanel/UFC/CNI/show_wind";
        public const string InstrumentPanel_MFD_Radar_Elevation = "mbi/instrumentPanel/MFD/radar_elevation";
        public const string InstrumentPanel_MFD_Radar_Horizon = "mbi/instrumentPanel/MFD/radar_horizon";
        public const string InstrumentPanel_MFD_Radar_Range = "mbi/instrumentPanel/MFD/radar_range";
        public const string InstrumentPanel_MFD_Left_Show_Page = "mbi/instrumentPanel/MFD_left/show_page";
        public const string InstrumentPanel_MFD_Left_PageIndex = "mbi/instrumentPanel/MFD_left/pageIndex";
        public const string InstrumentPanel_MFD_Left_PageTab = "mbi/instrumentPanel/MFD_left/pageTab";
        public const string InstrumentPanel_MFD_Left_PageTabs = "mbi/instrumentPanel/MFD_left/pageTabs";
        public const string InstrumentPanel_UFC_TIME_SystemTime = "mbi/instrumentPanel/UFC/TIME/SystemTime";
        public const string InstrumentPanel_UFC_COM2_PRE_VHF = "mbi/instrumentPanel/UFC/COM2/PRE_VHF";
        public const string InstrumentPanel_UFC_COM2_VHF = "mbi/instrumentPanel/UFC/COM2/VHF";
        public const string InstrumentPanel_UFC_STPT_ELEV = "mbi/instrumentPanel/UFC/STPT/ELEV";
        public const string InstrumentPanel_UFC_STPT_LAT = "mbi/instrumentPanel/UFC/STPT/LAT";
        public const string InstrumentPanel_UFC_STPT_LNG = "mbi/instrumentPanel/UFC/STPT/LNG";
        public const string InstrumentPanel_UFC_STPT_STPT = "mbi/instrumentPanel/UFC/STPT/STPT";
        public const string InstrumentPanel_UFC_COM1_PRE_UHF = "mbi/instrumentPanel/UFC/COM1/PRE_UHF";
        public const string InstrumentPanel_UFC_TIME_DeltaTOS = "mbi/instrumentPanel/UFC/TIME/DeltaTOS";
        public const string InstrumentPanel_UFC_TIME_HackTime = "mbi/instrumentPanel/UFC/TIME/HackTime";
        public const string InstrumentPanel_UFC_COM2_PRE = "mbi/instrumentPanel/UFC/COM2/PRE";
        public const string InstrumentPanel_UFC_COM1_PRE = "mbi/instrumentPanel/UFC/COM1/PRE";
        public const string InstrumentPanel_UFC_COM1_UHF = "mbi/instrumentPanel/UFC/COM1/UHF";
        public const string InstrumentPanel_MFD_Weapon_Info = "mbi/instrumentPanel/MFD/weapon_info";
        public const string InstrumentPanel_MFD_LineOfSight = "mbi/instrumentPanel/MFD/lineOfSight";
        public const string InstrumentPanel_UFC_MasterMode = "mbi/instrumentPanel/UFC/MasterMode";
        public const string InstrumentPanel_MFD_Right_PageTab = "mbi/instrumentPanel/MFD_right/pageTab";
        public const string InstrumentPanel_MFD_Right_PageTabs = "mbi/instrumentPanel/MFD_right/pageTabs";
        public const string InstrumentPanel_MFD_Right_PageIndex = "mbi/instrumentPanel/MFD_right/pageIndex";
        public const string InstrumentPanel_MFD_Right_Show_Page = "mbi/instrumentPanel/MFD_right/show_page";

    }
}
