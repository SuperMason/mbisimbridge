﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MBILibrary.Util;
using XPlaneConnector;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using MBILibrary.Enums;
using XPlaneConnector.Instructions;
using XPlaneConnector.Controllers;
using MBISimBridge.Services;
using MBILibrary.Setting;
using System.Globalization;
using System.Collections.Concurrent;

namespace MBISimBridge.MbiSystems
{
    /// <summary>
    /// 負責處理與 X-Plane 溝通的介面
    /// </summary>
    public class XPlaneSystem : MbiBaseSystem
    {
        #region X-Plane Connector (Singleton)
        /// <summary>
        /// 紀錄 X-Plane Connector 的產生數量
        /// </summary>
        private static int instanceCNT = 0;
        /// <summary>
        /// X-Plane Connector (Singleton)
        /// </summary>
        private static XPlaneConnector.XPlaneConnector instance = null;
        /// <summary>
        /// X-Plane Connector (Singleton)
        /// </summary>
        public static XPlaneConnector.XPlaneConnector GetConnector {
            get {
                if (instance == null && instanceCNT == 0) {
                    ++instanceCNT;
                    instance = new XPlaneConnector.XPlaneConnector(UtilConstants.appConfig.XPlaneIP, UtilConstants.appConfig.XPlaneUDPPort);
                    Log($"XPlaneConnector IP({UtilConstants.appConfig.XPlaneIP}), Port({UtilConstants.appConfig.XPlaneUDPPort}) counter({instanceCNT}) instance : {instance}");
                }
                return instance;
            }
        }
        #endregion


        #region fields
        /// <summary>
        /// 監控是否已經與 X-Plane 連線成功 ? 必須取得 Subscribe Data 才算連線成功.
        /// </summary>
        private BackgroundWorker mPollingWorker = null;
        /// <summary>
        /// mPollingWorker 的執行頻率, 每一次前後隔 1 秒做一次
        /// </summary>
        private const int PollingWorkerFrequency = 1000;
        /// <summary>
        /// 累積 BackgroundWorker 的執行次數
        /// </summary>
        private int WorkerCounter = 0;
        private const string ClearTextUI_CleanAllMsg = "CleanAll";
        private const string ClearTextUI_HoldLatestMsg = "HoldLatest";
        /// <summary>
        /// 用來控制 Continuously Task, 如果取消, 就會馬上取消 Task 作業
        /// </summary>
        private CancellationTokenSource igniteToken = null;
        /// <summary>
        /// 累積跑 10 次, 清空一次畫面訊息
        /// </summary>
        private const int cleanUiN = 10;
        /// <summary>
        /// (完成作業) 將訂閱的 DataRefs 註冊到 X-Plane
        /// </summary>
        public static bool SubscribedDataRefsFinished = false;
        /// <summary>
        /// 是否已經有啟動註冊 DataRef 至 X-Plane 的 Thread ?
        /// <para>true : 已啟動, 避免重複啟動 Thread</para>
        /// <para>false : 未啟動, 可以執行註冊程序</para>
        /// </summary>
        private static bool StartWaitingSubscribedDataRefsToXP = false;
        /// <summary>
        /// 紀錄 "啟動註冊 DataRef 至 X-Plane 的 Thread" 的時間
        /// </summary>
        private static DateTime StartWaitingSubscribedDataRefsToXPDT = DateTime.MinValue;
        /// <summary>
        /// 用來記錄已經註冊過的 DataRef, 避免重複註冊
        /// </summary>
        private readonly List<string> RecordSubscribedDataRef = new List<string>();
        #endregion

        /// <summary>
        /// X-Plane 即時的連線狀態
        /// <para>true :: 連線</para>
        /// <para>false :: 斷線</para>
        /// </summary>
        public bool XPlaneConnected => UtilConstants.GetXPlaneStatus == XPlaneConnStatusEnum.ConnectedGetDataRefs;
        /// <summary>
        /// 中繼站 ConcurrentQueue 變數, 負責餵資料給 DoWorkAsyncInfiniteLoop() 去執行寫入 X-Plane
        /// <para>Count == 0 ::: 沒有資料要執行</para>
        /// <para>Count != 0 ::: 有資料, 立刻寫給 X-Plane, 寫完馬上 Dequeue() 掉 object</para>
        /// </summary>
        private readonly ConcurrentQueue<DataRefEventArgs> Queue4Process = new ConcurrentQueue<DataRefEventArgs>();
        /// <summary>
        /// Queue4Process 已經清空
        /// </summary>
        private bool IsEmptyQueue => Queue4Process.Count == 0;
        /// <summary>
        /// 控制處理 Queue 的 Flag, 如果 超過 DataRefElementBase.LetXPlaneUpdateDataRefValueWaitingMS, 就先停止執行, 等下一 run 再繼續
        /// </summary>
        private bool StopProcessQueue = false;


        #region constructor
        /// <summary>
        /// 負責處理與 X-Plane 溝通的介面
        /// </summary>
        public XPlaneSystem() {
            triggerBySystem = TriggerByWhatSystem.FromXPlane;
            EvtInterface.ExecuteAirManagerCommand += ExecuteAirManagerCommand; //從 'Air Manager' 取得 Command Write into 'X-Plane', 'GRPC', 'AirForceSystem'
            EvtInterface.ExecuteAirManagerDataRefWrite += ExecuteAirManagerDataRefWrite; //從 'Air Manager' 取得 DataRef Value Write into 'X-Plane', 'GRPC', 'AirForceSystem'
            EvtInterface.AirForceStarted += AirForceStarted; // 與 Air Force 互動
            InitXPlaneConnector(string.Format(CultureInfo.CurrentCulture, "Start {0}...", nameof(XPlaneSystem)));
        }
        #endregion


        /// <summary>
        /// 初始化 XPlaneConnector
        /// </summary>
        /// <param name="initFrom">從哪邊初始化?</param>
        private void InitXPlaneConnector(string initFrom) {
            Log($"{initFrom}");
            while (GetConnector == null) { Log($"{UtilConstants.XPlaneSoftware} Connector 尚未建立完成, waiting..."); } // 確保 XPlaneConnector 一定有 new 出來
            GetConnector.OnLog += Log;
            GetConnector.OnXPlaneConnectionStatus += OnXPlaneConnectionStatus;
            InitPollingWorker(); // PollingWorker 監控是否已經與 X-Plane 連線成功 ?
        }

        #region 處理 PollingWorker
        /// <summary>
        /// 初始化 PollingWorker, 建立後執行
        /// </summary>
        private void InitPollingWorker() {
            CreatePollingWorker();
            RunPollingWorker();
        }
        /// <summary>
        /// 建立 PollingWorker
        /// </summary>
        private void CreatePollingWorker() {
            if (mPollingWorker == null) {
                mPollingWorker = new BackgroundWorker {
                    WorkerReportsProgress = true,
                    WorkerSupportsCancellation = true
                };
                mPollingWorker.DoWork += _DoWork;
                mPollingWorker.RunWorkerCompleted += _RunWorkerCompleted;
            }
        }
        /// <summary>
        /// 執行 PollingWorker
        /// </summary>
        private void RunPollingWorker() {
            if (mPollingWorker != null && !mPollingWorker.IsBusy)
            { mPollingWorker.RunWorkerAsync(); }
        }
        /// <summary>
        /// 註銷 PollingWorker
        /// </summary>
        private void DisposePollingWorker() {
            mPollingWorker?.Dispose();
            mPollingWorker = null;
            Log(string.Format(CultureInfo.CurrentCulture, "'{0}' :: DisposePollingWorker('{1}')", UtilConstants.XPlaneSoftware, mPollingWorker));
        }
        #endregion

        /// <summary>
        /// 每 0.001 秒執行, 監控是否有 "DataRef Value" 要 Write into 'X-Plane' ?
        /// </summary>
        private async void DoWorkAsyncInfiniteLoop(int DelayMS = 1) {
            string ThreadHeader = string.Format(CultureInfo.CurrentCulture, "ThreadID({0}) ::: ", Thread.CurrentThread.ManagedThreadId);
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}與 {1} 已連線, 開始監控是否有 DataRef Value 要 Write into '{1}' ?", ThreadHeader, UtilConstants.XPlaneSoftware));

            while (true) {
                if (!XPlaneConnected) {
                    UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}與 {1} 斷線, 停止監控...", ThreadHeader, UtilConstants.XPlaneSoftware));
                    break;
                }

                if (Queue4Process.Count != 0) {
                    UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}有 DataRef Value 要 Write into '{1}'...... CNT({2})", ThreadHeader, UtilConstants.XPlaneSoftware, Queue4Process.Count));
                    ExeUpdXpDataRefValue();
                }
                await Task.Delay(DelayMS); // 每 0.001 秒 掃描是否有物件需要處理
            }
        }
        /// <summary>
        /// 寫入 X-Plane 時, 需要調整 "資料新鮮度" 的設定, 所以以 Thread 方式去執行非同步化作業, 有短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS
        /// <para>1. 針對 "資料新鮮度", 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value, 更新後, 會再把 "資料新鮮度" 調整回來 User 的設定</para>
        /// <para>2. 短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS, 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度"</para>
        /// </summary>
        private void ExeUpdXpDataRefValue() {
            double ConfigCheckXPlaneAliveMS = UtilConstants.appConfig.CheckXPlaneAliveMS;
            RevisedDataRefreshTime(DataRefElementBase.LetXPlaneUpdateDataRefValueWaitingMS); // 針對 "資料新鮮度", 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value
            TaskRun4DeQueue4Process();

            // sleep LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value
            // 如果 LetXPlaneUpdateDataRefValueWaitingMS 還沒跑完時, 就已經處理完成清空了, 一樣會跳離 SpinWait
            SpinWait.SpinUntil(() => IsEmptyQueue, DataRefElementBase.MaxAge);
            ResetAndBreakTaskRunning4Queue4Process();
            RevisedDataRefreshTime(ConfigCheckXPlaneAliveMS); // 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度" 10 ms 的要求...
        }
        /// <summary>
        /// 趁 sleep LetXPlaneUpdateDataRefValueWaitingMS 時間, 將 Queue 在 Queue4Process 的 DataRef 更新至 X-Plane
        /// </summary>
        private void TaskRun4DeQueue4Process() {
            Task.Factory.StartNew(() => {
                //int ProcessCNT = 0;
                while (Queue4Process.TryDequeue(out DataRefEventArgs QueueElement)) {
                    //++ProcessCNT;
                    UpdXpDataRefValue(QueueElement);// 取當下頭一個 element 寫入 X-Plane
                    if (StopProcessQueue || IsEmptyQueue) {
                        //UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format("該次總共處理 {0} 個", ProcessCNT));
                        break;
                    }
                }
            }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
        }
        /// <summary>
        /// 用來停止 "專門處理 Queue4Process" 的 Task.Factory.StartNew()
        /// </summary>
        private void ResetAndBreakTaskRunning4Queue4Process() {
            StopProcessQueue = true;
            SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(3));
            StopProcessQueue = false; // reset for 下一次 run
        }
        /// <summary>
        /// 針對監聽 X-Plane "資料新鮮度" 的設定, 進行調整作業
        /// <para>1. 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value</para>
        /// <para>2. 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度" 要求</para>
        /// </summary>
        private void RevisedDataRefreshTime(double ms) {
            UtilConstants.appConfig.CheckXPlaneAliveMS = ms;
            DataRefElementBase.MaxAge = TimeSpan.FromMilliseconds(UtilConstants.appConfig.CheckXPlaneAliveMS);
        }

        #region 監控是否已經與 X-Plane 連線成功
        /// <summary>
        /// 監控 X-Plane 連線狀態
        /// </summary>
        /// <param name="sender">connection物件本身</param>
        /// <param name="status">連線狀態</param>
        /// <param name="ConnStatus">連線敘述</param>
        /// <param name="CurrentStatus">取得 MBISimBridge 與 X-Plane 之間的互動狀態</param>
        private void OnXPlaneConnectionStatus(XPlaneConnector.XPlaneConnector sender, bool status, XPlaneConnStatusEnum ConnStatus, XPlaneCurrentStatusEnum CurrentStatus) {
            UtilConstants.GetXPlaneStatus = ConnStatus;
            StartWaitingSubscribedDataRefsToXP = false;
            StartWaitingSubscribedDataRefsToXPDT = DateTime.MinValue;
            Log(string.Format(CultureInfo.CurrentCulture, "No.{0} {1} ConnStatus({2}), ConnDescr({3}), CurrentStatus({4})"
                , WorkerCounter, UtilConstants.XPlaneSoftware, status, ((int)ConnStatus).ToEnumName<XPlaneConnStatusEnum>(), ((int)CurrentStatus).ToEnumName<XPlaneCurrentStatusEnum>()));

            if (!status) { InitPollingWorker(); }// 尚未與 X-Plane 連線

            if (XPlaneConnected) // 確認與 X-Plane 連線後, 再啟動 專職 Thread 處理, 斷線時, Thread 也會跟著消失...
            { Task.Factory.StartNew(() => { DoWorkAsyncInfiniteLoop(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest); }

            if (!status && CurrentStatus == XPlaneCurrentStatusEnum.BeClosed) {// X-Plane 軟體被關閉, 同步 reset XPlaneConnector
                instance = null;
                instanceCNT = 0;
                InitXPlaneConnector(string.Format(CultureInfo.CurrentCulture, "{0} be closed, reset {1}...", UtilConstants.XPlaneSoftware, nameof(XPlaneConnector.XPlaneConnector)));
            }
        }
        private void _DoWork(object sender, DoWorkEventArgs e) {
            ++WorkerCounter;
            Start_Connect();
        }
        private void _RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e) {
            if (GetConnector != null) {
                if (mPollingWorker != null && !mPollingWorker.IsBusy) {
                    if (!GetConnector.ConnectOK) {
                        if (WorkerCounter % cleanUiN == 0) {
                            //ClearTextUI(new MessageEventArgs(ClearTextUI_CleanAllMsg));
                        }
                        RunPollingWorker();
                    } else {
                        if (XPlaneConnector.XPlaneConnector.ConnStatus == XPlaneCurrentStatusEnum.WaitingToConnect)
                        { RunPollingWorker(); } // 剛從測試連線階段跳出, 只有 1 組 DataRef, 所以必須再跑一次, 將 All DataRefs Load in
                        else {
                            DisposePollingWorker();
                            //ClearTextUI(new MessageEventArgs(ClearTextUI_HoldLatestMsg)); // 已經成功取得 X-Plane 回覆, 所以清空之前的畫面訊息
                            EvtInterface.OnXPlaneConnected(this, new MessageEventArgs($"{UtilConstants.XPlaneSoftware} ConnectOK({GetConnector.ConnectOK}), Listening at ({GetConnector.LocalIPsendPort}), waiting for DataRefs..."));
                        }
                    }
                }
            }
        }
        #endregion


        #region 開始與 X-Plane 進行連線
        /// <summary>
        /// 開始與 X-Plane 進行連線
        /// </summary>
        private void Start_Connect() {
            if (GetConnector != null) {
                if (!GetConnector.ConnectOK) {
                    SubscribedDataRefsFinished = false;
                    SubscribeTestConnDataRef(); // 一開始不知道 X-Plane 是否已經開啟並連線成功, 所以用 1 個 DataRefs 去測試連線
                } else {
                    SubscribedDataRefsFinished = false;
                    Task.Factory.StartNew(() => { SubscribeToXPlane(); }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);
                }

                ConfirmSubscribeListeningCount(UtilConstants.Try2ConnectXPlane);
                GetConnector.Start(WorkerCounter);

                //Log(string.Format(CultureInfo.CurrentCulture, "測試對外網路狀態, NetworkAlive ? {0}", UtilNetwork.CheckNetworkStatus()));
                SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(PollingWorkerFrequency)); // 先等待 1 秒, 讓 GetConnector.LocalSendMsgToXPlane UdpClient 有時間可以建立
                SpinWait.SpinUntil(() => GetConnector.GetXPlaneReply); // 取得 X-Plane 的回覆動作
                SpinWait.SpinUntil(() => GetConnector.ConnectOK, TimeSpan.FromMilliseconds(PollingWorkerFrequency)); // 等待與 X-Plane 連線成功, 最多等 1 秒, 沒有成功就執行下一次, 如果 1 秒之內就連線成功, 則 ConnectOK = true 也會 by pass 這個 SpinWait
                SpinWait.SpinUntil(() => GetConnector.ServerReceiveAsyncTaskStatus == TaskStatus.RanToCompletion && GetConnector.ObserverTaskStatus == TaskStatus.RanToCompletion); // 兩個 Task 都確定執行完成了, 所以繼續下一次 BackgroundWorker
            }
        }
        /// <summary>
        /// 顯示目前訂閱的參數總個數有幾個 ?
        /// <para>1. byte[]  的參數一次只會訂閱 1 組 id</para>
        /// <para>2. string 的參數則是看其字串長度而定, 假如該字串定義 40 個字元, 那麼會拆分成 40 個 char 去訂閱, 所以一個 string 一共會訂閱 40 組 id</para>
        /// <para>3. X-Plane 的監聽機制是針對每一個 id 去 feedback 回來, 如果該 "訂閱 id" 的值有異動, 則會馬上反應回來</para>
        /// <para>4. 假設 string 有 40 的長度, 而第 33 個 char 有異動, 則此時, X-Plane 會針對第 33 個 char 的異動值, 馬上反應回來</para>
        /// </summary>
        private void ConfirmSubscribeListeningCount(string triggerFrom) {
            UtilGlobalFunc.SetMBISimBridgeConsoleTitleMessage(); // app 在尚未與 X-Plane 連線成功時, 更新最新狀態
            Log($"========== No.{WorkerCounter} {triggerFrom} ==========");
        }
        #endregion

        #region Subscribe DataRefs
        /// <summary>
        /// 依據 AirManagerSystem 那邊的 DataRef 訂閱數, 全數註冊至 X-Plane
        /// </summary>
        private void SubscribeToXPlane() {
            string StartEnrollProcMsg = string.Format(CultureInfo.CurrentCulture, "啟動註冊 '{0}' 程序", UtilConstants.XPlaneSoftware);
            if (StartWaitingSubscribedDataRefsToXP) {
                Log(string.Format(CultureInfo.CurrentCulture, "不需要重複{0}, 於 {1} 已經有啟動 1 個註冊 Thread", StartEnrollProcMsg, StartWaitingSubscribedDataRefsToXPDT.ToString(UtilDateTime.Format_HHmmssfffWithDot)));
            } else {
                StartWaitingSubscribedDataRefsToXP = true;
                StartWaitingSubscribedDataRefsToXPDT = DateTime.Now;
                Log(string.Format(CultureInfo.CurrentCulture, "第 1 次{0}, '{1}' 先等待 '{2}' 註冊結果, 因為有可能有些 DataRef 會註冊失敗", StartEnrollProcMsg, UtilConstants.XPlaneSoftware, UtilConstants.AirManagerSoftware));
                SpinWait.SpinUntil(() => AirManagerService.SubscribedDataRefsFinished);
                Log(string.Format(CultureInfo.CurrentCulture, "'{0}' 已經註冊完畢{1}", UtilConstants.AirManagerSoftware, Environment.NewLine));

                if (GetConnector != null) {
                    RecordSubscribedDataRef.Clear();
                    string SubscribeDataRefCntMsg = string.Format(CultureInfo.CurrentCulture, "{0} 個 DataRefs", InitParamsCtrller.SubscribedDataRefs.Count);
                    string RecordSubscribedDataRefMeaningMsg = string.Format(CultureInfo.CurrentCulture, "'{0}' 紀錄的總筆數, 包含 '單一 DataRef' + '分拆 Array DataRef'", nameof(RecordSubscribedDataRef));
                    Log(string.Format(CultureInfo.CurrentCulture, "{0} 已成功註冊至 '{1}'", SubscribeDataRefCntMsg, UtilConstants.AirManagerSoftware));
                    Log(string.Format(CultureInfo.CurrentCulture, "'{0}' 開始註冊 {1}, RecordSubscribedDataRef(初始,{2}), {3}", UtilConstants.XPlaneSoftware, SubscribeDataRefCntMsg, RecordSubscribedDataRef.Count, RecordSubscribedDataRefMeaningMsg));

                    int CNT = 0, SkipCNT = 0;
                    string[] FilterOutDR = string.IsNullOrEmpty(UtilConstants.appConfig.SkipDataRefSubscribeToXP) ? null : UtilConstants.appConfig.SkipDataRefSubscribeToXP.Split(UtilConstants.CommandLineSplitor);
                    InitParamsCtrller.SubscribedDataRefs.ForEach(o => {
                        string NoMsg = string.Format(CultureInfo.CurrentCulture, "({0}). ", ++CNT);
                        string SubscribedMsg = string.Format(CultureInfo.CurrentCulture, "Subscribed to '{0}', DataRef({1})", UtilConstants.XPlaneSoftware, o.DataRef);
                        bool SkipIt = !FilterOutDR.IsListEmpty() && FilterOutDR.Where(s => !string.IsNullOrEmpty(s) && o.DataRef.StartsWith(s)).FirstOrDefault() != null;
                        if (SkipIt) {
                            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}Skip Not {1}", NoMsg, SubscribedMsg));
                            SkipCNT++;
                        } else {
                            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0}{1}", NoMsg, SubscribedMsg));
                            AddDataRefIntoXPlane(o);
                        }
                    });
                    Log(string.Format(CultureInfo.CurrentCulture, "'{0}' 結束註冊 {1}, RecordSubscribedDataRef(全部,{2}), {3}", UtilConstants.XPlaneSoftware, SubscribeDataRefCntMsg, RecordSubscribedDataRef.Count, RecordSubscribedDataRefMeaningMsg));
                    SubscribedDataRefsFinished = true;
                    string ResMsg = string.Format(CultureInfo.CurrentCulture, ", {0} 個 DataRefs 忽略註冊至 '{1}', (因 '{2}' 設定檔定義了 '{3}')", SkipCNT, UtilConstants.XPlaneSoftware, nameof(AppConfig), GetFilterOutDRmsg(FilterOutDR));
                    Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.SubscribeDataRefSuccessMessage, UtilConstants.XPlaneSoftware, InitParamsCtrller.SubscribedDataRefs.Count, ResMsg));
                }
            }
        }
        /// <summary>
        /// 取得實際進行過濾的 DataRef Name 標頭字串
        /// </summary>
        private string GetFilterOutDRmsg(string[] ary) => ary.IsListEmpty() ? string.Empty : string.Join(UtilConstants.CommandLineSplitor, ary);

        /// <summary>
        /// 將 Array DataRef 分拆並註冊到 X-Plane
        /// <para>分拆 n 個 DataRef 進行註冊</para>
        /// <para>例如 :: DataRef_xxx[3] 要分拆成 3 個 進行註冊</para>
        /// <para>DataRef_xxx[0], DataRef_xxx[1], DataRef_xxx[2] 進行 Subscribe</para>
        /// </summary>
        private void SubscribeArrayToXPlane(DataRefElement dr) {
            for (int i = 0; i < dr.ArrayLen; i++) // 不管是 int[], float[], double[] 全部分拆成 ArrayLen 個註冊
            { SubscribeDR(DataRefs.GetDataRefElement(dr.GetDataRefIdx(i), dr.RawType, dr.Writable, dr.Units, dr.Description, dr.Frequency)); }
        }
        /// <summary>
        /// 註冊 DataRefElement
        /// </summary>
        private void SubscribeDR(DataRefElement dr) {
            if (!RecordSubscribedDataRef.ToArray().Any(s => s.Equals(dr.DataRef))) {
                GetConnector.Subscribe(dr, dr.Frequency, NotifyChangedToAirManager);
                RecordSubscribedDataRef.Add(dr.DataRef);
            }
        }
        /// <summary>
        /// 逐筆加入 DataRef
        /// <para>1. 將 DataRefElementBase 的 callback function 與 XPlaneConnector 做連結</para>
        /// </summary>
        private void AddDataRefIntoXPlane(DataRefElementBase drBase) {
            try {
                if (drBase.IsDataRefElement) {
                    var dr = (DataRefElement)drBase;
                    if (dr.TypeIsArray) { SubscribeArrayToXPlane(dr); }// int[], float[], double[] Array 值的註冊, 要分拆 n 個 Elements
                    else if (dr.IsSingleType) { SubscribeDR(dr); }// byte, int, float, double 單一值的註冊
                    else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElement), DataRefs.ManuallyDefinitionType, dr.Type)); }
                } else if (drBase.IsStringDataRefElement) {
                    var dr = (StringDataRefElement)drBase;
                    if (dr.IsStringType) {
                        if (!RecordSubscribedDataRef.ToArray().Any(s => s.Equals(dr.DataRef))) {
                            GetConnector.Subscribe(dr, dr.Frequency, NotifyChangedToAirManager); // string 單一值的註冊
                            RecordSubscribedDataRef.Add(dr.DataRef);
                        }
                    } else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(StringDataRefElement), DataRefs.ManuallyDefinitionType, dr.Type)); }
                } else { Log(string.Format(CultureInfo.CurrentCulture, DataRefs.TypeNotDefineErrMsg, nameof(DataRefElementBase), DataRefs.SystemClassType, drBase.GetType())); }
            } catch (Exception ex) { Log($"Exception ::: {ex.Message}"); }
        }
        /// <summary>
        /// 訂閱 1 個 Test Connection 的 DataRef ::: DataRefs.CockpitElectricalBatteryOn(電池是否開啟?)
        /// </summary>
        private void SubscribeTestConnDataRef() {
            if (GetConnector != null) {
                Log(string.Format(CultureInfo.CurrentCulture, "using '{0}' to check '{1}' connection is Ready or NOT ?", DataRefs.CockpitElectricalBatteryOn.DataRef, UtilConstants.XPlaneSoftware));
                SubscribeDR(DataRefs.CockpitElectricalBatteryOn);
            }
        }
        #endregion

        /// <summary>
        /// 更新已註冊於 X-Plane 上的 DataRef 物件
        /// </summary>
        private void RefreshXPDataRefSubscribed(DataRefElement dr) {
            if (GetConnector != null && GetConnector.LocalSendMsgToXPlane != null) {
                GetConnector.UnSubscribe(dr); // 先取消監聽該 DataRef value
                var existed = RecordSubscribedDataRef.ToArray().Where(s => !string.IsNullOrEmpty(s) && s.Equals(dr.DataRef)).FirstOrDefault();
                if (!string.IsNullOrEmpty(existed)) { RecordSubscribedDataRef.Remove(existed); }
                SubscribeDR(dr); // 再重新註冊一次
            }
        }

        #region 控制 UI 上的訊息顯示
        /// <summary>
        /// 清空 console UI 上的文字內容
        /// </summary>
        private void ClearTextUI(MessageEventArgs e) {
            try {
                if (e.Data.Equals(ClearTextUI_HoldLatestMsg)) {
                    var XPlaneResponseMessages = UtilGlobalFunc.ConsoleWriter.ToString().Split(Environment.NewLine).ToArray().Where(s => s.Contains(UtilConstants.XPlanePatternStart)).ToList();
                    ClearAll();
                    for (int i = 0; i < XPlaneResponseMessages.Count; i++) {
                        string substringPattern = "- ";
                        Log(XPlaneResponseMessages[i].Substring(XPlaneResponseMessages[i].IndexOf(substringPattern) + substringPattern.Length));
                    }
                } else { ClearAll(); }
            } catch (Exception ex) { Log($"ClearTextUI() Exception :: {ex.Message}"); }
        }
        private void ClearAll() {
            //Console.Clear();
            UtilGlobalFunc.ConsoleWriter.Clear();
        }
        #endregion

        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// <para>byte, float, int, bool, double 的更新, 皆以 float 帶入 X-Plane</para>
        /// <para>實際測試結果為 ::: 如果 SPEC 定義 type 為 int, bool 時, 要帶入 float 才能順利寫入值</para>
        /// <para>因為是要寫入 'X-Plane', 所以必須成功註冊至 'X-Plane' 後, 才能成功寫入</para>
        /// <para>offset 是位移量, 值從 0 開始, user 可以透過 offset 調整寫入 DataRef[i] 的起始位置</para>
        /// <para>從 'Air Manager' 過來的指令, DLL API 沒有提供 offset 功能, 必須完整的 Array 才可以執行</para>
        /// </summary>
        private void ExecuteAirManagerDataRefWrite(object sender, DataRefEventArgs e) {
            if (GetConnector != null && GetConnector.LocalSendMsgToXPlane != null) {
                if (e == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefEventArgs), e)); return; }
                if (e.DataBus4Object == null) { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.ObjectIsNullMsg, nameof(DataRefElementBase), e.DataBus4Object)); return; }
                Queue4Process.Enqueue(e); // 加入 Queue, 等待 DoWorkAsyncInfiniteLoop() 處理
            }
        }
        /// <summary>
        /// 寫入 X-Plane 時, 需要調整 "資料新鮮度" 的設定, 所以以 Thread 方式去執行非同步化作業, 有短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS
        /// <para>1. 針對 "資料新鮮度", 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value, 更新後, 會再把 "資料新鮮度" 調整回來 User 的設定</para>
        /// <para>2. 短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS, 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度"</para>
        /// <para>ALL data type ::: int, float, double, int[], float[], double[], string</para>
        /// <para>DataRef Name ::: Single(xxx/xx), Array(xxx/xx[i])</para>
        /// </summary>
        private void UpdXpDataRefValue(DataRefEventArgs e) {
            if (e.DataRefIsWritable) {
                if (e.ObjCheck != null) {
                    bool WriteIt = false;

                    if (e.DataBus4ObjectIsBytes) {
                        WriteIt = true;
                        if (e.ObjCheck.GetType() == typeof(char)) {
                            GetConnector.SetDataRefValue(e.DataRefName, e.ObjCheck.ConvertObjectToChar());
                        } else {
//把這一 function() 與原本的 code 整合試試...
                            GetConnector.SetDataRefValueNEW(e.DataRefName, e.ObjCheck);// 立即執行更新 DataRef value                        
                        }
                    } else if (e.DataBus4ObjectIsString) {
                        WriteIt = true;
                        GetConnector.SetDataRefValue(e.DataRefName, e.ObjCheck.ConvertObjectToString());
                    }

                    if (WriteIt)
                    { UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueInfo, GetType().Name, e.TriggerByWhichSystem, UtilConstants.XPlaneSoftware, e.DataRefName, e.DataRefType, e.DataRefObjValShowString)); }
                }
            } else { Log(string.Format(CultureInfo.CurrentCulture, UtilConstants.DataRefCanNotWrittenMessage, e.DataRefName, UtilConstants.Flag_Yes)); }
        }

        #region 基本 Command 指令測試, "Battery On/Off", "RadiosStbyNav1Fine Up/Down", "X-Plane Quit"
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        private void ExecuteAirManagerCommand(object serder, CommandEvevntArgs e) {
            if (GetConnector != null) {
                // 寫入 X-Plane 時, 需要調整 "資料新鮮度" 的設定, 所以要改成 Thread 方式去執行非同步化作業, 有短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value, 更新後, 會再把 "資料新鮮度" 調整回來 User 的設定
                Task.Factory.StartNew(() => {
                    #region 針對 "資料新鮮度", 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value, 同時, 短暫 sleep LetXPlaneUpdateDataRefValueWaitingMS, 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度"
                    double ConfigCheckXPlaneAliveMS = UtilConstants.appConfig.CheckXPlaneAliveMS;
                    RevisedDataRefreshTime(DataRefElementBase.LetXPlaneUpdateDataRefValueWaitingMS); // 針對 "資料新鮮度", 先改成 LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value
                    SpinWait.SpinUntil(() => false, DataRefElementBase.MaxAge); // sleep LetXPlaneUpdateDataRefValueWaitingMS, 讓 X-Plane 有短暫時間可以完成更新 DataRef value
                    ExeCommand(e.Command);
                    RevisedDataRefreshTime(ConfigCheckXPlaneAliveMS); // 確認 X-Plane 更新 DataRef value 完成後, 再調回原本的 "資料新鮮度" 要求...
                    #endregion
                }, CancellationToken.None, TaskCreationOptions.None, PriorityScheduler.Highest);

                UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.ExecuteCommandInfo, GetType().Name, e.Command.Name, e.Command.Value, e.Command.Description));
                #region 關閉 X-Plane 之後, 繼續開始監聽 X-Plane 是否再度開啟 ?
                if (e.Command.Name.Equals(Commands.OperationQuit.Name)) {
                    //GetConnector.QuitXPlane(); //關閉 X-Plane 也可以使用這個
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(1000));// 中斷 connection 後, 先等 3 秒, 等 X-Plane 確定關閉完成
                    InitPollingWorker();
                }
                #endregion
            }
        }
        /// <summary>
        /// 執行 X-Plane 的 Command 指令
        /// </summary>
        private void ExeCommand(XPlaneCommand cmd) { if (GetConnector != null && GetConnector.LocalSendMsgToXPlane != null) { GetConnector.SendCommand(cmd); } }
        #endregion

        /// <summary>
        /// 空戰已開始
        /// </summary>
        private void AirForceStarted(object sender, MessageEventArgs e)
        {
            Log($"{e.Data}, 傳送相關指令給 {UtilConstants.XPlaneSoftware}");
        }

        #region Start/Stop Continuously Command, 用來測試 X-Plane 用的
        private void StartIgniteToken() {
            if (GetConnector != null) {
                if (GetConnector.LocalSendMsgToXPlane != null) {
                    if (igniteToken == null)
                    { igniteToken = GetConnector.StartContinuouslyCommand(Commands.EnginesEngageStarters); }
                    else
                    { StopIgniteToken(); }
                }
            }
        }
        /// <summary>
        /// 如果有啟動 IgniteToken, 先強制停止 IgniteToken
        /// </summary>
        private void StopIgniteToken() {
            if (GetConnector != null) {
                if (igniteToken != null) {
                    GetConnector.StopContinuouslyCommand(igniteToken);
                    igniteToken = null;
                }
            }
        }
        #endregion

    }
}