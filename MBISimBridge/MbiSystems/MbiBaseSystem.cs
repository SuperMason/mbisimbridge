﻿using MBILibrary.Enums;
using MBILibrary.Util;
using System.Globalization;
using XPlaneConnector;
using XPlaneConnector.Controllers;
using XPlaneConnector.DataRef;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;
using XPlaneConnector.DataSwitch.MbiEvtInterfaces;

namespace MBISimBridge.MbiSystems
{
    public abstract class MbiBaseSystem
    {
        #region Properties
        /// <summary>
        /// Global Event Interface for subscribing Event Handler
        /// </summary>
        public static EvtInterfaceService EvtInterface { get; set; }
        /// <summary>
        /// 觸發更新 Air Manager DataRef value 的系統名稱
        /// <para>注意!!! 只有 X-Plane 可以直接從內部進入, 其餘外部系統, 皆必須由上一層 function 進入</para>
        /// </summary>
        public TriggerByWhatSystem triggerBySystem = TriggerByWhatSystem.FromInitial;
        /// <summary>
        /// 注意!!! 只有 X-Plane 可以直接從內部進入, 其餘外部系統, 皆必須由上一層 function 進入
        /// </summary>
        protected bool IsInnerSystem { get { return triggerBySystem == TriggerByWhatSystem.FromInitial; } }
        /// <summary>
        /// 因為 object or value is null, 所以不需要去通知 Air Manager
        /// </summary>
        private static string DoNotNeedtoNotifyAM = string.Format(CultureInfo.CurrentCulture, "{0}.{1}, Do NOT Notify to '{2}'", nameof(MbiBaseSystem), "{0} :: {1}({2}) is null", UtilConstants.AirManagerSoftware);
        private const string EventFunc4ByteArray = "NotifyChangedToAirManagerByte[]";
        private const string EventFunc4String = "NotifyChangedToAirManagerString";
        /// <summary>
        /// Update "X-Plane", "Air Manager" Data
        /// </summary>
        public WriteDataRefCtrller WriteDrCtrller = new WriteDataRefCtrller();
        #endregion


        #region 通知 "Air Manager" DataRef value 有異動
        /// <summary>
        /// 通知 "Air Manager" DataRef value 有異動
        /// <para>1. Update Value 至 'Air Manager'</para>
        /// <para>2. Contain all data type</para>
        /// </summary>
        protected void NotifyAirManagerValueChanged(TriggerByWhatSystem triggerBy, string FuncName, string RawDataRef, object val) {
            bool WriteSuccess = false;
            DataRefElementBase drBase = WriteDrCtrller.GetInitDataRefElementBase(RawDataRef, val);
            if (drBase != null) { WriteSuccess = true; NotifyAirManagerValueChanged(triggerBy, FuncName, drBase, val); }
            if (!WriteSuccess) { UtilGlobalFunc.Log(RawDataRef, triggerBy, FuncName); }
        }
        /// <summary>
        /// 通知 "Air Manager" DataRef value 有異動
        /// <para>1. Update Value 至 'Air Manager'</para>
        /// <para>2. Contain all data type</para>
        /// </summary>
        protected void NotifyAirManagerValueChanged(TriggerByWhatSystem triggerBy, string FuncName, DataRefElementBase drBase, object val) {
            if (drBase == null || val == null) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "DataRefObj({0}) or value({1}) is null, {2}.", drBase, val, UtilGlobalFunc.GetTriggerByWhatFunc(triggerBy, FuncName))); return; }

            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, "{0} : '{1}' Value({2})", UtilGlobalFunc.GetTriggerByWhatFunc(triggerBy, FuncName), drBase.RawDataRef, UtilGlobalFunc.GetPrecisionValue(val)));
            triggerBySystem = triggerBy;
            drBase.ObjVal = val;

            bool WriteSuccess = false;
            if (drBase.IsStringDataRefElement && drBase.IsStringType && drBase.IsObjValTypeString) {
                var sdr = InitParamsCtrller.GetStringDataRefElementFromInit(drBase); // 註冊總表 ::: string
                if (sdr != null) { WriteSuccess = true; NotifyChangedToAirManager((StringDataRefElement)drBase, drBase.ObjVal2String); }
            } else if (drBase.IsDataRefElement) {
                var dr = InitParamsCtrller.GetDataRefElementFromInit(drBase); // 註冊總表 ::: int, float, double, int[], float[], double[]
                if (dr != null) {
                    if (drBase.IsSingleType && drBase.IsObjValSingleValue) {
                        if (drBase.IsSingleIntType && drBase.IsObjValTypeInt) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement, drBase.ObjVal2Int.ConvertToByteArray()); }
                        else if (drBase.IsSingleByteType && drBase.IsObjValTypeByte) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement, drBase.ObjVal2Bytes); }
                        else if (drBase.IsSingleFloatType && drBase.IsObjValTypeFloat) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement, drBase.ObjVal2Float.ConvertToByteArray()); }
                        else if (drBase.IsSingleDoubleType && drBase.IsObjValTypeDouble) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement, drBase.ObjVal2Double.ConvertToByteArray()); }
                    } else if (drBase.TypeIsArray && drBase.IsObjValArrayValue) {
                        if (drBase.IsArrayIntType && drBase.IsObjValTypeIntArray) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement); }
                        else if (drBase.IsArrayFloatType && drBase.IsObjValTypeFloatArray) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement); }
                        else if (drBase.IsArrayDoubleType && drBase.IsObjValTypeDoubleArray) { WriteSuccess = true; NotifyChangedToAirManager(drBase as DataRefElement); }
                    }
                }
            }

            if (!WriteSuccess) { UtilGlobalFunc.Log(drBase.RawDataRef, triggerBy, FuncName); }
        }
        #endregion




        #region 通知 "Air Manager" DataRef value 有異動, Update Value 至 "Air Manager" 的 C++ Wrapper
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: byte[]
        /// <para>1. 只有 X-Plane 可以直接從這邊進入, 其餘外部系統, 皆必須由上一層 function 進入</para>
        /// <para>2. StringDataRefElement 所分拆的 n 個 DataRefElement callback 不會到這邊處理, 會直接在 XPlaneConnector.cs 那邊作業, 這邊是專門處理直接註冊 DataRefElement 的物件</para>
        /// </summary>
        protected void NotifyChangedToAirManager(DataRefElement dr, byte[] newValue) {
            UtilGlobalFunc.SetMBISimBridgeConsoleTitleMessage(); // 透過 X-Plane 持續有資料流入時, 順便更新最新狀態
            if (dr == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4ByteArray, nameof(DataRefElement), dr)); return; }
            if (UtilGlobalFunc.IsArrayEmpty(newValue))
            { Log(string.Format(CultureInfo.CurrentCulture, "{0}, DataRef({1})", string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4ByteArray, "byte[]", newValue), dr.DataRef)); return; }
            dr.RawByteValue = newValue.DeepClone();
            dr.CheckObjValFrRawByteValue();
            if (IsInnerSystem) { triggerBySystem = TriggerByWhatSystem.FromXPlane; }
            WriteToAirManager(dr, new DataRefEventArgs($"({dr.DataRef})", dr, triggerBySystem));
        }
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: string
        /// <para>只有 X-Plane 可以直接從這邊進入, 其餘外部系統, 皆必須由上一層 function 進入</para>
        /// </summary>
        protected void NotifyChangedToAirManager(StringDataRefElement sdr, string newValue) {
            if (sdr == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4String, nameof(StringDataRefElement), sdr)); return; }
            if (string.IsNullOrEmpty(newValue))
            { Log(string.Format(CultureInfo.CurrentCulture, "{0}, DataRef({1})", string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, EventFunc4String, "string", newValue), sdr.DataRef)); return; }
            sdr.RawStringValue = newValue.DeepClone();
            sdr.CheckObjValFrRawStringValue();
            if (IsInnerSystem) { triggerBySystem = TriggerByWhatSystem.FromXPlane; }
            WriteToAirManager(sdr, new DataRefEventArgs($"({sdr.DataRef})", sdr, triggerBySystem));
        }
        /// <summary>
        /// 主動更新 "Air Manager" DataRef value :: '單一值' int, float, double, string
        /// <para>注意 !!! Array DataRef 不是從這邊觸發更新</para>
        /// </summary>
        private void WriteToAirManager(DataRefElementBase drBase, DataRefEventArgs e) {
            if (drBase == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, "NotifyChangedToAirManagerSingle", nameof(DataRefElementBase), drBase)); return; }
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueIntoAM, triggerBySystem, UtilConstants.AirManagerSoftware, drBase.Type, drBase.DataRef, drBase.ObjValShowString));
            EvtInterface.OnUpdateAirManagerDataRefSingleVal(this, e);
        }
        /// <summary>
        /// 主動/手動更新 "Air Manager" DataRef value :: Array值 int[], float[], double[]
        /// <para>1. X-Plane 背景自動更新, 自行由 BackgroundWorker 組裝執行完成, 因 X-Plane 分拆 array element[i] 回覆時間有時間差, 必須等待到齊後, 才能統一寫入 AM</para>
        /// <para>2. AirForceSystem / GRPC 手動更新, 自行控制 array element[i] 完整到齊後, 統一寫入 AM</para>
        /// <para>Array DataRef value updated 同步至 Air Manager 的 C++ Wrapper</para>
        /// </summary>
        private void NotifyChangedToAirManager(DataRefElementBase drBase) {
            if (drBase == null) { Log(string.Format(CultureInfo.CurrentCulture, DoNotNeedtoNotifyAM, "NotifyChangedToAirManagerArray", nameof(DataRefElementBase), drBase)); return; }
            UtilGlobalFunc.ShowMsgByDebug4BigMason(string.Format(CultureInfo.CurrentCulture, UtilConstants.WriteDataRefValueIntoAM, triggerBySystem, UtilConstants.AirManagerSoftware, drBase.Type, drBase.DataRef, drBase.ObjValShowString));
            EvtInterface.OnManualUpdateAirManagerDataRefArrayVal(drBase, new DataRefEventArgs($"({drBase.DataRef})", drBase, drBase.ObjVal, triggerBySystem));
        }
        #endregion

        #region General Function()
        /// <summary>
        /// Execute Command to 'X-Plane'
        /// </summary>
        protected void ExecuteCommandToXPlane(XPlaneCommand cmd) {
            if (cmd == null) { Log(string.Format(CultureInfo.CurrentCulture, "Command({0}) is null.", cmd)); return; }
            EvtInterface.OnExecuteAirManagerCommand(this, new CommandEvevntArgs(cmd.Description, cmd));
        }
        /// <summary>
        /// 統一紀錄 MbiSystem 的 Log
        /// </summary>
        protected static void Log(string msg) => UtilGlobalFunc.Log(msg);

        /// <summary>
        /// Only for AircraftSystem to get 'DataRefElement'
        /// </summary>
        protected static DataRefElement GetDataRefElementFromInit(string name) => InitParamsCtrller.GetDataRefElementFromInit(name);
        /// <summary>
        /// Only for AircraftSystem to get 'StringDataRefElement'
        /// </summary>
        protected static StringDataRefElement GetStringDataRefElementFromInit(string name) => InitParamsCtrller.GetStringDataRefElementFromInit(name);
        #endregion


        /// <summary>
        /// Write DataRef To Air Manager for AircraftSystem
        /// </summary>
        protected void WriteDataRefToAirManager4AircraftSystem(DataRefElementBase drBase, object obj)
        { NotifyAirManagerValueChanged(TriggerByWhatSystem.FromAircraftSys, nameof(WriteDataRefToAirManager4AircraftSystem), drBase, obj); }
    }
}
