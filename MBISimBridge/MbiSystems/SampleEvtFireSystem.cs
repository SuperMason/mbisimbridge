﻿using System;
using XPlaneConnector.DataSwitch.CustomJsonMessage;
using XPlaneConnector.DataSwitch.CustomJsonMessage.MbiEventArgs;

namespace MBISimBridge.MbiSystems
{
    public class SampleEvtFireSystem : MbiBaseSystem
    {
        #region constructor
        public SampleEvtFireSystem()
        { EvtInterface.StartElectricalCheck += StartElectricalCheck; }

        #endregion
        /// <summary>
        /// 啟動電力系統檢查
        /// </summary>
        private void StartElectricalCheck(object sender, MessageEventArgs e) {
            Log($"{e.Data}");
            e.electricalProperty = new ElectricalProperty
            { A1 = "aa11", A2 = 258, A3 = 0.123f, A4 = 0.987654321, AbcDef = true, UpdatingTime = DateTime.Now, UpdatingEnable = false  };
            e.Data = "假裝檢查所有電力系統..相關流程參閱 SPEC... 檢查完畢.. 下一步啟動空戰系統";
            EvtInterface.OnStartAirForceSystem(this, e);
        }
    }
}
