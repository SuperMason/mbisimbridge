﻿namespace AirManagerAPICallbackDefine
{
    /// <summary>
    /// <para>1. 給 C++ Wrapper Project 參考宣告使用</para>
    /// <para>2. 給 C# asign function 使用</para>
    /// <para>3. 這樣 C# 就可以將資訊同步至 C++</para>
    /// </summary>
    /// <param name="size"></param>
    /// <param name="message"></param>
    public delegate void ManagedCallback(int size, string message);
    public delegate void IntVariableManagedCallback(int value);//20211021-1,Zac
    public unsafe delegate void IntArrayVariableManagedCallback(int* value,int length);//20220125-1,Zac
    public unsafe delegate void ByteArrayVariableManagedCallback(byte* value, int length);
    public unsafe delegate void FloatArrayVariableManagedCallback(float* value, int length);//20220125-3,Zac
    public unsafe delegate void DoubleArrayVariableManagedCallback(double* value, int length);//20220516-1,Zac
    public delegate void ByteVariableManagedCallback(byte value);//20211021-1,Zac
    public delegate void FloatVariableManagedCallback(float value);//20211021-1,Zac
    public delegate void DoubleVariableManagedCallback(double value);//20220125-2,Zac
    public delegate void StringVariableManagedCallback(string value);//20211021-1,Zac
    public class Class1
    {
    }
}
