﻿#pragma once
#include "MBIAmAPIBase.h"
#define DLL_CLASS_EXPORTS
using namespace System;
using namespace AirManagerAPICallbackDefine;
namespace MBIAmAPI {
	public ref class MBIAmAPIWrapper
	{
	public:
		MBIAmAPIWrapper();
		~MBIAmAPIWrapper();
		!MBIAmAPIWrapper();
		//void init(list<pair<const char *, callback_function>> cmdlist);
		void init(const char* DATA_SOURCE_NAME, int DATA_SOURCE_UDP_PORT);
		void start();
		void close();
		void sendFloat(const char * name, float value);
		void sendFloatArray(const char* name, float* value);//20220125-3,Zac
		void sendDouble(const char* name, double value);//20220125-2,Zac
		void sendDoubleArray(const char* name, double* value);//20220516-1,Zac
		void sendInt(const char * name, int value);
		void sendIntArray(const char* name, int* value);//20220125-1,Zac
		void sendString(const char * name, const char * value);
		void sendByte(const char* name, Byte value);//20220125-1,Zac

		//void DoSomething(ManagedCallback ^ callback);
		void AddCommandProvider(const char * name, ManagedCallback ^ callbackFuncInput);
		void AddVariableProvider(const char * name,int type, float value);
		void AddVariableProvider(const char * name, int type, int value,IntVariableManagedCallback ^ callbackFuncInput);//20211021-1,Zac
		void AddVariableProvider(const char* name, int type, int* value, int arraySize, IntArrayVariableManagedCallback ^ callbackFuncInput);//20220125-1,Zac
		void AddVariableProvider(const char * name, int type, float value,FloatVariableManagedCallback ^ callbackFuncInput);//20211021-1,Zac
		void AddVariableProvider(const char* name, int type, float* value, int arraySize, FloatArrayVariableManagedCallback ^ callbackFuncInput);//20220125-3,Zac
		void AddVariableProvider(const char* name, int type, double value, DoubleVariableManagedCallback ^ callbackFuncInput);//20220125-2,Zac
		void AddVariableProvider(const char* name, int type, double* value, int arraySize, DoubleArrayVariableManagedCallback^ callbackFuncInput);//20220516-1,Zac
		void AddVariableProvider(const char * name, int type, const char* value, StringVariableManagedCallback ^ callbackFuncInput);//20211021-1,Zac
		void AddVariableProvider(const char* name, int type, Byte value, ByteVariableManagedCallback ^ callbackFuncInput);//20220125-1,Zac
	private:
		MBIAmAPIBase* m_MBIAmAPIBase;
	};
}
