#include "pch.h"

#include "MBIAmAPIWrapper.h"

namespace MBIAmAPI {
	MBIAmAPIWrapper::MBIAmAPIWrapper()
	{
		m_MBIAmAPIBase = new MBIAmAPIBase();
	}
	MBIAmAPIWrapper::~MBIAmAPIWrapper()
	{
		delete m_MBIAmAPIBase;
	}
	MBIAmAPIWrapper::!MBIAmAPIWrapper()
	{
		delete m_MBIAmAPIBase;
	}
	//void MBIAmAPIWrapper::DoSomething(ManagedCallback ^ callback)
	//{
	//	System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callback);

	//	m_MBIAmAPIBase->DoSomething(static_cast<callback_function>(callbackPtr.ToPointer()));
	//}
	//void MBIAmAPIWrapper::init(list<pair<const char *, callback_function>> cmdlist)
	void MBIAmAPIWrapper::init(const char* DATA_SOURCE_NAME, int DATA_SOURCE_UDP_PORT)
	{
		if (DATA_SOURCE_NAME == nullptr) {
			return;
		}
		m_MBIAmAPIBase->init(DATA_SOURCE_NAME, DATA_SOURCE_UDP_PORT);
	}
	void MBIAmAPIWrapper::AddCommandProvider(const char* name, ManagedCallback^ callbackFuncInput) {//20211021-1,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddCommandProvider(name, static_cast<callback_function>(callbackPtr.ToPointer()));
	}

	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, int value, IntVariableManagedCallback^ callbackFuncInput) {//20211021-1,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, static_cast<intCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, float value, FloatVariableManagedCallback^ callbackFuncInput) {//20211021-1,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, static_cast<floatCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, const char* value, StringVariableManagedCallback^ callbackFuncInput) {//20211021-1,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, static_cast<strCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, Byte value, ByteVariableManagedCallback^ callbackFuncInput) {//20220125-1,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, static_cast<intCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, int* value, int arraySize, IntArrayVariableManagedCallback^ callbackFuncInput) {//20220125-1,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value,arraySize, static_cast<intPointCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, float* value, int arraySize, FloatArrayVariableManagedCallback^ callbackFuncInput) {//20220125-3,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, arraySize, static_cast<floatPointCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, double* value, int arraySize, DoubleArrayVariableManagedCallback^ callbackFuncInput) {//20220516-1,Zac
		if (name == nullptr || value == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, arraySize, static_cast<doublePointCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, double value, DoubleVariableManagedCallback^ callbackFuncInput) {//20220125-2,Zac
		if (name == nullptr || callbackFuncInput == nullptr) {
			return;
		}
		System::IntPtr callbackPtr = System::Runtime::InteropServices::Marshal::GetFunctionPointerForDelegate(callbackFuncInput);
		m_MBIAmAPIBase->AddVariableProvider(name, type, value, static_cast<doubleCallback_function>(callbackPtr.ToPointer()));
	}
	void MBIAmAPIWrapper::AddVariableProvider(const char* name, int type, float value) {
		if (name == nullptr) {
			return;
		}
		m_MBIAmAPIBase->AddVariableProvider(name, type, value);
	}
	void MBIAmAPIWrapper::close()
	{
		m_MBIAmAPIBase->close();
	}
	void MBIAmAPIWrapper::start()
	{
		m_MBIAmAPIBase->start();
	}
	void MBIAmAPIWrapper::sendFloat(const char* name, float value)
	{
		if (name == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendFloat(name,value);
	}
	void MBIAmAPIWrapper::sendFloatArray(const char* name, float* value)
	{
		if (name == nullptr || value == nullptr) {
			return;
		}

		m_MBIAmAPIBase->sendFloatArray(name, value);
	}
	void MBIAmAPIWrapper::sendDouble(const char* name, double value)
	{
		if (name == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendDouble(name, value);
	}
	void MBIAmAPIWrapper::sendDoubleArray(const char* name, double* value)
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendDoubleArray(name, value);
	}
	void MBIAmAPIWrapper::sendInt(const char* name, int value)
	{
		if (name == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendInt(name, value);
	}
	void MBIAmAPIWrapper::sendIntArray(const char* name, int* value)
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendIntArray(name, value);
	}
	void MBIAmAPIWrapper::sendString(const char* name, const char* value)
	{
		if (name == nullptr || value == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendString(name, value);
	}
	void MBIAmAPIWrapper::sendByte(const char* name, Byte value)//20220125-1,Zac
	{
		if (name == nullptr) {
			return;
		}
		m_MBIAmAPIBase->sendByte(name, value);
	}
}