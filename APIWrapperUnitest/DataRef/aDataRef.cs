﻿using AirManagerAPICallbackDefine;
using System;

namespace APIWrapperUnitest.DataRef
{
    public class aDataRef : DataRefBase
    {
        public IntVariableManagedCallback _callbackInt;
        public FloatVariableManagedCallback _callbackFloat;
        
        public aDataRef()
        {
            _callbackInt = intCallback;
            _callbackFloat = floatCallback;
        }

        private void intCallback(int value)
        {
            Console.WriteLine($"DataRef:: {DataRefName} , int value change -> " + value.ToString());
        }
        private void floatCallback(float value)
        {
            Console.WriteLine($"DataRef:: {DataRefName} , float value change -> " + value.ToString());
        }
        
    }
}
