﻿using AirManagerAPICallbackDefine;
using System;

namespace APIWrapperUnitest.DataRef
{
    public class bDataRef : DataRefBase
    {
        public StringVariableManagedCallback _callbackString;

        public bDataRef()
        {
            _callbackString = stringCallback;
        }

        private void stringCallback(string value)
        {
            Console.WriteLine($"DataRef:: {DataRefName} , string value change -> " + value.ToString());
        }

    }
}
