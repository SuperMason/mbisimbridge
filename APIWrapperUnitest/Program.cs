﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using AirManagerAPICallbackDefine;
using APIWrapperUnitest.DataRef;
using MBIAmAPI;
namespace APIWrapperUnitest
{
    class Program
    {
        enum variableType
        {
            floatType = 1,
            intType = 2,
            stringType = 3,
            doubleType = 4,
            byteType = 5,//air manager不支援，AM API支援，所以無法測試，但已實現底層
            intArrayType = 6,
            floatArrayType = 7,
            doubleArrayType = 8,
        }
        static MBIAmAPIWrapper temp = new MBIAmAPIWrapper();
        private static ManagedCallback _callback;
        private static ManagedCallback _callback1;
        private static ManagedCallback _callback2;
        private static IntVariableManagedCallback _callbackInt;
        //private static ByteVariableManagedCallback _callbackByte;
        //private static IntArrayVariableManagedCallback _callbackIntA;
        private static StringVariableManagedCallback _callbackString;
        private static FloatVariableManagedCallback _callbackFloat;
        private static StringVariableManagedCallback _callbackString1;
        private static FloatVariableManagedCallback _callbackFloat1;
        private static FloatArrayVariableManagedCallback _callbackFloatA;
        private static DoubleArrayVariableManagedCallback _callbackDoubleA;//20220516-1,Zac
        private static DoubleVariableManagedCallback _callbackDouble;
        //static DateTime beforDT = System.DateTime.Now;
        //static DateTime afterDT = System.DateTime.Now;
        private static Stopwatch sw = new Stopwatch();
        private static List<DataRefBase> dataRefs = null;
        private static List<aCommand> commands = null;

        private static string DATA_SOURCE_NAME = "MY_TEST_SOURCE";
        private static int DATA_SOURCE_UDP_PORT = 65000;

        static void Main(string[] args)
        {
            dataRefs = new List<DataRefBase>();
            int drList = 500;
            for (int z = 0; z < drList; z++)
            {
                dataRefs.Add(new aDataRef { DataRefName = "aDataRefInt_" + z });
                dataRefs.Add(new aDataRef { DataRefName = "aDataRefFloat_" + z });
                dataRefs.Add(new bDataRef { DataRefName = "aDataRefString_" + z });
            }

            commands = new List<aCommand>();
            commands.Add(new aCommand { Name = "sim/operation/quit" });
            for (int z = 0; z < drList; z++)
            { commands.Add(new aCommand { Name = "aCommand_" + z }); }
            
            _callback = MyCallback;
            _callback1 = MyCallback1;
            _callback2 = MyCallback2;
            _callbackString = stringCallback;
            _callbackInt = intCallback;
            _callbackFloat = floatCallback;
            _callbackString1 = stringCallback1;
            _callbackFloat1 = floatCallback1;
            _callbackDouble = doubleCallback;
            //_callbackByte = byteCallback;
            //MBIAmAPIWrapper temp = new MBIAmAPIWrapper();
            unsafe
            {
                //_callbackIntA = intCallbackA;
                _callbackFloatA = floatCallbackA;
                _callbackDoubleA = doubleCallbackA;//20220516-1,Zac
                temp.init(string2Sbyte(DATA_SOURCE_NAME), DATA_SOURCE_UDP_PORT);
                foreach (var dr in dataRefs) {
                    if (dr.GetType() == typeof(aDataRef) && dr.DataRefName.StartsWith("aDataRefInt"))
                    { temp.AddVariableProvider(string2Sbyte(dr.DataRefName), (int)variableType.intType, 0, ((aDataRef)dr)._callbackInt); }
                    else if (dr.GetType() == typeof(aDataRef) && dr.DataRefName.StartsWith("aDataRefFloat"))
                    { temp.AddVariableProvider(string2Sbyte(dr.DataRefName), (int)variableType.floatType, 0, ((aDataRef)dr)._callbackFloat); }
                    else
                    { temp.AddVariableProvider(string2Sbyte(dr.DataRefName), (int)variableType.stringType, string2Sbyte("Test"), ((bDataRef)dr)._callbackString); }
                }

                foreach (var cc in commands)
                {
                    if (cc.GetType() == typeof(aCommand))
                    { temp.AddCommandProvider(string2Sbyte(cc.Name), cc._callback); }
                }

                //temp.AddVariableProvider(string2Sbyte("myFloat"), (int)variableType.floatType, 0, _callbackFloat);
                //temp.AddVariableProvider(string2Sbyte("myStr"), (int)variableType.stringType, 0, _callbackString);
                //temp.AddVariableProvider(string2Sbyte("sim/flightmodel2/wing/elements/element_surface_area_mtr_sq"), (int)variableType.stringType, 0, _callbackString);
                //temp.AddVariableProvider(string2Sbyte("myFloat1"), (int)variableType.floatType, 0, _callbackFloat1);
                //temp.AddVariableProvider(string2Sbyte("myStr1"), (int)variableType.stringType, 0, _callbackString1);
                //temp.AddVariableProvider(string2Sbyte("myDouble"), (int)variableType.doubleType, 1.1, _callbackDouble);
                temp.AddVariableProvider(string2Sbyte("myInt"), (int)variableType.intType, 0, _callbackInt);
                //temp.AddVariableProvider(string2Sbyte("myByte"), (int)variableType.byteType, 0x01, _callbackByte);
                //fixed (int* test = new int[8] { 1,2,3,4,5,6,7,8}){temp.AddVariableProvider(string2Sbyte("myIntValueA"), (int)variableType.intArrayType, test, 8, _callbackIntA);}
                //fixed (float* test = new float[8] { 1.1f,2.2f,3.3f,4.4f,5.5f,6.6f,7.7f,8.8f}){temp.AddVariableProvider(string2Sbyte("myFloatValueA"), (int)variableType.floatArrayType, test, 8, _callbackFloatA);}
                fixed (double* test = new double[8] { 1.1f,2.2f,3.3f,4.4f,5.5f,6.6f,7.7f,8.8f}){temp.AddVariableProvider(string2Sbyte("myFloatValueA"), (int)variableType.doubleArrayType, test, 8, _callbackDoubleA); }//20220516-1,Zac
                //temp.AddCommandProvider(string2Sbyte("myCommandName"), _callback);
                //temp.AddCommandProvider(string2Sbyte("F1"), _callback1);
                //temp.AddCommandProvider(string2Sbyte("F2"), _callback2);

            }
            temp.start();
            Console.WriteLine();
            for (float i = 0; i < 10000; i++)
            {
                System.Threading.Thread.Sleep(1000);
                unsafe
                {
                    sw = new Stopwatch();
                    float a = i;
                    sbyte* b = string2Sbyte("myInt");
                    //sbyte* b = string2Sbyte("myFloatValueA");
                    sw.Start();
                    temp.sendInt(b, (int)i);
                    //temp.sendDouble(string2Sbyte("myDouble"), (double)i);
                    //fixed (int* test = new int[8] { (int)i, 2, 3, 4, 5, 6, 7, 8 }) temp.sendIntArray(b, test);
                    //fixed (float* test = new float[8] { (float)i, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f }) temp.sendFloatArray(b, test);
                    fixed (double* test = new double[8] { (double)i, 2.3f, 3.4f, 4.5f, 5.6f, 6.7f, 7.8f, 8.9f }) temp.sendDoubleArray(b, test);//20220516-1,Zac
                    if (i < drList) {
                        dataRefs.Where(o => o.DataRefName.EndsWith(string.Format("_{0}", i)))
                            .ToList().ForEach(dr => {
                                if (dr.GetType() == typeof(aDataRef) && dr.DataRefName.StartsWith("aDataRefInt"))
                                { temp.sendInt(string2Sbyte(dr.DataRefName), (int)i); }
                                else if (dr.GetType() == typeof(aDataRef) && dr.DataRefName.StartsWith("aDataRefFloat"))
                                { temp.sendFloat(string2Sbyte(dr.DataRefName), i / 12576); }
                                else
                                { temp.sendString(string2Sbyte(dr.DataRefName), string2Sbyte("Mason===" + i)); }
                            });
                    }
                    //temp.sendInt(b, (int)i);
                    temp.sendString(string2Sbyte("sim/flightmodel2/wing/elements/element_surface_area_mtr_sq"), string2Sbyte(""));
                    //temp.sendFloat(string2Sbyte("myFloatValue"), i / 100);
                    //temp.sendString(string2Sbyte("myStr"), string2Sbyte(i.ToString() + "************Zac"));
                }
                Console.WriteLine("send value:" + i.ToString());
            }
            Console.ReadLine();
            temp.close();
        }

        private static void MyCallback(int value, string message)
        {
            sw.Stop();
            TimeSpan ts2 = sw.Elapsed;
            Console.WriteLine("DateTime總共花費" + ts2.TotalMilliseconds + "ms");
            Console.WriteLine("0receive from callback -> " + message + ", value=" + value.ToString());
        }
        private static void MyCallback1(int value, string message)
        {
            Console.WriteLine("1receive from callback -> " + message + ", value=" + value.ToString());
        }
        private static void MyCallback2(int value, string message)
        {
            Console.WriteLine("2receive from callback -> " + message + ", value=" + value.ToString());
        }
        private static void intCallback(int value)
        {
            Console.WriteLine("1...int value change -> " + value.ToString());
        }
        private static void byteCallback(byte value)//20220125-1,Zac
        {
            Console.WriteLine("1...byteCallback value change -> " + value.ToString());
        }
        private unsafe static void intCallbackA(int* value,int length)
        {
            int[] list = new int[length];
            for (int i = 0; i < length; i++)
                list[i] = ((int*)value)[i];

            Console.WriteLine("1...int value change -> ");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        private unsafe static void floatCallbackA(float* value, int length)
        {
            float[] list = new float[length];
            for (int i = 0; i < length; i++)
                list[i] = ((float*)value)[i];

            Console.WriteLine("1...float value change -> ");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        private unsafe static void doubleCallbackA(double* value, int length)
        {
            double[] list = new double[length];
            for (int i = 0; i < length; i++)
                list[i] = ((double*)value)[i];

            Console.WriteLine("1...double value change -> ");
            foreach (var item in list)
            {
                Console.WriteLine(item.ToString());
            }
        }
        private static void floatCallback(float value)
        {
            Console.WriteLine("1...float value change -> " + value.ToString());
        }
        private static void doubleCallback(double value)
        {
            Console.WriteLine("1...double value change -> " + value.ToString());
        }
        private static void stringCallback(string value)
        {
            Console.WriteLine("1...string value change -> " + value.ToString());
        }
        private static void floatCallback1(float value)
        {
            Console.WriteLine("2...float value change -> " + value.ToString());
        }
        private static void stringCallback1(string value)
        {
            Console.WriteLine("2...string value change -> " + value.ToString());
        }
        private unsafe static sbyte* string2Sbyte(string input)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(input);


            unsafe
            {
                fixed (byte* p = bytes)
                {
                    sbyte* sp = (sbyte*)p;
                    return sp;
                }
            }
        }
    }
}
