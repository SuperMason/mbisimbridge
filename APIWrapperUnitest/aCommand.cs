﻿using AirManagerAPICallbackDefine;
using System;

namespace APIWrapperUnitest
{
    public class aCommand
    {
        public ManagedCallback _callback;
        public string Name { get; set; }

        public aCommand()
        {
            _callback = MyCallback;
        }

        private void MyCallback(int value, string message)
        {
            Console.WriteLine($"Command callback -> {message}, value={value}");
        }

    }
}
