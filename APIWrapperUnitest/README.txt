﻿---------------Air manager api for various data type---------------
static MBIAmAPIWrapper temp = new MBIAmAPIWrapper(); //建立API instance

////////////////////////Single value data type////////////////////////////
private static IntVariableManagedCallback _callbackInt;//建立function delegate instance，若沒建立instance會產生run time memory issue
_callbackInt = intCallback;//指向指定Function

private static void intCallback(int value)//若air manager有write value則event觸發
{
    Console.WriteLine("1...int value change -> " + value.ToString());
}

temp.AddVariableProvider(string2Sbyte("myInt"), (int)variableType.intType, 1.1, _callbackInt);//新增API listening provider

temp.sendInt(string2Sbyte("myInt"), (int)value);//從MBISimBridge寫值至air manager


////////////////////////Array data type////////////////////////////
private static IntArrayVariableManagedCallback _callbackIntA;//建立function delegate instance，若沒建立instance會產生run time memory issue
_callbackIntA = intCallbackA;//指向指定Function
unsafe //因為使用指標需要寫在unsafe裡
{
    _callbackIntA = intCallbackA;//指向指定Function
}

private unsafe static void intCallbackA(int* value,int length)//若air manager有write value則event觸發
{
    int[] list = new int[length];
    for (int i = 0; i < length; i++)
        list[i] = ((int*)value)[i];

    Console.WriteLine("1...int array value change -> ");
    foreach (var item in list)
    {
        Console.WriteLine(item.ToString());
    }
}

//新增API listening provider
//送至底層的array必須以fixed固定長度，否則array內容記憶體可能會被release掉
//參數需要設定dataref array長度，用於建立Tag
fixed (int* test = new int[8] { 1,2,3,4,5,6,7,8}){temp.AddVariableProvider(string2Sbyte("myIntValueA"), (int)variableType.intArrayType, test, 8, _callbackIntA);}

fixed (int* test = new int[8] { (int)i, 2, 3, 4, 5, 6, 7, 8 }) temp.sendIntArray(string2Sbyte("myIntValueA"), test);//從MBISimBridge寫值至air manager