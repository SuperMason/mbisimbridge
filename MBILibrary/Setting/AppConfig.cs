﻿using MBILibrary.Enums;
using MBILibrary.Util.CustomJsonConverter;
using Newtonsoft.Json;
using System;

namespace MBILibrary.Setting
{
    /// <summary>
    /// 紀錄所有系統參數設定值
    /// </summary>
    public class AppConfig
    {
        #region gRPC
        /// <summary>
        /// gRPC Server Name
        /// </summary>
        [JsonProperty("gRPCServerName")]
        public string gRPCServerName { get; set; }
        /// <summary>
        /// gRPC Server IP
        /// </summary>
        [JsonProperty("gRPCServerIP")]
        public string gRPCServerIP { get; set; }
        /// <summary>
        /// gRPC Server Port
        /// </summary>
        [JsonProperty("gRPCServerPort")]
        public int gRPCServerPort { get; set; }
        /// <summary>
        /// 啟動 gRPC
        /// <para>true : 啟動</para>
        /// <para>false : 不啟動(default)</para>
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("gRPCon")]
        public bool gRPCon { get; set; }
        #endregion


        #region X-Plane
        /// <summary>
        /// X-Plane IP
        /// </summary>
        [JsonProperty("XPlaneIP")]
        public string XPlaneIP { get; set; }
        /// <summary>
        /// X-Plane UDP Port
        /// </summary>
        [JsonProperty("XPlaneUDPPort")]
        public int XPlaneUDPPort { get; set; }
        #endregion


        #region Air Manager
        /// <summary>
        /// Air Manager Tag
        /// </summary>
        [JsonProperty("AirManagerTag")]
        public string AirManagerTag { get; set; }
        /// <summary>
        /// Air Manager IP
        /// </summary>
        [JsonProperty("AirManagerIP")]
        public string AirManagerIP { get; set; }
        /// <summary>
        /// Air Manager UDP Port
        /// </summary>
        [JsonProperty("AirManagerUDPPort")]
        public int AirManagerUDPPort { get; set; }
        #endregion

        /// <summary>
        /// 監控 X-Plane 是否活著, 每隔 0.01 秒檢查, 也同步更新 ALL DataRef's value
        /// <para>注意 !!!</para>
        /// <para>1. 與 "解除註冊", "等待", "再註冊" 都沒關係... 與 UtilConstants.appConfig.CheckXPlaneAliveMS 非常有關係... 太快 X-Plane 來不及更新資料.. 就馬上又把舊值 Response 出來</para>
        /// <para>2. 所以更新時... UtilConstants.appConfig.CheckXPlaneAliveMS 必須從 10 ms 改成 LetXPlaneUpdateDataRefValueWaitingMS</para>
        /// <para>3. 給 X-Plane 有時間更新, 確認更新完成後, 再改回 10 ms</para>
        /// </summary>
        [JsonProperty("CheckXPlaneAliveMS")]
        public double CheckXPlaneAliveMS { get; set; }

        /// <summary>
        /// 測試 AF 寫入 X-Plane 的頻率, 單位(ms), default(-1, 不啟動)
        /// </summary>
        [JsonProperty("Debug4SendDataAF2XP")]
        public double Debug4SendDataAF2XP { get; set; }

        /// <summary>
        /// 測試 AF 寫入 Air Manager 的頻率, 單位(ms), default(-1, 不啟動)
        /// </summary>
        [JsonProperty("Debug4SendDataAF2AM")]
        public double Debug4SendDataAF2AM { get; set; }

        /// <summary>
        /// 測試 GRPC 寫入 X-Plane 的頻率, 單位(ms), default(-1, 不啟動)
        /// </summary>
        [JsonProperty("Debug4SendDataGRPC2XP")]
        public double Debug4SendDataGRPC2XP { get; set; }

        /// <summary>
        /// 測試 GRPC 寫入 Air Manager 的頻率, 單位(ms), default(-1, 不啟動)
        /// </summary>
        [JsonProperty("Debug4SendDataGRPC2AM")]
        public double Debug4SendDataGRPC2AM { get; set; }

        /// <summary>
        /// 送給 Air Manager 時, 將 float, double 取至小數點後 6 位
        /// </summary>
        [JsonProperty("Send2AMDigitals")]
        public int Send2AMDigitals { get; set; }

        /// <summary>
        /// 不註冊至 X-Plane 的 DataRef
        /// <para>1. 以 || 區隔</para>
        /// <para>2. 每一個 Element 皆為 DataRef Name 的 開頭字串</para>
        /// </summary>
        [JsonProperty("SkipDataRefSubscribeToXP")]
        public string SkipDataRefSubscribeToXP { get; set; }

        /// <summary>
        /// 啟動測試 "SD Mason" Code
        /// <para>true : 啟動</para>
        /// <para>false : 不啟動(default)</para>
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("Debug4BigMason")]
        public bool Debug4BigMason { get; set; }

        /// <summary>
        /// 顯示 "SD Mason" Debug 訊息
        /// <para>true : 顯示</para>
        /// <para>false : 不顯示(default)</para>
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("ShowBigMasonDebugMessage")]
        public bool ShowBigMasonDebugMessage { get; set; }

        #region Other System Configuration
        /// <summary>
        /// 不比對 DataRef's value, 直接回覆給 Air Manager
        /// <para>true : 直接回覆</para>
        /// <para>false : 值有異動才回覆</para>
        /// <para>注意 !!!</para>
        /// <para>因為 Array dataref 並不是每次都全部更新所有的 Elements, </para>
        /// <para>所以如果沒變動的 Elements 沒有回覆的話,</para>
        /// <para>會造成無法收齊整批 array 值, 而無法正常回覆給 Air Manager,</para>
        /// <para>所以現在統一全部回覆 ::: 預設值(true)</para>
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("DoNotNeedCompareValue")]
        public bool DoNotNeedCompareValue { get; set; }
        /// <summary>
        /// 取得 X-Plane UDP streaming data 時, 當下是否馬上執行 parsing byte[] data ?
        /// <para>為了減少 CPU usage, 最好 sleep 1 ms, 可有效降低 CPU 使用率至 0.18, 假設馬上執行的使用率為 1</para>
        /// <para>true : 馬上處理。 平均處理 data 的速度約 1344 micro seconds, 但 CPU usage (avg.) 約 0.61</para>
        /// <para>false : sleep 1 ms 後再處理。 平均處理 data 的速度約 19227 micro seconds(與全速比較, 慢 14 倍左右), 但 CPU usage (avg.) 約 0.11(與全速比較, 少 6 倍左右)</para>
        /// </summary>
        [JsonConverter(typeof(BoolConverter))]
        [JsonProperty("CpuUsageFullSpeed")]
        public bool CpuUsageFullSpeed { get; set; }
        /// <summary>
        /// 當 CpuUsageFullSpeed 為 false 時, 控制 CPU sleep 0.01 ms
        /// </summary>
        [JsonProperty("CpuUsageWaitingMS")]
        public double CpuUsageWaitingMS { get; set; }
        /// <summary>
        /// 自動重啟時間
        /// </summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [JsonProperty("AutoRebootTime4Updating")]
        public DateTime AutoRebootTime4Updating { get; set; }
        /// <summary>
        /// 使用哪一種人臉辨識軟體 ?
        /// <para>TCIT</para>
        /// <para>OpenVino</para>
        /// <para>NoneFacial</para>
        /// </summary>
        [JsonConverter(typeof(EnumConverter))]
        [JsonProperty("FacialSoftware")]
        public FacialDetectService FacialSoftware { get; set; }
        #endregion

    }
}
