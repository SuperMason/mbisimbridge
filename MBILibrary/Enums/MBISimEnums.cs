﻿namespace MBILibrary.Enums
{
    public enum SizeUnits
    { Byte, KB, MB, GB, TB, PB, EB, ZB, YB }

    /// <summary>
    /// 使用的人臉識別軟體
    /// </summary>
    public enum FacialDetectService
    {
        /// <summary>
        /// 使用 TCIT
        /// </summary>
        TCIT,
        /// <summary>
        /// 使用 OpenVino
        /// </summary>
        OpenVino,
        /// <summary>
        /// 沒有使用任何人臉識別軟體
        /// </summary>
        NoneFacial
    }

    /// <summary>
    /// gRPC 當下的行為狀態
    /// <para>OPEN(0, 連線成功開啟)</para>
    /// <para>CLOSE(1, 連線關閉)</para>
    /// </summary>
    public enum GrpcStatusEnum
    {
        /// <summary>
        /// 連線成功開啟
        /// </summary>
        OPEN = 0,
        /// <summary>
        /// 連線關閉
        /// </summary>
        CLOSE = 1,
    }

    /// <summary>
    /// 方案平台
    /// <para>x86</para>
    /// <para>x64</para>
    /// </summary>
    public enum SolutionPlatformEnum
    {
        x86 = 0,
        x64 = 1,
    }

    /// <summary>
    /// 方案組態
    /// <para>Debug</para>
    /// <para>Release</para>
    /// </summary>
    public enum SolutionConfigurationEnum
    {
        Debug = 0,
        Release = 1,
    }

    /// <summary>
    /// MBISimBridge 當下的行為狀態
    /// <para>Done(0, 完整執行整個行為)</para>
    /// <para>Abandon(1, 當下忙碌中, 捨棄該次行為)</para>
    /// </summary>
    public enum SessionStatusEnum
    {
        /// <summary>
        /// 完整執行整個行為
        /// </summary>
        Done = 0,
        /// <summary>
        /// 當下忙碌中, 捨棄該次行為
        /// </summary>
        Abandon = 1,
    }

    /// <summary>
    /// Trigger By what system ?
    /// </summary>
    public enum TriggerByWhatSystem {
        /// <summary>
        /// 從 X-Plane Trigger 過來的 DataRef
        /// </summary>
        FromXPlane = 0,
        /// <summary>
        /// 從 Testing Function Trigger 過來的 DataRef
        /// </summary>
        FromTestFunc = 1,
        /// <summary>
        /// 從 AirForce System Trigger 過來的 DataRef
        /// </summary>
        FromAirForce = 2,
        /// <summary>
        /// 從 GRPC Trigger 過來的 DataRef
        /// </summary>
        FromGRPC = 3,
        /// <summary>
        /// initializing...
        /// </summary>
        FromInitial = 4,
        /// <summary>
        /// 從 Aircraft System Trigger 過來的 DataRef
        /// </summary>
        FromAircraftSys = 5,
        /// <summary>
        /// 從 Air Manager Trigger 過來的 DataRef
        /// </summary>
        FromAirManager = 6,
        /// <summary>
        /// 從 TestPattern Trigger 過來
        /// </summary>
        FromTestPattern = 7,
        /// <summary>
        /// 從 FPGA Trigger 過來
        /// </summary>
        FromFpga = 8,
        /// <summary>
        /// 從 PageBase Trigger 過來
        /// </summary>
        FromPageBase = 9,
        /// <summary>
        /// 從 EntryBase Trigger 過來
        /// </summary>
        FromEntryBase = 10,
        /// <summary>
        /// 從 ElectricalSystem Trigger 過來
        /// </summary>
        FromElectricalSystem = 11,
        /// <summary>
        /// 從 InstrumentPanel Trigger 過來
        /// </summary>
        FromInstrumentPanel = 12,
        /// <summary>
        /// 從 Component Trigger 過來
        /// </summary>
        FromComponent = 13,
    }

    /// <summary>
    /// 從哪邊 Add Queue Element DR ?
    /// </summary>
    public enum AddQueueElementSrcFrom
    {
        /// <summary>
        /// X-Plane 回覆回來的 Array DataRef 物件, 等 Array 到齊後, 再一併 Send to Air Manager
        /// </summary>
        FromXP = 0,
        /// <summary>
        /// 當 Queue 超過 QueueMaxMS 時, 不足的 Element 自動補 0 回覆
        /// </summary>
        FromAutoGen = 1,
    }

    /// <summary>
    /// Air Manager 的 VariableProvider 有 8 種
    /// <para>單一值 : float</para>
    /// <para>單一值 : int</para>
    /// <para>單一值 : string, 假設 string length 為 8, 系統會轉成 float[0] ~ float[7]</para>
    /// <para>單一值 : double</para>
    /// <para>單一值 : byte, Air Manager不支援，但 AM API 支援，所以無法測試，但已實現底層</para>
    /// <para>Array 值 : int</para>
    /// <para>Array 值 : float</para>
    /// <para>Array 值 : double</para>
    /// </summary>
    public enum variableType {
        /// <summary>
        /// 單一值 : float
        /// </summary>
        floatType = 1,
        /// <summary>
        /// 單一值 : int
        /// </summary>
        intType = 2,
        /// <summary>
        /// 單一值 : string
        /// <para>假設 string length 為 8, 系統會轉成 float[0] ~ float[7]</para>
        /// </summary>
        stringType = 3,
        /// <summary>
        /// 單一值 : double
        /// </summary>
        doubleType = 4,
        /// <summary>
        /// 單一值 : byte
        /// <para>Air Manager不支援，但 AM API 支援，所以無法測試，但已實現底層</para>
        /// </summary>
        byteType = 5,
        /// <summary>
        /// Array 值 : int
        /// </summary>
        intArrayType = 6,
        /// <summary>
        /// Array 值 : float
        /// </summary>
        floatArrayType = 7,
        /// <summary>
        /// Array 值 : double
        /// </summary>
        doubleArrayType = 8,
        /// <summary>
        /// Array 值 : byte
        /// </summary>
        byteArrayType = 9,
    }

    /// <summary>
    /// 將 'Air Manager' 或 'X-Plane' 的訊息, 透過 gRPC Client 傳送給 gRPC Server
    /// <para>AirManagerExecuteCommand(0, 從 'Air Manager' 取得 Command execute to 'X-Plane')</para>
    /// <para>AirManagerExecuteDataRefWrite(1, 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane')</para>
    /// <para>XPlaneConnected(2, X-Plane 已連線成功)</para>
    /// <para>XPlaneReadDataRef(3, 即時取得 X-Plane 的 DataRef 更新值)</para>
    /// </summary>
    public enum SendGrpcTypeEnum
    {
        /// <summary>
        /// 從 'Air Manager' 取得 Command execute to 'X-Plane'
        /// </summary>
        AirManagerExecuteCommand = 0,
        /// <summary>
        /// 從 'Air Manager' 取得 DataRef Value Write into 'X-Plane'
        /// </summary>
        AirManagerExecuteDataRefWrite = 1,
        /// <summary>
        /// X-Plane 已連線成功
        /// </summary>
        XPlaneConnected = 2,
        /// <summary>
        /// 即時取得 X-Plane 的 DataRef 更新值
        /// </summary>
        XPlaneReadDataRef = 3,
    }

    /// <summary>
    /// 是否有正確送達 gRPC Server ?
    /// </summary>
    public enum SendGrpcResultEnum
    {
        /// <summary>
        /// 成功
        /// </summary>
        Success = 0,
        /// <summary>
        /// 失敗
        /// </summary>
        Failure = 1,
    }

    /// <summary>
    /// gRPC Client / Server 之間, Send Message's Target
    /// <para>傳送此訊息的對象</para>
    /// </summary>
    public enum GrpcSendMessageTargetEnum
    {
        /// <summary>
        /// Server Broadcast To Client for checking alive
        /// </summary>
        ServerBroadcastCheckingClientAlive = 0,
        /// <summary>
        /// Server Broadcast To "Air Manager"
        /// </summary>
        ServerBroadcastToAirManager = 1,
        /// <summary>
        /// Server Broadcast To "X-Plane"
        /// </summary>
        ServerBroadcastToXPlane = 2,
        /// <summary>
        /// "gRPC Client" 端, 送 Message 給 "gRPC Server" 端
        /// </summary>
        ClientToServer = 3,
    }

    /// <summary>
    /// gRPC Server 下達的指令對象是什麼系統要承接 ?
    /// </summary>
    public enum CmdTargetSystemEnum
    {
        /// <summary>
        /// Air Manager
        /// </summary>
        AirManager = 0,
        /// <summary>
        /// X-Plane
        /// </summary>
        XPlane = 1,
    }

    /// <summary>
    /// Sets the specified window's show state. 控制 Console 視窗狀態, (ref. https://docs.microsoft.com/zh-tw/windows/win32/api/winuser/nf-winuser-showwindow?redirectedfrom=MSDN)
    /// </summary>
    public enum nCmdShowWindowEnum
    {
        /// <summary>
        /// Hides the window and activates another window.
        /// </summary>
        SW_HIDE = 0,
        /// <summary>
        /// Maximizes the specified window.
        /// </summary>
        SW_MAXIMIZE = 3,
        /// <summary>
        /// Activates the window and displays it in its current size and position.
        /// </summary>
        SW_SHOW = 5,
        /// <summary>
        /// Minimizes the specified window and activates the next top-level window in the Z order.
        /// </summary>
        SW_MINIMIZE = 6,
        /// <summary>
        /// Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when restoring a minimized window.
        /// </summary>
        SW_RESTORE = 9,
        /// <summary>
        /// Sets the show state based on the SW_ value specified in the STARTUPINFO structure passed to the CreateProcess function by the program that started the application.
        /// </summary>
        SW_SHOWDEFAULT = 10,
        /// <summary>
        /// Minimizes a window, even if the thread that owns the window is not responding. This flag should only be used when minimizing windows from a different thread.
        /// </summary>
        SW_FORCEMINIMIZE = 11,
    }

}