﻿namespace MBILibrary.Enums
{
    /// <summary>
    /// X-Plane 的連線狀態
    /// <para>DisConnected(0, 尚未取得連線)</para>
    /// <para>ConnectedInitialize(1, 已經可以與 X-Plane 通訊, 但尚未能取得 Subscribe data)</para>
    /// <para>ConnectedGetDataRefs(2, 已經可以與 X-Plane 通訊, 且能取得 DataRefs data)</para>
    /// </summary>
    public enum XPlaneConnStatusEnum
    {
        /// <summary>
        /// 尚未取得連線
        /// </summary>
        DisConnected = 0,
        /// <summary>
        /// 已經可以與 X-Plane 通訊, 但尚未能取得 Subscribe data
        /// </summary>
        ConnectedInitialize = 1,
        /// <summary>
        /// 已經可以與 X-Plane 通訊, 且能取得 DataRefs data
        /// </summary>
        ConnectedGetDataRefs = 2,
    }

    /// <summary>
    /// X-Plane 回覆 array index 的狀態
    /// <para>DataType List ::: string, int[], float[], double[]</para>
    /// <para>Fully(0, 全部的 array index 皆已回覆)</para>
    /// <para>Partial(1, 限制時間內, 尚有未回覆的 index, 所以先填預設值, 回覆給 Air Manager)</para>
    /// </summary>
    public enum XPlaneResponseStatusEnum
    {
        /// <summary>
        /// 全部的 array index 皆已回覆
        /// </summary>
        Fully = 0,
        /// <summary>
        /// 限制時間內, 尚有未回覆的 index, 所以先填預設值, 回覆給 Air Manager
        /// </summary>
        Partial = 1,
    }

    /// <summary>
    /// X-Plane 目前狀態
    /// <para>WaitingToConnect(0, 等待與 X-Plane 連線)</para>
    /// <para>BeClosed(1, X-Plane 被關閉)</para>
    /// </summary>
    public enum XPlaneCurrentStatusEnum
    {
        /// <summary>
        /// 等待與 X-Plane 連線
        /// </summary>
        WaitingToConnect = 0,
        /// <summary>
        /// X-Plane 被關閉
        /// </summary>
        BeClosed = 1,
        
    }
}