﻿using MBILibrary.Util;
using System;

namespace MBILibrary.Models
{
    /// <summary>
    /// 紀錄每一組 Queue DataRef 的耗時, 超過 QueueMaxMS 就強制補齊其他未到齊的 Elements
    /// <para>1. 因為 X-Plane 只會針對有異動的 Elements 回覆, 所以並不會一次全部的 Elements 都會 Trigger Response 行為</para>
    /// <para>2. 所以這邊自訂上限值, 超過 500 ms 沒有收集齊全所有的 Elements 時, 就會自動補齊其餘缺少的 Elements</para>
    /// <para>3. 自動補齊的 Element 值, 預設為 0</para>
    /// <para>4. 實測結果, 如果一個 int[8] 所有的 elements 的值都有異動, X-Plane 大約耗時 50 ms, 全部會 Response 回來</para>
    /// </summary>
    public class ArrayElementQueueTime
    {
        /// <summary>
        /// 注意 !!!
        /// <para>1. 因為 X-Plane 只會針對有異動的 Elements 回覆, 所以並不會一次全部的 Elements 都會 Trigger Response 行為</para>
        /// <para>2. 所以這邊自訂上限值, 超過 500 ms 沒有收集齊全所有的 Elements 時, 就會自動補齊其餘缺少的 Elements</para>
        /// <para>3. 自動補齊的 Element 值, 預設為 0</para>
        /// <para>4. 實測結果, 如果一個 int[8] 所有的 elements 的值都有異動, X-Plane 大約耗時 50 ms, 全部會 Response 回來</para>
        /// </summary>
        public const double QueueMaxMS = 500;

        /// <summary>
        /// Queue 住的 Raw DataRef
        /// </summary>
        public string RawDataRefName { get; private set; }
        /// <summary>
        /// 最後更新的 Element Index
        /// <para>依據 X-Plane 回覆的順序而定, 所以 Index 順序不固定</para>
        /// </summary>
        public int LatestIndex { get; private set; }
        /// <summary>
        /// 最後更新的 Element Value
        /// <para>依據 X-Plane 回覆的順序而定, 所以 Index 順序不固定</para>
        /// </summary>
        public float LatestValue { get; private set; }
        /// <summary>
        /// 開始 Queue 的時間點
        /// </summary>
        public DateTime QueueStartDT { get; private set; }
        /// <summary>
        /// 從 Queue 發生的開始時間點 ~ Now(), 已經耗時 ? ms 了
        /// </summary>
        public double SpendingMS { get { return UtilSysTimes.GetSpendingMilliseconds(QueueStartDT); } }
        /// <summary>
        /// 是否已經超過等待的上限值 QueueMaxMS 500 ms
        /// </summary>
        public bool IsOverMaxMS { get { return SpendingMS > QueueMaxMS; } }

        /// <summary>
        /// 紀錄每一組 Queue DataRef 的耗時, 超過 QueueMaxMS 就強制補齊其他未到齊的 Elements
        /// <para>1. 因為 X-Plane 只會針對有異動的 Elements 回覆, 所以並不會一次全部的 Elements 都會 Trigger Response 行為</para>
        /// <para>2. 所以這邊自訂上限值, 超過 500 ms 沒有收集齊全所有的 Elements 時, 就會自動補齊其餘缺少的 Elements</para>
        /// <para>3. 自動補齊的 Element 值, 預設為 0</para>
        /// <para>4. 實測結果, 如果一個 int[8] 所有的 elements 的值都有異動, X-Plane 大約耗時 50 ms, 全部會 Response 回來</para>
        /// </summary>
        public ArrayElementQueueTime(string rawDataRef, float value, int arrayIdx) {
            RawDataRefName = rawDataRef;
            UpdateQueueStartDT(value, arrayIdx);
        }

        /// <summary>
        /// <para>因為 X-Plane 的回覆狀態不固定, 可能會持續一段時間都沒回覆, 之後再回覆</para>
        /// <para>所以如果持續有新的 Element 進來, 就重新設定起始的 QueueStartDT</para>
        /// <para>如果超過一段時間都沒動作, 才開始補齊</para>
        /// </summary>
        public void UpdateQueueStartDT(float value, int arrayIdx) {
            LatestValue = value;
            LatestIndex = arrayIdx;
            QueueStartDT = DateTime.Now;
        }
    }
}
