﻿using Newtonsoft.Json;

namespace MBILibrary.Models
{
    /// <summary>
    /// gRPC Exception Information
    /// </summary>
    public class RpcExceptionModel
    {
        /// <summary>
        /// Exception Created
        /// </summary>
        [JsonProperty("created")]
        public string Created { get; set; }

        /// <summary>
        /// Exception Description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Exception occur file
        /// </summary>
        [JsonProperty("file")]
        public string OccurFile { get; set; }

        /// <summary>
        /// Exception occur file code line
        /// </summary>
        [JsonProperty("file_line")]
        public int OccurFileCodeLine { get; set; }

        /// <summary>
        /// gRPC Message
        /// </summary>
        [JsonProperty("grpc_message")]
        public string GrpcMessage { get; set; }

        /// <summary>
        /// gRPC Status
        /// </summary>
        [JsonProperty("grpc_status")]
        public int GrpcStatus { get; set; }
    }
}
