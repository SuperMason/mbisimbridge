﻿using MBILibrary.Util;
using System;
using System.IO;
using System.Reflection;

namespace MBILibrary.Models
{
    /// <summary>
    /// 取得版本資訊
    /// </summary>
    public class MBISimVersion
    {
        private const string VerStringFormat = "{0}.{1}.{2}.{3}-{4}";

        private int mMajor = 0;
        private int mMinor = 0;
        private int mBuild = 0;
        private int mRevision = 0;
        private string mDate = string.Empty;

        /// <summary>
        /// 取得版本資訊
        /// </summary>
        public MBISimVersion() {
            Version ver = GetAssemblyInfo;
            mMajor = ver.Major;
            mMinor = ver.Minor;
            mBuild = ver.Build;
            mRevision = ver.Revision;
            mDate = GetCreateDate().ToString(UtilDateTime.Format_Default);
        }

        /// <summary>
        /// 取得版本資訊
        /// </summary>
        public string SimVersion => string.Format(VerStringFormat, mMajor, mMinor, mBuild, mRevision, mDate);

        /// <summary>
        /// 取得組件的版本資訊
        /// </summary>
        private Version GetAssemblyInfo => GetCurrentAssembly.GetName().Version;

        /// <summary>
        /// 取得執行入口的組件
        /// </summary>
        private Assembly GetCurrentAssembly => Assembly.GetEntryAssembly();

        /// <summary>
        /// 取得目前組件的 "最後更新時間"
        /// </summary>
        private DateTime GetLatestModifiedDate() {
            var fileInfo = new FileInfo(GetCurrentAssembly.Location);
            return fileInfo.Exists ? fileInfo.LastWriteTime : DateTime.Now;
        }
        /// <summary>
        /// 取得目前組件的 "建立時間"
        /// </summary>
        private DateTime GetCreateDate() {
            var fileInfo = new FileInfo(GetCurrentAssembly.Location);
            return fileInfo.Exists ? fileInfo.CreationTime : DateTime.Now;
        }
    }
}
