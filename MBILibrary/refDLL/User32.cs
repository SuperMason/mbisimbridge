﻿using System;
using System.Runtime.InteropServices;

namespace MBILibrary.refDLL
{
    public sealed class User32
    {
        private User32() { }
        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;

        public const int WM_CHAR = 0x0102;
        public const int VK_RETURN = 0x0D;
        public const int VK_ENTER = 0x0D;

        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow([In] IntPtr hWnd, [In] int nCmdShow);

        [DllImport("User32.dll", EntryPoint = "PostMessageA")]
        public static extern bool PostMessage(IntPtr hWnd, uint msg, int wParam, int lParam);
    }
}
