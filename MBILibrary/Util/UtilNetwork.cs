﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MBILibrary.Util
{
    /// <summary>
    /// 檢查網路狀態, 軟、硬體是否皆正常
    /// </summary>
    public static class UtilNetwork
    {
        #region Properties
        private static bool LogOneTime = true;

        /// <summary>
        /// 每 10 秒檢查一次網路狀況
        /// </summary>
        private const double CheckNetworkFrequencyMS = 10000;
        /// <summary>
        /// 紀錄每次檢查的當下時間
        /// </summary>
        private static DateTime CheckDT = DateTime.Now;
        /// <summary>
        /// iPlayer 剛啟動的檢查
        /// </summary>
        private static bool InitCheck = true;
        /// <summary>
        /// 當下網路狀態, 預設網路是暢通的
        /// <para>true : 網路暢通</para>
        /// <para>false: 網路有問題</para>
        /// </summary>
        private static bool CurrentNetworkAlive = true;


        #region 開啟 iPlayer 後, 紀錄每一次的 RoundtripTime, 並寫到 log file 備查
        /// <summary>
        /// 紀錄開啟 iPlayer 後, 最慢的 RoundtripTime
        /// </summary>
        public static long RoundtripTimeMax = -1;
        /// <summary>
        /// 紀錄開啟 iPlayer 後, 最快的 RoundtripTime
        /// </summary>
        public static long RoundtripTimeMin = 999;
        /// <summary>
        /// 紀錄開啟 iPlayer 後, 平均的 RoundtripTime
        /// </summary>
        public static long RoundtripTimeAvg = 0;
        /// <summary>
        /// 控制 該小時的第 1 秒可以執行 1 次, 其餘秒數則忽略不理
        /// <para>控制每小時只會執行 1 次</para>
        /// </summary>
        private static int HourExeOnceFlag = -1;
        #endregion

        #region 紀錄 Ping 後的結果
        /// <summary>
        /// Ping 的結果狀態
        /// </summary>
        private static string PingReplyStatus = string.Empty;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ?
        /// </summary>
        private static string PingSuccessAddressTarget = string.Empty;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ? 成功後的來回時間 (以毫秒 ms 為單位)
        /// </summary>
        private static long PingSuccessRoundtripTime = 0;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ? 成功後的 TTL
        /// <para>TTL 會定義節點在其來源和目的地之間移動時，可轉送封包的次數。 如果轉送的數目（也稱為躍點）超過針對 TTL 指定的值，封包會被視為無法傳遞，而且會被捨棄。</para>
        /// </summary>
        private static int PingSuccessTimeToLive = 0;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ? 成功後的 "封包傳送資料" 是否可以切割
        /// <para>如果無法以多個封包傳送資料則為 true，否則為 false。 預設為 false。</para>
        /// </summary>
        private static bool PingSuccessDoNotFragment = false;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ? 成功後的傳送的資料緩衝區
        /// <para>使用 ICMP echo 要求傳送的資料緩衝區會傳回到 echo 回復的寄件者，因此可以計算已知大小封包的往返行進時間。 結合選項的資料緩衝區 DontFragment 可以用來探索來源與目的地電腦之間網路路徑的最大傳輸單位。</para>
        /// </summary>
        private static int PingSuccessBufferSize = 0;
        /// <summary>
        /// Ping 哪一個 Server 成功的 ? 成功後的傳送的資料緩衝區
        /// <para>使用 ICMP echo 要求傳送的資料緩衝區會傳回到 echo 回復的寄件者，因此可以計算已知大小封包的往返行進時間。 結合選項的資料緩衝區 DontFragment 可以用來探索來源與目的地電腦之間網路路徑的最大傳輸單位。</para>
        /// </summary>
        private static string PingSuccessBufferString = string.Empty;
        #endregion

        #region 檢查網路通訊的目標對象
        /// <summary>
        /// 測試網路連線是否通暢的對象 :: 有 9 個
        /// </summary>
        private static readonly List<string> PingTargetIpList = new List<string>() {
            "168.95.1.1" // Hinet 中華電信 DNS Server 1, ping回應平均速度: 21 ~ 22 ms
            , "168.95.192.1" // Hinet 中華電信 DNS Server 2, ping回應平均速度: 18 ~ 20 ms
            , "168.95.192.2" // Hinet 中華電信 DNS Server 3, ping回應平均速度: 18 ~ 19 ms (偶而會有爆ping到38以上的不穩定現象...)
            , "8.8.8.8" // Google 的 DNS Server 1, ping回應平均速度: 20 ~ 21 ms
            , "8.8.4.4" // Google 的 DNS Server 2, ping回應平均速度: 20 ~ 21 ms
            , "aws.amazon.com" // AWS
            , "www.mblock.com.tw" // MacroBlock
            , "www.mbi-iot.com" // AWS CMS Server
            , "www.google.com" // Google
        };

        /// <summary>
        /// 測試網路連線是否通暢的對象 : Google
        /// </summary>
        private const string UdpTestTargetIP = "www.google.com";
        #endregion

        public const string HardwareErrorMessage = "本機網路環境異常, 偵測不到 [網路介面卡] 可供使用";
        public const string SoftwareErrorMessage = "本機網路環境異常, [網路封包] 無法出去外部網路";

        /// <summary>
        /// 本機網路使用 Others(非 Ethernet, Wi-Fi, Mobile)
        /// </summary>
        private const string NetworkType_Others = "Others";
        /// <summary>
        /// 本機網路使用 Mobile
        /// </summary>
        private const string NetworkType_Mobile = "Mobile";
        /// <summary>
        /// 本機網路使用 Ethernet
        /// </summary>
        private const string NetworkType_Ethernet = "Ethernet";
        /// <summary>
        /// 本機沒有任何網路介面卡
        /// </summary>
        public const string NetworkType_Error = "NoNetworkInterfaceActive";
        /// <summary>
        /// Watch Dog 檢查的時間頻率, Default: 10 微毫秒 (單位: Milliseconds)
        /// </summary>
        private const double WatchDogRetryTimeIntervalMilliseconds = 10;
        /// <summary>
        /// Create a buffer of 32 bytes of data to be transmitted.
        /// </summary>
        private static readonly byte[] _PingBuffer = Encoding.ASCII.GetBytes("PingTesting");
        /// <summary>
        /// Set options for transmission:
        /// The data can go through 64 gateways or routers
        /// before it is destroyed, and the data packet
        /// cannot be fragmented.
        /// </summary>
        private static readonly PingOptions _PingOptions = new PingOptions(64, true);
        #endregion


        #region 檢查網路狀態, 軟、硬體是否皆正常
        /// <summary>
        /// <para>檢查網路狀態, 軟、硬體是否皆正常</para>
        /// <para>true : 網路暢通</para>
        /// <para>false: 網路有問題</para>
        /// </summary>
        public static bool CheckNetworkStatus() {
            bool CheckingNetworkGo = false; // 當下是否啟動檢查 ?

            if (InitCheck) {
                InitCheck = false;
                //UtilGlobalFunc.Log("網路狀態 :: 剛啟動...必須馬上檢查...");
                CheckingNetworkGo = true;
            }

            if (UtilSysTimes.GetSpendingMilliseconds(CheckDT) >= CheckNetworkFrequencyMS) {
                CheckingNetworkGo = true;
                CheckDT = DateTime.Now;
            }

            if (CheckingNetworkGo) {
                //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "網路狀態 :: 每超過上一次檢查後的 {0} ms, 再檢查一次", CheckNetworkFrequencyMS));
                CurrentNetworkAlive = true; // 檢查網路前, 預設網路是暢通的

                var ErrMsg = new List<string>();
                //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "每 {0} 微毫秒,檢查本機硬體 [網路介面卡].", WatchDogRetryTimeIntervalMilliseconds));
                while (ActiveNetworkInterface().Equals(NetworkType_Error)) {
                    ErrMsg.Add(HardwareErrorMessage);
                    SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(WatchDogRetryTimeIntervalMilliseconds));
                    CurrentNetworkAlive = false;
                    break;
                } // 確認硬體 :: 介面卡

                //if (!res) { return res; } // 硬體不通
                if (CurrentNetworkAlive) {
                    //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "每 {0} 微毫秒,檢查本機軟體 [網路封包].", WatchDogRetryTimeIntervalMilliseconds));
                    while (!PingServerAlive()) {
                        ErrMsg.Add(SoftwareErrorMessage);
                        SpinWait.SpinUntil(() => false, TimeSpan.FromMilliseconds(WatchDogRetryTimeIntervalMilliseconds));
                        CurrentNetworkAlive = false;
                        break;
                    } // 確認軟體 :: 網路封包
                }

                if (ErrMsg != null && ErrMsg.Count != 0) {
                    string msg = string.Join(Environment.NewLine, ErrMsg);
                    UtilGlobalFunc.Log(msg);
                }
            } else {
                //UtilGlobalFunc.Log("網路狀態 :: 沿用上一次的結果");
            }

            //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "當下網路狀態 ::: {0}", CurrentNetworkAlive));
            return CurrentNetworkAlive;
        }
        /// <summary>
        /// 硬體 ::: 取得正在運作的硬體網路介面卡(Network Interface) : Ethernet / Wi-Fi / Mobile / Others / NoNetworkInterfaceActive, 5 擇 1
        /// </summary>
        public static string ActiveNetworkInterface() {
            try {
                NetworkInterface ActiveAdapter = null;
                var LocalAddr = ((IPEndPoint)new UdpClient(UdpTestTargetIP, 1).Client.LocalEndPoint).Address;
                foreach (var adapter in NetworkInterface.GetAllNetworkInterfaces().Where(a => a.OperationalStatus == OperationalStatus.Up)) {
                    foreach (var AddrInfo in adapter.GetIPProperties().UnicastAddresses) {
                        if (LocalAddr.Equals(AddrInfo.Address)) // check if localAddr is in ipProps.UnicastAddresses
                        {
                            //Console.WriteLine("\nDescription: {0} \nId: {1} \nIsReceiveOnly: {2} \nName: {3} \nNetworkInterfaceType: {4} " +
                            //                  "\nOperationalStatus: {5} " +
                            //                  "\nSpeed (bits per second): {6} " +
                            //                  "\nSpeed (kilobits per second): {7} " +
                            //                  "\nSpeed (megabits per second): {8} " +
                            //                  "\nSpeed (gigabits per second): {9} " +
                            //                  "\nSupportsMulticast: {10}",
                            //                    adapter.Description, adapter.Id, adapter.IsReceiveOnly, adapter.Name, adapter.NetworkInterfaceType,
                            //                    adapter.OperationalStatus,
                            //                    adapter.Speed,
                            //                    adapter.Speed / 1000,
                            //                    adapter.Speed / 1000 / 1000,
                            //                    adapter.Speed / 1000 / 1000 / 1000,
                            //                    adapter.SupportsMulticast);
                            //var ipv4Info = adapter.GetIPv4Statistics();
                            //Console.WriteLine("OutputQueueLength: {0}", ipv4Info.OutputQueueLength);
                            //Console.WriteLine("BytesReceived: {0}", ipv4Info.BytesReceived);
                            //Console.WriteLine("BytesSent: {0}", ipv4Info.BytesSent);
                            //Console.WriteLine("*** {0} Network - Speed (bits per seconde): {1}", adapter.NetworkInterfaceType, adapter.Speed);
                            ActiveAdapter = adapter;
                        }
                    }
                }

                UtilConstants.NetworkType = (ActiveAdapter == null) ? NetworkType_Error :
                       (ActiveAdapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet) ? NetworkType_Ethernet :
                       (ActiveAdapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) ? ActiveAdapter.Name : // 本機網路使用 Wi-Fi
                       (ActiveAdapter.NetworkInterfaceType == NetworkInterfaceType.Wwanpp) ? NetworkType_Mobile : NetworkType_Others;

                if (LogOneTime) {
                    //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Current using NetworkType ::: {0}, {1}, {2}"
                    //    , UtilConstants.NetworkType, ActiveAdapter.Description, ActiveAdapter.NetworkInterfaceType));
                    LogOneTime = false; // 只記錄一次
                }

                return UtilConstants.NetworkType;
            } catch { return NetworkType_Error; }
        }

        /// <summary>
        /// 軟體 ::: 檢查封包是否真的可以出去
        /// </summary>
        public static bool PingServerAlive() {
            bool res = false;
            string Target = string.Empty;

            try {
                foreach (var _IP in PingTargetIpList) {
                    Target = _IP;
                    res = PingTarget(Target);
                    if (res) { break; }
                }
            } catch (Exception e) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "PingServerAlive() : Can NOT ping to '{0}', {1}", Target, e.Message)); }

            if (!res) {
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "PingServerAlive() ::: ping ALL Fail, please check Network. All Targets ({0}).", string.Join(", ", PingTargetIpList)));
            } else {
                //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "PingServerAlive() ::: ping '{0}' {1}.", Target, res));
            }

            return res;
        }
        /// <summary>
        /// 實際執行 Ping
        /// </summary>
        public static bool PingTarget(string target) {
            try {
                if (string.IsNullOrEmpty(target) || string.IsNullOrWhiteSpace(target)) {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Ping target '{0}' is null or empty.", target));
                    return false;
                }

                PingSuccessAddressTarget = target;
                using (var PingSender = new Ping()) {
                    var pingReply = PingSender.Send(target, UtilGlobalFunc.GetPingTimeout, _PingBuffer, _PingOptions); // Send the request.
                    bool res = pingReply != null && pingReply.Status == IPStatus.Success;
                    if (res) { // 紀錄 Ping 成功的資訊
                        PingReplyStatus = string.Format(CultureInfo.CurrentCulture, "{0}", pingReply.Status);
                        PingSuccessAddressTarget = string.Format(CultureInfo.CurrentCulture, "{0}", pingReply.Address);
                        PingSuccessRoundtripTime = pingReply.RoundtripTime;
                        PingSuccessTimeToLive = pingReply.Options.Ttl;
                        PingSuccessDoNotFragment = pingReply.Options.DontFragment;
                        PingSuccessBufferSize = pingReply.Buffer.Length;
                        PingSuccessBufferString = Encoding.UTF8.GetString(pingReply.Buffer);

                        #region 開啟 iPlayer 後, 紀錄每一次的 RoundtripTime, 並寫到 log file 備查
                        if (PingSuccessRoundtripTime > RoundtripTimeMax) { RoundtripTimeMax = PingSuccessRoundtripTime; } // 紀錄開啟 iPlayer 後, 最慢的 RoundtripTime
                        if (PingSuccessRoundtripTime < RoundtripTimeMin) { RoundtripTimeMin = PingSuccessRoundtripTime; } // 紀錄開啟 iPlayer 後, 最快的 RoundtripTime
                        if (RoundtripTimeAvg == 0)
                        { RoundtripTimeAvg = PingSuccessRoundtripTime; } // 第 1 次,  // 紀錄開啟 iPlayer 後, 平均的 RoundtripTime
                        else
                        { RoundtripTimeAvg = (RoundtripTimeAvg + PingSuccessRoundtripTime) / 2; } // 第 2 次以後, 取平均

                        // 控制每小時只會執行一次
                        if (HourExeOnceFlag != DateTime.Now.Hour) {
                            //string LogData = string.Format(CultureInfo.CurrentCulture, "Max({0}), Min({1}), Avg({2})", RoundtripTimeMax, RoundtripTimeMin, RoundtripTimeAvg);
                            //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Network RoundtripTime ::: {0}", LogData));
                        }
                        HourExeOnceFlag = DateTime.Now.Hour;
                        #endregion

                        //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "Ping '{0}' alive {1}. Detail Message :: RoundtripTime {2} ms, TimeToLive({3}), DoNotFragment({4}), BufferSize({5}), BufferString({6})"
                        //, PingSuccessAddressTarget, PingReplyStatus, PingSuccessRoundtripTime, PingSuccessTimeToLive, PingSuccessDoNotFragment, PingSuccessBufferSize, PingSuccessBufferString));
                    } else {
                        if (pingReply != null && pingReply.Status != IPStatus.TimedOut) // TimedOut 錯誤可以忽略不計, 因為有 6 個 ping 對象, 所以某一個成功即可, 其他所有的錯誤都會記錄下來
                        { PingReplyStatus = string.Format(CultureInfo.CurrentCulture, "Ping '{0}' 失敗, ReplyStatus : {1}", target, pingReply.Status); }
                        else
                        { PingReplyStatus = string.Format(CultureInfo.CurrentCulture, "ping '{0}' 失敗, pingReply is null :: '{1}'", target, pingReply); }
                        UtilGlobalFunc.Log(PingReplyStatus);
                    }

                    return res;
                }
            } catch (Exception e) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "PingTarget() : Can NOT ping to '{0}', {1}", target, e.Message)); return false; }
        }
        #endregion

    }
}
