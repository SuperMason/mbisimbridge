﻿using MBILibrary.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace MBILibrary.Util
{
    public sealed class UtilGenTestingData
    {
        private UtilGenTestingData() { }
        private static Random rnd = null;
        private static List<int> Listq = null;
        /// <summary>
        /// 隨機 seed 值, 共 1000 個, 隨機亂序排列
        /// </summary>
        private const int RandomSeedMaxRange = 1000;

        static UtilGenTestingData() {
            ResetRandom();
            Listq = new List<int>(Enumerable.Range(1, RandomSeedMaxRange));
        }

        /// <summary>
        /// 更新 Random 物件
        /// </summary>
        private static void ResetRandom() => rnd = new Random(Guid.NewGuid().GetHashCode());
        /// <summary>
        /// 隨機運算結果
        /// </summary>
        private static int GetRandomValue(int min = 1, int max = RandomSeedMaxRange) => (rnd.Next(min, max) + rnd.Next(min, max)) * rnd.Next(min, max);

        /// <summary>
        /// 依數量需求, 取得 Seed Array 值
        /// <para>隨機 seed 值, 共 1000 個, 隨機亂序排列</para>
        /// </summary>
        private static int[] GetRandomSeed(int cnt) {
            ResetRandom();
            Listq = Listq.OrderBy(num => rnd.Next()).ToList(); // 隨機亂序排列
            return Listq.Take(cnt).ToArray();
        }

        /// <summary>
        /// 取得測試用的 UserModels
        /// </summary>
        public static List<UserModel> GetUserModels() { return new List<UserModel> { GetUserModel(), GetUserModel(), GetUserModel(), }; }
        /// <summary>
        /// 取得測試用的 UserModel
        /// </summary>
        public static UserModel GetUserModel() {
            int age = GetRandomInt;
            return new UserModel { Age = age, Name = string.Format(CultureInfo.CurrentCulture, "Tester{0}", age), Id = Guid.NewGuid() };
        }

        #region Get Random 'int', 'float', 'double', 'string'
        /// <summary>
        /// Get Random :: 'bool'
        /// </summary>
        public static bool GetRandomBool => GetRandomInt % 2 == 1;
        /// <summary>
        /// Get Random :: 'int'
        /// </summary>
        public static int GetRandomInt {
            get {
                var ii = GetRandomIntArray(1);
                return !UtilGlobalFunc.IsArrayEmpty(ii) ? ii[0] : DateTime.Now.Millisecond;
            }
        }
        /// <summary>
        /// Get Random :: 'float'
        /// </summary>
        public static float GetRandomFloat {
            get {
                var ff = GetRandomFloatArray(1);
                return !UtilGlobalFunc.IsArrayEmpty(ff) ? ff[0] : (float)(DateTime.Now.Millisecond / (float)17);
            }
        }
        /// <summary>
        /// Get Random :: 'double'
        /// </summary>
        public static double GetRandomDouble {
            get {
                var dd = GetRandomDoubleArray(1);
                return !UtilGlobalFunc.IsArrayEmpty(dd) ? dd[0] : (double)(DateTime.Now.Millisecond / (double)34);
            }
        }
        /// <summary>
        /// Get Random :: 'string'
        /// </summary>
        public static string GetRandomString => string.Format(CultureInfo.CurrentCulture, "{0}_{1}_{2}", DateTime.Now.Ticks, GetRandomInt, "***Mason***");
        #endregion


        #region Get Random int[], float[], double[]
        /// <summary>
        /// Get Random :: int[]
        /// </summary>
        public static int[] GetRandomIntArray(int cnt) => GetRandomSeed(cnt);
        /// <summary>
        /// Get Random :: float[]
        /// </summary>
        public static float[] GetRandomFloatArray(int cnt, int min = 1, int max = RandomSeedMaxRange) {
            ResetRandom();
            var RndNumAry = new float[cnt];
            for (int i = 0; i < cnt; i++)
            { RndNumAry[i] = GetRandomValue(min, max) / Convert.ToSingle(rnd.Next(min, max)); }
            return RndNumAry;
        }
        /// <summary>
        /// Get Random :: double[]
        /// </summary>
        public static double[] GetRandomDoubleArray(int cnt, int min = 1, int max = RandomSeedMaxRange) {
            ResetRandom();
            var RndNumAry = new double[cnt];
            for (int i = 0; i < cnt; i++)
            { RndNumAry[i] = GetRandomValue(min, max) / Convert.ToDouble(rnd.Next(min, max)); }
            return RndNumAry;
        }
        #endregion
    }
}
