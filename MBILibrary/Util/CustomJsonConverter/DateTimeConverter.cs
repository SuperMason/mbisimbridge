﻿using Newtonsoft.Json;
using System;

namespace MBILibrary.Util.CustomJsonConverter
{
    /// <summary>
    /// <para>個別針對屬性值為 DateTime 的欄位進行值的轉換</para>
    /// <para>只有客製化 WriteJson (寫出)</para>
    /// <para>ReadJson (讀取) 採用原本預設的</para>
    /// </summary>
    public class DateTimeConverter : JsonConverter
    {
        // 需要客製化 WriteJson, CanWrite "return true"
        public override bool CanWrite { get { return true; } }
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            string OutputString = string.Empty;
            if (writer.Path.Equals("AutoRebootTime4Updating"))
            { OutputString = ((DateTime)value).ToString(UtilDateTime.Format_HHmmss); }
            else if (writer.Path.Equals("mAnalysisTime"))
            { OutputString = ((DateTime)value).ToString(UtilDateTime.Format_Default); }
            else
            { OutputString = ((DateTime)value).ToString(UtilDateTime.Format_Default); }

            writer.WriteValue(OutputString);
        }

        // 不需要客製化實作 ReadJson, 直接用預設的, CanRead "return false"
        public override bool CanRead { get { return false; } }
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTime);
        }
    }
}
