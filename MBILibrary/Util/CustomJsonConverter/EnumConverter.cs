﻿using Newtonsoft.Json;
using System;

namespace MBILibrary.Util.CustomJsonConverter
{
    /// <summary>
    /// 針對 "列舉型別" 與 "int" 的轉換
    /// </summary>
    public class EnumConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue((int)value);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            Enum value = (Enum)Enum.ToObject(objectType, reader.Value);
            return Enum.IsDefined(objectType, value) ? value : null; // null 時, 會以 Enum index 0 為主
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType.IsEnum ? true : false;
        }

    }
}
