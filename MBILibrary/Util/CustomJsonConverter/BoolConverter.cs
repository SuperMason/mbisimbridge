﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MBILibrary.Util.CustomJsonConverter
{
    /// <summary>
    /// <para>個別針對屬性值為 Bool 的欄位進行 值的轉換</para>
    /// <para>"1", "true", "y", "yes" => 1</para>
    /// <para>others string => 0</para>
    /// </summary>
    public class BoolConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            writer.WriteValue(((bool)value) ? 1 : 0);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var trueResult = new List<string>() { "1", "true", "y", "yes" }; // 正面表列 : 只有這 4 種值是 true, 其餘皆為 false
            return trueResult.ToArray().Any(s => s.Equals(reader.Value.ToString(), StringComparison.OrdinalIgnoreCase));
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(bool);
        }
    }
}
