﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;

namespace MBILibrary.Util
{
    /// <summary>
    /// 控制 Thread 的 Priority
    /// </summary>
    public sealed class PriorityScheduler : TaskScheduler //, IDisposable 不需要特別清除它, 因為會造成 _tasks.GetConsumingEnumerable() 即時讀取失敗, 所以就不理了, 讓它自行回收
    {
        #region Class Level Properties (希望當下執行 Thread 的 Priority 位階為何 ?)
        /// <summary>
        /// 當下所有 Threads 中, 最先執行
        /// </summary>
        public static PriorityScheduler Highest = new PriorityScheduler(ThreadPriority.Highest);
        /// <summary>
        /// 當下所有 Threads 中, 高於平常
        /// </summary>
        public static PriorityScheduler AboveNormal = new PriorityScheduler(ThreadPriority.AboveNormal);
        /// <summary>
        /// 當下所有 Threads 中, 平常
        /// </summary>
        public static PriorityScheduler Normal = new PriorityScheduler(ThreadPriority.Normal);
        /// <summary>
        /// 當下所有 Threads 中, 低於平常
        /// </summary>
        public static PriorityScheduler BelowNormal = new PriorityScheduler(ThreadPriority.BelowNormal);
        /// <summary>
        /// 當下所有 Threads 中, 最慢執行 (不需要非常即時的作業)
        /// </summary>
        public static PriorityScheduler Lowest = new PriorityScheduler(ThreadPriority.Lowest);
        #endregion

        #region properties
        /// <summary>
        /// 監控是否有 Task 一直 Queue 住, 而沒有執行 ?
        /// <para>如果某一種 Priority Queue 住的 Task 個數一直長大, 表示有問題, 要檢查 !!!</para>
        /// </summary>
        private const int MonitorUndoTasksCount = 30;
        private BlockingCollection<Task> _tasks = new BlockingCollection<Task>();
        private Thread[] _threads = null;
        private readonly ThreadPriority _priority;
        private readonly int _MaximumConcurrencyLevel = Math.Max(1, Environment.ProcessorCount);
        #endregion

        #region constructor
        /// <summary>
        /// 控制 Thread 的 Priority
        /// </summary>
        public PriorityScheduler(ThreadPriority priority)
        { _priority = priority; }
        #endregion

        /// <summary>
        /// 顯示尚未執行的 Task 數量, 超過 MonitorUndoTasksCount 個以上的才要顯示
        /// <para>目的 :: 用來監控是否 Task 的執行狀態, 是否有 locked Task 卡住</para>
        /// </summary>
        public static void ShowRunningPriorityScheduler() {
#if DEBUG
            string ShowFormat = string.Format(CultureInfo.CurrentCulture, "{0} : {1}", nameof(ShowRunningPriorityScheduler), "{0} undo Task Count :: {1}");
            if (Highest._threads != null && Highest._tasks.Count > MonitorUndoTasksCount) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ShowFormat, nameof(Highest), Highest._tasks.Count)); }
            if (AboveNormal._threads != null && AboveNormal._tasks.Count > MonitorUndoTasksCount) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ShowFormat, nameof(AboveNormal), AboveNormal._tasks.Count)); }
            if (Normal._threads != null && Normal._tasks.Count > MonitorUndoTasksCount) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ShowFormat, nameof(Normal), Normal._tasks.Count)); }
            if (BelowNormal._threads != null && BelowNormal._tasks.Count > MonitorUndoTasksCount) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ShowFormat, nameof(BelowNormal), BelowNormal._tasks.Count)); }
            if (Lowest._threads != null && Lowest._tasks.Count > MonitorUndoTasksCount) { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, ShowFormat, nameof(Lowest), Lowest._tasks.Count)); }
#endif
        }

        /*
        #region Methods
        private void Dispose(bool disposing) { 不需要特別清除它, 因為會造成 _tasks.GetConsumingEnumerable() 即時讀取失敗, 所以就不理了, 讓它自行回收
            if (!disposing) { return; }
            if (_tasks != null) {
                _tasks.Dispose();
                _tasks = null;
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "{0} is Disposed, _tasks = '{1}'", _priority, _tasks));
            }
        }
        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
        */

        public override int MaximumConcurrencyLevel { get { return _MaximumConcurrencyLevel; } }
        protected override IEnumerable<Task> GetScheduledTasks() { return _tasks; }
        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued) {
            return false; // we might not want to execute task that should schedule as high or low priority inline
        }
        protected override void QueueTask(Task task) {
            if (_tasks != null) { _tasks.Add(task); }

            #region initializing 階段才會執行, 其餘時間都不會
            if (_threads == null) {
                _threads = new Thread[_MaximumConcurrencyLevel];
                for (int i = 0; i < _threads.Length; i++) {
                    _threads[i] = new Thread(() => {
                        if (_tasks != null) {
                            foreach (Task t in _tasks.GetConsumingEnumerable())
                            { base.TryExecuteTask(t); } // 運作過程中, Each Thread 會透過 _tasks.GetConsumingEnumerable() 抓走 Queue 在 _tasks 裡的工作 (Task)
                        }
                    }) {
                        Name = string.Format(CultureInfo.CurrentCulture, "PriorityScheduler_{0}", i),
                        Priority = _priority,
                        IsBackground = true
                    };
                    if (!_threads[i].IsAlive) { _threads[i].Start(); }

                    /*
                    _threads[i] = new Thread(async () => {
                        foreach (Task t in _tasks.GetConsumingEnumerable()) {
                            await Task.Run(() => {
                                try {
                                    DateTime _startDT = DateTime.Now;
                                    base.TryExecuteTask(t);
                                    var TaskDone = t.Wait(UtilityConstants.TaskTimeOut);
                                    if (TaskDone)
                                    { UtilityGlobalFunc.LogTaskRunningResult(_startDT, t.Id); }
                                    else
                                    { UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, UtilityConstants.LogTaskTimeOutMsg, t.Id, UtilityConstants.TaskTimeOut)); }
                                } catch (Exception ex) { UtilGlobalFunc.Log(ex.StackTrace); }
                            });
                            //await t;
                        }
                    });
                    _threads[i].Name = string.Format(CultureInfo.CurrentCulture, "_{0}PriorityScheduler: {1}", _priority, i);
                    _threads[i].Priority = _priority;
                    _threads[i].IsBackground = true;
                    _threads[i].Start();
                    */
#if DEBUG
                    //UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "initialize ::: '{0}' _threads No.{1}, Name({2}), IsAlive({3})", _threads[i].Priority, i, _threads[i].Name, _threads[i].IsAlive));
#endif
                }
            }
            #endregion

        }
    }
}
