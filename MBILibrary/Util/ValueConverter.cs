﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace MBILibrary.Util
{
    public static class ValueConverter
    {
        #region 送給 Air Manager 時, 將 float, double 預設取至小數點後 6 位
        /// <summary>
        /// 送給 Air Manager 時, 將 double 預設取至小數點後 6 位
        /// </summary>
        public static double ShrinkDigitals(this double src) => RoundDown(src, UtilConstants.appConfig.Send2AMDigitals);
        /// <summary>
        /// 送給 Air Manager 時, 將 float 預設取至小數點後 6 位
        /// <para>1. float 的精準度整數 + 小數有效位數, 只有到 7 位數, 之後的數值都是無效數字</para>
        /// <para>2. 例如: 12345.6789012345 => 12345.679</para>
        /// </summary>
        public static float ShrinkDigitals(this float src) => (float)src.ConvertFloatToDouble().ShrinkDigitals();
        /// <summary>
        /// 取小數點後 decimalPlaces 位
        /// </summary>
        public static double RoundDown(double number, int decimalPlaces) => Math.Floor(number * Math.Pow(10, decimalPlaces)) / Math.Pow(10, decimalPlaces);
        #endregion

        /// <summary>
        /// 將 string 逐一個 char 加總
        /// </summary>
        public static int ConvertStringToInt(this string str) {
            int sum = 0;
            if (string.IsNullOrEmpty(str)) { return sum; }
            foreach (char c in str) { sum += c; }
            return sum;
        }

        /// <summary>
        /// Convert 'char' to 'float'
        /// </summary>
        public static float ConvertCharToFloat(this char value) => Convert.ToSingle((byte)value);

        #region Convert 'int', 'short', 'long', 'float', 'double', 'string' -> byte[]
        /// <summary>
        /// Convert 'byte' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this byte value) => new byte[] { value, 0, 0, 0 };
        /// <summary>
        /// Convert 'char' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this char value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'int' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this int value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'short' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this short value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'long' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this long value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'float' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this float value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'double' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this double value) => BitConverter.GetBytes(value);
        /// <summary>
        /// Convert 'string' to 'byte[]'
        /// </summary>
        public static byte[] ConvertToByteArray(this string value) => Encoding.ASCII.GetBytes(value);
        /// <summary>
        /// Convert 'string' to 'double'
        /// </summary>
        public static double ConvertToDouble(this string value) => Convert.ToDouble(value);
        /// <summary>
        /// Convert 'string' to 'float'
        /// </summary>
        public static double ConvertToFloat(this string value) => Convert.ToSingle(value.Replace("F", string.Empty));
        /// <summary>
        /// Convert 'string' to 'int'
        /// </summary>
        public static double ConvertToInt(this string value) => Convert.ToInt32(value);

        #endregion

        #region Convert byte[] -> 'int', 'short', 'long', 'float', 'double'
        /// <summary>
        /// Convert 'byte[]' to 'int'
        /// </summary>
        public static int ConvertByteArrayToInt(this byte[] value) => value == null || value.Length == 0 ? 0 : BitConverter.ToInt32(value, 0);
        /// <summary>
        /// Convert 'byte[]' to 'short'
        /// </summary>
        public static short ConvertByteArrayToShort(this byte[] value) => value == null || value.Length == 0 ? (short)0 : BitConverter.ToInt16(value, 0);
        /// <summary>
        /// Convert 'byte[]' to 'long'
        /// </summary>
        public static long ConvertByteArrayToLong(this byte[] value) => value == null || value.Length == 0 ? 0 : BitConverter.ToInt64(value, 0);
        /// <summary>
        /// Convert 'byte[]' to 'float'
        /// </summary>
        public static float ConvertByteArrayToFloat(this byte[] value) => value == null || value.Length == 0 ? 0 : BitConverter.ToSingle(value, 0);
        /// <summary>
        /// Convert 'byte[]' to 'double'
        /// </summary>
        public static double ConvertByteArrayToDouble(this byte[] value) => value == null || value.Length == 0 ? 0 : BitConverter.ToDouble(value, 0);
        /// <summary>
        /// Convert 'byte[]' to 'int[]'
        /// </summary>
        public static int[] ConvertByteArrayToIntArray(this byte[] value) => value == null || value.Length == 0 ? null : Array.ConvertAll(value, c => (int)c);
        /// <summary>
        /// Convert 'byte[]' to 'float[]'
        /// </summary>
        public static float[] ConvertByteArrayToFloatArray(this byte[] value) => value == null || value.Length == 0 ? null : Array.ConvertAll(value, c => (float)c);
        /// <summary>
        /// Convert 'byte[]' to 'double[]'
        /// </summary>
        public static double[] ConvertByteArrayToDoubleArray(this byte[] value) => value == null || value.Length == 0 ? null : Array.ConvertAll(value, c => (double)c);
        #endregion


        #region Convert object -> 'char', 'int', 'byte', 'float', 'double', 'string', 'int[]', 'float[]', 'double[]'
        /// <summary>
        /// object => int[]
        /// </summary>
        public static int[] ConvertObjectToIntArray(this object value) => value == null ? default : (int[])value;
        /// <summary>
        /// object => float[]
        /// </summary>
        public static float[] ConvertObjectToFloatArray(this object value) => value == null ? default : (float[])value;
        /// <summary>
        /// object => double[]
        /// </summary>
        public static double[] ConvertObjectToDoubleArray(this object value) => value == null ? default : (double[])value;
        /// <summary>
        /// object => char
        /// </summary>
        public static char ConvertObjectToChar(this object value) => value == null ? default : Convert.ToChar(value);
        /// <summary>
        /// object => int
        /// </summary>
        public static int ConvertObjectToInt32(this object value) => value == null ? default : Convert.ToInt32(value);
        /// <summary>
        /// object => int => float
        /// </summary>
        public static float ConvertObjectToInt32ToFloat(this object value) => value == null ? default : value.ConvertObjectToInt32().ConvertInt32ToFloat();
        /// <summary>
        /// object => char => byte => int => float
        /// </summary>
        public static float ConvertObjectToCharToByteToInt32ToFloat(this object value) => value == null ? default : value.ConvertObjectToChar().ConvertCharToByte().ConvertByteToInt32().ConvertInt32ToFloat();
        /// <summary>
        /// object => byte
        /// </summary>
        public static byte ConvertObjectToByte(this object value) => value == null ? default : Convert.ToByte(value);
        /// <summary>
        /// object => float
        /// </summary>
        public static float ConvertObjectToFloat(this object value) => value == null ? default : Convert.ToSingle(value);
        /// <summary>
        /// object => double => float
        /// </summary>
        public static float ConvertObjectToDoubleToFloat(this object value) => value == null ? default : value.ConvertObjectToDouble().ConvertDoubleToFloat();
        /// <summary>
        /// object => double
        /// </summary>
        public static double ConvertObjectToDouble(this object value) => value == null ? default : Convert.ToDouble(value);
        /// <summary>
        /// object => string
        /// </summary>
        public static string ConvertObjectToString(this object value) => value == null ? default : Convert.ToString(value);
        /// <summary>
        /// object => byte[]
        /// </summary>
        public static byte[] ConvertObjectToByteArray(this object value) {
            if (value == null) { return null; }
            using (var ms = new MemoryStream()) {
                new BinaryFormatter().Serialize(ms, value);
                return ms.ToArray();
            }
        }
        #endregion

        
        /// <summary>
        /// byte => int
        /// </summary>
        public static int ConvertByteToInt32(this byte value) => Convert.ToInt32(value);
        /// <summary>
        /// char => byte
        /// </summary>
        public static byte ConvertCharToByte(this char value) => Convert.ToByte(value);
        /// <summary>
        /// char => int => float
        /// </summary>
        public static float ConvertCharToInt32ToFloat(this char value) => value.ConvertCharToInt32().ConvertInt32ToFloat();
        /// <summary>
        /// char => int
        /// </summary>
        public static int ConvertCharToInt32(this char value) => value;
        /// <summary>
        /// int => float
        /// </summary>
        public static float ConvertInt32ToFloat(this int value) => value;
        /// <summary>
        /// int => char
        /// </summary>
        public static char ConvertInt32ToChar(this int value) => Convert.ToChar(value);
        /// <summary>
        /// '單精確度' 浮點數的值，轉成 相等的 '雙精確度' 浮點數
        /// </summary>
        //public static double ConvertFloatToDouble(this float value) { return Convert.ToDouble(value); }
        /// <summary>
        /// double => float
        /// </summary>
        public static float ConvertDoubleToFloat(this double value) => (float)value;
        /// <summary>
        /// float => double
        /// </summary>
        public static double ConvertFloatToDouble(this float value) => (double)new decimal(value);
        /// <summary>
        /// '單精確度' 浮點數的值 => int
        /// </summary>
        public static int ConvertFloatToInt32(this float value) => Convert.ToInt32(value);
        /// <summary>
        /// '單精確度' 浮點數的值 => byte
        /// </summary>
        public static byte ConvertFloatToByte(this float value) => Convert.ToByte(value);
        /// <summary>
        /// '單精確度' 浮點數的 Array 值 => int[]
        /// </summary>
        public static int[] ConvertFloatArrayToIntArray(this float[] value) => value == null ? null : Array.ConvertAll(value, f => f.ConvertFloatToInt32());
        /// <summary>
        /// '單精確度' 浮點數的 Array 值 => double[]
        /// </summary>
        public static double[] ConvertFloatArrayToDoubleArray(this float[] value) => value == null ? null : Array.ConvertAll(value, f => f.ConvertFloatToDouble());

        /// <summary>
        /// int[] => byte[]
        /// </summary>
        public static byte[] ConvertIntArrayToByteArray(this int[] value) => value == null ? null : Array.ConvertAll(value, i => Convert.ToByte(i));
        /// <summary>
        /// float[] => byte[]
        /// </summary>
        public static byte[] ConvertFloatArrayToByteArray(this float[] value) => value == null ? null : Array.ConvertAll(value, f => Convert.ToByte(f));
        /// <summary>
        /// double[] => byte[]
        /// </summary>
        public static byte[] ConvertDoubleArrayToByteArray(this double[] value) => value == null ? null : Array.ConvertAll(value, d => Convert.ToByte(d));
        /// <summary>
        /// string[] => double[]
        /// </summary>
        public static double[] ConvertStringArrayToDoubleArray(this string[] value) => value == null ? null : Array.ConvertAll(value, d => Convert.ToDouble(d));
        /// <summary>
        /// string[] => float[]
        /// </summary>
        public static float[] ConvertStringArrayToFloatArray(this string[] value) => value == null ? null : Array.ConvertAll(value, d => Convert.ToSingle(d.Replace("F", string.Empty)));
        /// <summary>
        /// string[] => int[]
        /// </summary>
        public static int[] ConvertStringArrayToIntArray(this string[] value) => value == null ? null : Array.ConvertAll(value, d => Convert.ToInt32(d));
    }
}
