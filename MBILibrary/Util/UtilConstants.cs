﻿using MBILibrary.Enums;
using MBILibrary.Models;
using MBILibrary.Setting;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;

namespace MBILibrary.Util
{
    public sealed class UtilConstants
    {
        private UtilConstants() { }
        /// <summary>
        /// [Debug Mode], 用來量測處理時間
        /// </summary>
        public static readonly Stopwatch DebugMeasureProcessTime = new Stopwatch();

        /// <summary>
        /// 取得目前的系統時間
        /// <para>HH:mm:ss fff</para>
        /// </summary>
        public static string CurrentTimeSpanfff => DateTime.Now.ToString(UtilDateTime.Format_HHmmssfff);

        #region 系統常數
        public const string ConsoleTitleShowVersion = "{0} [{1}] {2}";
        /// <summary>
        /// <para>1. gRPC Server Name</para>
        /// </summary>
        public const string MBIServer = "MBIServer";
        /// <summary>
        /// <para>0. X-Plane, AirManager data Switch</para>
        /// <para>1. gRPC Client Name</para>
        /// </summary>
        public const string MBISimBridge = "MBISimBridge";
        /// <summary>
        /// Air Manager Software Name
        /// </summary>
        public const string AirManagerSoftware = "Air Manager";
        /// <summary>
        /// X-Plane Software Name
        /// </summary>
        public const string XPlaneSoftware = "X-Plane";
        public const string Try2ConnectXPlane = "Connect To X-Plane";
        public const string JsonFile = "{0}.json";
        /// <summary>
        /// Testing run 在本機
        /// </summary>
        public const string IP_127_0_0_1 = "127.0.0.1";
        public const string Flag_Yes = "y";
        public const string Flag_No = "n";
        public static CultureInfo EnCulture = new CultureInfo("en-US");
        public const string ConsoleTitlePreHeader = "";
        public const string IPPortFormat = "{0}:{1}";
        public const string GrpcHostNameFormat = "{0}[{1}]";
        public const string ArrayDataRefNameFormat = "{0}[{1}]";
        public const string SendingMessageToSubscriberFormat = "Broadcast response Streaming MESSAGE ::: '{0}' to '{1}'";
        public const int DefaultXPlaneUDPPort = 49000;
        public const int DefaultAirManagerUDPPort = 65000;
        public const int DefaultGrpcServerPort = 5555;
        public const int InitConstValue = -9999;
        /// <summary>
        /// offset 是位移量, 值從 0 開始, user 可以透過 offset 調整寫入 DataRef[i] 的起始位置
        /// </summary>
        public const int NullOffset = -1;
        /// <summary>
        /// 關閉 "測試資料" 傳送的功能
        /// <para>1. AF -> X-Plane</para>
        /// <para>2. AF -> Air Manager</para>
        /// <para>3. GRPC -> X-Plane</para>
        /// <para>4. GRPC -> Air Manager</para>
        /// </summary>
        public const int Debug4SendDataDisable = -1;
        /// <summary>
        /// StringDataRefElement 如果沒有設定字串長度, 預設 : 255
        /// </summary>
        public const int NullStringLength = -1;
        public const string NullDataRefName = "NullDataRefName";
        public const string NullUnits = "NullUnits";
        public const string NullDescr = "NullDescription";
        /// <summary>
        /// 預設字串長度 : 255
        /// </summary>
        public const int DefaultStringLength = 255;
        /// <summary>
        /// 初始化 DataRefElement 紀錄 X-Plane 監聽回來的 byte[] BytesValue 值
        /// <para>4 bytes ::: int, float</para>
        /// </summary>
        public static byte[] InitialBytesValue4Float = new byte[] { 0, 0, 0, 0 };
        /// <summary>
        /// 初始化 DataRefElement 紀錄 X-Plane 監聽回來的 byte[] BytesValue 值
        /// <para>8 bytes ::: double</para>
        /// </summary>
        public static byte[] InitialBytesValue4Double = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        /// <summary>
        /// 容許記錄 "重送訊息" 的最大容量上限
        /// <para>最多 queue 100 則訊息, 超過就忽略不計, 避免記憶體爆掉</para>
        /// </summary>
        public const int Queue4ReSendGrpcMsgMaxCNT = 100;
        /// <summary>
        /// 關閉 Application 的指令
        /// </summary>
        public const string ExitPattern = "zzz";
        /// <summary>
        /// Hosting Service 剛啟動時的訊息
        /// </summary>
        public const string HostingServiceFormat = "{0} Start running Hosting Service '{1}'";
        /// <summary>
        /// 將 DataRef 註冊到兩個系統
        /// <para>1. X-Plane</para>
        /// <para>2. Air Manager</para>
        /// </summary>
        public const string SubscribeDataRefSuccessMessage = "註冊 DataRef 至 '{0}', {1} 個成功{2}";
        /// <summary>
        /// 將 Command 註冊到兩個系統
        /// <para>1. X-Plane</para>
        /// <para>2. Air Manager</para>
        /// </summary>
        public const string SubscribeCommandSuccessMessage = "註冊 Command 至 '{0}' {1} 個成功";
        public const string DataRefValueIsNullMessage = "the DataRef's value is null.";
        public const string DataRefNoValueChangedMessage = "the DataRef's DataType is not defined '{0}'. No value changed.";
        public const string DataRefCanNotWrittenMessage = "the DataRef({0}) can NOT be written, because of its Writable flag is NOT '{1}'.";
        public const string ErrorDataTypeNotSupportMessage = "Error DataType !! '{0}' not support...";
        public const string WaitingRegisterCompleted = "No.{0} {1} 等待完成註冊, SubscribedDataRefsFinished({2})..... {3}";
        public const string CanNotWriteIntoXPlane = "目前不存在於 initial 註冊的清單, 當下已註冊的 DataRef 個數({0}), DataRef({1}) Value({2}) 無法寫入 {3} !! {3} ConnectStatus({4})";
        public const string DataRefIsNotSubscribed = "DataRef({0}) is NOT subscribed, please check it.";
        public const string WriteDataRefValueIntoAM = "'{0}' Write to '{1}', {2}, '{3}', Value({4})";
        public const string SendData2AMMsg = "{0} : TriggerBy({1}), {2}, '{3}', Value({4})";
        public const string UDPsentFailMsg = "UDPsentFail and re-Send again!!";
        #endregion


        /// <summary>
        /// 執行 Task 超過 60 秒, 就停止執行
        /// </summary>
        public const int TaskTimeOut = 60000;
        /// <summary>
        /// 紀錄耗時超過 10 秒以上的 Log
        /// <para>紀錄對象 ::: "Task 執行成功後", "Query API 執行成功後"</para>
        /// </summary>
        public const int MaxSpendingMS = 10000;
        public const string LogTaskTimeOutMsg = "MasonSay :: TaskID({0}) operation has timed out, over {1} ms.";

        #region 本機的網路
        /// <summary>
        /// 使用的 "網路類型" ?
        /// <para>三擇一 ::: Ethernet / Wi-Fi / Mobile</para>
        /// </summary>
        public static string NetworkType = string.Empty;
        /// <summary>
        /// 網路狀態
        /// </summary>
        public static bool NetworkAlive = UtilNetwork.CheckNetworkStatus();
        #endregion

        /// <summary>
        /// 取得 Platform 字串, x64 ? x86 ?
        /// </summary>
        public static string RunPlatform = UtilGlobalFunc.TargetPlatform;
        /// <summary>
        /// 取得 Build Mode 字串, Debug ? Release ?
        /// </summary>
        public static SolutionConfigurationEnum RunConfiguration = UtilGlobalFunc.GetBuildMode;
        /// <summary>
        /// 系統參數
        /// </summary>
        public static AppConfig appConfig = UtilGlobalFunc.LoadAppConfig();


        #region 本機的 unique key
        /// <summary>
        /// 此 app 常駐在 windows 內的 app unique id
        /// </summary>
        public static string AppGuid = string.Empty;
        /// <summary>
        /// 有分 Debug/Release mode
        /// <para>Debug :: 增加 App Name, 目的是允許同一環境, 可以同時開啟 MBIServer/MBISimBridge, 方便 debug</para>
        /// <para>Release :: 只有 AppGuid, 同一環境, MBIServer/MBISimBridge 共用同一個 AppGuid, 所以同時只能開啟其中一個 App</para>
        /// </summary>
        public static string MutexId = string.Empty;
        public const string MutexIdMsg = "this app MutexId ::: {0}";
        /// <summary>
        /// 本機的 UniqueKey
        /// <para>Data Format => "{0}:{1}:{2}", appName, HostRegistryLicenseGuid, HostRegistryMachineName</para>
        /// <para>ex1. MBISimBridge:ff858fa5a4e0edf8084aab6f9538fb2b:DESKTOP-7DRSTLN</para>
        /// <para>ex1. MBIServer:54147fdfd966de88e0928aa0388d1a21:DESKTOP-7DRSTLN</para>
        /// </summary>
        public static string UniqueKeyOfMachine { get; set; }
        public const string UniqueKeyDataFormat = "{0}:{1}:{2}";
        /// <summary>
        /// UniqueKey - 紀錄在 Registry 上的 Host Name
        /// </summary>
        public const string HostRegistryName = "HostName";
        /// <summary>
        /// UniqueKey - 紀錄在 Registry 上的 Host License
        /// </summary>
        public const string HostRegistryLicense = "HostLicense";
        /// <summary>
        /// UniqueKey - 紀錄在 Registry 上的 Host License DateTime
        /// </summary>
        public const string HostRegistryLicenseDateTime = "HostLicenseDateTime";
        /// <summary>
        /// 取出 UniqueKey - 紀錄在 Registry 上的 Host Name
        /// </summary>
        public static string HostRegistryMachineName = string.Empty;
        /// <summary>
        /// 取出 UniqueKey - 紀錄在 Registry 上的 Host License
        /// </summary>
        public static string HostRegistryLicenseGuid = string.Empty;
        /// <summary>
        /// 取出 UniqueKey - 紀錄在 Registry 上的 Host License DateTime
        /// </summary>
        public static string HostRegistryLicenseGuidDateTime = string.Empty;
        #endregion

        /// <summary>
        /// 壓力測試 Flag
        /// <para>true : Load in 2000 個 DataRefs</para>
        /// <para>false : Load in 必須的 DataRefs (目前約 30 個)</para>
        /// </summary>
        public static bool TestingMode = false;
        public const int MaxSubscribeCNT = 2000;


        #region Process Console Title Message
        public const string DataRef_FrameratePeriod = "sim/time/framerate_period";
        /// <summary>
        /// 顯示 gRPC server 是否開啟 ?
        /// </summary>
        public const string gRPCServerPattern = "[gRPCServer]";
        /// <summary>
        /// 顯示 Daemon 的模式
        /// </summary>
        public const string BuildModePattern = "[BuildMode]";
        /// <summary>
        /// 顯示 Air Manager's TAG name
        /// </summary>
        public const string AMTagPattern = "[AMTag]";
        /// <summary>
        /// 顯示 Air Manager's IP Port
        /// </summary>
        public const string AMIPPortPattern = "[AMIPPort]";
        /// <summary>
        /// 顯示 X-Plane 所在 IP
        /// </summary>
        public const string XPlanePattern = "[XPlane]";
        /// <summary>
        /// 顯示系統時間
        /// </summary>
        public const string NowPattern = "[Now]";
        /// <summary>
        /// 顯示 X-Plane Current FPS value
        /// </summary>
        public const string FPSPattern = "[FPS]";
        /// <summary>
        /// 在 MBISimBridge Console 視窗上方顯示的標頭內容
        /// </summary>
        public static string MBISimBridgeConsoleTitleHeader = string.Format(CultureInfo.CurrentCulture, "{0} gRPC({1}), BuildMode({2}), AMTag({3}), AMPort({4}), X-Plane({5}), NOW({6}), FPS({7})"
            , ConsoleTitlePreHeader, gRPCServerPattern, BuildModePattern, AMTagPattern, AMIPPortPattern, XPlanePattern, NowPattern, FPSPattern);
        #endregion


        #region Google gRPC setting
        /// <summary>
        /// 取得最新更新的 Grpc 連線狀態, OPEN/CLOSE
        /// </summary>
        public static GrpcStatusEnum GetGrpcStatus => GrpcServerIsRunning ? GrpcStatusEnum.OPEN : GrpcStatusEnum.CLOSE;
        /// <summary>
        /// [gRPC Client 專用] 監聽 "gRPC Server Status" 是否已經開啟 ?
        /// <para>已開啟, 才會傳送 message 過去</para>
        /// <para>未開啟, 將 message log 在本機, 等待連線成功後, 再傳送過去</para>
        /// </summary>
        public static bool GrpcServerIsRunning = false;
        /// <summary>
        /// gRPC Server 所在的 IP:Port
        /// </summary>
        public static string GrpcServer = string.Format(CultureInfo.CurrentCulture, IPPortFormat, appConfig.gRPCServerIP, appConfig.gRPCServerPort);
        /// <summary>
        /// gRPC Client 如果沒有傳送成功, 會馬上再傳一次, 但會增加此 Pattern 來區分
        /// </summary>
        public const string GrpcReSendEventDataMessageHeader = "[GrpcReSend]";
        /// <summary>
        /// gRPC Server 隨時 Broadcast 給 ALL Clients 的訊息, 用途是確認 Client 端是否還活著, 如果無法回覆訊息, 就直接砍掉該 Client 端
        /// </summary>
        public const string CheckingClientAlive = "Checking Client Alive";
        /// <summary>
        /// 判斷 gRPC Client 是否有從 gRPC Server 順利取得回覆
        /// </summary>
        public const string IsRegistered = "is Subscribed OK.";
        /// <summary>
        /// 從 Grpc Server 傳送 Command Line 指令 給 Grpc Client, 各個參數之間的分割符號
        /// </summary>
        public const string CommandLineSplitor = "||";
        /// <summary>
        /// 忽略不需要處理的 Exception Message
        /// </summary>
        public static List<string> SkipErrorException = new List<string>() { "Only one write can be pending at a time" };
        #endregion

        public const string ObjectIsNullMsg = "{0} is null '{1}'.";
        public const string ConsoleLogMsg = "{0} - {1}{2}";
        public const string XPlaneConnectedMsg = "{0} : 'X-Plane' 已經準備好了, {1}";
        public const string GrpcTaskCanceledExceptionMsg = "{0} TaskCanceledException Message :: {1}\nStackTrace :: {2}";

        /// <summary>
        /// [Release] mode :: 限制 user 在同一個環境下, 只能開啟 'MBIServer' 或 'MBISimBridge' 擇一
        /// <para>1. 不能同時開啟 1 個以上的 'MBIServer' 或 'MBISimBridge'</para>
        /// <para>2. 不能同時開啟 'MBIServer' 或 'MBISimBridge'</para>
        /// <para>3. 但 [Debug] mode 環境下, 可以允許同時開啟 'MBIServer', 'MBISimBridge', 不允許重複開啟相同的 app</para>
        /// </summary>
        public static string SingletonErrorMessage = string.Format(CultureInfo.CurrentCulture, "Only one instance of {0}/{1} app is allowed at the same machine.", appConfig.gRPCServerName, appConfig.AirManagerTag);
        /// <summary>
        /// 顯示 Write X-Plane DataRef Value 後的結果資訊
        /// </summary>
        public static string WriteDataRefValueInfo = "{0} :: triggerBy({1}), Write {2} '{3}', Type({4}), value({5})";
        /// <summary>
        /// 顯示 Execute Command 後的結果資訊
        /// </summary>
        public static string ExecuteCommandInfo = "{0}, Execute Command({1}), Value({2}), Description({3}).";
        /// <summary>
        /// X-Plane Parameter Pattern 的起頭字串格式
        /// </summary>
        public const string XPlanePatternStart = "sim/";
        /// <summary>
        /// X-Plane 所在的 IP:Port
        /// </summary>
        public static string XPlaneInfo = string.Format(CultureInfo.CurrentCulture, IPPortFormat, appConfig.XPlaneIP, appConfig.XPlaneUDPPort);
        /// <summary>
        /// 取得最新更新的 X-Plane 連線狀態, DisConnected/ConnectedInitialize/ConnectedGetDataRefs
        /// </summary>
        public static XPlaneConnStatusEnum GetXPlaneStatus = XPlaneConnStatusEnum.DisConnected;
        /// <summary>
        /// 取得最新更新的 AirManager 連線狀態
        /// <para>true ::: 連線</para>
        /// <para>false :: 斷線</para>
        /// </summary>
        public static bool GetAirManagerStatus = false;
        /// <summary>
        /// AirManager 所在的 IP:Port
        /// </summary>
        public static string AirManagerInfo = string.Format(CultureInfo.CurrentCulture, IPPortFormat, appConfig.AirManagerIP, appConfig.AirManagerUDPPort);

        /// <summary>
        /// 用 Ping() 檢查網路是否正常通訊, 最多等待 60 秒, 如果 Ping 不到目標 Host, 就視為斷線
        /// </summary>
        public const int DefaultPingTimeout = 60000;

        /// <summary>
        /// 因為有時候 UDP 已經送出完成了, 但 X-Plane 值依然沒更新, 所以多送幾遍, 應該就不會掉資料了
        /// <para>預設送 8 次</para>
        /// </summary>
        public const int SetDataRefValueResendCNT = 8;
        /// <summary>
        /// 因為有時候已經送出完成了, 但 X-Plane 值依然沒更新, 所以多送幾遍, 應該就不會掉資料了
        /// <para>預設送 20 次</para>
        /// </summary>
        public const int UpdateDataRefValueResendCNT = 20;

        private static string versionStr = string.Empty;
        /// <summary>
        /// 取得版本資訊
        /// </summary>
        public static string VersionStr {
            get {
                if (string.IsNullOrEmpty(versionStr)) { versionStr = new MBISimVersion().SimVersion; }
                return versionStr;
            }
        }

        #region MBIServer Parameters Setting
        /// <summary>
        /// Grpc Server ConsoleTitle 的顯示主體
        /// </summary>
        public static string GrpcServerConsoleTitle = string.Format(CultureInfo.CurrentCulture, "{0} {1} Listening {2}", ConsoleTitlePreHeader, MBIServer, GrpcServer);
        /// <summary>
        /// 沒有任何 Grpc Client 連上線, 顯示為 ::: (0)
        /// </summary>
        public static readonly string GrpcClientEmpty = string.Format(CultureInfo.CurrentCulture, "({0})", 0);
        /// <summary>
        /// Grpc Server 因為沒有開啟 Grpc 功能, 所以直接要求 User 按 Enter 離開..
        /// </summary>
        public const string PressAnyKey = "請按 Enter 繼續...";
        /// <summary>
        /// AppConfig.gRPCon 設定為 false, 關閉 gRPC
        /// </summary>
        public const string GrpcOff = "off";
        #endregion

    }
}