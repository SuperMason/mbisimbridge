﻿using Microsoft.Win32;
using System;
using System.Globalization;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace MBILibrary.Util
{
    /// <summary>
    /// 依據 appName, registryKey 去 Window Regedit 取出對應的值
    /// </summary>
    public class RegistryHelper
    {
        #region fields
        private string keyPathStringPattern = "SOFTWARE\\{0}";
        private string AppName = string.Empty;
        private string keyPathString = string.Empty;
        private string mKey = string.Empty;
        private const string Error_NoHKEY_CURRENT_USER = "No HKEY_CURRENT_USER, please check regedit.exe!";
        private const string Error_NoKeyPath = "No [{0}] in HKEY_CURRENT_USER, please check regedit.exe!";
        #endregion


        #region Constructor
        /// <summary>
        /// 依據 appName, registryKey 去 Window Regedit 取出對應的值
        /// </summary>
        public RegistryHelper(string appName, string registryKey) {
            AppName = appName;
            keyPathString = string.Format(CultureInfo.CurrentCulture, keyPathStringPattern, AppName);
            mKey = registryKey;
        }
        #endregion


        #region Get/Set Registry Value
        /// <summary>
        /// 取出紀錄在 Registry 上的 Value
        /// <para>第一次取值, 會自動產生</para>
        /// <para>第二次以後, 會以第一次的值為主</para>
        /// </summary>
        public string GetRegistryValue() {
            try {
                string ret = null;

                #region 先讀取已經存在於 Registry 內的 value
                using (var currentUsr = Registry.CurrentUser) {
                    if (currentUsr == null)
                    { throw new Exception(Error_NoHKEY_CURRENT_USER); }
                    else {
                        using (var aimdir = currentUsr.OpenSubKey(keyPathString, true)) {
                            if (aimdir != null) {
                                object resVal = aimdir.GetValue(mKey);
                                ret = (resVal == null) ? null : Convert.ToString(resVal);
                            }
                        }
                    }
                }
                #endregion

                #region Open APP first time, set initial value into Registry
                if (string.IsNullOrEmpty(ret)) {
                    string setValue = string.Empty;
                    if (mKey.Equals(UtilConstants.HostRegistryLicense))
                    { setValue = GetHostRegistryLicenseGuid(); }
                    else if (mKey.Equals(UtilConstants.HostRegistryLicenseDateTime))
                    { setValue = GetDateTimeNow; }
                    else if (mKey.Equals(UtilConstants.HostRegistryName))
                    { setValue = GetMachineName(); }
                    else { }

                    if (!string.IsNullOrEmpty(setValue)) {
                        SetRegistryValue(setValue);
                        ret = GetRegistryValue();
                    }
                }
                #endregion

                return ret;
            } catch (Exception ex) {
                UtilGlobalFunc.Log(ex.Message);
                return null;
            }
        }
        /// <summary>
        /// 設定 Registry Value for 'keyPathString'
        /// </summary>
        /// <param name="mValue"></param>
        private void SetRegistryValue(string mValue) {
            try {
                using (var currentUsr = Registry.CurrentUser) {
                    if (currentUsr == null)
                    { throw new Exception(Error_NoHKEY_CURRENT_USER); }
                    else {
                        currentUsr.CreateSubKey(keyPathString);
                        using (var aimdir = currentUsr.OpenSubKey(keyPathString, true)) {
                            if (aimdir == null)
                            { throw new Exception(string.Format(CultureInfo.CurrentCulture, Error_NoKeyPath, keyPathString)); }
                            else {
                                if (aimdir.GetValue(mKey) != null)
                                { aimdir.DeleteValue(mKey, false); } //若登錄檔已經存在則刪除

                                aimdir.SetValue(mKey, mValue); //寫入登錄檔值
                            }
                        }
                    }
                }
            } catch (Exception ex) { UtilGlobalFunc.Log(ex.Message); }
        }
        #endregion


        #region Open APP first time, set initial value into Registry
        private string GetDateTimeNow => UtilGlobalFunc.GetOnLineTimeTick;
        /// <summary>
        /// 取得本機的裝置名稱
        /// </summary>
        /// <returns></returns>
        private string GetMachineName()
        { return Dns.GetHostName(); }
        /// <summary>
        /// auto generate Guid for 'Unique License Key'
        /// </summary>
        private string GetHostRegistryLicenseGuid() {
            using (MD5 md5 = MD5.Create()) {
                byte[] hash = md5.ComputeHash(Encoding.UTF8.GetBytes(GetDateTimeNow));
                return new Guid(hash).ToString().ToUpper();
            }
        }
        #endregion
    }
}
