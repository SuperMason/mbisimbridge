﻿using System.Runtime.InteropServices;

namespace MBILibrary.Util
{
    /// <summary>
    /// 用 Windows 內建的 msvcrt.dll 直接比對兩個 byte[], int[], float[], double[]
    /// </summary>
    public sealed class CompareArrayValue
    {
        private CompareArrayValue() { }
        #region 用 Windows 內建的 msvcrt.dll 直接比對兩個 byte[], int[], float[], double[]
        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int memcmp(byte[] x1, byte[] x2, long count);
        /// <summary>
        /// 用 Windows 內建的 msvcrt.dll 直接比對兩個 byte[]
        /// </summary>
        public static bool ByteArrayCompareEqual(byte[] x1, byte[] x2)
        {
            if (x1 == null || x2 == null) { return false; }
            // Validate buffers are the same length. This also ensures that the count does not exceed the length of either buffer.
            return x1.Length == x2.Length && memcmp(x1, x2, x1.Length) == 0;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int memcmp(int[] x1, int[] x2, long count);
        /// <summary>
        /// 用 Windows 內建的 msvcrt.dll 直接比對兩個 int[]
        /// </summary>
        public static bool IntArrayCompareEqual(int[] x1, int[] x2)
        {
            if (x1 == null || x2 == null) { return false; }
            // Validate buffers are the same length. This also ensures that the count does not exceed the length of either buffer.
            return x1.Length == x2.Length && memcmp(x1, x2, x1.Length) == 0;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int memcmp(float[] x1, float[] x2, long count);
        /// <summary>
        /// 用 Windows 內建的 msvcrt.dll 直接比對兩個 float[]
        /// </summary>
        public static bool FloatArrayCompareEqual(float[] x1, float[] x2)
        {
            if (x1 == null || x2 == null) { return false; }
            // Validate buffers are the same length. This also ensures that the count does not exceed the length of either buffer.
            return x1.Length == x2.Length && memcmp(x1, x2, x1.Length) == 0;
        }

        [DllImport("msvcrt.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int memcmp(double[] x1, double[] x2, long count);
        /// <summary>
        /// 用 Windows 內建的 msvcrt.dll 直接比對兩個 double[]
        /// </summary>
        public static bool DoubleArrayCompareEqual(double[] x1, double[] x2)
        {
            if (x1 == null || x2 == null) { return false; }
            // Validate buffers are the same length. This also ensures that the count does not exceed the length of either buffer.
            return x1.Length == x2.Length && memcmp(x1, x2, x1.Length) == 0;
        }
        #endregion

    }
}
