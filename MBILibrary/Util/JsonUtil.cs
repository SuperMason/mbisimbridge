﻿using MBILibrary.Setting;
using Newtonsoft.Json;
using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace MBILibrary.Util
{
    /// <summary>
    /// 專門處理 JSON 字串
    /// </summary>
    public sealed class JsonUtil
    {
        private JsonUtil() { }
        /// <summary>
        /// Formatted JSON String For Writing to File.
        /// </summary>
        public static string JsonPrettify(string json) {
            using (var sReader = new StringReader(json)) {
                using (var sWriter = new StringWriter()) {
                    var jReader = new JsonTextReader(sReader);
                    var jWriter = new JsonTextWriter(sWriter) { Formatting = Formatting.Indented };
                    jWriter.WriteToken(jReader);
                    return sWriter.ToString();
                }
            }
        }
        /// <summary>
        /// Get [***.json] File Parameter Setting
        /// </summary>
        public static T GetJsonFileSettings<T>(string jsonFilePath) where T : class {
            try {
                if (File.Exists(jsonFilePath))
                { return ReadJsonFile2Instance<T>(jsonFilePath); }
                else {
                    UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, "[jsonFilePath] file is not exist, path:{0}", jsonFilePath));
                    return (typeof(T) == typeof(AppConfig)) ? (T)Activator.CreateInstance(typeof(T)) : null;
                }
            } catch { return null; }
        }
        /// <summary>
        /// 將 JSON File 讀出至 Object
        /// </summary>
        public static T ReadJsonFile2Instance<T>(string filePath) where T : class {
            try {
                using (var fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                    using (var sr = new StreamReader(fs, Encoding.UTF8, true))
                    { return GetInstanceFromJSON<T>(sr.ReadToEnd()); }
                }
            } catch { return (T)Activator.CreateInstance(typeof(T)); }
        }
        /// <summary>
        /// 將 JSON String 寫出至檔案 *.json
        /// </summary>
        public static bool Write2JsonFile(string filePath, object obj) => Write2File(filePath, JsonPrettify(Convert2JsonString(obj)));
        /// <summary>
        /// 將 String 寫出至檔案 *.*
        /// </summary>
        public static bool Write2File(string filePath, string fileContent) {
            try {
                using (var fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite)) {
                    fs.SetLength(0); // discard the contents of the file by setting the length to 0
                    using (var sw = new StreamWriter(fs, Encoding.UTF8)) {
                        sw.Write(fileContent);// write the new content
                        sw.Flush();
                    }
                    fs.Close();
                }
                return true;
            } catch (Exception ex) { throw ex; }
        }
        /// <summary>
        /// Convert Instance to [JSON String]
        /// </summary>
        public static string Convert2JsonString(object value) => JsonConvert.SerializeObject(value, GetJsonSerializerSettings());
        /// <summary>
        /// Convert [JSON String] to Instance
        /// </summary>
        public static T GetInstanceFromJSON<T>(string jsonString) where T : class {
            try {
                if (string.IsNullOrEmpty(jsonString)) {
                    return (T)Activator.CreateInstance(typeof(T));
                } else {
                    if (jsonString.Contains("\\"))
                    { jsonString = jsonString.Replace("\\", "\\\\"); } // 排除會造成 json parser error 的字元
                    return JsonConvert.DeserializeObject<T>(jsonString, GetJsonSerializerSettings());
                }
            } catch { return (T)Activator.CreateInstance(typeof(T)); }
        }
        /// <summary>
        /// 取得 JSON 序列化 的設定
        /// </summary>
        private static JsonSerializerSettings GetJsonSerializerSettings() {
            return new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };
        }

    }
}
