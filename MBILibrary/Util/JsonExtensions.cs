﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.IO;

namespace MBILibrary.Util
{
    public static class JsonExtensions
    {
        public static IEnumerable<T> DeserializeValues<T>(Stream stream)
        { return DeserializeValues<T>(new StreamReader(stream)); }

        public static IEnumerable<T> DeserializeValues<T>(TextReader textReader) {
            var serializer = JsonSerializer.CreateDefault();
            var reader = new JsonTextReader(textReader) {
                SupportMultipleContent = true
            };

            while (reader.Read()) {
                if (reader.TokenType == JsonToken.StartArray) {
                    while (reader.Read()) {
                        if (reader.TokenType == JsonToken.Comment)
                            continue; // Do nothing
                        else if (reader.TokenType == JsonToken.EndArray)
                            break; // Break from the loop
                        else
                            yield return serializer.Deserialize<T>(reader);
                    }
                } else if (reader.TokenType == JsonToken.StartObject) {
                    while (reader.Read()) {
                        if (reader.TokenType == JsonToken.Comment)
                            continue; // Do nothing
                        else if (reader.TokenType == JsonToken.PropertyName)
                            continue; // Eat the property name
                        else if (reader.TokenType == JsonToken.EndObject)
                            break; // Break from the loop
                        else
                            yield return serializer.Deserialize<T>(reader);
                    }
                }
            }
        }

        public static bool IsPrimitive(this JsonToken tokenType) {
            switch (tokenType) {
                case JsonToken.Integer:
                case JsonToken.Float:
                case JsonToken.String:
                case JsonToken.Boolean:
                case JsonToken.Undefined:
                case JsonToken.Null:
                case JsonToken.Date:
                case JsonToken.Bytes:
                    return true;
                default:
                    return false;
            }
        }

        public static IEnumerable<string> ReadPrimitives(Stream stream)
        { return ReadPrimitives(new StreamReader(stream)); }

        public static IEnumerable<string> ReadPrimitives(TextReader textReader) {
            var reader = new JsonTextReader(textReader) {
                SupportMultipleContent = true
            };

            while (reader.Read()) {
                if (reader.TokenType.IsPrimitive()) {
                    if (reader.TokenType == JsonToken.String)
                        yield return reader.Value.ToString(); // No need for conversion
                    else
                        yield return (string)JValue.Load(reader); // Convert to string.
                }
            }
        }
    }
}
