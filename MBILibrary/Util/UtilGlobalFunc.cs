﻿using MBILibrary.Enums;
using MBILibrary.refDLL;
using MBILibrary.Setting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MBILibrary.Util
{
    public sealed class UtilGlobalFunc
    {
        private UtilGlobalFunc() {}

        /// <summary>
        /// 讀取外部 "AppConfig.json" 設定檔失敗時, 提供預設的系統參數值, 避免後續程式運作失敗
        /// </summary>
        private static AppConfig GetDefaultAppConfig => new AppConfig() { // 讀取 AppConfig.json 失敗, 提供預設的系統參數值
            gRPCServerName = UtilConstants.MBIServer,
            gRPCServerIP = UtilConstants.IP_127_0_0_1,
            gRPCServerPort = UtilConstants.DefaultGrpcServerPort,
            gRPCon = false,
            XPlaneIP = UtilConstants.IP_127_0_0_1,
            XPlaneUDPPort = UtilConstants.DefaultXPlaneUDPPort,
            AirManagerTag = UtilConstants.MBISimBridge,
            AirManagerIP = UtilConstants.IP_127_0_0_1,
            AirManagerUDPPort = UtilConstants.DefaultAirManagerUDPPort,
            CheckXPlaneAliveMS = 10, // 監控 X-Plane 是否活著, 每隔 0.01 秒檢查, 也同步更新 ALL DataRef's value
            Debug4SendDataAF2XP = -1, // 測試 AF 寫入 X-Plane 的頻率, 單位(ms)
            Debug4SendDataAF2AM = -1, // 測試 AF 寫入 Air Manager 的頻率, 單位(ms)
            Debug4SendDataGRPC2XP = -1, // 測試 GRPC 寫入 X-Plane 的頻率, 單位(ms)
            Debug4SendDataGRPC2AM = -1, // 測試 GRPC 寫入 Air Manager 的頻率, 單位(ms)
            Send2AMDigitals = 6, // 送給 Air Manager 時, 將 float, double 取至小數點後 6 位
            SkipDataRefSubscribeToXP = "",
            Debug4BigMason = false, // 啟動測試 "SD Mason" Code
            ShowBigMasonDebugMessage = false, // 顯示 "SD Mason" Debug 訊息
            DoNotNeedCompareValue = true,
            CpuUsageFullSpeed = false, // 取得 X-Plane UDP streaming data 時, 當下是否馬上執行 parsing byte[] data ? 為了減少 CPU usage, 最好 sleep CpuUsageWaitingMS ms(false), 可有效降低 CPU 使用率至 0.18, 假設馬上執行的使用率為 1
            CpuUsageWaitingMS = 0.01, // 當 CpuUsageFullSpeed 為 false 時, 控制 CPU sleep 0.01 ms
            AutoRebootTime4Updating = new DateTime(2020, 1, 1, 23, 59, 59),
            FacialSoftware = FacialDetectService.NoneFacial,
        };
        /// <summary>
        /// Load "AppConfig.json"
        /// <para>由 User 定義的系統參數值</para>
        /// </summary>
        public static AppConfig LoadAppConfig() {
            AppConfig appConfig = null;
            string AppConfigName = nameof(AppConfig);
            try { appConfig = JsonUtil.GetJsonFileSettings<AppConfig>(GetCurrentDirectoryJsonFilePath(AppConfigName)); }
            catch { appConfig = GetDefaultAppConfig; }
            Log(string.Format(CultureInfo.CurrentCulture, "Loading {0} ::: '{1}'", AppConfigName, appConfig));
            InitializePriorityScheduler();
            return appConfig;
        }
        public static string GetCurrentDirectoryJsonFilePath(string whichJsonFile) {
            string jsonFileName = string.Format(CultureInfo.CurrentCulture, UtilConstants.JsonFile, whichJsonFile);
            return Path.Combine(Directory.GetCurrentDirectory(), jsonFileName);
        }

        /// <summary>
        /// 列印出該物件的所有屬性值
        /// </summary>
        public static void PrintProperties(object value) {
            if (value == null) { return; }
            Log($"{Environment.NewLine}");
            Log($"------------ {value.GetType()} -------------");
            foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(value))
            { Log(string.Format(CultureInfo.CurrentCulture, "{0} : {1}", descriptor.Name, descriptor.GetValue(value))); }
            Log("---------------------------------------------------------------------------");
        }

        /// <summary>
        /// 調整 Console 視窗大小、位置
        /// </summary>
        public static void MiniSizeConsoleWindow() {
            IntPtr handle = Process.GetCurrentProcess().MainWindowHandle;
            User32.ShowWindow(handle, (int)nCmdShowWindowEnum.SW_MINIMIZE);
        }

        /// <summary>
        /// 依照 Grpc Client 連線數, 即時顯示在標頭上
        /// </summary>
        public static void SetMBIServerGrpcClientNum(string grpcClientNum) => SetMBIServerConsoleTitleMessage(string.Format(CultureInfo.CurrentCulture, "{0}{1}", grpcClientNum, UtilConstants.GrpcServerConsoleTitle));
        /// <summary>
        /// 動態即時更新 MBIServer Console 視窗的 Title 資訊
        /// <para>依照 Grpc Client 連線數, 即時顯示在標頭上</para>
        /// </summary>
        public static void SetMBIServerConsoleTitleMessage(string titleMessage) => Console.Title = string.Format(CultureInfo.CurrentCulture, UtilConstants.ConsoleTitleShowVersion, UtilConstants.MBIServer, UtilConstants.VersionStr, titleMessage);
        /// <summary>
        /// 動態即時更新 MBISimBridge Console 視窗的 Title 資訊
        /// </summary>
        public static void SetMBISimBridgeConsoleTitleMessage(string dataRefName = null, string val = null) {
            string TitleStr = (UtilConstants.RunConfiguration == SolutionConfigurationEnum.Debug)
                ? UtilConstants.MBISimBridgeConsoleTitleHeader
                : UtilConstants.MBISimBridgeConsoleTitleHeader.Replace(string.Format(CultureInfo.CurrentCulture, ", FPS({0})", UtilConstants.FPSPattern), string.Empty);

            string TitleMessage = TitleStr
                .Replace(UtilConstants.gRPCServerPattern, GetGrpcStatus)
                .Replace(UtilConstants.BuildModePattern, string.Format(CultureInfo.CurrentCulture, "{0},{1}", UtilConstants.RunPlatform, UtilConstants.RunConfiguration))
                .Replace(UtilConstants.AMTagPattern, UtilConstants.appConfig.AirManagerTag)
                .Replace(UtilConstants.AMIPPortPattern, GetAirManagerStatus)
                .Replace(UtilConstants.XPlanePattern, GetXPlaneStatus)
                .Replace(UtilConstants.NowPattern, GetOnLineTimeTick)
                .Replace(UtilConstants.FPSPattern, (!string.IsNullOrEmpty(dataRefName) && dataRefName.Equals(UtilConstants.DataRef_FrameratePeriod)) ? val : "0");
            Console.Title = string.Format(CultureInfo.CurrentCulture, UtilConstants.ConsoleTitleShowVersion, UtilConstants.MBISimBridge, UtilConstants.VersionStr, TitleMessage);
#if DEBUG
            PriorityScheduler.ShowRunningPriorityScheduler(); // Debug Mode 再監控 Task 執行狀況
#endif
        }
        /// <summary>
        /// 依據 AppConfig.gRPCon 設定值, 顯示 gRPC 狀態
        /// <para>功能 off :: 直接顯示 "off"</para>
        /// <para>功能 on :: 依據連線狀態, 顯示 OPEN/CLOSE</para>
        /// </summary>
        private static string GetGrpcStatus => UtilConstants.appConfig.gRPCon ? string.Format(CultureInfo.CurrentCulture, "{0},{1}", string.Format(CultureInfo.CurrentCulture, "{0}", UtilConstants.GetGrpcStatus), UtilConstants.GrpcServer) : UtilConstants.GrpcOff;
        /// <summary>
        /// 顯示 X-Plane 的連線狀態
        /// <para>連線成功 :: 顯示 "OPEN"</para>
        /// <para>尚未連線 :: 顯示 "CLOSE"</para>
        /// </summary>
        private static string GetXPlaneStatus => string.Format(CultureInfo.CurrentCulture, "{0},{1}", string.Format(CultureInfo.CurrentCulture, "{0}", UtilConstants.GetXPlaneStatus), UtilConstants.XPlaneInfo);
        /// <summary>
        /// 顯示 AirManager 的連線狀態
        /// <para>連線成功 :: 顯示 "OPEN"</para>
        /// <para>尚未連線 :: 顯示 "CLOSE"</para>
        /// </summary>
        private static string GetAirManagerStatus => string.Format(CultureInfo.CurrentCulture, "{0}", UtilConstants.AirManagerInfo);//string.Format(CultureInfo.CurrentCulture, "{0},{1}", string.Format(CultureInfo.CurrentCulture, "{0}", UtilConstants.GetAirManagerStatus), UtilConstants.AirManagerInfo);

        /// <summary>
        /// 初始化五種 TaskScheduler
        /// </summary>
        private static void InitializePriorityScheduler() {
            Log(string.Format(CultureInfo.CurrentCulture, "{0} Thread Count :: {1}", nameof(PriorityScheduler.Highest), PriorityScheduler.Highest.MaximumConcurrencyLevel));
            Log(string.Format(CultureInfo.CurrentCulture, "{0} Thread Count :: {1}", nameof(PriorityScheduler.AboveNormal), PriorityScheduler.AboveNormal.MaximumConcurrencyLevel));
            Log(string.Format(CultureInfo.CurrentCulture, "{0} Thread Count :: {1}", nameof(PriorityScheduler.Normal), PriorityScheduler.Normal.MaximumConcurrencyLevel));
            Log(string.Format(CultureInfo.CurrentCulture, "{0} Thread Count :: {1}", nameof(PriorityScheduler.BelowNormal), PriorityScheduler.BelowNormal.MaximumConcurrencyLevel));
            Log(string.Format(CultureInfo.CurrentCulture, "{0} Thread Count :: {1}", nameof(PriorityScheduler.Lowest), PriorityScheduler.Lowest.MaximumConcurrencyLevel));
        }

        public static string GetPrecisionValue(object value) {
            if (value.ValueIsFloat()) { return ((float)value).ToString("R"); }
            else if (value.ValueIsInt()) { return ((int)value).ToString(CultureInfo.CurrentCulture); }
            else if (value.ValueIsBool()) { return ((bool)value).ToString(CultureInfo.CurrentCulture); }
            else if (value.ValueIsDouble()) { return ((double)value).ToString(CultureInfo.CurrentCulture); }
            else if (value.ValueIsByte()) { return ((byte)value).ToString(CultureInfo.CurrentCulture); }
            else if (value.ValueIsByteArray()) { return string.Join(",", (byte[])value); }
            else if (value.ValueIsIntArray()) { return string.Join(",", (int[])value); }
            else if (value.ValueIsFloatArray()) { return string.Join(",", (float[])value); }
            else if (value.ValueIsDoubleArray()) { return string.Join(",", (double[])value); }
            else { return value.ToString(); }
        }
        
        /// <summary>
        /// 取得 Listen MBIServer 的基本資訊
        /// </summary>
        public static string GetMBIServerInfo => string.Format(CultureInfo.CurrentCulture, "Listen {0}({1})", UtilConstants.MBIServer, UtilConstants.GrpcServer);
        /// <summary>
        /// 取得 gRPC off 的訊息
        /// </summary>
        public static string GetGRPCoffMsg(string msg) => string.Format(CultureInfo.CurrentCulture, "{0}, {1}", GrpcFunctionStatus, msg);
        /// <summary>
        /// 顯示 gRPC 功能 啟動/關閉
        /// </summary>
        private static string GrpcFunctionStatus => string.Format(CultureInfo.CurrentCulture, "{0}.{1} 功能 '{2}'", nameof(AppConfig), nameof(AppConfig.gRPCon), ActiveStatusDesc(UtilConstants.appConfig.gRPCon));
        private static string ActiveStatusDesc(bool flag) => flag ? "啟動" : "關閉";

        /// <summary>
        /// 記錄上線時間點
        /// </summary>
        public static string GetOnLineTimeTick => DateTime.Now.ToString(UtilDateTime.Format_HHmmssfffWithDot);

        /// <summary>
        /// UtilConstants.UniqueKeyOfMachine = string.Format(CultureInfo.CurrentCulture, UtilConstants.UniqueKeyDataFormat, AppName, UtilConstants.HostRegistryLicenseGuid, UtilConstants.HostRegistryMachineName);
        /// </summary>
        /// <param name="uniqueKeyOfMachine">AppName:UtilConstants.HostRegistryLicenseGuid:UtilConstants.HostRegistryMachineName</param>
        public static List<string> GetDetailOfUniqueKeyOfMachine(string uniqueKeyOfMachine) => string.IsNullOrEmpty(uniqueKeyOfMachine) ? null : uniqueKeyOfMachine.Split(':').ToList();

        /// <summary>
        /// 偵測 Platform 是否為 x64 ?
        /// </summary>
        public static bool Is64BitProcess => IntPtr.Size == 8;
        /// <summary>
        /// 取得 Platform 字串, x64 ? x86 ?
        /// </summary>
        public static string TargetPlatform => string.Format(CultureInfo.CurrentCulture, "{0}", Is64BitProcess ? SolutionPlatformEnum.x64 : SolutionPlatformEnum.x86);

        public static string GetTriggerByWhatFunc(TriggerByWhatSystem triggerBy, string funcName) => string.Format(CultureInfo.CurrentCulture, "triggerBy({0}), FuncName({1})", triggerBy, funcName);

        /// <summary>
        /// 取得 Debug/Release Mode
        /// </summary>
        public static SolutionConfigurationEnum GetBuildMode {
            get {
#if DEBUG
                return SolutionConfigurationEnum.Debug;
#else
                return SolutionConfigurationEnum.Release;
#endif            
            }
        }


        /// <summary>
        /// Print out to console UI
        /// </summary>
        public static StringBuilder ConsoleWriter = new StringBuilder();

        public static void Log(string rawDataRef, TriggerByWhatSystem triggerBy, string funcName) => Log(string.Format(CultureInfo.CurrentCulture, "'{0}' 並沒有註冊至 '{1}', {2}.", rawDataRef, UtilConstants.XPlaneSoftware, GetTriggerByWhatFunc(triggerBy, funcName)));
        /// <summary>
        /// 將訊息顯示在 console.log 上
        /// </summary>
        public static void Log(string text) {
            if (string.IsNullOrEmpty(text)) { return; }
            string s = string.Format(CultureInfo.CurrentCulture, UtilConstants.ConsoleLogMsg, DateTime.Now.ToString(UtilDateTime.Format_HHmmssfff), text, Environment.NewLine);

            if (s.Contains(UtilConstants.Try2ConnectXPlane)) { s = Environment.NewLine + s; }
            ConsoleWriteColor(s, ConsoleColor.Red);
            //if (ConsoleWriter.Length < 100000)
            //{ ConsoleWriter.Append(s); }
            //else
            //{ Console.WriteLine($"避免 Out Of Memory, 所以不記錄, skip this log. 這應該是大量測試的 log data ::: {s}"); }
        }
        /// <summary>
        /// 依據 appConfig.Debug4BigMason 設定, 來顯示 Log 資訊
        /// </summary>
        public static void ShowMsgByDebug4BigMason(string msg) {
#if DEBUG
            if (UtilConstants.appConfig.Debug4BigMason && UtilConstants.appConfig.ShowBigMasonDebugMessage) { Log(msg); }
#endif
        }

        /// <summary>
        /// usage: WriteColor("This is my [[message]] with inline [[color]] changes.", ConsoleColor.Yellow);
        /// </summary>
        /// <param name="message">要著色的字串用 [[xxxxxx]] 包起來</param>
        /// <param name="color">著色顏色</param>
        public static void ConsoleWriteColor(string message, ConsoleColor color) {
            var pieces = Regex.Split(message, @"(\[[^\]]*\]])");
            for (int i = 0; i < pieces.Length; i++) {
                if (pieces[i].StartsWith(UtilXPlane.startPattern) && pieces[i].EndsWith(UtilXPlane.endPattern)) {
                    Console.ForegroundColor = color;
                    pieces[i] = pieces[i].Substring(UtilXPlane.startPattern.Length, pieces[i].Length - (UtilXPlane.startPattern.Length + UtilXPlane.endPattern.Length));
                }
                Console.Write(pieces[i]);
                Console.ResetColor();
            }
        }

        /// <summary>
        /// 紀錄 Task 執行成功後, 耗時多久 ??
        /// <para>耗時超過 1000 ms 再紀錄下來, 其餘 by pass 忽略不計</para>
        /// </summary>
        public static void LogTaskRunningResult(DateTime startDT, int taskId) {
            double SpendingMS = UtilSysTimes.GetSpendingMilliseconds(startDT);
            if (SpendingMS > UtilConstants.MaxSpendingMS) { Log(string.Format(CultureInfo.CurrentCulture, "TaskID({0}) spending {1} ms.", taskId, SpendingMS)); }
        }

        /// <summary>
        /// 用 Ping() 檢查網路是否正常通訊, Timeout Setting
        /// <para>1. 最多等待 60 秒, 如果 Ping 不到目標 Host, 就視為斷線</para>
        /// <para>2. 初始預設值 :: 10 秒</para>
        /// </summary>
        public static int GetPingTimeout => UtilConstants.DefaultPingTimeout;

        /// <summary>
        /// 判斷 DataRefName, Type 是否為 Array 型態 ?
        /// </summary>
        public static bool IsArray(string str) => !string.IsNullOrEmpty(str) && str.Contains("[") && str.Contains("]");

        /// <summary>
        /// 判斷 Array Type 是何種型態 ?
        /// <para>int? byte? float? double?</para>
        /// </summary>
        public static bool IsTheTypeArray(string baseType, string theType) => !string.IsNullOrEmpty(baseType) && baseType.StartsWith(string.Format(CultureInfo.CurrentCulture, "{0}[", theType), StringComparison.OrdinalIgnoreCase);

        /// <summary>
        /// 判斷 單一值 Type 是何種型態 ?
        /// <para>int? byte? float? double?</para>
        /// </summary>
        public static bool IsTheSingleType(string baseType, string theType) => !string.IsNullOrEmpty(baseType) && baseType.StartsWith(theType, StringComparison.OrdinalIgnoreCase);

        /// <summary>
        /// 成功? 失敗?
        /// </summary>
        public static string GetResultMsg(bool res) => res ? "成功" : "失敗";

        /// <summary>
        /// 判斷此 Array 是否為空 ?
        /// </summary>
        public static bool IsArrayEmpty<T>(T[] tAry) => tAry == null || tAry.Count() == 0;

    }
}
