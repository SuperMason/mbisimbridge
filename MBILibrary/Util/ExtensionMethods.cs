﻿using MBILibrary.Enums;
using Microsoft.Win32.SafeHandles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.AccessControl;
using System.Text;
using System.Xml;

namespace MBILibrary.Util
{
    public static class ExtensionMethods
    {
        #region 檢查 value 是何種 data type ? int, byte, char, float, double, string, bool, int[], byte[], float[], double[]
        /// <summary>
        /// 值是否為 int, float, double, byte 其中一種 ?
        /// </summary>
        public static bool ValueIsSingle(this object value) => value.ValueIsInt() || value.ValueIsFloat() || value.ValueIsDouble() || value.ValueIsByte();
        /// <summary>
        /// 值是否為 int[], float[], double[], byte[] 其中一種 ?
        /// </summary>
        public static bool ValueIsArray(this object value) => value.ValueIsIntArray() || value.ValueIsFloatArray() || value.ValueIsDoubleArray() || value.ValueIsByteArray();
        /// <summary>
        /// 值是否為 int ?
        /// </summary>
        public static bool ValueIsInt(this object value) => value != null && value.GetType() == typeof(int);
        /// <summary>
        /// 值是否為 byte ?
        /// </summary>
        public static bool ValueIsByte(this object value) => value != null && value.GetType() == typeof(byte);
        /// <summary>
        /// 值是否為 char ?
        /// </summary>
        public static bool ValueIsChar(this object value) => value != null && value.GetType() == typeof(char);
        /// <summary>
        /// 值是否為 float ?
        /// </summary>
        public static bool ValueIsFloat(this object value) => value != null && value.GetType() == typeof(float);
        /// <summary>
        /// 值是否為 double ?
        /// </summary>
        public static bool ValueIsDouble(this object value) => value != null && value.GetType() == typeof(double);
        /// <summary>
        /// 值是否為 string ?
        /// </summary>
        public static bool ValueIsString(this object value) => value != null && value.GetType() == typeof(string);
        /// <summary>
        /// 值是否為 bool ?
        /// </summary>
        public static bool ValueIsBool(this object value) => value != null && value.GetType() == typeof(bool);
        /// <summary>
        /// 值是否為 int[] ?
        /// </summary>
        public static bool ValueIsIntArray(this object value) => value != null && value.GetType() == typeof(int[]);
        /// <summary>
        /// 值是否為 byte[] ?
        /// </summary>
        public static bool ValueIsByteArray(this object value) => value != null && value.GetType() == typeof(byte[]);
        /// <summary>
        /// 值是否為 float[] ?
        /// </summary>
        public static bool ValueIsFloatArray(this object value) => value != null && value.GetType() == typeof(float[]);
        /// <summary>
        /// 值是否為 double[] ?
        /// </summary>
        public static bool ValueIsDoubleArray(this object value) => value != null && value.GetType() == typeof(double[]);
        #endregion

        /// <summary>
        /// 判斷此 List 是否為空 ?
        /// </summary>
        public static bool IsListEmpty<T>(this IEnumerable<T> tList) where T : class
        { return tList == null || tList.Count() == 0; }

        #region 在 List 清單中, 依據某一屬性值, 為 最大值/最小值 者, 取之
        /// <summary>
        /// 在 List 清單中, 依據某一屬性值, 為最大值者, 取之
        /// </summary>
        public static T MaxBy<T, R>(this IEnumerable<T> en, Func<T, R> evaluate) where R : IComparable<R> {
            return en.Select(t => new Tuple<T, R>(t, evaluate(t)))
                .Aggregate((max, next) => next.Item2.CompareTo(max.Item2) > 0 ? next : max).Item1;
        }
        /// <summary>
        /// 在 List 清單中, 依據某一屬性值, 為最小值者, 取之
        /// </summary>
        public static T MinBy<T, R>(this IEnumerable<T> en, Func<T, R> evaluate) where R : IComparable<R> {
            return en.Select(t => new Tuple<T, R>(t, evaluate(t)))
                .Aggregate((max, next) => next.Item2.CompareTo(max.Item2) < 0 ? next : max).Item1;
        }
        #endregion

        #region Slice to IEnumerable
        public static T[] CopySlice<T>(this T[] source, int index, int length, bool padToLength = false) {
            int n = length;
            T[] slice = null;

            if (source == null) { return slice; }
            if (source.Length < index + length) {
                n = source.Length - index;
                if (padToLength)
                { slice = new T[length]; }
            }

            if (slice == null) { slice = new T[n]; }
            //Array.Copy(source, index, slice, 0, n);
            Buffer.BlockCopy(source, index, slice, 0, n);
            return slice;
        }
        /// <summary>
        /// 每 n 個, 切一片
        /// </summary>
        public static IEnumerable<T[]> Slices<T>(this T[] source, int count, bool padToLength = false) {
            for (var i = 0; i < source.Length; i += count)
            { yield return source.CopySlice(i, count, padToLength); }
        }
        #endregion


        /// <summary>
        /// 將 "來源物件(source)", 相同屬性的值, 複製貼上至 "目的物件(dest)"
        /// </summary>
        public static void CopyPropertiesTo<T, TU>(this T source, TU dest) {
            var sourceProps = typeof(T).GetProperties().Where(x => x.CanRead).ToList();
            var destProps = typeof(TU).GetProperties().Where(x => x.CanWrite).ToList();

            foreach (var sourceProp in sourceProps) {
                if (destProps.ToArray().Any(x => x.Name == sourceProp.Name)) {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    if (p.CanWrite) // check if the property can be set or no.
                    { p.SetValue(dest, sourceProp.GetValue(source, null), null); }
                }
            }
        }

        /// <summary>
        /// 將 File Size 從 bytes 轉換成指定的 Size String
        /// <para>例如 : 12345 bytes => 12.05 KB</para>
        /// </summary>
        public static string ToSize(this long value, SizeUnits unit) => (value / Math.Pow(1024, (long)unit)).ToString("0.00");
        /// <summary>
        /// 將 File Size 從 bytes 轉換成指定的 Size String
        /// <para>例如 : 12345 bytes => 12.05 KB</para>
        /// </summary>
        public static double ToSizeDouble(this long value, SizeUnits unit) => Math.Round(value / Math.Pow(1024, (long)unit), 3);

        /// <summary>
        /// 合併任兩個 List, 並且移除重複的 element
        /// </summary>
        public static List<T> JoinList<T>(this List<T> first, List<T> second) {
            if (first == null) { return second; }
            if (second == null) { return first; }
            return first.Concat(second).ToList();
        }

        public static T[] SubArray<T>(this T[] data, int index, int length) {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        /// <summary>
        /// TimeSpan 乘以 n 值, 例如: 3 個 TimeSpans = 3 x TimeSpan
        /// </summary>
        public static TimeSpan MultiplyBy(this TimeSpan t, int multiplier) => new TimeSpan(t.Ticks * multiplier);


        #region 字串轉全形, 字串轉半形
        /// <summary>
        /// 字串轉全形
        /// </summary>
        /// <param name="input">任一字元串</param>
        /// <returns>全形字元串</returns>
        public static string ToWide(this string input) {
            if (string.IsNullOrEmpty(input)) { return input; }
            char[] c = input.ToCharArray(); //半形轉全形
            for (int i = 0; i < c.Length; i++) {   //全形空格為12288，半形空格為32
                if (c[i] == 32) {
                    c[i] = (char)12288;
                    continue;
                }

                if (c[i] < 127) //其他字元半形(33-126)與全形(65281-65374)的對應關係是：均相差65248
                { c[i] = (char)(c[i] + 65248); }
            }
            return new string(c);
        }
        /// <summary>
        /// 字串轉半形
        /// </summary>
        /// <param name="input">任一字元串</param>
        /// <returns>半形字元串</returns>
        public static string ToNarrow(this string input) {
            if (string.IsNullOrEmpty(input)) { return input; }
            char[] c = input.ToCharArray(); //全形轉半形
            for (int i = 0; i < c.Length; i++) {   //全形空格為12288，半形空格為32
                if (c[i] == 12288) {
                    c[i] = (char)32;
                    continue;
                }

                if (c[i] > 65280 && c[i] < 65375)
                { c[i] = (char)(c[i] - 65248); }
            }
            return new string(c);
        }
        #endregion

        /// <summary>
        /// 深層複製物件
        /// </summary>
        public static T DeepClone<T>(this T item) {
            if (item != null) {
                using (var stream = new MemoryStream()) {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, item);
                    stream.Seek(0, SeekOrigin.Begin);
                    return (T)formatter.Deserialize(stream);
                }
            }

            return default(T);
        }

        /// <summary>
        /// List convert to DataTable
        /// </summary>
        public static DataTable ToDataTable<T>(this List<T> items) {
            var tb = new DataTable(typeof(T).Name + "_" + DateTime.Now.Millisecond);

            PropertyInfo[] props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (PropertyInfo prop in props) {
                Type t = GetCoreType(prop.PropertyType);
                tb.Columns.Add(prop.Name, t);
            }

            foreach (T item in items) {
                var values = new object[props.Length];

                for (int i = 0; i < props.Length; i++)
                { values[i] = props[i].GetValue(item, null); }

                tb.Rows.Add(values);
            }

            return tb;
        }

        /// <summary>
        /// Return underlying type if type is Nullable otherwise return the type
        /// </summary>
        public static Type GetCoreType(Type t) {
            if (t != null && IsNullable(t))
            { return (!t.IsValueType) ? t : Nullable.GetUnderlyingType(t); }
            else
            { return t; }
        }

        /// <summary>
        /// Determine of specified type is nullable
        /// </summary>
        public static bool IsNullable(Type t) => !t.IsValueType || (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(Nullable<>));


        public static IEnumerable<TSource> Distinct<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector) {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source) {
                var elementValue = keySelector(element);
                if (seenKeys.Add(elementValue))
                { yield return element; }
            }
        }

        /// <summary>
        /// <para>true ::: File is Unavailable, because of reason</para>
        /// <para>1. still being written to</para>
        /// <para>2. being processed by another thread</para>
        /// <para>3. does not exist (has already been processed)</para>
        /// <para>false ::: File is Available, exist and NOT locked</para>
        /// </summary>
        public static bool IsFileLocked(this FileInfo file) {
            return IsFileInUse(file.FullName);
            //Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "新版判斷檔案存取方式 :: {0}, File : {1}", AccessFileResult, file.FullName));
            //FileStream stream = null;
            //try { stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None); }
            //catch (IOException ex)
            //{
            //    Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "ExtensionMethods.IsFileLocked() ::: File Access Failed, ErrMsg: {0}", ex.Message));
            //    return true;
            //}
            //finally
            //{
            //    if (stream != null)
            //    {
            //        stream.Close();
            //        stream.Dispose();
            //    }
            //}
            //return false;
        }
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern SafeFileHandle CreateFile(string lpFileName, FileSystemRights dwDesiredAccess, FileShare dwShareMode,
                                                        IntPtr securityAttrs, FileMode dwCreationDisposition, FileOptions dwFlagsAndAttributes, IntPtr hTemplateFile);
        private const int ERROR_SHARING_VIOLATION = 32;
        /// <summary>
        /// <para>判斷檔案是否正在被存取... 無法使用</para>
        /// <para>true 正在使用中</para>
        /// <para>false 沒有被 locked</para>
        /// </summary>
        public static bool IsFileInUse(string fileName) {
            bool inUse = false;
            using (SafeFileHandle fileHandle = CreateFile(fileName, FileSystemRights.Modify, FileShare.Write,
                                                          IntPtr.Zero, FileMode.OpenOrCreate, FileOptions.None, IntPtr.Zero)) {
                if (fileHandle.IsInvalid) {
                    if (Marshal.GetLastWin32Error() == ERROR_SHARING_VIOLATION)
                    { inUse = true; }
                }
                fileHandle.Close();
            }
            return inUse;
        }

        public static string ToHtmlTable(this DataTable myDataTable) {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("<table width='100%' border='2'>"); //start the table.

            sb.AppendLine("<tr>"); //start the row.
            for (int colIdx = 0; colIdx < myDataTable.Columns.Count; colIdx++) //write the columns.
            { sb.AppendLine(string.Format(CultureInfo.CurrentCulture, "<th align='left'>{0}</th>", myDataTable.Columns[colIdx].ColumnName)); }
            sb.AppendLine("</tr>"); //close the row.

            //add the data.
            for (int rowIdx = 0; rowIdx < myDataTable.Rows.Count; rowIdx++) {
                sb.AppendLine("<tr>");
                for (int colIdx = 0; colIdx < myDataTable.Columns.Count; colIdx++)
                { sb.AppendLine(string.Format(CultureInfo.CurrentCulture, "<td>{0}</td>", myDataTable.Rows[rowIdx][colIdx].ToString())); }
                sb.AppendLine("</tr>");
            }

            sb.AppendLine("</table>"); //close the table and create a new line.
            return sb.ToString(); //return the html.
        }

        /// <summary>
        /// 依據 [特定字串] 進行字串切割
        /// </summary>
        /// <param name="str">原始字串</param>
        /// <param name="splitter">切割字串</param>
        public static string[] Split(this string str, string splitter) => str.Split(new[] { splitter }, StringSplitOptions.None).FilterNullOrEmpty();
        /// <summary>
        /// 過濾掉 null, empty 的 elements
        /// </summary>
        public static string[] FilterNullOrEmpty(this string[] ta) => ta.Where(s => !string.IsNullOrEmpty(s)).ToArray();

        public static string Stringify(this XmlDocument doc) {
            using (var mStream = new MemoryStream()) {
                using (var writer = new XmlTextWriter(mStream, Encoding.Unicode)) {
                    writer.Formatting = Formatting.Indented;
                    doc.WriteContentTo(writer);
                    writer.Flush();
                    mStream.Flush();
                    mStream.Position = 0;
                    using (var sReader = new StreamReader(mStream))
                    { return sReader.ReadToEnd(); }
                }
            }
        }


        #region int to "Enum", int to "Enum String"
        public static T ToEnum<T>(this int value) where T : struct
        { return (T)(object)value; }
        public static string ToEnumName<T>(this int value) where T : struct
        { return ((T)(object)value).ToString(); }
        #endregion

        #region Convert String <-> Base64
        /// <summary>
        /// String 轉換成 Base64
        /// </summary>
        public static string ToBase64(this string str) {
            var bytesEncode = Encoding.UTF8.GetBytes(str);
            return Convert.ToBase64String(bytesEncode);
        }
        /// <summary>
        /// Base64 轉換成 String
        /// </summary>
        public static string FromBase64(this string str) {
            var bytesDecode = Convert.FromBase64String(str);
            return Encoding.UTF8.GetString(bytesDecode);
        }
        #endregion

        public static string objectProperties2String(this object value) {
            if (value == null) { return null; }
            var props = value.GetType().GetProperties();
            var sb = new StringBuilder();
            foreach (var p in props)
            { sb.AppendLine(p.Name + ":" + p.GetValue(value, null)); }
            return sb.ToString();
        }

        #region Convert DbNull <-> Null
        /// <summary>
        /// 若值為DBNull.Value, 則轉為Null
        /// </summary>
        public static object DbNullToNull(this object original)
        { return original == DBNull.Value ? null : original; }
        /// <summary>
        /// 若值為null, 則轉成DBNull.Value
        /// </summary>
        public static object NullToDbNull(this object original)
        { return original ?? DBNull.Value; }
        #endregion

        #region Get Certain Week Number & its FirstDate, LastDate
        /// <summary>Get the week number of a certain date, provided that
        /// the first day of the week is Monday, the first week of a year
        /// is the one that includes the first Thursday of that year and
        /// the last week of a year is the one that immediately precedes
        /// the first calendar week of the next year.
        /// </summary>
        /// <param name="date">Date of interest.</param>
        /// <returns>The week number.</returns>
        public static int GetWeekNumber(this DateTime date)
        {
            //Constants
            const int JAN = 1;
            const int DEC = 12;
            const int LASTDAYOFDEC = 31;
            const int FIRSTDAYOFJAN = 1;
            const int THURSDAY = 4;
            bool thursdayFlag = false;

            //Get the day number since the beginning of the year
            int dayOfYear = date.DayOfYear;

            //Get the first and last weekday of the year
            int startWeekDay = (int)(new DateTime(date.Year, JAN, FIRSTDAYOFJAN)).DayOfWeek;
            int endWeekDay = (int)(new DateTime(date.Year, DEC, LASTDAYOFDEC)).DayOfWeek;

            //Compensate for using monday as the first day of the week
            if (startWeekDay == 0)
            {
                startWeekDay = 7;
            }
            if (endWeekDay == 0)
            {
                endWeekDay = 7;
            }

            //Calculate the number of days in the first week
            int daysInFirstWeek = 8 - (startWeekDay);

            //Year starting and ending on a thursday will have 53 weeks
            if (startWeekDay == THURSDAY || endWeekDay == THURSDAY)
            {
                thursdayFlag = true;
            }

            //We begin by calculating the number of FULL weeks between
            //the year start and our date. The number is rounded up so
            //the smallest possible value is 0.
            int fullWeeks = (int)Math.Ceiling((dayOfYear - (daysInFirstWeek)) / 7.0);
            int result = fullWeeks;

            //If the first week of the year has at least four days, the
            //actual week number for our date can be incremented by one.
            if (daysInFirstWeek >= THURSDAY)
            {
                result = result + 1;
            }

            //If the week number is larger than 52 (and the year doesn't
            //start or end on a thursday), the correct week number is 1.
            if (result > 52 && !thursdayFlag)
            {
                result = 1;
            }

            //If the week number is still 0, it means that we are trying
            //to evaluate the week number for a week that belongs to the
            //previous year (since it has 3 days or less in this year).
            //We therefore execute this function recursively, using the
            //last day of the previous year.
            if (result == 0)
            {
                result = GetWeekNumber(new DateTime(date.Year - 1, DEC, LASTDAYOFDEC));
            }

            return result;
        }
        /// <summary>
        /// Get the first date of the week for a certain date, provided
        /// that the first day of the week is Monday, the first week of
        /// a year is the one that includes the first Thursday of that
        /// year and the last week of a year is the one that immediately
        /// precedes the first calendar week of the next year.
        /// </summary>
        /// <param name="date">ISO 8601 date of interest.</param>
        /// <returns>The first week date.</returns>
        public static DateTime GetFirstDateOfWeek(this DateTime date)
        {
            if (date == DateTime.MinValue)
            { return date; }

            int week = date.GetWeekNumber();

            while (week == date.GetWeekNumber())
            { date = date.AddDays(-1); }

            return date.AddDays(1);
        }
        /// <summary>
        /// Get the last date of the week for a certain date, provided
        /// that the first day of the week is Monday, the first week of
        /// a year is the one that includes the first Thursday of that
        /// year and the last week of a year is the one that immediately
        /// precedes the first calendar week of the next year.
        /// </summary>
        /// <param name="date">ISO 8601 date of interest.</param>
        /// <returns>The first week date.</returns>
        public static DateTime GetLastDateOfWeek(this DateTime date)
        {
            if (date == DateTime.MaxValue)
            { return date; }

            int week = date.GetWeekNumber();

            while (week == date.GetWeekNumber())
            { date = date.AddDays(1); }

            return date.AddDays(-1);
        }
        #endregion


        /// <summary>
        /// 驗證 JSON 字串是否合法
        /// </summary>
        public static bool ValidateJSON(this string s) {
            try {
                JToken.Parse(s);
                return true;
            } catch { return false; }
        }

        /// <summary>
        /// 取得最深層的 Exception
        /// </summary>
        public static Exception GetOriginalException(this Exception ex) {
            if (ex.InnerException == null) return ex;
            return ex.InnerException.GetOriginalException();
        }

        #region 將 byte 轉換成 bit string
        /// <summary>
        /// 將 byte 轉換成 bit string
        /// </summary>
        public static List<string> Byte2BinaryString(this byte[] value) {
            var tpStr = new List<string>();
            foreach (byte a in value) { tpStr.Add(a.Byte2BinaryString()); }
            return tpStr;
        }
        /// <summary>
        /// 將 byte 轉換成 bit string
        /// </summary>
        public static string Byte2BinaryString(this byte value) {
            StringBuilder str = new StringBuilder(8);
            int[] bl = new int[8];

            for (int i = 0; i < bl.Length; i++) { bl[bl.Length - 1 - i] = ((value & (1 << i)) != 0) ? 1 : 0; }
            foreach (int num in bl) { str.Append(num); }

            Console.WriteLine(string.Format(CultureInfo.CurrentCulture, "{0} ::: {1}", value, str.ToString()));
            return str.ToString();
        }
        #endregion

        /// <summary>
        /// 依據 bitIndex 回傳該 bit 的 true / false
        /// </summary>
        /// <param name="bits">8 bits Of 1 byte</param>
        /// <param name="bitIndex">bit 的所在位置</param>
        public static bool _8bitsOf1ByteValue(this string bits, int bitIndex) {
            try { return (bits.Substring(bits.Length - bitIndex, 1).Equals("1")) ? true : false; }
            catch { return false; }
        }

    }
}
