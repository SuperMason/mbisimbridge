﻿using System.Globalization;
using System.Threading;

namespace MBILibrary.Util
{
    /// <summary>
    /// 調整系統 Thread Pool 的設定值
    /// </summary>
    public class UtilThreadPool
    {
        #region Properties
        /// <summary>
        /// 手動設定 Thread Pool 裡的最小 Thread 數量
        /// <para>設定最小同時執行的 Thread 數量</para>
        /// </summary>
        private const int ChangedTheMinimumNumberOfWorkerThreads = 512;
        /// <summary>
        /// 手動設定 Thread Pool 裡的最大 Thread 數量
        /// <para>設定最大同時執行的 Thread 數量</para>
        /// </summary>
        private const int ChangedTheMaximumNumberOfWorkerThreads = 32767;
        /// <summary>
        /// 顯示目前 Thread Pool 的狀態
        /// </summary>
        private const string threadStatusString = "{0} :: ThreadPool... {1} minWorker({2}), minIOC({3}), maxWorker({4}), maxIOC({5}), AvailableWorker({6}), Available Asynchronous I/O threads({7})";
        #endregion

        /// <summary>
        /// 調整系統 Thread Pool 的設定值
        /// </summary>
        public UtilThreadPool() { }

        /// <summary>
        /// 監控 ThreadPool 的狀態, 並且調整 Thread 的 Min, Max 執行個數
        /// <para>系統預設值為 [CPU個數], 如果是 4核心的CPU, Default值為 (4, 4)。 5個區域要設定 20 以上, 多個區域(>5)要設定 50 以上, 才有感覺效能較佳</para>
        /// </summary>
        public void MonitorThreadPool() {
            GetNumbers(out int minWorker, out int minIOC, out int AvailableWorker, out int AvailableIOC, out int maxWorker, out int maxIOC);
            UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, threadStatusString, UtilConstants.CurrentTimeSpanfff, "Initial", minWorker, minIOC, maxWorker, maxIOC, AvailableWorker, AvailableIOC));

            // Change the minimum number of worker threads to 'ChangedTheMinimumNumberOfWorkerThreads', but keep the old setting for minimum asynchronous I/O completion threads.
            bool SetMinThreadsRes = ThreadPool.SetMinThreads(ChangedTheMinimumNumberOfWorkerThreads, minIOC);
            if (!SetMinThreadsRes) { UtilGlobalFunc.Log("The minimum number of threads was not changed."); }

            // Change the maximum number of worker threads to 'ChangedTheMaximumNumberOfWorkerThreads', but keep the old setting for maximum asynchronous I/O completion threads.
            bool SetMaxThreadsRes = ThreadPool.SetMaxThreads(ChangedTheMaximumNumberOfWorkerThreads, maxIOC);
            if (!SetMaxThreadsRes) { UtilGlobalFunc.Log("The maximum number of threads was not changed."); }

            if (SetMinThreadsRes && SetMaxThreadsRes) {
                GetNumbers(out minWorker, out minIOC, out AvailableWorker, out AvailableIOC, out maxWorker, out maxIOC);
                UtilGlobalFunc.Log(string.Format(CultureInfo.CurrentCulture, threadStatusString, UtilConstants.CurrentTimeSpanfff, "Changed Successfully", minWorker, minIOC, maxWorker, maxIOC, AvailableWorker, AvailableIOC));
            }
        }
        /// <summary>
        /// Get Thread Pool Info.
        /// </summary>
        private void GetNumbers(out int minWorker, out int minIOC, out int AvailableWorker, out int AvailableIOC, out int maxWorker, out int maxIOC) {
            ThreadPool.GetMinThreads(out minWorker, out minIOC); // Get the current Min settings.
            ThreadPool.GetMaxThreads(out maxWorker, out maxIOC); // Get the current Max settings.
            ThreadPool.GetAvailableThreads(out AvailableWorker, out AvailableIOC); // Get the current Available settings.
        }
    }
}
