﻿using System;

namespace MBILibrary.Util
{
    public sealed class UtilSysTimes
    {
        private UtilSysTimes() { }

        /// <summary>
        /// StartDT ~ NOW(), 共耗時幾秒 ?
        /// </summary>
        public static double GetSpendingSeconds(DateTime startDT) => (DateTime.Now - startDT).TotalSeconds;

        /// <summary>
        /// StartDT ~ NOW(), 共耗時幾毫秒 ?
        /// </summary>
        public static double GetSpendingMilliseconds(DateTime startDT) => (DateTime.Now - startDT).TotalMilliseconds;

    }
}
