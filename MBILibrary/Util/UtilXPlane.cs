﻿namespace MBILibrary.Util
{
    public static class UtilXPlane
    {
        #region X-Plane Const String
        /// <summary>
        /// (DataRef Write) X-Plane Set DataRef 的字串開頭, 必須是 "DREF"
        /// </summary>
        public const string XPlaneSetDataRefHeader = "DREF";
        /// <summary>
        /// (DataRef Write) X-Plane 定義 'DREF' 的封包資料長度 : 509
        /// </summary>
        public const int XPlaneDREFMessageBodyLength = 509;
        /// <summary>
        /// (DataRef Read) X-Plane Response 的字串開頭, 必須是 "RREF" 才是我們需要的資訊內容
        /// <para>回覆我們所訂閱的 DataRef Value</para>
        /// </summary>
        public const string XPlaneResponseHeader = "RREF";
        /// <summary>
        /// (DataRef Read) X-Plane 定義 'RREF' 的封包資料長度 : 413
        /// </summary>
        public const int XPlaneRREFMessageBodyLength = 413;
        /// <summary>
        /// X-Plane Command 的字串開頭, 必須是 "CMND"
        /// </summary>
        public const string XPlaneCommandHeader = "CMND";
        /// <summary>
        /// 要著色 highlight 的字串用 [[xxxxxx]] 包起來
        /// </summary>
        public const string highlightPattern = "[[{0}]]";
        public const string startPattern = "[[";
        public const string endPattern = "]]";
        #endregion

    }
}
