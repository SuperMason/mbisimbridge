﻿using MBILibrary.Enums;
using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;

namespace MBILibrary.Util
{
    /// <summary>
    /// 檢查是否有重複開啟 Application ?
    /// </summary>
    public class SingletonCheck : IDisposable
    {
        #region fields
        /// <summary>
        /// 是否已經有開啟過相同的 MutexId app ?
        /// <para>Debug Mode : AppGuid-AppName</para>
        /// <para>Release Mode : AppGuid</para>
        /// </summary>
        public bool isAnotherInstanceOpen = false;
        private Mutex _mutex = null;
        /// <summary>
        /// Application Name
        /// </summary>
        private string AppName = string.Empty;
        private const string GlobalMutexIdFormat = "Global\\{{{0}}}";
        private const string DebugMutexIdFormat = "{0}-{1}-{2}";
        private const string ReleaseMutexIdFormat = "{0}-{1}-{2}";
        #endregion


        #region Constructor
        /// <summary>
        /// initialize mutex
        /// </summary>
        private void InitMutex() {
            #region new Mutex()
            UtilConstants.AppGuid = ((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(GuidAttribute), false).GetValue(0)).Value;
            UtilConstants.MutexId = (UtilConstants.RunConfiguration == SolutionConfigurationEnum.Debug) ?
                string.Format(CultureInfo.CurrentCulture, GlobalMutexIdFormat, string.Format(CultureInfo.CurrentCulture, DebugMutexIdFormat, UtilConstants.AppGuid, UtilConstants.RunPlatform, AppName)) :
                string.Format(CultureInfo.CurrentCulture, GlobalMutexIdFormat, string.Format(CultureInfo.CurrentCulture, ReleaseMutexIdFormat, UtilConstants.AppGuid, UtilConstants.RunPlatform, AppName));
            // 原本只有 Debug Mode 才能同時在同一台機器上 run 'MBIServer', 'MBISimBridge', 方便 Debug
            // 但 Joe 想在 Release Mode 同時 run 兩個 daemon, 所以解除限制, 現在 Release Mode 也可以同時 run
            // 如果要還原限制, 只要 ReleaseMutexIdFormat 不要帶入 AppName 即可
            _mutex = new Mutex(false, UtilConstants.MutexId);
            #endregion

            #region allow Everyone access this app
            var allowEveryoneRule = new MutexAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), MutexRights.FullControl, AccessControlType.Allow);
            var securitySettings = new MutexSecurity();
            securitySettings.AddAccessRule(allowEveryoneRule);
            _mutex.SetAccessControl(securitySettings);
            #endregion
        }
        /// <summary>
        /// 檢查是否有重複開啟 Application ?
        /// </summary>
        /// <param name="ApplicationName"></param>
        public SingletonCheck(string ApplicationName) {
            try {
                AppName = ApplicationName;
                InitMutex();

                #region get/gen Unique Key
                UtilConstants.HostRegistryLicenseGuidDateTime = new RegistryHelper(AppName, UtilConstants.HostRegistryLicenseDateTime).GetRegistryValue();
                UtilConstants.HostRegistryLicenseGuid = new RegistryHelper(AppName, UtilConstants.HostRegistryLicense).GetRegistryValue();
                UtilConstants.HostRegistryMachineName = new RegistryHelper(AppName, UtilConstants.HostRegistryName).GetRegistryValue();
                UtilConstants.UniqueKeyOfMachine = string.Format(CultureInfo.CurrentCulture, UtilConstants.UniqueKeyDataFormat, AppName, UtilConstants.HostRegistryLicenseGuid, UtilConstants.HostRegistryMachineName);
                #endregion

                isAnotherInstanceOpen = !_mutex.WaitOne(TimeSpan.Zero); // TimeSpan.Zero to test the mutex's signal state and return immediately without blocking
                if (isAnotherInstanceOpen)
                { UtilGlobalFunc.Log(UtilConstants.SingletonErrorMessage); }
            }
            catch (AbandonedMutexException)
            { isAnotherInstanceOpen = true; }
        }
        #endregion

        public void Dispose() {
            if (_mutex != null) {
                //if (isAnotherInstanceOpen)
                //{ _mutex.ReleaseMutex(); }
                _mutex.Close();
            }
        }
    }
}
