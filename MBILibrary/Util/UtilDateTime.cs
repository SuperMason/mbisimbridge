﻿namespace MBILibrary.Util
{
    public sealed class UtilDateTime
    {
        private UtilDateTime() { }
        
        #region DateTimeFormat
        public const string Format_HHmmssfffWithDot = "HH:mm:ss.fff";
        public const string Format_HHmmssfff = "HH:mm:ss fff";
        public const string Format_HHmmss = "HH:mm:ss";
        public const string Format_Default = "yyyy-MM-dd HH:mm:ss";
        public const string Format_Defaultfff = "yyyy-MM-dd HH:mm:ss.fff";
        public const string Format_yyyyMMddWithDash = "yyyy-MM-dd";
        #endregion

    }
}
