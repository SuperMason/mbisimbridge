﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace MBILibrary.Util
{
    /// <summary>
    /// 專職處理 Google.Protobuf.ByteString 轉換
    /// </summary>
    public sealed class UtilByteString
    {
        private UtilByteString() { }
        /// <summary>
        /// 將 C# object 轉換成 Google.Protobuf.ByteString.CopyFrom() 可以吃進去的格式
        /// </summary>
        public static byte[] ToByteArray<T>(T value) {
            if (value == null) { return null; }
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream()) {
                bf.Serialize(ms, value);
                return ms.ToArray();
            }
        }
        /// <summary>
        /// 將 Google.Protobuf.ByteString.ToByteArray() 轉換成 C# object
        /// </summary>
        public static T FromByteArray<T>(byte[] data) {
            if (data == null) { return default(T); }
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream(data)) {
                object obj = bf.Deserialize(ms);
                return (T)obj;
            }
        }

    }
}
